package com.monitorcloud.test;

public class TestMemory {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Runtime currRuntime = Runtime.getRuntime ();   
	       int nFreeMemory = ( int ) (currRuntime.freeMemory() / 1024 / 1024);   
	       int nTotalMemory = ( int ) (currRuntime.totalMemory() / 1024 / 1024);  
	       String info =  nFreeMemory + "M/" + nTotalMemory + "M(free/total)" ; 
	       System.out.println(info);
	}

}
