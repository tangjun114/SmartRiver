<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目监控-测点设置-监测项</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor,jqgrid" />
</head>

<body class="white-bg"  formid="mcProItemMonitorItemForm">
    <form:form id="mcProItemMonitorItemForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>监测项名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="MonitorItemName" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>

					
					
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>是否为自动化:</label>
		            </td>
					<td class="width-35">
						<form:select path="isAuto" htmlEscape="false" class="form-control"  dict="sf"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>监测组类别:</label>
		            </td>
					<%-- <td class="width-35">
						<form:select path="groupTypeName" htmlEscape="false" class="form-control"    datatype="*"    />
						<label class="Validform_checktip"></label>
					</td> --%>

			 
					<td>
					    <form:select path="groupTypeId" htmlEscape="false" class="form-control" onchange="groupTypeChange();"     />
					  
					   <form:hidden path="groupTypeName" htmlEscape="false" class="form-control"  />
 					   <form:hidden path="groupTypeCode" htmlEscape="false" class="form-control"  />
					</td>
				</tr>
				
				<tr style="display:none">
					<td  class="width-15 active text-right">	
		              <label>设备类型:</label>
		            </td>
					<td class="width-35">
					   <form:select path="deviceTypeId" htmlEscape="false" class="form-control" onchange="deviceTypeChange();"     />
 					   <form:hidden path="deviceTypeName" htmlEscape="false" class="form-control"  />
 					</td>
				    <td  class="width-15 active text-right">	
		              <label>设备型号:</label>
		            </td>
					<td class="width-35">
					   <form:select path="deviceModelId" htmlEscape="false" class="form-control" onchange="deviceModelChange();"     />
 					   <form:hidden path="deviceModelName" htmlEscape="false" class="form-control"  />
 					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>依据规范:</label>
		            </td>
					<td class="width-35">
 						<form:gridselect gridId="mcProItemStandardGridId" title="依据规范" nested="false"
						   path="standardId" gridUrl="${adminPath}/supervise/mcproitemstandard?projectId=${param.projectId}" value="${data.standardId}"
						   labelName="standardName" labelValue="${data.standardName}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="standardName"
						 
						   htmlEscape="false" class="form-control" readonly="true" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>观测方法名称:</label>
		            </td>
					<td class="width-35">
						<form:select path="observationName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					 
					<td  class="width-15 active text-right">	
		              <label>累计值报警值统计:</label>
		            </td>
					<td class="width-35">
						<form:input path="totalAlarmValue" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				
					<td  class="width-15 active text-right">	
		              <label>变化速率报警值统计:</label>
		            </td>
					<td class="width-35">
						<form:input path="rateChangeAlarmValue" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
			    </tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>累计值控制值统计:</label>
		            </td>
					<td class="width-35">
						<form:input path="totalControlValue" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				
					<td  class="width-15 active text-right">	
		              <label>变化速率控制值统计:</label>
		            </td>
					<td class="width-35">
						<form:input path="rateChangeControlValue" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>预警值:</label>
		            </td>
					<td class="width-35">
						<form:input path="yellowWarningValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				
					<td  class="width-15 active text-right">	
		              <label>设计数量:</label>
		            </td>
					<td class="width-35">
						<form:input path="designQuantity" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				 
		  		</tr>
		  		<tr>
		  		                      <td  class="width-15 active text-right">	
		              <label>备注:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="remarks" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
		  		</tr>
		      <tr>
					
				
					 
				 
		  		</tr>
		   </tbody>
		</table>   
	</form:form>
 <html:js name="ff" />
<html:js name="bootstrap-fileinput" />
<html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline" />
<script type="text/javascript">

    $(function()
    {
    		var selector= $("select[name=groupTypeId]");  
    		 
         	ff.form.combox.load(selector,{
        		  url: "sm/mcsmmonitorgrouptype/loadbyproject",
        	      valueField: "id",
        	      select:$("#groupTypeName").val(),
        	      labelField: "groupTypeName",
        	      data:{projectId:"${projectId}"}
         		  
        		},function(){
        			loadDeviceType($("#groupTypeName").val());
        			groupTypeChange();
            	});
         	loadDeviceType();
         	
         	
     });
    
    function loadDeviceType(groupTypeName)
    {
    	var data = null;
    	if(null != groupTypeName && '' != groupTypeName)
    	{
    		data = {"name":groupTypeName};
    	}
    	
    	
    	var selector= $("select[name=deviceTypeId]");  
    	ff.form.combox.load(selector,{
    	     url: "sm/mcsmparamconfig/devicebytype",
    	     select:$("#deviceTypeName").val(),
    	     valueField: "id",
    	     labelField: "name",
    	     data:data
    	},function(){
    		loadDeviceModel($("#deviceTypeName").val());
    	});
    }
    
    
    function loadDeviceModel(typeName)
    {
    	var data = null;
    	if(null != typeName && '' != typeName)
    	{
    		data = {"name":typeName};
    	}
    	
    	
    	var selector= $("select[name=deviceModelId]");  
    	ff.form.combox.load(selector,{
    	     url: "sm/mcsmparamconfig/devicebymodel",
    	     select:$("#deviceModelName").val(),
    	     valueField: "id",
    	     labelField: "name",
    	     data:data
    	});
    }
    function groupTypeChange()
    {
    	var groupTypeName = $("#groupTypeId").find("option:selected").text();
    	$("#groupTypeName").val(groupTypeName);
    	var option=$("select[name=groupTypeId] option:selected");
    	var item=  ff.com.getOptions(option);
    	 $("#groupTypeCode").val(item.code);
     	 $("#remarks").val(item.itemRemark)
    	 
    	 loadObservationName(item.defaultMethod);
    	 
    	 loadDeviceType(groupTypeName);
    }
    function loadObservationName(method)
    {
    	debugger;
    	var methodList = method.split("；");
    	var dataList = [];
    	for(var i=0;i<methodList.length;i++)
    	{
    		var obj = {id:methodList[i],name:methodList[i]};
    		dataList.push(obj);
    	}	
    	var select = '${data.observationName}';
    	if(null == select)
    	{
    		select = 'first';
    	}
    	
    	var selector= $("select[name=observationName]"); 
    	ff.form.combox.load(selector,{
  		  data:dataList,
  	      valueField: "id",
  	      select:select,
  	      labelField: "name",
   		  
  		});
    }
    
    function deviceTypeChange()
    {
    	var deviceTypeName = $("#deviceTypeId").find("option:selected").text();
    	$("#deviceTypeName").val(deviceTypeName);
    	loadDeviceModel($("#deviceTypeName").val());
   
    }
    function deviceModelChange()
    {
    	var deviceModelName = $("#deviceModelId").find("option:selected").text();
    	$("#deviceModelName").val(deviceModelName);
    	 
   
    }
    
	function subareaDataPicker(rowDatas, fildId, fildName) {
		$("#" + fildId).val(rowDatas[0].id);
		$("#" + fildName).val(rowDatas[0].subareaName);
		$("#" + fildName).focus();
	}
 

</script>
</body>
</html>