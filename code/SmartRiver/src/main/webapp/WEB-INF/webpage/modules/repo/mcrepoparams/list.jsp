<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>技术参数列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="技术参数">
<grid:grid id="mcRepoParamsGridId" url="${adminPath}/repo/mcrepoparams/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.path" hidden="true"  name="path" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="name" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <%-- <grid:button title="下载"  groupname="opt" function="downloadFile" outclass="btn-success" innerclass="fa-file" url="${adminPath}/" path="path" /> --%>
    <grid:column label="文件名"  name="name"  query="true"  formatter="button" queryMode="input"  condition="like" />
    <grid:column label="上传时间"  name="createDate" />
    <grid:column label="上传人"  name="createByName" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="func" />
<script type="text/javascript">
function mcRepoParamsGridIdNameFormatter(value, options, row){
	  try{
	         if(!row.id) {
               return '';
           }
	        debugger;
	         var href=linkFormatter(row.name,row.path);
	   }catch(err){}
	   return href;
	}
</script>
</body>

</html>