<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>监测结果统计</title>
    <meta name="decorator" content="single"/>
</head>

<body class="white-bg" >

    <div class="animated fadeInRight">
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="myTab">
           <li role="presentation" class="active">
                <a href="#status" aria-controls="status" role="tab" data-toggle="tab">状态</a>
               </li>
            <li role="presentation" >
                <a href="#area" aria-controls="area" role="tab" data-toggle="tab">区域</a></li>
            <li role="presentation" >
                <a href="#type" aria-controls="type" role="tab" data-toggle="tab">类别</a></li>
       
              
       </ul>
        <!-- Tab panes -->
        <div class="tab-content" style="overflow-y:hidden;">
            <div role="tabpanel" class="tab-pane in active" id="status">
            <iframe src="${adminPath}/supervise/mcprojectstatistics/status" width="100%" height="100%" frameborder="0" scrolling="no" id="imgs_content"></iframe> 
            </div>
			<div role="tabpanel" class="tab-pane" id="area">
				<iframe src="${adminPath}/supervise/mcprojectstatistics/area" width="100%" height="100%" frameborder="0" scrolling="no" id="areaIframe"></iframe> 
			</div>
			 <div role="tabpanel" class="tab-pane" id="type">
				<iframe src="${adminPath}/supervise/mcprojectstatistics/type" width="100%" height="100%" frameborder="0" scrolling="no" id="typeIframe"></iframe> 
			</div>
        </div>
 
    </div>
 
    </div>

<html:js name="func" />
<html:js name="ff,layer,bootstrap,bootstrap-table" />
<script>
var winHeight;

    $(function () {
        $('#myTab a:first').tab('show');
    })
$(document).ready(function () {
	  var winHeight = 0;
	if (window.innerHeight) {
			winHeight = window.innerHeight;
		} else if ((document.body) && (document.body.clientHeight)) {
			winHeight = document.body.clientHeight;
		}
		var newH = winHeight - 50; 
     	$("#status, #area,#type").css({
			"height" : newH + "px",
			"overflow-y":"hidden"
		});  

		
	});
</script>
</body>
</html>