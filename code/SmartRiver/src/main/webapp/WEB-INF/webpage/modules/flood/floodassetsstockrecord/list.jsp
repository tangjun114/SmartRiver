<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>库存统计查询列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="库存统计查询">
<grid:grid id="floodAssetsStockRecordGridId" url="${adminPath}/flood/floodassetsstockrecord/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="资产代码"  name="assetsCode"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="资产名称"  name="assetsName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="库存变化数量"  name="changeQty" />
    <grid:column label="库存变化类型名称"  name="changeType" />
    <grid:column label="单位"  name="unit" />
    <grid:column label="库存点名称"  name="stockLocation" />
    <grid:column label="状态"  name="status"  query="true"  queryMode="select"  condition="eq"  dict="flood_stock_status"/>
    <grid:column label="申请人用户名"  name="applyUserName" />
    <grid:column label="审核人用户名"  name="checkUserName" />
    <grid:column label="审核时间"  name="checkTime" />
	<grid:toolbar title="审核"  function="update" url="${adminPath}/flood/floodassetsstockrecord/{id}/update"/>
	
	<grid:toolbar function="exportFile"/>
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>