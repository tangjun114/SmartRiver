<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>报警短信列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="报警短信">
<grid:grid id="mcProItemMessageGridId" url="${adminPath}/supervise/mcproitemmessage/ajaxList?projectId=${projectId}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
 
    
    <grid:column label="监测项名称"  name="monitorItemName" />
    <grid:column label="测点列表"  name="ponitList" />
    <grid:column label="用户名"  name="userName" />
    <grid:column label="时间"  name="createDate" />
    <grid:column label="短信内容"  name="content" />
    <grid:column label="状态"  name="status" />
    <grid:column label="类型"  name="type" />
	 
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<script type="text/javascript">
var winHeight;
$(document).ready(function()   
{  
	//获取窗口高度 兼容各个浏览器
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}

	var newH = winHeight-210;
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
}); 
</script>
</body>
</html>