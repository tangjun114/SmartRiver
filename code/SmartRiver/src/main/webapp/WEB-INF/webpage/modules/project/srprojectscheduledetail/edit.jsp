<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>进度拆分管理</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput"/>
    <html:css name="simditor"/>
</head>

<body class="white-bg" formid="srProjectScheduleDetailForm">
<form:form id="srProjectScheduleDetailForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>
            <td class="width-15 active text-right">
                <label>截止日期:</label>
            </td>
            <td class="width-35">
                <form:input path="deadTime" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>描述:</label>
            </td>
            <td class="width-35">
                <form:input path="desc" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>所占比例 %:</label>
            </td>
            <td class="width-35">
                <form:input path="rate" htmlEscape="false" class="form-control" type="number" min="1" max="100"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>备注信息:</label>
            </td>
            <td class="width-35">
                <form:input path="remarks" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>

        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline,layer"/>
<html:js name="ff"/>
<script type="text/javascript">

    $(function () {


        var today = new Date().format("yyyy-MM-dd");
        var day = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
        if ("" == document.getElementById("deadTime").value) {
            document.getElementById("deadTime").value = today + " 23:59:59";
        }

    });
</script>

</body>
</html>