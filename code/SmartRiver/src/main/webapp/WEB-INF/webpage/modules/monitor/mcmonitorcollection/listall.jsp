<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>通信设备列表</title>
  <meta name="decorator" content="list"/>
    <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
</head>
<body title="通信设备">

 <div class="row">

<div id="tree" class="col-sm-3 col-md-2" >
        
	   <view:treeview id="organizationTreeview" treeviewSettingCallback="callback" dataUrl="${adminPath}/sys/organization/bootstrapTreeData" onNodeSelected="orgOnclick"></view:treeview>
	    <script type="text/javascript">
	    function callback(){
     	 
 	    	var tree = $('#organizationTreeview').treeview(true) ;
	    	var node = tree.getSelected();
	    	if(null != node && node.length != 0)
	    	{
	    		$("input[name='monitorId']").val(node[0].href);
	    		var item = {"id":node[0].href,"name":node[0].text};
			    ff.cache.set("organization",item);
	    		setTimeout("search('mcMonitorCollectionGridIdGrid')",500);
	    	}	
 	    	 
	    };
	       function orgOnclick(event, node) {
  	    	   $("input[name='monitorId']").val(node.href);
	    	   search('mcMonitorCollectionGridIdGrid');
	    	   debugger;
		   	   var item = {"id":node.href,"name":node.text};
 				ff.cache.set("organization",item);
	       }
	   </script>
	</div>
<div id="grid"  class="col-sm-12 col-md-12">
<grid:grid id="mcMonitorCollectionGridId" gridSetting="{shrinkToFit:true}" url="${adminPath}/monitor/mcmonitorcollection/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:query name="monitorId"  queryMode="hidden" />
    <grid:column label="类型"  name="type" hidden="true"   />
    <grid:column label="型号"  name="unitType" hidden="true"   />
    <grid:column label="设备名称"  name="name"  query="true"    condition="eq"   />
    <grid:column label="编号"  name="code"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="电话号码"  name="phone" />
    <grid:column label="使用状态"  name="status"  dict="SBXX_SBSFSY"/>
    <grid:column label="类型"  name="typeName" />
    <grid:column label="型号"  name="unitTypeName" />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button title="sys.common.delete"  groupname="opt" function="delete"   />
    <grid:toolbar title='展开/折叠' function="show"/>
    <grid:toolbar function="create" url="${adminPath}/monitor/mcmonitorcollection/create?projectId=${projectId}"/>
  	<grid:toolbar function="update"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</div>
 
</div>
<script type="text/javascript">   
var status = 1;
var  winHeight;
var projectId = '${projectId}';
$(document).ready(function()   
{  
	//获取窗口高度 兼容各个浏览器
	show();
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}

	var newH = winHeight-250;
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});   

</script> 
</body>
</html>