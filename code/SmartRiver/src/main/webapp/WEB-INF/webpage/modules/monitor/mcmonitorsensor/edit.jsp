<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>传感器信息</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor,jqgrid" />
</head>

<body class="white-bg"  formid="mcMonitorSensorForm" beforeSubmit="beforeSubmit">
    <form:form id="mcMonitorSensorForm" modelAttribute="data"  method="post" class="form-horizontal">
		<form:hidden path="id"/>
		
		<form:hidden path="monitorItemId"/>
		<form:hidden path="monitorItemTypeCode"/>
	 
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr class="t_com" >
					<td  class="width-15 active text-right">	
		              <label>传感器编号:</label>
		            </td>
					<td class="width-35">
						<form:input path="sensorNum" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					 
				
					<td  class="width-15 active text-right">	
		              <label>传感器名称:</label>
		            </td>
					<td class="width-35">
					   <form:select path="sensorType" htmlEscape="false" class="form-control" onchange="sensorTypeChange();"     />
 					   <form:hidden path="sensorTypeName" htmlEscape="false" class="form-control"  />
 					</td>
 				</tr>
               <tr class="t_com">
				    <td  class="width-15 active text-right">	
		              <label>传感器型号:</label>
		            </td>
					<td class="width-35">
					   <form:select path="sensorModel" htmlEscape="false" class="form-control" onchange="sensorModelChange();"     />
 					   <form:hidden path="sensorModelName" htmlEscape="false" class="form-control"  />
 					</td>
				 
				
					 
					<td  class="width-15 active text-right">	
		              <label>精度:</label>
		            </td>
					<td class="width-35">
						<form:input path="accuracy" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr class="t_com">
					<td  class="width-15 active text-right">	
		              <label>初始值:</label>
		            </td>
					<td class="width-35">
						<form:input path="initialValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>标定系数(K):</label>
		            </td>
					<td class="width-35">
						<form:input path="caliCoefficient" htmlEscape="false" class="form-control"/>
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				 
				<tr class="t_extend">
					<td  class="width-15 active text-right">	
		              <label>购买价格:</label>
		            </td>
					<td class="width-35">
						<form:input path="buyPrice" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>购买时间:</label>
		            </td>
					<td class="width-35">
						<form:input path="buyDate" htmlEscape="false"  datefmt="yyyy-MM-dd" class="form-control layer-date" placeholder="YYYY-MM-DD" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})" />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				
				<tr class="t_com">
					<td  class="width-15 active text-right">	
		              <label>量程最小值:</label>
		            </td>
					<td class="width-35">
						<form:input path="rangeMin" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>量程最大值:</label>
		            </td>
					<td class="width-35">
						<form:input path="rangeMax" htmlEscape="false" class="form-control"/>
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr >
					
					<td  class="width-15 active text-right">	
		              <label>使用状态:</label>
		            </td>
					<td class="width-35">
						<form:input path="status" htmlEscape="false" class="form-control" readonly="true"/>
						<label class="Validform_checktip"></label>
					</td>
					<td   class="width-15 active text-right t_extend spec">	
			              <label>规格:</label>
			            </td>
					 <td class="width-35 t_extend spec">
							<form:select path="spec" htmlEscape="false" class="form-control"/>
							<label class="Validform_checktip"></label>
					</td>
				 
				 
					</tr>
		 
				<tr>
					<td  class="width-15 active text-right t_extend  S002 S003 S004 S005 S008 S010 S011">	
		              <label>温补系数:</label>
		            </td>
					<td class="width-35 t_extend S002 S003 S004 S005 S008 S010 S011">
						<form:input path="tempCoefficient" htmlEscape="false" class="form-control"/>
						<label class="Validform_checktip"></label>
					</td>
					
					<td  class="width-15 active text-right t_extend S002">	
			              <label>钢筋计截面积（mm2）:</label>
			            </td>
						<td class="width-35 t_extend S002">
							<form:input path="rebarSectArea" htmlEscape="false" class="form-control"/>
							<label class="Validform_checktip"></label>
						</td>
					
				</tr>
		 
				<tr>
					<td  class="width-15 active text-right">	
		              <label>图片:</label>
		            </td>
					<td class="width-35">
						<form:fileinput showType="avatar" fileInputWidth="100px"  fileInputHeight="100px"  path="img" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td class="width-15 active text-right"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
	<div class="row">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
            </ul>
            <div class="tab-content">
          
            </div>
        </div>
    </div>
<html:js name="bootstrap-fileinput" />
<html:js name="ff" />
<html:js name="func,simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline" />
<script>
	$(document).ready(function () {
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	    	 resizeGrid();
		});
	    
	});
 
    $(function(){
    	  
    	init("S001");
    	loadSensorType();
    });
    
    function init(type)
    {   debugger;
    	if(null == type)
    	{
    		var option=$("select[name=sensorType] option:selected");
        	var item=  ff.com.getOptions(option);
    		type = item.paraValue;
    	}	
    	$(".t_com").show();
     	$(".t_extend").hide();
     	$("."+type).show();
     	
       	var selector= $("select[name=spec]");  
        
     	ff.form.combox.load(selector,{
  		  url: "sys/dict/loadbygroup?group="+type,
  	      valueField: "label",
  	      labelField: "value",
  	      select:"first",
  		  data: {
  			  group: type
  		  },
     	 
  		 
  		},function(obj)
   	   {
   		  if(null==obj || obj.length == 0)
   		  {
   			  $(".spec").hide();
   		  }	  
   		  else
   		  {
   			  $(".spec").show();
   		  }	  
   	  });
     	
    }

    function loadSensorType()
    {
    	var selector= $("select[name=sensorType]");  
    	ff.form.combox.load(selector,{
    	     url: "sm/mcsmparamconfig/sensorbytype",
    	     select:$("#sensorTypeName").val(),
    	     valueField: "id",
    	     labelField: "name"
    	},function()
	     {
	    	 sensorTypeChange();
	     });
    }
    function loadSensorModel(typeName)
    {
    	
    	var data = null;
    	if(null != typeName && '' != typeName)
    	{
    		data = {"name":typeName};
    	}
    	
    	
    	var selector= $("select[name=sensorModel]");  
    	ff.form.combox.load(selector,{
    	     url: "sm/mcsmparamconfig/sensorbymodel",
    	     select:$("#sensorModelName").val(),
    	     valueField: "id",
    	     labelField: "name",
    	     data:data
    	});
    }
    
    function sensorTypeChange()
    {
    	var sensorTypeName = $("#sensorType").find("option:selected").text();
     
    	$("#sensorTypeName").val(sensorTypeName);
        init();
    	loadSensorModel($("#sensorTypeName").val());
   
    }
    function sensorModelChange()
    {
    	var sensorModelName = $("#sensorModel").find("option:selected").text();
    	$("#sensorModelName").val(sensorModelName);
    	 
   
    }
	
	$(function(){
	   $(window).resize(function(){   
		   resizeGrid();
	   });
	});
	function resizeGrid(){
	}

	/**
	*提交回调方法
	*/
	function beforeSubmit(curform){
		 //这里最好还是使用JSON提交，控制器改变提交方法，并使用JSON方式来解析数
		 //通过判断，如果有问题不提交
		 return true;
	}
	
	function initGridFormData(curform,gridId){
		 var rowDatas =getRowDatas(gridId+"Grid");
		 var rowJson = JSON.stringify(rowDatas);
		 if(rowJson.indexOf("editable") > 0&&rowJson.indexOf("inline-edit-cell") )
	     {
	    	 return false;
	     }
		 var gridListJson=$('#'+gridId+"ListJson").val();
		 if(gridListJson==undefined||gridListJson==''){
			 var rowInput = $('<input id="'+gridId+'ListJson" type="hidden" name="'+gridId+'ListJson" />');  
			 rowInput.attr('value', rowJson);  
			 // 附加到Form  
			 curform.append(rowInput); 
		 }else{
			 $('#'+gridId+"ListJson").val(rowJson);
		 }
		 return true;
	}
</script>
</body>
</html>