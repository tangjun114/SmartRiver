<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目监控-测点设置-监测项-测点</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemMeasuringPointForm">
    <form:form id="mcProItemMeasuringPointForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>备注信息:</label>
		            </td>
					<td class="width-35">
						<form:input path="remarks" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>调试模式:</label>
		            </td>
					<td class="width-35">
						<form:select path="debug" htmlEscape="false" class="form-control"  dict="sf"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>测点编号:</label>
		            </td>
					<td class="width-35">
						<form:input path="measuringPointCode" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>继承名:</label>
		            </td>
					<td class="width-35">
						<form:input path="inheritName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>继承:</label>
		            </td>
					<td class="width-35">
						<form:select path="inheriCheck" htmlEscape="false" class="form-control"  dict="sf"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>监测点类型:</label>
		            </td>
					<td class="width-35">
						<form:input path="measuringPointTypeName" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>监测仪器类型:</label>
		            </td>
					<td class="width-35">
						<form:input path="instrumentTypeVal" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>监测仪器:</label>
		            </td>
					<td class="width-35">
						<form:input path="instrumentModelName" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>极坐标方向:</label>
		            </td>
					<td class="width-35">
						<form:select path="polarCoordinates" htmlEscape="false" class="form-control"  dict="polarCoordinates"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>初始值次数:</label>
		            </td>
					<td class="width-35">
						<form:select path="initialValueTimes" htmlEscape="false" class="form-control"  dict="InitialValueTimes"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>报警值设置类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="alarmValueType" htmlEscape="false" class="form-control"  dict="alarmValueType"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>预警值(mm):</label>
		            </td>
					<td class="width-35">
						<form:input path="warningValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>报警值(mm):</label>
		            </td>
					<td class="width-35">
						<form:input path="alarmValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>控制值(mm):</label>
		            </td>
					<td class="width-35">
						<form:input path="controlValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>速率报警值(mm/d):</label>
		            </td>
					<td class="width-35">
						<form:input path="rateAlarmValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>报警组选择:</label>
		            </td>
					<td class="width-35">
						<form:input path="alarmGroupName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>断面起点:</label>
		            </td>
					<td class="width-35">
						<form:input path="sectionStartName" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>断面终点:</label>
		            </td>
					<td class="width-35">
						<form:input path="sectionEndName" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>断面方向选择:</label>
		            </td>
					<td class="width-35">
						<form:select path="directionSelection" htmlEscape="false" class="form-control"  dict="directionSelection"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>初始累计值(mm):</label>
		            </td>
					<td class="width-35">
						<form:input path="initialCumulativeValue" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>初始累计值设:</label>
		            </td>
					<td class="width-35">
						<form:input path="initialSettingExtend" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>报警值设置扩展:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="alarmRangeExtend" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>支撑参数设置:</label>
		            </td>
					<td class="width-35">
						<form:input path="supportParam" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>对角点设置:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="diagonalSetting" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>支撑类型Val:</label>
		            </td>
					<td class="width-35">
						<form:input path="supportType" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>支撑类型:</label>
		            </td>
					<td class="width-35">
						<form:input path="supportTypeName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>传感器数量:</label>
		            </td>
					<td class="width-35">
						<form:select path="sensorQuantity" htmlEscape="false" class="form-control"  dict="sensorQuantity"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>原始数据类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="rawDataType" htmlEscape="false" class="form-control"        />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>传感器类别:</label>
		            </td>
					<td class="width-35">
						<form:select path="sensorType" htmlEscape="false" class="form-control"        />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>传感器信息:</label>
		            </td>
					<td class="width-35">
						<form:input path="sensorInfo" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>