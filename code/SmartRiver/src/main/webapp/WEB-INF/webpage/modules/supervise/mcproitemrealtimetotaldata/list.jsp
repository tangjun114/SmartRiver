<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>工作量统计</title>
  <meta name="decorator" content="list"/>
      <link rel="stylesheet" href="${staticPath}/vendors/bootstrap/js/bootstrap-table.css?v=1.0.7" type="text/css" media="screen" />
  
</head>
<body title="工作量统计">
 
    <div class="tab-content" style="overflow-x:hidden;">
        
        <form id="ff_filter"   method="post">
					 <div class="row">
					  <div class="col-sm-12">
					   <div class="form-inline">
					        起止日期：
					   <input  type="hidden" filter="eq" name="projectId" class="form-control layer-date"  value="${projectId }"    />
					 
 					   <input id="startTime"  filter="ge" name="day" class="form-control layer-date"  onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"       />
					   —— <input id="endTime"  filter="le" name="day" class="form-control layer-date"   onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"       />
					   </div>
					   <div class="pull-right">
                            <a onclick="load()" class="btn btn-primary btn-sm" ><i class="fa fa-arrow-circle-up"></i>&nbsp;&nbsp;查询</a>
                            <a onclick="location.href=location.href;"  class="btn btn-primary btn-sm"><i class="fa fa-remove"></i>&nbsp;&nbsp;清空</a>
  					  </div>
  					  </div>
  					  </div>
					</form>
             <table id="sumData"  class='table  ' style="  word-wrap:break-all;">
             
             </table>
		 
    </div>
	 <html:js name="ff,layer,bootstrap,bootstrap-table" />
   
	<script type="text/javascript">
 

	//载入图表
	$(function () {
		
		var today = new Date().format("yyyy-MM-dd");
		var day = new Date(new Date().getTime()-7*24*60*60*1000);
		  
		$("#startTime").val(day.format("yyyy-MM-dd"));
		$("#endTime").val(today);
	 
		load();
     
	    
	});
	function load()
	{
		
	    ff.req.filter = ff.com.getFilterObj("#ff_filter");
		ff.util.submit({
			url: "supervise/mcproitemrealtimetotaldata/sum?projectId=${param.projectId}",
			data:"${param.projectId}",
            success:function(rsp)
            {
            	loadTable(rsp.obj);
            }
		});
	}
	function loadTable(data)
	{
		  if(null == data || data.length == 0)
		  {
			  return;
		  }
		  var columns = [];
		  
		  columns.push({field:"日期",title:"日                 期"});
		  for(var key in data[0])
		  {
			  var col = {};
			  col.field = key;
			  col.title = key;
			 
			  if(key=='日期' || key=='汇总')
			  {
 			  }	  
 			  else
			  {
				  columns.push(col);
			  }
			  
			  
		  }	  
		  columns.push({field:"汇总",title:"汇总"});
 
		  $("#sumData").bootstrapTable('destroy').bootstrapTable({
			    columns: columns,
			    data: data

	      });
	 
		  
	}
	
 
	</script>
</body>
</html>