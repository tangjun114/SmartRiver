<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目沟通交流</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemExchangeForm">
    <form:form id="mcProItemExchangeForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>可见类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="visibleType" htmlEscape="false" class="form-control"  dict="member_sub_type"   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>内容类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="infoType" htmlEscape="false" class="form-control"  dict="implementLogType"       />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>附件路径:</label>
		            </td>
					<td colspan="4" class="width-35">
						<form:fileinput path="filePath" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>

				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>图片路径:</label>
		            </td>
					<td colspan="4" class="width-35">
						<form:fileinput path="imgPath" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>交流内容:</label>
		            </td>
					<td colspan="4" class="width-35">
						<form:textarea path="content" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>

		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>