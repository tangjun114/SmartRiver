<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>成果管理</title>
  <meta name="decorator" content="list"/>
      <link rel="stylesheet" href="${staticPath}/vendors/bootstrap/js/bootstrap-table.css?v=1.0.7" type="text/css" media="screen" />
 <style type="text/css">
 		.color_warn {
			color: #FCB247;
		}
		.color_caution {
			color: red;
		}
		.color_control {
			color: #993366;
		}	
		.gly_green{font-size:20px;color:green;text-shadow:#CCC 2px 2px 2px;}
		.gly_yellow{font-size:20px;color:orange;text-shadow:#CCC 2px 2px 2px;}
		.gly_red{font-size:20px;color:red;text-shadow:#CCC 2px 2px 2px;}
		.gly_navy{font-size:20px;color:#993366;text-shadow:#CCC 2px 2px 2px;}	
  th.ui-th-column div{
    white-space:normal !important;
    height:auto !important;
    text-align: center;
    padding:0px;
}
</style>
</head>
<body title="成果管理">
<grid:grid id="mcProItemRealTimeMonDataGridId" url="${adminPath}/supervise/mcproitemrealtimemondata/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
 	 <grid:column label="测点编号"  hidden="true"  name="measuringPointCode"  />
 	 <grid:column label="深度"  hidden="true"  name="nowValueDepth" />
 		<grid:column label="本次深度" hidden="true" name="minusValueDepth" />
	<grid:column label="变化率深度"  hidden="true" name="changeRateDepth" />
	<grid:column label="累计变化深度" hidden="true" name="sumValueDepth" />
	<grid:column label="标志" hidden="true" name="flag" />
    <grid:column label="测试" hidden="true"   name="test" />
	
    <grid:column label="监测项选择"  name="monitorItemId"  hidden="true" query="true"  queryMode="select"  condition="eq"  dict="0" /> 
	<grid:column label="测点选择"  name="measuringPointId"  hidden="true" query="true"  queryMode="select"  condition="eq"  dict="0" /> 
	<grid:column label="场次"  name="projectCount"  hidden="true" query="true"    queryMode="select"  condition="eq"  dict="0"  /> 
	<grid:column label="测点编号"  name="measuringPointCode" formatter="button" />
 	<grid:column label="测量次数"  name="testCount" />
	<grid:column label="初始值"  name="initValue" formatter="number" formatoptions="{decimalPlaces:1}"/>
	
	<grid:column label="本次结果"  name="nowValue"   formatter="label" />
	<grid:column label="本次变化"  name="minusValue" formatter="label" />
	<grid:column label="变化率"  name="changeRate" formatter="label" />
	<grid:column label="累计变化"  name="sumValue"  formatter="label" />
	<grid:column label="数据采集时间"  name="collectTime" sortable="true" />
	<grid:column label="数据上传时间"  name="uploadTime" />
 <grid:column label="有效性"  name="dataValid" />
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>

    <div class="tab-content" style="overflow-y:hidden;">
        
             <table id="sumData"  class='table table-hover'>
             
             </table>
		 
    </div>
    
 
<html:js name="ff,layer,bootstrap,bootstrap-table" />
<script type="text/javascript">

 
function loadTable(data)
{
	  var dataNew = [];
	  debugger;
	  if(null != data)
	  {
		  for(var i=0;i<data.length;i++)
		  {
			  var temp = data[i];
			  dataNew.push(temp);
			  var temp1 = ff.util.copy(temp);
			  temp1.minName = temp.maxName
			  temp1.minValue = temp.maxValue
			  temp1.minPoint = temp.maxPoint
			  dataNew.push(temp1);
			  
			  var temp2 = ff.util.copy(temp);
			  temp2.minName = temp.rateName
			  temp2.minValue = temp.rateValue
			  temp2.minPoint = temp.ratePoint
			  
			  temp2.alarmValue = temp.rateAlarmValue
			  temp2.controlValue = temp.rateControlValue
			  dataNew.push(temp2);
		  }
	  }	  
	  $('#sumData').bootstrapTable('destroy');
	  $("#sumData").bootstrapTable({
		    columns: [{
		        field: 'monitorItemName',
		        title: '监测项名称'
		    }, {
		        field: 'minName',
		        title: '最大值'
		    }, {
		        field: 'minValue',
		        title: '数值',
		       
		    }, {
		        field: 'minPoint',
		        title: '测点编号'
		    }, {
		        field: 'alarmValue',
		        title: '报警值'
		    }, {
		        field: 'controlValue',
		        title: '控制值'
		    }, {
		        field: 'alarmPoint',
		        title: '报警测点'
		    }, {
		        field: 'alarmStatus',
		        title: '安全状态',
		        formatter: alarmTypeFormat
		    }],
		    data: dataNew

      });
	  
	  for(var i=0;i<data.length;i++)
	  {
		  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'monitorItemName', colspan: 0, rowspan:3});
		  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'alarmValue', colspan: 0, rowspan:2});
		  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'controlValue', colspan: 0, rowspan:2});
	 	  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'alarmPoint', colspan: 0, rowspan:3});
		  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'alarmStatus', colspan: 0, rowspan:3});

	  }	  
	 
	 // $('#sumData').bootstrapTable('refresh');
	  
}
function alarmTypeFormat(value, row, index){
	 var alarmTypeId;
	 
	         if (row.alarmStatus == '超控'){
	        	 alarmTypeId = '3';
	         } else if (row.alarmStatus == '告警') {
	        	 alarmTypeId = '2';
	         } else if (row.alarmStatus == '预警') {
	        	 alarmTypeId = '1';
	         } else {
	        	 alarmTypeId = '0';
	         }
	         
	   return ff.service.alarm(alarmTypeId);;
}
$(function () {
	var select = ff.cache.get("monitorItemName");
	 
	if(select==null)
	{
		select = 'first';
	}	
	ff.cache.set("monitorItemName",null);
	var selector2= $("select[name=monitorItemId]");  
 	ff.form.combox.load(selector2,{
		  url: "supervise/mcproitemmonitoritem/loadall",
	      valueField: "id",
	      labelField: "monitorItemName",
	      select:select,
		  data: {
			  projectId: '${projectId}'
		  } 
		 },function(){
			 
			 loadProjectCount();
			 loadUnit();
			 loadPoint();}
		 );
 	
 	selector2.change(function(){
 	 
 		loadProjectCount();
 		loadUnit();
 		loadPoint();
 		});

});

function formatData(fieldName,row,num)
{
	  var temp  = 1;
	  if(null != num)
	  {
		  temp = num;
	  }	  
	  
	  var href = parseFloat(row[fieldName]).toFixed(temp);
	  if(null != row[fieldName+"Depth"])
	  {
		 href += "(" + row[fieldName+"Depth"] + "m)";
	  }	  
 
	  if( !(row.dataValid =='未处理' ||  row.dataValid =='有效' ||  row.dataValid =='重布观测' ))
	  {
		  href = row.flag;
	  }	
 	   return href;
}
function mcProItemRealTimeMonDataGridIdNowValueFormatter(value, options, row){
 
 	   return formatData('nowValue',row);
}
function mcProItemRealTimeMonDataGridIdMinusValueFormatter(value, options, row){
	return formatData('minusValue',row);
}
function mcProItemRealTimeMonDataGridIdSumValueFormatter(value, options, row){
	return formatData('sumValue',row);
}
function mcProItemRealTimeMonDataGridIdChangeRateFormatter(value, options, row){
	return formatData('changeRate',row,2);
}
function loadProjectCount()
{
	debugger;
	var selector2= $("select[name=monitorItemId]");
	var monitorItemId = selector2.val();
 
	var selector2= $("select[name=projectCount]");  
 	ff.form.combox.load(selector2,{
		  url: "supervise/mcproitemrealtimemondata/project/count",
	      valueField: "projectCount",
	      labelField: "projectCount",
	      select:"first",
		  data: {
			  monitorItemId: monitorItemId
		  } 
		 },function(){
			// setTimeout("search('mcProItemRealTimeMonDataGridIdGrid')",500);
			 reload();
		 } 
		 );
 	selector2.change(function(){
 		
 		reload();
 		 
   });
}

function reload()
{
	 debugger;
		search('mcProItemRealTimeMonDataGridIdGrid');
		var selector1= $("select[name=monitorItemId]");
		var monitorItemId = selector1.val();
 		var selector2= $("select[name=projectCount]"); 
 		var projectCount = selector2.val();
 		ff.util.submit({
			url: "supervise/mcproitemrealtimemondata/sum?projectId=${param.projectId}",
			data:{projectId:"${param.projectId}",monitorItemId:monitorItemId,projectCount:projectCount},
	        success:function(rsp)
	        {
	        	loadTable(rsp.obj);
	        }
		});
}
function loadUnit()
{
	debugger;
	var option=$("select[name=monitorItemId] option:selected");
	var item=  ff.com.getOptions(option);
	
	var head =[];
	head['initValue']="初始值<br/>({ff_unit})";
	head['nowValue']="本次结果<br/>({ff_unit})";
	head['minusValue']="本次变化<br/>({ff_unit})";
	head['changeRate']="变化率<br/>({ff_unit}/d)";
	head['sumValue']="累计变化<br/>({ff_unit})";
	
	for(var key in head)
	{
		var val = ff.service.unit.get(item.groupTypeCode,head[key]);
		$("#jqgh_mcProItemRealTimeMonDataGridIdGrid_"+key).html(val);
 
	}
	
	 
}
function loadPoint()
{
	var selector2= $("select[name=monitorItemId]");
	var item = selector2.val();
	var selector3= $("select[name=measuringPointId]");  
 	ff.form.combox.load(selector3,{
		  url: "supervise/mcproitemmeasuringpoint/loadall",
	      valueField: "id",
	      labelField: "measuringPointCode",
		  data: {
			  monitor_item_id: item
		  },
		 
		});
}

function mcProItemRealTimeMonDataGridIdMeasuringPointCodeFormatter(value, options, row){
	  var href = row.measuringPointCode;
	  var option=$("select[name=monitorItemId] option:selected");
	  var item=  ff.com.getOptions(option);
	  if(item.groupTypeCode=='T003')
	  {
		  href="<a href=\"#\" class=\"btn btn-danger\"";
	      href +="onclick=\"viewDetail('"+row.id+"')\"";
		  href +=">&nbsp "+row.measuringPointCode+"</a>&nbsp&nbsp";
	  }	  
	  
	 
	 
	   return href;
	}
function viewDetail(id){
	var url = "${adminPath}/supervise/mcproitemrealtimemondata/detail?id="+id;
	var width = "80%";
	var height = "530px";
 
	top.layer.open({
		type : 2,
		area : [ width, height ],
		title : "测斜数据",
		maxmin : false, // 开启最大化最小化按钮
		content : url,
	 
		cancel : function(index) {
		}
	 });
}
var winHeight;
$(document).ready(function()   
{  
	//获取窗口高度 兼容各个浏览器
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}

	var newH = winHeight-290;
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
}); 
</script>
</body>
</html>