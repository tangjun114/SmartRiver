<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>库存管理</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="floodAssetsStockForm">
    <form:form id="floodAssetsStockForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>资产代码:</label>
		            </td>
					<td class="width-35">
						<c:if test="${type != 0}">
							${data.assetsCode}
						</c:if>
						<c:if test="${type == 0}">
							<form:input path="assetsCode" htmlEscape="false" class="form-control" />
						</c:if>
					</td>
					<td  class="width-15 active text-right">	
		              <label>资产名称:</label>
		            </td>
					<td class="width-35">
						<c:if test="${type != 0}">
							${data.assetsName}
						</c:if>
						<c:if test="${type == 0}">
							<form:input path="assetsName" htmlEscape="false" class="form-control" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>库存数量:</label>
		            </td>
					<td class="width-35">
						<c:if test="${type == 1}">
							${data.stockQty}
						</c:if>
						<c:if test="${type != 1}">
							<form:input path="stockQty" htmlEscape="false" class="form-control" />
							<form:hidden path="type" defaultValue="${type}"  htmlEscape="false" class="form-control" />
							<label class="Validform_checktip"></label>
						</c:if>
					</td>

					<td  class="width-15 active text-right">	
		              <label>单位:</label>
		            </td>
					<td class="width-35">
						<c:if test="${type != 0}">
							${data.unit}
						</c:if>
						<c:if test="${type == 0}">
							<form:input path="unit" htmlEscape="false" class="form-control" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>库存点名称:</label>
		            </td>
					<td class="width-35">
						<c:if test="${type == 2}">
							${data.stockLocation}
						</c:if>
						<c:if test="${type != 2}">
							<form:select path="stockLocation" htmlEscape="false" class="form-control"  dict="flood_stock_location"      />
							<label class="Validform_checktip"></label>
						</c:if>
					</td>
					<td class="width-15 active text-right"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>