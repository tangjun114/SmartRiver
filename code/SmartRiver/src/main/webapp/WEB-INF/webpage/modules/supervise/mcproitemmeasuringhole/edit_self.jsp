<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目监控-测点设置-监测项-测孔</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
        <html:css name="simditor,jqgrid" />
</head>

<body class="white-bg"  formid="mcProItemMeasuringHoleForm">
    <form:form id="mcProItemMeasuringHoleForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<%-- <td  class="width-15 active text-right">	
		              <label>备注信息:</label>
		            </td>
					<td class="width-35">
						<form:input path="remarks" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td> --%>
					<td  class="width-15 active text-right">	
		              <label>调试模式:</label>
		            </td>
					<td class="width-35" colspan="3">
						<form:select path="debug" htmlEscape="false" class="form-control"  dict="sf"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr>
					<td  class="width-15 active text-right">	
		              <label>监测项:</label>
		            </td>
					<td class="width-35">
						<form:gridselect gridId="mcProItemMonitorItemGridId" title="监测项" nested="false"
						   path="monitorItemId" gridUrl="${adminPath}/supervise/mcproitemmonitoritem" value="${data.monitorItemId}"
						   labelName="monitorItemName" labelValue="${data.monitorItemName}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="monitorItemName"
						   callback="mcProItemMonitorItemDataPicker" 
						   htmlEscape="false" class="form-control" readonly="true" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				
				<tr>
					<td  class="width-15 active text-right">	
		              <label>测点编号:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="measuringPointCode" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr>
					<td  class="width-15 active text-right">	
		              <label>继承:</label>
		            </td>
					<td class="width-35">
						<form:select path="inheriCheck" htmlEscape="false" class="form-control"  dict="sf"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>继承名:</label>
		            </td>
					<td class="width-35">
						<form:input path="inheritName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					
					<td  class="width-15 active text-right">	
		              <label>监测点类型:</label>
		            </td>
					<td class="width-35"   colspan="3">
						<form:select path="measuringPointType" htmlEscape="false" class="form-control" onchange="measuringPointChange();" dict ="measuringPointType"   />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<%-- <td  class="width-15 active text-right">	
		              <label>监测仪器类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="instrumentType" htmlEscape="false" class="form-control"  dict="instrumentType"   />
						<label class="Validform_checktip"></label>
					</td> --%>
					
					<td  class="width-15 active text-right">	
		              <label>监测仪器型号:</label>
		            </td>
					<%-- <td class="width-35">
						<form:input path="instrumentModelName" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td> --%>
					
					<td class="width-35" colspan="3">
						 <form:treeselect title="传感器型号" path="instrumentModel" dataUrl="${adminPath}/sm/mcsmparamconfig/treeData" callback="selectOk" labelName="instrumentModelName" labelValue="${data.instrumentModelName}" 
						 htmlEscape="false" class="form-control"  datatype="*" readonly="true"
						 />	
						<label class="Validform_checktip"></label>
					</td>
				</tr>

				
				<tr>
					<td  class="width-15 active text-right">	
		              <label>起始深度:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="startDepth" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr>
					<td  class="width-15 active text-right">	
		              <label>深度步长:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="depthStep" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr>
					<td  class="width-15 active text-right">	
		              <label>测孔深度:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="holeDepth" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr>
					<td  class="width-15 active text-right">	
		              <label>传感器深度:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="sensorDepth" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr>
					
					<td  class="width-15 active text-right">	
		              <label>管口高程:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="pipeAltitude" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr >
					<td  class="width-15 active text-right">	
		              <label>初始值次数:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:select path="InitialValueTimes" htmlEscape="false" class="form-control"  dict="InitialValueTimes"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				
				<%-- <tr>
					<td  class="width-15 active text-right">	
		              <label>报警值设置类型:</label>
		            </td>
					<td class="width-35" colspan="3">
						<form:select path="alarmValueType" htmlEscape="false" class="form-control"  dict="alarmValueType"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr> --%>
				
				<tr id="tr_warningValue">
					<td  class="width-15 active text-right">	
		              <label>预警值(mm):</label>
		            </td>
					<td class="width-35" colspan="3">
						<form:input path="warningValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				
				<tr id="tr_alarmValue">
					<td  class="width-15 active text-right">	
		              <label>报警值(mm):</label>
		            </td>
					<td class="width-35" colspan="3">
						<form:input path="alarmValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>

				<tr id="tr_controlValue">
					<td class="width-15 active text-right"><label>控制值(mm):</label>
					</td>
					<td class="width-35"  colspan="3">
					<form:input path="controlValue" htmlEscape="false" class="form-control" /> 
					<label class="Validform_checktip"></label>
					</td>
				</tr>



				<tr id="tr_rateAlarmValue">
					<td  class="width-15 active text-right">	
		              <label>速率报警值(mm/d):</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="rateAlarmValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr id="tr_alarmGroupName">
					<td  class="width-15 active text-right">	
		              <label>报警组选择:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="alarmGroupName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				
				
				<tr id="tr_sectionStart">
					<td  class="width-15 active text-right">	
		              <label>断面起点:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:select path="sectionStart" htmlEscape="false" class="form-control"  dict="sectionPoint"   />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr  id="tr_sectionEnd">
					<td  class="width-15 active text-right">	
		              <label>断面终点:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:select path="sectionEnd" htmlEscape="false" class="form-control" dict="sectionPoint"   />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				
				<tr id="tr_directionSelection">
					<td  class="width-15 active text-right">	
		              <label>断面方向选择:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:select path="directionSelection" htmlEscape="false" class="form-control"  dict="directionSelection"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>

				<tr id="tr_InitialCumulativeValue">
					<td  class="width-15 active text-right">	
		              <label>初始累计值(mm):</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="InitialCumulativeValue" htmlEscape="false" class="form-control"     />
						<label class="Validform_checktip"></label>
					</td>
				</tr>				
				
				
				<tr id="tr_InitialSettingExtend">
					<td  class="width-15 active text-right">	
		              <label>初始累计值设:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="InitialCumulativeValue" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<%-- <td  class="width-15 active text-right">	
		              <label>报警值设置扩展:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="alarmRangeExtend" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td> --%>
				</tr>
				<tr id="tr_deepValue">
				
					<td  class="width-15 active text-right">	
		              <label>深度快速设置区:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:textarea path="deepValue" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td> 
				</tr>
				
			
				
		   </tbody>
		</table>  
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline" />
    <script language="javascript" type="text/javascript"> 
    $(function(){
    	measuringPointChange();
    });
    var mustValArry = [];
    function measuringPointChange(){
    	var val  = $("#measuringPointType").val();
    	if("sectionPoint"==val){//
    	$("tr[id^='tr_']").hide(); 
    	removeMustVal(mustValArry)
    	}else{
    	$("tr[id^='tr_']").show(); 
    	//预警值必填
    	mustValArry.push(initMustVal("alarmValue","*"));
    	//控制值必填
    	mustValArry.push(initMustVal("controlValue","*"));
    	//初始累计值
    	mustValArry.push(initMustVal("InitialCumulativeValue","*"));
    	
    	addMustVal(mustValArry);
    	}
    }
    function initMustVal(id,dataTypeRes){
    	var mustVal = {};
    	mustVal.id=id;
    	mustVal.dataTypeRes = dataTypeRes;
    	return mustVal;
    }
    function removeMustVal(mustValArry){
    	for(var i in mustValArry){
    		var item = mustValArry[i];
    		var id = item.id;
    		$("#"+id).removeAttr("datatype");
    	}
    	
    }
    function addMustVal(mustValArry){
    	for(var i in mustValArry){
    		var item = mustValArry[i];
    		var id = item.id;
    		var dataTypeRes = item.dataTypeRes||"*";
    		$("#"+id).attr("datatype",dataTypeRes);
    	}
    }
	function selectOk(nodes){
		var node = nodes[0];
		//debugger;
		if(2!=node.level||node.leaf==false){
		$("#instrumentModelClear").click();
		top.layer.alert('请选择全站仪第三级叶子节点!', {
			icon : 0,
			title : '警告'
		});
		}else{
		initParentNodes(nodes[0].getParentNode());
		}
	}
	function mcProItemMonitorItemDataPicker(rowDatas, fildId, fildName) {
		$("#" + fildId).val(rowDatas[0].id);
		$("#" + fildName).val(rowDatas[0].monitorItemName);
		$("#" + fildName).focus();
	}
    </script>
</body>
</html>