<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>工程质量检测</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput"/>
    <html:css name="simditor"/>
</head>

<body class="white-bg" formid="srProjectQualityMonitorForm">
<form:form id="srProjectQualityMonitorForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>

        <tr>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>项目名称:</label>
            </td>
            <td class="width-35">
                <form:select path="proId" htmlEscape="false" class="form-control" onchange="changeProName()"/>
                <form:hidden path="proName" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>检测指标:</label>
            </td>
            <td class="width-35">
                <form:select path="quaId" htmlEscape="false" class="form-control" onchange="changeQuaName()"/>
                <form:hidden path="quaName" htmlEscape="false" class="form-control"/>
                <form:hidden path="quaType" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>

        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>是否达标(0:不达标,1:达标):</label>
            </td>
            <td class="width-35">
                <form:select path="isReach" htmlEscape="false" class="form-control" dict="project_quality_monitor_isreach"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>监测值:</label>
            </td>
            <td class="width-35">
                <form:input path="value" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>备注信息:</label>
            </td>
            <td class="width-35">
                <form:input path="remarks" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>

        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor"/>
<html:js name="ff"/>
<script>
    $(function () {

        var selector = $("select[name=proId]");

        ff.form.combox.load(selector, {
            url: "project/srprojectinfo/loadall",
            valueField: "id",
            select: $("#proId").val(),
            labelField: "name",
            data: {}
        }, function () {
        });
        var selector = $("select[name=quaId]");

        ff.form.combox.load(selector, {
            url: "project/srprojectqualityindicator/loadall",
            valueField: "id",
            select: $("#quaId").val(),
            labelField: "name",
            data: {}
        }, function () {
        });

    });

    function changeProName() {
        var proName = $('#proId').find('option:selected').text();
        $('#proName').val(proName);
    }

    function changeQuaName() {
        var proName = $('#quaId').find('option:selected').text();
        var json = $('#quaId').find('option:selected').attr(data - options);
        $('#quaName').val(proName);
        $('#quaType').val(json.type);
    }
</script>
</body>
</html>