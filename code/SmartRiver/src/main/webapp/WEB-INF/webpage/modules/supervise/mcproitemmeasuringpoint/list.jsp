<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>测点列表</title>
  <meta name="decorator" content="list"/>
   <style type="text/css">
  th.ui-th-column div{
    white-space:normal !important;
    height:auto !important;
    text-align: center;
    padding:0px;
}
</style>
</head>
<body title="测点列表">
<div class="row">
  <div class="col-md-3" style="height:500px;" id="list_row">
	<div style="height:100%;overflow-y:scroll;">
<div class="list-group" id="monitorItemDiv">
	
</div>
</div>
  </div>
  <div class="col-md-9">

  <grid:grid id="mcProItemMeasuringPointGridId" url="${adminPath}/supervise/mcproitemmeasuringpoint/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
 
    <grid:column label=""  name="monitorItemId"  hidden="true" query="true"  queryMode="input"  condition="eq" /> 
    <grid:column label="编号"  name="measuringPointCode"  query="true"  queryMode="input"  condition="like"   />
     
 
    <grid:column label="采集仪编号"  name="deviceName" />
    <grid:column label="采集仪型号"  name="deviceModelName" />
    <grid:column label="预警值(mm)"  name="warningValue" />
    <grid:column label="报警值(mm)"  name="alarmValue" />
    <grid:column label="控制值(mm)"  name="controlValue" />
    <grid:column label="速率报警值(mm/d)"  name="rateAlarmValue" />
    <grid:column label="初始累计值(mm)"  name="initialCumulativeValue" />
    
    <grid:column label="支撑类型"  name="supportType" dict="MP_support_type" />
    <grid:column label="断面起点"  name="sectionStartName" />
    <grid:column label="断面终点"  name="sectionEndName" />
    <grid:column label="断面方向"  name="directionSelection" dict="directionSelection" />
    
    <grid:column label="传感器类型"  name="sensorTypeName" />
    <grid:column label="测控深度"  name="pointHoleDepth" />
    <grid:column label="传感器深度"  name="sensor1Depth" />
    
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	
 	<grid:toolbar function="create"  url="${adminPath}/supervise/mcproitemmeasuringpoint/create?projectId=${projectId}"/>
 	<grid:toolbar title="复制" icon="fa-database"  function="updateDialog" url="${adminPath}/supervise/mcproitemmeasuringpoint/{id}/copy?projectId=${projectId}"  />
	
	<grid:toolbar function="update"  url="${adminPath}/supervise/mcproitemmeasuringpoint/{id}/update?projectId=${projectId}"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
  </div>
  
</div>


   <html:js name="ff" />
<script type="text/javascript">

 function searchByMonitorItem(id) {
	    $("#monitorItemDiv a").each(function(){ 
	        $(this).attr("class","list-group-item");  	        
	        if($(this).attr("id")==id){
	        	$(this).attr("class","list-group-item active");  
	        }
	         
	    }); 
	    
	    var item = ff.com.getOptions(id);
	    loadUnit(item);
		$("input[name=monitorItemId]").val(item.id);
		ff.cache.set("monitorItem",item);
		loadColumn(item.groupTypeCode)
		search('mcProItemMeasuringPointGridIdGrid');
	}
 var monitorItemDiv = $("#monitorItemDiv");
 var newHtml= '';
 
 function loadColumn(type)
 {
	 var len=$("#mcProItemMeasuringPointGridIdGrid").getGridParam("width");
	 debugger;
	 var column = [];
 
	 column['T002'] =['sectionStartName','sectionEndName','directionSelection']; 
	 column['T005'] =['sensor1Depth','pointHoleDepth']; 
	 column['T007'] =['sensor1Depth','pointHoleDepth']; 
	 column['T008'] =['supportType','sensorTypeName']; 
	 column['T010'] =['sensorTypeName']; 
	 column['T011'] =['sensorTypeName'];
	 
	 var hide = [];
	 hide['T003'] = ['initialCumulativeValue'];
	 for(var e in column)
	 {
		 for(var key in column[e])
		 {
			 $("#mcProItemMeasuringPointGridIdGrid").setGridParam().hideCol(column[e][key]);
 		 }	 
	 }	 
	 for(var key in hide[type])
	 {
		 $("#mcProItemMeasuringPointGridIdGrid").setGridParam().hideCol(hide[type][key]);
  	 }
	 for(var key in column[type])
	 {
		 $("#mcProItemMeasuringPointGridIdGrid").setGridParam().showCol(column[type][key]);
  	 }
	 $("#mcProItemMeasuringPointGridIdGrid").setGridWidth(len);
 }
 
 
 function loadUnit(item)
 {
 	 
 	var head =[];
 	head['warningValue']="预警值<br/>({ff_unit})";
 	head['alarmValue']="报警值<br/>({ff_unit})";
 	head['controlValue']="控制值<br/>({ff_unit})";
 	head['rateAlarmValue']="速率报警值<br/>({ff_unit}/d)";

 	head['initialCumulativeValue']="初始累计值<br/>({ff_unit})";
 	
 	for(var key in head)
 	{
 		var val = ff.service.unit.get(item.groupTypeCode,head[key]);
 		$("#jqgh_mcProItemMeasuringPointGridIdGrid_"+key).html(val);
 		 
 	}
 	
 	 
 }
 var winHeight;
 var firstVal = '';
 $(document).ready(function(){
		if(window.innerHeight){
			winHeight=window.innerHeight;
			}
		else if((document.body)&&(document.body.clientHeight)){
			winHeight=document.body.clientHeight;
			}
		var newH = winHeight-290;
		var newHL = winHeight-130;
		$("#list_row").css("cssText","height: "+newHL+"px!important;");
		$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
	 $("input[name=monitorItemId]").parent().hide();
	 debugger;
 	ff.util.submit({
 		url:"supervise/mcproitemmonitoritem/loadall",
		filter:{
			  projectId: '${projectId}'
		  },
		success:function(rsp){
			
			monitorItemDiv.html("<div></div>");
		 
			if(null != rsp && rsp.obj)
			{				
				$.each(rsp.obj, function(index, element) {
					newHtml +="<a href=\"javascript:void(0)\" "; 
					newHtml +="id='" + element.id+ "'";
					newHtml +="data-options='" + ff.util.objToJson(element) + "'";
					newHtml +="onclick=\"searchByMonitorItem('";
					newHtml += element.id;
					newHtml +="')\" class=\"list-group-item\"> <h4 class=\"list-group-item-heading\">";
					newHtml += element.monitorItemName;
					newHtml +="</h4></a>";
					if(index == 0){
						firstVal = element.id;
					}
			    });
				monitorItemDiv.html(newHtml);
				if(firstVal != ''){
					setTimeout(function(){searchByMonitorItem(firstVal)}, 100);
				}
				
			}
		}
	
	});
 	
 
  


});
</script>
</body>
</html>