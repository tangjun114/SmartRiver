<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目中的其他设备</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemOtherDeviceForm">
    <form:form id="mcProItemOtherDeviceForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
	 
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					
					<td  class="width-15 active text-right">	
		              <label>设备编码:</label>
		            </td>
		            
		           <td class="width-35">
						<form:gridselect gridId="mcMonitorOtherDeviceGridId" title="设备列表" nested="false"
						   path="otherDeviceId" gridUrl="${adminPath}/monitor/mcmonitorotherdevice" value="${data.otherDeviceId}"
						   labelName="otherDeviceCode" labelValue="${data.otherDeviceCode}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="otherDeviceCode"
						   callback="mcMonitorOtherDevicePicker" 
						   htmlEscape="false" class="form-control" readonly="true" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
				 
					<td  class="width-15 active text-right">	
		              <label>设备名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="otherDeviceName" readonly="true" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
				 
					<td  class="width-15 active text-right">	
		              <label>设备类型:</label>
		            </td>
					<td class="width-35">
						<form:input path="otherDeviceTypeName" readonly="true"  htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>设备型号:</label>
		            </td>
					<td class="width-35">
						<form:input path="otherDeviceUnitTypeName" readonly="true"  htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					
					<td  class="width-15 active text-right">	
		              <label>设备生产厂家:</label>
		            </td>
					<td class="width-35">
						<form:input path="otherDeviceManufacturer" readonly="true"  htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>使用状态:</label>
		            </td>
					<td class="width-35">
						<form:select  path="otherDeviceStatus" htmlEscape="false"  readonly="true"  class="form-control"   dict="SBXX_SBSFSY"   />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					
					<td class="width-15 active text-right"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
<script >

	function mcMonitorOtherDevicePicker(rowDatas,fildId,fildName){		
		//$("#"+fildId).val(rowDatas[0].id);
		//$("#"+fildName).val(rowDatas[0].username);
	 
	   $("#otherDeviceId").val(rowDatas[0].id);
		$("#otherDeviceCode").val(rowDatas[0].code);
		 
		$("#otherDeviceName").val(rowDatas[0].name);
		 
 
		$("#otherDeviceTypeName").val(rowDatas[0].typeName);
		$("#otherDeviceUnitTypeName").val(rowDatas[0].unitTypeName);
		$("#otherDeviceManufacturer").val(rowDatas[0].manufacturer);
		$("#otherDeviceStatus").val(rowDatas[0].status);
 
	}
</script>
</body>
</html>