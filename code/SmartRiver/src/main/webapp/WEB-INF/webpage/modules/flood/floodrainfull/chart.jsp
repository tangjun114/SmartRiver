<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>监测结果统计</title>
    <meta name="decorator" content="single"/>
</head>

<body class="white-bg" >

<div class="animated fadeInRight">
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="myTab">
            <li role="presentation" class="active">
                <a href="#rainfull" aria-controls="rainfull" role="tab" data-toggle="tab">雨量</a>
            </li>
            <li role="presentation" >
                <a href="#waterlevel" aria-controls="waterlevel" role="tab" data-toggle="tab">水位</a></li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content" style="overflow-y:hidden;">
            <div role="tabpanel" class="tab-pane  in active" id="rainfull">
                <iframe src="${adminPath}/flood/floodrainfull/chart/rainfull" width="100%" height="100%" frameborder="0" scrolling="no" id="rainfullIfream"></iframe>
            </div>
            <div role="tabpanel" class="tab-pane " id="waterlevel">
                <iframe src="${adminPath}/flood/floodrainfull/chart/waterlevel" width="100%" height="100%" frameborder="0" scrolling="no" id="waterlevelIframe"></iframe>
            </div>
        </div>

    </div>

</div>

<html:js name="func" />
<html:js name="ff,layer,bootstrap,bootstrap-table" />
<script>
    var winHeight;

    $(function () {
        $('#myTab a:first').tab('show');
    })
    $(document).ready(function () {
        var winHeight = 0;
        if (window.innerHeight) {
            winHeight = window.innerHeight;
        } else if ((document.body) && (document.body.clientHeight)) {
            winHeight = document.body.clientHeight;
        }
        var newH = winHeight - 50;
        $("#rainfull, #waterlevel").css({
            "height" : newH + "px",
            "overflow-y":"hidden"
        });


    });
</script>
</body>
</html>