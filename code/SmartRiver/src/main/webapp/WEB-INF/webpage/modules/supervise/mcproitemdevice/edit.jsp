<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目与设备管理表</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemDeviceForm">
    <form:form id="mcProItemDeviceForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="deviceId"/>
		<form:hidden path="type"/>
		 <form:hidden path="unitType"/>
		<form:hidden path="id"/>
		
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>设备编码:</label>
		            </td>
					 
		            
		           <td class="width-35">
						<form:gridselect gridId="mcMonitorDeviceGridId" title="设备" nested="false"
						   path="code" gridUrl="${adminPath}/monitor/mcmonitordevice" value="${data.code}"
						   labelName="deviceCode" labelValue="${data.code}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="code"
						   callback="mcMonitorOtherDevicePicker" 
						   htmlEscape="false" class="form-control" readonly="true" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>设备类型名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="typeName" htmlEscape="false" readonly="true"  class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>设备型号名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="unitTypeName" htmlEscape="false" readonly="true"  class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>是否自动化:</label>
		            </td>
					<td class="width-35">
						<form:select path="isAuto" htmlEscape="false" readonly="true" dict="sf"   class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>设备名修正:</label>
		            </td>
					<td class="width-35">
						<form:input path="nameCorrection" htmlEscape="false" readonly="true"  class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>规格修正:</label>
		            </td>
					<td class="width-35">
						<form:input path="specRevision" htmlEscape="false" readonly="true"  class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>校准日期:</label>
		            </td>
					<td class="width-35">
						<form:input path="correctingDate" htmlEscape="false" readonly="true"  class="form-control layer-date" placeholder="YYYY-MM-DD" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"       />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>校准到期日期:</label>
		            </td>
					<td class="width-35">
						<form:input path="correctingEndDate" htmlEscape="false"  readonly="true"  class="form-control layer-date" placeholder="YYYY-MM-DD" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"     />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>使用状态:</label>
		            </td>
					<td class="width-35">
						<form:select path="status" htmlEscape="false" class="form-control"  readonly="true"  dict="SBXX_SBSFSY"      />
						<label class="Validform_checktip"></label>
					</td>
					<td class="width-15 active text-right"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
<script >

	function mcMonitorOtherDevicePicker(rowDatas,fildId,fildName){		
		//$("#"+fildId).val(rowDatas[0].id);
		//$("#"+fildName).val(rowDatas[0].username);
	 
	   $("#deviceId").val(rowDatas[0].id);
		$("#code").val(rowDatas[0].code);
		
		
		$("#deviceCode").val(rowDatas[0].code);

		
		$("#isAuto").val(rowDatas[0].isAuto);

		$("#nameCorrection").val(rowDatas[0].nameCorrection);
		$("#specRevision").val(rowDatas[0].specRevision);

		
	 
		$("#correctingDate").val(new Date(rowDatas[0].correctingDate).format("yyyy-MM-dd"))
		$("#correctingEndDate").val(new Date(rowDatas[0].correctingEndDate).format("yyyy-MM-dd"))

		//$("#correctingDate").val(rowDatas[0].correctingDate);
		//$("#correctingEndDate").val(rowDatas[0].correctingEndDate);
		
		$("#type").val(rowDatas[0].type);
		$("#typeName").val(rowDatas[0].typeName);
		$("#unitType").val(rowDatas[0].unitType);
		$("#unitTypeName").val(rowDatas[0].unitTypeName);
 		$("#status").val(rowDatas[0].status); 
	}
</script>
</body>
</html>