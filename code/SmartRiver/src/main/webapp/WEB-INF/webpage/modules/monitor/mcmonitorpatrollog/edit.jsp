<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>巡检日志</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>
<body class="white-bg"  formid="mcMonitorPatrolLogForm">
    <form:form id="mcMonitorPatrolLogForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="name" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				 
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>巡检日期:</label>
		            </td>
					<td class="width-35">
						<form:input path="patrolDate" htmlEscape="false" class="form-control layer-date" placeholder="YYYY-MM-DD hh:mm:ss" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"       />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>巡检人:</label>
		            </td>
					<td class="width-35">
						<form:input path="rummagerName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>地址:</label>
		            </td>
					<td class="width-35">
						<form:input path="address" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>状态:</label>
		            </td>
					<td class="width-35">
						<form:select path="status" htmlEscape="false" class="form-control"  dict="mc_monitor_patrol_status"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>说明:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="desc" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					</tr>
					<tr>
					<td  colspan="4" class="width-15 active text-left">	
		              <label>图片:</label>
		            </td>
		            </tr>
		            <tr>
					<td colspan="4" class="width-35">
						<form:fileinput showType="file" extend="jpg,png,gif" maxFileCount="10" fileInputWidth="100px"  fileInputHeight="100px"  path="imgs" htmlEscape="false" class="form-control" />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		      	<tr>
					<td  class="width-15 active text-right">	
		              <label>总体情况:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="overallSituation" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>结论:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="summary" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		   </tbody>
		</table>
		
	</form:form>
 	<div >
	<ul id="dowebok">
	
	</ul>
	</div> 
<script src="${staticPath}/modules/mcmonitorpatrollog/edit.js"></script>
<html:js name="bootstrap-fileinput" />
<script src="${staticPath}/viewer/js/viewer.min.js"></script>
<link href="${staticPath}/viewer/css/viewer.min.css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">

</script>
</body>
</html>