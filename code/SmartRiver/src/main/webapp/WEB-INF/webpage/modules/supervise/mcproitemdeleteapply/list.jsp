<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>原始文件删除申请列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="原始文件删除申请">
<grid:grid id="mcProItemDeleteApplyGridId" url="${adminPath}/supervise/mcproitemdeleteapply/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.path" hidden="true"  name="filePath" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="fileName" width="100"/>
	<grid:column label="" hidden="true"  name="applyFile" width="100"/>
	
    <grid:column label="申请机构"  name="monitorName" query="true"  queryMode="input"  condition="eq" />
    <grid:column label="申请者"  name="applyUserName" />
    <grid:column label="项目名称"  name="projectName" />
    <grid:column label="监测项名称"  name="monitorItemName" />
    <grid:column label="测点"  name="point" />
    <grid:column label="监测次数"  name="monitorCount" />
    <grid:column label="测点数量"  name="pointNum" />
    <grid:column label="原始文件"  name="fileName" formatter="button" width="150"/>
    <grid:column label="申请理由"  name="reason" width="150"/>
    <grid:column label="申请文件"  name="applyFile" formatter="button"/>
    <grid:column label="申请时间"  name="createDate" />
    <grid:column label="状态"  name="status" dict="applyStatus" queryMode="select"  condition="eq" query="true"/>
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
    <grid:button title="审批"  tipMsg="是否批准删除文件?" groupname="opt" function="rowConfirm" outclass="btn-success" innerclass="" url="${adminPath}/supervise/mcproitemdeleteapply/approve?gid=\"+row.id+\"" />
    <grid:column label="操作者"  name="handleUserName" />
    <grid:column label="批准时间"  name="handleTime" />

	<grid:toolbar function="update" title="审批"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="func" />
<script type="text/javascript">
function mcProItemDeleteApplyGridIdFileNameFormatter(value, options, row){
	  try{
	         if(!row.id) {
             return '';
         }
	         var href=linkFormatter(row.fileName,row.filePath); }catch(err){}
	   return href;
}

function mcProItemDeleteApplyGridIdApplyFileFormatter(value, options, row){
	  try{
	         if(!row.id || row.applyFile=='' || row.applyFile==null) {
           return '';
       }
	         var href=linkFormatter('查看',row.applyFile); }catch(err){}
	   return href;
}
</script>
</body>
</html>