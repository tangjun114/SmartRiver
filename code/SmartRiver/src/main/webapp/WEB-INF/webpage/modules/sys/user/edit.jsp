<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="sys.user.updateuser" /></title>
    <meta name="decorator" content="form"/> 
    <html:component name="bootstrap-fileinput" />
</head>
<body class="white-bg"  formid="userForm">
<form:form id="userForm" modelAttribute="data" method="post" class="form-horizontal">
  <form:hidden path="id" />
  <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
    <tbody>
      <tr>
        <td class="width-15 active text-right">
          <label>用户名:</label></td>
        <td class="width-35">${data.username}</td>
        <td class="width-15 active text-right">
          <label>
            <font color="red">*</font>真实姓名:</label></td>
        <td class="width-35">
          <form:input path="realname" class="form-control " datatype="*" nullmsg="请输入姓名！" htmlEscape="false" />
          <label class="Validform_checktip"></label>
        </td>
      </tr>
      <tr>
        <td class="width-15 active text-right">
          <label>
            邮箱:</label></td>
        <td class="width-35">
          <form:input path="email" class="form-control" datatype="e"  htmlEscape="false" />
          <label class="Validform_checktip"></label>
        </td>
        <td class="width-15 active text-right">
          <label>
                                 联系电话:</label></td>
        <td class="width-35">
          <form:input path="phone" class="form-control" htmlEscape="false" ignore="ignore" datatype="m" nullmsg="请输入用户名！" />
          <label class="Validform_checktip"></label>
        </td>
      </tr>
      
      		       <tr>
		         <td  class="width-15 active text-right">	
		              <label>部门:</label>
		         </td>
		         <td class="width-35" >
		             <form:input path="deptName"  class="form-control"   validErrorMsg="部门" htmlEscape="false" />
		             <label class="Validform_checktip"></label>
		         </td>
		         <td  class="width-15 active text-right">	<label> 职位:</label></td>
		         <td  class="width-35" >
		             <form:input path="positionName" class="form-control"   htmlEscape="false" />
		             <label class="Validform_checktip"></label>
		         </td>
		      </tr>
		      
		      
		      <tr>
		         <td  class="width-15 active text-right">	
		              <label>qq/微信:</label>
		         </td>
		         <td class="width-35" >
		             <form:input path="qqWx"  class="form-control" />
		             <!-- <label class="Validform_checktip"></label> -->
		         </td>
		         <td  class="width-15 active text-right">	<label>gender:</label></td>
		         <td  class="width-35" >
		         <form:radiobuttons path="gender"   dict="sex" defaultvalue="1"  htmlEscape="false" cssClass="i-checks required" />
		         </td>
		      </tr>
      
      <tr>
        <td class="active">
          <label class="pull-right">
            <font color="red">*</font>用户角色:</label></td>
        <td colspan="3">
          <form:checkboxes path="roleIdList" nested="false" items="${allRoles}" defaultvalue="${roleIdList}" itemLabel="name" itemValue="id" htmlEscape="false" cssClass="i-checks required" /></td>
      </tr>
      <tr>
		      <td  class="width-15 active text-right">	
				     <label>人员类型:</label>
			  </td>
			 <td class="width-35">
				   <form:select path="type" htmlEscape="false" class="form-control"  dict="projectMemberType"      />
				   <label class="Validform_checktip"></label>
			  </td>
			  <td  class="width-15 active text-right">	
				     <label>人员类别:</label>
			  </td>
			 <td class="width-35">
				   <form:select path="subType" htmlEscape="false" class="form-control"  dict="member_sub_type"      />
				   <label class="Validform_checktip"></label>
			  </td>
	 </tr>
      <tr>
        <td class="width-15 active">
          <label class="pull-right">组织机构:</label></td>
        <td colspan="3">
          <form:treeselect title="请选择组织机构" path="organizationIds" nested="false" dataUrl="${adminPath}/sys/organization/treeData"  labelName="parentname" labelValue="${organizationNames}"  /></td>
      </tr>
    </tbody>
  </table>
</form:form>
</body>
</html>