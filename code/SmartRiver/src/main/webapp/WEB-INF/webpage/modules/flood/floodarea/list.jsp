<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>监测区域列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="监测区域">
<grid:grid id="floodAreaGridId" url="${adminPath}/flood/floodarea/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="名字" name="name" query="true"  queryMode="input"  condition="like" />
    <grid:column label="备注信息" name="remarks"/>
    <%--<grid:column label="创建者" name="createBy"/>--%>
    <grid:column label="创建时间" name="createDate"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>