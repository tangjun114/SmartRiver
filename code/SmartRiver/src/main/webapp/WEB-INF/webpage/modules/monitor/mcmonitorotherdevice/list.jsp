<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>其他设备列表</title>
  <meta name="decorator" content="list"/>
    <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
</head>
<body title="其他设备">
 
<grid:grid id="mcMonitorOtherDeviceGridId" url="${adminPath}/monitor/mcmonitorotherdevice/ajaxList" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
     <grid:column label="类型"  name="type" hidden="true"   />
    <grid:column label="型号"  name="unitType" hidden="true"   />
    <grid:column label="设备编码"  name="code" />
    <grid:column label="设备名称"  name="name" />
    <grid:column label="设备类型"  name="typeName" />
    <grid:column label="设备型号"  name="unitTypeName" />
    <grid:column label="生产厂家"  name="manufacturer" />
    <grid:column label="使用状态"  name="status"  dict="deviceStatus"/>
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
    <grid:toolbar title="复制" icon="fa-database"  function="updateDialog" url="${adminPath}/supervise/mcmonitorotherdevice/{id}/copy"  />
	
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<script type="text/javascript">
var winHeight;
$(document).ready(function()   
{  
	//获取窗口高度 兼容各个浏览器
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}

	var newH = winHeight-210;
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
}); 
</script>
</body>
</html>