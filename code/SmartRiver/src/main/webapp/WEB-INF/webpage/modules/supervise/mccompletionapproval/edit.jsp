<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>完工审批</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcCompletionApprovalForm">
    <form:form id="mcCompletionApprovalForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="certificateName"/>
		
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="name" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>申请状态:</label>
		            </td>
					<td class="width-35">
						<form:select path="status" htmlEscape="false" class="form-control"  dict="SHENPIZHUANGTAI"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
		   			<td  class="width-15 active text-right">	
		              <label>完工证明:</label>
		            </td>
					<td class="width-35">
						<form:fileinput path="certificate" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
		   			
		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
<script type="text/javascript">

/**
*提交回调方法
*/
function beforeSubmit(curform){
	 //这里最好还是使用JSON提交，控制器改变提交方法，并使用JSON方式来解析数
	 //通过判断，如果有问题不提交
	 var fileName = $("div[class='file-footer-caption'").attr("title");
	 $("input[name='certificateName']").val(fileName);
	 return true;
}
</script>
</body>
</html>