<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>测孔列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="测孔列表">
<grid:grid id="mcProItemMeasuringHoleGridId" url="${adminPath}/supervise/mcproitemmeasuringhole/ajaxList?projectId=${projectId}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="调试模式"  name="debug"  query="true"  queryMode="select"  condition="eq"  dict="sf"/>
    <grid:column label="继承名"  name="inheritName" />
    <grid:column label="监测点类型"  name="measuringPointTypeName" />
    <grid:column label="监测仪器类型"  name="instrumentTypeVal" />
    <grid:column label="监测仪器型号"  name="instrumentModelName" />
    <grid:column label="初始值次数"  name="InitialValueTimes"  dict="InitialValueTimes"/>
    <grid:column label="报警值设置类型"  name="alarmValueType"  dict="alarmValueType"/>
    <grid:column label="预警值(mm)"  name="warningValue" />
    <grid:column label="报警值(mm)"  name="alarmValue" />
    <grid:column label="控制值(mm)"  name="controlValue" />
    <grid:column label="速率报警值(mm/d)"  name="rateAlarmValue" />
    <grid:column label="报警组选择"  name="alarmGroupName" />
	<grid:toolbar function="create"  url="${adminPath}/supervise/mcproitemmeasuringhole/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<script type="text/javascript">  
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-270;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});
</script>
</body>
</html>