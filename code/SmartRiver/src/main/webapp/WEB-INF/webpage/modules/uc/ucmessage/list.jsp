<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>个人中心消息列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="个人中心消息">
<grid:grid id="ucMessageGridId" url="${adminPath}/uc/ucmessage/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="信息"  name="message" />
    <grid:column label="消息状态"  name="status"  query="true"  queryMode="select"  condition="eq"  dict="ucMsgStatus"/>
    <grid:column label="时间"  name="createDate" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>