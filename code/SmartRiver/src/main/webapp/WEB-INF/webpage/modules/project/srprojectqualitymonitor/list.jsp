<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>工程质量检测列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="工程质量检测">
<grid:grid id="srProjectQualityMonitorGridId" url="${adminPath}/project/srprojectqualitymonitor/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="项目名称" name="proName" query="true" queryMode="input" condition="eq"/>
    <grid:column label="指标name" name="quaName"/>
    <grid:column label="指标类型" name="quaType"/>
    <grid:column label="监测值" name="value"/>
    <grid:column label="是否达标" name="isReach" query="true" queryMode="select" dict="project_quality_monitor_isreach"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:column label="备注信息" name="remarks"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>