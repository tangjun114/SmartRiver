<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>最新警报信息</title>
    <meta name="decorator" content="single"/>
    <style type="text/css">
		.pro-item-1 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: #e0e2e5
		}
		.pro-item-2 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: transparent
		}
</style>
</head>

<body class="gray-bg" >

    <div class="animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet box  portlet-blue">
                	<div class="portlet-header">
						<div class="caption">最新警报信息</div>

					</div>
					<div class="portlet-body" id="project_content">
					        <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>分区</strong></div>
                                <div class="col-sm-9" >
                                	${detail.subareaName}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>监测项</strong></div>
                                <div class="col-sm-9" >
                                ${detail.monitorTypeName}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label" ><strong>点号</strong></div>
                                <div class="col-sm-9" >
                                	${detail.measuringPointCode}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label"><strong>当前值</strong></div>
                                <div class="col-sm-9">
                                    ${detail.nowValue}
                                </div>
                            </div>
                            				        
					</div>
            </div>
         </div> 
    </div>
</div>
<html:js name="func" />
<html:js name="ff,layer" />
<script>
	$(document).ready(function () {
		var newH = $(document).height()-58;
	
		$("#project_content").css({
			"height":newH+"px",
			"overflow-y":"scroll"});
	});


	
</script>
</body>
</html>