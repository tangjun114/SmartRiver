<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>项目设计方案列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="项目设计方案">
<grid:grid id="srProjectDesignGridId" url="${adminPath}/project/srprojectdesign/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.path" hidden="true" name="path" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="文档名" name="name" query="true" queryMode="input" condition="eq"/>
    <grid:column label="项目名称" name="proName" query="true" queryMode="input" condition="eq"/>
    <grid:column label="文档类别" name="type" dict="project_design_type" query="true" queryMode="select"/>
    <grid:column label="文件地址" name="path" formatter="button"/>
    <grid:column label="备注信息" name="remarks"/>
    <grid:column label="创建者" name="createByName"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
<html:js name="ff"/>
<script type="text/javascript">

    function srProjectDesignGridIdPathFormatter(value, options, row) {
        var href = '';
        try {
            if (!row.id) {
                return '';
            }
            if (row.path != '') {
                debugger;
                var strs = new Array();
                strs = row.path.split(',');

                for (i = 0; i < strs.length; i++) {
                    href += "<a href=\"";
                    href += ff.util.rootUrl() + "/" + strs[i];
                    href += "\" class=\"fancybox \"";
                    href += " data-fancybox-group=\"gallery" + row.id + "\" title=\"\">文档" + (i + 1) + "</a>&nbsp&nbsp";
                }

                console.log(href);
            }

        } catch (err) {

        }
        return href;
    }
</script>
</body>
</html>