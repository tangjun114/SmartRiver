<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>警报管理</title>
  <meta name="decorator" content="list"/>
    <style type="text/css">
		.color_warn {
			color: #FCB247;
		}
		.color_caution {
			color: red;
		}
		.color_control {
			color: #993366;
		}	
		.gly_green{font-size:20px;color:green;text-shadow:#CCC 2px 2px 2px;}
		.gly_yellow{font-size:20px;color:orange;text-shadow:#CCC 2px 2px 2px;}
		.gly_red{font-size:20px;color:red;text-shadow:#CCC 2px 2px 2px;}
		.gly_navy{font-size:20px;color:#993366;text-shadow:#CCC 2px 2px 2px;}		
		  th.ui-th-column div{
    white-space:normal !important;
    height:auto !important;
    text-align: center;
    padding:0px;
}
	</style>
</head>
<body title="警报管理">
	<div class="row">
		<div class="col-md-3" style="height: 500px;" id="list_row">
			<div style="height: 100%; overflow-y: scroll;">
				<div class="list-group" id="monitorItemDiv"></div>
			</div>
		</div>
		<div class="col-md-9">
		<grid:grid id="mcProItemAlarmGridId" url="${adminPath}/monitor/mcproitemalarm/ajaxList?projectId=${projectId}" multiselect="false">
		<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	    <grid:column label="sys.common.path" hidden="true"  name="alarmTypeId" width="100"/>
	    <grid:column label="sys.common.name" hidden="true"  name="alarmTypeName" width="100"/>
		
		<grid:column label="状态"  name="alarmTypeId" formatter="button" />
		<grid:column label="报警测点"  name="measuringPointCode" query="true"  queryMode="input"  condition="like"/>
		<grid:column label="报警时间"  name="createDate" query="true"  queryMode="date"  condition="between" />
		<grid:column label="累计变化值"  name="sumValue" />
		<grid:column label="速率值"  name="changeRate" />
		<grid:column label="预警值"  name="warnValue" />
		<grid:column label="报警值"  name="alarmValue" />
		
		<grid:column label="控制值"  name="controlValue" />
		<grid:column label="速率报警值"  name="rateAlarmValue" />
 		<grid:column label="报警说明"  name="alarmDesp" />
		<grid:column label="处理状态"  hidden="true" name="handleStatus" dict="alarm_handle_status"/>
		<grid:column label="处理时间"  hidden="true" name="updateDate" />
		<grid:column label="处理措施"  hidden="true" name="handleResult" />
		<grid:column label=""  name="monitorItemId"  hidden="true" query="true"  queryMode="input"  condition="eq" /> 

		<grid:button groupname="opt" function="delete" />
		<grid:column label="sys.common.opt"  name="opt" formatter="button" width="50"/>	
<%-- 		<grid:toolbar function="create"  url="${adminPath}/monitor/mcproitemalarm/create?projectId=${projectId}"/> --%>
		<grid:toolbar title="消警" function="update" url="${adminPath}/monitor/mcproitemalarm/{id}/update?clear=monitorItem"/>	
		<grid:toolbar function="search"/>
		<grid:toolbar function="reset"/>
		</grid:grid>
		</div>
	</div>
<html:js name="ff" />	
<html:js name="func" />
<script type="text/javascript">  
var monitorItemDiv = $("#monitorItemDiv");
var newHtml= '';
function searchByMonitorItem(id) {
    $("#monitorItemDiv a").each(function(){ 
        $(this).attr("class","list-group-item");  	        
        if($(this).attr("id")==id){
        	$(this).attr("class","list-group-item active");  
        }
         
    }); 
    
    var item = ff.com.getOptions(id);
	$("input[name=monitorItemId]").val(item.id);
	ff.cache.set("monitorItem",item);
	loadUnit(item);
	search('mcProItemAlarmGridIdGrid');
}

function loadUnit(item)
{
  	var head =[];
	
	head['rateAlarmValue']="速率报警值<br/>({ff_unit}/d)";
	head['controlValue']="控制值<br/>({ff_unit})";
	head['alarmValue']="报警值<br/>({ff_unit})";
	head['warnValue']="预警值<br/>({ff_unit})";
	head['changeRate']="速率值<br/>({ff_unit}/d)";
	head['sumValue']="累计变化<br/>({ff_unit})";
	
	for(var key in head)
	{
		var val = ff.service.unit.get(item.groupTypeCode,head[key]);
		$("#jqgh_mcProItemAlarmGridIdGrid_"+key).html(val);
 
	}
}

function mcProItemAlarmGridIdAlarmTypeIdFormatter(value, options, row){
	 return ff.service.alarm(row.alarmTypeId);
}
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-270;
	var newHL = winHeight - 220;
	$("input[name=monitorItemId]").parent().hide();

	$("#list_row").css("cssText","height: "+newHL+"px!important;");
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
	 ff.util.submit({
	 		url:"supervise/mcproitemmonitoritem/loadall",
			filter:{
				  projectId: '${projectId}'
			  },
			success:function(rsp){
				if(null != rsp && rsp.obj)
				{	
					var firstVal ='';
					$.each(rsp.obj, function(index, element) {
						newHtml +="<a href=\"javascript:void(0)\" "; 
						newHtml +="id='" + element.id+ "'";
						newHtml +="data-options='" + ff.util.objToJson(element) + "'";
						newHtml +="onclick=\"searchByMonitorItem('";
						newHtml += element.id;
						newHtml +="')\" class=\"list-group-item\"> <h4 class=\"list-group-item-heading\">";
						newHtml += element.monitorItemName;
						newHtml +="</h4></a>";
						if(index == 0){
							firstVal = element.id;
						}
				    });

					monitorItemDiv.html(newHtml);
					if(firstVal != ''){
						setTimeout(function(){searchByMonitorItem(firstVal)}, 500);
					}
				}
			}
		
		});
});
</script>
</body>
</html>