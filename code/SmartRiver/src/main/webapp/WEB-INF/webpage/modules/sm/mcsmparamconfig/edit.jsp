<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>系统管理-参数设置</title>
    <meta name="decorator" content="form"/> 
    
</head>

<body class="white-bg"  formid="mcSmParamConfigForm">
    <form:form id="mcSmParamConfigForm"  modelAttribute="data"  method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
			  <tr>
		         <td class="width-15 active"><label class="pull-right">上级系统管理-参数设置:</label></td>
		         <td class="width-35">
		            <form:treeselect title="请选择上级系统管理-参数设置" path="parentId" dataUrl="${adminPath}/sm/mcsmparamconfig/treeData" labelName="parentname" labelValue="${data.parent.name}" />	   
				  </td>
		          <td  class="width-15 active text-right">	
		              <label><font color="red">*</font>系统管理-参数设置名称:</label>
		         </td>
		         <td class="width-35" >
		             <form:input path="name" class="form-control " datatype="*" nullmsg="请输入系统管理-参数设置名称！" htmlEscape="false" />
		             <label class="Validform_checktip"></label>
		         </td>
		      </tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>参数值:</label>
		            </td>
					<td class="width-35">
						<form:input path="paraValue" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>映射标签:</label>
		            </td>
					<td class="width-35">
						<form:input path="fieldName" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>映射字段:</label>
		            </td>
					<td class="width-35">
						<form:input path="fieldMapper" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>系统初始化数据:</label>
		            </td>
					<td class="width-35">
						<form:select path="sysInit" htmlEscape="false" class="form-control"  dict="sf"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>备注信息:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="remarks" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td class="width-15 active text-right"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
</body>
</html>