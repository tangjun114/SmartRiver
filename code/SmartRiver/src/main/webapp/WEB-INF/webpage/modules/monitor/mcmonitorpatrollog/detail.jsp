<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>最新巡检结果</title>
    <meta name="decorator" content="single"/>
    <style type="text/css">
		.pro-item-1 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: #e0e2e5
		}
		.pro-item-2 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: transparent
		}
</style>
</head>

<body class="gray-bg" >

    <div class="animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet box  portlet-blue">
					 <div class="portlet-header">
						<div class="caption">最新巡检结果</div>

					</div>	
					<div class="portlet-body" id="project_content">
					        <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>巡检名称</strong></div>
                                <div class="col-sm-9" >
                                	${detail.name}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>状态</strong></div>
                                <div class="col-sm-9" >
                                ${detail.status}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label" ><strong>巡检人员</strong></div>
                                <div class="col-sm-9" >
                                	${detail.rummagerName}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label"><strong>地址信息</strong></div>
                                <div class="col-sm-9">
                                    ${detail.address}
                                </div>
                            </div>
					        <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>说明</strong></div>
                                <div class="col-sm-9" >
                                	${detail.desc}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" >&nbsp;&nbsp;</div>
                                <div class="col-sm-9" >
                                <%-- <img src="${detail.imgs}" class="img-thumbnail"> --%>
                                </div>
                            </div>
                            				        
					</div>
            </div>
         </div> 
    </div>
</div>
<html:js name="func" />
<html:js name="ff,layer" />
<script>
	$(document).ready(function () {
		var newH = $(document).height()-58;
	
		$("#project_content").css({
			"height":newH+"px",
			"overflow-y":"scroll"});
	});

	function showMap(){
		var lat = '${detail.latitude}';
		var lng = '${detail.longitude}';
		var url = "${staticPath}/map/bdmap.html?lat="+lat+"&lng="+lng;
		layer.open({
			  type: 2,
			  title: false,
			  area: ['630px', '360px'],
			  closeBtn: false,
			  shadeClose: true,
			  btn: ['关闭'],
			  content: url,
			  success: function(layero){

				  }
			}); 
	}

	
</script>
</body>
</html>