<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>${param.projectName}</title>
  <meta name="decorator" content="list"/>
  <html:component name="bootstrap-treeview"/>
</head>
<body title="${param.projectName}" style="margin-top:-20px">
<div class="row" >
	<div class="col-sm-3 col-md-2" >
	   <view:treeview id="monitorItemTreeview" dataUrl="${adminPath}/sys/menu/menuData?nodeid=7ee85f1d4de141639b8b00b1e9ca2780" onNodeChecked="monitorItemOnclick" onNodeSelected="monitorItemOnclick" treeviewSettingCallback="initMenu"></view:treeview>
	    <script type="text/javascript">
	    var projectId = '${projectId}';
	       function monitorItemOnclick(event, node) {
	    	   if(node.href!=''&& node.href!='#'){
	    		   $("iframe").attr("src","${adminPath}/"+node.href+"?projectId="+projectId);
	    	   }
	    	   else
	    	   { 
	    		   if(node.state.expanded){   
	    		        //处于展开状态则折叠  
	    		        $("#monitorItemTreeview").treeview('collapseNode', node.nodeId);    
	    		    } else {  
	    		        //展开  
	    		        $("#monitorItemTreeview").treeview('expandNode', node.nodeId);  
	    		    }
	    		   $("li[data-nodeid='"+node.nodeId+"']").click();
	    		  // node.state.expanded = !node.state.expanded;
	    	   }
	    	 	   
	    	   
	    	  
	    	   
	       }
	   </script>
	</div>
	<div  class="col-sm-9 col-md-10">
	<iframe width="100%" height="900" src="${adminPath}/supervise/mcmonitorresultstatistics/content?projectId=${projectId}"  frameborder="0"> </iframe>
	</div>

</div>
<script type="text/javascript">   
var projectId = '${projectId}';
var winHeight;
$(document).ready(function()   
{  
	//获取窗口高度 兼容各个浏览器
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}

	var newH = winHeight-100;
	$("iframe").attr("height",newH);
	
	$("#monitorItemTreeview").css({
		"height":newH+"px",
		"overflow-y":"scroll"});	
});   
function initMenu(){
	 $("#monitorItemTreeview").treeview('collapseAll');
	 $("#monitorItemTreeview").treeview('expandNode', 0);  

}
</script>  
</body>

</html>