<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目与设备管理表列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目与设备管理表">
<grid:grid id="mcProItemDeviceGridId" url="${adminPath}/supervise/mcproitemdevice/ajaxList?projectId=${projectId}&typeName=${typeName}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
    <grid:column label="设备型号"  name="unitType"  hidden="true"/>
    <grid:column label="设备类型"  name="type"  hidden="true" />
    
    <grid:column label="设备编码"  name="code" />
    <grid:column label="设备类型"  name="typeName"  query="true"  queryMode="select"  condition="eq" dict="0" />
    <grid:column label="设备型号"  name="unitTypeName" />
    <grid:column label="是否自动化"  name="isAuto" dict="sf"/>
    <grid:column label="设备名修正"  name="nameCorrection" />
    <grid:column label="规格修正"  name="specRevision" />
    <grid:column label="校准日期"  name="correctingDate"  formatter="date"/>
    <grid:column label="校准到期日期"  name="correctingEndDate"  formatter="date"  />
    <grid:column label="使用状态"  name="status"  query="true"  queryMode="select"  condition="eq"  dict="SBXX_SBSFSY"/>
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproitemdevice/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="ff" />
<script type="text/javascript">
		$(function() {
			debugger;
			var typeName = ff.util.urlPara("typeName");
			loadDeviceType(typeName);

		});
		function loadDeviceType(select) {
			var selector = $("select[name=typeName]");

			ff.form.combox.load(selector, {
				url : "sm/mcsmparamconfig/devicebytype",
				valueField : "name",
				labelField : "name",
				select : select,
				callback : function() {

				}
			});

		}

		$(document).ready(function() {
			var winHeight;
			if(window.innerHeight){
				winHeight=window.innerHeight;
				}
			else if((document.body)&&(document.body.clientHeight)){
				winHeight=document.body.clientHeight;
				}
			var newH = winHeight-250;
			 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
		});

</script>
</body>
</html>