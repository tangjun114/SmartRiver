<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>监测项列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="监测项列表">
<grid:grid id="mcProItemMonitorItemGridId" url="${adminPath}/supervise/mcproitemmonitoritem/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
    <grid:column label="监测组类别"  name="groupTypeName"  query="true"  queryMode="select"  condition="eq" dict="0"/>

    <grid:column label="监测项名称"  name="monitorItemName"   />
    <grid:column label="是否为自动化"  name="isAuto"  query="true"  queryMode="select"  condition="eq"  dict="sf"/>
    
    <grid:column label="埋设数量"  name="pointCount"   />
    <grid:column label="备注"  name="remarks"   />
    
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	<grid:button title="清除结果数据"  exp="true" tipMsg="是否确定清除结果数据?" groupname="opt" function="rowConfirm" outclass="btn-success" innerclass="" url="${adminPath}/supervise/mcproitemmonitoritem/clear?gid=\"+row.id+\"" />
	
	<grid:toolbar function="create"  url="${adminPath}/supervise/mcproitemmonitoritem/create?projectId=${projectId}"/>
	<grid:toolbar function="update" url="${adminPath}/supervise/mcproitemmonitoritem/{id}/update?projectId=${projectId}"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
 <html:js name="ff" />
<script type="text/javascript">  
var winHeight;
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-250;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
	     
	 var selector= $("select[name=groupTypeName]");  
	 debugger;
  	ff.form.combox.load(selector,{
 		  url: "sm/mcsmparamconfig/alltype",
 	      valueField: "name",
 	      labelField: "name",
  		  
 		} );
  	
});
</script>
</body>
</html>