<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>监测结果统计</title>
    <meta name="decorator" content="single"/>
</head>

<body class="white-bg" >

    <div class="animated fadeInRight">
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="myTab">
           <li role="presentation" class="active" >
                <a href="#sum" aria-controls="sum" role="tab" data-toggle="tab">最新监测结果</a>
               </li>
            <li role="presentation" >
                <a href="#imgs" aria-controls="imgs" role="tab" data-toggle="tab">平面布置图</a></li>
            <li role="presentation" >
                <a href="#3dimgs" aria-controls="3dimgs" role="tab" data-toggle="tab">项目三维图</a></li>
       
              
       </ul>
        <!-- Tab panes -->
        <div class="tab-content" style="overflow-y:hidden;">
            <div role="tabpanel" class="tab-pane  " id="imgs">
            <iframe src="${adminPath}/supervise/mcproitemsceneimg?projectId=${param.projectId}" width="100%" height="100%" frameborder="0" scrolling="no" id="imgs_content"></iframe> 
            </div>
			<div role="tabpanel" class="tab-pane" id="3dimgs">
			<iframe src="http://xietong.bimsop.com/iframeShareAdv/5aa68de38ca801190008b77c/IGFCmLKKSBBC8QgC6ldlOr2DQ1rNydgh4ZyB9FA5PE5Fan6N8fTHXxPtUduGGOolkEqw" width="100%" height="100%" frameborder="0" scrolling="no" id="3dimgs_content"></iframe> 
			</div>
			 <div role="tabpanel" class="tab-pane in active" id="sum">
			<iframe src="${adminPath}/supervise/mcmonitorresultstatistics?projectId=${param.projectId}" width="100%" height="100%" frameborder="0" scrolling="no" id="sum_content"></iframe> 
			</div>
        </div>
 
    </div>
 
    </div>

<html:js name="func" />
<html:js name="ff,layer,bootstrap,bootstrap-table" />
<script>
var winHeight;

    $(function () {
        $('#myTab a:first').tab('show');
    })
$(document).ready(function () {
	  var winHeight = 0;
	if (window.innerHeight) {
			winHeight = window.innerHeight;
		} else if ((document.body) && (document.body.clientHeight)) {
			winHeight = document.body.clientHeight;
		}
		var newH = winHeight - 50; 
     	$("#3dimgs").css({
			"height" : newH + "px",
			"overflow-y":"scroll"
		});  
     	$("#imgs").css({
			"height" : newH + "px",
			"overflow-y":"hidden"
		}); 
     	$("#sum").css({
			"height" : newH + "px",
			"overflow-y":"hidden"
		}); 
     	

		
	});
</script>
</body>
</html>