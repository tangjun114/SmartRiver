<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>监控站点</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput"/>
    <html:css name="simditor"/>
</head>

<body class="white-bg" formid="floodSiteForm">
<form:form id="floodSiteForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>
            <td class="width-15 active text-right">
                <label>站点名:</label>
            </td>
            <td class="width-35">
                <form:input path="siteName" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>编号:</label>
            </td>
            <td class="width-35">
                <form:input path="scid" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>横坐标:</label>
            </td>
            <td class="width-35">
                <form:input path="latitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>纵坐标:</label>
            </td>
            <td class="width-35">
                <form:input path="longitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-35">
                <button type="button" class="btn btn-primary" id="btn_gps">
                    <i class="fa fa-map"></i></button>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>站点类型(主干,支流):</label>
            </td>
            <td class="width-35">
                <form:select path="type" htmlEscape="false" class="form-control" dict="flood_site_type"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>站点类型(水文,水位):</label>
            </td>
            <td class="width-35">
                <form:select path="kind" htmlEscape="false" class="form-control" dict="flood_site_kind"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>区域名:</label>
            </td>
            <td class="width-35">
                <form:select path="areaId" htmlEscape="false" cssClass="form-control" onchange="changeAreaName()"/>
                <form:hidden path="areaName" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>备注信息:</label>
            </td>
            <td class="width-35">
                <form:input path="remarks" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>

        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor"/>
<html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline,layer"/>
<html:js name="ff"/>
<script>
    $(function () {
        var selector = $("select[name=areaId]");

        ff.form.combox.load(selector, {
            url: "flood/floodarea/loadall",
            valueField: "id",
            select: $("#areaName").val(),
            labelField: "name",
            data: {}

        }, function () {
            // loadDeviceType($("#groupTypeName").val());
            // groupTypeChange();
        });
        // loadDeviceType();

        $("#btn_gps").on('click', function (e) {
            var lat = '${data.latitude}';
            var lng = '${data.longitude}';
            ff.service.map(lat, lng, function (obj) {
                $("#latitude").val(obj.lat);
                $("#longitude").val(obj.lng);
            });
        });

    });

    function changeAreaName() {
        var areaName = $('#areaId').find('option:selected').text();
        $('#areaName').val(areaName);
    }
</script>
</body>

</html>