<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>巡检详情列表</title>
  <meta name="decorator" content="list"/>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="${staticPath}/vendors/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/jquery.fancybox.pack.js?v=2.1.7"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
</head>
<body title="巡检详情">
	<div class="row">
		<div class="col-xs-3" style="height: 500px;" id="list_row">
			<div style="height: 100%; overflow-y: scroll;">
				<div class="list-group" id="monitorItemDiv"></div>
			</div>
		</div>
		<div class="col-xs-9">
		<grid:grid id="mcMonitorPatrolDetailGridId" url="${adminPath}/monitor/mcmonitorpatroldetail/ajaxList?patrolId=${param.patrolId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	
	<grid:column label="sys.common.path" hidden="true"  name="abnormalImgs" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="abnormalDesc" width="100"/>
	
	<grid:column label=""  name="situationTypeId"  hidden="true" query="true"  queryMode="input"  condition="eq" /> 
	<grid:column label="巡检描述"  name="detailName"  width="120"/> 
	<grid:column label="是否有异常"  name="abnormalResult"  width="60"/> 	
	<grid:column label="照片"  name="abnormalImgs" formatter="button" width="70"/> 
	<grid:column label="异常说明"  name="abnormalDesc"  />
  
 	<grid:toolbar function="update" />  

</grid:grid>
	<div id="patrol_log" style="height:180px;">
	</div>
	</div>
</div>
<html:js name="func" />
<html:js name="ff" />
<script type="text/javascript">  
var winH;
var monitorItemDiv = $("#monitorItemDiv");
var newHtml= '';
function mcMonitorPatrolDetailGridIdAbnormalImgsFormatter(value, options, row){
	var href ='';
	  try{
	       if(!row.id) {
           		return '';
       		}
	       if(row.abnormalImgs != '') {
	    	   debugger;
	    	   var strs= new Array();
	    	   strs = row.abnormalImgs.split(',');
	    	   
	    	   for(i=0;i<strs.length;i++) {
		    	   href +="<a href=\"";
		  		   href += rootUrl() + "/" + strs[i];
		  		   href +="\" class=\"fancybox \"";
				   href +=" data-fancybox-group=\"gallery"+row.id+"\" title=\"\">图片"+(i+1)+"</a>&nbsp&nbsp";
	    	   }

	       }
	         
	    }catch(err){
	        	 
	    }
	   return href;
}
//注意：下面的代码是放在iframe引用的子页面中调用
$(window).resize(function() {

});
$(document).ready(function() {
	if(window.innerHeight){
		winH=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winH=document.body.clientHeight;
		}
	var newH2 = winH - 350; 
	var newHL = winH - 80;
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH2+"px!important;");
	$(".ibox .ibox-title").css("cssText","display: none"); 
	$("input[name=situationTypeId]").parent().hide();
	$("#list_row").css("cssText","height: "+newHL+"px!important;");
	$(".fancybox").fancybox();
	ff.util.submit({
 		url:"monitor/mcmonitorpatroldetail/loadSituation",
		filter:{
			  projectId: '${projectId}'
		  },
		success:function(rsp){
			if(null != rsp && rsp.obj)
			{	
				var firstVal ='';
				$.each(rsp.obj, function(index, element) {
					newHtml +="<a href=\"javascript:void(0)\" "; 
					newHtml +="id='" + element.value+ "'";
					newHtml +="data-options='" + ff.util.objToJson(element) + "'";
					newHtml +="onclick=\"searchBySituation('";
					newHtml += element.value;
					newHtml +="')\" class=\"list-group-item\"> <h4 class=\"list-group-item-heading\">";
					newHtml += element.label;
					newHtml +="</h4></a>";
					if(index == 0){
						firstVal = element.value;
					}
			    });

				monitorItemDiv.html(newHtml);
				if(firstVal != ''){
					setTimeout(function(){searchBySituation(firstVal)}, 1000);
				}
			}
		}
	
	});
	getPatrolLog();
});

function getPatrolLog() {
	//var id = '${param.patrolId}';
	var logDiv = $("#patrol_log");
	ff.util.submit({
 		url:"monitor/mcmonitorpatrollog/get",
		data: '${param.patrolId}' ,
		success:function(rsp){
			if(null != rsp && rsp.obj)
			{	
				var logHtml = '';
			    logHtml +="<dl class=\"dl-horizontal\" style=\"margin-top:15px;\">";
			    logHtml +="<dt><h2>巡检总体情况：</h2></dt>";
			  	logHtml +="<dd><h2>"+rsp.obj.overallSituation+"</h2></dd>";
			  	logHtml +="<dt><h2>巡检结论：</h2></dt>";
			  	logHtml +="<dd><h2>"+rsp.obj.summary+"</h2></dd></dl>";
				logHtml +="<div class=\"pull-right\"><h2><span>"+rsp.obj.rummagerName+"</span><span>&nbsp&nbsp"+rsp.obj.patrolDate+"</span><span>&nbsp&nbsp"+rsp.obj.address+"</span></h2></div>";
				logDiv.html(logHtml);
			}
		}
	
	});
}
function searchBySituation(id){
    $("#monitorItemDiv a").each(function(){ 
        $(this).attr("class","list-group-item");  	        
        if($(this).attr("id")==id){
        	$(this).attr("class","list-group-item active");  
        }
         
    }); 
    
    var item = ff.com.getOptions(id);
	$("input[name=situationTypeId]").val(item.value);
	ff.cache.set("situationTypeId",item);
	search('mcMonitorPatrolDetailGridIdGrid');
}


</script>
</body>
</html>