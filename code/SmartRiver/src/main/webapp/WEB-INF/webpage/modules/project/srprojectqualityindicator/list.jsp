<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目质量指标列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目质量指标">
<grid:grid id="srProjectQualityIndicatorGridId" url="${adminPath}/project/srprojectqualityindicator/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="指标名称"  name="name"  query="true"  queryMode="input"  condition="eq" />
    <grid:column label="指标类型"  name="type"  query="true"  queryMode="input"  condition="eq" />
    <grid:column label="描述"  name="desc" />
    <grid:column label="达标值"  name="value" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>