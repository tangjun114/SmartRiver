<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>监测站设备列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="监测站设备">
<grid:grid id="waterEquipmentGridId" url="${adminPath}/water/waterequipment/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="名称"  name="equipmentName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="参数"  name="equipmentParam" />
    <grid:column label="图片"  name="equipmentImg" />
    <grid:column label="图纸"  name="equipmentDesignDoc" />
    <grid:column label="公式"  name="equipmentFormula" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>