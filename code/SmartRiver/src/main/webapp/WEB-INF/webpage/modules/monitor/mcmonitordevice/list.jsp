<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>设备信息列表</title>
  <meta name="decorator" content="list"/>
  <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
</head>
<body title="设备信息">
     
<grid:grid id="mcMonitorDeviceGridId" url="${adminPath}/monitor/mcmonitordevice/ajaxList?projectId=${projectId}&typeName=${typeName}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
     <grid:column label="类型"  name="type" hidden="true"   />
    <grid:column label="型号"  name="unitType" hidden="true"   />
    <grid:column label="日期"  name="correctingDate" hidden="true"   />
    <grid:column label="日期"  name="correctingEndDate" hidden="true"   />
    
    
    <grid:column label="设备编号"  name="code"  query="true"  queryMode="input"  condition="like" />
    
    <grid:column label="设备类型"  name="typeName" query="true"  queryMode="select"  condition="eq" dict="0"  />
    <grid:column label="设备型号"  name="unitTypeName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="是否自动化"  name="isAuto"  query="true"  queryMode="select"  condition="eq"  dict="sf"/>
   
    <grid:column label="设备名修正"  name="nameCorrection" />
    <grid:column label="规格修正"  name="specRevision" />
    <grid:column label="使用状态"  name="status"  query="true"  queryMode="select"  condition="eq"  dict="SBXX_SBSFSY"/>
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button title="sys.common.delete"  groupname="opt" function="delete"   />
	<grid:toolbar function="create" url="${adminPath}/monitor/mcmonitordevice/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
    <grid:toolbar title="复制" icon="fa-database"  function="updateDialog" url="${adminPath}/supervise/mcmonitordevice/{id}/copy"  />
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
 
 
	<script type="text/javascript">
		$(function() {
			debugger;
			var typeName = ff.util.urlPara("typeName");
			loadDeviceType(typeName);

		});
		function loadDeviceType(select) {
			var selector = $("select[name=typeName]");

			ff.form.combox.load(selector, {
				url : "sm/mcsmparamconfig/devicebytype",
				valueField : "name",
				labelField : "name",
				select : select,
				callback : function() {

				}
			});

		}
		   var winHeight;
		   $(document).ready(function()   
		   {  
		   	//获取窗口高度 兼容各个浏览器
		   	if(window.innerHeight){
		   		winHeight=window.innerHeight;
		   		}
		   	else if((document.body)&&(document.body.clientHeight)){
		   		winHeight=document.body.clientHeight;
		   		}

		   	var newH = winHeight-250;
		   	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
		   }); 
	</script>
</body>
</html>