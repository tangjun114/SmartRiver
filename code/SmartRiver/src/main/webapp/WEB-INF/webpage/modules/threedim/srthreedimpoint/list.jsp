<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>实时影像地点列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="实时影像地点">
<grid:grid id="srThreedimPointGridId" url="${adminPath}/threedim/srthreedimpoint/ajaxList">
    <grid:column label="sys.common.path" hidden="true" name="url" width="100"/>
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>

    <grid:column label="名称" name="name" query="true" queryMode="input" condition="eq"/>
    <grid:column label="地址" name="addr"/>
    <grid:column label="经度" name="longitude"/>
    <grid:column label="纬度" name="latitude"/>
    <grid:column label="文件" name="url" formatter="button"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
<html:js name="ff"/>
<script type="text/javascript">


    function srThreedimPointGridIdUrlFormatter(value, options, row) {
        var href = '';
        debugger;
        try {
            if (!row.id) {
                return '';
            }
            if (row.url != '') {
                var strs = new Array();
                strs = row.url.split(',');

                for (i = 0; i < strs.length; i++) {
                    href += "<a href=\"";
                    href += ff.util.rootUrl() + "/" + strs[i];
                    href += "\">文件" + (i + 1) + "<\/a>&nbsp&nbsp";
                }

            }

        } catch (err) {

        }
        return href;
    }

</script>
</body>
</html>