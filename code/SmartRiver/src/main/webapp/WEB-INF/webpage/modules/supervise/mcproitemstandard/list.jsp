<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目规范列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目规范">
<grid:grid id="mcProItemStandardGridId" url="${adminPath}/supervise/mcproitemstandard/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	
	<grid:column label="sys.common.path" hidden="true"  name="standardPath" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="standardName" width="100"/>

    <grid:column label="规范编码"  name="standardCode" />
    <grid:column label="规范名称"  name="standardName"  />
    
    <grid:column label="添加者"  name="createByName" />
    <grid:column label="添加时间"  name="createDate" />
    <grid:column label="下载"  name="operate"  formatter="button" />
	<grid:button groupname="opt" function="delete" />
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproitemstandard/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>

</grid:grid>
<script type="text/javascript">  
$(document).ready(function() {
	var winHeight;
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-210;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});
function mcProItemStandardGridIdOperateFormatter(value, options, row){
	 
	debugger;
	  var href=linkFormatter('下载',row.standardPath);
	    
	   return href;
}
</script>
</body>
</html>