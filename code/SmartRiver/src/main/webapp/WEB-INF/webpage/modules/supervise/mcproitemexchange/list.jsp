<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目沟通交流列表</title>
  <meta name="decorator" content="list"/>
  <!-- Add fancyBox -->
<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/jquery.fancybox.pack.js?v=2.1.7"></script>
</head>
<body title="项目沟通交流">
<grid:grid id="mcProItemExchangeGridId" url="${adminPath}/supervise/mcproitemexchange/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="" hidden="true"  name="imgPath" width="100"/>
	<grid:column label="" hidden="true"  name="filePath" width="100"/>
	<grid:column label="" hidden="true"  name="createBy" width="100"/>
	
	<grid:column label="交流内容"  name="content" width="120"/>
    
    <grid:column label="可见类型"  name="visibleType"  query="true"  queryMode="select"  condition="eq"  dict="member_sub_type"/>
    <grid:column label="内容类型"  name="infoType"  query="true"  queryMode="select"  condition="eq"  dict="implementLogType"/>
    <grid:column label="附件路径"  name="filePath" formatter="button"/>
    <grid:column label="图片路径"  name="imgPath" formatter="button"/>
    <grid:column label="创建时间"  name="createDate" />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="80"/>
    <grid:button groupname="opt" function="delete" exp="'${fns:getUser().id}' == row.createBy"/>
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproitemexchange/create?projectId=${projectId}"/>
	<grid:toolbar function="update" url="${adminPath}/supervise/mcproitemexchange/{id}/update?projectId=${projectId}"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>

<html:js name="ff" />
<script type="text/javascript">

 var winHeight;
 $(document).ready(function(){
		if(window.innerHeight){
			winHeight=window.innerHeight;
			}
		else if((document.body)&&(document.body.clientHeight)){
			winHeight=document.body.clientHeight;
			}
		var newH = winHeight-220;

		$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
		$(".fancybox").fancybox();
		$(".ibox .ibox-title").css("cssText","display: none"); 
});
 function mcProItemExchangeGridIdFilePathFormatter(value, options, row){
		var href ='';
		  try{
		       if(!row.id) {
	           		return '';
	       		}
		       if(row.imgPath != '') {
		    	   debugger;
		    	   var strs= new Array();
		    	   strs = row.filePath.split(',');
		    	   
		    	   for(i=0;i<strs.length;i++) {
			    	   href +="<a href=\"";
			  		   href += rootUrl() + "/" + strs[i];
			  		   href +="\" ";
					   href +=" title=\"\">附件"+(i+1)+"</a>&nbsp&nbsp";
		    	   }

		       }
		         
		    }catch(err){
		        	 
		    }
		   return href;
	} 
function mcProItemExchangeGridIdImgPathFormatter(value, options, row){
		var href ='';
		  try{
		       if(!row.id) {
	           		return '';
	       		}
		       if(row.imgPath != '') {
		    	   debugger;
		    	   var strs= new Array();
		    	   strs = row.imgPath.split(',');
		    	   
		    	   for(i=0;i<strs.length;i++) {
			    	   href +="<a href=\"";
			  		   href += rootUrl() + "/" + strs[i];
			  		   href +="\" class=\"fancybox \"";
					   href +=" data-fancybox-group=\"gallery"+row.id+"\" title=\"\">图片"+(i+1)+"</a>&nbsp&nbsp";
		    	   }

		       }
		         
		    }catch(err){
		        	 
		    }
		   return href;
	}
</script>
</body>
</html>