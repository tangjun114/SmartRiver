<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>违法事件</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput"/>
    <html:css name="simditor"/>
</head>

<body class="white-bg" formid="lawEventForm">
<form:form id="lawEventForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>
            <td class="width-15 active text-right">
                <label>备注信息:</label>
            </td>
            <td class="width-35">
                <form:input path="remarks" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>事件名称:</label>
            </td>
            <td class="width-35">
                <form:input path="eventName" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>事件地址:</label>
            </td>
            <td class="width-35">
                <form:input path="eventAddr" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>发生时间:</label>
            </td>
            <td class="width-35">
                <form:input path="occurTime" htmlEscape="false" class="form-control layer-date"
                            placeholder="YYYY-MM-DD hh:mm:ss"
                            onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>事件等级:</label>
            </td>
            <td class="width-35">
                <form:select path="eventLevel" htmlEscape="false" class="form-control" dict="law_event_level"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>上报人:</label>
            </td>
            <td class="width-35">
                <form:input path="reportUser" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>图片:</label>
            </td>
            <td class="width-35">
                <form:fileinput
                        showType="file" extend="jpg,png,gif" maxFileCount="5"
                        fileInputWidth="50px" fileInputHeight="50px" path="eventImg"
                        htmlEscape="false" class="form-control" saveType="filepath" idField="id"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>视频:</label>
            </td>
            <td class="width-35">
                <form:fileinput
                        showType="file" extend="jpg,png,gif" maxFileCount="5"
                        fileInputWidth="50px" fileInputHeight="50px" path="eventVideo"
                        htmlEscape="false" class="form-control" saveType="filepath" idField="id"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td  class="width-15 active text-right">
                <label>所属科室:</label>
            </td>
            <td class="width-35">
                <form:input path="ownDepartment" htmlEscape="false" class="form-control"      />
                <label class="Validform_checktip"></label>
            </td>
            <td  class="width-15 active text-right">
                <label>所属所:</label>
            </td>
            <td class="width-35">
                <form:input path="ownStation" htmlEscape="false" class="form-control"      />
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td  class="width-15 active text-right">
                <label>类型:</label>
            </td>
            <td class="width-35">
                <form:select path="type" htmlEscape="false" class="form-control"    dict="law_event_type"  />
                <label class="Validform_checktip"></label>
            </td>
            <td  class="width-15 active text-right">
                <label>状态:</label>
            </td>
            <td class="width-35">
                <form:select path="status" htmlEscape="false" class="form-control"    dict="law_event_status"  />
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td  class="width-15 active text-right">
                <label>责任人:</label>
            </td>
            <td class="width-35">
                <form:input path="responsible" htmlEscape="false" class="form-control"      />
                <label class="Validform_checktip"></label>
            </td>
            <td  class="width-15 active text-right">
                <label>事件描述:</label>
            </td>
            <td class="width-35">
                <form:input path="desc" htmlEscape="false" class="form-control"      />
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td  class="width-15 active text-right">
                <label>处理措施:</label>
            </td>
            <td class="width-35">
                <form:input path="treatment" htmlEscape="false" class="form-control"      />
                <label class="Validform_checktip"></label>
            </td>
            <td  class="width-15 active text-right">
                <label>违法人员:</label>
            </td>
            <td class="width-35">
                <form:input path="offender" htmlEscape="false" class="form-control"      />
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor"/>
<script>

    $(function () {

        var today = new Date().format("yyyy-MM-dd");
        var day = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
        if ("" == document.getElementById("occurTime").value) {
            document.getElementById("occurTime").value = today + " 00:00:00";
        }
    });

</script>
</body>
</html>