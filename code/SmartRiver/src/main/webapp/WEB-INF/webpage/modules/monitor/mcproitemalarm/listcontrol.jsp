<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>警报管理</title>
  <meta name="decorator" content="list"/>
   <style type="text/css">
		.color_warn {
			color: #FCB247;
		}
		.color_caution {
			color: red;
		}
		.color_control {
			color: #993366;
		}	
		.gly_green{font-size:20px;color:green;text-shadow:#CCC 2px 2px 2px;}
		.gly_yellow{font-size:20px;color:yellow;text-shadow:#CCC 2px 2px 2px;}
		.gly_red{font-size:20px;color:red;text-shadow:#CCC 2px 2px 2px;}
		.gly_navy{font-size:20px;color:#993366;text-shadow:#CCC 2px 2px 2px;}		
	</style>
</head>
<body title="警报管理">
 
		<grid:grid id="mcProItemAlarmGridId" url="${adminPath}/monitor/mcproitemalarm/ajaxList?newProjectCount=1&alarmTypeId=3"  multiselect="false">
		<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	    <grid:column label="sys.common.path" hidden="true"  name="alarmTypeId" width="100"  />
	    <grid:column label="sys.common.name" hidden="true"  name="alarmTypeName" width="100"/>
		
		
	    <grid:column label="项目名称"  name="projectName" />
		<grid:column label="监测项"  name="monitorItemName" />
		<grid:column label="测点名称"  name="measuringPointCode" query="true"  queryMode="input"  condition="like"/>
		<grid:column label="超控时间"  name="createDate" query="false"  queryMode="date"  condition="between" />
	    <grid:column label="当前值"  name="sumValue" />
   		
		<grid:column label="控制值"  name="controlValue" />
		<grid:column label="速率报警值"  name="rateAlarmValue" />
 		<grid:column label="超控说明"  name="alarmDesp" />
 		<grid:column label="状态"  name="alarmTypeId" formatter="button" />
		<grid:column label="处理状态"  name="handleStatus" dict="alarm_handle_status"/>
		<grid:column label="处理时间"  name="updateDate" />
		<grid:column label="处理措施"  name="handleResult" />
 <%-- 		<grid:toolbar function="create"  url="${adminPath}/monitor/mcproitemalarm/create?projectId=${projectId}"/> --%>
		<grid:toolbar function="search"/>
		<grid:toolbar function="reset"/>
		</grid:grid>
 
<html:js name="ff" />	
<html:js name="func" />
<script type="text/javascript">  
var monitorItemDiv = $("#monitorItemDiv");
var newHtml= '';
 
function mcProItemAlarmGridIdAlarmTypeIdFormatter(value, options, row){
	return ff.service.alarm(row.alarmTypeId);
}
$(document).ready(function() {
 
});
</script>
</body>
</html>