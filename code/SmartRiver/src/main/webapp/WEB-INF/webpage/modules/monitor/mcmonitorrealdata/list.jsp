<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>原始记录</title>
  <meta name="decorator" content="list"/>
</head>
<body title="原始记录">
<grid:grid id="mcMonitorRealdataGridId" url="${adminPath}/monitor/mcmonitorrealdata/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	
	<grid:column label="sys.common.path" hidden="true"  name="filePath" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="fileName" width="100"/>
	
    <grid:column label="监测项选择"  name="monitorItemId"  hidden="true" query="true"  queryMode="select"  condition="eq"  dict="0" /> 
 	<grid:column label="采集时间"  name="collectTime" />
	<grid:column label="上传时间"  name="uploadTime" />
	<grid:column label="监测次数"  name="monitorCount" />
	<grid:column label="测点数量"  name="mpCount" />
	<grid:column label="上传人"  name="uploadPerson" />
	<grid:column label="检测结果"  name="measureResult" />
    <grid:column label="文件名称"  name="fileName"  formatter="button"/>
<%--     <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/> --%>

	<grid:toolbar title="申请删除" function="update" url="${adminPath}/monitor/mcmonitorrealdata/{id}/update?op=deleteapply"/>
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="func" />
<html:js name="ff" />
<script type="text/javascript">
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-270;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});
function mcMonitorRealdataGridIdFileNameFormatter(value, options, row){
	  try{
	         if(!row.id) {
               return '';
           }
	         var href=linkFormatter(row.fileName,row.filePath); }catch(err){}
	   return href;
	}
$(function () {
	 
	var selector2= $("select[name=monitorItemId]");  
 	ff.form.combox.load(selector2,{
		  url: "supervise/mcproitemmonitoritem/loadall",
	      valueField: "id",
	      labelField: "monitorItemName",
	      select:"first",
		  data: {
			  projectId: '${projectId}'
		  },
		  
		 
		},function(){
			
			setTimeout("search('mcMonitorRealdataGridIdGrid')",500);
			});
 
 	selector2.change(function(){search('mcMonitorRealdataGridIdGrid');});
 
});
</script>
</body>
</html>