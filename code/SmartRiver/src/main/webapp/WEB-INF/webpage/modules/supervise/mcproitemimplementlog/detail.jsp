<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>最新巡检结果</title>
    <meta name="decorator" content="list"/>

</head>

<body >
	<div class="row">
			<div id="carousel-example-generic" class="carousel slide"
				data-ride="carousel">

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox" id="slideDiv"></div>
				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic"
					role="button" data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#carousel-example-generic"
					role="button" data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
			<div id="desc" >
				<h3 id="time" >		
				</h3>	
				<h3  >		
				   ${detail.serial} 
				</h3>
				<h3>${detail.implementLogType}</h3>
				<h3>
					${detail.content}
				</h3>

			</div>


</div>
<html:js name="func" />
<html:js name="ff,layer" />
<script>
var newH;

$(document).ready(function(){
	
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var descH = 80;
	newH = winHeight-50 - descH;
	$("#slideDiv").css("cssText","height: "+newH+"px!important;");
	$(".ibox-title").css("cssText","display: none;");
	$("#desc").css("cssText",
			"height: "+descH+"px!important;padding:5px 10px 10px;overflow-y:scroll;");

	var datetime = '${detail.createDate}';
	var timestr = formatDateTime(datetime);//datetime.toLocaleDateString();//
	$("#time").html(timestr);
	showPager('${detail.imgPath}');
});

function showPager(data){
	var slideDiv = $("#slideDiv");
	debugger;
	var newHtml= '';
	var result=data.split(",");
	for(var i=0;i<result.length;i++){
	  if(i == 0){
			newHtml +="<div class=\"item active\" style=\"text-align=center;\">";
		} else {
			newHtml +="<div class=\"item \" style=\"text-align=center;\">";
		}
		//newHtml +="<a class=\"fancybox\" href=\"";
		//newHtml += rootUrl() + "/" + result[i];
		//newHtml +="\" data-fancybox-group=\"gallery\" title=\"\">";			
		newHtml +="<img src=\"";
		newHtml += rootUrl() + "/" + result[i];
		newHtml +="\" alt=\"暂无图片\" />";
		newHtml +="</div>";
	}

	slideDiv.html(newHtml);
	slideDiv.each(function(){
	    //获取对应img的高度
	    //将img高度赋给对应的div
	    $(this).find("img").css("height",newH-50);
	    $(this).find("img").css("margin","0 auto");
	});
}
 
	
</script>
</body>
</html>