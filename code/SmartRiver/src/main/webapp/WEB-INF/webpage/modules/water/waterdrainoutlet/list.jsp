<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>排污口管理列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="排污口管理">
<grid:grid id="waterDrainOutletGridId" url="${adminPath}/water/waterdrainoutlet/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="名称" name="outletName" query="true" queryMode="input" condition="like"/>
    <grid:column label="地址" name="outletAddr"/>
    <grid:column label="经度" name="longitude"/>
    <grid:column label="纬度" name="latitude"/>
    <grid:column label="控制信息" name="waterContrl"/>
    <grid:column label="用户信息" name="bindUser"/>
    <grid:column label="监控方式" name="monitorMode"/>
    <grid:column label="设计信息" name="designInfo"/>
    <grid:column label="统计信息" name="statisticsInfo"/>
    <grid:column label="水量" name="waterAmont"/>
    <grid:column label="状态" name="status" dict="water_out_status"/>
    <grid:toolbar title="审核通过" function="update" url="${adminPath}/water/waterdrainoutlet/{id}/update?status=1"/>
    <grid:toolbar title="审核拒绝" function="update" url="${adminPath}/water/waterdrainoutlet/{id}/update?status=2"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>