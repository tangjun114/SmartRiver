<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>抽查记录列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="抽查记录">
<grid:grid id="mcProItemInspectionGridId" url="${adminPath}/supervise/mcproiteminspection/ajaxList?projectId=${projectId}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="更新时间"  name="updateDate" />
    <grid:column label="上传人员"  name="uploadUser" />
    <grid:column label="具体地址"  name="addr" />
    <grid:column label="抽查人员"  name="inspectUser" />
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproiteminspection/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	<grid:button title="抽查"  tipMsg="是否发送抽查短信?" groupname="opt" function="rowConfirm" outclass="btn-success" innerclass="" url="${adminPath}/supervise/mcproiteminspection/send?gid=\"+row.id+\"" />
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>