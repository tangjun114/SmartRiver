<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>系统管理-参数设置列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="系统管理-参数设置">
<grid:grid id="mcSmParamConfigGridId" async="true" treeGrid="true"  expandColumn="name"  url="${adminPath}/sm/mcsmparamconfig/ajaxTreeList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
    <grid:column label="名称"  name="name" />
    <grid:column label="参数值"  name="paraValue" />
    <grid:column label="映射标签"  name="fieldName" />
    <grid:column label="映射字段"  name="fieldMapper" />
    <grid:column label="系统初始化数据"  name="sysInit"  dict="sf"/>
    <grid:column label="备注信息"  name="remarks" />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete"/>
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>