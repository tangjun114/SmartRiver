<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>项目预算列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="项目预算">
<grid:grid id="srProjectBudgetGridId" url="${adminPath}/project/srprojectbudget/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="预算名称" name="name" query="true" queryMode="input" condition="eq"/>
    <grid:column label="数量" name="qty" />
    <grid:column label="单价" name="price" />
    <grid:column label="总价" name="sum" formatter="button"/>
    <grid:column label="预算状态" name="status"/>
    <grid:column label="审核人" name="approvalUserName"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>