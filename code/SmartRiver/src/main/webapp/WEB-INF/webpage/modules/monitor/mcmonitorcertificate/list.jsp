<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>机构证书管理列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="机构证书管理">
<grid:grid id="mcMonitorCertificateGridId" url="${adminPath}/monitor/mcmonitorcertificate/ajaxList" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>

    <grid:column label="证书编号"  name="code"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="证书名称"  name="name"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="图片"  name="imgPath" />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>