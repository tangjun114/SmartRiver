<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>水毁报表列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="水毁报表">
<grid:grid id="floodDestoryReportGridId" url="${adminPath}/flood/flooddestoryreport/ajaxList">
    <%--<grid:column label="sys.common.key" hidden="true" name="id" width="100"/>--%>
    <%--<grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>--%>
    <%--<grid:button groupname="opt" function="delete"/>--%>
    <grid:column label="区域" name="areaName"/>
    <grid:column label="经济损失" name="econLoss"/>
    <grid:column label="年" name="year"/>
    <grid:column label="月" name="month"/>
    <grid:column label="日" name="day"/>
    <%--<grid:toolbar function="create"/>--%>
    <%--<grid:toolbar function="update"/>--%>
    <%--<grid:toolbar function="delete"/>--%>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>