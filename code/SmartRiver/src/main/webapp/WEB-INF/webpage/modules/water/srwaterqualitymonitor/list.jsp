<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>水质监测列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="水质监测">
<grid:grid id="srWaterQualityMonitorGridId" url="${adminPath}/water/srwaterqualitymonitor/ajaxList?siteName=${siteName}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="记录日期"  name="recordTime" query="true"  queryMode="input"  condition="eq"/>
    <grid:column label="cod值"  name="codMeas" />
    <grid:column label="氨氮"  name="nh4Meas" />
    <grid:column label="总磷"  name="tpMeas" />
    <grid:column label="toc"  name="tocMeas" />
    <grid:column label="电导率"  name="tdsMeas" />
    <grid:column label="PH"  name="phMeas" />
    <grid:column label="浊度"  name="meas" />
    <grid:column label="溶解氧"  name="doMeas" />
    <grid:column label="温度"  name="temMeas" />
    <grid:column label="盐度"  name="ydMeas" />
    <grid:column label="站点ids"  name="siteId" />
    <grid:column label="站点名"  name="siteName" query="true"  queryMode="input"  condition="like" />
	<%--<grid:toolbar function="create"/>--%>
	<%--<grid:toolbar function="update"/>--%>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>