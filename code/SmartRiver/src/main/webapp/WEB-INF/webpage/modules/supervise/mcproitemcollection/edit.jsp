<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目中的通信设备</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemCollectionForm">
    <form:form id="mcProItemCollectionForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
	    <form:hidden path="collectionId"/>
		<form:hidden path="type"/>
		 <form:hidden path="unitType"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
				<td  class="width-15 active text-right">	
		              <label>设备编码:</label>
		            </td>
					<td class="width-35">
						<form:gridselect gridId="mcMonitorCollectionGridId" title="通信设备" nested="false"
						   path="code" gridUrl="${adminPath}/monitor/mcmonitorcollection" value="${data.code}"
						   labelName="deviceCode" labelValue="${data.code}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="code"
						   callback="mcMonitorCollectionPicker" 
						   htmlEscape="false" class="form-control" readonly="true" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>设备名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="name" htmlEscape="false" readonly="true"  class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
		  		</tr>
		  		<tr>
		  		<td  class="width-15 active text-right">	
		              <label>设备类型名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="typeName" htmlEscape="false" readonly="true"  class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>设备型号名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="unitTypeName" htmlEscape="false" readonly="true"  class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
		  		</tr>
		      <tr>
					<td  class="width-15 active text-right">	
		              <label>电话:</label>
		            </td>
					<td class="width-35">
						<form:input path="phone" htmlEscape="false" class="form-control"  readonly="true"     />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>使用状态:</label>
		            </td>
					<td class="width-35">
						<form:select path="status" htmlEscape="false" class="form-control"  readonly="true"  dict="SBXX_SBSFSY"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
<script >

	function mcMonitorCollectionPicker(rowDatas,fildId,fildName){		
		//$("#"+fildId).val(rowDatas[0].id);
		//$("#"+fildName).val(rowDatas[0].username);
	 
	   $("#collectionId").val(rowDatas[0].id);
	 
	   $("#deviceCode").val(rowDatas[0].code);
	   $("#code").val(rowDatas[0].code);
		$("#name").val(rowDatas[0].name);
		 
 
		$("#type").val(rowDatas[0].type);
		$("#typeName").val(rowDatas[0].typeName);
		$("#unitType").val(rowDatas[0].unitType);
		$("#unitTypeName").val(rowDatas[0].unitTypeName);
 		$("#status").val(rowDatas[0].status);
 		$("#phone").val(rowDatas[0].phone);
	}
</script>
</body>
</html>