<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><spring:message code="sys.site.title" arguments="${platformName}"/></title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- 导入CSS -->
		<html:css  name="favicon,bootstrap,font-awesome,ace-theme"/>
	</head>

	<body class="no-skin" style="overflow-x: hidden;">
		<!-- #section:basics/navbar.layout -->
		<div id="navbar" class="navbar navbar-default ace-save-state" >
			 <%@include file="../ace/header.jsp"%>
		</div>
		<!-- /section:basics/navbar.layout -->
		<div class="main-container ace-save-state" id="main-container" >
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar  responsive  ace-save-state" style="height:100%;overflow-y:scroll; ">
				 <%@include file="../ace/left.jsp"%>
			</div>
			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="main-content-inner">
					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">				
						<div class="row content-tabs" style="margin-right:5px;">
			                <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i>
			                </button>
			                <nav class="page-tabs J_menuTabs">
			                    <div class="page-tabs-content">
			                        <a href="javascript:;" class="active J_menuTab" data-id="${adminPath}/main">首页</a>
			                    </div>
			                </nav>
			                <button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
			                </button>
			                <div class="btn-group roll-nav roll-right">
			                    <button class="dropdown J_tabClose" data-toggle="dropdown">关闭操作<span class="caret"></span>
			
			                    </button>
			                    <ul role="menu" class="dropdown-menu dropdown-menu-right">
			                        <li class="J_tabShowActive"><a>定位当前选项卡</a>
			                        </li>
			                        <li class="divider"></li>
			                        <li class="J_tabCloseAll"><a>关闭全部选项卡</a>
			                        </li>
			                        <li class="J_tabCloseOther"><a>关闭其他选项卡</a>
			                        </li>
			                    </ul>
			                </div>
			                <a href="${adminPath}/logout" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
			            </div>
			            <div class="row J_mainContent" id="content-main" >
			                <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="${adminPath}/main" frameborder="0" data-id="${adminPath}/main" seamless></iframe>
			            </div>

					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
<%-- 			            <div class="footer">
							<div class="footer-inner">
								<!-- #section:basics/footer -->
								<div class="footer-content">
									<span class="bigger-120">
										 <spring:message code="sys.site.bottom.copyright" />
									</span>
								</div>
			
								<!-- /section:basics/footer -->
							</div>
						</div>  --%>

<!-- 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a> -->
		</div><!-- /.main-container -->
		<!-- 全局js -->
		<html:js  name="jquery,bootstrap,metisMenu,slimscroll,layer,ace-theme"/>

		
		<script type="text/javascript" src="${staticPath}/common/js/contabs.js"></script>
		
	</body>
</html>