<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>巡查结果列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="巡查结果">
<grid:grid id="lawCheckResultGridId" url="${adminPath}/law/lawcheckresult/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="创建时间"  name="createDate"  query="true"  queryMode="date"  condition="between" />
    <grid:column label="设备名称"  name="equipmentName" />
    <grid:column label="计划名称"  name="planName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="路线名称"  name="routeName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="巡检次数"  name="checkCount" />
    <grid:column label="巡检结果"  name="checkResult" />
    <grid:column label="巡检人员"  name="userName"  query="true"  queryMode="input"  condition="like" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>