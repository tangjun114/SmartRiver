<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title><spring:message code="sys.organization.title" /></title>
  <meta name="decorator" content="list"/>
</head>
<body title="<spring:message code="sys.organization.title" />">
<grid:grid id="organizationGridId"  treeGrid="true" expandColumn="name"   url="${adminPath}/sys/organization/ajaxTreeList?nodeid=${user.organizationIds}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.isMonitor" hidden="true"   name="isMonitor" width="100"/>
	<grid:column label="sys.common.monitorId" hidden="true"   name="monitorId" width="100"/>
	
	<grid:column label="sys.organization.name"  name="name"  query="true" condition="like"   />
	<grid:column label="sys.organization.remarks"  name="remarks"  />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button   groupname="opt" function="delete" />
    
    <grid:toolbar  function="create"/>
	<grid:toolbar  function="update"/>
	<grid:toolbar  function="delete"/>
	<grid:toolbar  function="search"/>
	<grid:toolbar  function="reset"/>
	
	<!-- /monitor/mcmonitororg/create -->
</grid:grid>
<script type="text/javascript">

function organizationGridIdOptFormatter(value, options, row){
	  try{
	        
	    	 var href="";
	         href +="<a href=\"#\" class=\"btn btn-xs btn-danger\" class=\"btn btn-xs \"";
			 href +="onclick=\"deleteRowData('删除','${adminPath}/sys/organization/{id}/delete','"+row.id+"','organizationGridIdGrid')\"";
			 href +="><i class=\"fa fa-trash\"></i>&nbsp删除</a>&nbsp&nbsp";
			 
			 if(row.isMonitor=="1"){
				 if(null!=row.monitorId&&""!=row.monitorId){
					 ///update('修改','/admin/sys/organization/{id}/update','organizationGridIdGrid','800px','500px')
					 href +="<a href=\"#\" class=\"btn btn-xs btn-success\" class=\"btn btn-xs \"";
					 href +="onclick=\"openDialog('完善信息','${adminPath}/monitor/mcmonitororg/"+row.monitorId+"/update','"+options.gid+"','850px','650px')\"";
					 href +="><i class=\"fa fa-file-text-o\"></i>&nbsp完善信息</a>&nbsp&nbsp"; 
				 }else{
					 //debugger;
					 href +="<a href=\"#\" class=\"btn btn-xs btn-success\" class=\"btn btn-xs \"";
					 href +="onclick=\"openDialog('完善信息','${adminPath}/monitor/mcmonitororg/create?orgId="+row.id+"','"+options.gid+"','850px','650px')\"";
					 href +="><i class=\"fa fa-file-text-o\"></i>&nbsp完善信息</a>&nbsp&nbsp"; 
				 }
			 }
			 
	    	return href;
	   }catch(err){}
	   return value;
	}
</script>
</body>
</html>