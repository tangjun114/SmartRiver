<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>监控站点列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="监控站点">
<grid:grid id="floodSiteGridId" url="${adminPath}/flood/floodsite/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="160"/>
    <grid:column label="名字" name="siteName" query="true" queryMode="input" condition="eq"/>
    <grid:column label="编号" name="scid" query="true" queryMode="input" condition="eq"/>
    <grid:column label="横坐标" name="latitude"/>
    <grid:column label="纵坐标" name="longitude"/>
    <grid:column label="类型" name="type" query="true" queryMode="select" dict="flood_site_type"/>
    <grid:column label="种类" name="kind" dict="flood_site_kind"/>
    <grid:column label="区域名" name="areaName"/>
    <grid:column label="水位" name="waterLevel"/>
    <grid:column label="雨量" name="rainfull"/>
    <grid:column label="备注" name="remark"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:button title="配置警报阈值" groupname="opt" function="rowDialogDetailRefresh" outclass="btn-primary"
                 innerclass="fa-plus" url="${adminPath}/flood/floodsitewarn?parentId=\"+row.id+\""/>
    <%--<grid:toolbar groupname="opt" title="配置警报阈值" outclass="btn-success" function="configWarn()"/>--%>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
<script>
    <%--function configWarn() {--%>
    <%--var url = "${adminPath}/flood/floodsitewarn/eidtSiteWarn?parentId={id}";--%>
    <%--update("配置阈值", url, 'floodSiteGridIdGrid', "800px", "500px");--%>
    <%--}--%>
</script>
</body>
</html>