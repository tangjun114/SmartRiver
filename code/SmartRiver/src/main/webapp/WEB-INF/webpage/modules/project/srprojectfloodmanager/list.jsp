<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>工程水务管理列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="工程水务管理">
<grid:grid id="srProjectFloodManagerGridId" url="${adminPath}/project/srprojectfloodmanager/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="name" name="name" query="true" queryMode="input" condition="eq"/>
    <grid:column label="项目名称" name="proName" query="true" queryMode="input" condition="eq"/>
    <grid:column label="供水工程类别" name="type" dict="project_flood_type" query="true" queryMode="select"/>
    <grid:column label="运行状态" name="workType" dict="project_flood_work_type" query="true" queryMode="select"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:column label="备注信息" name="remarks"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>