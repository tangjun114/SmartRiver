<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>排污口水质监控列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="排污口水质监控">
<grid:grid id="waterDrainOutletResultGridId" url="${adminPath}/water/waterdrainoutletresult/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="排污口名称"  name="outletName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="监控类型"  name="monitorType"  query="true"  queryMode="select"  condition="eq"  dict="water_monitor_type"/>
    <grid:column label="水量"  name="waterAmount" />
    <grid:column label="水质"  name="waterQuality" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>