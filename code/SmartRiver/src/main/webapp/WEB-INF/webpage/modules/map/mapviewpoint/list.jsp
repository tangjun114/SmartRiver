<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>观察点管理列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="观察点管理">
<grid:grid id="mapViewPointGridId" url="${adminPath}/map/mapviewpoint/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="观测点名称"  name="pointName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="经度"  name="longitude" />
    <grid:column label="纬度"  name="latitude" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>