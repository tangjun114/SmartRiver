<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目管理列表</title>
  <meta name="decorator" content="list"/>
    <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
  <style type="text/css">
		.color_warn {
			color: #FCB247;
		}
		.color_caution {
			color: red;
		}
		.color_control {
			color: #993366;
		}	
		.gly_green{font-size:20px;color:green;text-shadow:#CCC 2px 2px 2px;}
		.gly_yellow{font-size:20px;color:orange;text-shadow:#CCC 2px 2px 2px;}
		.gly_red{font-size:20px;color:red;text-shadow:#CCC 2px 2px 2px;}
		.gly_navy{font-size:20px;color:#993366;text-shadow:#CCC 2px 2px 2px;}		
	</style>
</head>
<body title="项目管理">
 <div class="row">
<div id="tree" class="col-sm-3 col-md-2" >
        
	   <view:treeview id="organizationTreeview" treeviewSettingCallback="callback" dataUrl="${adminPath}/sys/organization/bootstrapTreeData" onNodeSelected="orgOnclick"></view:treeview>
	    <script type="text/javascript">
	    function callback(){
     	 
 	    	var tree = $('#organizationTreeview').treeview(true) ;
	    	var node = tree.getSelected();
	    	if(null != node && node.length != 0)
	    	{
	    		$("input[name='monitorId']").val(node[0].href);
	    		var item = {"id":node[0].href,"name":node[0].text};
			    ff.cache.set("organization",item);
	    		setTimeout("search('mcProjectGridIdGrid')",500);
	    	}	
 	    	 
	    };
	       function orgOnclick(event, node) {
  	    	   $("input[name='monitorId']").val(node.href);
	    	   search('mcProjectGridIdGrid');
	    	   debugger;
		   	   var item = {"id":node.href,"name":node.text};
 				ff.cache.set("organization",item);
	       }
	   </script>
	</div>
	<div id="grid" class="col-sm-12 col-md-12">
<grid:grid id="mcProjectGridId" url="${adminPath}/supervise/mcproject/ajaxList" multiselect="false" >
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.name" hidden="true"   name="name" width="100"/>

	
	   <grid:column label="项目名称"  name="name"  query="true"  formatter="button"  queryMode="input"  condition="like" />
	
	<shiro:hasPermission name="supervise:mcproject-monitor">
	
	<grid:query name="monitorId"  queryMode="hidden" />
    <grid:column label="项目编号"  name="code"  query="true"  queryMode="input"  condition="like" width="70"/>
    <grid:column label="项目类别"  name="projectType"  query="true"  queryMode="select"  condition="eq"  dict="XIANGMULB" width="70"/> 
     <grid:column label="项目地址"  name="fullAddress" />
    <grid:column label="项目负责人"  name="personInChargeName"  query="false"  queryMode="input"  condition="like" width="70"/>
    <grid:column label="数据更新时间"  name="updateDate" sortable="true"/>
    <grid:column label="工程施工进度"  name="implementLatestLog"  width="70"/>
    <grid:column label="监测状态"  name="workStatus"  query="true"  queryMode="select"  condition="eq"  dict="XCGZZT" width="70"/>
    <grid:column label=""  name="alarmPointCount"  width="70" align="center" hidden="true"/> 
    <grid:column label=""  name="controlPointCount"  width="70" align="center" hidden="true"/> 
    <grid:column label=""  name="warnPointCount"  width="70" hidden="true"/> 
    <grid:column label="警报状态"  name="alarmStatus" formatter="button" width="70" align="center"/> 
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="70"/>
	<grid:button title="sys.common.delete"  groupname="opt" function="delete"   />
	<grid:button title="项目完工"  exp="row.workStatus!='FINISHING'" tipMsg="是否确定项目已完工?" groupname="opt" function="rowConfirm" outclass="btn-success" innerclass="" url="${adminPath}/supervise/mcproject/finish?gid=\"+row.id+\"" />
	<grid:toolbar title='展开/折叠' function="show"/>
	</shiro:hasPermission>
 
	
	<shiro:hasPermission name="supervise:mcproject-supervise">
	
     <grid:column label="监测单位"  name="monitorName"   />
     <grid:column label="项目地址"  name="fullAddress" />
     <grid:column label="数据更新时间"  name="updateDate"  />
   
     <grid:column label="项目负责人"  name="personInChargeName"  query="false"  queryMode="input"  condition="like" width="70"/>
     <grid:column label="监测状态"  name="workStatus"  query="true"  queryMode="select"  condition="eq"  dict="XCGZZT" width="70"/>
     <grid:column label=""  name="alarmPointCount"  width="70" align="center" hidden="true"/> 
     <grid:column label=""  name="controlPointCount"  width="70" align="center" hidden="true"/> 
    <grid:column label=""  name="warnPointCount"  width="70" hidden="true"/> 
    <grid:column label="警报状态"  name="alarmStatus" formatter="button" width="70" align="center"/> 
      
	</shiro:hasPermission>
	
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</div>
</div>
<html:js name="func" />
<script type="text/javascript">
var status = 1;
show();
function mcProjectGridIdNameFormatter(value, options, row){
	  try{
	         if(!row.id) {
             return '';
         }
	         var href="<a href=\"#\"";
           href +="onclick=\"viewMonitorItem('"+row.id+"','"+options.gid+"','"+row.name+"','"+row.monitorName+"')\"";
				href +=">&nbsp"+row.name+"</a>&nbsp&nbsp";
	   }catch(err){}
	   return href;
}

function mcProjectGridIdAlarmStatusFormatter(value, options, row){
	var href = '';
		try{
	         if(!row.id) {
          return '';
      	}
	         var w = row.warnPointCount;
	         var a = row.alarmPointCount;
	         var c = row.controlPointCount;
	         if(c != 0) {
	        	 href +="<span class=\"glyphicon glyphicon-bell hzh-tip gly_navy\" aria-hidden=\"true\" data-original-title=\"\" title=\"\"></span><br>";
	         } else if(a != 0){
	        	 //告警
	        	 href += "<span class=\"glyphicon glyphicon-bell hzh-tip gly_red\" aria-hidden=\"true\" data-original-title=\"\" title=\"\" ></span><br>";
	         } else if (w !=0) {
	        	 //预警
	        	 href += "<span class=\"glyphicon glyphicon-bell hzh-tip gly_yellow\" aria-hidden=\"true\" data-original-title=\"\" title=\"\" ></span><br>";
	         } else {
	        	 //正常
	        	 href += "<span class=\"glyphicon glyphicon-bell hzh-tip gly_green\" aria-hidden=\"true\" data-original-title=\"\" title=\"\"></span><br>";
	         }
	         
	         href +="<span class=\"color_warn\">"+w+"</span>/";
	         href +="<span class=\"color_caution\">"+a+"</span>/";
	         href += "<span class=\"color_control\">"+c+"</span>";
	   }catch(err){}
	   
	   return href;
}

function viewMonitorItem(id,optionsGid,name,monitorName){
	var url = "${adminPath}/supervise/mcproject/monitorlist?projectId="+id+"&projectName="+name+"&monitorName="+monitorName;
	var width = "100%";
	var height = "100%";
	if (navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)) {// 如果是移动端，就使用自适应大小弹窗
		width = 'auto';
		height = 'auto';
	} else {// 如果是PC端，根据用户设置的width和height显示。

	}
	url = encodeURI(url)
	window.open(url,"project");
	//top.layer.open({
	//	type : 2,
	//	area : [ width, height ],
	//	title : "项目管理",
	//	maxmin : false, // 开启最大化最小化按钮
	//	content : url,
	 
	//	cancel : function(index) {
	//	}
	// });
}
$(document).ready(function() {
	 var newHeight = $(window).height() - 270;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newHeight+"px!important;");
	 //$("select[name='workStatus'] option[value='NOT_WORKING']").attr("selected","selected"); 
	 //setTimeout(function(){search('mcProjectGridIdGrid')}, 600);

}); 
</script>

</body>
</html>