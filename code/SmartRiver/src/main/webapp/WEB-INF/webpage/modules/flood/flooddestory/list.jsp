<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>水毁信息表列表</title>
    <meta name="decorator" content="list"/>

    <link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
    <script type="text/javascript" src="${staticPath}/vendors/fancybox/source/jquery.fancybox.pack.js?v=2.1.7"></script>

    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

    <link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
    <script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

</head>
<body title="水毁信息表">
<grid:grid id="floodDestoryGridId" url="${adminPath}/flood/flooddestory/ajaxList?id=${id}">
    <grid:column label="sys.common.path" hidden="true" name="imgPath" width="100"/>
    <grid:column label="sys.common.path" hidden="true" name="audio" width="100"/>
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="标题" name="title" query="true" queryMode="input" condition="eq"/>
    <grid:column label="严重程度" name="grade" query="true" queryMode="select" dict="flood_destory_degree" condition="eq"/>
    <grid:column label="横坐标" name="latitude"/>
    <grid:column label="纵坐标" name="longitude"/>
    <grid:column label="图像" name="imgPath" formatter="button"/>
    <grid:column label="音频" name="audio" formatter="button"/>
    <grid:column label="经济损失" name="econLoss"/>
    <grid:column label="文字描述" name="details"/>
    <%--<grid:column label="创建者" name="createBy"/>--%>
    <grid:column label="创建时间" name="createDate"/>
    <%--<grid:column label="更新者" name="updateBy"/>--%>
    <%--<grid:column label="更新时间" name="updateDate"/>--%>
    <grid:column label="备注信息" name="remarks"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
<html:js name="ff"/>
<script type="text/javascript">

    $(document).ready(function() {
        $(".fancybox").fancybox();
        $(".ibox .ibox-title").css("cssText","display: none");
    });
    function floodDestoryGridIdImgPathFormatter(value, options, row) {
        var href = '';
        try {
            if (!row.id) {
                return '';
            }
            if (row.imgPath != '') {
                debugger;
                var strs = new Array();
                strs = row.imgPath.split(',');

                for (i = 0; i < strs.length; i++) {
                    href += "<a href=\"";
                    href += ff.util.rootUrl() + "/" + strs[i];
                    href += "\" class=\"fancybox \"";
                    href += " data-fancybox-group=\"gallery" + row.id + "\" title=\"\">图片" + (i + 1) + "</a>&nbsp&nbsp";
                }

                console.log(href);
            }

        } catch (err) {

        }
        return href;
    }

    function floodDestoryGridIdAudioFormatter(value, options, row){
        var href ='';
        try{
            if(!row.id) {
                return '';
            }
            if(row.audio != '') {
                debugger;
                var strs= new Array();
                strs = row.audio.split(',');

                for(i=0;i<strs.length;i++) {
                    href +="<a href=\"";
                    href += ff.util.rootUrl() + "/" + strs[i];
                    href +="\">音频"+(i+1)+"<\/a>&nbsp&nbsp";
                }

            }

        }catch(err){

        }
        return href;
    }

</script>
</body>
</html>