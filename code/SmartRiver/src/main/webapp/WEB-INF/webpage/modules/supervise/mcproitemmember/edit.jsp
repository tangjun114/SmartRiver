<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目人员</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemMemberForm">
    <form:form id="mcProItemMemberForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
		   
		   	<tr>				
					<td  class="width-15 active text-right">	
		              <label>人员类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="type"   readonly="true" htmlEscape="false" class="form-control"  dict="projectMemberType"      />
						<label class="Validform_checktip"></label>
					</td>

					<td  class="width-15 active text-right">	
		              <label>人员类别:</label>
		            </td>
					<td class="width-35">
						<form:select path="subType"   readonly="true" htmlEscape="false" class="form-control"  dict="member_sub_type"      />
						<label class="Validform_checktip"></label>
					</td>
					</tr>
				<tr>
				<td  class="width-15 active text-right">	
		              <label>人员:</label>
		            </td>
					<td class="width-35">
						<%-- <form:input path="personInChargeName" htmlEscape="false" class="form-control"      /> --%>
						 <form:gridselect gridId="userGridId" title="人员" nested="false" 
						   path="userId" gridUrl="${adminPath}/sys/user/select?projectId=${projectId }"  value="${data.userId}"
						   labelName="username" labelValue="${data.username}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="username"
						   callback="userDataPicker" 
						   htmlEscape="false" class="form-control" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="realname" readonly="true" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>	
				</tr>
				<tr>
					
					<td  class="width-15 active text-right">	
		              <label>电话:</label>
		            </td>
					<td class="width-35">
						<form:input path="phone" readonly="true" htmlEscape="false" class="form-control"  datatype="m"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>邮箱:</label>
		            </td>
					<td class="width-35">
						<form:input path="email" readonly="true" htmlEscape="false" class="form-control"  datatype="e"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
								<tr>
					<td  class="width-15 active text-right">	
		              <label>部门:</label>
		            </td>
					<td class="width-35">
						<form:input path="deptName" readonly="true" htmlEscape="false" class="form-control"       />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>职位:</label>
		            </td>
					<td class="width-35">
						<form:input path="position" readonly="true" htmlEscape="false" class="form-control"       />
						<label class="Validform_checktip"></label>
					</td>
					
				</tr>
				<tr>
					
					
					<td  class="width-15 active text-right">	
		              <label>备注信息:</label>
		            </td>
					<td class="width-35">
						<form:input path="remarks" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>客户端设置:</label>
		            </td>
					<td class="width-35">
						<form:checkboxes path="clientType" htmlEscape="false"   dict="member_client_type"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				 <tr>
		         <td class="active"><label class="pull-right"><font color="red">*</font>短信设置:</label></td>
		         <td colspan="3">
		         	<form:checkboxes path="msgPerm" nested="true" dict="msgPerm"    htmlEscape="false" cssClass="i-checks required"/>
		          
		         </td>
		      </tr>
		       <tr>
		         <td class="active"><label class="pull-right"><font color="red">*</font>报告设置:</label></td>
		         <td colspan="3">
		         	<form:checkboxes path="reportPerm" nested="false" dict="reportPerm"    htmlEscape="false" cssClass="i-checks required"/>
		          
		         </td>
		      </tr>
		            
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
<html:js name="ff" />
<script>
 
    $(function(){
    	var item = ff.cache.get("type");
    	$("#type").val(item.value);
    	debugger;
     
    	var para = "type="+item.value;
    	$("#userId").attr("para",para);
    });
	
	function userDataPicker(rowDatas,fildId,fildName){		
		$("#"+fildId).val(rowDatas[0].id);
		$("#"+fildName).val(rowDatas[0].username);
		$("#subType").val(rowDatas[0].subType);
		$("#realname").val(rowDatas[0].realname);
		$("#phone").val(rowDatas[0].phone);
		$("#email").val(rowDatas[0].email);
		 
		$("#position").val(rowDatas[0].positionName);
		$("#deptName").val(rowDatas[0].deptName);
	}
</script>
</body>
</html>