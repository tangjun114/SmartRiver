<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>工程经费列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="工程经费">
<grid:grid id="srProjectFundsGridId" url="${adminPath}/project/srprojectfunds/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="220"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="项目名称" name="proName" query="true" queryMode="input" condition="eq"/>
    <grid:column label="经费类型" name="type" query="true" queryMode="select" condition="eq" dict="project_funds_type"/>
    <grid:column label="标题" name="name" query="true" queryMode="input" condition="eq"/>
    <grid:column label="金额" name="amount"/>
    <grid:column label="审批状态" name="approvalStatus" query="true" queryMode="select" condition="eq"
                 dict="project_funds_aduitstatus"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:column label="审批人" name="approvalUserName"/>
    <grid:column label="备注信息" name="remarks"/>
    <grid:button title="审核通过" groupname="opt" function="updateStatus"  outclass="btn-primary" tipMsg="确认审核通过吗"
                 innerclass="fa-plus" url="${adminPath}/project/srprojectfunds/{id}/update?approvalStatus=1"/>
    <grid:button title="审核失败" groupname="opt" function="updateStatus" outclass="btn-primary" tipMsg="确认审核通过吗"
                 innerclass="fa-plus" url="${adminPath}/project/srprojectfunds/{id}/update?approvalStatus=2"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>