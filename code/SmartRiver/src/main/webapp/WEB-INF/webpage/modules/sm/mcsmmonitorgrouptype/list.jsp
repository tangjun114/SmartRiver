<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>监测组类别列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="监测组类别">
<grid:grid id="mcSmMonitorGroupTypeGridId" url="${adminPath}/sm/mcsmmonitorgrouptype/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="编码"  name="code"  query="true"  queryMode="input"  condition="like" />
	
    <grid:column label="监测组类别"  name="groupTypeName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="备注信息"  name="remarks" />
    <grid:column label="单位"  name="unit" />
    <grid:column label="最大值名称"  name="maxName" />
    <grid:column label="最小值名称"  name="minName" />
    <grid:column label="监测项备注"  name="itemRemark" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>