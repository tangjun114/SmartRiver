<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>项目详情</title>
    <meta name="decorator" content="single"/>
    <style type="text/css">
		.pro-item-1 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: #e0e2e5
		}
		.pro-item-2 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: transparent
		}
</style>
</head>

<body class="gray-bg" >

    <div class="animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet box  portlet-blue">
					 <div class="portlet-header">
						<div class="caption">基本信息</div>
						<div class="tools">
							<input  class="btn  btn-success" style="dispaly:none;" type="button" onclick="showModify()" value="编辑" />
	                     </div>
					</div>	
					<div class="portlet-body" id="project_content">
					        <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>机构名称</strong></div>
                                <div class="col-sm-9" >
                                	${data.name}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>邮政编码</strong></div>
                                <div class="col-sm-9" >
                                ${data.zipCode}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label" ><strong>机构传真</strong></div>
                                <div class="col-sm-9" >
                                	${data.fax}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label"><strong>机构地址</strong></div>
                                <div class="col-sm-9">
                                    ${data.address}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>机构网址</strong></div>
                                <div class="col-sm-9">
                                    ${data.url}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>办公面积</strong></div>
                                <div class="col-sm-9">
                                    ${data.acreage}
                                </div>
                            </div>	
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label" ><strong>设备总数</strong></div>
                                <div class="col-sm-9" >
                                	${data.device}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label"><strong>机构邮箱</strong></div>
                                <div class="col-sm-9">
                                    ${data.email}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>注册资金</strong></div>
                                <div class="col-sm-9">
                                    ${data.regCapital}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>技术人员</strong></div>
                                <div class="col-sm-9">
                                    ${data.artisan}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>联系人电话</strong></div>
                                <div class="col-sm-9">
                                    ${data.phone}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>机构座机号码</strong></div>
                                <div class="col-sm-9">
                                    ${data.orgPhone}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>初级职称</strong></div>
                                <div class="col-sm-9">
                                    ${data.primaryJobTitle}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>法人代表</strong></div>
                                <div class="col-sm-9">
                                    ${data.representative}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>中级职称</strong></div>
                                <div class="col-sm-9">
                                    ${data.midJobTitle}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>技术总负责人</strong></div>
                                <div class="col-sm-9">
                                    ${data.chiefTechOfficer}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>高级职称</strong></div>
                                <div class="col-sm-9">
                                    ${data.seniorJobTitle}
                                </div>
                            </div>
                                                 				        
					</div>
            </div>
         </div> 
    </div>
</div>

<html:js name="ff,layer,func" />
<script>
	$(document).ready(function () {
		var newH = $(document).height()-58;
	
		$("#project_content").css({
			"height":newH+"px",
			"overflow-y":"scroll"});
	});

	 
	function showModify() {
		var id = '${data.id}';
		var url = "${adminPath}/monitor/mcmonitororg/"+id+"/update";
		top.layer.open({
			type : 2,
			area: ['800px', '500px'],
			title : '编辑',
			maxmin : false, // 开启最大化最小化按钮
			content : url,
			btn : [ '确定', '关闭' ],
			yes : function(index, layero) {
  				var body = top.layer.getChildFrame('body', index);
				var iframeWin = layero.find('iframe')[0]; // 得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
				// 文档地址
				// http://www.layui.com/doc/modules/layer.html#use
				iframeWin.contentWindow.doSubmit(top, index, function() {
					// 刷新表单
					 //refreshTable('contentTable');
					top.layer.close(index);
				}); 
				
				return true;

			},
			cancel : function(index) {
			}
		});
	}
	
</script>
</body>
</html>