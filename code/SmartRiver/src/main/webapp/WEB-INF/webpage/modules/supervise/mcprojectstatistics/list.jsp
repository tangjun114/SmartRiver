<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  
  <meta name="decorator" content="list"/>
      <link rel="stylesheet" href="${staticPath}/vendors/bootstrap/js/bootstrap-table.css?v=1.0.7" type="text/css" media="screen" />
  
</head>
<body  >
 
    <div class="tab-content" style="overflow-x:hidden;">
        
        <form id="ff_filter"   method="post">
					 <div class="row">
					  <div class="col-sm-12">
					   <div class="form-inline">
					        单位名称：
 					 
 					   <input  filter="eq" name="monitor_name" class="form-control  "     />
 					   </div>
					   <div class="pull-right">
                            <a onclick="load()" class="btn btn-primary btn-sm" ><i class="fa fa-arrow-circle-up"></i>&nbsp;&nbsp;查询</a>
                            <a onclick="location.href=location.href;"  class="btn btn-primary btn-sm"><i class="fa fa-remove"></i>&nbsp;&nbsp;清空</a>
  					  </div>
  					  </div>
  					  </div>
					</form>
             <table id="sumData"  class='table  ' style="  word-wrap:break-all;">
             
             </table>
		 
    </div>
	 <html:js name="ff,layer,bootstrap,bootstrap-table" />
   
	<script type="text/javascript">
 

	//载入图表
	$(function () {
		
 
		load();
     
	    
	});
	function load()
	{
		
	    ff.req.filter = ff.com.getFilterObj("#ff_filter");
		ff.util.submit({
			url: "supervise/mcprojectstatistics/sum?projectId=${param.projectId}",
             success:function(rsp)
            {
            	loadTable(rsp.obj);
            }
		});
	}
	function loadTable(data)
	{
		  if(null == data || data.length == 0)
		  {
			  return;
		  }
		  var columns = [];
		  
		  columns.push({field:"monitor_name",title:"单位名称"});
		  columns.push({field:"code",title:"项目总数"});
		  columns.push({field:"name",title:"正常项目"});
		  columns.push({field:"warn_point_count",title:"预警项目"});
		  columns.push({field:"alarm_point_count",title:"报警项目"});
		  columns.push({field:"control_point_count",title:"超控项目"});
	 

		  $("#sumData").bootstrapTable('destroy').bootstrapTable({
			    columns: columns,
			    data: data

	      });
	 
		  
	}
	
 
	</script>
</body>
</html>