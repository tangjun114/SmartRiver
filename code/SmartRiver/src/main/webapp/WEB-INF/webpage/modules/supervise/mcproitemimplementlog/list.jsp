<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目监控-实施日志列表</title>
  <meta name="decorator" content="list"/>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->


<!-- Add fancyBox -->
<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/jquery.fancybox.pack.js?v=2.1.7"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
</head>
<body title="项目监控-实施日志">
<grid:grid id="mcProItemImplementLogGridId" url="${adminPath}/supervise/mcproitemimplementlog/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.path" hidden="true"  name="imgPath" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="imgName" width="100"/>
    <grid:column label="日志类型"  name="implementLogType"  query="true"  queryMode="select"  condition="eq"  dict="implementLogType"/>
    <grid:column label="工况编号"  name="serial" />
    <grid:column label="内容"  name="content" />
    
    <grid:column label="时间"  name="createDate" />
    <grid:column label="图片"  name="imgPath" formatter="button"/>
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproitemimplementlog/create?projectId=${projectId}"/>
	<grid:toolbar function="update" url="${adminPath}/supervise/mcproitemimplementlog/{id}/update?projectId=${projectId}"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<script type="text/javascript">  
var winH;
//注意：下面的代码是放在iframe引用的子页面中调用
$(window).resize(function() {
	if(window.innerHeight){
		winH=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winH=document.body.clientHeight;
		}
	var newH2 = winH - 215; 
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH2+"px!important;");
});
$(document).ready(function() {
	$(".fancybox").fancybox();
	$(".ibox .ibox-title").css("cssText","display: none"); 
});

function mcProItemImplementLogGridIdImgPathFormatter(value, options, row){
	var href ='';
	  try{
	       if(!row.id) {
           		return '';
       		}
	       if(row.imgPath != '') {
	    	   debugger;
	    	   var strs= new Array();
	    	   strs = row.imgPath.split(',');
	    	   
	    	   for(i=0;i<strs.length;i++) {
		    	   href +="<a href=\"";
		  		   href += rootUrl() + "/" + strs[i];
		  		   href +="\" class=\"fancybox \"";
				   href +=" data-fancybox-group=\"gallery"+row.id+"\" title=\"\">图片"+(i+1)+"</a>&nbsp&nbsp";
	    	   }

	       }
	         
	    }catch(err){
	        	 
	    }
	   return href;
}


</script>
</body>
</html>