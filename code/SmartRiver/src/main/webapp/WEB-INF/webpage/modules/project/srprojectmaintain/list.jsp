<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>项目维护列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="项目维护">
<grid:grid id="srProjectMaintainGridId" url="${adminPath}/project/srprojectmaintain/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="name" name="name" query="true" queryMode="input" condition="eq"/>
    <grid:column label="项目名称" name="proName" query="true" queryMode="input" condition="eq"/>
    <grid:column label="承建单位" name="contractor"/>
    <grid:column label="开始时间" name="starttime"/>
    <grid:column label="结束时间" name="endtime"/>
    <grid:column label="资金" name="amt"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:column label="备注信息" name="remarks"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>