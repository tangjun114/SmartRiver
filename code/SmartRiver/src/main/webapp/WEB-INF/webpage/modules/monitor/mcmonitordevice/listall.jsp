<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>设备信息列表</title>
  <meta name="decorator" content="list"/>
  <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
</head>
<body title="设备信息">
<div class="row">
<div id="tree" class="col-sm-3 col-md-2" >
        
	   <view:treeview id="organizationTreeview" treeviewSettingCallback="callback" dataUrl="${adminPath}/sys/organization/bootstrapTreeData" onNodeSelected="orgOnclick"></view:treeview>
	    <script type="text/javascript">
	    function callback(){
     	 
 	    	var tree = $('#organizationTreeview').treeview(true) ;
	    	var node = tree.getSelected();
	    	if(null != node && node.length != 0)
	    	{
	    		$("input[name='monitorId']").val(node[0].href);
	    		var item = {"id":node[0].href,"name":node[0].text};
			    ff.cache.set("organization",item);
	    		setTimeout("search('mcMonitorDeviceGridIdGrid')",500);
	    	}	
 	    	 
	    };
	       function orgOnclick(event, node) {
	    	   //查询时间
	    	   //gridquery隐藏 查询标签概念，query,单独的query
	    	   
	    	
	    	   $("input[name='monitorId']").val(node.href);
	    	   search('mcMonitorDeviceGridIdGrid');
	    	   debugger;
		   	   var item = {"id":node.href,"name":node.text};
		   	 
			 
				ff.cache.set("organization",item);
	       }
	   </script>
	</div>
	<div id="grid"  class="col-sm-12 col-md-12">
<grid:grid id="mcMonitorDeviceGridId" url="${adminPath}/monitor/mcmonitordevice/ajaxList?projectId=${projectId}&typeName=${typeName}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
    <grid:query name="monitorId"  queryMode="hidden" />
    <grid:column label="类型"  name="type" hidden="true"   />
    <grid:column label="型号"  name="unitType" hidden="true"   />
    <grid:column label="日期"  name="correctingDate" hidden="true"   />
    <grid:column label="日期"  name="correctingEndDate" hidden="true"   />
    
    
    <grid:column label="设备编号"  name="code"  query="true"  queryMode="input"  condition="like" />
    
    <grid:column label="设备类型"  name="typeName" query="true"  queryMode="select"  condition="eq" dict="0"  />
    <grid:column label="设备型号"  name="unitTypeName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="是否自动化"  name="isAuto"  query="true"  queryMode="select"  condition="eq"  dict="sf"/>
   
    <grid:column label="设备名修正"  name="nameCorrection" />
    <grid:column label="规格修正"  name="specRevision" />
    <grid:column label="使用状态"  name="status"  query="true"  queryMode="select"  condition="eq"  dict="SBXX_SBSFSY"/>
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button title="sys.common.delete"  groupname="opt" function="delete"   />
	<grid:toolbar title='展开/折叠' function="show"/>
	<grid:toolbar function="create" url="${adminPath}/monitor/mcmonitordevice/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</div>
</div>
 
	<script type="text/javascript">
	var status = 1;
	 
		$(function() {
			debugger;
			show();
			var typeName = ff.util.urlPara("typeName");
			loadDeviceType(typeName);

		});
		function loadDeviceType(select) {
			var selector = $("select[name=typeName]");

			ff.form.combox.load(selector, {
				url : "sm/mcsmparamconfig/devicebytype",
				valueField : "name",
				labelField : "name",
				select : select,
				callback : function() {

				}
			});

		}
		$(document).ready(function() {
			if(window.innerHeight){
				winHeight=window.innerHeight;
				}
			else if((document.body)&&(document.body.clientHeight)){
				winHeight=document.body.clientHeight;
				}
			var newH = winHeight-310;
			 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
		});
	</script>
</body>
</html>