<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>视频站点管理</title>
    <meta name="decorator" content="form"/>
    <html:js name="bootstrap-fileinput"/>
    <html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline,layer"/>
    <html:js name="ff"/>
</head>

<body class="white-bg" formid="srVideoPointForm">
<form:form id="srVideoPointForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>

            <td class="width-15 active text-right">
                <label>名称:</label>
            </td>
            <td class="width-35">
                <form:input path="name" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>备注信息:</label>
            </td>
            <td class="width-35">
                <form:input path="remarks" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>经度:</label>
            </td>
            <td class="width-35">
                <form:input path="longitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>纬度:</label>
            </td>
            <td class="width-35">
                <form:input path="latitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>

        <tr>

            <td class="width-15 active text-right">
                <label>地址:</label>
            </td>
            <td class="width-35">

                <div style="width: 80%;float: left">
                    <form:input path="addr" htmlEscape="false" class="form-control" datatype="*"/>
                    <label class="Validform_checktip"></label>
                </div>
                <div>
                    <button type="button" class="btn btn-primary" id="btn_gps">
                        <i class="fa fa-map"></i></button>
                </div>

            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>视频url:</label>
            </td>
            <td class="width-35">
                <form:input path="url" htmlEscape="false" class="form-control" datatype="*"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>

            <td class="width-15 active text-right">
                <label>账号:</label>
            </td>
            <td class="width-35">
                <form:input path="account" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>密码:</label>
            </td>
            <td class="width-35">
                <form:input path="password" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>

        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor"/>

<script>
    $(function () {

        $("#btn_gps").on('click', function (e) {

            var lat = '${data.latitude}';
            var lng = '${data.longitude}';
            ff.service.map(lat, lng, function (obj) {

                $("#latitude").val(obj.lat);
                $("#longitude").val(obj.lng);
                $("#addr").val(obj.addr);
            });
        });
    });

</script>
</body>
</html>