<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>报告列表</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProReportForm">
    <form:form id="mcProReportForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>文件名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="reportName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>文件类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="reportType" htmlEscape="false" class="form-control"        />
						<label class="Validform_checktip"></label>
					</td>
		  		</tr>
		  		<tr>
					<td  class="width-15 active text-right">	
		              <label>起始时间:</label>
		            </td>
		            <td class="width-35">
						<form:input path="extendForm.startDate"  datefmt="yyyy-MM-dd hh:mm:ss" class="form-control layer-date" placeholder="YYYY-MM-DD hh:mm:ss" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"       />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>终止时间:</label>
		            </td>
		            <td class="width-35">
						<form:input path="extendForm.endDate"  datefmt="yyyy-MM-dd hh:mm:ss" class="form-control layer-date" placeholder="YYYY-MM-DD hh:mm:ss" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"       />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>报告编号:</label>
		            </td>
					<td class="width-35">
						<form:input path="extendForm.reportID" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>管理编号:</label>
		            </td>
					<td class="width-35">
						<form:input path="extendForm.manageID" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		      	<tr>
		      		<td  class="width-15 active text-right">	
		              <label>工况条数:</label>
		            </td>
					<td class="width-35">
						<form:input path="extendForm.conditionCount" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
		      		<td  class="width-15 active text-right" style="display:none">	
		              <label>报告水印:</label>
		            </td>
					<td class="width-35" style="display:none">
						<form:select path="extendForm.waterMark" htmlEscape="false" class="form-control"  dict="report_mark"      />
						<label class="Validform_checktip"></label>
					</td>
					
		  		</tr>
		  		<tr>
					<td class="width-15 active text-right">
					   <label class="pull-right">是否显示巡检:</label>
					</td>
					<td class="width-35">
					     <form:radiobuttons path="extendForm.showPatrol"   dict="sf" defaultValue="1"  htmlEscape="false" cssClass="i-checks required" />
					</td>
					<td class="width-15 active text-right">
					   <label class="pull-right">是否显示监测项:</label>
					</td>
					<td class="width-35">
					     <form:radiobuttons path="extendForm.showMonitorItem"   dict="sf" defaultValue="0"  htmlEscape="false" cssClass="i-checks required" />
					</td>
			  </tr>
			  <tr>
		         <td  class="width-15 active text-right">	
		              <label>根据各监测项目监测成果可知:</label>
		         </td>
		         <td class="width-35" colspan="3" >
		              <form:textarea path="extendForm.monitorResultAnalysis" htmlEscape="false" rows="3" maxlength="200" class="form-control "/>
		         </td>
		      </tr>
		      <tr>
		         <td  class="width-15 active text-right">	
		              <label>本次新增报警处理:</label>
		         </td>
		         <td class="width-35" colspan="3" >
		              <form:textarea path="extendForm.newAlarmHandle" htmlEscape="false" rows="3" maxlength="200" class="form-control "/>
		         </td>
		      </tr>
		      <tr>
		         <td  class="width-15 active text-right">	
		              <label>历史报警处理:</label>
		         </td>
		         <td class="width-35" colspan="3" >
		              <form:textarea path="extendForm.historyAlarmHandle" htmlEscape="false" rows="3" maxlength="200" class="form-control "/>
		         </td>
		      </tr>
		      <tr>
		         <td  class="width-15 active text-right">	
		              <label>建议如下:</label>
		         </td>
		         <td class="width-35" colspan="3" >
		              <form:textarea path="extendForm.suggest" htmlEscape="false" rows="3" maxlength="200" class="form-control "/>
		         </td>
		      </tr>	
		  		<tr>
					<td class="width-15 active text-right">
					   <label class="pull-right">图纸选择:</label>
					</td>
					<td class="width-35">
					     <form:radiobuttons path="extendForm.sheetType"   dict="report_sheet_type" defaultValue="auto"  htmlEscape="false" cssClass="i-checks required" />
					</td>
					<td class="width-15 active text-right">
					   <label class="pull-right">新版体验:</label>
					</td>
					<td class="width-35">
					     <form:radiobuttons path="extendForm.newVersion"   dict="sf" defaultValue="0"  htmlEscape="false" cssClass="i-checks required" />
					</td>
			  </tr>		      	      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput,ff" />
<html:js name="simditor" />
<script type="text/javascript">
   $(function()
	 {  
	   var selector= $("select[name=reportType]");  
		 
	   var data = "${data.projectId}";
	   if("" == data)
	   {
		   data = "${projectId}";
	   }	   
    	ff.form.combox.load(selector,{
   		  url: "supervise/mcproreport/loadtemplate",
   		  data:{projectId:data},
   	      valueField: "value",
    	   labelField: "label",
     		  
   		} );
	 debugger;
		var today = new Date().format("yyyy-MM-dd");
		var day = new Date(new Date().getTime()-7*24*60*60*1000);
		var data = "${data.extendForm.startDate}";
		 
 		 if("" == data) 
 		{ 
 			 document.getElementById("extendForm.startDate").value = day.format("yyyy-MM-dd")+" 00:00:00";
 		} 	 
 		 if("" == document.getElementById("extendForm.endDate").value) 
  		{
 	 		  document.getElementById("extendForm.endDate").value =today+" 23:59:59";

  		}	
 		 
 		 });
</script>
</body>
</html>