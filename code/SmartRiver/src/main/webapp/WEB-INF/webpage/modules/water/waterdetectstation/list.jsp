<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>监测站管理列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="监测站管理">
<grid:grid id="waterDetectStationGridId" url="${adminPath}/water/waterdetectstation/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
   
    <grid:column label="站点名称"  name="stationName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="备注信息"  name="remarks" />
    <grid:column label="经度"  name="longitude" />
    <grid:column label="纬度"  name="latitude" />
    <grid:column label="地址"  name="addr" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>