<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目三维图列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目三维图">
<grid:grid id="mcProItemScene3dimgGridId" url="${adminPath}/supervise/mcproitemscene3dimg/ajaxList?projectId=${param.projectId}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproitemscene3dimg/create?projectId=${param.projectId}"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<script type="text/javascript">  
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-230;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});
</script>
</body>
</html>