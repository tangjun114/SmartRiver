<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>工程水务管理</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput"/>
    <html:css name="simditor"/>
</head>

<body class="white-bg" formid="srProjectFloodManagerForm">
<form:form id="srProjectFloodManagerForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>项目名称:</label>
            </td>
            <td class="width-35">
                <form:select path="proId" htmlEscape="false" class="form-control" onchange="changeProName()"/>
                <form:hidden path="proName" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>name:</label>
            </td>
            <td class="width-35">
                <form:input path="name" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>供水工程类别:</label>
            </td>
            <td class="width-35">
                <form:select path="type" htmlEscape="false" class="form-control" dict="project_flood_type"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>运行状态:</label>
            </td>
            <td class="width-35">
                <form:select path="workType" htmlEscape="false" class="form-control" dict="project_flood_work_type"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>备注信息:</label>
            </td>
            <td class="width-35">
                <form:input path="remarks" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right"></td>
            <td class="width-35"></td>
        </tr>

        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor"/>
<html:js name="ff"/>
<script>
    $(function () {

        var selector = $("select[name=proId]");

        ff.form.combox.load(selector, {
            url: "project/srprojectinfo/loadall",
            valueField: "id",
            select: $("#proId").val(),
            labelField: "name",
            data: {}
        }, function () {
        });

    });

    function changeProName() {
        var proName = $('#proId').find('option:selected').text();
        $('#proName').val(proName);
    }
</script>
</body>
</html>