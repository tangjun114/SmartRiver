<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>检测数据列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="检测数据">
<grid:grid id="waterDetectResultGridId" url="${adminPath}/water/waterdetectresult/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="备注信息"  name="remarks" />
    <grid:column label="站点名称"  name="stationName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="设备名称"  name="equipmentName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="检测结果"  name="detectResult" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>