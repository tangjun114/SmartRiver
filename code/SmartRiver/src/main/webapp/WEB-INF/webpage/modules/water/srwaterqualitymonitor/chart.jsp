<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>分析图</title>
    <meta name="decorator" content="single"/>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div>
            <div class="portlet box  portlet-grey">
                <div>
                    <h5></h5>
                    <form class="form-inline">
                        <div>
                            <div class="form-group">
                                <label class="sr-only" for="siteId">站点</label>
                                <select
                                        class="form-control" id="siteName" onchange="changeProName()"></select>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="startTime">开始时间</label>
                                <input id="startTime" name="deadTime"
                                       onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                                       placeholder="YYYY-MM-DD hh:mm:ss" class="form-control layer-date" type="text"
                                       value="">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="endTime">结束时间</label>

                                <input id="endTime" name="deadTime"
                                       onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                                       placeholder="YYYY-MM-DD hh:mm:ss" class="form-control layer-date" type="text"
                                       value="">

                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" id="search" type="button" onclick="searchByArea()">查询
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-8">
                        <div class="echarts" id="echarts-bar-chart1" style="height:600px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<html:js name="echarts"/>

<html:js name="ff"/>
<script type="text/javascript">

    $(function () {


        var selector = $("#siteName");

        ff.form.combox.load(selector, {
            url: "water/waterdetectstation/loadall",
            valueField: "stationName",
            select: $("#siteName").val(),
            labelField: "stationName",
            data: {}
        }, function () {
        });

        var today = new Date().format("yyyy-MM-dd");
        var day = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000).format("yyyy-MM-dd");
        if ("" == document.getElementById("endTime").value) {
            document.getElementById("endTime").value = today + " 23:59:59";
        }
        if ("" == document.getElementById("startTime").value) {
            document.getElementById("startTime").value = day + " 00:00:00";
        }

        searchByArea("深圳河—布吉河");

    });


    function searchByArea(siteIdvar) {
        var siteName = $('#siteName option:selected').val();
        if (typeof(siteIdvar) != "undefined") {
            siteName = siteIdvar;
        }
        var startTime = $('#startTime').val();
        var endTime = $('#endTime').val();
        ff.util.submit({
            url: "/water/srwaterqualitymonitor/chart/data",
            data: 'waterlevel',
            filter: {
                siteName: siteName,
                startTime: startTime,
                endTime: endTime
            },
            success: function (rsp) {
                if (null != rsp && rsp.obj) {
                    var option = {
                        xAxis: {
                            name: "时间",
                        },

                        yAxis: {
                            // name: "水位(mm)",
                        },
                    };

                    ff.chart.load("#echarts-bar-chart1", "", rsp.obj, "line", option);
                }
            }

        });
        var winHeight;
        if (window.innerHeight) {
            winHeight = window.innerHeight;
        }
        else if ((document.body) && (document.body.clientHeight)) {
            winHeight = document.body.clientHeight;
        }
        var newH = winHeight - 80;
        $("#echarts-bar-chart1").css("cssText", "height: " + newH + "px!important;");


    }

</script>

</body>

</html>