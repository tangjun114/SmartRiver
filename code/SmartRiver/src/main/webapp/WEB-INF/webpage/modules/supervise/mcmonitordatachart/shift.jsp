<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>警报图</title>
    <meta name="decorator" content="single"/>
    	<!-- 全局js -->
	<html:js name="echarts" />
 
	<html:js name="bootstrap-multiselect" />
    <html:js name="ff" />
</head>

<body class="white-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet box  portlet-grey">
					 <div class="portlet-header">
						<div class="caption">曲线图</div>
						<div class="tools">
	                        <i class="fa fa-chevron-up"></i>
	                        <i class="fa fa-refresh"></i><i class="fa fa-times"></i>
	                     </div>
					</div>	
					<form id="ff_filter"   method="post" >
					 <div class="row" style="margin-left:10px">
					  <div class="col-sm-12">
					   <div class="form-inline" >
					   	 <label style="display:none">显示告警: </label>   
					   	 <select  style="display:none" id="alarmList"   class="form-control"  onchange="load()">
 					       
 					   </select>
					    <select  filter="eq" name="subareaId" class="form-control"    style="display: none;" ></select>
					 <label>监测项选择 :</label>  
					 <select  filter="eq" name="monitorItemId"   class="form-control"    style=" background-color: #EEEEEE;" disabled="disabled" ></select>
					<label>测点选择: </label>  <select  id="measuringPointCode"    filter="eq" name="measuringPointCode"   style=" background-color: #EEEEEE;" disabled="disabled"  >
 					  </select>
 					  
 					 <label>数据类型: </label> <select  id="valueType"    name="valueType"   class="form-control"  onchange="load()">
 					       <option value="sumValue">累计变化</option>
 					       <option value="minusValue">本次变化</option>
 					       <option value="nowValue">结果值</option>
 					       <option value="changeRate">速率变化</option>
 					   </select>
					   <div>
					    <label>时间选择: </label>  <select  id="time" multiple="multiple"   name="time"     >
 					  </select>
 					    </div>
 					  <label style="display:none;">时间: </label>  
 					  <div style="display:none;">
 					  <input id="startTime"   filter="ge" name="collectTime" class="form-control layer-date" placeholder="YYYY-MM-DD" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm'})"    />
					   <input id="endTime"   filter="le" name="collectTime" class="form-control layer-date" placeholder="YYYY-MM-DD" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm'})"       />
					   
					   </div>
					   </div>
					   <div class="pull-right">
                            <a onclick="load()" class="btn btn-primary btn-sm" ><i class="fa fa-arrow-circle-up"></i>&nbsp;&nbsp;查询</a>
                            <a onclick="location.href=location.href;"  class="btn btn-primary btn-sm"><i class="fa fa-remove"></i>&nbsp;&nbsp;清空</a>
  					  </div>
  					  </div>
  					  </div>
					</form>
					<div >
                        <div class="echarts" id="dataChart" style="height:600px"></div>
                    </div>
                </div>
            </div>
         </div> 
    </div>
    

	<script type="text/javascript">
	$(document).ready(function() {
		
		
		if(window.innerHeight){
			winHeight=window.innerHeight;
			}
		else if((document.body)&&(document.body.clientHeight)){
			winHeight=document.body.clientHeight;
			}
		var newH = winHeight+100;
		 $("#dataChart").css("cssText","height: "+newH+"px!important;");
	 
	});

	//载入图表
	$(function () {
   
		var today = new Date().format("yyyy-MM-dd");
		var day = new Date(new Date().getTime()-7*24*60*60*1000);
		  
		var startTime = ff.util.urlPara('startTime');
		 
		var endTime = ff.util.urlPara('endTime');
		$("#startTime").val(startTime);
		$("#endTime").val(endTime);
       	var selector= $("select[name=monitorItemId]");  
 
       	var monitorItemId = ff.util.urlPara('monitorItemId');
     	ff.form.combox.load(selector,{
    		  url: "supervise/mcproitemmonitoritem/loadall",
    	      valueField: "id",
    	      labelField: "monitorItemName",
    	      select:monitorItemId,
    		  data: {
    			  projectId: '${param.projectId}'
    		  },
    		 
    		},loadPoint);
     	selector.change(function(){
     		//loadUnit();
     		 loadPoint();
     		
     		});
     	 //$("#measuringPointCode").multiselect("nonSelectedText : '请选择'");
     	 $("#time").multiselect("nonSelectedText : '请选择'");
     	
	    
	});
	function loadPoint()
	{
		var point = ff.util.urlPara('point');
		var selector2= $("select[name=monitorItemId]");
		item = selector2.val();
		var selector3= $("select[name=measuringPointCode]");  
	 	ff.form.combox.load(selector3,{
			  url: "supervise/mcproitemmeasuringpoint/loadall",
		      valueField: "measuringPointCode",
		      labelField: "measuringPointCode",
		      select:point,
			  data: {
				  monitor_item_id: item
			  },
			 
			},loadTime);
	 	 
	}
	function loadTime()
	{
		
		 
		var selector3= $("select[name=time]");  
		var filter = ff.com.getFilterObj("#ff_filter");
		ff.util.submit({url:"supervise/mcproitemrealtimemondata/loadall",
			filter:filter,
			 
			success:function(rsp){
				var temp = rsp.obj;
				if(null!=rsp.obj && rsp.obj.length > 10)
				{
					//temp = rsp.obj.slice(0,10);
				}	
				ff.form.combox.load(selector3,{
 				      valueField: "collectTime",
				      labelField: "collectTime",
				      select:'first',
					  data:temp,
					 
					},load);
				
				$("#time").multiselect("destroy").multiselect({
		 		    includeResetDivider: true,
		  			includeResetOption: true,
		 			includeSelectAllOption: true,
		 			selectAllText: '全选',
		 			resetText:'全不选',
			         nonSelectedText : '请选择',  
			        selectedList:5,//如果多余那么就不会显示合理的选择  
		 	   });
			 }
		    }
			);
		
		
	 	
	}
	function load(data)
	{
		 
		 $("#time").multiselect("destroy").multiselect({
	 		    includeResetDivider: true,
	  			includeResetOption: true,
	 			includeSelectAllOption: true,
	 			selectAllText: '全选',
	 			resetText:'全不选',
		         nonSelectedText : '请选择',  
		        selectedList:5,//如果多余那么就不会显示合理的选择  
	 	   });
		 
		if(null != data)
		{
			ff.form.combox.load("#alarmList",{
	 		      valueField: "measuringPointCode",
			      labelField: "measuringPointCode",
			      
				  data:data,
				} );
		}	
		
		debugger;
		ff.req.filter = ff.com.getFilterObj("#ff_filter");
		
		var type = $("#valueType").val();
		 
 	    ff.util.submit({
	    	url:"supervise/mcmonitordatachart/shiftData",
            data:{dim:'collectTime',indicator:type},
            success:loadChart
            });
 	  
  
	}
	
	function loadChart(rsp)
	{
		if(rsp.obj.length == 0)
		{
			return;
		}	
		debugger;
		var temp = rsp.obj[0];
		var data = [];
		var point =   $("select[name=time]").val();  
		
		var series = [];
		var y_name_list = [];
		for(var i=0;i<rsp.obj.length;i++)
		{
			if($.inArray(rsp.obj[i].x_name, point) != -1)
			{
				y_name_list.push(rsp.obj[i].x_name);
				var obj =  {
			            name:rsp.obj[i].x_name,
			            type:'line',
			            smooth:true,
			            itemStyle: {
			                normal: {
			                    lineStyle: {
			                        shadowColor : 'rgba(0,0,0,0.4)'
			                    }
			                }
			            },
			            data: rsp.obj[i].x_data
			        };
				
				series.push(obj);
			};
			
		}	

		 var option   = {
				    title:{text:"曲线图",left:'center'},
				    legend: {
				        data:y_name_list,
				        y:'bottom'
				    },
				    toolbox: {
				        show : false,
	 			    },
				    calculable : true,
				    tooltip : {
				        trigger: 'item',
				        formatter:   function(a,b,c){
				        	 //alert(ff.util.objToJson(a));
				        	var val =parseFloat(a.value).toFixed(2);
				        	return "时间：" +a.seriesName+"<br>"+"深度: " + a.name +"<br>"+"数值 : " + val ;
				        }//"累计值 :{c} <br/> 深度: {b}"
				    },
				    xAxis : [
				        {
				        	name:"数值 (mm)",
				        	precision: 1, 
				        	position:'top',
				        	//inverse:true,
				            type : 'value',
	 			        }
				    ],
				    yAxis : [
				        {
				        	 
				        	name:"深度(m)",
				        	position:'right',
				            type : 'category',
				            inverse:true,
				            axisLine : {onZero: false},
	 			            boundaryGap : false,
				            data : temp.y_data,
				        }
				    ],
				    series :series
				};
			var chart =  echarts.init($("#dataChart")[0]);
			chart.setOption(option,true);      
		//ff.chart.load("#dataChart","监测数据成果曲线图",temp,"line", option);
	}
	
	</script>
</body>

</html>