<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>巡检计划列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="巡检计划">
<grid:grid id="lawCheckPlanGridId" url="${adminPath}/law/lawcheckplan/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="计划名称"  name="planName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="路线名称"  name="routeName" />
    <grid:column label="巡检人员名称"  name="userName" />
    <grid:column label="计划次数"  name="planCount" />
    <grid:column label="完成次数"  name="completeCount" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>