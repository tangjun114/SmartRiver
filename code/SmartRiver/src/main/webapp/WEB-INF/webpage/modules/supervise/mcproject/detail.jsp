<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>项目详情</title>
    <meta name="decorator" content="single"/>
    <style type="text/css">
		.pro-item-1 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: #e0e2e5
		}
		.pro-item-2 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: transparent
		}
</style>
</head>

<body class="gray-bg" >

    <div class="animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet box  portlet-blue">
					 <div class="portlet-header">
						<div class="caption">项目详细介绍</div>
						<div class="tools">
							<input  class="btn  btn-success" style="dispaly:none;" type="button" onclick="showModify()" value="编辑" />
	                     </div>
					</div>	
					<div class="portlet-body" id="project_content">
					        <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>项目名称</strong></div>
                                <div class="col-sm-9" >
                                	${detail.name}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>项目简称</strong></div>
                                <div class="col-sm-9" >
                                ${detail.simpleName}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label" ><strong>项目编号</strong></div>
                                <div class="col-sm-9" >
                                	${detail.code}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label"><strong>项目类别</strong></div>
                                <div class="col-sm-9">
                                    ${detail.projectType}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>监督单位</strong></div>
                                <div class="col-sm-9">
                                    ${detail.monitorOrgName}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>监督人员</strong></div>
                                <div class="col-sm-9">
                                    ${detail.monitorByName}
                                </div>
                            </div>	
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label" ><strong>合同额(W)</strong></div>
                                <div class="col-sm-9" >
                                	${detail.contractLimit}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label"><strong>合同类型</strong></div>
                                <div class="col-sm-9">
                                    ${detail.contractDype}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>项目负责人</strong></div>
                                <div class="col-sm-9">
                                    ${detail.personInChargeName}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>支护形式</strong></div>
                                <div class="col-sm-9">
                                    ${detail.protectType}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>开工日期</strong></div>
                                <div class="col-sm-9">
                                <script type="text/javascript">
								    var fd = formatDateTime('${detail.startDate}');
									document.write(fd);
								 
								</script>
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>开挖面积</strong></div>
                                <div class="col-sm-9">
                                    ${detail.area}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>开挖深度</strong></div>
                                <div class="col-sm-9">
                                    ${detail.depth}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>安全等级</strong></div>
                                <div class="col-sm-9">
                                    ${detail.level}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>备注</strong></div>
                                <div class="col-sm-9">
                                    ${detail.remarks}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>建设单位</strong></div>
                                <div class="col-sm-9">
                                    ${detail.buildUnit}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>设计单位</strong></div>
                                <div class="col-sm-9">
                                    ${detail.designUnit}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>总包单位</strong></div>
                                <div class="col-sm-9">
                                    ${detail.mainUnit}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>委托单位</strong></div>
                                <div class="col-sm-9">
                                    ${detail.entrustUnit}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>委托单位地址</strong></div>
                                <div class="col-sm-9">
                                    ${detail.entrustAddr}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>支护施工单位</strong></div>
                                <div class="col-sm-9">
                                    ${detail.implementUnit}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>总包单位</strong></div>
                                <div class="col-sm-9">
                                    ${detail.supervisingUnit}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>详细地址</strong></div>
                                <div class="col-sm-9">
                                   <a href="javascript:void(0)" onclick="showMap()"><i class="glyphicon glyphicon-map-marker"> </i>${detail.fullAddress}</a>
                                </div>
                            </div>	 
                            <div class="form-group col-sm-6 pro-item-2">
                                <div class="col-sm-3 control-label"><strong>项目概况</strong></div>
                                <div class="col-sm-9">
                                   ${detail.note}
                                </div>
                            </div>	
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>监测频率简述</strong></div>
                                <div class="col-sm-9">
                                   ${detail.frequencyDesc}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1">
                                <div class="col-sm-3 control-label"><strong>测点保护措施</strong></div>
                                <div class="col-sm-9">
                                   ${detail.pointProtect}
                                </div>
                            </div>	                           				        
					</div>
            </div>
         </div> 
    </div>
</div>

<html:js name="ff,layer,func" />
<script>
var winHeight;
	$(document).ready(function () {
		if(window.innerHeight){
			winHeight=window.innerHeight;
			}
		else if((document.body)&&(document.body.clientHeight)){
			winHeight=document.body.clientHeight;
			}
		var newH = winHeight-70;
	
		$("#project_content").css({
			"height":newH+"px",
			"overflow-y":"scroll"});
	});

	function showMap(){
		var lat = '${detail.latitude}';
		var lng = '${detail.longitude}';
		var url = "${staticPath}/map/bdmap.html?lat="+lat+"&lng="+lng;
		top.layer.open({
			  type: 2,
			  title: false,
			  area: ['800px', '500px'],
			  closeBtn: false,
			  shadeClose: true,
			  btn: ['关闭'],
			  content: url,
			  success: function(layero){

				  }
			}); 
	}
	function showModify() {
		var id = '${detail.id}';
		var url = "${adminPath}/supervise/mcproject/"+id+"/update";
		top.layer.open({
			type : 2,
			area: ['800px', '500px'],
			title : '编辑',
			maxmin : false, // 开启最大化最小化按钮
			content : url,
			btn : [ '确定', '关闭' ],
			yes : function(index, layero) {
  				var body = top.layer.getChildFrame('body', index);
				var iframeWin = layero.find('iframe')[0]; // 得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
				// 文档地址
				// http://www.layui.com/doc/modules/layer.html#use
				iframeWin.contentWindow.doSubmit(top, index, function() {
					// 刷新表单
					 //refreshTable('contentTable');
					top.layer.close(index);
				}); 
				
				return true;

			},
			cancel : function(index) {
			}
		});
	}
	
</script>
</body>
</html>