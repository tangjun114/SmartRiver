<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>项目进度列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="项目进度">
<grid:grid id="srProjectScheduleGridId" url="${adminPath}/project/srprojectschedule/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="项目名称" name="proName"/>
    <grid:column label="标题" name="name"/>
    <grid:column label="截止日期" name="deadTime"/>
    <grid:column label="描述" name="desc"/>
    <grid:column label="备注信息" name="remarks"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>
    <grid:button title="子进度管理" groupname="opt" function="rowDialogDetailRefresh" outclass="btn-primary"
                  innerclass="fa-plus" url="${adminPath}/project/srprojectscheduledetail/?scId=\"+row.id+\""/>
    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
    <grid:toolbar function="exportFile"/>
</grid:grid>
</body>
</html>