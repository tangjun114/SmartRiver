<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title><spring:message code="sys.user.title" /></title>
  <meta name="decorator" content="list"/>
  <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
</head>
<body title="<spring:message code="sys.user.title" />">
<div class="row">
	<div class="col-sm-3 col-md-2" >
	   <view:treeview id="organizationTreeview" treeviewSettingCallback="callback"  dataUrl="${adminPath}/sys/organization/bootstrapTreeData" onNodeSelected="organizationOnclick"></view:treeview>
	    <script type="text/javascript">
	    function callback(){
	     	 
 	    	var tree = $('#organizationTreeview').treeview(true) ;
	    	var node = tree.getSelected();
	    	if(null != node && node.length != 0)
	    	{
	    		$("input[name='organizationid']").val(node[0].href);
	    		var item = {"id":node[0].href,"name":node[0].text};
			    ff.cache.set("organization",item);
	    		setTimeout("search('userGridIdGrid')",500);
	    	}	
 	    	 
	    };
	       function organizationOnclick(event, node) {
	    	   //查询时间
	    	   //gridquery隐藏 查询标签概念，query,单独的query
	    	   $("input[name='organizationid']").val(node.href);
	    	   search('userGridIdGrid');
	    	   debugger;
		   	   var item = {"id":node.href,"name":node.text};
		   	   ff.cache.set("organization",item);
	       }
	   </script>
	</div>
	<div  class="col-sm-9 col-md-10">
		<grid:grid id="userGridId" url="${adminPath}/sys/user/ajaxList">
			<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
			<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
			<grid:button groupname="opt" function="delete"/>
			<grid:query name="organizationid"  queryMode="hidden" />
		    <grid:column label="sys.user.realname"  name="realname"  query="true"  condition="like" />
		    <grid:column label="sys.user.username"  name="username"  query="true" condition="like"   />
		    <grid:column label="sys.user.email"  name="email"  />
		    <grid:column label="sys.user.phone"  name="phone"  />
		    <grid:column label="人员类型"  name="type" dict="projectMemberType" />
			<grid:toolbar title="sys.user.createuser" onclick="createUser()"  url="${adminPath}/sys/user/create?projectId=${projectId}"/>
		 	<grid:toolbar title="sys.user.updateuser" onclick="updateUser()"  />
			<grid:toolbar title="sys.user.modifypwd" icon="fa-database"  function="updateDialog" url="${adminPath}/sys/user/{id}/changePassword"  />
			<grid:toolbar function="delete"/>
			<grid:toolbar  function="search"/>
			<grid:toolbar  function="reset"/>
		</grid:grid>
	</div>
</div>
	    <script type="text/javascript">
		   var winHeight;
		   $(document).ready(function()   
		   {  
		   	//获取窗口高度 兼容各个浏览器
		   	if(window.innerHeight){
		   		winHeight=window.innerHeight;
		   		}
		   	else if((document.body)&&(document.body.clientHeight)){
		   		winHeight=document.body.clientHeight;
		   		}

		   	var newH = winHeight-250;
		   	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
		   }); 
	       function createUser()
	       {
	    	   debugger;
	    	   var url = "${adminPath}/sys/user/create?orgId=" +$("input[name='organizationid']").val();
	    	   create("创建用户",url,'userGridIdGrid',"800px","500px");
	       }
	       function updateUser()
	       {
	    	   debugger;
	    	   var url = "${adminPath}/sys/user/{id}/update?orgId=" +$("input[name='organizationid']").val();
	    	   update("修改用户",url,'userGridIdGrid',"800px","500px");
	       }
	   </script>
</body>
</html>