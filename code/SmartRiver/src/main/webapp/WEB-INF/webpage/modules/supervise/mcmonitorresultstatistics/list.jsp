<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>最新监测结果</title>
    <meta name="decorator" content="single"/>
    <link rel="stylesheet" href="${staticPath}/vendors/bootstrap/js/bootstrap-table.css?v=1.0.7" type="text/css" media="screen" />
        <style type="text/css">
		.color_warn {
			color: #FCB247;
		}
		.color_caution {
			color: red;
		}
		.color_control {
			color: #993366;
		}	
		.gly_green{font-size:20px;color:green;text-shadow:#CCC 2px 2px 2px;}
		.gly_yellow{font-size:20px;color:orange;text-shadow:#CCC 2px 2px 2px;}
		.gly_red{font-size:20px;color:red;text-shadow:#CCC 2px 2px 2px;}
		.gly_navy{font-size:20px;color:#993366;text-shadow:#CCC 2px 2px 2px;}		
	</style>
</head>

<body  >
    <div class="tab-content" style="overflow-y:hidden;">
        
             <table id="sumData"  class='table table-hover'>
             
             </table>
		 
    </div>
    
	<!-- 全局js -->
	<html:js name="echarts" />
	 <html:js name="ff,layer,bootstrap,bootstrap-table" />
   
	<script type="text/javascript">
 

	//载入图表
	$(function () {
   
		ff.util.submit({
			url: "supervise/mcproitemrealtimemondata/sum?projectId=${param.projectId}",
			data:{projectId:"${param.projectId}"},
            success:function(rsp)
            {
            	loadTable(rsp.obj);
            }
		});
     
	    
	});
	function loadTable(data)
	{
		  var dataNew = [];
		  debugger;
		  if(null != data)
		  {
			  for(var i=0;i<data.length;i++)
			  {
				  var temp = data[i];
				  dataNew.push(temp);
				  var temp1 = ff.util.copy(temp);
				  temp1.minName = temp.maxName
				  temp1.minValue = temp.maxValue
				  temp1.minPoint = temp.maxPoint
				  dataNew.push(temp1);
				  
				  var temp2 = ff.util.copy(temp);
				  temp2.minName = temp.rateName
				  temp2.minValue = temp.rateValue
				  temp2.minPoint = temp.ratePoint
				  
				  temp2.alarmValue = temp.rateAlarmValue
				  temp2.controlValue = temp.rateControlValue
				  dataNew.push(temp2);
			  }
		  }	  
		  
		  $("#sumData").bootstrapTable({
			    columns: [{
			        field: 'monitorItemName',
			        title: '监测项名称',
			         formatter: nameFormat
			    }, {
			        field: 'minName',
			        title: '最大值'
			    }, {
			        field: 'minValue',
			        title: '数值',
			       
			    }, {
			        field: 'minPoint',
			        title: '测点编号'
			    }, {
			        field: 'alarmValue',
			        title: '报警值'
			    }, {
			        field: 'controlValue',
			        title: '控制值'
			    }, {
			        field: 'alarmPoint',
			        title: '报警测点'
			    }, {
			        field: 'alarmStatus',
			        title: '安全状态',
			        formatter: alarmTypeFormat
			    }],
			    data: dataNew

	      });
		  for(var i=0;i<data.length;i++)
		  {
			  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'monitorItemName', colspan: 0, rowspan:3});
			  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'alarmValue', colspan: 0, rowspan:2});
			  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'controlValue', colspan: 0, rowspan:2});
		 	  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'alarmPoint', colspan: 0, rowspan:3});
			  $('#sumData').bootstrapTable('mergeCells', { index: i*3, field: 'alarmStatus', colspan: 0, rowspan:3});

		  }	  
		  
	}
	function nameFormat(value, row, index)
	{
		var href="<a href=\"#\"";
        href +="onclick=\"view('"+value+"')\"";
				href +="style='color:blue;font-weight:bold' >&nbsp"+value+"</a>&nbsp&nbsp";
 		//var url = "${adminPath}"+"/supervise/mcproitemrealtimemondata?projectId=${projectId}";
 		 
		 
	    return href;
	}
	function view(name){
  		var url = "${adminPath}"+"/supervise/mcproitemrealtimemondata?projectId=${projectId}";
  		ff.cache.set("monitorItemName",name);
		var width = "1000px";
		var height = "600px";
		
		if (navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)) {// 如果是移动端，就使用自适应大小弹窗
			width = 'auto';
			height = 'auto';
		}  
		
		url = encodeURI(url)
		 //window.open(url,"project");
		 top.layer.open({
		 	type : 2,
		 	area : [ width, height ],
		 	title : "成果管理",
		 	maxmin : false, // 开启最大化最小化按钮
		 	content : url,
		 
		 	cancel : function(index) {
		 	}
		  });
	}
	function alarmTypeFormat(value, row, index){
		 var alarmTypeId;
		 
	 
		         if (row.alarmStatus == '超控'){
		        	 alarmTypeId = '3';
		         } else if (row.alarmStatus == '报警') {
		        	 alarmTypeId = '2';
		         } else if (row.alarmStatus == '预警') {
		        	 alarmTypeId = '1';
		         } else {
		        	 alarmTypeId = '0';
		         }
		              
		   return ff.service.alarm(alarmTypeId);;
	}
	</script>
</body>

</html>