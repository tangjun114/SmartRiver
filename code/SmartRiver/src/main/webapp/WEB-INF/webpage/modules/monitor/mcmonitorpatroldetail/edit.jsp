<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>巡检详情</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcMonitorPatrolDetailForm">
    <form:form id="mcMonitorPatrolDetailForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="patrolId"/>
		<form:hidden path="patrolName"/>
		<form:hidden path="projectId"/>
		<form:hidden path="projectName"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
					<tr>
				 
					<td  class="width-15 active text-right">	
		              <label>巡检描述:</label>
		            </td>
					<td class="width-35">
						<form:input path="detailName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>有无异常:</label>
		            </td>
					<td class="width-35">
						<form:input path="abnormalResult" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>异常说明:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="abnormalDesc" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					
				</tr>

				<tr>
					<td  class="width-15 active text-right">	
		              <label>图片:</label>
		            </td>
					<td colspan="4" class="width-35"><form:fileinput
							showType="file" extend="jpg,png,gif" maxFileCount="10"
							fileInputWidth="100px" fileInputHeight="100px" path="abnormalImgs"
							htmlEscape="false" class="form-control" saveType="filepath" idField="id"/> <label
						class="Validform_checktip"></label></td>
				</tr>

			</tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />

<script type="text/javascript">  
$(document).ready(function () {
/* 		//var imgs = $("#abnormalImgs").val();
		var content = $('#previewImgs');
		var newHtml = '';
		var result= imgs.split(",");
		for(var i=0;i<result.length;i++){
			newHtml +="<div  class=\"thumbnail\"><img src=\""+
			 rootUrl() + "/" + result[i]+"\" alt=\"图片无法显示\"></div>";

		}
		//alert(newHtml);
		content.html(newHtml); */
	
});

</script>
</body>
</html>