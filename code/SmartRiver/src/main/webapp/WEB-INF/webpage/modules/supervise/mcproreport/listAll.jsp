<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>报告列表列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="报告列表">
<grid:grid id="mcProReportGridId" url="${adminPath}/supervise/mcproreport/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.path" hidden="true"  name="reportPath"  />
	
	<grid:column label="报告名称"  name="reportName"  />
	<grid:column label="项目名称"  name="projectId"  hidden="true"/>
	<grid:column label="项目编号"  name="projectCode"  />
	<grid:column label="项目名称"  name="projectName"  />
	
	<grid:column label="报告生成时间"  name="createDate" />
	<grid:column label="报告生成人员"  name="createByName" />
	
	<grid:column label="类型"  name="reportType"  query="true"  queryMode="select"  condition="eq"  dict="pro_report_type" /> 
    
    <grid:column label="状态" hidden="true" name="status" dict="report_status" />
    <grid:column label="状态" name="statusBtn" formatter="button"/>

    <grid:column label="校核人员"  name="checker" />
	<grid:column label="校核时间"  name="checkTime" />
	
	<grid:column label="审核人员"  name="approver" />
	<grid:column label="审核时间"  name="approveTime" />
	<grid:column label="操作"  name="myOpt" formatter="button" width="200"/>

	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
	
</grid:grid>
<html:js name="func" />
<html:js name="ff" />
<script type="text/javascript">
var winHeight;
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-270;
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});
function mcProReportGridIdStatusBtnFormatter(value, options, row){
	var href = '';
		try{
	         if(!row.id) {
          return '';
      	}
		debugger;
	    href="<a href=\"#\"";
	    
	    if(row.status =='to_check'){
	    	href +="onclick=\"checkReport('"+row.id+"')\"";
	    	href +=">&nbsp待校核</a>";
	    }
	    else if(row.status =='to_approve'){
	    	href +="onclick=\"checkReport('"+row.id+"')\"";
			href +=">&nbsp待审核</a>";
		}
		else if(row.status =='approved') {
			href +=">&nbsp已审核</a>";
		} else {
			href +=">&nbsp</a>";
		}
	   }catch(err){}
	   
	   return href;
}
function mcProReportGridIdMyOptFormatter(value, options, row){
	  try{
	         if(!row.id) {
               return '';
           }
	         debugger;
	    	 var href="";
	    	 
	    	 var url = rootUrl() + row.reportPath;
	               		href +="<a href=\"#\" class=\"btn btn-xs btn-success\" class=\"btn btn-xs \"";
				           href +="onclick=\"downloadFile('下载','"+url+"')\"";
				           href +="><i class=\"fa fa-download\"></i>&nbsp下载</a>&nbsp&nbsp";
	               		href +="<a href=\"#\" class=\"btn btn-xs btn-danger\" class=\"btn btn-xs \"";
				               		  href +="onclick=\"deleteRowData('删除','${adminPath}/supervise/mcproreport/{id}/delete','"+row.id+"','"+options.gid+"')\"";
				           href +="><i class=\"fa fa-trash\"></i>&nbsp删除</a>&nbsp&nbsp";
			return href;
	   }catch(err){}
	   return value;
	}
function checkReport(id) {
	ff.util.submit({
 		url:"supervise/mcproreport/check",
		data:id,
		success:function(rsp){
			search('mcProReportGridIdGrid');

		}
	});
}

</script>
</body>
</html>