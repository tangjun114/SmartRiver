<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>库存管理列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="库存管理">
<grid:grid id="floodAssetsStockGridId" url="${adminPath}/flood/floodassetsstock/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="资产代码" name="assetsCode"/>
    <grid:column label="资产名称" name="assetsName" query="true" queryMode="input" condition="like"/>
    <grid:column label="库存数量" name="stockQty"/>
    <grid:column label="单位" name="unit"/>
    <grid:column label="库存点名称" name="stockLocation" query="true" queryMode="select" condition="eq"
                 dict="flood_stock_location"/>
    <grid:toolbar title="入库" function="create" url="${adminPath}/flood/floodassetsstock/create?type=0"/>
    <grid:toolbar title="移库" function="update" url="${adminPath}/flood/floodassetsstock/{id}/update?type=1"/>
    <grid:toolbar title="出库" function="update" url="${adminPath}/flood/floodassetsstock/{id}/update?type=2"/>


    <grid:toolbar function="search"/>
    <grid:toolbar function="exportFile"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>