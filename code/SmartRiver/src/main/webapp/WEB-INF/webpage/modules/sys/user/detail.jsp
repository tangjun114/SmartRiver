<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>项目详情</title>
    <meta name="decorator" content="single"/>
    <style type="text/css">
		.pro-item-1 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: #e0e2e5
		}
		.pro-item-2 {
			padding: 10px;
			font-weight: normal;
			margin-bottom: 0px;
			background-color: transparent
		}
</style>
</head>

<body class="gray-bg" >

    <div class="animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet box  portlet-blue">
					 <div class="portlet-header">
						<div class="caption">基本信息</div>
						<div class="tools">
							<input  class="btn  btn-success" style="dispaly:none;" type="button" onclick="showModifyUser()" value="编辑" />
	                     </div>
					</div>	
					<div class="portlet-body" id="project_content">
					        <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>用户名</strong></div>
                                <div class="col-sm-9" >
                                	${data.username}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>真实姓名</strong></div>
                                <div class="col-sm-9" >
                                ${data.realname}
                                </div>
                            </div>
                             <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label" ><strong>邮箱</strong></div>
                                <div class="col-sm-9" >
                                	${data.email}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-2" >
                                <div class="col-sm-3 control-label"><strong>电话</strong></div>
                                <div class="col-sm-9">
                                    ${data.phone}
                                </div>
                            </div><div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label" ><strong>部门</strong></div>
                                <div class="col-sm-9" >
                                	${data.deptName}
                                </div>
                            </div>
                            <div class="form-group col-sm-6 pro-item-1" >
                                <div class="col-sm-3 control-label"><strong>职位</strong></div>
                                <div class="col-sm-9">
                                    ${data.positionName}
                                </div>
                            </div>
                                                 				        
					</div>
            </div>
         </div> 
    </div>
</div>

<html:js name="ff,layer,func" />
<script>
	$(document).ready(function () {
		var newH = $(document).height()-58;
	
		$("#project_content").css({
			"height":newH+"px",
			"overflow-y":"scroll"});
	});

	 
	function showModifyUser() {
		var id = '${data.id}';
		var url = "${adminPath}/sys/user/"+id+"/update";
		top.layer.open({
			type : 2,
			area: ['800px', '500px'],
			title : '编辑',
			maxmin : false, // 开启最大化最小化按钮
			content : url,
			btn : [ '确定', '关闭' ],
			yes : function(index, layero) {
  				var body = top.layer.getChildFrame('body', index);
				var iframeWin = layero.find('iframe')[0]; // 得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
				// 文档地址
				// http://www.layui.com/doc/modules/layer.html#use
				iframeWin.contentWindow.doSubmit(top, index, function() {
					// 刷新表单
					 //refreshTable('contentTable');
					top.layer.close(index);
				}); 
				
				return true;

			},
			cancel : function(index) {
			}
		});
	}
	
</script>
</body>
</html>