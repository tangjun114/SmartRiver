<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>警报图</title>
    <meta name="decorator" content="single"/>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div >
                <div class="portlet box  portlet-grey">
					<div >
						<h5></h5>
						<form class="form-inline">
							<div id="distpicker5">
								<div class="form-group">
									<label class="sr-only" for="province10">Province</label> <select
										class="form-control" id="province10"></select>
								</div>
								<div class="form-group">
									<label class="sr-only" for="city10">City</label> <select
										class="form-control" id="city10"></select>
								</div>
								<div class="form-group">
									<label class="sr-only" for="district10">District</label> 
									<select
										class="form-control" id="district10"></select>
								</div>
								<div class="form-group">
            						<button class="btn btn-primary" id="search" type="button" onclick="searchByArea()">查询</button>

          						</div>
							</div>
						</form>
					</div>
					<div class="col-sm-12">
						<div class="col-sm-6">
                        	<div class="echarts" id="echarts-bar-chart" style="height:400px"></div>
                    	</div>
						<div class="col-sm-6">
                        	<div class="echarts" id="echarts-pie-chart" style="height:400px"></div>
                    	</div>
					</div>
                </div>
            </div>
         </div> 
    </div>
    
	<!-- 全局js -->
<html:js name="echarts" />
	 
<html:js name="ff" />
<script type="text/javascript">
var winHeight;
$(window).resize(function() {
	if(window.innerHeight){
		winHeight = window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight = document.body.clientHeight;
		}
	var newH = winHeight-80;
	$("#echarts-bar-chart").css("cssText","height: "+newH+"px!important;");
	$("#echarts-pie-chart").css("cssText","height: "+newH+"px!important;");

	 searchByArea();
});
	function searchByArea() {
		var selProvince = $('#province10 option:selected').val();
		var selCity = $('#city10 option:selected').val();
		var selDistrict = $('#district10 option:selected').val();
		
		ff.util.submit({
			url:"supervise/mcprojectstatistics/by",
			data:'area',
			filter:{
				province:selProvince,
				city: selCity,
				district:selDistrict
			},
			success:function(rsp){
				if(null != rsp && rsp.obj)
				{	
			     	ff.chart.load("#echarts-bar-chart","",rsp.obj,"bar");	 
					ff.chart.load("#echarts-pie-chart","区域项目统计",rsp.obj,"pie");
					
				}
			}
		
		});
	}
	
</script>
<script src="${staticPath}/vendors/dist/js/distpicker.data.js"></script>
<script src="${staticPath}/vendors/dist/js/distpicker.js"></script>
<script src="${staticPath}/vendors/dist/js/main.js"></script>

</body>

</html>