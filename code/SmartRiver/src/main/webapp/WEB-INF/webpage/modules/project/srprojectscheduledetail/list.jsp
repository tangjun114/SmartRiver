<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>进度拆分管理列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="进度拆分管理">
<grid:grid id="srProjectScheduleDetailGridId" url="${adminPath}/project/srprojectscheduledetail/ajaxList?scId=${scId}">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="140"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="描述" name="desc"/>
    <grid:column label="截止日期" name="deadTime"/>
    <grid:column label="所占比例 %" name="rate"/>
    <grid:column label="完成状态" name="complete" dict="project_schedule_complete"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:column label="备注信息" name="remarks"/>
    <grid:toolbar function="create" url="${adminPath}/project/srprojectscheduledetail/create?scId=${scId}"/>
    <grid:button title="已完成" groupname="opt" function="updateStatus" outclass="btn-primary" tipMsg="确认审核通过吗"
                 innerclass="fa-plus"
                 url="${adminPath}/project/srprojectscheduledetail/{id}/update?complete=1"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>