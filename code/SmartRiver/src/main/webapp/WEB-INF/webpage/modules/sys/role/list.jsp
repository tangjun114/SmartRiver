<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title><spring:message code="sys.role.title" /></title>
  <meta name="decorator" content="list"/>
  <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
</head>
<body title="<spring:message code="sys.role.title" />">
<div class="row">
<div class="col-sm-3 col-md-2" >
        
	   <view:treeview id="organizationTreeview" treeviewSettingCallback="callback" dataUrl="${adminPath}/sys/organization/bootstrapTreeData" onNodeSelected="orgOnclick"></view:treeview>
	    <script type="text/javascript">
	    function callback(){
     	 
 	    	var tree = $('#organizationTreeview').treeview(true) ;
	    	var node = tree.getSelected();
	    	if(null != node && node.length != 0)
	    	{
	    		$("input[name='orgId']").val(node[0].href);
	    		var item = {"id":node[0].href,"name":node[0].text};
			    ff.cache.set("organization",item);
	    		setTimeout("search('roleGridIdGrid')",500);
	    	}	
 	    	 
	    };
	       function orgOnclick(event, node) {
	    	   //查询时间
	    	   //gridquery隐藏 查询标签概念，query,单独的query
	    	   
	    	
	    	   $("input[name='orgId']").val(node.href);
	    	   search('roleGridIdGrid');
	    	   debugger;
		   	   var item = {"id":node.href,"name":node.text};
		   	 
			 
				ff.cache.set("organization",item);
	       }
	   </script>
	</div>
	<div  class="col-sm-9 col-md-10">
<grid:grid id="roleGridId" url="${adminPath}/sys/role/ajaxList">
    <div>
       <form:hidden path="id" nested="false" />
        <form:input path="nametest" nested="false" />
    </div>
    <grid:query name="orgId"  queryMode="hidden" />
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button title="sys.role.authMenu"  groupname="opt" function="updateObj" outclass="btn-info" winwidth="300px" innerclass="fa-plus" url="${adminPath}/sys/role/authMenu" />
	<grid:button   groupname="opt" function="delete" />
    <grid:column label="sys.role.name"  name="name"  query="true"  />
    <grid:column label="sys.role.code"  name="code"  query="true"  />
    <grid:column label="sys.role.isSys"  name="isSys"  dict="sf" />
    <grid:column label="sys.role.usable"  name="usable"  dict="sf"/>

	<grid:toolbar function="create"/>
	<grid:toolbar   function="update"/>
	<grid:toolbar   function="delete"/>
	
	<grid:toolbar  function="search"/>
	<grid:toolbar  function="reset"/>
</grid:grid>
</div>
</div>
</body>
</html>