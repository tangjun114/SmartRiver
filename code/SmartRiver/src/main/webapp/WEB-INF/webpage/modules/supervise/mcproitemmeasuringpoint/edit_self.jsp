<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目监控-测点设置-监测项-测点</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor,jqgrid" />
    <html:css  name="font-awesome,animate,iCheck,datepicker,sweetalert,Validform"/>
    
    <link href="${staticPath}/common/css/style.css?v=4.1.0" rel="stylesheet">
 </head>

<body class="white-bg"  formid="mcProItemMeasuringPointForm">
    
    <form:form id="mcProItemMeasuringPointForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="depthExt" htmlEscape="false" />
		<form:hidden path="initValue"/>
		<form:hidden path="monitorItemTypeCode"/>
		<form:hidden path="monitorItemTypeName"/>
		<form:hidden path="modifyDesp"/>
		<form:hidden path="modifyFilePath"/>
		<form:hidden path="modifyFileName"/>
		<div class="panel-body">
		<div class="ibox-title"><h3>测点设置</h3></div>
		
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">修改报警值</h4>
            </div>
            <div class="modal-body" >
            	<div class="form-horizontal" >
            				<div id="alarm_val_div">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">预警值({ff_unit}):</label>
                                <div class="col-sm-9">
                                    <input type="text" id="m_warn_val" value="${data.warningValue}" class="form-control well" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">报警值({ff_unit}):</label>
                                <div class="col-sm-9">
                                    <input type="text" id="m_alarm_val" value="${data.alarmValue}" class="form-control well" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">控制值({ff_unit}):</label>
                                <div class="col-sm-9">
                                    <input type="text" id="m_control_val" value="${data.controlValue}" class="form-control well" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">速率报警值({ff_unit}/d):</label>
                                <div class="col-sm-9">
                                    <input type="text" id="m_rate_val" value="${data.rateAlarmValue}" class="form-control well" />
                                </div>
                            </div>                            
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">修改说明:</label>
                                <div class="col-sm-9">
                                <textarea id="m_modifyDesp" class="form-control" rows="4"></textarea>
								<label class="Validform_checktip"></label>
                                </div>
                            </div> 
                             <div class="form-group">
                             <label class="col-sm-3 control-label">上传文件:</label>
                                <div class="col-sm-9">
									<form:fileinput
										showType="file" maxFileCount="10"
										fileInputWidth="100px" fileInputHeight="100px" path="modifyFilePath"
										htmlEscape="false" class="form-control" saveType="filepath" idField="id"/> <label
									class="Validform_checktip"></label>

                                </div>
                            </div>

                 </div>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="modifyAlarm();">提交更改</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
 
				<tr class="t_com">
					<td  class="width-15 active text-right">	
		              <label>监测项:</label>
		            </td>
					<td class="width-35">
						<form:gridselect gridId="mcProItemMonitorItemGridId" title="监测项" nested="false"
						   path="monitorItemId" gridUrl="${adminPath}/supervise/mcproitemmonitoritem" value="${data.monitorItemId}"
						   labelName="monitorItemName" labelValue="${data.monitorItemName}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="monitorItemName"
						   callback="mcProItemMonitorItemDataPicker" 
						   htmlEscape="false" class="form-control" readonly="true" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
					 <td  class="width-15 active text-right">	
		              <label  >调试模式:</label>
		            </td>
					<td class="width-35"  >
						<form:select path="debug" htmlEscape="false"  class="form-control"  dict="sf"      />
						<label class="Validform_checktip"></label>
						<a href="#" id="debugTooltip" data-toggle="tooltip" data-placement="bottom"  data-html="true" title="<label align='left' >1、调试模式下不做任何报警处理 2、调试模式下可以随意删除测点3、非调试模式下不能切换为调试模式 <br>4、调试模式切换到非调试模式会清楚现有数据 <br>5、调试模式下不能继承测点<br> 6、请及时关闭调试模式</label>">说明</a>
					</td>
				</tr>
				
				
				<tr class="t_com">
				   
					<td  class="width-15 active text-right">	
		              <label>测点编号:</label>
		            </td>
					<td class="width-35"  >
						<form:input path="measuringPointCode" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
			 
 
					<td  class="width-15 active text-right">	
		              <label>设备类型:</label>
		            </td>
					<td class="width-35">
					   <form:select path="deviceTypeId" htmlEscape="false" class="form-control" onchange="deviceTypeChange();"     />
 					   <form:hidden path="deviceTypeName" htmlEscape="false" class="form-control"  />
 					</td>
 				</tr>
				 <tr class="t_com">
 
					<td  class="width-15 active text-right">	
		              <label>设备型号:</label>
		            </td>
					<td class="width-35">
					   <form:input path="deviceModelName" htmlEscape="false" class="form-control" readonly="true"     />
 					   <form:hidden path="deviceModel" htmlEscape="false"  class="form-control"  />
 					</td>
	 
 
					<td  class="width-15 active text-right">	
		              <label>监测仪器:</label>
		            </td>
					<td class="width-35">
						<form:gridselect gridId="mcProItemDeviceGridId" title="设备" nested="false"
						   path="deviceId" gridUrl="${adminPath}/supervise/mcproitemdevice?projectId=${projectId}" value="${data.deviceId}"
						   labelName="deviceName" labelValue="${data.deviceName}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="instrumentModelName"
						   callback="mcDeviceSensorPicker" 
						   htmlEscape="false" class="form-control" readonly="true" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
		 
				</tr>
				<tr class="t_extend ">
					<td  class="width-15 active text-right">	
		              <label>继承:</label>
		            </td>
					<td class="width-35">
						<form:select path="inheriCheck" htmlEscape="false" class="form-control"  dict="sf"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>继承名:</label>
		            </td>
					<td class="width-35">
						<form:input path="inheritName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr id="dataUploadUnit" class="t_extend">
			     	<td  class="width-15 active text-right">	
		              <label>数据上传单元:</label>
		             </td>
					<td class="width-35">
					   <form:select path="dataUploadUnitId" htmlEscape="false" class="form-control" onchange="dataUploadUnitChange();"     />
 					   <form:hidden path="dataUploadUnitName" htmlEscape="false" class="form-control"  />
 					</td>
				</tr>

 
				<tr class="t_extend T001 T008 ">
					
					<td  class="width-15 active text-right">	
		              <label>支撑类型:</label>
		            </td>
					<td class="width-35"   colspan="3">
						<form:select path="supportType" htmlEscape="false" class="form-control" onchange="supportTypeChange();" dict ="MP_support_type"   />
						<label class="Validform_checktip"></label>
						<form:hidden path="supportTypeName"/>
					</td>
				</tr>
  
				<tr   class="t_extend  " >
					<td  class="width-15 active text-right">	
		              <label>原始数据类型:</label>
		            </td>
					<td class="width-35" colspan="3">
						<form:select path="rawDataType" htmlEscape="false" class="form-control"        />
						<label class="Validform_checktip"></label>
					</td>
				</tr>		
				
				
			<tr  id="sensorType" class="t_com T001 T008 T010 T006 T007 T011 T005" >
					<td  class="width-15 active text-right">	
		              <label>传感器类型:</label>
		            </td>
		            
					<td class="width-35">
					   <form:select path="sensorType" htmlEscape="false" class="form-control" onchange="sensorTypeChange();"     />
 					   <form:hidden path="sensorTypeName" htmlEscape="false" class="form-control"  />
 					</td>
					 
				</tr>		
 
				<tr class="t_extend T001 T008 T010 T006 T007 T011 T005" id="sensorQtyDiv">
				   
					<td  class="width-15 active text-right">	
		              <label>传感器数量:</label>
		            </td>
					<td class="width-35" colspan="3">
						<form:select id="sensorQuantity" path="sensorQuantity" htmlEscape="false" onchange="sensorNumChange();"  class="form-control"  dict="sensorQuantity"   value="1"   />
						<label class="Validform_checktip"></label>
					</td>
				</tr>

				
				<tr class="t_com sensor1 T001 T008 T010 T006 T007 T011 T005">
					<td  class="width-15 active text-right">	
		              <label>传感器1信息:</label>
		            </td>
				 
					<td class="width-35">
						<form:gridselect gridId="mcMonitorSensorGridId" title="传感器" nested="false"
						   path="sensorId1" gridUrl="${adminPath}/monitor/mcmonitorsensor" value="${data.sensorId1}" 
						   labelName="sensorNum1" labelValue="${data.sensorNum1}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="sensorNum1"
						   para=""
						   callback="mcMonitorSensorPicker" 
						   htmlEscape="false" class="form-control" readonly="true" 
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right t_extend T001 T008 T010 T006 T007 T011 ">	
		              <label>初始频率:</label>
		            </td>
					<td class="width-35 t_extend T001 T008 T010 T006 T007 T011 "  colspan="3">
						<form:input path="sensorFq1" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>

                
				<tr class="t_extend sensor2">
					<td  class="width-15 active text-right">	
		              <label>传感器2信息(与1是对角):</label>
		            </td>
				 
					<td class="width-35">
						<form:gridselect gridId="mcMonitorSensorGridId" title="传感器" nested="false"
						   path="sensorId2" gridUrl="${adminPath}/monitor/mcmonitorsensor" value="${data.sensorId2}"
						   labelName="sensorNum2" labelValue="${data.sensorNum2}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="sensorNum1"
						   callback="mcMonitorSensorPicker" 
						   htmlEscape="false" class="form-control" readonly="true" 
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>初始频率:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="sensorFq2" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr></tr>
				 <tr class="t_extend sensor3">
					<td  class="width-15 active text-right">	
		              <label>传感器3信息:</label>
		            </td>
				 
					<td class="width-35">
						<form:gridselect gridId="mcMonitorSensorGridId" title="传感器" nested="false"
						   path="sensorId3" gridUrl="${adminPath}/monitor/mcmonitorsensor" value="${data.sensorId3}"
						   labelName="sensorNum3" labelValue="${data.sensorNum3}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="sensorNum1"
						   callback="mcMonitorSensorPicker" 
						   htmlEscape="false" class="form-control" readonly="true"  
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>初始频率:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="sensorFq3" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				 <tr class="t_extend sensor4">
					<td  class="width-15 active text-right">	
		              <label>传感器4信息(与3是对角):</label>
		            </td>
				 
					<td class="width-35">
						<form:gridselect gridId="mcMonitorSensorGridId" title="传感器" nested="false"
						   path="sensorId4" gridUrl="${adminPath}/monitor/mcmonitorsensor" value="${data.sensorId4}"
						   labelName="sensorNum4" labelValue="${data.sensorNum4}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="sensorNum4"
						   callback="mcMonitorSensorPicker" 
						   htmlEscape="false" class="form-control" readonly="true"  
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>初始频率:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:input path="sensorFq4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr class="t_extend T002 T013">
					
					<td  class="width-15 active text-right t_extend T002">	
		              <label>监测点类型:</label>
		            </td>
		            <td  class="width-15 active text-right t_extend T013">	
		              <label>监测类型:</label>
		            </td>
					<td class="width-35"   colspan="3">
						<form:select path="measuringPointType" htmlEscape="false" class="form-control" onchange="measuringPointTypeChange();" dict ="0"   />
						 <form:hidden path="measuringPointTypeName" htmlEscape="false" class="form-control"  />
						
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr class="t_extend T005 T003">
					
					<td  class="width-15 active text-right t_extend T005">	
		              <label>测孔深度(mm):</label>
		            </td>
		            <td  class="width-15 active text-right t_extend T003">	
		              <label>基点偏移值:</label>
		             </td>
					<td class="width-35"   colspan="3">
						<form:input path="pointHoleDepth" htmlEscape="false" class="form-control"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr class="t_extend T005 T007">
					
					<td  class="width-15 active text-right">	
		              <label>传感器深度(mm):</label>
		            </td>
					<td class="width-35"    >
						<form:input path="sensor1Depth" htmlEscape="false" class="form-control"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>管口高程(mm):</label>
		            </td>
					<td class="width-35"   colspan="3">
						<form:input path="pipeRangeGkgc" htmlEscape="false" class="form-control"  />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
			   
			   
		 
				<tr class="t_extend" >
					<td  class="width-15 active text-right">	
		              <label>极坐标方向:</label>
		            </td>
					<td class="width-35" colspan="3">
						<form:select path="polarCoordinates" htmlEscape="false" class="form-control"  dict="polarCoordinates"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
                <tr class="t_extend T014">
					
					<td  class="width-15 active text-right">	
		              <label>裂缝位置:</label>
		            </td>
					<td class="width-35"    >
						<form:input path="rupturePosition" htmlEscape="false" class="form-control"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>裂缝走向:</label>
		            </td>
					<td class="width-35"   colspan="3">
						<form:input path="ruptureDirection" htmlEscape="false" class="form-control"  />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
 
				
				<tr   class="t_extend section_para help_para" >
					<td  class="width-15 active text-right t_extend T002">	
		              <label>断面起点:</label>
		            </td>
		            <td  class="width-15 active text-right t_extend T013">	
		              <label>辅助点选择:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:select path="sectionStart" htmlEscape="false" class="form-control"  onchange="sectionStartChange()"   />
						<form:hidden path="sectionStartName" htmlEscape="false"  class="form-control"  />
						<label class="Validform_checktip"></label>
					</td>
				</tr> 
				<tr  class="t_extend T012 help_para">
			     	<td  class="width-15 active text-right t_extend T012">	
		              <label>修正值(mm):</label>
		             </td>
		             <td  class="width-15 active text-right t_extend help_para">	
		              <label>高度(mm):</label>
		            </td>
					<td class="width-35">
					   <form:input path="updateValue" htmlEscape="false" class="form-control"  />
  					</td>
				</tr>
				<tr    class="t_extend section_para"  >
					<td  class="width-15 active text-right">	
		              <label>断面终点:</label>
		            </td>
					<td class="width-35"  colspan="3">
						<form:select path="sectionEnd" htmlEscape="false" class="form-control"    onchange="sectionEndChange()"   />
						<form:hidden path="sectionEndName" htmlEscape="false"  class="form-control"  />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				
				<tr   class="t_extend section_para" >
					<td  class="width-15 active text-right">	
		              <label>断面方向选择:</label>
		            </td>
		            
					<td class="width-35"  colspan="3">
						<form:select path="directionSelection" htmlEscape="false" class="form-control"  dict="directionSelection"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr class="t_com"  >
					<td  class="width-15 active text-right">	
		              <label>初始值次数:</label>
		            </td>
					<td class="width-35"   >
						<form:select path="initialValueTimes" htmlEscape="false" class="form-control"  dict="InitialValueTimes"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  id="init_val_lbl" class="width-15 active text-right section_para">	
		              <label>初始值累计值({ff_unit}):</label>
		            </td>
					<td class="width-35 section_para"  colspan="3">
						<form:input path="initialCumulativeValue" htmlEscape="false" class="form-control"       />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr  class="t_extend T003">
			     	<td  class="width-15 active text-right">	
		              <label>起始深度:</label>
		             </td>
					<td class="width-35">
					   <form:input path="startDepth" htmlEscape="false" onchange="initDepthTable()" class="form-control"  />
  					</td>
			 
			     	<td  class="width-15 active text-right">	
		              <label>深度步长:</label>
		             </td>
					<td class="width-35">
					   <form:input path="stepDepth" htmlEscape="false" onchange="initDepthTable()" class="form-control"  />
  					</td>
				</tr>
				<tr  class="t_extend T003">
			     	<td  class="width-15 active text-right">	
		              <label>测孔深度:</label>
		             </td>
					<td class="width-35">
					   <form:input path="endDepth" htmlEscape="false" onchange="initDepthTable()" class="form-control"  />
  					</td>
  					
  					 
				</tr>
				
		   </tbody>
		</table> 
		</div>
		
		
		<div class="ibox-title section_para help_para"><h3>报警设置</h3>

			 <button style="float: right;" class="btn btn-primary btn-sm" onclick="$('#myModal').modal('toggle');return false;"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;修改</button>
		</div>
		<div class="panel-body section_para help_para" >
			<div  align="center" style="margin-top:-20px;margin-bottom:10px" >
			 <form:select path="alarmValueType" htmlEscape="false" style="width:200px;" class="form-control" onchange= "alarmValueTypeChange()" dict="alarmValueType"      />
			 </div> 
		<table id="alarm_set" class="table table-bordered  table-condensed dataTables-example dataTable no-footer t_com section_para help_para">
		 
		  <tbody>
		      
		    
		        <tr   class="t_extend T001 T008   " >
					<td  class="width-15 active text-right">	
		              <label> </label>
		            </td>
					<td class="width-35"   align="center">
						<label>拉力</label>
					</td>
					<td class="width-35"  align="center">
						<label>压力</label>
					</td>
				</tr>
				<tr   class="t_extend   T005" >
					<td  class="width-15 active text-right">	
		              <label> </label>
		            </td>
					<td class="width-35"   align="center">
						<label>水位上升</label>
					</td>
					<td class="width-35"  align="center">
						<label>水位下降</label>
					</td>
				</tr>
		        <tr   class="t_com" >
					<td  class="width-15 active text-right">	
		              <label>预警值({ff_unit}):</label>
		            </td>
					<td class="width-35" >
						<form:input path="warningValue" htmlEscape="false" class="form-control"   readonly="true"   />
						<label class="Validform_checktip"></label>
					</td>
				 
					<td class="width-35 t_extend T001 T008 T005" >
						<form:input path="warnValueNg" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				
				  <tr  class="t_com designSet" >
					<td  class="width-15 active text-right"  >	
		              <label>报警值({ff_unit}):</label>
		            </td>
					<td class="width-35" >
						<form:input path="alarmValue" htmlEscape="false" class="form-control"     readonly="true"  />
						<label class="Validform_checktip"></label>
					</td>
					<td class="width-35 t_extend T001 T008 T005"  >
						<form:input path="alarmValueNg" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>


				<tr     class="t_com designSet" >
					<td class="width-15 active text-right"><label>控制值({ff_unit}):</label>
					</td>
					<td class="width-35"  >
					<form:input path="controlValue" htmlEscape="false" class="form-control"  readonly="true" /> 
					<label class="Validform_checktip"></label>
					</td>
					<td class="width-35 t_extend T001 T008 T005"  >
					<form:input path="controlValueNg" htmlEscape="false" class="form-control" /> 
					<label class="Validform_checktip"></label>
					</td>
				</tr>
  
				<tr  class="t_com" >
					<td  class="width-15 active text-right">	
		              <label>速率报警值({ff_unit}/d):</label>
		            </td>
					<td class="width-35"  >
						<form:input path="rateAlarmValue" htmlEscape="false" class="form-control"   readonly="true"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				
				<tr  class="t_extend" >
					<td  class="width-15 active text-right">	
		              <label>报警组选择:</label>
		            </td>
					<td class="width-35"   >
						<form:input path="alarmGroupName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		   </tbody>
		</table> 
		</div>
		<div class="ibox-title t_extend T003"><h3>初始累计值设置</h3></div>
		 <div class="panel-body">
	     <table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer t_extend T003">
            <thead>
              <th style="width:50px">深度</th>
              <th>累计值</th>
              <th style="width:50px">深度</th>
              <th>累计值</th>
              <th style="width:50px">深度</th>
              <th>累计值</th>
              <th style="width:50px">深度</th>
              <th>累计值</th>
            </thead>
            <tbody id="depthContent">
 			</tbody>
         </table>
		</div>
	   <div class="ibox-title t_extend T001 T008"><h3>参数设置</h3></div>
		<div class="panel-body"  >
		<table   class="table table-bordered  table-condensed dataTables-example dataTable no-footer t_extend T001 T008 ">
		  <tbody>
		   <tr >
			    <td class="width-15 active text-right"><label>支撑截面积(mm):</label>
					</td>
					<td class="width-35"  colspan="3">
					<form:input path="supportZcjmj" htmlEscape="false" class="form-control" /> 
					<label class="Validform_checktip"></label>
					</td>
				   </tr>
		      
				    <tr >
					<td class="width-15 active text-right"><label>钢筋弹性模量(kN/mm²):</label>
					</td>
					<td class="width-35"  colspan="3">
					<form:input path="supportGjtxml" htmlEscape="false" class="form-control" /> 
					<label class="Validform_checktip"></label>
					</td>
				   </tr>
				 
		  </tbody>
		
		</table>
		<table id="support_para" class="table table-bordered  table-condensed dataTables-example dataTable no-footer t_extend T001 T008 ">
		  <tbody>
		           <tr >
					<td class="width-15 active text-right"><label>钢筋截面积(mm):</label>
					</td>
					<td class="width-35"  colspan="3">
					<form:input path="supportGjjmj" htmlEscape="false" class="form-control" /> 
					<label class="Validform_checktip"></label>
					</td>
				   </tr>
				    <tr >
					<td class="width-15 active text-right"><label>混凝土弹性模量(kN/mm²):</label>
					</td>
					<td class="width-35"  colspan="3">
					<form:input path="supportHnttxml" htmlEscape="false" class="form-control" /> 
					<label class="Validform_checktip"></label>
					</td>
				   </tr>
				   
				     <tr id="supportGjjjmj">
					<td class="width-15 active text-right"><label>钢筋计截面积(mm²):</label>
					</td>
					<td class="width-35"  colspan="3">
					<form:input path="supportGjjjmj" htmlEscape="false" class="form-control" /> 
					<label class="Validform_checktip"></label>
					</td>
				   </tr>
		  </tbody>
		
		</table>
	    </div>
	</form:form>

<html:js name="bootstrap-fileinput" />
<html:js name="ff" />
<html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline" />
 
<script src="${staticPath}/modules/codegen/js/table.data.js"></script>
<script src="${staticPath}/modules/codegen/js/table.edit.js"></script>
    <script type="text/javascript"> 
    $(function(){
    	$('#debugTooltip').tooltip();
    	var item = ff.cache.get("monitorItem");
    	if(null == item)
    	{
    		init($("#monitorItemTypeCode").val());
    	}	
    	else
    	{
    		$("#monitorItemId").val(item.id);
    		$("#monitorItemName").val(item.monitorItemName);
         	init(item.groupTypeCode);
         	loadSectionStart(item.id);
         	loadSectionEnd(item.id);
    	}
    	 
    	
    	if(item.isAuto =="1")
    	{
    		if(item.groupTypeCode =="T002" || item.groupTypeCode =="T004" || item.groupTypeCode =="T012" )
    		{
    			$("#dataUploadUnit").show();
    			$("#sensorType").show();
    			$(".sensor1").show();
    			
    		}	
    	}	
		
    });
    function modifyAlarm() {
    	var desp = $("#m_modifyDesp").val();
    	if (desp == null || desp == '') {
    		swal("提示！", '描述信息为空', "error");
    		return false;
    	}
    	var warnVal = $("#m_warn_val").val();
    	var alarmVal = $("#m_alarm_val").val();
    	var controlVal = $("#m_control_val").val();
    	var rateVal = $("#m_rate_val").val();
    	
    	$("#warningValue").val(warnVal);
    	$("#alarmValue").val(alarmVal);
    	$("#controlValue").val(controlVal);
    	$("#rateAlarmValue").val(rateVal);
    	$("#modifyDesp").val(desp);
    	$('#myModal').modal('hide');
    	return false;
    }
    function init(type)
    {
    	$(".t_com").show();
     	$(".t_extend").hide();
     	$("."+type).show();
    	debugger;
    	if(type=="T010" || type=="T011" ||type=="T007" ||type=="T006" ||type=="T005")
    	{
    		$("#sensorQuantity").attr("disabled",true);
    		$("#sensorQtyDiv").hide();
    	}
    	
     	loadSensorType(type);
     	loadDeviceType(type);
     	sensorTypeChange();
    	supportTypeChange();
    	alarmValueTypeChange();
     
    	loadDataUploadUnit();
    	
    	
    	sensorNumChange();
    	debugger;
    	ff.service.unit.load(type,"#alarm_set");
    	ff.service.unit.load(type,"#alarm_val_div");
    	ff.service.unit.load(type,"#init_val_lbl");
    	
    	if(type=="T003")
    	{
    		 initDepthTable();
    	}
    	if(type=="T002")
    	{
    		var selector= $("select[name=measuringPointType]");  
        	ff.form.combox.load(selector,{
        	     url: "sys/dict/loadbygroup",
        	     select:$("#measuringPointTypeName").val(),
        	     valueField: "value",
        	     labelField: "label",
        	     data:{"group":"measuringPointType"},
        	     callback:measuringPointTypeChange()
        	});
    	}	
    	if(type=="T013")
    	{
    		var selector= $("select[name=measuringPointType]");  
        	ff.form.combox.load(selector,{
        	     url: "sys/dict/loadbygroup",
        	     select:$("#measuringPointType").val(),
        	     valueField: "value",
        	     labelField: "label",
        	     data:{"group":"help_type"},
        	     callback:measuringPointTypeChange()
        	});
    	}
    	 
    }
    function initDepthTable()
    {
    	debugger;
	   	 var start = parseFloat($("#startDepth").val());
		 var end = parseFloat($("#endDepth").val());
		 var step = parseFloat($("#stepDepth").val());
    	 var count = Math.ceil((end-start)/step) +1;
    	 var html ="";
    	 var old = '${data.depthExt}';
    	 alert(old);
       	var oldDepth = ff.util.jsonToObj(old);
    	 for(var i=0;i<count/4;i++)
         {
     		 var row ="";
      		 for(var j=0;j<4;j++)
     		 {
      			 var index = i*4+j;
      			 var depth = start + step*index;
      			 if(depth>end)
     		     {
      				 break;
     		     }
      			 var temp = ff.obj.find(oldDepth,"depthExt",depth);
      			 var val = "0";
      			 if(null != temp)
      			 {
      				val = temp.initialCumulativeValue;
      			 }	 
        		 row += '<td>  <label>' + depth + '</label> </td>';
        		 row += '<td > <input id="depth'+ index +'" htmlEscape="false" onchange="depthChange()" class="form-control" value="'+val+'" /> </td>';
     		 }
    		 html +="<tr>"+row +"</tr>";
         }		
         $("#depthContent").html(html);
         depthChange();
    }
 
    
    function depthChange()
    {
    	var start = parseFloat($("#startDepth").val());
		 var end = parseFloat($("#endDepth").val());
		 var step = parseFloat($("#stepDepth").val());
    	 var count = Math.ceil((end-start)/step) +1;
    	 var old = '${data.depthExt}';
        var oldDepth = ff.util.jsonToObj(old);
    	var newDepth = []	
    	for(var i=0;i<count;i++)
    	{
    		var depth =  start + i*step;
    		var obj =  ff.obj.find(oldDepth,"depthExt",depth);
    		if(null == obj)
    		{
    			obj = {};
    			obj.initValue =0;
    		}	
    		obj.depthExt = depth;
    		obj.initialCumulativeValue = $("#depth"+i).val();
    		newDepth.push(obj);
    	}
     	$("#depthExt").val(ff.util.objToJson(newDepth));
    }
    
    function loadSensorType(groupTypeCode)
    {
    	var selector= $("select[name=sensorType]");  
    	ff.form.combox.load(selector,{
    	     url: "sm/mcsmparamconfig/sensorbytype",
    	     select:$("#sensorTypeName").val(),
    	     valueField: "id",
    	     labelField: "name",
    	     data:{"paraValue":groupTypeCode} 
    	},function()
	     {
	    	 sensorTypeChange();
	     });
    }
    
    function loadSectionStart(monitorItemId)
    {
    	var selector= $("select[name=sectionStart]");  
    	ff.form.combox.load(selector,{
    	     url: "supervise/mcproitemmeasuringpoint/loadall",
    	     select:$("#sectionStartName").val(),
    	     valueField: "id",
    	     labelField: "measuringPointCode",
    	     data:{"monitorItemId":monitorItemId} 
    	});
    }
    function loadSectionEnd(monitorItemId)
    {
    	
    	var selector= $("select[name=sectionEnd]");  
    	ff.form.combox.load(selector,{
    	     url: "supervise/mcproitemmeasuringpoint/loadall",
    	     select:$("#sectionEndName").val(),
    	     valueField: "id",
    	     labelField: "measuringPointCode",
    	     data:{"monitorItemId":monitorItemId} 
    	});
    }
    function sectionStartChange()
    {
    	var sectionStartName = $("#sectionStart").find("option:selected").text();
    	$("#sectionStartName").val(sectionStartName);
    }
    function sectionEndChange()
    {
    	var sectionEndName = $("#sectionEnd").find("option:selected").text();
    	$("#sectionEndName").val(sectionEndName);
    }
    
    function loadDeviceType(groupTypeCode)
    {
    	debugger;
    	var selector= $("select[name=deviceTypeId]");  
    	ff.form.combox.load(selector,{
    	     url: "sm/mcsmparamconfig/devicebytype",
    	     select:$("#deviceTypeName").val(),
    	     valueField: "id",
    	     labelField: "name",
    	     data:{"paraValue":groupTypeCode}
    	},function(){deviceTypeChange();});
    }
    
    function loadDataUploadUnit()
    {
    	debugger;
    	var selector= $("select[name=dataUploadUnitId]");  
    	ff.form.combox.load(selector,{
    	     url: "sm/mcsmparamconfig/loadall",
    	     select:$("#dataUploadUnitName").val(),
    	     valueField: "id",
    	     labelField: "name",
    	     data:{"parentId":"1e59cd410b1c44069c1dde54f16626bb"}
    	},function(){deviceTypeChange();});
    }
    function dataUploadUnitChange()
    {
    	var dataUploadUnitName = $("#dataUploadUnitId").find("option:selected").text();
    	$("#dataUploadUnitName").val(dataUploadUnitName);
    }
    function deviceTypeChange()
    {
    	var deviceTypeName = $("#deviceTypeId").find("option:selected").text();
    	$("#deviceTypeName").val(deviceTypeName);
    	if("" != deviceTypeName && "请选择" !=deviceTypeName)
    	{
    		var para = "typeName="+deviceTypeName;
        	$("#deviceId").attr("para",para);
 
    	}
    	
    }
    
    function sensorTypeChange()
    {
    	var sensorTypeName = $("#sensorType").find("option:selected").text();
    	$("#sensorTypeName").val(sensorTypeName);
    	var para = "projectId=${projectId}";
    	if("" != sensorTypeName && "请选择" !=sensorTypeName)
    	{
    	   para = "sensorTypeName="+sensorTypeName + "&projectId=${projectId}";
     	}
    	 
    	if(sensorTypeName=='钢筋计')
    	{
    		 
    		$("#supportGjjjmj").show();
    	}	
    	else
    	{
    		 
    		$("#supportGjjjmj").hide();
    	}
 
    	$("#sensorId1").attr("para",para);
    	$("#sensorId2").attr("para",para);
    	$("#sensorId3").attr("para",para);
    	$("#sensorId4").attr("para",para);
    	
    }
 
	function supportTypeChange()
	{
		debugger;
		var val  = $("#supportType").val();
		$("#supportTypeName").val($("#supportType").find("option:selected").text());
		if(val=="MP_support_steel")
		{
			$("#support_para").hide();
		}	
		else
		{
			$("#support_para").show();
		}
	}
	function alarmValueTypeChange()
	{
		debugger;
		var val  = $("#alarmValueType").val();
 		if(val=="SHEJIWEITIGONG")
		{
			$(".designSet").hide();
		}	
		else
		{
			$(".designSet").show();
		}
 
	}
	function measuringPointTypeChange()
	{
		 
		var name  = $("#measuringPointType").find("option:selected").text();
	    var val = $("#measuringPointType").val();
	    if(null == val)
	    {
	    	val = "${data.measuringPointType}";
	    }	
 
		$("#measuringPointTypeName").val(name);
	 
		if(val=="measuring_point" )
		{
			$(".section_para").show();
		}	
		else if(val=="no_help")
	    {
			$(".help_para").show();
	    }		
		else 
		{
			$(".section_para").hide();
			$(".help_para").hide();
		}
     
	}
	function sensorNumChange()
	{
		var qty = $("#sensorQuantity").val();
		if(qty == 1)
		{
 			toHide(".sensor2");
			toHide(".sensor3");
			toHide(".sensor4");
 		}
		else if(qty == 2)
		{
			$(".sensor2").show();
 
			toHide(".sensor3");
			toHide(".sensor4");
		}
		else if(qty == 3)
		{
			$(".sensor2").show();
 			$(".sensor3").show();
  			toHide(".sensor4");
		}
		else if(qty == 4)
		{
			$(".sensor2").show();
 			$(".sensor3").show();
 			$(".sensor4").show();
		}
		
	}
	function mcProItemMonitorItemDataPicker(rowDatas, fildId, fildName) {
		$("#" + fildId).val(rowDatas[0].id);
		$("#" + fildName).val(rowDatas[0].monitorItemName);
		$("#" + fildName).focus();
 		init(rowDatas[0].groupTypeCode);
 		
	}
	function mcMonitorSensorPicker(rowDatas, fildId, fildName) {
		$("#" + fildId).val(rowDatas[0].id);
		$("#" + fildName).val(rowDatas[0].sensorNum);
		$("#" + fildName).focus();
	}
	function mcDeviceSensorPicker(rowDatas, fildId, fildName) {
		$("#" + fildId).val(rowDatas[0].id);
		$("#" + fildName).val(rowDatas[0].code);
		$("#" + fildName).focus();
		$("#deviceModel").val(rowDatas[0].unitType);
		$("#deviceModelName").val(rowDatas[0].unitTypeName);
	}
	
 
	
	function toHide(select)
	{
		$(select).hide();
		$(select).removeAttr("datatype");
		$(select).find("input").removeAttr("datatype");
	}

    </script>
</body>
</html>