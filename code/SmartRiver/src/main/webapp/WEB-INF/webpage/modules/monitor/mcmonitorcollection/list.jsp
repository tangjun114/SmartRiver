<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>通信设备列表</title>
  <meta name="decorator" content="list"/>
    <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
</head>
<body title="通信设备">
  
<grid:grid id="mcMonitorCollectionGridId" url="${adminPath}/monitor/mcmonitorcollection/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
     <grid:column label="类型"  name="type" hidden="true"   />
    <grid:column label="型号"  name="unitType" hidden="true"   />
    <grid:column label="设备名称"  name="name"  query="true"    condition="eq"   />
    <grid:column label="编号"  name="code"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="电话号码"  name="phone" />
    <grid:column label="使用状态"  name="status"  dict="SBXX_SBSFSY"/>
    <grid:column label="类型"  name="typeName" />
    <grid:column label="型号"  name="unitTypeName" />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button title="sys.common.delete"  groupname="opt" function="delete"   />
    <grid:toolbar function="create" url="${adminPath}/monitor/mcmonitorcollection/create?projectId=${projectId}"/>
    
	<grid:toolbar function="update"/>
    <grid:toolbar title="复制" icon="fa-database"  function="updateDialog" url="${adminPath}/supervise/mcmonitorcollection/{id}/copy"  />
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
 
<script type="text/javascript">   
var projectId = '${projectId}';
var winHeight;
$(document).ready(function()   
{  
	//获取窗口高度 兼容各个浏览器
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}

	var newH = winHeight-250;
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});   

</script> 
</body>
</html>