<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>警报图</title>
    <meta name="decorator" content="single"/>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div >
                <div class="portlet box  portlet-grey">
					<div >
						<div id="pie-content" class="col-sm-12">
                        	<div class="echarts col-sm-4"  id="echarts-pie-chart0" style="height:280px"></div>
                        	<div class="echarts col-sm-4" id="echarts-pie-chart1" style="height:280px"></div>
                        	<div class="echarts col-sm-4" id="echarts-pie-chart2" style="height:280px"></div>
                    	</div>
                    	<div class="echarts" id="bar-content" style="height:400px"></div>

					</div>
                </div>
            </div>
         </div> 
    </div>
    
	<!-- 全局js -->
<html:js name="echarts" />
	 
<html:js name="ff" />
<script type="text/javascript">
var winHeight2;
//注意：下面的代码是放在iframe引用的子页面中调用
$(document).ready(function() {
		if(window.innerHeight){
			winHeight2=window.innerHeight;
			}
		else if((document.body)&&(document.body.clientHeight)){
			winHeight2=document.body.clientHeight;
			}
		var newH = winHeight2-200;
		$("#bar-content").css("cssText","height: "+newH+"px!important;");
		//$("#echarts-bar-chart").css("cssText","height: "+newH+"px!important;");
		//$("#echarts-pie-chart-container").css("cssText","height: "+newH+"px!important;");

		 load();
});
	function load() {
		//alert('ok');
		ff.util.submit({
				url:"monitor/mcmonitororg/statistics/by",
				data:'type',
				success:function(rsp){
					if(null != rsp && rsp.obj)
					{	debugger;
					    ff.chart.load("#bar-content","",rsp.obj,"bar");	
					    //ff.chart.load("#echarts-pie-chart0","",rsp.obj[0],"pie");
  					    $.each(rsp.obj, function(index, element) {
					    	ff.chart.load("#echarts-pie-chart"+index,element.y_name,element,"pie");
					    });  
						
						
					}
				}
			
			});
	}
		
	</script>
</body>

</html>