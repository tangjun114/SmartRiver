<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>报警记录列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="报警记录">
<grid:grid id="floodWarnLogGridId" url="${adminPath}/flood/floodwarnlog/ajaxList">
    <%--<grid:column label="sys.common.key" hidden="true" name="id" width="100"/>--%>
    <%--<grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>--%>
    <grid:column label="站点" name="siteName" query="true" queryMode="input" condition="eq"/>
    <grid:column label="站点类型" name="type" query="true" queryMode="select" condition="eq" dict="flood_site_type"/>
    <grid:column label="站点类别" name="kind" query="true" queryMode="select" condition="eq" dict="flood_site_kind"/>
    <grid:column label="预警信息" name="warnInfo"/>
    <grid:column label="创建时间" name="createDate"/>
    <%--<grid:toolbar function="create"/>--%>
    <%--<grid:toolbar function="update"/>--%>
    <%--<grid:toolbar function="delete"/>--%>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>