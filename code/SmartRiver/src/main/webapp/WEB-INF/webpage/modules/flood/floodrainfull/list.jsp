<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>雨量监测列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="雨量监测">
<grid:grid id="floodRainfullGridId"
           url="${adminPath}/flood/floodrainfull/ajaxList?recordType=${recordType}&siteId=${siteId}">
    <%--<grid:column label="sys.common.key" hidden="true" name="id" width="100"/>--%>
    <%--<grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>--%>
    <%--<grid:button groupname="opt" function="delete"/>--%>
    <grid:column label="站点" name="siteName" query="true" queryMode="input" condition="eq"/>
    <grid:column label="站点类型" name="type" query="true" queryMode="select" dict="flood_site_type"/>
    <grid:column label="站点类别" name="kind" query="true" queryMode="select" dict="flood_site_kind"/>
    <grid:column label="雨量/mm" name="rainfull"/>
    <%--<grid:column label="水位" name="waterLevel" />--%>
    <%--<grid:column label="流量" name="flow"/>--%>
    <grid:column label="记录时间" name="recordTime"/>
    <%--<grid:toolbar function="create"/>--%>
    <%--<grid:toolbar function="update"/>--%>
    <%--<grid:toolbar function="delete"/>--%>

    <grid:toolbar function="exportFile"/>
    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>