<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目类型列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目类型">
<grid:grid id="projectTypeGridId" url="${adminPath}/sm/projecttype/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="类型名称"  name="name"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="工况编号"  name="need_wcno"  query="true"  queryMode="select"  condition="eq"  dict="sf"/>
    <grid:column label="监测目的与意义"  name="purpose_desc" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>