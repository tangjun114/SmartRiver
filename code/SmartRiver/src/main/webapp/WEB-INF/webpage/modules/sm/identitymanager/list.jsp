<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>身份管理列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="身份管理">
<grid:grid id="identityManagerGridId" url="${adminPath}/sm/identitymanager/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
    <grid:button title="sys.common.delete"  groupname="opt" function="delete"   />
    <grid:column label="身份名称"  name="name"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="描述"  name="desc"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="类型"  name="type"  dict="mechanismType"/>
    <grid:column label="状态"  name="status"  dict="identityStatus"/>
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>