<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目管理</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor,jqgrid" />
</head>

<body class="white-bg"  formid="mcProjectForm" beforeSubmit="beforeSubmit">
    <form:form id="mcProjectForm" modelAttribute="data"  method="post" class="form-horizontal">
		
		<input type="hidden" key="personInCharge" value="${data.personInCharge}">
		<input type="hidden" key="monitorBy" value="${data.monitorBy}">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>项目编号:</label>
		            </td>
					<td class="width-35">
						<form:input path="code" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>项目名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="name" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>

				<tr>
					<td  class="width-15 active text-right">	
		              <label>项目类别:</label>
		            </td>
					<td class="width-35">
						<form:select path="projectType" htmlEscape="false" class="form-control"  dict="XIANGMULB"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>项目地址:</label>
		            </td>
					<td class="width-35">
						<input name="latitude" class="form-control"  type="text" id="lat" value="${data.latitude}" style="display: none;"/>
						<input name="longitude"  class="form-control"  type="text" id="lng" value="${data.longitude}" style="display: none;"/>
						<input name="province"  class="form-control"  type="text" id="province" value="${data.province}" style="display: none;"/>
						<input name="city"  class="form-control"  type="text" id="city" value="${data.city}" style="display: none;"/>
						<input name="district"  class="form-control"  type="text" id="district" value="${data.district}" style="display: none;"/>
						
						<div style="width: 80%;float: left">
						<form:input path="fullAddress" htmlEscape="false" class="form-control"  datatype="*"   id="full_addr" />
						<label class="Validform_checktip"></label>
						</div>
						<div>
						<button type="button" class="btn btn-primary" id="btn_gps">
						<i class="fa fa-map"></i></button></div>
						<label class="Validform_checktip"></label>
					</td>

				</tr>
				 
		   </tbody>
		</table>   
	</form:form>
	<div class="row">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
            </ul>
            <div class="tab-content">
          
            </div>
        </div>
    </div>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline,layer" />

<html:js name="ff" />
<script>
	$(document).ready(function () {
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	    	 resizeGrid();
		});
	    initDatePikerData();
	});
	
	
	$(function(){
	   $(window).resize(function(){   
		   resizeGrid();
	   });
	   
	   var selector= $("select[name=monitorOrgId]");  
	   var data={type:"supervise"};
	   	ff.form.combox.load(selector,{
	   	     url: "monitor/mcmonitororg/parent/list",
	   	     select:$("#monitorOrgName").val(),
	   	     valueField: "org",
	   	     labelField: "name",
	   	     data:data
	   	});
	});
 
	function superviseOrgChange()
	{
    	var groupTypeName = $("#monitorOrgId").find("option:selected").text();
    	$("#monitorOrgName").val(groupTypeName);
    	 
	}
	
	function  initDatePikerData(){
		$("input[key]").each(function(i,ele){
			var key = $(ele).attr("key");
			var val = $(ele).val();
			if(val){
			$("#"+key).val(val);
			}
		});
	}
	/**
	*提交回调方法
	*/
	function beforeSubmit(curform){
		 //这里最好还是使用JSON提交，控制器改变提交方法，并使用JSON方式来解析数
		 //通过判断，如果有问题不提交
		 return true;
	}
	
	function initGridFormData(curform,gridId){
		 var rowDatas =getRowDatas(gridId+"Grid");
		 var rowJson = JSON.stringify(rowDatas);
		 if(rowJson.indexOf("editable") > 0&&rowJson.indexOf("inline-edit-cell") )
	     {
	    	 return false;
	     }
		 var gridListJson=$('#'+gridId+"ListJson").val();
		 if(gridListJson==undefined||gridListJson==''){
			 var rowInput = $('<input id="'+gridId+'ListJson" type="hidden" name="'+gridId+'ListJson" />');  
			 rowInput.attr('value', rowJson);  
			 // 附加到Form  
			 curform.append(rowInput); 
		 }else{
			 $('#'+gridId+"ListJson").val(rowJson);
		 }
		 return true;
	}
	function userDataPicker(rowDatas,fildId,fildName){
		 
		$("#"+fildId).val(rowDatas[0].id);
		$("#"+fildName).val(rowDatas[0].realname);
		$("#"+fildName).focus();
	}
	$( "#id-btn-dialog2" ).on('click', function(e) {
		e.preventDefault();
	
		$( "#dialog-confirm" ).removeClass('hide').dialog({
			resizable: false,
			width: '320',
			modal: true,
			title: "<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Empty the recycle bin?</h4></div>",
			title_html: true,
			buttons: [
				{
					html: "<i class='ace-icon fa fa-trash-o bigger-110'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger btn-minier",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
				,
				{
					html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Cancel",
					"class" : "btn btn-minier",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
	});
	$("#btn_gps").on('click', function(e) {

		var lat = '${data.latitude}';
		var lng = '${data.longitude}';
		var url = "${staticPath}/map/bdmap.html?lat="+lat+"&lng="+lng;
		layer.open({
			  type: 2,
			  title: false,
			  area: ['630px', '360px'],
			  closeBtn: false,
			  shade: 0.8,
			  btn: ['确定', '关闭'],
			  content: url,
			  success: function(layero){
				    var btn = layero.find('.layui-layer-btn');

				  },
		 	  yes: function(index, layero){
					 //$(window.frames["iframeName"].document).find("#newlat").html()
		 		    //debugger;
		 		    var newlat = ff.cache.get('newlat');
		 		    var newlng = ff.cache.get('newlng');
		 		    var newadrr = ff.cache.get('newaddr');
		 		    var province = ff.cache.get('newprovince');
		 		    
		 		    var city = ff.cache.get('newcity');
		 		    var district = ff.cache.get('newdistrict');

					$("#lat").val(newlat);
					$("#lng").val(newlng);
					$("#full_addr").val(newadrr);
					$("#province").val(province);
					$("#city").val(city);
					$("#district").val(district);
		 		    //console.log(res);
				    layer.close(index); //如果设定了yes回调，需进行手工关闭
				    
				  }
			}); 

	});
	
</script>
</body>
</html>