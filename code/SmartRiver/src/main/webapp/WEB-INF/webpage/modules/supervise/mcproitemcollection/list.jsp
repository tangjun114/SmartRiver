<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目中的通信设备列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目中的通信设备">
<grid:grid id="mcProItemCollectionGridId" url="${adminPath}/supervise/mcproitemcollection/ajaxList?projectId=${projectId}">
 
	    <grid:column label="类型"  name="type" hidden="true"   />
    <grid:column label="型号"  name="unitType" hidden="true"   />
    <grid:column label="设备名称"  name="name"  query="true"    condition="eq"   />
    <grid:column label="编号"  name="code"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="电话号码"  name="phone" />
    <grid:column label="使用状态"  name="status"  dict="SBXX_SBSFSY"/>
    <grid:column label="类型"  name="typeName" />
    <grid:column label="型号"  name="unitTypeName" />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button title="sys.common.delete"  groupname="opt" function="delete"   />
    <grid:toolbar function="create" url="${adminPath}/supervise/mcproitemcollection/create?projectId=${projectId}"/>
    <grid:toolbar function="delete"/>
	<grid:toolbar function="update"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<script type="text/javascript">  
$(document).ready(function() {
	var winHeight;
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-250;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});

</script>
</body>
</html>