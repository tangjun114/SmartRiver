<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>报警短信列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="报警短信">
<grid:grid id="mcProItemMessageGridId" url="${adminPath}/supervise/mcproitemmessage/ajaxList?isMy=1">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
      
    <grid:column label="项目名称"  name="projectName" />
    <grid:column label="短信类型"  name="type" />
    <grid:column label="时间"  name="createDate" />
    <grid:column label="短信内容"  name="content" />
    <grid:column label="状态"  name="status" />
     
 
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>