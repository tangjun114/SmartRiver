<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>设备异常状况列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="设备异常状况">
<grid:grid id="mcProDeviceAbnormalGridId" url="${adminPath}/supervise/mcprodeviceabnormal/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>

	<grid:column label="项目名称"  name="projectName" />
 	<grid:column label="设备名称"  name="deviceName" />
	<grid:column label="设备编号"  name="deviceCode" />
	<grid:column label="报警时间"  name="alarmTime" />
	<grid:column label="异常信息"  name="abnormalInfo" />
	
	<grid:column label="状态"  hidden="true" name="status" />
	<grid:column label="状态"  name="statusBtn" formatter="button"/>
	<grid:column label="处理人"  name="handlerName" />
	<grid:column label="处理时间"  name="handleTime" />
	<grid:column label="处理措施"  name="handleResult" />
	
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />

</grid:grid>
<script type="text/javascript">

var winHeight;
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-180;
	$(".ibox .ibox-title").css("cssText","display: none"); 

	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");

});
function mcProDeviceAbnormalGridIdStatusBtnFormatter(value, options, row){
	var href = '';
		try{
	         if(!row.id) {
          return '';
      	}
		debugger;
	    href="<a href=\"#\"";
	    
	    if(row.status =='to_handle'){
	    	href +="onclick=\"showModify('"+row.id+"')\"";
	    	href +=">&nbsp未处理</a>";
	    }
	    else if(row.status =='handled'){
	    	//href +="onclick=\"showModify('"+row.id+"')\"";
			href +=">&nbsp已处理</a>";
		} else {
			href +=">&nbsp</a>";
		}
	   }catch(err){}
	   
	   return href;
}
function showModify(id) {
	var url = "${adminPath}/supervise/mcprodeviceabnormal/"+id+"/update";
	openDialog('异常信息处理',url,'mcProDeviceAbnormalGridIdGrid','600px', '300px');

}
</script>
</body>
</html>