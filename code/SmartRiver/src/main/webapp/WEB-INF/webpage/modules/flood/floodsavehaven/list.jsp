<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>避难中心列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="避难中心">
<grid:grid id="floodSaveHavenGridId" url="${adminPath}/flood/floodsavehaven/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="避险中心名称"  name="safeHavenName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="地址"  name="address" />
    <grid:column label="联系方式"  name="phone" />
    <grid:column label="联系人"  name="contacts" />
    <grid:column label="容纳人数"  name="containNum" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>