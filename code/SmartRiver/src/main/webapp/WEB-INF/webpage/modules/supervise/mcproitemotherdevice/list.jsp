<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目中的其他设备列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目中的其他设备">
<grid:grid id="mcProItemOtherDeviceGridId" url="${adminPath}/supervise/mcproitemotherdevice/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>

    <grid:column label="设备名称"  name="otherDeviceName" />
    <grid:column label="设备编码"  name="otherDeviceCode" />
    <grid:column label="设备类型"  name="otherDeviceTypeName" />
    <grid:column label="设备型号"  name="otherDeviceUnitTypeName" />
    <grid:column label="生产厂家"  name="otherDeviceManufacturer" />
    <grid:column label="使用状态"  name="otherDeviceStatus"  dict="SBXX_SBSFSY" />
    
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproitemotherdevice/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<script type="text/javascript">  
$(document).ready(function() {
	var winHeight;
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-210;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});

</script>
</body>
</html>