<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>站点警报阈值列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="站点警报阈值">
<grid:grid id="floodSiteWarnGridId" url="${adminPath}/flood/floodsitewarn/ajaxList?parentId=${parentId}">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="名字" name="name"/>
    <grid:column label="阈值" name="threshold"/>
    <grid:column label="备注信息" name="remarks"/>
    <grid:toolbar function="create" url="${adminPath}/flood/floodsitewarn/create?parentId=${parentId}"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <%--<grid:toolbar function="search"/>--%>
    <%--<grid:toolbar function="reset"/>--%>
</grid:grid>
</body>
</html>