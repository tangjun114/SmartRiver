<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>排污口管理</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput"/>
    <html:css name="simditor"/>
</head>

<body class="white-bg" formid="waterDrainOutletForm">
<form:form id="waterDrainOutletForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>
            <td class="width-15 active text-right">
                <label>名称:</label>
            </td>
            <td class="width-35">
                <form:input path="outletName" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>地址:</label>
            </td>
            <td class="width-35">
                <form:input path="outletAddr" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>经度:</label>
            </td>
            <td class="width-35">
                <form:input path="longitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>纬度:</label>
            </td>
            <td class="width-35">
                <form:input path="latitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>控制信息:</label>
            </td>
            <td class="width-35">
                <form:input path="waterContrl" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>用户信息:</label>
            </td>
            <td class="width-35">
                <form:input path="bindUser" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>监控方式:</label>
            </td>
            <td class="width-35">
                <form:input path="monitorMode" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>设计信息:</label>
            </td>
            <td class="width-35">
                <form:input path="designInfo" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>统计信息:</label>
            </td>
            <td class="width-35">
                <form:input path="statisticsInfo" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>水量:</label>
            </td>
            <td class="width-35">
                <form:input path="waterAmont" htmlEscape="false" class="form-control" type="number" min="1"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>

        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor"/>
</body>
</html>