<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>其他设备</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcMonitorOtherDeviceForm">
    <form:form id="mcMonitorOtherDeviceForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					
					<td  class="width-15 active text-right">	
		              <label>设备编码:</label>
		            </td>
					<td class="width-35">
						<form:input path="code" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>设备名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="name" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					
					<td  class="width-15 active text-right">	
		              <label>设备类型:</label>
		            </td>
					<td class="width-35">
						<form:input path="typeName" htmlEscape="false" readonly="true" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>设备型号:</label>
		            </td>
					<td class="width-35">
						<form:treeselect title="设备型号" path="unitType" dataUrl="${adminPath}/sm/mcsmparamconfig/treeData?nodeid=6be05fda78744609ab45f7a717a15f85" callback="selectOk" labelName="unitTypeName" labelValue="${data.unitTypeName}" 
						 htmlEscape="false" class="form-control"  datatype="*" readonly="true"
						 />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					
					<td  class="width-15 active text-right">	
		              <label>生产厂家:</label>
		            </td>
					<td class="width-35">
						<form:input path="manufacturer" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>使用状态:</label>
		            </td>
					<td class="width-35">
						<form:select path="status" htmlEscape="false" class="form-control" readonly="true" dict="SBXX_SBSFSY"      />
						<label class="Validform_checktip"></label>
					</td>
					<td class="width-15 active text-right"></td>
				</tr>
				<tr>
					
		   		 
		   			<td  class="width-15 active text-right">	
		              <label>备注信息:</label>
		            </td>
					<td class="width-35">
						<form:input path="remarks" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
<script type="text/javascript">

function selectOk(nodes){
	var node = nodes[0];
	if(1!=node.level||node.leaf==false){
	$("#unitTypeClear").click();
	top.layer.alert('请选择第二级叶子节点!', {
		icon : 0,
		title : '警告'
	});
	}else{
	initParentNodes(nodes[0].getParentNode());
	}
}
</script>
</body>
</html>