<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>巡视检查</title>
    <meta name="decorator" content="single"/>
</head>

<body class="white-bg" >

    <div class="animated fadeInRight">
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="myTab">
            <li role="presentation" class="active">
                <a href="#latest" aria-controls="latest" role="tab" data-toggle="tab">最新</a></li>
            <li role="presentation">
                <a href="#datalist" aria-controls="datalist" role="tab" data-toggle="tab">全部</a></li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content" style="overflow-y:hidden;">
            <div role="tabpanel" class="tab-pane in active" id="latest">
            <iframe src="${adminPath}/monitor/mcmonitorpatroldetail?patrolId=${detail.id}" width="100%" height="100%" frameborder="0" scrolling="no" id="latest_content"></iframe> 
            </div>
			<div role="tabpanel" class="tab-pane" id="datalist">
			<iframe src="${adminPath}/monitor/mcmonitorpatrollog?projectId=${param.projectId}" width="100%" height="100%" frameborder="0" scrolling="no" id="datalist_content"></iframe> 
			</div>
        </div>
    </div>
 
    </div>

<html:js name="func" />
<html:js name="ff,layer" />
<script>
var winHeight;

    $(function () {
        $('#myTab a:first').tab('show');
    })
$(document).ready(function () {
	
	if (window.innerHeight) {
			winHeight = window.innerHeight;
		} else if ((document.body) && (document.body.clientHeight)) {
			winHeight = document.body.clientHeight;
		}
		var newH = winHeight - 50; 
     	$("#datalist").css({
			"height" : newH + "px",
			"overflow-y":"hidden"
		});  
     	$("#latest").css({
			"height" : newH + "px",
			"overflow-y":"hidden"
		}); 
     	
		
	});
</script>
</body>
</html>