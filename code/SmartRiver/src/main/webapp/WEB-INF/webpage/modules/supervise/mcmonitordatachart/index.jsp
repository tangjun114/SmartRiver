<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>警报图</title>
    <meta name="decorator" content="single"/>
    	<!-- 全局js -->
	<html:js name="echarts" />
 
	<html:js name="bootstrap-multiselect" />
    <html:js name="ff" />
</head>

<body class="white-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet box  portlet-grey">
					 <div class="portlet-header">
						<div class="caption">曲线图</div>
						<div class="tools">
	                        <i class="fa fa-chevron-up"></i>
	                        <i class="fa fa-refresh"></i><i class="fa fa-times"></i>
	                     </div>
					</div>	
					<form id="ff_filter"   method="post" >
					 <div class="row" style="margin-left:10px">
					  <div class="col-sm-12">
					   <div class="form-inline">
					   	 <label>显示告警: </label>   
					   	 <select  id="alarmList"   class="form-control"  onchange="load()">
 					       
 					   </select>
					    <select  filter="eq" name="subareaId" class="form-control"    style="display: none;" ></select>
					 <label>监测项选择 :</label>  
					 <select  filter="eq" name="monitorItemId"   class="form-control"     ></select>
					<label>测点选择: </label>  <select  id="measuringPointCode" multiple="multiple" filter="in" name="measuringPointCode"     >
 					  </select>
 					   
 					 <label>数据类型: </label> <select  id="valueType"    name="valueType"   class="form-control"  onchange="load()">
 					       <option value="sumValue">累计变化</option>
 					       <option value="minusValue">本次变化</option>
 					       <option value="nowValue">结果值</option>
 					       <option value="changeRate">速率变化</option>
 					   </select>
					   
 					  <label>时间: </label>  
 					  <input id="startTime"  filter="ge" name="collectTime" class="form-control layer-date" placeholder="YYYY-MM-DD" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm'})"    />
					   <input id="endTime"  filter="le" name="collectTime" class="form-control layer-date" placeholder="YYYY-MM-DD" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm'})"       />
					   
					   
					   </div>
					   <a onclick="viewDetail()" class="btn btn-primary btn-sm" ><i class="fa fa-arrow-circle-up"></i>&nbsp;&nbsp;测斜深度曲线</a>
					   
					   <div class="pull-right">
					        
                            <a onclick="load()" class="btn btn-primary btn-sm" ><i class="fa fa-arrow-circle-up"></i>&nbsp;&nbsp;查询</a>
                            <a onclick="location.href=location.href;"  class="btn btn-primary btn-sm"><i class="fa fa-remove"></i>&nbsp;&nbsp;清空</a>
  					  </div>
  					  </div>
  					  </div>
					</form>
					<div >
                        <div class="echarts" id="dataChart" style="height:600px"></div>
                    </div>
                </div>
            </div>
         </div> 
    </div>
    

	<script type="text/javascript">
	var winHeight;
	$(document).ready(function() {
		if(window.innerHeight){
			winHeight=window.innerHeight;
			}
		else if((document.body)&&(document.body.clientHeight)){
			winHeight=document.body.clientHeight;
			}
		var newH = winHeight-110;
		 $("#dataChart").css("cssText","height: "+newH+"px!important;");
	 
	});

	//载入图表
	$(function () {
   
		var today = new Date().format("yyyy-MM-dd");
		var day = new Date(new Date().getTime()-7*24*60*60*1000);
		  
		$("#startTime").val(day.format("yyyy-MM-dd")+" 00:00");
		$("#endTime").val(today+" 23:59");
       	var selector= $("select[name=monitorItemId]");  
 
     	ff.form.combox.load(selector,{
    		  url: "supervise/mcproitemmonitoritem/loadall",
    	      valueField: "id",
    	      labelField: "monitorItemName",
    	      select:"first",
    		  data: {
    			  projectId: '${param.projectId}'
    		  },
    		 
    		},loadPoint);
     	selector.change(function(){
     		//loadUnit();
     		 loadPoint();
     		});
     	$("#measuringPointCode").multiselect("nonSelectedText : '请选择'");
	    
	});
	function loadPoint()
	{
		 
		var selector2= $("select[name=monitorItemId]");
		item = selector2.val();
		var selector3= $("select[name=measuringPointCode]");  
	 	ff.form.combox.load(selector3,{
			  url: "supervise/mcproitemmeasuringpoint/loadall",
		      valueField: "measuringPointCode",
		      labelField: "measuringPointCode",
		      select:"first",
			  data: {
				  monitor_item_id: item
			  },
			 
			},load);
	}
	function load(data)
	{
		if(null != data)
		{
			ff.form.combox.load("#alarmList",{
	 		      valueField: "measuringPointCode",
			      labelField: "measuringPointCode",
			      
				  data:data,
				} );
		}	
		
		debugger;
		ff.req.filter = ff.com.getFilterObj("#ff_filter");
		
		var type = $("#valueType").val();
		 
 	    ff.util.submit({
	    	url:"supervise/mcmonitordatachart/load",
            data:{dim:'collectTime',indicator:type},
            success:loadChart
            });
 	   $("#measuringPointCode").multiselect("destroy").multiselect({
 		    includeResetDivider: true,
  			includeResetOption: true,
 			includeSelectAllOption: true,
 			selectAllText: '全选',
 			resetText:'全不选',
	         nonSelectedText : '请选择',  
	        selectedList:5,//如果多余那么就不会显示合理的选择  
 	   });
  
	}
	
	function loadChart(rsp)
	{
		var temp = rsp.obj;

		var selector= $("select[name=monitorItemId] option:selected"); 
		var item = ff.com.getOptions(selector);
	 
		var yName = $("#valueType").find("option:selected").text()+'(';
		var type = $("#valueType").val();
		if(type=="changeRate")
		{
			 yName  +=  ff.service.unit.get(item.groupTypeCode) +"/d)";
		}	
		else
		{
			yName += ff.service.unit.get(item.groupTypeCode) +")";
		}	
		
		var selector= $("select[id=alarmList] option:selected"); 
		var point = ff.com.getOptions(selector);
		var option = {yAxis:{name:yName}};
 		debugger;
		if(null != point && point.measuringPointCode)
		{
			if(0 != temp.length)
			{
				 var dataList = [{name:'预警值',value:point.warningValue},{name:'告警值',value:point.alarmValue},{name:'控制值',value:point.controlValue}];
				 
				 for(var i=0;i<dataList.length;i++)
				 {
					 var data = dataList[i];
					 
					 var y_data = [];
					 for(var j=0;j<temp[0].x_data.length;j++)
					 {
						 y_data.push(data.value)
					 }
						 
					 
					  var obj = {
					    		y_name:point.measuringPointCode+data.name,
 					            x_data:temp[0].x_data,
					            y_data:y_data,
					            markLine : {
					                data : [
					                    {type : 'average', name : '平均值'}
					                ]
					            }
					           
					        }
					   temp.push(obj);
				 }	 
			  
			}	
 		}	
		option.title = {text:item.monitorItemName,left:'center'};

		option.tooltip = {
		        trigger: 'item',
		        formatter:   function(a,b,c){
		        	 
		        	var val =parseFloat(a.value[1]).toFixed(2);
		        	return "测点：" +a.seriesName+"<br>"+"数值: " + val +"<br>"+" 时间: " + a.name ;
		        }//"累计值 :{c} <br/> 深度: {b}"
		    };
		
		option.xAxis ={
				name:'时间',
		        type: 'time',
		        splitLine: {
		            show: true
		        },
		        splitNumber:10
		    };
		option.yAxis ={
			name:yName,
	        type: 'value',
	        boundaryGap: [0, '100%'],
	        splitLine: {
	            show: true
	        }
	    };
		var series = [];
		var y_nameList = [];
		for(var i=0;i<temp.length;i++)
		{
			var obj = {};
			obj.name = temp[i].y_name;
			y_nameList.push(obj.name);
			obj.type = 'line';
			var data = [];
			for(var j=0;j<temp[i].x_data.length;j++)
			{
				var d = {};
				d.name = temp[i].x_data[j];
				d.value= [d.name,temp[i].y_data[j]]
				data.push(d);
			}	
			obj.data = data;
			series.push(obj);
		}	
		
		option.legend = {y:'bottom',
				data:y_nameList};
		option.series= series;
		
		 
		        
		var chart =  echarts.init($("#dataChart")[0]);
		chart.setOption(option,true);
		// ff.chart.load("#dataChart",item.monitorItemName,temp,'line',option);
	}
	
	function viewDetail(id){
		
		var point =   $("select[name=measuringPointCode]").val();  
		  var option=$("select[name=monitorItemId] option:selected");
		  var item=  ff.com.getOptions(option);
	 
 		if(item.groupTypeCode !='T003' || point.length > 1)
		{
			alert("请选择一个测斜类型的测点");
			return;
		}	
		var monitorItemId =   $("select[name=monitorItemId]").val();  
		var startTime =   $("#startTime").val();  
		var endTime =   $("#endTime").val();  
		var url = "${adminPath}/supervise/mcmonitordatachart/shift?point=" + point + "&monitorItemId="+monitorItemId + "&startTime="+ startTime+ "&endTime=" +endTime;
		var width = "60%";
		var height = "630px";
	 
		top.layer.open({
			type : 2,
			area : [ width, height ],
			title : "测斜数据",
			maxmin : false, // 开启最大化最小化按钮
			content : url,
		 
			cancel : function(index) {
			}
		 });
	}
	
	</script>
</body>

</html>