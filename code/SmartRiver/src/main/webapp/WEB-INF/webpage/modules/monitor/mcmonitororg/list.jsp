<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>监督检测机构列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="监督检测机构">
<grid:grid id="mcMonitorOrgGridId" url="${adminPath}/monitor/mcmonitororg/ajaxList?type=${type}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>

    <grid:column label="机构名称"  name="name"  query="true"  queryMode="input"  condition="eq"  />

     <grid:column label="联系电话"  name="phone" />
    <grid:column label="机构邮箱"  name="email" />
  
    <grid:column label="传真"  name="fax" />
    <grid:column label="地址"  name="address" />
    <grid:column label="企业性质"  name="companyType" />
    <grid:column label="关联组织id"   hidden="true" name="org.id" />
    
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button title="sys.common.delete"  groupname="opt" function="delete" outclass="btn-danger" innerclass="fa-trash"  />
 
	<grid:toolbar function="update"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>