<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目人员列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目人员">
<div class="row">
  <div class="col-md-3" style="height:500px;" id="list_row">
	<div style="height:100%;overflow-y:scroll;">
		<div class="list-group" id="memberTypeDiv">
			
		</div>
		</div>
  </div>
<div class="col-md-9">
<grid:grid id="mcProItemMemberGridId" url="${adminPath}/supervise/mcproitemmember/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>

	<grid:column label=""  name="type"  hidden="true" query="true"  queryMode="input"  condition="eq" /> 
    <grid:column label="姓名"  name="realname" />
    <grid:column label="部门"  name="deptName" />
    <grid:column label="职位"  name="position" />
    <grid:column label="短信通知"  name="msgPerm" dict="msgPerm"/>
    <grid:column label="报告操作"  name="reportPerm" dict="reportPerm"/>
    <grid:column label="电话"  name="phone" />
    <grid:column label="邮箱"  name="email" />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproitemmember/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>

</grid:grid>
</div>
</div>

<html:js name="ff" />
<script type="text/javascript">  
var memberTypeDiv = $("#memberTypeDiv");
var newHtml= '';
var firstVal;
function searchByMemberType(code) {
    $("#memberTypeDiv a").each(function(){ 
        $(this).attr("class","list-group-item");  	        
        if($(this).attr("id")==code){
        	$(this).attr("class","list-group-item active");  
        }
         
    }); 
    var item = ff.com.getOptions(code);
    
	$("input[name=type]").val(code);
	ff.cache.set("type",item);
	search('mcProItemMemberGridIdGrid');
}
var winHeight;
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-210;
	var newHL = winHeight-120;
	$("#list_row").css("cssText","height: "+newHL+"px!important;");
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
	$("input[name=type]").parent().hide();

	ff.util.submit({
			url:"sys/dict/loadbygroup",
			filter:{
				group: "projectMemberType"
			  },
			success:function(rsp){
				if(null != rsp && rsp.obj)
				{				
					$.each(rsp.obj, function(index, element) {
						newHtml +="<a href=\"javascript:void(0)\" "; 
						newHtml +="id='" + element.value+ "'";
						newHtml +="data-options='" + ff.util.objToJson(element) + "'";
						newHtml +="onclick=\"searchByMemberType('";
						newHtml += element.value;
						newHtml +="')\" class=\"list-group-item\"> <h4 class=\"list-group-item-heading\">";
						newHtml += element.label;
						newHtml +="</h4></a>";
						if(index == 0){
							firstVal = element.value;
						}
				    });

					memberTypeDiv.html(newHtml);
	
					setTimeout(function(){searchByMemberType(firstVal)}, 100);
					
				}
			}
		
		});
});


</script>
</body>
</html>