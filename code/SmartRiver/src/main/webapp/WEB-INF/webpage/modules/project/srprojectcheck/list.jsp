<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目检查列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目检查">
<grid:grid id="srProjectCheckGridId" url="${adminPath}/project/srprojectcheck/ajaxList?proId=${proId}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="备注信息"  name="remarks" />
    <grid:column label="项目名称"  name="proName" />
    <grid:column label="检查时间"  name="checkTime" />
    <grid:column label="检查项"  name="checkName" />
    <grid:column label="检查结果"  name="console" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>