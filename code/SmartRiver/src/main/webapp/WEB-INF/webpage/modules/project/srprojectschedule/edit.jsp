<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>项目进度</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput"/>
    <html:css name="simditor"/>
</head>

<body class="white-bg" formid="srProjectScheduleForm">
<form:form id="srProjectScheduleForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>项目名称:</label>
            </td>
            <td class="width-35">
                <form:select path="proId" htmlEscape="false" class="form-control" onchange="changeProName()"/>
                <form:hidden path="proName" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>标题:</label>
            </td>
            <td class="width-35">
                <form:input path="name" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>截止日期:</label>
            </td>
            <td class="width-35">
                <form:input path="deadTime" datefmt="yyyy-MM-dd hh:mm:ss" class="form-control layer-date"
                            placeholder="YYYY-MM-DD hh:mm:ss"
                            onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"/>

                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>描述:</label>
            </td>
            <td class="width-35">
                <form:input path="desc" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>备注信息:</label>
            </td>
            <td class="width-35">
                <form:input path="remarks" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>

        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline,layer" />
<html:js name="ff"/>
<script type="text/javascript">

    $(function () {


        var selector = $("select[name=proId]");

        ff.form.combox.load(selector, {
            url: "project/srprojectinfo/loadall",
            valueField: "id",
            select: $("#proId").val(),
            labelField: "name",
            data: {}
        }, function () {
        });

        var today = new Date().format("yyyy-MM-dd");
        var day = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
        if ("" == document.getElementById("deadTime").value) {
            document.getElementById("deadTime").value = today + " 23:59:59";
        }

    });

    function changeProName() {
        var proName = $('#proId').find('option:selected').text();
        $('#proName').val(proName);
    }

</script>
</body>
</html>