<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目规范</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemStandardForm">
    <form:form id="mcProItemStandardForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="standardCode"/>
 		<form:hidden path="standardPath"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
				
				   <td  class="width-15 active text-right">	
		              <label>监测标准:</label>
		            </td>
					<td class="width-35">
						<form:gridselect gridId="mcRepoStandardGridId" title="监测标准" nested="false"
						   path="standardId" gridUrl="${adminPath}/repo/mcrepostandard" value="${data.standardId}"
						   labelName="standardName" labelValue="${data.standardName}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="standardName"
						   callback="mcRepoStandardDataPicker" 
						   htmlEscape="false" class="form-control" readonly="true" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>备注信息:</label>
		            </td>
					<td class="width-35">
						<form:input path="remarks" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td class="width-15 active text-right"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
<script type="text/javascript">
$(function(){
	 
});

function mcRepoStandardDataPicker(rowDatas, fildId, fildName) {
	debugger;
	$("#" + fildId).val(rowDatas[0].id);
	$("#" + fildName).val(rowDatas[0].name);
	$("#standardCode").val(rowDatas[0].standardCode);
	$("#standardPath" ).val(rowDatas[0].path);
	$("#" + fildName).focus();
	 
		
}

</script>
</body>
</html>