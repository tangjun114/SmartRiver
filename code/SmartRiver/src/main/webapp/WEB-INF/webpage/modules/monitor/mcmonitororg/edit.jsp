<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>监督检测机构</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor,jqgrid" />
</head>

<body class="white-bg"  formid="mcMonitorOrgForm" beforeSubmit="beforeSubmit">
    <form:form id="mcMonitorOrgForm" modelAttribute="data"  method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="org"/>
		<%-- <input type="hidden" name="org.id" value="${data.org.id}}"/> --%>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>机构名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="name" htmlEscape="false" readonly="true" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>邮编:</label>
		            </td>
					<td class="width-35">
						<form:input path="zipCode" htmlEscape="false" class="form-control"  datatype="p"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>机构传真:</label>
		            </td>
					<td class="width-35">
						<form:input path="fax" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>机构地址:</label>
		            </td>
					<td class="width-35">
					
						<input name="latitude" class="form-control"  type="text" id="lat" value="${data.latitude}" style="display: none;"/>
						<input name="longitude"  class="form-control"  type="text" id="lng" value="${data.longitude}" style="display: none;"/>
						<input name="province"  class="form-control"  type="text" id="province" value="${data.province}" style="display: none;"/>
						<input name="city"  class="form-control"  type="text" id="city" value="${data.city}" style="display: none;"/>
						<input name="district"  class="form-control"  type="text" id="district" value="${data.district}" style="display: none;"/>
						
						<div style="width: 80%;float: left">
						<form:input path="address" htmlEscape="false" class="form-control"  datatype="*"   id="full_addr" />
						</div>
						<div>
						<button type="button" class="btn btn-primary" id="btn_gps">
						<i class="fa fa-map"></i></button></div>
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>机构网址:</label>
		            </td>
					<td class="width-35">
						<form:input path="url" htmlEscape="false" class="form-control t-read"  datatype="url"    />
		
					</td>
					<td  class="width-15 active text-right">	
		              <label>办公面积:</label>
		            </td>
					<td class="width-35">
						<form:input path="acreage" htmlEscape="false" class="form-control t-read"  datatype="n"    />
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>设备总数:</label>
		            </td>
					<td class="width-35">
						<form:input path="device" htmlEscape="false" class="form-control t-read"  datatype="n"    />
					</td>
					<td  class="width-15 active text-right">	
		              <label>机构邮箱:</label>
		            </td>
					<td class="width-35">
						<form:input path="email" htmlEscape="false" class="form-control"  datatype="e"    />
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>注册资金:</label>
		            </td>
					<td class="width-35">
						<form:input path="regCapital" htmlEscape="false" class="form-control t-read"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>联系人电话:</label>
		            </td>
					<td class="width-35">
						<form:input path="phone" htmlEscape="false" class="form-control"      />
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>技术人员:</label>
		            </td>
					<td class="width-35">
						<form:input path="artisan" htmlEscape="false" class="form-control t-read"      />
					</td>
					<td  class="width-15 active text-right">	
		              <label>机构座机号:</label>
		            </td>
					<td class="width-35">
						<form:input path="orgPhone" htmlEscape="false" class="form-control t-read"      />
					</td>
				</tr>
			
				<tr>
					<td  class="width-15 active text-right">	
		              <label>初级职称:</label>
		            </td>
					<td class="width-35">
						<form:input path="primaryJobTitle" htmlEscape="false" class="form-control t-read"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>中级职称:</label>
		            </td>
					<td class="width-35">
						<form:input path="midJobTitle" htmlEscape="false" class="form-control t-read"      />
						<label class="Validform_checktip"></label>
					</td>
					 
					
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>高级职称:</label>
		            </td> 
		            <td class="width-35">
						<form:input path="seniorJobTitle" htmlEscape="false" class="form-control t-read"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>技术总负责人:</label>
		            </td>
					<td class="width-35">
						<form:input path="chiefTechOfficer" htmlEscape="false" class="form-control t-read"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
					<tr>
					
					<td  class="width-15 active text-right">	
		              <label>法人代表:</label>
		            </td>
					<td class="width-35">
						<form:input path="representative" htmlEscape="false" class="form-control t-read"      />
						<label class="Validform_checktip"></label>
					</td>
				<td  class="width-15 active text-right ">	
		              <label>机构类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="type" htmlEscape="false" class="form-control t-disable"  dict="org_type"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
					<tr>
					
				 
				<td  class="width-15 active text-right">	
		              <label>企业性质:</label>
		            </td>
					<td class="width-35">
						<form:select path="companyType" htmlEscape="false" class="form-control"  dict="company_type"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>报告模板设置:</label>
		            </td>
					<td class="width-35">
						<form:checkboxes path="templatePerm" htmlEscape="false"   dict="pro_report_type"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr class="t-hide">
					<td  class="width-15 active text-right">	
		              <label>机构图片:</label>
		            </td>
					<td class="width-35">
						<form:fileinput showType="avatar" fileInputWidth="100px"  fileInputHeight="100px"  path="logo" htmlEscape="false" class="form-control t-read"      />
						<label class="Validform_checktip"></label>
					</td>
					
				</tr>
		      
		   </tbody>
		</table>   
	</form:form>
	<div class="row t-hide" >
        <div class="tabs-container">
            <ul class="nav nav-tabs">
            	<li class="active"><a data-toggle="tab" href="#tab_mcMonitorCertificate" aria-expanded="true">机构证书管理</a></li>
            </ul>
            <div class="tab-content">
                 <div id="tab_mcMonitorCertificate" class="tab-pane active">
                    <div class="panel-body">
                         <%-- <grid:grid id="mcMonitorCertificate"  datas="${mcMonitorCertificateList}"  gridShowType="form" pageable="false"  editable="true">
							    <grid:column label="证书编号"  name="code"  editable="true"        />
							    <grid:column label="证书名称"  name="name"  editable="true"        />
							    <grid:column label="图片"  name="imgPath"  editable="true"/>
						</grid:grid>  --%>
						 <grid:grid id="mcMonitorCertificate" url="${adminPath}/monitor/mcmonitorcertificate/ajaxList?monitorId=${data.id}">
							    <grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
							    <grid:column label="sys.common.imgPath" hidden="true"   name="imgPath" width="100"/>
							    <grid:column label="证书编号"  name="code"/>
							    <grid:column label="证书名称"  name="name"/>
							    <grid:column label="图片"  name="imgPath" formatter="button"/>
							   	<grid:toolbar function="create" url="/admin/monitor/mcmonitorcertificate/create?monitorId=${data.id}"/>
								<grid:toolbar function="update"/>
								<grid:toolbar function="delete"/>
						</grid:grid> 
						<script type="text/javascript">
						mcMonitorCertificateImgPathFormatter
						function mcMonitorCertificateImgPathFormatter(value, options, row){
							  try{
							         if(!row.id) {
						                 return '';
						             }
							         var href="<a href='${adminPath}/"+row.imgPath+"' target='_blank'";
						  				href +="><i class=\"fa fa-file\"></i>&nbsp查看</a>&nbsp&nbsp";
							    	return href;
							   }catch(err){}
							   return value;
							}
						</script>
					</div>
                </div>
          
            </div>
        </div>
    </div>
<html:js name="bootstrap-fileinput" />
<html:js name="ff" />
<html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline,layer" />
<script>
	$(document).ready(function () {
		var type = '${data.type}';
		//alert(type);
		if (type == 'supervise') {

		}
		$(".t-hide").hide();
     	$(".t-read").attr("readonly","readonly");
     	$(".t-disable").attr("disabled", "disabled");
     	///$('input').removeAttr("readonly");
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	    	 resizeGrid();
		});
	});
	$(function(){
	   $(window).resize(function(){   
		   resizeGrid();
	   });
	});
	function resizeGrid(){
		 $("#mcMonitorCertificateGrid").setGridWidth($(window).width()-60);
	}

	/**
	*提交回调方法
	*/
	function beforeSubmit(curform){
		 //这里最好还是使用JSON提交，控制器改变提交方法，并使用JSON方式来解析数
		 //通过判断，如果有问题不提交
		 if(!initGridFormData(curform,"mcMonitorCertificate"))return false;
		 return true;
	}
	
	function initGridFormData(curform,gridId){
		 var rowDatas =getRowDatas(gridId+"Grid");
		 var rowJson = JSON.stringify(rowDatas);
		 if(rowJson.indexOf("editable") > 0&&rowJson.indexOf("inline-edit-cell") )
	     {
	    	 return false;
	     }
		 var gridListJson=$('#'+gridId+"ListJson").val();
		 if(gridListJson==undefined||gridListJson==''){
			 var rowInput = $('<input id="'+gridId+'ListJson" type="hidden" name="'+gridId+'ListJson" />');  
			 rowInput.attr('value', rowJson);  
			 // 附加到Form  
			 curform.append(rowInput); 
		 }else{
			 // $('#'+gridId+"ListJson").val(rowJson);
		 }
		 return true;
	}
	$("#btn_gps").on('click', function(e) {

		var lat = '${data.latitude}';
		var lng = '${data.longitude}';
		var url = "${staticPath}/map/bdmap.html?lat="+lat+"&lng="+lng;
		layer.open({
			  type: 2,
			  title: false,
			  area: ['630px', '360px'],
			  closeBtn: false,
			  shade: 0.8,
			  btn: ['确定', '关闭'],
			  content: url,
			  success: function(layero){
				    var btn = layero.find('.layui-layer-btn');

				  },
		 	  yes: function(index, layero){
					 //$(window.frames["iframeName"].document).find("#newlat").html()
		 		    //debugger;
		 		    var newlat = ff.cache.get('newlat');
		 		    var newlng = ff.cache.get('newlng');
		 		    var newadrr = ff.cache.get('newaddr');
		 		    var province = ff.cache.get('newprovince');
		 		    
		 		    var city = ff.cache.get('newcity');
		 		    var district = ff.cache.get('newdistrict');

					$("#lat").val(newlat);
					$("#lng").val(newlng);
					$("#full_addr").val(newadrr);
					$("#province").val(province);
					$("#city").val(city);
					$("#district").val(district);
		 		    //console.log(res);
				    layer.close(index); //如果设定了yes回调，需进行手工关闭
				    
				  }
			}); 

	});
</script>
</body>
</html>