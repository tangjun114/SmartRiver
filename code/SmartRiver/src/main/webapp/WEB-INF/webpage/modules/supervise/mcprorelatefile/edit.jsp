<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目相关文件</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProRelateFileForm">
    <form:form id="mcProRelateFileForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>文件名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="fileName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>文件类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="fileType" htmlEscape="false" class="form-control"  dict="relate_file_type"      />
						<label class="Validform_checktip"></label>
					</td>
		  		</tr>
		      	<tr>
<%-- 					<td  class="width-15 active text-right">	
		              <label>上传者:</label>
		            </td>
					<td class="width-35">
						<form:input path="uploadUserName" htmlEscape="false" class="form-control"    readonly="true"  />
						<label class="Validform_checktip"></label>
					</td> --%>
					<td  class="width-15 active text-right">	
		              <label>备注:</label>
		            </td>
					<td class="width-35">
						<form:input path="remarks" htmlEscape="false" class="form-control"   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label></label>
		            </td>
					<td class="width-35">

					</td>
		  		</tr>
				<tr>
				<td  class="active text-right">	
		              <label>附件:</label>
		            </td>
					<td colspan="3">
						<form:fileinput path="filePath" htmlEscape="false" class="form-control"     showType="file"  />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>