<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>项目监控-实施日志</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemImplementLogForm">
    <form:form id="mcProItemImplementLogForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="projectId"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>日志类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="implementLogType" htmlEscape="false" class="form-control"  dict="implementLogType"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>可见类型:</label>
		            </td>
					<td class="width-35">
						<form:select path="visibleType" htmlEscape="false" class="form-control"  dict="visibleType"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>附件:</label>
		            </td>
					<td class="width-35" colspan="4">
						<form:fileinput path="filePath" htmlEscape="false" class="form-control"    showType="file" maxFileCount="5" 
						saveType="filepath" idField="id"/>
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>图片:</label>
		            </td>
		            <td colspan="4" class="width-35"><form:fileinput
							showType="file" extend="jpg,png,gif" maxFileCount="5"
							fileInputWidth="100px" fileInputHeight="100px" path="imgPath"
							htmlEscape="false" class="form-control" saveType="filepath" idField="id"/> 
							<label	class="Validform_checktip"></label>
					</td>

				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>内容:</label>
		            </td>
					<td class="width-35" colspan="4">
						<form:textarea path="content" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>

		  		</tr>
		      
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>