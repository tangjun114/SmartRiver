<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目三维图</title>
  <meta name="decorator" content="list"/>
  	<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="${staticPath}/vendors/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/jquery.fancybox.pack.js?v=2.1.7"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="${staticPath}/vendors/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

</head>
<body >
<div class="row">
		<a onclick="showDataList()" class="btn btn-primary btn-sm"><i
							class="fa fa-arrow-circle-up"></i>&nbsp;&nbsp;编辑</a> 
			<div id="carousel-example-generic" class="carousel slide"
				data-ride="carousel">
				<!-- Indicators -->
<!-- 				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0"
						class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol> -->

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox" id="slideDiv"></div>

				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic"
					role="button" data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#carousel-example-generic"
					role="button" data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>

<html:js name="func" />
<html:js name="ff" />
<script type="text/javascript">  
var winH;
var newH;
//注意：下面的代码是放在iframe引用的子页面中调用
$(window).resize(function() {
	if(window.innerHeight){
		winH=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winH=document.body.clientHeight;
		}
	newH = winH - 80; 
	$(".ibox-title").css("cssText","display: none;");
	$("#slideDiv").css("cssText","height: "+newH+"px!important;");
	$('.fancybox').fancybox({
		afterLoad: function () {
	        $(".fancybox-wrap,.fancybox-mobile,.fancybox-type-image, .fancybox-opened").css("z-index", 19898830);
	        $(".fancybox-opened,.fancybox-overlay").css("z-index", 19898810);
	    }
	});

	load();
});

function load()
{
	//alert('${param.projectId}');
	ff.util.submit({
			url : "supervise/mcproitemscene3dimg/loadall?projectId=${param.projectId}",
			filter : {
				projectId: '${param.projectId}'
			},
			success : showPager
		});
}

function showPager(rsp){
	var slideDiv = $("#slideDiv");
	var newHtml= '';
	if(null != rsp && rsp.obj)
	{				
		$.each(rsp.obj, function(index, element) {
			if(index == 0){
				newHtml +="<div class=\"item active\" style=\"text-align=center;\">";
			} else {
				newHtml +="<div class=\"item \" style=\"text-align=center;\">";
			}
			newHtml +="<a class=\"fancybox\" href=\"";
			newHtml += rootUrl() + "/" + element.imgPath;
			newHtml +="\" data-fancybox-group=\"gallery\" title=\"\">";			
			newHtml +="<img src=\"";
			newHtml += rootUrl() + "/" + element.imgPath;
			newHtml +="\" alt=\"暂无图片\" />";
			newHtml +="</a>";
			newHtml +="<p class=\"text-center\" >";
			newHtml +=element.remarks;
			newHtml +="</p></div>";
	    });
		slideDiv.html(newHtml);
		slideDiv.each(function(){
		    //获取对应img的高度
		    //将img高度赋给对应的div
		    $(this).find("img").css("height",newH-50);
		    $(this).find("img").css("margin","0 auto");
		    $(this).find("p").css("height",50);
		    $(this).find("p").css("overflow-y","auto");
		});
	}
}

function showDataList(){
	var url = "${adminPath}/supervise/mcproitemscene3dimg/datalist";
	top.layer.open({
		type : 2,
		area: ['630px', '400px'],
		title : '编辑',
		maxmin : true, // 开启最大化最小化按钮
		content : url,
		btn : [ '确定', '关闭' ],
		yes : function(index, layero) {

		},
		cancel : function(index) {
		}
	});
}
</script>
</body>
</html>