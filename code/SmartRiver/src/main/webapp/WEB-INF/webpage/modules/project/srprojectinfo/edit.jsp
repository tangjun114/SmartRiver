<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>项目信息</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput"/>
    <html:css name="simditor"/>
</head>

<body class="white-bg" formid="srProjectInfoForm">
<form:form id="srProjectInfoForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>
            <td class="width-15 active text-right">
                <label>备注信息:</label>
            </td>
            <td class="width-35">
                <form:input path="remarks" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>项目名称:</label>
            </td>
            <td class="width-35">
                <form:input path="name" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>项目描述:</label>
            </td>
            <td class="width-35">
                <form:input path="desc" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>项目地址:</label>
            </td>
            <td class="width-35">
                <form:input path="addr" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>负责人名称:</label>
            </td>
            <td class="width-35">
                <form:input path="dutyUserName" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>项目类别:</label>
            </td>
            <td class="width-35">
                <form:select path="type" htmlEscape="false" class="form-control" dict="projet_info_type"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>地图经度:</label>
            </td>
            <td class="width-35">
                <form:input path="longitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>地图纬度:</label>
            </td>
            <td class="width-35">
                <form:input path="latitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-35">
                <button type="button" class="btn btn-primary" id="btn_gps">
                    <i class="fa fa-map"></i></button>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>项目状态:</label>
            </td>
            <td class="width-35">
                <form:select path="status" htmlEscape="false" class="form-control" dict="project_status"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>进展情况:</label>
            </td>
            <td class="width-35">
                <form:input path="progress" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>责任单位:</label>
            </td>
            <td class="width-35">
                <form:input path="dutyOrg" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>建设单位:</label>
            </td>
            <td class="width-35">
                <form:input path="buildOrg" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>设计单位:</label>
            </td>
            <td class="width-35">
                <form:input path="designOrg" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>监理单位:</label>
            </td>
            <td class="width-35">
                <form:input path="monitorOrg" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>建设规模:</label>
            </td>
            <td class="width-35">
                <form:input path="scale" htmlEscape="false" class="form-control"/><p>公里</p>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>项目资金来源:</label>
            </td>
            <td class="width-35">
                <form:input path="amtSource" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>资金投入:</label>
            </td>
            <td class="width-35">
                <form:input path="amt" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>开工时间:</label>
            </td>
            <td class="width-35">
                <form:input path="startTime" datefmt="yyyy-MM-dd hh:mm:ss" class="form-control layer-date"
                            placeholder="YYYY-MM-DD hh:mm:ss"
                            onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>

        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline,layer"/>>
<html:js name="ff"/>
<script type="text/javascript">

    $(function () {

        $("#btn_gps").on('click', function (e) {
            var lat = '${data.latitude}';
            var lng = '${data.longitude}';
            ff.service.map(lat, lng, function (obj) {
                $("#latitude").val(obj.lat);
                $("#longitude").val(obj.lng);
            });
        });

        var today = new Date().format("yyyy-MM-dd");
        var day = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
        if ("" == document.getElementById("startTime").value) {
            document.getElementById("startTime").value = today + " 23:59:59";
        }

    });



</script>
</body>
</html>