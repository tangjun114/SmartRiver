<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
 
  <meta name="decorator" content="list"/>
</head>
<body  >
<div class="row">
  <div class="col-md-7 col-sm-7">
	<grid:grid id="mcProItemRealTimeMonDataDetailGridId"   rowNum="200" url="${adminPath}/supervise/mcproitemrealtimemondata/detail/ajaxList?id=${id}" multiselect="false">
		<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	 	
	  	<grid:column label="深度(m)"  name="depthExt" />
	  	<grid:column label="本次结果(mm)"  name="nowValue" formatter="number" formatoptions="{decimalPlaces:1}" />
	 	<grid:column label="本次变化(mm)"  name="minusValue" formatter="number" formatoptions="{decimalPlaces:1}" />
	 	<grid:column label="累计变化(mm)"  name="sumValue" formatter="number" formatoptions="{decimalPlaces:1}"  />
	    <grid:column label="变化速率(mm/d)"  name="changeRate" formatter="currency"  />
	 </grid:grid>
 
 </div>
  
   <div class="col-md-5 col-sm-5" style=" height:440px;" >
      
      <div id="dataChart"  style="height:440px">
     
      </div>
   </div>
</div>
<html:js name="echarts" />
<html:js name="ff" />
	
 
<script type="text/javascript">
  $(function()
   {
	  $(".ibox .ibox-title").css("cssText","display: none");  
	  $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+350+"px!important;");
	  ff.util.submit({
		  	url:"supervise/mcproitemrealtimemondata/detail/chart?id=${id}",
		      data:"${id}",
		      success:loadChart
		      });
  });
  
 

function loadChart(rsp)
{
	debugger;
	var temp = rsp.obj;

	 var option   = {
			    title:{text:"曲线图"},
			    legend: {
			        data:['']
			    },
			    toolbox: {
			        show : false,
 			    },
			    calculable : true,
			    tooltip : {
			        trigger: 'axis',
			        formatter:   function(a,b,c){
			        	var val =parseFloat(a[0].value).toFixed(1);
			        	return "深度: " + a[0].name +"<br>"+"累计值 : " + val ;
			        }//"累计值 :{c} <br/> 深度: {b}"
			    },
			    xAxis : [
			        {
			        	name:"累计值 (mm)",
			        	precision: 1, 
			        	position:'top',
			        	//inverse:true,
			            type : 'value',
 			        }
			    ],
			    yAxis : [
			        {
			        	 
			        	name:"深度(m)",
			        	position:'right',
			            type : 'category',
			            inverse:true,
			            axisLine : {onZero: false},
 			            boundaryGap : false,
			            data : temp.y_data,
			        }
			    ],
			    series : [
			        {
			            name:'',
			            type:'line',
			            smooth:true,
			            itemStyle: {
			                normal: {
			                    lineStyle: {
			                        shadowColor : 'rgba(0,0,0,0.4)'
			                    }
			                }
			            },
			            data: temp.x_data
			        }
			    ]
			};
		var chart =  echarts.init($("#dataChart")[0]);
		chart.setOption(option,true);      
	//ff.chart.load("#dataChart","监测数据成果曲线图",temp,"line", option);
}
 
</script>
</body>
</html>