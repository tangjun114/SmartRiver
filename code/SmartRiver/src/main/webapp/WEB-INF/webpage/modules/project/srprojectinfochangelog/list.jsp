<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>工程信息变动调整记录列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="工程信息变动调整记录">
<grid:grid id="srProjectInfoChangeLogGridId" url="${adminPath}/project/srprojectinfochangelog/ajaxList?id=${id}">
    <grid:column label="记录时间" name="createDate"/>
    <grid:column label="项目名称" name="name" query="true" queryMode="input" condition="like"/>
    <grid:column label="项目地址" name="addr"/>
    <grid:column label="负责人名称" name="dutyUserName" query="true" queryMode="input" condition="like"/>
    <grid:column label="项目类别" name="type" query="true" queryMode="select" dict="projet_info_type"/>
    <grid:column label="项目状态" name="status" dict="project_status" query="true" queryMode="select"/>
    <grid:column label="进展情况" name="progress"/>
    <grid:column label="责任单位" name="dutyOrg"/>
    <grid:column label="建设单位" name="buildOrg"/>
    <grid:column label="设计单位" name="designOrg"/>
    <grid:column label="监测单位" name="monitorOrg"/>
    <grid:column label="资金来源" name="amtSource"/>
    <grid:column label="资金投入" name="amt"/>
    <grid:column label="建设规模" name="scale"/>
    <grid:column label="开工时间" name="startTime"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>