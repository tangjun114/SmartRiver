<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>巡检管理列表</title>
  <meta name="decorator" content="list"/>
</head>
<body >
<grid:grid id="mcMonitorPatrolLogGridId" url="${adminPath}/monitor/mcmonitorpatrollog/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>

	<grid:column label="sys.common.path" hidden="true"  name="id" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="name" width="100"/>
	

    <grid:column label="巡检日期"  name="patrolDate"  query="true"  queryMode="date"  condition="eq" />
    <grid:column label="巡检人"  name="rummagerName"  query="true"  queryMode="input"  condition="like" width="70"/>
    <grid:column label="地址"  name="address" />
    <grid:column label="巡检详情"  name="id"  formatter="button" width="70"/>
    <grid:column label="状态"  name="status"  query="true"  hidden="true" queryMode="select"  condition="eq"  dict="mc_monitor_patrol_status"/>
    <grid:column label="巡检结论"  name="summary" />
    <grid:column label="巡检总体情况"  name="overallSituation" />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="50"/>
    <grid:button groupname="opt" function="delete" />
    	
	<grid:toolbar function="create" url="${adminPath}/monitor/mcmonitorpatrollog/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="func" />
<html:js name="ff" />
<script type="text/javascript"> 
var winH;
//注意：下面的代码是放在iframe引用的子页面中调用
$(window).resize(function() {
	if(window.innerHeight){
 		winH=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winH=document.body.clientHeight;
		}
	var newH2 = winH - 215; 
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH2+"px!important;");
});
$(document).ready(function() {

	$(".ibox .ibox-title").css("cssText","display: none"); 
});
function mcMonitorPatrolLogGridIdIdFormatter(value, options, row){
	  try{
	         if(!row.id) {
             return '';
         } 
	         var href="<a href=\"#\" class=\"btn btn-danger\"";
	           href +="onclick=\"viewDetail('"+row.id+"')\"";
					href +=">&nbsp 查看详情</a>&nbsp&nbsp";
		}catch(err){}
	   return href;
	}
function viewDetail(id){
	var url = "${adminPath}/monitor/mcmonitorpatroldetail?patrolId="+id;
	var width = "70%";
	var height = "80%";
	if (navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)) {// 如果是移动端，就使用自适应大小弹窗
		width = 'auto';
		height = 'auto';
	} else {// 如果是PC端，根据用户设置的width和height显示。

	}
	//window.open(url);
	top.layer.open({
		type : 2,
		area : [ width, height ],
		title : "巡检详情",
		maxmin : true, // 开启最大化最小化按钮
		content : url,
	 
		cancel : function(index) {
		}
	 });
}
</script>
</body>
</html>