<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>规范资料列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="规范资料">
<grid:grid id="mcRepoStandardGridId" url="${adminPath}/repo/mcrepostandard/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:column label="sys.common.path" hidden="true"  name="path" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="name" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="规范编号"  name="standardCode"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="规范名称"  name="name"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="下载"  name="operate"  query="true"  formatter="button" />
   
    <grid:column label="上传人"  name="createByName" />
    <grid:column label="更新日期"  name="updateDate" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="func" />
<script type="text/javascript">
function mcRepoStandardGridIdOperateFormatter(value, options, row){
	  try{
	         if(!row.id) {
               return '';
           }
	         var href=linkFormatter(row.name,row.path);
	   }catch(err){}
	   return href;
	}
</script>
</body>
</html>