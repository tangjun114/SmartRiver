<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>抽查记录</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemInspectionForm">
    <form:form id="mcProItemInspectionForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				 
		       <tr>
					<td  class="width-15 active text-right">	
		              <label>开始:</label>
		            </td>
					<td colspan="4" class="width-35"><form:fileinput
							showType="file" extend="jpg,png,gif" maxFileCount="10"
							fileInputWidth="100px" fileInputHeight="100px" path="startFile"
							htmlEscape="false" class="form-control" saveType="filepath" idField="id"/> <label
						class="Validform_checktip"></label></td>
					
			</tr>
				<tr>
				   <td  class="width-15 active text-right">	
		              <label>时间:</label>
		            </td>
					<td class="width-35">
						<form:input path="startTime" datefmt="yyyy-MM-dd hh:mm:ss"  htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>抽查:</label>
		            </td>
					<td colspan="4" class="width-35"><form:fileinput
							showType="file" extend="jpg,png,gif" maxFileCount="10"
							fileInputWidth="100px" fileInputHeight="100px" path="inspectFile"
							htmlEscape="false" class="form-control" saveType="filepath" idField="id"/> <label
						class="Validform_checktip"></label></td>
			   </tr>
				<tr>
				   <td  class="width-15 active text-right">	
		              <label>时间:</label>
		            </td>
			 		<td class="width-35">
						<form:input path="inspectTime" datefmt="yyyy-MM-dd hh:mm:ss"  htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>结束:</label>
		            </td>
					<td colspan="4" class="width-35"><form:fileinput
							showType="file" extend="jpg,png,gif" maxFileCount="10"
							fileInputWidth="100px" fileInputHeight="100px" path="endFile"
							htmlEscape="false" class="form-control" saveType="filepath" idField="id"/> <label
						class="Validform_checktip"></label></td>
			  </tr>
				<tr>
				   <td  class="width-15 active text-right">	
		              <label>时间:</label>
		            </td>
					<td class="width-35">
						<form:input path="endTime" datefmt="yyyy-MM-dd hh:mm:ss" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>