<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>监测分区</title>
  <meta name="decorator" content="list"/>
</head>
<body title="监测分区">
<grid:grid id="mcSmMonitorSubareaGridId" url="${adminPath}/sm/mcsmmonitorsubarea/ajaxList?projectId=${projectId}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="监测分区"  name="subareaName"  query="true"  queryMode="input"  condition="like" />
	<grid:toolbar function="create" url="${adminPath}/sm/mcsmmonitorsubarea/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<script type="text/javascript">  
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-270;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});
</script>
</body>
</html>