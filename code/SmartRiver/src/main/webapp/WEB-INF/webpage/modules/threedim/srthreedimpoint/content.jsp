<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <title>首页</title>
	<meta name="keywords" content="<spring:message code="sys.site.keywords" arguments="${platformName}"/>">
	<meta name="description" content="<spring:message code="sys.site.description" arguments="${platformName}"/>">
    <html:css  name="favicon,bootstrap,font-awesome,animate"/>
    
    <link href="${staticPath}/common/css/style.css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<style type="text/css">
		body, html{
			width: 100%;
			height: 100%;
			margin:0;
			font-family:"微软雅黑";
			font-size:14px;
		}
		#l-map {
			width:100%; 
			height:100%;
			overflow: hidden;
		}
		#result{
			width:100%;
		}
		li{
			line-height:28px;
		}
		.cityList{
			height: 320px;
			width:372px;
			overflow-y:auto;
		}
		.sel_container{
			z-index:9999;
			font-size:12px;
			position:absolute;
			right:0px;
			top:0px;
			width:140px;
			background:rgba(255,255,255,0.8);
			height:30px;
			line-height:30px;
			padding:5px;
		}
		.map_popup {
			position: absolute;
			z-index: 200000;
			width: 382px;
			height: 344px;
			right:0px;
			top:40px;
		}
		.map_popup .popup_main { 
			background:#fff;
			border: 1px solid #8BA4D8;
			height: 100%;
			overflow: hidden;
			position: absolute;
			width: 100%;
			z-index: 2;
		}
		.map_popup .title {
			background: url("http://map.baidu.com/img/popup_title.gif") repeat scroll 0 0 transparent;
			color: #6688CC;
			font-weight: bold;
			height: 24px;
			line-height: 25px;
			padding-left: 7px;
		}
		.map_popup button {
			background: url("http://map.baidu.com/img/popup_close.gif") no-repeat scroll 0 0 transparent;
			cursor: pointer;
			height: 12px;
			position: absolute;
			right: 4px;
			top: 6px;
			width: 12px;
		}	
	</style>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=QbzDKD2x6qHqkYqweGxBcwgGbnGodW4A"></script>
	<!-- 加载百度地图样式信息窗口 -->
		<!-- 加载城市列表 -->
	<script type="text/javascript" src="http://api.map.baidu.com/library/CityList/1.2/src/CityList_min.js"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/library/SearchInfoWindow/1.5/src/SearchInfoWindow_min.js"></script>
	
	<link rel="stylesheet" href="http://api.map.baidu.com/library/SearchInfoWindow/1.5/src/SearchInfoWindow_min.css" />
	<script type="text/javascript" src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>    
    <link rel="stylesheet" href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />    
    
</head>

<body class="gray-bg" >
    <div class="row  border-bottom white-bg dashboard-header" style="height:100%;overflow-y:scroll; ">
		
			<div class="row">
				<div class="col-sm-12">
				
					<div class="form-inline">
					<form id="ff_filter" method="post">
						<input filter="like" name="name" class="form-control" placeholder="输入项目名称" />
						<a onclick="loadProjects()" class="btn btn-primary btn-sm"><i
							class="fa fa-arrow-circle-up"></i>&nbsp;&nbsp;查询</a> <a
							onclick="location.href=location.href;"
							class="btn btn-primary btn-sm"><i class="fa fa-remove"></i>&nbsp;&nbsp;清空</a>
							<div class="sel_container">
			<strong id="curCity">广东省</strong> [<a id="curCityText"
				href="javascript:void(0)">更换城市</a>]
		</div>
				</form>
		<div class="map_popup" id="cityList" style="display: none;margin-right:100px;">
			<div class="popup_main">
				<div class="title">城市列表</div>
				<div class="cityList" id="citylist_container"></div>
				<button id="popup_close"></button>
			</div>
		</div>
					</div>
				</div>
			</div>

		
		<div id="l-map"></div>
	<!--城市列表-->

    </div>

 	<!-- 全局js -->
	<html:js  name="jquery,bootstrap"/>

	<!-- 自定义js -->
	<script src="${staticPath}/common/js/content.js"></script>
	

<html:js name="ff" />

<script type="text/javascript">
	var sContent = "<h3>title</h3>"
	var map = new BMap.Map("l-map", {});                        // 创建Map实例
	var infoWindow = new BMap.InfoWindow(sContent);  // 创建信息窗口对象
	 
	map.centerAndZoom(new BMap.Point(114.124933, 22.532058), 15);     // 初始化地图,设置中心点坐标和地图级别
	map.enableScrollWheelZoom();                        //启用滚轮放大缩小
	//map.centerAndZoom('广东省');
	// 创建CityList对象，并放在citylist_container节点内
	var myCl = new BMapLib.CityList({container : "citylist_container", map : map});
    //给城市点击时，添加相关操作
    myCl.addEventListener("cityclick", function (e) {
        //修改当前城市显示
        document.getElementById("curCity").innerHTML = e.name;
        //点击后隐藏城市列表
        document.getElementById("cityList").style.display = "none";
    });
    
    map.addControl(new BMap.MapTypeControl()); 
    
    
    var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
    var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
    var top_right_navigation = new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_SMALL}); //右上角，仅包含平移和缩放按钮
    /*缩放控件type有四种类型:
    BMAP_NAVIGATION_CONTROL_SMALL：仅包含平移和缩放按钮；BMAP_NAVIGATION_CONTROL_PAN:仅包含平移按钮；BMAP_NAVIGATION_CONTROL_ZOOM：仅包含缩放按钮*/
    
    //添加控件和比例尺
  
     map.addControl(top_left_control);      // 测距离的   
     map.addControl(top_left_navigation);   // 左导航默认  
    // map.addControl(top_right_navigation);  // 右导航平移与缩放 
    
    
    var stCtrl = new BMap.PanoramaControl();  
	stCtrl.setOffset(new BMap.Size(20, 80));  
	map.addControl(stCtrl);
     
     var myDrawingManagerObject = new BMapLib.DrawingManager(map, {isOpen: true, 
    	    drawingType: BMAP_DRAWING_MARKER, enableDrawingTool: true,
    	    enableCalculate: true,
    	    drawingToolOptions: {
    	        anchor: BMAP_ANCHOR_TOP_RIGHT,
    	        offset: new BMap.Size(5, 30),
    	        drawingTypes : [
    	            BMAP_DRAWING_MARKER,
    	            BMAP_DRAWING_CIRCLE,
    	            BMAP_DRAWING_POLYLINE,
    	            BMAP_DRAWING_POLYGON,
    	            BMAP_DRAWING_RECTANGLE 
    	         ]
    	    },
    	    polylineOptions: {
    	        strokeColor: "#333"
    	    }});
     map.addControl(myDrawingManagerObject);      // 测距离的   
     
    
    
    
    
    
    
    
	loadProjects();
	// 给“更换城市”链接添加点击操作
	document.getElementById("curCityText").onclick = function() {
		var cl = document.getElementById("cityList");
		if (cl.style.display == "none") {
			cl.style.display = "";
		} else {
			cl.style.display = "none";
		}	
	};
	// 给城市列表上的关闭按钮添加点击操作
	document.getElementById("popup_close").onclick = function() {
 		var cl = document.getElementById("cityList");
		if (cl.style.display == "none") {
			cl.style.display = "";
		} else {
			cl.style.display = "none";
		} 
	};
	function loadProjects(){
		ff.req.filter = ff.com.getFilterObj("#ff_filter");
		 ff.util.submit({
		    	url:"threedim/srthreedimpoint/loadall",
	           success:addPots
	             });

	}
	function addPots(rsp){
		if(null != rsp && rsp.obj)
		{	
			if (document.createElement('canvas').getContext) {  // 判断当前浏览器是否支持绘制海量点
			    var points = [];  // 添加海量点数据
			    map.clearOverlays(); 
				$.each(rsp.obj, function(index, element) {
					//alert(element.name);
					 points.push(new BMap.Point(element.longitude, element.latitude));
			    });

			    var options = {
			        size: BMAP_POINT_SIZE_BIG,
			        shape: BMAP_POINT_SHAPE_STAR,
			        color: 'red'
			    }
			    var pointCollection = new BMap.PointCollection(points, options);  // 初始化PointCollection
			    map.addOverlay(pointCollection);  // 添加Overlay
/* 			    pointCollection.addEventListener('click', function (e) {
			    	debugger;
				      //alert('单击点的坐标为2：' + e.point.lng + ',' + e.point.lat);  // 监听点击事件
					$.each(rsp.obj, function(index, element) {
						if(element.longitude == e.point.lng && element.latitude == e.point.lat) {
							openInfo(element);
							return false;
						}
			    	});
					
				}); */
			    pointCollection.addEventListener("mouseover", function(e){            
					$.each(rsp.obj, function(index, element) {
						if(element.longitude == e.point.lng && element.latitude == e.point.lat) {
							openInfo(element);
							return false;
						}
			    	}); //开启信息窗口  
		        });  
/* 			    pointCollection.addEventListener("mouseout", function(e){            
		            map.closeInfoWindow(infoWindow,point); //开启信息窗口  
		        });   */
			} else {
				alert('请在chrome、safari、IE8+以上浏览器查看本示例');
			}

		}
	}
	var opts = {
		width : 250, // 信息窗口宽度
		height : 160, // 信息窗口高度
		title : "实景信息", // 信息窗口标题
		enableMessage : true
	//设置允许信息窗发送短息
	};
	function openInfo(obj) {
		var content = "<p>名称："+obj.name+"</p>";
 		 
		content +="<div><input   type=\"button\"   value=\"查看全景\"   onclick=\"viewMonitorItem('"+obj.id+"','"+obj.name+"','"+obj.url+"')\"/></div>"
		var point = new BMap.Point(obj.longitude, obj.latitude);
		var infoWindow = new BMap.InfoWindow(content, opts); // 创建信息窗口对象 
		map.openInfoWindow(infoWindow, point); //开启信息窗口
	}
	function viewMonitorItem(id,name,url){
 
		ff.service.overall(url);
	 
	}
</script>
</body>
</html>