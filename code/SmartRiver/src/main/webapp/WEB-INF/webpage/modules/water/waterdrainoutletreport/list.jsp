<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>排污口检测报表列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="排污口检测报表">
<grid:grid id="waterDrainOutletReportGridId" url="${adminPath}/water/waterdrainoutletreport/ajaxList">
    <grid:column label="排污口名称"  name="outletName" />
    <grid:column label="监控类型"  name="monitorType" />
    <grid:column label="水质"  name="waterQuality" />
    <grid:column label="次数"  name="count" />

	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>