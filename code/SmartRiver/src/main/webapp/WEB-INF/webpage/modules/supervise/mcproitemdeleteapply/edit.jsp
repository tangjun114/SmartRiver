<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>原始文件删除申请</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemDeleteApplyForm">
    <form:form id="mcProItemDeleteApplyForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>申请理由:</label>
		            </td>
					<td class="width-35" colspan="4">
						<form:textarea path="reason" rows="4" htmlEscape="false" class="form-control"    readonly="true"  />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
<%-- 		      	<tr>
					<td  class="width-15 active text-right">	
		              <label>申请文件:</label>
		            </td>
					<td class="width-35" colspan="4">
						<form:fileinput path="applyFile" htmlEscape="false" class="form-control"    showType="file" maxFileCount="5" 
						saveType="filepath" idField="id"/>
						<label class="Validform_checktip"></label>
					</td>
				</tr> --%>
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>