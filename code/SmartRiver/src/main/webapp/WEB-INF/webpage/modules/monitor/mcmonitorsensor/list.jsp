<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>传感器信息列表</title>
  <meta name="decorator" content="list"/>
    <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
</head>
<body title="传感器信息">
 
<grid:grid id="mcMonitorSensorGridId" url="${adminPath}/monitor/mcmonitorsensor/ajaxList?projectId=${projectId}&sensorTypeName=${sensorTypeName}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>

     <grid:column label="传感器编号"  name="sensorNum"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="传感器类型"  name="sensorTypeName" query="true"  queryMode="select"  condition="eq"  dict="0" />
    <grid:column label="传感器型号"  name="sensorModelName" />
    <grid:column label="精度"  name="accuracy" />
    <grid:column label="标定系数(K)"  name="caliCoefficient" />
    <grid:column label="初始值"  name="initialValue" />
    <grid:column label="温补系数"  name="tempCoefficient" />
    <grid:column label="使用状态"  name="status"  query="true"  queryMode="select"  condition="eq"  dict="SBXX_SBSFSY"/>
    <grid:column label="测点编号"  name="measurePointCode"   />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button title="sys.common.delete"  groupname="opt" function="delete"   />
	
	<grid:toolbar function="create" url="${adminPath}/monitor/mcmonitorsensor/create?projectId=${projectId}"/>
	<grid:toolbar title="复制" icon="fa-database"  function="updateDialog" url="${adminPath}/monitor/mcmonitorsensor/{id}/copy?projectId=${projectId}"  />
	<grid:toolbar function="update"/>

	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="ff" /> 
 
<script type="text/javascript">
$(function () {
	debugger;
	var sensorTypeName = ff.util.urlPara("sensorTypeName");
	loadSensorType(sensorTypeName);
	
});
    function loadSensorType(select)
    {
    	var selector= $("select[name=sensorTypeName]");  
    	
    	ff.form.combox.load(selector,{
    	     url: "sm/mcsmparamconfig/sensorbytype",
    	     valueField: "name",
    	     labelField: "name",
    	     select:select,
    	     callback:function()
    	     {
    	    	 
    	     }
    	});
    	
    }
	   var winHeight;
	   $(document).ready(function()   
	   {  
	   	//获取窗口高度 兼容各个浏览器
	   	if(window.innerHeight){
	   		winHeight=window.innerHeight;
	   		}
	   	else if((document.body)&&(document.body.clientHeight)){
	   		winHeight=document.body.clientHeight;
	   		}

	   	var newH = winHeight-250;
	   	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
	   }); 
 </script>
</body>
</html>