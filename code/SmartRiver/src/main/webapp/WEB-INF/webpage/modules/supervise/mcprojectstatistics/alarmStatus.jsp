<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>警报图</title>
    <meta name="decorator" content="single"/>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet box  portlet-grey">
                	<div class="col-sm-6">
                        <div class="echarts" id="echarts-bar-chart" style="height:400px"></div>
                    </div>
					<div class="col-sm-6">
                        <div class="echarts" id="echarts-pie-chart" style="height:400px"></div>
                    </div>
                </div>
            </div>
         </div> 
    </div>
    
	<!-- 全局js -->
<html:js name="echarts" />
	 
<html:js name="ff" />
<script type="text/javascript">
var winHeight;
	$(document).ready(function() {

		if(window.innerHeight){
			winHeight=window.innerHeight;
			}
		else if((document.body)&&(document.body.clientHeight)){
			winHeight=document.body.clientHeight;
			}
		var newH = winHeight-40;
		$("#echarts-bar-chart").css("cssText","height: "+newH+"px!important;");
		$("#echarts-pie-chart").css("cssText","height: "+newH+"px!important;");
  		load();
	});
	function load() {

		ff.util.submit({
				url:"supervise/mcprojectstatistics/by",
				data:'status',
				success:function(rsp){
					if(null != rsp && rsp.obj)
					{	
				     ff.chart.load("#echarts-bar-chart","",rsp.obj,"bar");	 
 					 ff.chart.load("#echarts-pie-chart","状态项目统计",rsp.obj,"pie");
						
					}
				}
			
			});
	}
	 
	
	</script>
</body>

</html>