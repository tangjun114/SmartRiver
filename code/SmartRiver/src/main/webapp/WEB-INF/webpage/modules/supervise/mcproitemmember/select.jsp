<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目人员列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目人员">
<div class="row">
 
  <div class="col-md-12">
<grid:grid id="mcProItemMemberGridId" url="${adminPath}/supervise/mcproitemmember/ajaxList?projectId=${projectId}&type=${type}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="姓名"  name="realname" />
    <grid:column label="部门"  name="deptName" />
    <grid:column label="人员类型"  name="type" query="true"  queryMode="select"  condition="eq"  dict="projectMemberType" />
    <grid:column label="职位"  name="position" />
    <grid:column label="短信通知"  name="msgPerm" dict="msgPerm"/>
    <grid:column label="报告操作"  name="reportPerm" dict="reportPerm"/>
    <grid:column label="电话"  name="phone" />
    <grid:column label="邮箱"  name="email" />
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproitemmember/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</div>
</div>

<html:js name="ff" />
<script type="text/javascript">  
 
</script>
</body>
</html>