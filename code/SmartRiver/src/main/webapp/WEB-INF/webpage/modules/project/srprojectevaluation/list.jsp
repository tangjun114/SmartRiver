<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>项目评比列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="项目评比">
<grid:grid id="srProjectEvaluationGridId" url="${adminPath}/project/srprojectevaluation/ajaxList?proId=${proId}">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="项目名称" name="proName"/>
    <grid:column label="评比项目" name="evaName"/>
    <grid:column label="排名" name="no"/>
    <grid:column label="备注信息" name="remarks"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>