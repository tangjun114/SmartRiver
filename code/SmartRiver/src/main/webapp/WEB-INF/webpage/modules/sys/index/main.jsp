<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>报表</title>
    <meta name="decorator" content="single"/>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div>
            <div class="portlet box  portlet-grey">
                <div>
                    <form class="form-inline">
                        <div id="distpicker5">
                            <div class="form-group">
                                <label class="sr-only" for="areaId">区域</label>
                                <select
                                        class="form-control" id="areaId" onchange="changeProName()"></select>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="level">时间维度</label>
                                <select class="form-control" id="level">
                                    <option value="year">年</option>
                                    <option value="month">月</option>
                                    <option value="day" selected="selected">日</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="startTime">开始时间</label>
                                <input id="startTime" name="deadTime"
                                       onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                                       placeholder="YYYY-MM-DD hh:mm:ss" class="form-control layer-date" type="text"
                                       value="">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="endTime">结束时间</label>

                                <input id="endTime" name="deadTime"
                                       onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                                       placeholder="YYYY-MM-DD hh:mm:ss" class="form-control layer-date" type="text"
                                       value="">

                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" id="search" type="button" onclick="searchByArea()">查询
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="echarts" id="echarts-bar-chart" style="height:600px"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="echarts" id="echarts-pie-chart" style="height:200px"></div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="row">
        <div>
            <div class="portlet box  portlet-grey">

                <div>
                    <form class="form-inline">
                        <div id="distpicker51">
                            <div class="form-group">
                                <label class="sr-only" for="siteId">站点</label>
                                <select
                                        class="form-control" id="siteId" onchange="changeProName()"></select>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="startTime">开始时间</label>
                                <input id="startTime1" name="deadTime"
                                       onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                                       placeholder="YYYY-MM-DD hh:mm:ss" class="form-control layer-date" type="text"
                                       value="">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="endTime">结束时间</label>

                                <input id="endTime1" name="deadTime"
                                       onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                                       placeholder="YYYY-MM-DD hh:mm:ss" class="form-control layer-date" type="text"
                                       value="">

                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" id="search1" type="button" onclick="searchByArea1()">查询
                                </button>

                            </div>
                        </div>
                    </form>
                </div>


                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="echarts" id="echarts-bar-chart1" style="height:600px"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="echarts" id="echarts-bar-chart2" style="height:200px"></div>
                    </div>
                </div>


            </div>
        </div>
    </div>


</div>

<!-- 全局js -->
<html:js name="echarts"/>

<html:js name="ff"/>
<script type="text/javascript">

    $(function () {

        var areaIdDefault = 'e1816538db014dd7b3194e493f4540e1';
        var siteIdDefault = '940d016502cd44aab519db548137b4a1';

        var selector = $("#areaId");

        ff.form.combox.load(selector, {
            url: "flood/floodarea/loadall",
            valueField: "id",
            select: $("#areaId").val(),
            labelField: "name",
            select: areaIdDefault,
            data: {}
        }, function () {
        });

        var selector = $("#siteId");

        ff.form.combox.load(selector, {
            url: "flood/floodsite/loadall",
            valueField: "id",
            select: $("#siteId").val(),
            labelField: "siteName",
            select: siteIdDefault,
            data: {}
        }, function () {
        });

        var today = new Date().format("yyyy-MM-dd");
        var day = new Date(new Date().getTime() - 30 * 24 * 60 * 60 * 1000).format("yyyy-MM-dd");
        if ("" == document.getElementById("endTime").value) {
            document.getElementById("endTime").value = today + " 23:59:59";
        }
        if ("" == document.getElementById("startTime").value) {
            document.getElementById("startTime").value = day + " 00:00:00";
        }
        if ("" == document.getElementById("endTime1").value) {
            document.getElementById("endTime1").value = today + " 23:59:59";
        }
        if ("" == document.getElementById("startTime1").value) {
            document.getElementById("startTime1").value = day + " 00:00:00";
        }


        var winHeight;
        var winWidth;
        if (window.innerHeight) {
            winHeight = window.innerHeight;
        }
        else if ((document.body) && (document.body.clientHeight)) {
            winHeight = document.body.clientHeight;
        }

        if (window.innerWidth) {
            winWidth = window.innerWidth;
        }
        else if ((document.body) && (document.body.clientWidth)) {
            winWidth = document.body.clientWidth;
        }
        var newH = winHeight - 240;
        $("#echarts-bar-chart").css("cssText", "height: " + newH + "px!important;");
        $("#echarts-pie-chart").css("cssText", "height: " + newH + "px!important;");
        $("#echarts-bar-chart1").css("cssText", "height: " + newH + "px!important;");
        $("#echarts-bar-chart2").css("cssText", "height: " + newH + "px!important;");


        searchByArea(areaIdDefault);
        searchByArea1(siteIdDefault);
    });

    function searchByArea(siteIdvar) {
        debugger;
        var siteId = $('#areaId option:selected').val();
        if (typeof(siteIdvar) != "undefined") {
            siteId = siteIdvar;
        }
        var level = $('#level option:selected').val();
        var startTime = $('#startTime').val();
        var endTime = $('#endTime').val();
        ff.util.submit({
            url: "/flood/flooddestoryreport/chart/data",
            data: '',
            filter: {
                areaId: siteId,
                level: level,
                startTime: startTime,
                endTime: endTime
            },
            success: function (rsp) {
                if (null != rsp && rsp.obj) {
                    var option = {
                        xAxis: {
                            name: "时间",
                            nameLocation:"middle",
                            nameGap:30
                        },

                        yAxis: {
                            name: "经济损失(元)",
                            // nameLocation:"middle",
                            // nameGap:30
                        },
                    };
                    ff.chart.load("#echarts-bar-chart", "", rsp.obj[0], "bar", option);
                    ff.chart.load("#echarts-pie-chart", "雨毁报表区域统计", rsp.obj[1], "pie");
                }
            }

        });
    }

    function searchByArea1(siteIdvar) {
        searchRainfull(siteIdvar);
        searchWaterlevel(siteIdvar);
    }

    function searchRainfull(siteIdvar) {
        var siteId = $('#siteId option:selected').val();
        if (typeof(siteIdvar) != "undefined") {
            siteId = siteIdvar;
        }
        var startTime = $('#startTime1').val();
        var endTime = $('#endTime1').val();
        ff.util.submit({
            url: "/flood/floodrainfull/chart/data",
            data: 'rainfull',
            filter: {
                siteId: siteId,
                startTime: startTime,
                endTime: endTime
            },
            success: function (rsp) {
                if (null != rsp && rsp.obj) {
                    var option = {
                        xAxis: {
                            name: "时间",
                            nameLocation:"middle",
                            nameGap:30
                        },

                        yAxis: {
                            name: "降雨量(mm)",
                            // nameLocation:"middle",
                            // nameGap:30
                        },
                    };

                    ff.chart.load("#echarts-bar-chart1", "", rsp.obj, "bar", option);
                }
            }

        });
    }

    function searchWaterlevel(siteIdvar) {
        var siteId = $('#siteId option:selected').val();
        if (typeof(siteIdvar) != "undefined") {
            siteId = siteIdvar;
        }
        var startTime = $('#startTime1').val();
        var endTime = $('#endTime1').val();
        ff.util.submit({
            url: "/flood/floodrainfull/chart/data",
            data: 'waterlevel',
            filter: {
                siteId: siteId,
                startTime: startTime,
                endTime: endTime
            },
            success: function (rsp) {
                if (null != rsp && rsp.obj) {
                    var option = {
                        xAxis: {
                            name: "时间",
                            nameLocation:"middle",
                            nameGap:30
                        },

                        yAxis: {
                            name: "水位(mm)",
                            // nameLocation:"middle",
                            // nameGap:30
                        },
                    };
                    ff.chart.load("#echarts-bar-chart2", "", rsp.obj, "bar", option);
                }
            }

        });

    }

</script>
</body>

</html>