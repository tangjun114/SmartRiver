<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>报告列表列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="报告列表">
<grid:grid id="mcProReportGridId" url="${adminPath}/supervise/mcproreport/ajaxList?projectId=${projectId}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.path" hidden="true"  name="reportPath"  />
	
	<grid:column label="文件名称"  name="reportName"  />
	<grid:column label="文件类型"  name="reportType"  query="true"  queryMode="select"  condition="eq"  dict="pro_report_type" /> 
	<grid:column label="上传时间"  name="createDate" />
	
    <grid:column label="状态" hidden="true" name="remarks" />
	<grid:column label="操作"  name="opt" formatter="button"/>
	
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproreport/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<script type="text/javascript">

 
function mcProReportGridIdOptFormatter(value, options, row){
	   
	   var href =row.remarks;
	   if(row.remarks == '报告生成完毕')
	   {
		   href = linkFormatter("下载",row.reportPath); 
	   }
	     
	   
	   return href;
}
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-270;
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});
</script>
</body>
</html>