<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>监测组类别</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcSmMonitorGroupTypeForm">
    <form:form id="mcSmMonitorGroupTypeForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>监测组类别:</label>
		            </td>
					<td class="width-35">
						<form:input path="groupTypeName" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>备注信息:</label>
		            </td>
					<td class="width-35">
						<form:input path="remarks" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label><font color="red">*</font>编码:</label>
		            </td>
					<td class="width-35">
						<form:input path="code" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>单位:</label>
		            </td>
					<td class="width-35">
						<form:input path="unit" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>最大值名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="maxName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>最小值名称:</label>
		            </td>
					<td class="width-35">
						<form:input path="minName" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>监测项备注:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="itemRemark" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>当前值与累计值的单位换算关系:</label>
		            </td>
					<td class="width-35">
						<form:input path="valueRatio" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
<%-- 		      	<tr>
					<td  class="width-15 active text-right">	
		              <label>观测方法（以中文分号隔开）:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="observationMethod" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>

				</tr> --%>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>观测方法:</label>
		            </td>
					<td class="width-35">
						<form:input path="defaultMethod" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>排序值(数值小排位前):</label>
		            </td>
					<td class="width-35">
						<form:input path="sortId" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>

				</tr>
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>