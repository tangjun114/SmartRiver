<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>现场照片列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="现场照片">
<grid:grid id="mcProItemSceneImgGridId" url="${adminPath}/supervise/mcproitemsceneimg/ajaxList?projectId=${param.projectId}">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="40"/>
	<grid:column label="sys.common.path" hidden="true"  name="imgPath" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="imgPath" width="100"/>
	
	<grid:button groupname="opt" function="delete" />
	<grid:column label="异常描述"  name="unusualDesc" />
    <grid:column label="图片路径"  name="imgPath" formatter="button" />
	<grid:toolbar function="create" url="${adminPath}/supervise/mcproitemsceneimg/create?projectId=${param.projectId}"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="func" />
<script type="text/javascript">   
//var projectId = '${projectId}';
function mcProItemSceneImgGridIdImgPathFormatter(value, options, row){
	  try{
	         if(!row.id) {
             return '';
         }
	         var href=linkFormatter(row.imgPath,row.imgPath); }catch(err){}
	   return href;
	}
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-230;
	 $(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
});
</script>
</body>
</html>