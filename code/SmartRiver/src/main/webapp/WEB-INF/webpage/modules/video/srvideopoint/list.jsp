<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>视频站点管理列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="视频站点管理">
<grid:grid id="srVideoPointGridId" url="${adminPath}/video/srvideopoint/ajaxList">
    <grid:column label="sys.common.path" hidden="true" name="url" width="100"/>
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>

    <grid:column label="名称" name="name" query="true" queryMode="input" condition="eq"/>
    <grid:column label="地址" name="addr"/>
    <grid:column label="经度" name="longitude"/>
    <grid:column label="纬度" name="latitude"/>
    <grid:column label="视频地址" name="url"/>
    <grid:column label="账号" name="account"/>
    <grid:column label="密码" name="password"/>
    <grid:column label="创建时间" name="createDate"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>
    <grid:toolbar title="视频参数配置" function="openNewTabWithSelect"/>
    <grid:toolbar title="多画面展示" function="openNewTab" url="/static/video/videomulti.html"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>