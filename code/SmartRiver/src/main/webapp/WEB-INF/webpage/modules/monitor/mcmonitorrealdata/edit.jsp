<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>监测原始记录信息</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcMonitorRealdataForm">
    <form:form id="mcMonitorRealdataForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>监测项名:</label>
		            </td>
					<td class="width-35" colspan="4">
						<form:input path="monitorItemName" htmlEscape="false" class="form-control"    readonly="true"/>
						<label class="Validform_checktip"></label>
					</td>
		  		</tr>
		      	<tr>
					<td  class="width-15 active text-right">	
		              <label>文件名:</label>
		            </td>
					<td class="width-35" colspan="4">
						<form:input path="fileName" htmlEscape="false" class="form-control"   readonly="true"/>
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		      	<tr>
					<td  class="width-15 active text-right">	
		              <label>测点名:</label>
		            </td>
					<td class="width-35" colspan="4">
						<form:input path="mpId" htmlEscape="false" class="form-control"   readonly="true"/>
						<label class="Validform_checktip"></label>
					</td>
				</tr>	
				<tr>
					<td  class="width-15 active text-right">	
		              <label>申请理由:</label>
		            </td>
					<td class="width-35" colspan="4">
						<form:textarea path="reason" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>申请文件:</label>
		            </td>
					<td class="width-35" colspan="4">
						<form:fileinput path="applyFile" htmlEscape="false" class="form-control"    showType="file" maxFileCount="5" 
						saveType="filepath" idField="id"/>
						<label class="Validform_checktip"></label>
					</td>
				</tr>			
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>