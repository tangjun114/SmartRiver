<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>报警修改日志列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="报警修改日志">
<grid:grid id="mcProItemAlarmModifyLogGridId" url="${adminPath}/supervise/mcproitemalarmmodifylog/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.path" hidden="true"  name="modifyFilePath" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="modifyFileName" width="100"/>
	
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="创建时间"  name="createDate" />
    <grid:column label="项目名称"  name="projectName" />
    <grid:column label="监测项名称"  name="monitorItemName" />
    <grid:column label="测点名称"  name="pointCode" />
    <grid:column label="修改描述"  name="modifyDesp" />
    <grid:column label="修改文件"  name="modifyFilePath" formatter="button"/>
    <grid:column label="修改人员"  name="modifyUser" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="func" />
<html:js name="ff" />
<script type="text/javascript">
function mcProItemAlarmModifyLogGridIdModifyFilePathFormatter(value, options, row){

	var href ='';
	  try{
	       if(!row.id) {
           		return '';
       		}
	       if(row.modifyFilePath != '') {
	    	   var strs= new Array();
	    	   strs = row.modifyFilePath.split(',');
	    	   
	    	   for(i=0;i<strs.length;i++) {
		    	   href +="<a href=\"";
		  		   href += rootUrl() + "/" + strs[i];
		  		   href +="\" ";
				   href +="  title=\"\">附件"+(i+1)+"</a>&nbsp&nbsp";
	    	   }

	       }
	         
	    }catch(err){
	        	 
	    }
	   return href;
}
</script>
</body>
</html>