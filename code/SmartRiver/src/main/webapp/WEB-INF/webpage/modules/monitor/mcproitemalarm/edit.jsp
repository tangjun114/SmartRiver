<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>警报管理</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor" />
</head>

<body class="white-bg"  formid="mcProItemAlarmForm">
    <form:form id="mcProItemAlarmForm" modelAttribute="data" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="projectId"/>
		<form:hidden path="monitorItemId"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>处理措施:</label>
		            </td>
					<td class="width-35" colspan="4" >
						<form:textarea path="handleResult" rows="3" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>

		  		</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>图片:</label>
		            </td>
					<td colspan="4" class="width-35"><form:fileinput
							showType="file" extend="jpg,png,gif" maxFileCount="10"
							fileInputWidth="100px" fileInputHeight="100px" path="handleImg"
							htmlEscape="false" class="form-control" saveType="filepath" idField="id"/> <label
						class="Validform_checktip"></label></td>
				</tr>
		   </tbody>
		</table>   
	</form:form>
<html:js name="bootstrap-fileinput" />
<html:js name="simditor" />
</body>
</html>