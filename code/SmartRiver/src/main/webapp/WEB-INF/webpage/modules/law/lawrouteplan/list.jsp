<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>路线规划列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="路线规划">
<grid:grid id="lawRoutePlanGridId" url="${adminPath}/law/lawrouteplan/ajaxList">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
    <grid:column label="路线名称"  name="routeName"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="位置1"  name="routeAddr1" />
    <grid:column label="位置2"  name="routeAddr2" />
    <grid:column label="位置3"  name="routeAddr3" />
    <grid:column label="位置4"  name="routeAddr4" />
    <grid:column label="位置5"  name="routeAddr5" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	<grid:toolbar function="delete"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>