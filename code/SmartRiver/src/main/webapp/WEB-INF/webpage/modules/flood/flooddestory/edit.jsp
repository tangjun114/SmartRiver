<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>水毁信息表</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput"/>
    <html:css name="simditor"/>
    <html:js name="simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline,layer"/>
    <html:js name="ff"/>
</head>

<body class="white-bg" formid="floodDestoryForm">
<form:form id="floodDestoryForm" modelAttribute="data" method="post" class="form-horizontal">
    <form:hidden path="id"/>
    <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
        <tbody>
        <tr>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>标题:</label>
            </td>
            <td class="width-35">
                <form:input path="title" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>严重程度:</label>
            </td>
            <td class="width-35">
                <form:select path="grade" dict="flood_destory_degree" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>横坐标:</label>
            </td>
            <td class="width-35">
                <form:input path="latitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label><font color="red">*</font>纵坐标:</label>
            </td>
            <td class="width-35">
                <form:input path="longitude" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-35">
                <button type="button" class="btn btn-primary" id="btn_gps">
                    <i class="fa fa-map"></i></button>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>图像:</label>
            </td>
            <td class="width-35">
                <form:fileinput
                        showType="file" extend="jpg,png,gif" maxFileCount="5"
                        fileInputWidth="100px" fileInputHeight="100px" path="imgPath"
                        htmlEscape="false" class="form-control" saveType="filepath" idField="id"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>音频:</label>
            </td>
            <td class="width-35">
                <form:fileinput
                        showType="file" extend="jpg,png,gif" maxFileCount="5"
                        fileInputWidth="100px" fileInputHeight="100px" path="audio"
                        htmlEscape="false" class="form-control" saveType="filepath" idField="id"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>经济损失:</label>
            </td>
            <td class="width-35">
                <form:input path="econLoss" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>文字描述:</label>
            </td>
            <td class="width-35">
                <form:input path="details" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>备注信息:</label>
            </td>
            <td class="width-35">
                <form:input path="remarks" htmlEscape="false" class="form-control"/>
                <label class="Validform_checktip"></label>
            </td>
            <td class="width-15 active text-right">
                <label>所属区域:</label>
            </td>
            <td class="width-35">
                <form:select path="areaId" htmlEscape="false" cssClass="form-control" onchange="changeAreaName()"/>
                <form:hidden path="areaName" htmlEscape="false" class="form-control"/>
            </td>
        </tr>
        <tr>
            <td class="width-15 active text-right">
                <label>发生时间:</label>
            </td>
            <td class="width-35">
                <form:input path="recordTime" datefmt="yyyy-MM-dd hh:mm:ss" class="form-control layer-date"
                            placeholder="YYYY-MM-DD hh:mm:ss"
                            onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"/>

                <label class="Validform_checktip"></label>
            </td>
        </tr>

        </tbody>
    </table>
</form:form>
<html:js name="bootstrap-fileinput"/>
<html:js name="simditor"/>
<script>

    $(function () {

        $("#btn_gps").on('click', function (e) {
            var lat = '${data.latitude}';
            var lng = '${data.longitude}';
            ff.service.map(lat, lng, function (obj) {
                $("#latitude").val(obj.lat);
                $("#longitude").val(obj.lng);
            });
        });

        var selector = $("select[name=areaId]");

        ff.form.combox.load(selector, {
            url: "flood/floodarea/loadall",
            valueField: "id",
            select: $("#areaName").val(),
            labelField: "name",
            data: {}

        }, function () {
        });

        var today = new Date().format("yyyy-MM-dd");
        var day = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
        if ("" == document.getElementById("recordTime").value) {
            document.getElementById("recordTime").value = today + " 00:00:00";
        }
    });

    function changeAreaName() {
        var areaName = $('#areaId').find('option:selected').text();
        $('#areaName').val(areaName);
    }
</script>
</body>
</html>