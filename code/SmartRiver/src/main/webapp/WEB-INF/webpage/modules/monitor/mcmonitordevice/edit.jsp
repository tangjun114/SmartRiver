<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>设备信息</title>
    <meta name="decorator" content="form"/>
    <html:css name="bootstrap-fileinput" />
    <html:css name="simditor,jqgrid" />
</head>

<body class="white-bg"  formid="mcMonitorDeviceForm" beforeSubmit="beforeSubmit">
    <form:form id="mcMonitorDeviceForm" modelAttribute="data"  method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="type"/>
		<table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>设备类型:</label>
		            </td>
					<td class="width-35">
						<form:input path="typeName" htmlEscape="false" class="form-control"    readonly="true"  />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>是否自动化:</label>
		            </td>
					<td class="width-35">
						<form:select path="isAuto" htmlEscape="false" class="form-control"  dict="sf"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>设备型号:</label>
		            </td>
					<td class="width-35">
						<form:treeselect title="设备型号" path="unitType" dataUrl="${adminPath}/sm/mcsmparamconfig/treeData?nodeid=6836d173d77847f797eadf3d1ae33a3b" callback="selectOk" labelName="unitTypeName" labelValue="${data.unitTypeName}" 
						 htmlEscape="false" class="form-control"  datatype="*" readonly="true"
						 />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>设备编号:</label>
		            </td>
					<td class="width-35">
						<form:input path="code" htmlEscape="false" class="form-control"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>设备名修正:</label>
		            </td>
					<td class="width-35">
						<form:input path="nameCorrection" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>规格修正:</label>
		            </td>
					<td class="width-35">
						<form:input path="specRevision" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>检定/校准日期:</label>
		            </td>
					<td class="width-35">
						<form:input path="correctingDate" htmlEscape="false" class="form-control layer-date" placeholder="YYYY-MM-DD" onclick="laydate({istime: true, format: 'YYYY-MM-DD',choose:startChange})"     />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>检定/校准到期日期:</label>
		            </td>
					<td class="width-35">
						<form:input path="correctingEndDate" htmlEscape="false" class="form-control layer-date" placeholder="YYYY-MM-DD" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"       />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>使用状态:</label>
		            </td>
					<td class="width-35">
						<form:select path="status" htmlEscape="false" class="form-control" readonly="true" dict="SBXX_SBSFSY"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>设备负责人(1):</label>
		            </td>
					<td class="width-35">
						<form:gridselect gridId="userGridId" title="设备负责人(1)" nested="false"
						   path="head1Id" gridUrl="${adminPath}/sys/user" value="${data.head1Id}"
						   labelName="head1Name" labelValue="${data.head1Name}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="head1Name"
						   callback="userDataPicker" 
						   htmlEscape="false" class="form-control" readonly="true" datatype="*"
						   />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>设备负责人(2):</label>
		            </td>
					<td class="width-35">
						<form:gridselect gridId="userGridId" title="设备负责人(2)" nested="true"
						   path="head2Id" gridUrl="${adminPath}/sys/user" value="${data.head2Id}"
						   labelName="head2Name" labelValue="${data.head2Name}" multiselect="false"
						   layerWidth="800px" layerHeight="500px" 
						   formField="head2Name"
						   callback="userDataPicker" 
						   htmlEscape="false" class="form-control" readonly="true"
						   />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>注意事项:</label>
		            </td>
					<td class="width-35">
						<form:textarea path="matters" rows="4" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr style="display:none">
					<td  class="width-15 active text-right">	
		              <label>连接状态:</label>
		            </td>
					<td class="width-35">
						<form:input path="linkStatus" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>是否过期:</label>
		            </td>
					<td class="width-35">
						<form:select path="isOverdue" htmlEscape="false" class="form-control"  dict="sf"  datatype="*"    />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>生产厂家:</label>
		            </td>
					<td class="width-35">
						<form:input path="manufacturer" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label> 精度:</label>
		            </td>
					<td class="width-35">
						<form:input path="accuracy" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>准确度等级/不确定度:</label>
		            </td>
					<td class="width-35">
						<form:input path="accuracyLevel" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>测量范围:</label>
		            </td>
					<td class="width-35">
						<form:input path="measuringRange" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
				<tr>
					<td  class="width-15 active text-right">	
		              <label>检定/校准机构:</label>
		            </td>
					<td class="width-35">
						<form:input path="calibrationInstitution" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
					<td  class="width-15 active text-right">	
		              <label>图片:</label>
		            </td>
					<td class="width-35">
						<form:fileinput showType="avatar" fileInputWidth="100px"  fileInputHeight="100px"  path="img" htmlEscape="false" class="form-control"      />
						<label class="Validform_checktip"></label>
					</td>
				</tr>
		      
		   </tbody>
		</table>   
	</form:form>
	<div class="row">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
            </ul>
            <div class="tab-content">
          
            </div>
        </div>
    </div>
<html:js name="bootstrap-fileinput" />
<html:js name="func,simditor,jqgrid,jqGrid_curdtools,jqGrid_curdtools_inline" />
<script>
	$(document).ready(function () {
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	    	 resizeGrid();
		});
	});
	$(function(){
	   $(window).resize(function(){   
		   resizeGrid();
	   });
	});
	function startChange()
	{
		 
		var dateStr = $("#correctingDate").val();
		var startDate = new Date(dateStr);
		var endStr = new Date(startDate.getTime() + 364*24*60*60*1000);
		
		$("#correctingEndDate").val(endStr.format("yyyy-MM-dd"))
		 
	}
	function selectOk(nodes){
		var node = nodes[0];
		if(1!=node.level||node.leaf==false){
		$("#unitTypeClear").click();
		top.layer.alert('请选择第二级叶子节点!', {
			icon : 0,
			title : '警告'
		});
		}else{
		initParentNodes(nodes[0].getParentNode());
		}
	}
	
	function resizeGrid(){
	}

	/**
	*提交回调方法
	*/
	function beforeSubmit(curform){
		 //这里最好还是使用JSON提交，控制器改变提交方法，并使用JSON方式来解析数
		 //通过判断，如果有问题不提交
		 return true;
	}
	
	function initGridFormData(curform,gridId){
		 var rowDatas =getRowDatas(gridId+"Grid");
		 var rowJson = JSON.stringify(rowDatas);
		 if(rowJson.indexOf("editable") > 0&&rowJson.indexOf("inline-edit-cell") )
	     {
	    	 return false;
	     }
		 var gridListJson=$('#'+gridId+"ListJson").val();
		 if(gridListJson==undefined||gridListJson==''){
			 var rowInput = $('<input id="'+gridId+'ListJson" type="hidden" name="'+gridId+'ListJson" />');  
			 rowInput.attr('value', rowJson);  
			 // 附加到Form  
			 curform.append(rowInput); 
		 }else{
			 $('#'+gridId+"ListJson").val(rowJson);
		 }
		 return true;
	}
	
	function userDataPicker(rowDatas,fildId,fildName){		
		$("#"+fildId).val(rowDatas[0].id);
		$("#"+fildName).val(rowDatas[0].realname);
		$("#"+fildName).focus();
	}
</script>
</body>
</html>