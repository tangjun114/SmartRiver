<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title><spring:message code="sys.user.title" /></title>
  <meta name="decorator" content="list"/>
  <html:component name="bootstrap-treeview"/>
  <html:js name="ff" />
</head>
<body title="<spring:message code="sys.user.title" />">
<div class="row">
 
	<div  class="col-sm-12 col-md-12">
		<grid:grid id="userGridId" url="${adminPath}/sys/user/ajaxList?projectId=${projectId }&type=${type}">
			<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
			<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
			<grid:button groupname="opt" function="delete"/>
	 
		    <grid:column label="sys.user.realname"  name="realname"  query="true"  condition="like" />
		    <grid:column label="sys.user.username"  name="username"  query="true" condition="like"   />
		    <grid:column label="sys.user.phone"  name="phone"  />
		    <grid:column label="sys.user.email"  name="email"  />
		    
		    <grid:column label="职位"  name="positionName"  />
		    <grid:column label="部门"  name="deptName"  />
		    
		    <grid:column label="人员类型"  name="type" dict="projectMemberType"  query="true" queryMode="select"  condition="eq" />
			 <grid:column label="人员类别"  name="subType" dict="member_sub_type"  query="true" queryMode="select"  condition="eq" />
			
			<grid:toolbar title="sys.user.createuser" function="create" url="${adminPath}/sys/user/create?projectId=${projectId}"/>
		 	<grid:toolbar title="sys.user.updateuser" function="update"/>
			<grid:toolbar title="sys.user.modifypwd" icon="fa-database"  function="updateDialog" url="${adminPath}/sys/user/{id}/changePassword"  />
			<grid:toolbar function="delete"/>
			<grid:toolbar  function="search"/>
			<grid:toolbar  function="reset"/>
		</grid:grid>
	</div>
</div>
<html:js name="ff" />
 <script type="text/javascript">
  $(function(){
 	   var type = ff.util.urlPara("type");
	  
	   $("select[name='type']").val(type);
	   search('userGridIdGrid');
  });
  </script>
</body>
</html>