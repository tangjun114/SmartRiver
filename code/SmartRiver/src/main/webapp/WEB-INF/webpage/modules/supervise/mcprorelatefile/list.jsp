<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>项目相关文件列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="项目相关文件">
<div class="row">
  <div class="col-md-3" style="height:500px;" id="list_row">
	<div style="height:100%;overflow-y:scroll;">
<div class="list-group" id="fileTypeDiv">
	
</div>
</div>
  </div>
  <div class="col-md-9">
  <grid:grid id="mcProRelateFileGridId" url="${adminPath}/supervise/mcprorelatefile/ajaxList?projectId=${projectId}" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.path" hidden="true"  name="filePath" width="100"/>
	<grid:column label="sys.common.name" hidden="true"  name="fileName" width="100"/>

	<grid:column label="文件名称"  name="fileName"  formatter="button"/>
	<grid:column label="文件类型"  name="fileType"  query="true"  queryMode="select"  condition="eq"  dict="relate_file_type" /> 
	
	<grid:column label="上传者"  name="uploadUserName" />
	<grid:column label="上传时间"  name="createDate" />
    <grid:column label="备注"  name="remarks" />
    <grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	
	<grid:toolbar function="create" url="${adminPath}/supervise/mcprorelatefile/create?projectId=${projectId}"/>
	<grid:toolbar function="update"/>


</grid:grid>
  </div>
</div>

<html:js name="func" />
<html:js name="ff" />
<script type="text/javascript">
var typeDiv = $("#fileTypeDiv");
var newHtml= '';
var winHeight;
$(document).ready(function() {
	if(window.innerHeight){
		winHeight=window.innerHeight;
		}
	else if((document.body)&&(document.body.clientHeight)){
		winHeight=document.body.clientHeight;
		}
	var newH = winHeight-210;
	var newHL = winHeight-120;
	$("#list_row").css("cssText","height: "+newHL+"px!important;");
	$(".ui-jqgrid .ui-jqgrid-bdiv").css("cssText","height: "+newH+"px!important;");
	$("select[name=fileType]").parent().hide();
	ff.util.submit({
		 		url:"sys/dict/loadbygroup",
				filter:{
					group: "relate_file_type"
				  },
				success:function(rsp){
					if(null != rsp && rsp.obj)
					{	var firstVal ='';			
						$.each(rsp.obj, function(index, element) {
							newHtml +="<a href=\"javascript:void(0)\" "; 
							newHtml +="id='" + element.value+ "'";
							newHtml +="data-options='" + ff.util.objToJson(element) + "'";
							newHtml +="onclick=\"searchByFileType('";
							newHtml += element.value;
							newHtml +="')\" class=\"list-group-item\"> <h4 class=\"list-group-item-heading\">";
							newHtml += element.label;
							newHtml +="</h4></a>";
							if(index == 0){
								firstVal = element.value;
							}
					    });

						typeDiv.html(newHtml);
						if(firstVal != ''){
							setTimeout(function(){searchByFileType(firstVal)}, 100);
						}
						
					}
				}
			
			});
});
function mcProRelateFileGridIdFileNameFormatter(value, options, row){
	  try{
	         if(!row.id) {
               return '';
           }
	         var href=linkFormatter(row.fileName,row.filePath); }catch(err){}
	   return href;
}

function searchByFileType(code) {
    $("#fileTypeDiv a").each(function(){ 
        $(this).attr("class","list-group-item");  	        
        if($(this).attr("id")==code){
        	$(this).attr("class","list-group-item active");  
        }
         
    }); 
    var item = ff.com.getOptions(code);
    
	$("select[name=fileType]").val(code);
	ff.cache.set("fileType",item);
	search('mcProRelateFileGridIdGrid')
}

</script>
</body>
</html>