<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
  <title>完工审批列表</title>
  <meta name="decorator" content="list"/>
</head>
<body title="完工审批">
<grid:grid id="mcCompletionApprovalGridId" url="${adminPath}/supervise/mccompletionapproval/ajaxList" multiselect="false">
	<grid:column label="sys.common.key" hidden="true"   name="id" width="100"/>
	<grid:column label="sys.common.certificate" hidden="true"   name="certificate" width="100"/>
	<grid:column label="sys.common.certificateName" hidden="true"   name="certificateName" width="100"/>

    <grid:column label="名称"  name="name"  query="true"  queryMode="input"  condition="like" />
    <grid:column label="申请人"  name="createByName" />
    <grid:column label="申请状态"  name="status"  dict="SHENPIZHUANGTAI"/>
    <grid:column label="完工证明"  name="certificateName" formatter="button"/>
    	
	<grid:column label="sys.common.opt"  name="opt" formatter="button" width="100"/>
	<grid:button groupname="opt" function="delete" />
	<grid:toolbar function="create"/>
	<grid:toolbar function="update"/>
	
	<grid:toolbar function="search"/>
	<grid:toolbar function="reset"/>
</grid:grid>
<html:js name="func" />
<script type="text/javascript">
function mcCompletionApprovalGridIdCertificateNameFormatter(value, options, row){
	  try{
	         if(!row.id) {
               return '';
           }
	         var href="<a href=\"#\"";
             href +="onclick=\"downloadFile('下载','${adminPath}/"+row.certificate+"','"+options.gid+"','"+row.id+"','','')\"";
  				href +="><i class=\"fa fa-file\"></i>&nbsp"+row.certificateName+"</a>&nbsp&nbsp";
	   }catch(err){}
	   return href;
	}
</script>
</body>
</html>