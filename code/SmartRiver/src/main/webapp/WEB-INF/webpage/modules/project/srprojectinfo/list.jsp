<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>项目信息列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="项目信息">
<grid:grid id="srProjectInfoGridId" url="${adminPath}/project/srprojectinfo/ajaxList?status=${status}">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="项目名称" name="name" query="true" queryMode="input" condition="like"/>
    <grid:column label="项目地址" name="addr"/>
    <grid:column label="负责人名称" name="dutyUserName" query="true" queryMode="input" condition="like"/>
    <grid:column label="项目类别" name="type" query="true" queryMode="select" dict="projet_info_type"/>
    <grid:column label="项目状态" name="status" dict="project_status" query="true" queryMode="select"/>
    <grid:column label="进展情况" name="progress"/>
    <grid:column label="责任单位" name="dutyOrg"/>
    <grid:column label="建设单位" name="buildOrg"/>
    <grid:column label="设计单位" name="designOrg"/>
    <grid:column label="监测单位" name="monitorOrg"/>
    <grid:column label="资金来源" name="amtSource"/>
    <grid:column label="资金投入" name="amt"/>
    <grid:column label="建设规模" name="scale"/>
    <grid:column label="开工时间" name="startTime"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>
    <grid:toolbar function="update" title="变动记录" url="${adminPath}/project/srprojectinfochangelog?id={id}"/>
    <grid:toolbar function="update" title="项目检查" url="${adminPath}/project/srprojectcheck?proId={id}"/>
    <grid:toolbar function="update" title="项目验收" url="${adminPath}/project/srprojectaccept?proId={id}"/>
    <grid:toolbar function="update" title="项目评比" url="${adminPath}/project/srprojectevaluation?proId={id}"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>
</body>
</html>