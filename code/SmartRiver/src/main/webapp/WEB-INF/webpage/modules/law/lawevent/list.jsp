<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/webpage/common/taglibs.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <title>违法事件列表</title>
    <meta name="decorator" content="list"/>
</head>
<body title="违法事件">
<grid:grid id="lawEventGridId" url="${adminPath}/law/lawevent/ajaxList">
    <grid:column label="sys.common.key" hidden="true" name="id" width="100"/>
    <grid:column label="sys.common.opt" name="opt" formatter="button" width="100"/>
    <grid:button groupname="opt" function="delete"/>
    <grid:column label="事件名称" name="eventName"/>
    <grid:column label="事件地址" name="eventAddr" query="true" queryMode="input" condition="like"/>
    <grid:column label="发生时间" name="occurTime" query="true" queryMode="date" condition="between"/>
    <grid:column label="事件等级" name="eventLevel" query="true" queryMode="select" condition="eq" dict="law_event_level"/>
    <grid:column label="上报人" name="reportUser"/>
    <grid:column label="图片" name="eventImg"/>
    <grid:column label="视频" name="eventVideo"/>
    <grid:column label="所属科室" name="ownDepartment"/>
    <grid:column label="所属所" name="ownStation"/>
    <grid:column label="类型" name="type" dict="law_event_type"/>
    <grid:column label="状态" name="status" dict="law_event_status"/>
    <grid:column label="责任人" name="responsible"/>
    <grid:column label="事件描述" name="desc"/>
    <grid:column label="处理措施" name="treatment"/>
    <grid:column label="违法人员" name="offender"/>
    <grid:toolbar function="create"/>
    <grid:toolbar function="update"/>
    <grid:toolbar function="delete"/>

    <grid:toolbar function="search"/>
    <grid:toolbar function="reset"/>
</grid:grid>

<script type="text/javascript">
    function initTableColor() {
        var today = new Date().format("yyyy-MM-dd");
        var day = new Date(new Date().getTime() - 30 * 24 * 60 * 60 * 1000);

        $("#lawEventGridIdGrid td[aria-describedby='lawEventGridIdGrid_occurTime']").each(function () {

            if (day > new Date($(this).text())) {
                $(this).parent().css("color", "red");
            }
        });
    }
</script>
</body>
</html>