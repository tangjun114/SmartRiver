$(document).ready(function () {
	 
	   $.ajax({  
	       type : "post",  
	       url : "/admin/sys/attachment/list",  
	       dataType : "json",  
	       data:{ id:'${data.imgs}',filepath:'',saveType:'id'},
	       success : function(data) { 
	          var previewJson = new Array();
	          var previewConfigJson = new Array(); 
	          var attachmentData=data.data;
	          if(attachmentData!=undefined&&attachmentData.length>0){
		          for ( var i = 0; i < attachmentData.length; i++) { 
		             var attachmentItem = attachmentData[i];   
		             imgsFileList.push({data:attachmentItem,previewId:attachmentItem.id })
		             previewJson[i]="/"+attachmentItem.filepath;
		             var itemJson={caption: attachmentItem.filename, 
		             				 filename: attachmentItem.filename+"."+attachmentItem.fileext, 
		             				 size: attachmentItem.filesize, 
		             				 url: "/admin/sys/attachment/"+attachmentItem.id+"/delete", 
		             				 extra:{id: attachmentItem.id},
		             				 key:attachmentItem.id
		            			  };
		             previewConfigJson[i]=itemJson;
		          }
	          }
	          updateimgsBind();
	          initimgsFileinput(previewJson,previewConfigJson);
	       }
	   });
});

/**
 *提交回调方法
 */

function beforeSubmit(curform) {
	if (imgsFileList != undefined && imgsFileList.length > 0) {
		var ids = [];
		for (var i = 0; i < imgsFileList.length; i++) {
			ids.push(imgsFileList[i].data.id);
		}
		$("#imgs").val(ids.join(","));
	}
	return true;
}