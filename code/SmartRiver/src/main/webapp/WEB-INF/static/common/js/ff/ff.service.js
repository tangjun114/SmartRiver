var ff = ff || {};

function show(title,a,grid)
{
   try
   {
	   if(status == 1)
		{
			$("#tree").hide();
			status=0;
			$("#grid").attr('class','col-sm-12 col-md-12');
		}	
		else
		{
			 
			$("#tree").show();
	 		status=1;
			$("#grid").attr('class','col-sm-9 col-md-10');
			 
		}	
		 var len=$("#grid").css("width").replace(/px/g, "");
	 	 $("#"+grid).setGridWidth(len-30);
   }
   catch(e)
   {
	   console.log("error");
   }
	
}
ff.service = {};

ff.service.overall = function(img)
{
	 
	var url = ff.util.rootUrl() + "/static/three/overall.html?img="+img;
	url = encodeURI(url)
	window.open(url,"overall");	 
}
ff.service.map = function(lat,lng,callback)
{
 
	var url = ff.util.rootUrl() + "/static/map/bdmap.html?lat="+lat+"&lng="+lng;
	layer.open({
		  type: 2,
		  title: false,
		  area: ['630px', '360px'],
		  closeBtn: false,
		  shade: 0.8,
		  btn: ['确定', '关闭'],
		  content: url,
		  success: function(layero){
			    var btn = layero.find('.layui-layer-btn');

			  },
	 	  yes: function(index, layero){
				 //$(window.frames["iframeName"].document).find("#newlat").html()
	 		    //debugger;
	 		    var newlat = ff.cache.get('newlat');
	 		    var newlng = ff.cache.get('newlng');
	 		    var newadrr = ff.cache.get('newaddr');
	 		    var province = ff.cache.get('newprovince');
	 		    
	 		    var city = ff.cache.get('newcity');
	 		    var district = ff.cache.get('newdistrict');

	 		    var obj = {};
	 		    obj.lat = newlat;
	 		    obj.lng = newlng;
	 		    obj.addr = newadrr;
	 		    obj.city = city;
	 		    obj.province = province;
	 		    obj.district = district;
	 
	 		    //console.log(res);
				if(null != callback)
				{
					callback(obj)
				}
			    layer.close(index); //如果设定了yes回调，需进行手工关闭
			    
			  }
		}); 
}

ff.service.unit={};
ff.service.unit.load = function(type,element)
{
	window.setTimeout( function(){return replace_unit(type,element)}, 500); 
	 
}
ff.service.alarm = function(alarmTypeId)
{ 
	var href = "";
	if (alarmTypeId == '3'){
   	 href ="<span class=\"glyphicon glyphicon-bell hzh-tip gly_navy\" aria-hidden=\"true\" data-original-title=\"\" title=\"\"></span><br>";
    } else if (alarmTypeId == '2') {
   	 //告警
   	 href = "<span class=\"glyphicon glyphicon-bell hzh-tip gly_red\" aria-hidden=\"true\" data-original-title=\"\" title=\"\" ></span><br>";
    } else if (alarmTypeId == '1') {
   	 //预警
   	 href = "<span class=\"glyphicon glyphicon-bell hzh-tip gly_yellow\" aria-hidden=\"true\" data-original-title=\"\" title=\"\" ></span><br>";
    } else {
   	 //正常
   	 href = "<span class=\"glyphicon glyphicon-bell hzh-tip gly_green\" aria-hidden=\"true\" data-original-title=\"\" title=\"\"></span><br>";
    }
	return href;
}
ff.service.unit.get = function(type,old)
{
	var typeUnit = [];
	typeUnit['T001']="kN";
	typeUnit['T002']="mm";
	typeUnit['T003']="mm";
	typeUnit['T004']="mm";
	typeUnit['T005']="mm";
	typeUnit['T006']="kPa";
	typeUnit['T007']="kPa";
	typeUnit['T008']="kN";
	typeUnit['T009']="mm";//没有
	typeUnit['T010']="kN";
	typeUnit['T011']="kN";
	typeUnit['T013']="°";
	typeUnit['T021']="°C";
	typeUnit['T022']="%RH";
	var unit = typeUnit[type];
	if(null == unit)
	{
		unit = "mm";
	}	
	var newStr = old;
	if(null != old)
	{
		newStr = old.replace(/{ff_unit}/g,unit);
		return newStr;
	}	
	
	return unit;
}

function replace_unit(type,element)
{
 
	var unit = ff.service.unit.get(type);
	if(null != element)
	{
		var obj = ff.com.getJqObj(element);
		var html = obj.html();
		html = html.replace(/{ff_unit}/g,unit); 
		obj.html(html);
	}	
	else
	{
	    document.body.innerHTML = document.body.innerHTML.replace(/{ff_unit}/g,unit);
	}
}

ff.service.vendor = function (options)
{
	ff.select.load({title:'供应商选择',url:'SystemData/VendorSelect',width:1200,height:600,options:options});
}

ff.service.address = function (options)
{
	ff.select.load({title:'地址选择',url:'SystemData/AddressSelect',width:1200,height:600,options:options});
}

ff.service.category = function (options)
{
 	ff.select.tree({title:'商品类别选择',url:"SystemData/CategoryTree",search:true,setting:{field:{pIdKey:'parentId'}},options:options});
}
