/**
 * @Description 
 * @author tangjun
 * @date 2016年8月14日
 * 
 */
var ff = ff || {};

ff.chart = {};

ff.chart.load = function(obj,tilte,data,type,option)
{
	debugger;
	 obj = ff.com.getJqObj(obj);
	if(null == type)
	{
		type ="line";
	}
	
	var options;
	if(type == "line" || type =="pie" || type=="bar")
	{
		var dataList = [];
		if(null != data)
		{
			if(!jQuery.isArray( data ))
			{
				dataList.push(data);
			}
			else
			{
				dataList = data;
			}
	      
		}

		options = ff.chart.getLines(tilte,dataList,type,option);
	}
 
	var chart =  echarts.init(obj[0]);
	chart.setOption(options,true);
	return chart;
};
ff.chart.getLines = function(tilte,data,type,dataOptions)
{
	var selectedMode = "multiple";
	if(null == selectedMode)
	{
		selectedMode = "multiple";
	}
	var dataList = [];
	var nameList = [];
	var x_dataList = [];
	var x_axis_name = 'x';
	var y_axis_name = 'y';
	for(var i=0;i<data.length;i++)
	{
		//x_axis_name = data[0].dim;
		var obj = new Object();
		//以下三个属性固定
		if(type == "pie")
		{
			obj.name = data[i].y_name;
			obj.type = type;
			var temp = [];
			if (null != data[i].y_data) {
				for(var j=0;j<data[i].y_data.length;j++)
				{
					temp.push({name:data[i].x_data[j],value:data[i].y_data[j]})
				}
			}

			obj.data = temp;
			//其他属性
			obj.radius=['50%', '70%'];
			obj.avoidLabelOverlap = false;
			obj.label= {
	                normal: {
	                    show: false,
	                    position: 'center'
	                },
	                emphasis: {
	                    show: true,
	                    textStyle: {
	                        fontSize: '30',
	                        fontWeight: 'bold'
	                    }
	                }
	            };
	         obj.labelLine={
	                normal: {
	                    show: false
	                }
	            };
	            
	 		dataList.push(obj);
	 		x_dataList = data[i].x_data;
	 		nameList[i] = data[i].y_name;
		}
		else
		{
			obj.name = data[i].y_name;
			obj.type = type;
			//obj.barWidth='60%';
			obj.data = data[i].y_data;
			obj.markLine = data[i].markLine;
			obj.markPoint = data[i].markPoint;
	 		dataList.push(obj);
	 		x_dataList = data[i].x_data;
	 		nameList[i] = data[i].y_name;
		}
		
 	}
	var types =  ['line', 'bar'];
	if(selectedMode == "single" )
	{
		types =  ['line', 'bar','pie'];
	}
	if(nameList.length == 1)
	{
		nameList = [];
	}
	if(type=='pie')
	{
		option = {
		        title : {
		            text: tilte,
		            subtext: '',
		            x:'center'
		        },
		        tooltip : {
		            trigger: 'item',
		            formatter: "{a} <br/>{b} : {c} ({d}%)"
		        },
		        legend: {
		            
		        	type: 'scroll',
		            orient: 'horizontal',
		            x:'left',
		            y:'bottom',
		            bottom: 20,
		            data:x_dataList
		        },
		        calculable : true,
		        series :dataList
		    };
	}
	else
	{
		option = {
	    		title: {
	    	        text: tilte,
	    	        left: 'center'
	    	    },
	    	    tooltip: {
	    	        trigger: 'axis',
	    	        
	    	    },
	    	    legend: {
	    	        left: 'left',
	    	        data: nameList,
	    	        selectedMode: selectedMode
	    	    },
	  
	    	    toolbox: {
	    	        show : true,
	 
	    	        feature : {
	    	            mark : {show: true},
	    	            dataView : {show: true, readOnly: true},
	    	            magicType : {show: true, type: types},
	    	            saveAsImage : {show: true}
	    	        }
	    	    },
	    	    xAxis: {
	    	        name: x_axis_name,
	    	        splitLine: {show: false},
	    	        data:x_dataList,
	    	        axisLabel : {  
	                    show:true,  
	                   
	                     
	                }   
	    	    },
	    	   
	    	    grid: {
	    	        left: '3%',
	    	        right: '4%',
	    	        //bottom: '15%',
	    	        x2: 100,
	    	        y2: 150,
	    	        containLabel: true
	    	    },
	    	    
	    	 
	    	    yAxis: {
	    	        type: 'value',
	    	        name: y_axis_name,
	    	        scale:true,
	    	    },
	    	    series: dataList
	  
	    };
	}	
	option = ff.obj.merge(option,dataOptions); 
 
	return option;
} 
 