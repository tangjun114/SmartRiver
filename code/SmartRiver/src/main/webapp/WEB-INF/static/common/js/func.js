/*!
 * Copyright &copy; 2016-2020 <a href="http://www.jeeweb.cn/">JeeWeb</a> All rights reserved.
 * 
 * 一些公共的方法
 * @author 王存见
 * @version 2017-06-03
 */
/*{
 "*":/[\w\W]+/,
 "*6-16":/^[\w\W]{6,16}$/,
 "n":/^\d+$/,
 "n6-16":/^\d{6,16}$/,
 "s":/^[\u4E00-\u9FA5\uf900-\ufa2d\w\.\s]+$/,
 "s6-18":/^[\u4E00-\u9FA5\uf900-\ufa2d\w\.\s]{6,18}$/,
 "p":/^[0-9]{6}$/,
 "m":/^13[0-9]{9}$|14[0-9]{9}|15[0-9]{9}$|18[0-9]{9}$/,
 "e":/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
 "url":/^(\w+:\/\/)?\w+(\.\w+)+.*$/
 }
 "*":"不能为空！",
 "*6-16":"请填写6到16位任意字符！",
 "n":"请填写数字！",
 "n6-16":"请填写6到16位数字！",
 "s":"不能输入特殊字符！",
 "s6-18":"请填写6到18位字符！",
 "p":"请填写邮政编码！",
 "m":"请填写手机号码！",
 "e":"邮箱地址格式不对！",
 "url":"请填写网址！"
 */
function validformJqgrid(value, datatype, nullmsg, errormsg) {
	var reg = "";
	var defaultNullmsg = "不能为空！";
	var defaultErrormsg = "";
	var first = datatype.substr(0, 1);
	var end = datatype.substring(1);
	var startNumber = 0;
	var endNumber = 0;
	if (end != '') {
		var startEnds = end.split("-");
		startNumber = startEnds[0];
		endNumber = startEnds[0];
		if (startEnds.length == 2) {
			endNumber = startEnds[1];
		}
	}
	switch (first) {
	case "*":
		defaultNullmsg = "不能为空！";
		reg = /[\w\W]+/;
		if (end != '') {
		  defaultNullmsg = "请填写"+startNumber+"到"+endNumber+"位任意字符！";
		  reg=eval("/^[\\w\\W]{"+startNumber + ","+endNumber + "}$/gim")
 		}
		break;
	case "n":
		defaultNullmsg = "请填写数字！";
		reg = /^\d+$/;
		if (end != '') {
			  defaultNullmsg = "请填写"+startNumber+"到"+endNumber+"位数字！";
			  reg=eval("/^\\d{"+startNumber + ","+endNumber + "}$/gim")
	 	}
		break;
	case "s":
		defaultNullmsg = "不能输入特殊字符！";
		reg = /^[\u4E00-\u9FA5\uf900-\ufa2d\w\.\s]+$/;
		if (end != '') {
			  defaultNullmsg = "请填写"+startNumber+"到"+endNumber+"位字符！";
			  reg=eval("/^[\\u4E00-\\u9FA5\\uf900-\\ufa2d\\w\\.\\s]{"+startNumber + ","+endNumber + "}$/gim")
	 	}
		break;
	case "p":
		defaultNullmsg = "请填写邮政编码！";
		reg = /^[0-9]{6}$/;
		break;
	case "m":
		defaultNullmsg = "请填写手机号码！";
		reg = /^13[0-9]{9}$|14[0-9]{9}|15[0-9]{9}$|18[0-9]{9}$/;
		break;
	case "e":
		defaultNullmsg = "邮箱地址格式不对！";
		reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		break;
	case "url":
		defaultNullmsg = "请填写网址！";
		reg = /^(\w+:\/\/)?\w+(\.\w+)+.*$/;
		break;
	case "/":
		reg = datatype;
		break;
	default:
		
	}
	if(value==undefined||value==''){
		if(nullmsg==undefined||nullmsg==''){
			nullmsg=defaultNullmsg;
		}
		return [false, nullmsg];
	}
	var r = value.match(reg);
	if (r == null){
		if(errormsg==undefined||errormsg==''){
			errormsg=defaultNullmsg;
		}
		return [false, errormsg];
    }
	return [ true, "" ];
}

/**
 * 下载文件
 * @param title 查看框标题
 * @param url//路劲字段
 * @param rowid//主键字段
 */
function downloadFile(title,url,gridId,rowid) {
	debugger;
	/*var gridObject = $("#"+gridId);  
	gridObject.saveRow(rowid, false, 'clientArray');
	var rowData=gridObject.jqGrid('getRowData',rowid);*/
	//方式一
	  //window.open(url);
	//方式 二 
	var $eleForm = $("<form method='post'></form>");
      $eleForm.attr("action",url);
      $(document.body).append($eleForm);
      //提交表单，实现下载
      $eleForm.submit(); 
     //方式三
  /*    try{ 
    	  debugger;
          var elemIF = document.createElement("iframe");   
          elemIF.src = url;   
          elemIF.style.display = "none";   
          document.body.appendChild(elemIF);   
      }catch(e){ 

      } */
}

/**
 * 多记录刪除請求
 * 
 * @param title
 * @param url
 * @param gname
 * @return
 */
function delObj(title, url, infoid,rowId) {
	swal({
		title : "提示",
		text : "您确定要删除这些信息么，请谨慎操作！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "确定",
		closeOnConfirm : false,
		cancelButtonText : "取消",
	}, function() {
		$.ajax({
			url : url.replace("{id}",rowId),
			type : 'post',
			data : {
				id : infoid
			},
			cache : false,
			success : function(d) {
				if (d.ret == 0) {
					var msg = d.msg;
					swal("删除成功！", msg, "success");
					// 刷新表单
					refreshTable(infoid);
				}
			}
		});
	});
}


//参数设置 选项
function initParentNodes(node){
	 
	$("#"+node.fieldMapper).val(node.paraValue);
	$("#"+node.fieldMapper+"Name").val(node.name);
	if(null!=node.getParentNode()){
		initParentNodes(node.getParentNode());
	}
 
}

function  rootUrl(){
	
    //获取当前网址，如： http://localhost:8080/Tmall/index.jsp 
    var curWwwPath = window.document.location.href;

    //获取主机地址之后的目录如：/Tmall/index.jsp 
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);

    //获取主机地址，如： http://localhost:8080 
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    //获取带"/"的项目名，如：/Tmall
  
    return localhostPath + projectName ;
 
};

function linkFormatter(name,url)
{
	 url = rootUrl() + "/" + url;
	 var href="<a href=\"#\"";
     href +="onclick=\"downloadFile('下载','"+url+"')\"";
			href +="  ><i class=\"fa fa-file\"></i>&nbsp"+name+"</a>&nbsp&nbsp";
	 return href;
}

function formatDateTime(draftTimeV) {  
	draftTimeV = draftTimeV + "";
	var date = "";
	var month = new Array();
	month["Jan"] = '01'; month["Feb"] = '02'; 
	month["Mar"] = '03'; month["Apr"] = '04'; 
	month["May"] = '05'; month["Jun"] = '06';
	month["Jul"] = '07'; month["Aug"] = '08'; 
	month["Sep"] = '09'; month["Oct"] = '10'; 
	month["Nov"] = '11'; month["Dec"] = '12';

	str = draftTimeV.split(" ");
	date = str[5] + "-";
	date = date + month[str[1]] + "-" + str[2]+" "+str[3];
	return date;

}; 
