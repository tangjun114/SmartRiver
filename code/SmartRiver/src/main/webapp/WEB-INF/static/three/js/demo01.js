/**
 * Created by Administrator on 2016/11/29.
 */


//必须在服务器上才能看到效果！
window.onload=function(){
    getTitleHeight();
    loadingAllImg();
}
//让全景图刚好撑满屏幕
var canvasHeight;
function getTitleHeight(){
    var maxHeight=window.innerHeight;
    canvasHeight=parseFloat(maxHeight)+'px';
}
//全景图参数配置函数
function loadingAllImg(){
	
	var path = rootUrl() +"/" +urlPara("img");
	 
    var div = document.getElementById('container');
    var PSV = new PhotoSphereViewer({
        // 全景图的完整路径
        panorama: path,

        // 放全景图的元素
        container: div,

        // 可选，默认值为2000，全景图在time_anim毫秒后会自动进行动画。（设置为false禁用它）
        time_anim: false,

        // 可选值，默认为false。显示导航条。
        navbar: true,

        // 可选，默认值null，全景图容器的最终尺寸。例如：{width: 500, height: 300}。
        size: {
            width: '100%',
            height: canvasHeight
        }
    });
}

 function urlPara(key) {
    var name, value;
    var str = location.href; //取得整个地址栏
    str = decodeURI(str);
    var num = str.indexOf("?")
    str = str.substr(num + 1); //取得所有参数   stringvar.substr(start [, length ]

    var arr = str.split("&"); //各个参数放到数组里
    var paras = [];
    for (var i = 0; i < arr.length; i++) {
        num = arr[i].indexOf("=");
        if (num > 0) {
            name = arr[i].substring(0, num);
            value = arr[i].substr(num + 1);
            paras[name] = value;
        }
    }
    if (null != key) {
        return paras[key]
    }
    return paras;
};

function rootUrl() {
    var curWwwPath = window.document.location.href;

    //获取主机地址之后的目录如：/Tmall/index.jsp 
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);

    //获取主机地址，如： http://localhost:8080 
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    if (projectName == '/admin') {
        projectName = '';
    }
    //获取带"/"的项目名，如：/Tmall

    return localhostPath + projectName;
}