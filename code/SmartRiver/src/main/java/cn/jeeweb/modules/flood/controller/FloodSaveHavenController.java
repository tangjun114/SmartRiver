package cn.jeeweb.modules.flood.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.FloodSaveHaven;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**   
 * @Title: 避难中心
 * @Description: 避难中心
 * @author liyonglei
 * @date 2018-11-09 11:19:20
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/floodsavehaven")
@RequiresPathPermission("flood:floodsavehaven")
public class FloodSaveHavenController extends BaseCRUDController<FloodSaveHaven, String> {

    @RequestMapping(value = "/floodInfo")
    public String floodInfo(Model model, HttpServletRequest request, HttpServletResponse response) {
        super.setAllAttribute(model, request, response);
        return display("floodInfo");
    }
}
