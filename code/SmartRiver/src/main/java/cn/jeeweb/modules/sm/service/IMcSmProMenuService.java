package cn.jeeweb.modules.sm.service;

import cn.jeeweb.core.common.service.ITreeCommonService;
import cn.jeeweb.modules.sm.entity.McSmProMenu;

/**   
 * @Title: 项目管理-监测菜单
 * @Description: 项目管理-监测菜单
 * @author shawloong
 * @date 2017-10-30 00:26:12
 * @version V1.0   
 *
 */
public interface IMcSmProMenuService extends ITreeCommonService<McSmProMenu,String>{

}

