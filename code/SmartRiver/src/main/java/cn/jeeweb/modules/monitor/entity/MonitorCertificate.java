package cn.jeeweb.modules.monitor.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.jeeweb.core.common.entity.AbstractEntity;

import cn.jeeweb.modules.monitor.entity.McMonitorOrg;

/**   
 * @Title: 机构证书管理
 * @Description: 机构证书管理
 * @author shawloong
 * @date 2017-10-05 21:31:07
 * @version V1.0   
 *
 */
@TableName("mc_monitor_certificate")
@SuppressWarnings("serial")
public class MonitorCertificate extends AbstractEntity<String> {

    /**id*/
    @TableField(value = "id",el="id.id")
	private String id;
    /**监督机构*/
    @TableField(value = "monitor",el="monitor.id")
	private McMonitorOrg monitor;
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}

    
    
}
