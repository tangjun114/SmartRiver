package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemDeviceMapper;
import cn.jeeweb.modules.supervise.entity.McProItemDevice;
import cn.jeeweb.modules.supervise.service.IMcProItemDeviceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目与设备管理表
 * @Description: 项目与设备管理表
 * @author jerry
 * @date 2018-01-29 17:01:28
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemDeviceService")
public class McProItemDeviceServiceImpl  extends CommonServiceImpl<McProItemDeviceMapper,McProItemDevice> implements  IMcProItemDeviceService {

}
