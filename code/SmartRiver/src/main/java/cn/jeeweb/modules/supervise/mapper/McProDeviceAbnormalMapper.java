package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProDeviceAbnormal;
 
/**   
 * @Title: 设备异常状况数据库控制层接口
 * @Description: 设备异常状况数据库控制层接口
 * @author Aether
 * @date 2018-04-19 21:46:24
 * @version V1.0   
 *
 */
public interface McProDeviceAbnormalMapper extends BaseMapper<McProDeviceAbnormal> {
    
}