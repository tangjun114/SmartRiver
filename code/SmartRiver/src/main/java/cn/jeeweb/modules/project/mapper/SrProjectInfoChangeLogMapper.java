package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectInfoChangeLog;
 
/**   
 * @Title: 工程信息变动调整记录数据库控制层接口
 * @Description: 工程信息变动调整记录数据库控制层接口
 * @author wsh
 * @date 2019-03-31 14:34:53
 * @version V1.0   
 *
 */
public interface SrProjectInfoChangeLogMapper extends BaseMapper<SrProjectInfoChangeLog> {
    
}