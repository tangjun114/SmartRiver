package cn.jeeweb.modules.law.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 巡查结果
 * @Description: 巡查结果
 * @author liyonglei
 * @date 2018-11-13 20:40:25
 * @version V1.0   
 *
 */
@TableName("sr_law_check_result")
@SuppressWarnings("serial")
public class LawCheckResult extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**设备名称*/
    @TableField(value = "equipment_name")
	private String equipmentName;
    /**计划名称*/
    @TableField(value = "plan_name")
	private String planName;
    /**路线名称*/
    @TableField(value = "route_name")
	private String routeName;
    /**巡检次数*/
    @TableField(value = "check_count")
	private Integer checkCount;
    /**巡检结果*/
    @TableField(value = "check_result")
	private Double checkResult;
    /**巡检人员*/
    @TableField(value = "user_name")
	private String userName;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  equipmentName
	 *@return: String  设备名称
	 */
	public String getEquipmentName(){
		return this.equipmentName;
	}

	/**
	 * 设置  equipmentName
	 *@param: equipmentName  设备名称
	 */
	public void setEquipmentName(String equipmentName){
		this.equipmentName = equipmentName;
	}
	/**
	 * 获取  planName
	 *@return: String  计划名称
	 */
	public String getPlanName(){
		return this.planName;
	}

	/**
	 * 设置  planName
	 *@param: planName  计划名称
	 */
	public void setPlanName(String planName){
		this.planName = planName;
	}
	/**
	 * 获取  routeName
	 *@return: String  路线名称
	 */
	public String getRouteName(){
		return this.routeName;
	}

	/**
	 * 设置  routeName
	 *@param: routeName  路线名称
	 */
	public void setRouteName(String routeName){
		this.routeName = routeName;
	}
	/**
	 * 获取  checkCount
	 *@return: Integer  巡检次数
	 */
	public Integer getCheckCount(){
		return this.checkCount;
	}

	/**
	 * 设置  checkCount
	 *@param: checkCount  巡检次数
	 */
	public void setCheckCount(Integer checkCount){
		this.checkCount = checkCount;
	}
	/**
	 * 获取  checkResult
	 *@return: Double  巡检结果
	 */
	public Double getCheckResult(){
		return this.checkResult;
	}

	/**
	 * 设置  checkResult
	 *@param: checkResult  巡检结果
	 */
	public void setCheckResult(Double checkResult){
		this.checkResult = checkResult;
	}
	/**
	 * 获取  userName
	 *@return: String  巡检人员
	 */
	public String getUserName(){
		return this.userName;
	}

	/**
	 * 设置  userName
	 *@param: userName  巡检人员
	 */
	public void setUserName(String userName){
		this.userName = userName;
	}
	
}
