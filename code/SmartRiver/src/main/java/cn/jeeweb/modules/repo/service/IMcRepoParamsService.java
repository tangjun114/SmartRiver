package cn.jeeweb.modules.repo.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.repo.entity.McRepoParams;

/**   
 * @Title: 技术参数
 * @Description: 技术参数
 * @author shawloong
 * @date 2017-10-04 01:19:11
 * @version V1.0   
 *
 */
public interface IMcRepoParamsService extends ICommonService<McRepoParams> {

}

