package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.flood.mapper.FloodAreaMapper;
import cn.jeeweb.modules.flood.entity.FloodArea;
import cn.jeeweb.modules.flood.service.IFloodAreaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 水清监测区域
 * @Description: 水情监测区域
 * @author wsh
 * @date 2018-11-30 16:11:01
 * @version V1.0   
 *
 */
@Transactional
@Service("floodAreaService")
public class FloodAreaServiceImpl  extends CommonServiceImpl<FloodAreaMapper,FloodArea> implements  IFloodAreaService {

}
