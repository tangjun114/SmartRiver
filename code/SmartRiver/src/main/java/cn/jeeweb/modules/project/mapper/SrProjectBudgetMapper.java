package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectBudget;
 
/**   
 * @Title: 项目预算数据库控制层接口
 * @Description: 项目预算数据库控制层接口
 * @author jerry
 * @date 2018-11-13 21:29:59
 * @version V1.0   
 *
 */
public interface SrProjectBudgetMapper extends BaseMapper<SrProjectBudget> {
    
}