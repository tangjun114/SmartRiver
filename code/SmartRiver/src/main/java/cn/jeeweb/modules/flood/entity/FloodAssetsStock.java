package cn.jeeweb.modules.flood.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import com.ff.common.service.model.FFExcelMetaAnno;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liyonglei
 * @version V1.0
 * @Title: 库存管理
 * @Description: 库存管理
 * @date 2018-11-09 16:59:23
 */
@TableName("sr_flood_assets_stock")
@SuppressWarnings("serial")
public class FloodAssetsStock extends AbstractEntity<String> {

    /**
     * 字段主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    /**
     * 创建者
     */
    @TableField(value = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField(value = "update_date")
    private Date updateDate;
    /**
     * 删除标记（0：正常；1：删除）
     */
    @TableField(value = "del_flag")
    private String delFlag;
    /**
     * 备注信息
     */
    @TableField(value = "remarks")
    private String remarks;
    /**
     * 资产代码
     */
    @TableField(value = "assets_code")
    @FFExcelMetaAnno(name = "资产代码")
    private String assetsCode;
    /**
     * 资产名称
     */
    @TableField(value = "assets_name")
    @FFExcelMetaAnno(name = "资产名称")
    private String assetsName;
    /**
     * 资产类型名称
     */
    @TableField(value = "assets_type_name")
    private String assetsTypeName;
    /**
     * 库存数量
     */
    @TableField(value = "stock_qty")
    @FFExcelMetaAnno(name = "库存数量")
    private BigDecimal stockQty;
    /**
     * 单位
     */
    @TableField(value = "unit")
    @FFExcelMetaAnno(name = "单位")
    private String unit;
    /**
     * 库存点名称
     */
    @TableField(value = "stock_location")
    @FFExcelMetaAnno(name = "库存点名称")
    private String stockLocation;

    @TableField(exist = false)
    private String status;

    @TableField(exist = false)
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取  id
     *
     * @return: String  字段主键
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置  id
     *
     * @param: id  字段主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取  createDate
     *
     * @return: Date  创建时间
     */
    public Date getCreateDate() {
        return this.createDate;
    }

    /**
     * 设置  createDate
     *
     * @param: createDate  创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取  updateDate
     *
     * @return: Date  更新时间
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 设置  updateDate
     *
     * @param: updateDate  更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取  delFlag
     *
     * @return: String  删除标记（0：正常；1：删除）
     */
    public String getDelFlag() {
        return this.delFlag;
    }

    /**
     * 设置  delFlag
     *
     * @param: delFlag  删除标记（0：正常；1：删除）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 获取  remarks
     *
     * @return: String  备注信息
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置  remarks
     *
     * @param: remarks  备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取  assetsCode
     *
     * @return: String  资产代码
     */
    public String getAssetsCode() {
        return this.assetsCode;
    }

    /**
     * 设置  assetsCode
     *
     * @param: assetsCode  资产代码
     */
    public void setAssetsCode(String assetsCode) {
        this.assetsCode = assetsCode;
    }

    /**
     * 获取  assetsName
     *
     * @return: String  资产名称
     */
    public String getAssetsName() {
        return this.assetsName;
    }

    /**
     * 设置  assetsName
     *
     * @param: assetsName  资产名称
     */
    public void setAssetsName(String assetsName) {
        this.assetsName = assetsName;
    }

    /**
     * 获取  assetsTypeName
     *
     * @return: String  资产类型名称
     */
    public String getAssetsTypeName() {
        return this.assetsTypeName;
    }

    /**
     * 设置  assetsTypeName
     *
     * @param: assetsTypeName  资产类型名称
     */
    public void setAssetsTypeName(String assetsTypeName) {
        this.assetsTypeName = assetsTypeName;
    }

    /**
     * 获取  stockQty
     *
     * @return: Double  库存数量
     */
    public BigDecimal getStockQty() {
        return this.stockQty;
    }

    /**
     * 设置  stockQty
     *
     * @param: stockQty  库存数量
     */
    public void setStockQty(BigDecimal stockQty) {
        this.stockQty = stockQty;
    }

    /**
     * 获取  unit
     *
     * @return: String  单位
     */
    public String getUnit() {
        return this.unit;
    }

    /**
     * 设置  unit
     *
     * @param: unit  单位
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * 获取  stockLocation
     *
     * @return: String  库存点名称
     */
    public String getStockLocation() {
        return this.stockLocation;
    }

    /**
     * 设置  stockLocation
     *
     * @param: stockLocation  库存点名称
     */
    public void setStockLocation(String stockLocation) {
        this.stockLocation = stockLocation;
    }

}
