package cn.jeeweb.modules.sm.service.impl;

import cn.jeeweb.core.common.service.impl.TreeCommonServiceImpl;
import cn.jeeweb.modules.sm.mapper.McSmParamConfigMapper;
import cn.jeeweb.modules.sm.entity.McSmParamConfig;
import cn.jeeweb.modules.sm.service.IMcSmParamConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 系统管理-参数设置
 * @Description: 系统管理-参数设置
 * @author shawloong
 * @date 2017-10-11 23:30:03
 * @version V1.0   
 *
 */
@Transactional
@Service("mcSmParamConfigService")
public class McSmParamConfigServiceImpl  extends TreeCommonServiceImpl<McSmParamConfigMapper,McSmParamConfig,String> implements  IMcSmParamConfigService {

}
