package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;
import cn.jeeweb.modules.monitor.entity.McMonitorOtherDevice;

/**   
 * @Title: 项目中的其他设备
 * @Description: 项目中的其他设备
 * @author jerry
 * @date 2018-01-28 18:52:59
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_other_device")
@SuppressWarnings("serial")
public class McProItemOtherDevice extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**设备Id*/
    @TableField(value = "other_device_id" )
	private String otherDeviceId;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "projectName")
	private String porjectName;
    /**设备名称*/
    @TableField(value = "other_device_name")
	private String otherDeviceName;
    /**设备编码*/
    @TableField(value = "other_device_code")
	private String otherDeviceCode;
    /**设备类型*/
    @TableField(value = "other_device_type_name")
	private String otherDeviceTypeName;
    /**设备型号*/
    @TableField(value = "other_device_unit_type_name")
	private String otherDeviceUnitTypeName;
    /**设备生产厂家*/
    @TableField(value = "other_device_manufacturer")
	private String otherDeviceManufacturer;
    /**设备使用状态*/
    @TableField(value = "other_device_status")
	private String otherDeviceStatus;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  otherDevice
	 *@return: McMonitorOtherDevice  设备Id
	 */
	public String getOtherDeviceId(){
		return this.otherDeviceId;
	}

	/**
	 * 设置  otherDevice
	 *@param: otherDevice  设备Id
	 */
	public void setOtherDeviceId(String otherDeviceId){
		this.otherDeviceId = otherDeviceId;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  porjectName
	 *@return: String  项目名称
	 */
	public String getPorjectName(){
		return this.porjectName;
	}

	/**
	 * 设置  porjectName
	 *@param: porjectName  项目名称
	 */
	public void setPorjectName(String porjectName){
		this.porjectName = porjectName;
	}
	/**
	 * 获取  otherDeviceName
	 *@return: String  设备名称
	 */
	public String getOtherDeviceName(){
		return this.otherDeviceName;
	}

	/**
	 * 设置  otherDeviceName
	 *@param: otherDeviceName  设备名称
	 */
	public void setOtherDeviceName(String otherDeviceName){
		this.otherDeviceName = otherDeviceName;
	}
	/**
	 * 获取  otherDeviceCode
	 *@return: String  设备编码
	 */
	public String getOtherDeviceCode(){
		return this.otherDeviceCode;
	}

	/**
	 * 设置  otherDeviceCode
	 *@param: otherDeviceCode  设备编码
	 */
	public void setOtherDeviceCode(String otherDeviceCode){
		this.otherDeviceCode = otherDeviceCode;
	}
	/**
	 * 获取  otherDeviceTypeName
	 *@return: String  设备类型
	 */
	public String getOtherDeviceTypeName(){
		return this.otherDeviceTypeName;
	}

	/**
	 * 设置  otherDeviceTypeName
	 *@param: otherDeviceTypeName  设备类型
	 */
	public void setOtherDeviceTypeName(String otherDeviceTypeName){
		this.otherDeviceTypeName = otherDeviceTypeName;
	}
	/**
	 * 获取  otherDeviceUnitTypeName
	 *@return: String  设备型号
	 */
	public String getOtherDeviceUnitTypeName(){
		return this.otherDeviceUnitTypeName;
	}

	/**
	 * 设置  otherDeviceUnitTypeName
	 *@param: otherDeviceUnitTypeName  设备型号
	 */
	public void setOtherDeviceUnitTypeName(String otherDeviceUnitTypeName){
		this.otherDeviceUnitTypeName = otherDeviceUnitTypeName;
	}
	/**
	 * 获取  otherDeviceManufacturer
	 *@return: String  设备生产厂家
	 */
	public String getOtherDeviceManufacturer(){
		return this.otherDeviceManufacturer;
	}

	/**
	 * 设置  otherDeviceManufacturer
	 *@param: otherDeviceManufacturer  设备生产厂家
	 */
	public void setOtherDeviceManufacturer(String otherDeviceManufacturer){
		this.otherDeviceManufacturer = otherDeviceManufacturer;
	}
	/**
	 * 获取  otherDeviceStatus
	 *@return: String  设备使用状态
	 */
	public String getOtherDeviceStatus(){
		return this.otherDeviceStatus;
	}

	/**
	 * 设置  otherDeviceStatus
	 *@param: otherDeviceStatus  设备使用状态
	 */
	public void setOtherDeviceStatus(String otherDeviceStatus){
		this.otherDeviceStatus = otherDeviceStatus;
	}
	
}
