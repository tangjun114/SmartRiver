package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectCheck;
 
/**   
 * @Title: 项目检查数据库控制层接口
 * @Description: 项目检查数据库控制层接口
 * @author wsh
 * @date 2019-03-31 16:40:32
 * @version V1.0   
 *
 */
public interface SrProjectCheckMapper extends BaseMapper<SrProjectCheck> {
    
}