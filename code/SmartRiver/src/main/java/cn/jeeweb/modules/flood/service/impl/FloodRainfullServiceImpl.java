package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.flood.mapper.FloodRainfullMapper;
import cn.jeeweb.modules.flood.entity.FloodRainfull;
import cn.jeeweb.modules.flood.service.IFloodRainfullService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 雨量监测
 * @Description: 雨量监测
 * @author wsh
 * @date 2018-11-27 18:15:03
 * @version V1.0   
 *
 */
@Transactional
@Service("floodRainfullService")
public class FloodRainfullServiceImpl  extends CommonServiceImpl<FloodRainfullMapper,FloodRainfull> implements  IFloodRainfullService {

}
