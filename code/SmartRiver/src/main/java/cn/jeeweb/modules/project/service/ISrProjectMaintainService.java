package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectMaintain;

/**   
 * @Title: 项目维护
 * @Description: 项目维护
 * @author wsh
 * @date 2018-12-25 20:43:42
 * @version V1.0   
 *
 */
public interface ISrProjectMaintainService extends ICommonService<SrProjectMaintain> {

}

