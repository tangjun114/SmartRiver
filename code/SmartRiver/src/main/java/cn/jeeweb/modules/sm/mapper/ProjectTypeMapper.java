package cn.jeeweb.modules.sm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.sm.entity.ProjectType;
 
/**   
 * @Title: 项目类型数据库控制层接口
 * @Description: 项目类型数据库控制层接口
 * @author shawloong
 * @date 2017-09-28 00:43:37
 * @version V1.0   
 *
 */
public interface ProjectTypeMapper extends BaseMapper<ProjectType> {
    
}