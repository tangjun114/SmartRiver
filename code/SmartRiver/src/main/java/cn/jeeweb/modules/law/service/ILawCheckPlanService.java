package cn.jeeweb.modules.law.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.law.entity.LawCheckPlan;

/**   
 * @Title: 巡检计划
 * @Description: 巡检计划
 * @author liyonglei
 * @date 2018-11-13 20:27:22
 * @version V1.0   
 *
 */
public interface ILawCheckPlanService extends ICommonService<LawCheckPlan> {

}

