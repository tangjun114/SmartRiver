package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemAlarmModifyLogMapper;
import cn.jeeweb.modules.supervise.entity.McProItemAlarmModifyLog;
import cn.jeeweb.modules.supervise.service.IMcProItemAlarmModifyLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 报警修改日志
 * @Description: 报警修改日志
 * @author Aether
 * @date 2018-06-16 20:17:34
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemAlarmModifyLogService")
public class McProItemAlarmModifyLogServiceImpl  extends CommonServiceImpl<McProItemAlarmModifyLogMapper,McProItemAlarmModifyLog> implements  IMcProItemAlarmModifyLogService {

}
