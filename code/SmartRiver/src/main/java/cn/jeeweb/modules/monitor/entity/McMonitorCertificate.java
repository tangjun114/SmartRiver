package cn.jeeweb.modules.monitor.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;

/**   
 * @Title: 机构证书管理
 * @Description: 机构证书管理
 * @author shawloong
 * @date 2017-10-05 22:28:45
 * @version V1.0   
 *
 */
@TableName("mc_monitor_certificate")
@SuppressWarnings("serial")
public class McMonitorCertificate extends AbstractEntity<String> {

    /**id*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**监督机构*/
    @TableField(value = "monitor_id",el="monitor.id")
	private McMonitorOrg monitor;
    /**证书编号*/
    @TableField(value = "code")
	private String code;
    /**证书名称*/
    @TableField(value = "name")
	private String name;
    /**图片*/
    @TableField(value = "img_path")
	private String imgPath;
	
	/**
	 * 获取  id
	 *@return: String  id
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  id
	 */
	public void setId(String id){
		this.id = id;
	}
	
	/**
	 * @return the monitor
	 */
	public McMonitorOrg getMonitor() {
		return monitor;
	}

	/**
	 * @param monitor the monitor to set
	 */
	public void setMonitor(McMonitorOrg monitor) {
		this.monitor = monitor;
	}

	/**
	 * 获取  code
	 *@return: String  证书编号
	 */
	public String getCode(){
		return this.code;
	}

	/**
	 * 设置  code
	 *@param: code  证书编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 * 获取  name
	 *@return: String  证书名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  证书名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  imgPath
	 *@return: String  图片
	 */
	public String getImgPath(){
		return this.imgPath;
	}

	/**
	 * 设置  imgPath
	 *@param: imgPath  图片
	 */
	public void setImgPath(String imgPath){
		this.imgPath = imgPath;
	}
	
}
