package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.SrFloodWarnLog;
 
/**   
 * @Title: 报警记录数据库控制层接口
 * @Description: 报警记录数据库控制层接口
 * @author wsh
 * @date 2019-03-17 14:20:32
 * @version V1.0   
 *
 */
public interface SrFloodWarnLogMapper extends BaseMapper<SrFloodWarnLog> {
    
}