package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.flood.mapper.FloodDestoryMapper;
import cn.jeeweb.modules.flood.entity.FloodDestory;
import cn.jeeweb.modules.flood.service.IFloodDestoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 水毁信息表
 * @Description: 水毁信息表
 * @author wsh
 * @date 2018-11-26 21:01:04
 * @version V1.0   
 *
 */
@Transactional
@Service("floodDestoryService")
public class FloodDestoryServiceImpl  extends CommonServiceImpl<FloodDestoryMapper,FloodDestory> implements  IFloodDestoryService {

}
