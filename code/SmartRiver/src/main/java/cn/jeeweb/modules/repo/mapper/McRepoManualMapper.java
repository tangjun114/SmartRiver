package cn.jeeweb.modules.repo.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.repo.entity.McRepoManual;
 
/**   
 * @Title: 平台手册 manual数据库控制层接口
 * @Description: 平台手册 manual数据库控制层接口
 * @author shawloong
 * @date 2017-10-04 14:28:20
 * @version V1.0   
 *
 */
public interface McRepoManualMapper extends BaseMapper<McRepoManual> {
    
}