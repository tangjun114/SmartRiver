package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectFloodManagerMapper;
import cn.jeeweb.modules.project.entity.SrProjectFloodManager;
import cn.jeeweb.modules.project.service.ISrProjectFloodManagerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 工程水务管理
 * @Description: 工程水务管理
 * @author wsh
 * @date 2018-12-25 19:33:17
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectFloodManagerService")
public class SrProjectFloodManagerServiceImpl  extends CommonServiceImpl<SrProjectFloodManagerMapper,SrProjectFloodManager> implements  ISrProjectFloodManagerService {

}
