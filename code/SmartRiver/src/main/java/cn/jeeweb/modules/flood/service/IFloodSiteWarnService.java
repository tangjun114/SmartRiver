package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodSiteWarn;

/**   
 * @Title: 站点警报阈值
 * @Description: 站点警报阈值
 * @author wsh
 * @date 2018-12-02 23:09:31
 * @version V1.0   
 *
 */
public interface IFloodSiteWarnService extends ICommonService<FloodSiteWarn> {

}

