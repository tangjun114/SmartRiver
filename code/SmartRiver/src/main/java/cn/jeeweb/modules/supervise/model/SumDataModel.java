package cn.jeeweb.modules.supervise.model;

import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;

public class SumDataModel 
{
	private String monitorItemId;
	private String monitorItemName;

	private String maxName;
	private String minName;
	private String rateName;
	
	private String maxValue="--";
	private String minValue="--";
	private String rateValue;
	
	private String maxPoint="--";
	private String minPoint="--";
 	private String ratePoint;
	
	private String alarmValue;
	private String controlValue;
  	private String rateAlarmValue;
	private String rateControlValue;
	
	
	private String alarmPoint;
	private String alarmStatus;
	
	private McProItemRealTimeMonData maxData;
	private McProItemRealTimeMonData minData;
	public String getMonitorItemId() {
		return monitorItemId;
	}
	public void setMonitorItemId(String monitorItemId) {
		this.monitorItemId = monitorItemId;
	}
	public String getMonitorItemName() {
		return monitorItemName;
	}
	public void setMonitorItemName(String monitorItemName) {
		this.monitorItemName = monitorItemName;
	}
	public McProItemRealTimeMonData getMaxData() {
		return maxData;
	}
	public void setMaxData(McProItemRealTimeMonData maxData) {
		this.maxData = maxData;
	}
	public McProItemRealTimeMonData getMinData() {
		return minData;
	}
	public void setMinData(McProItemRealTimeMonData minData) {
		this.minData = minData;
	}
	public String getMaxName() {
		return maxName;
	}
	public void setMaxName(String maxName) {
		this.maxName = maxName;
	}
	public String getMinName() {
		return minName;
	}
	public void setMinName(String minName) {
		this.minName = minName;
	}
	public String getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}
	public String getMinValue() {
		return minValue;
	}
	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}
	public String getMaxPoint() {
		return maxPoint;
	}
	public void setMaxPoint(String maxPoint) {
		this.maxPoint = maxPoint;
	}
	public String getMinPoint() {
		return minPoint;
	}
	public void setMinPoint(String minPoint) {
		this.minPoint = minPoint;
	}
	public String getAlarmValue() {
		return alarmValue;
	}
	public void setAlarmValue(String alarmValue) {
		this.alarmValue = alarmValue;
	}
	public String getControlValue() {
		return controlValue;
	}
	public void setControlValue(String controlValue) {
		this.controlValue = controlValue;
	}
	public String getAlarmPoint() {
		return alarmPoint;
	}
	public void setAlarmPoint(String alarmPoint) {
		this.alarmPoint = alarmPoint;
	}
	public String getAlarmStatus() {
		return alarmStatus;
	}
	public void setAlarmStatus(String alarmStatus) {
		this.alarmStatus = alarmStatus;
	}
	public String getRateName() {
		return rateName;
	}
	public void setRateName(String rateName) {
		this.rateName = rateName;
	}
	public String getRateValue() {
		return rateValue;
	}
	public void setRateValue(String rateValue) {
		this.rateValue = rateValue;
	}
	public String getRatePoint() {
		return ratePoint;
	}
	public void setRatePoint(String ratePoint) {
		this.ratePoint = ratePoint;
	}
	public String getRateAlarmValue() {
		return rateAlarmValue;
	}
	public void setRateAlarmValue(String rateAlarmValue) {
		this.rateAlarmValue = rateAlarmValue;
	}
	public String getRateControlValue() {
		return rateControlValue;
	}
	public void setRateControlValue(String rateControlValue) {
		this.rateControlValue = rateControlValue;
	}
 
	
	

}
