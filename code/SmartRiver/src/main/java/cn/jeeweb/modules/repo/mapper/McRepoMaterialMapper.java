package cn.jeeweb.modules.repo.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.repo.entity.McRepoMaterial;
 
/**   
 * @Title: 资料下载数据库控制层接口
 * @Description: 资料下载数据库控制层接口
 * @author shawloong
 * @date 2017-10-04 13:26:47
 * @version V1.0   
 *
 */
public interface McRepoMaterialMapper extends BaseMapper<McRepoMaterial> {
    
}