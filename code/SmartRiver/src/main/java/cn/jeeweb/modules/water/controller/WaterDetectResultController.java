package cn.jeeweb.modules.water.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.water.entity.WaterDetectResult;

/**   
 * @Title: 检测数据
 * @Description: 检测数据
 * @author shawloong
 * @date 2018-11-13 21:18:03
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/water/waterdetectresult")
@RequiresPathPermission("water:waterdetectresult")
public class WaterDetectResultController extends BaseCRUDController<WaterDetectResult, String> {

}
