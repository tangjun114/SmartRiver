package cn.jeeweb.modules.supervise.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.AbstractEntity;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 完工审批表
 * @Description: 完工审批表
 * @author shawloong
 * @date 2017-10-07 16:28:41
 * @version V1.0   
 *
 */
@TableName("mc_completion_approval")
@SuppressWarnings("serial")
public class McCompletionApproval extends AbstractEntity<String> {

    /**id*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**名称*/
    @TableField(value = "name")
	private String name;
    /**申请人ID*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**申请人*/
    @TableField(value = "create_by_name")
	private String createByName;
    /**审批人ID*/
    @TableField(value = "approval_by")
	private String approvalBy;
    /**审批人*/
    @TableField(value = "approval_by_name")
	private String approvalByBame;
    /**申请状态*/
    @TableField(value = "status")
	private String status;
    /**完工证明*/
    @TableField(value = "certificate")
	private String certificate;
    /**完工证明名称*/
    @TableField(value = "certificate_name")
	private String certificateName;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**备注*/
    @TableField(value = "remark")
	private String remark;
    /**审批意见（预留）*/
    @TableField(value = "approval_desc")
	private String approvalDesc;
    /**审批时间*/
    @TableField(value = "approval_date")
	private Date approvalDate;
	
	/**
	 * 获取  id
	 *@return: String  id
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  name
	 *@return: String  名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  createBy
	 *@return: User  申请人ID
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  申请人ID
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createByName
	 *@return: String  申请人
	 */
	public String getCreateByName(){
		return this.createByName;
	}

	/**
	 * 设置  createByName
	 *@param: createByName  申请人
	 */
	public void setCreateByName(String createByName){
		this.createByName = createByName;
	}
	/**
	 * 获取  approvalBy
	 *@return: String  审批人ID
	 */
	public String getApprovalBy(){
		return this.approvalBy;
	}

	/**
	 * 设置  approvalBy
	 *@param: approvalBy  审批人ID
	 */
	public void setApprovalBy(String approvalBy){
		this.approvalBy = approvalBy;
	}
	/**
	 * 获取  approvalByBame
	 *@return: String  审批人
	 */
	public String getApprovalByBame(){
		return this.approvalByBame;
	}

	/**
	 * 设置  approvalByBame
	 *@param: approvalByBame  审批人
	 */
	public void setApprovalByBame(String approvalByBame){
		this.approvalByBame = approvalByBame;
	}
	/**
	 * 获取  status
	 *@return: String  申请状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  申请状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  certificate
	 *@return: String  完工证明
	 */
	public String getCertificate(){
		return this.certificate;
	}

	/**
	 * 设置  certificate
	 *@param: certificate  完工证明
	 */
	public void setCertificate(String certificate){
		this.certificate = certificate;
	}
	/**
	 * 获取  certificateName
	 *@return: String  完工证明名称
	 */
	public String getCertificateName(){
		return this.certificateName;
	}

	/**
	 * 设置  certificateName
	 *@param: certificateName  完工证明名称
	 */
	public void setCertificateName(String certificateName){
		this.certificateName = certificateName;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  remark
	 *@return: String  备注
	 */
	public String getRemark(){
		return this.remark;
	}

	/**
	 * 设置  remark
	 *@param: remark  备注
	 */
	public void setRemark(String remark){
		this.remark = remark;
	}
	/**
	 * 获取  approvalDesc
	 *@return: String  审批意见（预留）
	 */
	public String getApprovalDesc(){
		return this.approvalDesc;
	}

	/**
	 * 设置  approvalDesc
	 *@param: approvalDesc  审批意见（预留）
	 */
	public void setApprovalDesc(String approvalDesc){
		this.approvalDesc = approvalDesc;
	}
	/**
	 * 获取  approvalDate
	 *@return: Date  审批时间
	 */
	public Date getApprovalDate(){
		return this.approvalDate;
	}

	/**
	 * 设置  approvalDate
	 *@param: approvalDate  审批时间
	 */
	public void setApprovalDate(Date approvalDate){
		this.approvalDate = approvalDate;
	}
	
}
