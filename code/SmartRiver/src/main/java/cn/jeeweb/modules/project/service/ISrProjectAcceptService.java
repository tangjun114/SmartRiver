package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectAccept;

/**   
 * @Title: 项目验收
 * @Description: 项目验收
 * @author wsh
 * @date 2019-03-31 21:56:15
 * @version V1.0   
 *
 */
public interface ISrProjectAcceptService extends ICommonService<SrProjectAccept> {

}

