package cn.jeeweb.modules.sm.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.DataEntity;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 身份管理
 * @Description: 身份管理
 * @author shawloong
 * @date 2017-09-29 00:44:22
 * @version V1.0   
 *
 */
@TableName("sm_identity_manager")
@SuppressWarnings("serial")
public class IdentityManager extends DataEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
     
    /**身份名称*/
    @TableField(value = "name")
	private String name;
    /**描述*/
    @TableField(value = "desc")
	private String desc;
    /**类型*/
    @TableField(value = "type")
	private String type;
    /**状态*/
    @TableField(value = "status")
	private String status;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
 
	/**
	 * 获取  name
	 *@return: String  身份名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  身份名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  desc
	 *@return: String  描述
	 */
	public String getDesc(){
		return this.desc;
	}

	/**
	 * 设置  desc
	 *@param: desc  描述
	 */
	public void setDesc(String desc){
		this.desc = desc;
	}
	/**
	 * 获取  type
	 *@return: String  类型
	 */
	public String getType(){
		return this.type;
	}

	/**
	 * 设置  type
	 *@param: type  类型
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	
}