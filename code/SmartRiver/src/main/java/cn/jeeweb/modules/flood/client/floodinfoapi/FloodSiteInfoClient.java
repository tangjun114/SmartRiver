package cn.jeeweb.modules.flood.client.floodinfoapi;

import cn.jeeweb.modules.flood.client.GetFloodInfoClient;
import cn.jeeweb.modules.flood.client.entity.ReqParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author:Wish
 * @Date: Created in 2018/12/14 10:08
 **/
public class FloodSiteInfoClient extends GetFloodInfoClient {

    private static String interfaceName = "stGroupApi.getStGroup";


    private Map<String, Object> getMap(String fcch, String gid, String r) {
        Map<String, Object> map = new HashMap<>();
        map.put("fcch", fcch);
        map.put("gid", gid);
        map.put("r", r);
        return map;
    }


    public FloodSiteInfoClient setReqParam(String fcch, String gid, String r) {
        ReqParam reqParam = new ReqParam();
        reqParam.setParams(getMap(fcch, gid, r));
        reqParam.setInterfaceName(interfaceName);
        reqParam.setToken(token);
        reqParams.add(reqParam);
        return this;
    }

    public static void main(String[] args) {
        GetFloodInfoClient client = new FloodSiteInfoClient()
                .setReqParam("1", "2", "1")
                .setReqParam("1", "3", "1");
        System.out.println(client.getFloodInfo());

    }
}
