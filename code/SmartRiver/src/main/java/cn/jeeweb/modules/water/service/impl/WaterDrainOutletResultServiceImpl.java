package cn.jeeweb.modules.water.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.water.mapper.WaterDrainOutletResultMapper;
import cn.jeeweb.modules.water.entity.WaterDrainOutletResult;
import cn.jeeweb.modules.water.service.IWaterDrainOutletResultService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 排污口水质监控
 * @Description: 排污口水质监控
 * @author liyonglei
 * @date 2018-11-14 19:41:31
 * @version V1.0   
 *
 */
@Transactional
@Service("waterDrainOutletResultService")
public class WaterDrainOutletResultServiceImpl  extends CommonServiceImpl<WaterDrainOutletResultMapper,WaterDrainOutletResult> implements  IWaterDrainOutletResultService {

}
