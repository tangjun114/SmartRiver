package cn.jeeweb.modules.law.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.law.entity.LawCheckResult;
 
/**   
 * @Title: 巡查结果数据库控制层接口
 * @Description: 巡查结果数据库控制层接口
 * @author liyonglei
 * @date 2018-11-13 20:40:25
 * @version V1.0   
 *
 */
public interface LawCheckResultMapper extends BaseMapper<LawCheckResult> {
    
}