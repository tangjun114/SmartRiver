package cn.jeeweb.modules.supervise.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jeeweb.core.model.AjaxJson;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.sms.data.SmsResult;
import cn.jeeweb.modules.sms.service.ISmsSendService;
import cn.jeeweb.modules.supervise.entity.McProItemInspection;
import cn.jeeweb.modules.supervise.entity.McProItemMember;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProItemMemberService;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 抽查记录
 * @Description: 抽查记录
 * @author jerry
 * @date 2018-06-04 10:36:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproiteminspection")
@RequiresPathPermission("supervise:mcproiteminspection")
public class McProItemInspectionController extends McProjectBaseController<McProItemInspection, String> {

	@Autowired
	private ISmsSendService smsSendService;
	
	@Autowired
	private IMcProjectService mcProjectService;
	@Autowired
	private IMcProItemMemberService  mcProItemMemberService;
	
	@RequestMapping(value = "/send", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson inspect(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String id = request.getParameter("gid");
		 
		McProItemInspection inspect = get(id);
		 
		AjaxJson ajaxJson = new AjaxJson();	
		ajaxJson.success("发送成功");
		try {
			
			McProject project = mcProjectService.selectById(inspect.getProjectId());
			String content = "%s您好,我是监督员:%s，对您发出检查指令，请及时使用上传软件录像上传本人及%s项目的监测相关内容，以备检查。";
			content = String.format(content, project.getPersonInChargeName(),UserUtils.getUser().getRealname(),project.getName());
			 
			inspect.setInspectUser(UserUtils.getUser().getRealname());
			McProItemMember member = mcProItemMemberService.selectById(project.getPersonInCharge());
			SmsResult smsResult = smsSendService.sendSyncSmsByCode(member.getPhone(), "130107", project.getPersonInChargeName(),UserUtils.getUser().getRealname(),project.getName());

			this.commonService.updateAllColumnById(inspect);
			
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.fail("短信发送失败");
		}
		return ajaxJson;
	}
}
