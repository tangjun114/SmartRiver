package cn.jeeweb.modules.repo.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.repo.entity.McRepoParams;
 
/**   
 * @Title: 技术参数数据库控制层接口
 * @Description: 技术参数数据库控制层接口
 * @author shawloong
 * @date 2017-10-04 01:19:11
 * @version V1.0   
 *
 */
public interface McRepoParamsMapper extends BaseMapper<McRepoParams> {
    
}