package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectQualityIndicator;
 
/**   
 * @Title: 项目质量指标数据库控制层接口
 * @Description: 项目质量指标数据库控制层接口
 * @author jerry
 * @date 2018-11-14 17:57:30
 * @version V1.0   
 *
 */
public interface SrProjectQualityIndicatorMapper extends BaseMapper<SrProjectQualityIndicator> {
    
}