package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectSchedule;
 
/**   
 * @Title: 项目进度数据库控制层接口
 * @Description: 项目进度数据库控制层接口
 * @author wsh
 * @date 2018-12-10 20:41:29
 * @version V1.0   
 *
 */
public interface SrProjectScheduleMapper extends BaseMapper<SrProjectSchedule> {
    
}