package cn.jeeweb.modules.supervise.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProItemStandard;

/**   
 * @Title: 项目规范
 * @Description: 项目规范
 * @author jerry
 * @date 2017-12-25 15:25:05
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemstandard")
@RequiresPathPermission("supervise:mcproitemstandard")
public class McProItemStandardController extends McProjectBaseController<McProItemStandard, String> {

}
