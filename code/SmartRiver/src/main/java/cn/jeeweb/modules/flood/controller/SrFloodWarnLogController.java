package cn.jeeweb.modules.flood.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.SrFloodWarnLog;

/**   
 * @Title: 报警记录
 * @Description: 报警记录
 * @author wsh
 * @date 2019-03-17 14:20:32
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/srfloodwarnlog")
@RequiresPathPermission("flood:srfloodwarnlog")
public class SrFloodWarnLogController extends BaseCRUDController<SrFloodWarnLog, String> {

}
