package cn.jeeweb.modules.sm.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.sm.entity.ProjectType;

/**   
 * @Title: 项目类型
 * @Description: 项目类型
 * @author shawloong
 * @date 2017-09-28 00:43:37
 * @version V1.0   
 *
 */
public interface IProjectTypeService extends ICommonService<ProjectType> {

}

