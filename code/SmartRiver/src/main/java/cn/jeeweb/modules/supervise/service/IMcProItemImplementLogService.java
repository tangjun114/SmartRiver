package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemImplementLog;

/**   
 * @Title: 项目监控-实施日志
 * @Description: 项目监控-实施日志
 * @author shawloong
 * @date 2017-11-15 00:48:21
 * @version V1.0   
 *
 */
public interface IMcProItemImplementLogService extends ICommonService<McProItemImplementLog> {

}

