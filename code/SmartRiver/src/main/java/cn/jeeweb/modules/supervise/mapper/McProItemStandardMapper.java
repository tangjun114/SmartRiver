package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemStandard;
 
/**   
 * @Title: 项目规范数据库控制层接口
 * @Description: 项目规范数据库控制层接口
 * @author jerry
 * @date 2017-12-25 15:25:05
 * @version V1.0   
 *
 */
public interface McProItemStandardMapper extends BaseMapper<McProItemStandard> {
    
}