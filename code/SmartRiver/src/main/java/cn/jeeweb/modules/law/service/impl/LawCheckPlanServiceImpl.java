package cn.jeeweb.modules.law.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.law.mapper.LawCheckPlanMapper;
import cn.jeeweb.modules.law.entity.LawCheckPlan;
import cn.jeeweb.modules.law.service.ILawCheckPlanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 巡检计划
 * @Description: 巡检计划
 * @author liyonglei
 * @date 2018-11-13 20:27:22
 * @version V1.0   
 *
 */
@Transactional
@Service("lawCheckPlanService")
public class LawCheckPlanServiceImpl  extends CommonServiceImpl<LawCheckPlanMapper,LawCheckPlan> implements  ILawCheckPlanService {

}
