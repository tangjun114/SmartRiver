package cn.jeeweb.modules.constant;

/*
 */

public enum FloodStockChangeTypeEnum {

    IN("入库","in"),
    OUT("出库","out"),
    MOVE_OUT("移出","move_out"),
    MOVE_IN("移入","move_in");

    private  String name;
    private String value;

    private FloodStockChangeTypeEnum(String name)
    {
        this.name = name;
    }

    private FloodStockChangeTypeEnum(String name,String value)
    {
        this.name = name;
        this.value = value;

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName()
    {
        return name;
    }
}
