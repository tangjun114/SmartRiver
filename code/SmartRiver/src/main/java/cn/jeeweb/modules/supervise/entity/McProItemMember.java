package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 项目人员
 * @Description: 项目人员
 * @author Jerry
 * @date 2018-02-28 19:36:25
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_member")
@SuppressWarnings("serial")
public class McProItemMember extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目Id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**名称*/
    @TableField(value = "username")
	private String username;
    /**电话*/
    @TableField(value = "phone")
	private String phone;
    /**邮箱*/
    @TableField(value = "email")
	private String email;
    /**成员类型*/
    @TableField(value = "type")
	private String type;
    /**用户id*/
    @TableField(value = "user_id")
	private String userId;
    /**短信权限*/
    @TableField(value = "msg_perm")
	private String msgPerm = "";
    /**报告权限*/
    @TableField(value = "report_perm")
	private String reportPerm = "";
    /**数据权限*/
    @TableField(value = "data_perm")
	private String dataPerm= "";
    /**部门*/
    @TableField(value = "dept_name")
	private String deptName;
    /**职位*/
    @TableField(value = "position")
	private String position;
    /**真实姓名*/
    @TableField(value = "realname")
	private String realname;
    /**客户端类型*/
    @TableField(value = "client_type")
	private String clientType;
    /**人员类别*/
    @TableField(value = "sub_type")
	private String subType;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目Id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目Id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  username
	 *@return: String  名称
	 */
	public String getUsername(){
		return this.username;
	}

	/**
	 * 设置  username
	 *@param: username  名称
	 */
	public void setUsername(String username){
		this.username = username;
	}
	/**
	 * 获取  phone
	 *@return: String  电话
	 */
	public String getPhone(){
		return this.phone;
	}

	/**
	 * 设置  phone
	 *@param: phone  电话
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	/**
	 * 获取  email
	 *@return: String  邮箱
	 */
	public String getEmail(){
		return this.email;
	}

	/**
	 * 设置  email
	 *@param: email  邮箱
	 */
	public void setEmail(String email){
		this.email = email;
	}
	/**
	 * 获取  type
	 *@return: String  成员类型
	 */
	public String getType(){
		return this.type;
	}

	/**
	 * 设置  type
	 *@param: type  成员类型
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 * 获取  userId
	 *@return: String  用户id
	 */
	public String getUserId(){
		return this.userId;
	}

	/**
	 * 设置  userId
	 *@param: userId  用户id
	 */
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**
	 * 获取  msgPerm
	 *@return: String  短信权限
	 */
	public String getMsgPerm(){
		return this.msgPerm;
	}

	/**
	 * 设置  msgPerm
	 *@param: msgPerm  短信权限
	 */
	public void setMsgPerm(String msgPerm){
		this.msgPerm = msgPerm;
	}
	/**
	 * 获取  reportPerm
	 *@return: String  报告权限
	 */
	public String getReportPerm(){
		return this.reportPerm;
	}

	/**
	 * 设置  reportPerm
	 *@param: reportPerm  报告权限
	 */
	public void setReportPerm(String reportPerm){
		this.reportPerm = reportPerm;
	}
	/**
	 * 获取  dataPerm
	 *@return: String  数据权限
	 */
	public String getDataPerm(){
		return this.dataPerm;
	}

	/**
	 * 设置  dataPerm
	 *@param: dataPerm  数据权限
	 */
	public void setDataPerm(String dataPerm){
		this.dataPerm = dataPerm;
	}
	/**
	 * 获取  deptName
	 *@return: String  部门
	 */
	public String getDeptName(){
		return this.deptName;
	}

	/**
	 * 设置  deptName
	 *@param: deptName  部门
	 */
	public void setDeptName(String deptName){
		this.deptName = deptName;
	}
	/**
	 * 获取  position
	 *@return: String  职位
	 */
	public String getPosition(){
		return this.position;
	}

	/**
	 * 设置  position
	 *@param: position  职位
	 */
	public void setPosition(String position){
		this.position = position;
	}
	/**
	 * 获取  realname
	 *@return: String  真实姓名
	 */
	public String getRealname(){
		return this.realname;
	}

	/**
	 * 设置  realname
	 *@param: realname  真实姓名
	 */
	public void setRealname(String realname){
		this.realname = realname;
	}
	/**
	 * 获取  clientType
	 *@return: String  客户端类型
	 */
	public String getClientType(){
		return this.clientType;
	}

	/**
	 * 设置  clientType
	 *@param: clientType  客户端类型
	 */
	public void setClientType(String clientType){
		this.clientType = clientType;
	}
	/**
	 * 获取  subType
	 *@return: String  人员类别
	 */
	public String getSubType(){
		return this.subType;
	}

	/**
	 * 设置  subType
	 *@param: subType  人员类别
	 */
	public void setSubType(String subType){
		this.subType = subType;
	}
	
}
