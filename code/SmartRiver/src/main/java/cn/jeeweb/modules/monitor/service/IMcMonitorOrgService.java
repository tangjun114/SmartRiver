package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McMonitorOrg;

/**   
 * @Title: 监督检测机构
 * @Description: 监督检测机构
 * @author shawloong
 * @date 2017-10-05 22:28:45
 * @version V1.0   
 *
 */
public interface IMcMonitorOrgService extends ICommonService<McMonitorOrg> {

}

