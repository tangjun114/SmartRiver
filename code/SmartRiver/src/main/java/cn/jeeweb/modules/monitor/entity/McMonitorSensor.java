package cn.jeeweb.modules.monitor.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 传感器信息
 * @Description: 传感器信息
 * @author shawloong
 * @date 2018-02-23 17:40:49
 * @version V1.0   
 *
 */
@TableName("mc_monitor_sensor")
@SuppressWarnings("serial")
public class McMonitorSensor extends AbstractEntity<String> {

    /**id*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**监测机构ID*/
    @TableField(value = "monitor_id")
	private String monitorId;
    /**监测机构*/
    @TableField(value = "monitor_name")
	private String monitorName;
    /**传感器编号*/
    @TableField(value = "sensor_num")
	private String sensorNum;
    /**监测项id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**监测项类型名称*/
    @TableField(value = "monitor_item_type_name")
	private String monitorItemTypeName;
    /**监测项类型编码*/
    @TableField(value = "monitor_item_type_code")
	private String monitorItemTypeCode;
    /**传感器类型值*/
    @TableField(value = "sensor_type")
	private String sensorType;
    /**传感器类型值*/
    @TableField(value = "sensor_type_name")
	private String sensorTypeName;
    /**传感器型号值*/
    @TableField(value = "sensor_model")
	private String sensorModel;
    /**传感器型号*/
    @TableField(value = "sensor_model_name")
	private String sensorModelName;
    /**精度*/
    @TableField(value = "accuracy")
	private String accuracy;
    /**钢筋计截面积（mm2）*/
    @TableField(value = "rebar_sect_area")
	private String rebarSectArea;
    /**量程最小值*/
    @TableField(value = "range_min")
	private String rangeMin;
    /**标定系数(K)*/
    @TableField(value = "cali_coefficient")
	private String caliCoefficient="0";
    /**初始值*/
    @TableField(value = "initial_value")
	private String initialValue;
    /**A值*/
    @TableField(value = "a_value")
	private String aValue;
    /**B值*/
    @TableField(value = "b_value")
	private String bValue;
    /**C值*/
    @TableField(value = "c_value")
	private String cValue;
    /**温补系数*/
    @TableField(value = "temp_coefficient")
	private String tempCoefficient;
    /**设备状态*/
    @TableField(value = "status")
	private String status;
    /**图片*/
    @TableField(value = "img")
	private String img;
    /**关联项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**测点Id*/
    @TableField(value = "measure_point_id")
	private String measurePointId;
    /**测点编码*/
    @TableField(value = "measure_point_code")
	private String measurePointCode;
    /**购买时间*/
    @TableField(value = "buy_date")
	private Date buyDate;
    /**购买价格*/
    @TableField(value = "buy_price")
	private Double buyPrice;
    /**规格*/
    @TableField(value = "spec")
	private String spec;
    /**备注*/
    @TableField(value = "remarks")
	private String remarks;
    /**量程最大值*/
    @TableField(value = "range_max")
	private String rangeMax;
    /**传感器参数*/
    @TableField(value = "sensor_para")
	private String sensorPara;
	
	/**
	 * 获取  id
	 *@return: String  id
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  monitorId
	 *@return: String  监测机构ID
	 */
	public String getMonitorId(){
		return this.monitorId;
	}

	/**
	 * 设置  monitorId
	 *@param: monitorId  监测机构ID
	 */
	public void setMonitorId(String monitorId){
		this.monitorId = monitorId;
	}
	/**
	 * 获取  monitorName
	 *@return: String  监测机构
	 */
	public String getMonitorName(){
		return this.monitorName;
	}

	/**
	 * 设置  monitorName
	 *@param: monitorName  监测机构
	 */
	public void setMonitorName(String monitorName){
		this.monitorName = monitorName;
	}
	/**
	 * 获取  sensorNum
	 *@return: String  传感器编号
	 */
	public String getSensorNum(){
		return this.sensorNum;
	}

	/**
	 * 设置  sensorNum
	 *@param: sensorNum  传感器编号
	 */
	public void setSensorNum(String sensorNum){
		this.sensorNum = sensorNum;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  monitorItemTypeName
	 *@return: String  监测项类型名称
	 */
	public String getMonitorItemTypeName(){
		return this.monitorItemTypeName;
	}

	/**
	 * 设置  monitorItemTypeName
	 *@param: monitorItemTypeName  监测项类型名称
	 */
	public void setMonitorItemTypeName(String monitorItemTypeName){
		this.monitorItemTypeName = monitorItemTypeName;
	}
	/**
	 * 获取  monitorItemTypeCode
	 *@return: String  监测项类型编码
	 */
	public String getMonitorItemTypeCode(){
		return this.monitorItemTypeCode;
	}

	/**
	 * 设置  monitorItemTypeCode
	 *@param: monitorItemTypeCode  监测项类型编码
	 */
	public void setMonitorItemTypeCode(String monitorItemTypeCode){
		this.monitorItemTypeCode = monitorItemTypeCode;
	}
	/**
	 * 获取  sensorType
	 *@return: String  传感器类型值
	 */
	public String getSensorType(){
		return this.sensorType;
	}

	/**
	 * 设置  sensorType
	 *@param: sensorType  传感器类型值
	 */
	public void setSensorType(String sensorType){
		this.sensorType = sensorType;
	}
	/**
	 * 获取  sensorTypeName
	 *@return: String  传感器类型值
	 */
	public String getSensorTypeName(){
		return this.sensorTypeName;
	}

	/**
	 * 设置  sensorTypeName
	 *@param: sensorTypeName  传感器类型值
	 */
	public void setSensorTypeName(String sensorTypeName){
		this.sensorTypeName = sensorTypeName;
	}
	/**
	 * 获取  sensorModel
	 *@return: String  传感器型号值
	 */
	public String getSensorModel(){
		return this.sensorModel;
	}

	/**
	 * 设置  sensorModel
	 *@param: sensorModel  传感器型号值
	 */
	public void setSensorModel(String sensorModel){
		this.sensorModel = sensorModel;
	}
	/**
	 * 获取  sensorModelName
	 *@return: String  传感器型号
	 */
	public String getSensorModelName(){
		return this.sensorModelName;
	}

	/**
	 * 设置  sensorModelName
	 *@param: sensorModelName  传感器型号
	 */
	public void setSensorModelName(String sensorModelName){
		this.sensorModelName = sensorModelName;
	}
	/**
	 * 获取  accuracy
	 *@return: String  精度
	 */
	public String getAccuracy(){
		return this.accuracy;
	}

	/**
	 * 设置  accuracy
	 *@param: accuracy  精度
	 */
	public void setAccuracy(String accuracy){
		this.accuracy = accuracy;
	}
	/**
	 * 获取  rebarSectArea
	 *@return: String  钢筋计截面积（mm2）
	 */
	public String getRebarSectArea(){
		return this.rebarSectArea;
	}

	/**
	 * 设置  rebarSectArea
	 *@param: rebarSectArea  钢筋计截面积（mm2）
	 */
	public void setRebarSectArea(String rebarSectArea){
		this.rebarSectArea = rebarSectArea;
	}
	/**
	 * 获取  rangeMin
	 *@return: String  量程最小值
	 */
	public String getRangeMin(){
		return this.rangeMin;
	}

	/**
	 * 设置  rangeMin
	 *@param: rangeMin  量程最小值
	 */
	public void setRangeMin(String rangeMin){
		this.rangeMin = rangeMin;
	}
	/**
	 * 获取  caliCoefficient
	 *@return: String  标定系数(K)
	 */
	public String getCaliCoefficient(){
		return this.caliCoefficient;
	}

	/**
	 * 设置  caliCoefficient
	 *@param: caliCoefficient  标定系数(K)
	 */
	public void setCaliCoefficient(String caliCoefficient){
		this.caliCoefficient = caliCoefficient;
	}
	/**
	 * 获取  initialValue
	 *@return: String  初始值
	 */
	public String getInitialValue(){
		return this.initialValue;
	}

	/**
	 * 设置  initialValue
	 *@param: initialValue  初始值
	 */
	public void setInitialValue(String initialValue){
		this.initialValue = initialValue;
	}
	/**
	 * 获取  aValue
	 *@return: String  A值
	 */
	public String getAValue(){
		return this.aValue;
	}

	/**
	 * 设置  aValue
	 *@param: aValue  A值
	 */
	public void setAValue(String aValue){
		this.aValue = aValue;
	}
	/**
	 * 获取  bValue
	 *@return: String  B值
	 */
	public String getBValue(){
		return this.bValue;
	}

	/**
	 * 设置  bValue
	 *@param: bValue  B值
	 */
	public void setBValue(String bValue){
		this.bValue = bValue;
	}
	/**
	 * 获取  cValue
	 *@return: String  C值
	 */
	public String getCValue(){
		return this.cValue;
	}

	/**
	 * 设置  cValue
	 *@param: cValue  C值
	 */
	public void setCValue(String cValue){
		this.cValue = cValue;
	}
	/**
	 * 获取  tempCoefficient
	 *@return: String  温补系数
	 */
	public String getTempCoefficient(){
		return this.tempCoefficient;
	}

	/**
	 * 设置  tempCoefficient
	 *@param: tempCoefficient  温补系数
	 */
	public void setTempCoefficient(String tempCoefficient){
		this.tempCoefficient = tempCoefficient;
	}
	/**
	 * 获取  status
	 *@return: String  设备状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  设备状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  img
	 *@return: String  图片
	 */
	public String getImg(){
		return this.img;
	}

	/**
	 * 设置  img
	 *@param: img  图片
	 */
	public void setImg(String img){
		this.img = img;
	}
	/**
	 * 获取  projectId
	 *@return: String  关联项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  关联项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  measurePointId
	 *@return: String  测点Id
	 */
	public String getMeasurePointId(){
		return this.measurePointId;
	}

	/**
	 * 设置  measurePointId
	 *@param: measurePointId  测点Id
	 */
	public void setMeasurePointId(String measurePointId){
		this.measurePointId = measurePointId;
	}
	/**
	 * 获取  measurePointCode
	 *@return: String  测点编码
	 */
	public String getMeasurePointCode(){
		return this.measurePointCode;
	}

	/**
	 * 设置  measurePointCode
	 *@param: measurePointCode  测点编码
	 */
	public void setMeasurePointCode(String measurePointCode){
		this.measurePointCode = measurePointCode;
	}
	/**
	 * 获取  buyDate
	 *@return: Date  购买时间
	 */
	public Date getBuyDate(){
		return this.buyDate;
	}

	/**
	 * 设置  buyDate
	 *@param: buyDate  购买时间
	 */
	public void setBuyDate(Date buyDate){
		this.buyDate = buyDate;
	}
	/**
	 * 获取  buyPrice
	 *@return: Double  购买价格
	 */
	public Double getBuyPrice(){
		return this.buyPrice;
	}

	/**
	 * 设置  buyPrice
	 *@param: buyPrice  购买价格
	 */
	public void setBuyPrice(Double buyPrice){
		this.buyPrice = buyPrice;
	}
	/**
	 * 获取  spec
	 *@return: String  规格
	 */
	public String getSpec(){
		return this.spec;
	}

	/**
	 * 设置  spec
	 *@param: spec  规格
	 */
	public void setSpec(String spec){
		this.spec = spec;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  rangeMax
	 *@return: String  量程最大值
	 */
	public String getRangeMax(){
		return this.rangeMax;
	}

	/**
	 * 设置  rangeMax
	 *@param: rangeMax  量程最大值
	 */
	public void setRangeMax(String rangeMax){
		this.rangeMax = rangeMax;
	}
	/**
	 * 获取  sensorPara
	 *@return: String  传感器参数
	 */
	public String getSensorPara(){
		return this.sensorPara;
	}

	/**
	 * 设置  sensorPara
	 *@param: sensorPara  传感器参数
	 */
	public void setSensorPara(String sensorPara){
		this.sensorPara = sensorPara;
	}
	
}