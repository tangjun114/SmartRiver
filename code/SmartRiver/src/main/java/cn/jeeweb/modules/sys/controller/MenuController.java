package cn.jeeweb.modules.sys.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.ff.common.util.meta.DataFilterUtil;

import cn.jeeweb.core.common.controller.BaseTreeController;
import cn.jeeweb.core.common.entity.tree.BootstrapTreeNode;
import cn.jeeweb.core.query.data.PropertyPreFilterable;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.ObjectUtils;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.sys.entity.Menu;
import cn.jeeweb.modules.sys.service.IMenuService;
import cn.jeeweb.modules.sys.utils.UserUtils;

@Controller
@RequestMapping("${admin.url.prefix}/sys/menu")
@RequiresPathPermission("sys:menu")
public class MenuController extends BaseTreeController<Menu, String> {
	@Autowired
	private IMenuService menuService;
	@RequestMapping(value = "menuData")
	private void bootstrapTreeData(Queryable queryable,
			@RequestParam(value = "nodeid", required = false, defaultValue = "") String nodeid, HttpServletRequest request,
			HttpServletResponse response, PropertyPreFilterable propertyPreFilterable) throws IOException {
		EntityWrapper<Menu> entityWrapper = new EntityWrapper<Menu>(entityClass);
		entityWrapper.setTableAlias("t.");
		entityWrapper.orderBy("t.sort ", true);
		if(!ObjectUtils.isNullOrEmpty(nodeid)){
			entityWrapper.eq("parentId", nodeid);
		}
		entityWrapper.eq("isshow", 1);
		List<Menu> treeNodeList = menuService.selectTreeList(queryable, entityWrapper);
		List<Menu> menus = UserUtils.getMenuList();
		Map<String,Menu> map = DataFilterUtil.buildMap("id", menus);
		List<BootstrapTreeNode> bootstrapTreeNodes = new ArrayList<>();
		for (Menu menu : treeNodeList) {
			
			if(! map.containsKey(menu.getId()))
			{
				continue;
			}
			
			BootstrapTreeNode bootstrapTreeNode = new BootstrapTreeNode();
			bootstrapTreeNode.setHref(menu.getUrl());
			bootstrapTreeNode.setText(menu.getName());
			bootstrapTreeNode.setIcon(menu.getMenuIcon());
			if(menu.isHasChildren()){
				EntityWrapper<Menu> entityWrapper1 = new EntityWrapper<Menu>(entityClass);
				entityWrapper1.setTableAlias("t.");
				entityWrapper1.orderBy("t.sort ", true);
				entityWrapper1.eq("parentId", menu.getId());
				List<Menu> childList = menuService.selectTreeList(entityWrapper1);
				if(!ObjectUtils.isNullOrEmpty(childList)){
					for(Menu menu1 : childList){
						
						if(! map.containsKey(menu1.getId()))
						{
							continue;
						}
						
						BootstrapTreeNode bootstrapTreeNode1 = new BootstrapTreeNode();
						bootstrapTreeNode1.setHref(menu1.getUrl());
						bootstrapTreeNode1.setText(menu1.getName());
						bootstrapTreeNode1.setIcon(menu1.getMenuIcon());
						bootstrapTreeNode.getNodes().add(bootstrapTreeNode1);
						

					}
					
				}
			}

			bootstrapTreeNodes.add(bootstrapTreeNode);
		}
		
		propertyPreFilterable.addQueryProperty("id", "name", "expanded", "hasChildren", "leaf", "loaded", "level",
				"parentId");
		SerializeFilter filter = propertyPreFilterable.constructFilter(entityClass);
		String content = JSON.toJSONString(bootstrapTreeNodes, filter);
		StringUtils.printJson(response, content);
	}
}
