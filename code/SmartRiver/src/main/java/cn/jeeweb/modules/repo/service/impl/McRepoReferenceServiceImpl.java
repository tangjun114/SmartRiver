package cn.jeeweb.modules.repo.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.repo.mapper.McRepoReferenceMapper;
import cn.jeeweb.modules.repo.entity.McRepoReference;
import cn.jeeweb.modules.repo.service.IMcRepoReferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;

/**   
 * @Title: 参考文献
 * @Description: 参考文献
 * @author shawloong
 * @date 2017-10-04 14:38:30
 * @version V1.0   
 *
 */
@Transactional
@Service("mcRepoReferenceService")
public class McRepoReferenceServiceImpl  extends CommonServiceImpl<McRepoReferenceMapper,McRepoReference> implements  IMcRepoReferenceService {
	
	@Override
	public boolean insert(McRepoReference mcRepoReference) {
		// 保存主表
		super.insert(mcRepoReference);
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(McRepoReference mcRepoReference) {
		try {
			// 更新主表
			super.insertOrUpdate(mcRepoReference);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}
	
	
	
}
