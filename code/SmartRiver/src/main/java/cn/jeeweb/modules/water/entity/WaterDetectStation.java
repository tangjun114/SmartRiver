package cn.jeeweb.modules.water.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 监测站管理
 * @Description: 监测站管理
 * @author liyonglei
 * @date 2018-11-28 19:41:24
 * @version V1.0   
 *
 */
@TableName("sr_water_detect_station")
@SuppressWarnings("serial")
public class WaterDetectStation extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**站点名称*/
    @TableField(value = "station_name")
	private String stationName;
    /**经度*/
    @TableField(value = "longitude")
	private String longitude;
    /**维度*/
    @TableField(value = "latitude")
	private String latitude;
    /**地址*/
    @TableField(value = "addr")
	private String addr;
    /**图片*/
    @TableField(value = "img")
	private String img;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  stationName
	 *@return: String  站点名称
	 */
	public String getStationName(){
		return this.stationName;
	}

	/**
	 * 设置  stationName
	 *@param: stationName  站点名称
	 */
	public void setStationName(String stationName){
		this.stationName = stationName;
	}
	/**
	 * 获取  longitude
	 *@return: String  经度
	 */
	public String getLongitude(){
		return this.longitude;
	}

	/**
	 * 设置  longitude
	 *@param: longitude  经度
	 */
	public void setLongitude(String longitude){
		this.longitude = longitude;
	}
	/**
	 * 获取  latitude
	 *@return: String  维度
	 */
	public String getLatitude(){
		return this.latitude;
	}

	/**
	 * 设置  latitude
	 *@param: latitude  维度
	 */
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
	/**
	 * 获取  addr
	 *@return: String  地址
	 */
	public String getAddr(){
		return this.addr;
	}

	/**
	 * 设置  addr
	 *@param: addr  地址
	 */
	public void setAddr(String addr){
		this.addr = addr;
	}
	/**
	 * 获取  img
	 *@return: String  图片
	 */
	public String getImg(){
		return this.img;
	}

	/**
	 * 设置  img
	 *@param: img  图片
	 */
	public void setImg(String img){
		this.img = img;
	}
	
}
