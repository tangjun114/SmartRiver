package cn.jeeweb.modules.water.controller;


import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.flood.entity.FloodDestoryReport;
import cn.jeeweb.modules.flood.entity.FloodRainfull;
import cn.jeeweb.modules.water.service.ISrWaterQualityMonitorService;
import com.ff.bi.model.RptDimDataJson;
import com.ff.common.service.model.FFExcelMeta;
import com.ff.common.service.model.FFExcelMetaAnno;
import com.ff.common.util.anno.Chart;
import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.util.meta.ReflectionUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.water.entity.SrWaterQualityMonitor;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wsh
 * @version V1.0
 * @Title: 水质监测
 * @Description: 水质监测
 * @date 2019-01-23 17:15:42
 */
@Controller
@RequestMapping("${admin.url.prefix}/water/srwaterqualitymonitor")
@RequiresPathPermission("water:srwaterqualitymonitor")
public class SrWaterQualityMonitorController extends BaseCRUDController<SrWaterQualityMonitor, String> {

    @Autowired
    ISrWaterQualityMonitorService waterQualityMonitorService;

    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        String siteName = request.getParameter("siteName");
        model.addAttribute("siteName", siteName);
    }

    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<SrWaterQualityMonitor> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
        String siteName = request.getParameter("siteName");
        if (StringUtils.isNotEmpty(siteName)) {
            queryable.addCondition("siteName", siteName);
        }
        queryable.addOrder(Sort.Direction.DESC, "recordTime");
        super.preAjaxList(queryable, entityWrapper, request, response);
    }

    @RequestMapping("/chart")
    public String sta(Model model, HttpServletRequest request, HttpServletResponse response) {
        return display("chart");
    }

    @RequestMapping("/chart/data")
    @ResponseBody
    public BaseRspJson<List<RptDimDataJson>> rainfullData(@RequestBody BaseReqJson reqJson) {

        BaseRspJson<List<RptDimDataJson>> rsp = new BaseRspJson<>();
        List<RptDimDataJson> dataList = new ArrayList<>();
        dataList = getChartData(reqJson);
        rsp.setObj(dataList);
        return rsp;
    }

    public List<RptDimDataJson> getChartData(BaseReqJson reqJson) {
        List<RptDimDataJson> obj = new ArrayList<>();
        List<SrWaterQualityMonitor> dataList = null;
        List<?> x_data = null;
        List<?> y_data = null;
        String siteName = "";
        String startTime = "";
        String endTime = "";
        List<Condition.Filter> filters = reqJson.getFilter();
        for (Condition.Filter filter : filters) {
            if ("siteName".equals(filter.getProperty())) {
                siteName = (String) filter.getValue();
            }
            if ("startTime".equals(filter.getProperty())) {
                startTime = (String) filter.getValue();
            }
            if ("endTime".equals(filter.getProperty())) {
                endTime = (String) filter.getValue();
            }
        }

        EntityWrapper<SrWaterQualityMonitor> entityWrapper1 = new EntityWrapper<>();

        entityWrapper1.like("site_name", siteName);
        entityWrapper1.gt("record_time", startTime);
        entityWrapper1.lt("record_time", endTime);

        entityWrapper1.orderBy("record_time", true);
        dataList = waterQualityMonitorService.selectList(entityWrapper1);
        x_data = DataFilterUtil.getFieldValueFromList(dataList, "recordTime");

        Field[] fields = ReflectionUtil.getAllFields(SrWaterQualityMonitor.class);
        for (Field field : fields) {
            Chart annotion = field.getAnnotation(Chart.class);
            if (null != annotion) {
                RptDimDataJson temp = new RptDimDataJson();
                temp.setX_data(x_data);
                temp.setY_data(DataFilterUtil.getFieldValueFromList(dataList, field.getName()));
                temp.setY_name(annotion.name());
                obj.add(temp);
            }
        }
        return obj;
    }

}
