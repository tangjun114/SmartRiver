package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodArea;
 
/**   
 * @Title: 水清监测区域数据库控制层接口
 * @Description: 水情监测区域数据库控制层接口
 * @author wsh
 * @date 2018-11-30 16:11:01
 * @version V1.0   
 *
 */
public interface FloodAreaMapper extends BaseMapper<FloodArea> {
    
}