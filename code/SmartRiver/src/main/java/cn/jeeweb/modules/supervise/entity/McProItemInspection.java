package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 抽查记录
 * @Description: 抽查记录
 * @author jerry
 * @date 2018-06-04 10:36:48
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_inspection")
@SuppressWarnings("serial")
public class McProItemInspection extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**开始上传文件*/
    @TableField(value = "start_file")
	private String startFile;
    /**开始上传人员*/
    @TableField(value = "upload_user")
	private String uploadUser;
    /**开始上传时间*/
    @TableField(value = "start_time")
	private String startTime;
    /**结束上传文件*/
    @TableField(value = "end_file")
	private String endFile;
    /**结束上传时间*/
    @TableField(value = "end_time")
	private String endTime;
    /**抽查上传文件*/
    @TableField(value = "inspect_file")
	private String inspectFile;
    /**抽查上传时间*/
    @TableField(value = "inspect_time")
	private String inspectTime;
    /**经度*/
    @TableField(value = "longtitude")
	private String longtitude;
    /**纬度*/
    @TableField(value = "latitude")
	private String latitude;
    /**具体地址*/
    @TableField(value = "addr")
	private String addr;
    /**抽查人员*/
    @TableField(value = "inspect_user")
	private String inspectUser;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  startFile
	 *@return: String  开始上传文件
	 */
	public String getStartFile(){
		return this.startFile;
	}

	/**
	 * 设置  startFile
	 *@param: startFile  开始上传文件
	 */
	public void setStartFile(String startFile){
		this.startFile = startFile;
	}
	/**
	 * 获取  uploadUser
	 *@return: String  开始上传人员
	 */
	public String getUploadUser(){
		return this.uploadUser;
	}

	/**
	 * 设置  uploadUser
	 *@param: uploadUser  开始上传人员
	 */
	public void setUploadUser(String uploadUser){
		this.uploadUser = uploadUser;
	}
	/**
	 * 获取  startTime
	 *@return: String  开始上传时间
	 */
	public String getStartTime(){
		return this.startTime;
	}

	/**
	 * 设置  startTime
	 *@param: startTime  开始上传时间
	 */
	public void setStartTime(String startTime){
		this.startTime = startTime;
	}
	/**
	 * 获取  endFile
	 *@return: String  结束上传文件
	 */
	public String getEndFile(){
		return this.endFile;
	}

	/**
	 * 设置  endFile
	 *@param: endFile  结束上传文件
	 */
	public void setEndFile(String endFile){
		this.endFile = endFile;
	}
	/**
	 * 获取  endTime
	 *@return: String  结束上传时间
	 */
	public String getEndTime(){
		return this.endTime;
	}

	/**
	 * 设置  endTime
	 *@param: endTime  结束上传时间
	 */
	public void setEndTime(String endTime){
		this.endTime = endTime;
	}
	/**
	 * 获取  inspectFile
	 *@return: String  抽查上传文件
	 */
	public String getInspectFile(){
		return this.inspectFile;
	}

	/**
	 * 设置  inspectFile
	 *@param: inspectFile  抽查上传文件
	 */
	public void setInspectFile(String inspectFile){
		this.inspectFile = inspectFile;
	}
	/**
	 * 获取  inspectTime
	 *@return: String  抽查上传时间
	 */
	public String getInspectTime(){
		return this.inspectTime;
	}

	/**
	 * 设置  inspectTime
	 *@param: inspectTime  抽查上传时间
	 */
	public void setInspectTime(String inspectTime){
		this.inspectTime = inspectTime;
	}
	/**
	 * 获取  longtitude
	 *@return: String  经度
	 */
	public String getLongtitude(){
		return this.longtitude;
	}

	/**
	 * 设置  longtitude
	 *@param: longtitude  经度
	 */
	public void setLongtitude(String longtitude){
		this.longtitude = longtitude;
	}
	/**
	 * 获取  latitude
	 *@return: String  纬度
	 */
	public String getLatitude(){
		return this.latitude;
	}

	/**
	 * 设置  latitude
	 *@param: latitude  纬度
	 */
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
	/**
	 * 获取  addr
	 *@return: String  具体地址
	 */
	public String getAddr(){
		return this.addr;
	}

	/**
	 * 设置  addr
	 *@param: addr  具体地址
	 */
	public void setAddr(String addr){
		this.addr = addr;
	}
	/**
	 * 获取  inspectUser
	 *@return: String  抽查人员
	 */
	public String getInspectUser(){
		return this.inspectUser;
	}

	/**
	 * 设置  inspectUser
	 *@param: inspectUser  抽查人员
	 */
	public void setInspectUser(String inspectUser){
		this.inspectUser = inspectUser;
	}
	
}
