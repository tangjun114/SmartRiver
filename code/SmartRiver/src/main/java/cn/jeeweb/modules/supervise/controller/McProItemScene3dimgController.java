package cn.jeeweb.modules.supervise.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProItemScene3dimg;

/**   
 * @Title: 项目监控-项目三维图
 * @Description: 项目监控-项目三维图
 * @author shawloong
 * @date 2017-11-02 21:39:34
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemscene3dimg")
@RequiresPathPermission("supervise:mcmonitorresultstatistics")
public class McProItemScene3dimgController extends McProjectBaseController<McProItemScene3dimg, String> {
	@RequestMapping("/datalist")
	public String showDataList(Model model, HttpServletRequest request,
			HttpServletResponse response) {

		return display("datalist");
	}
}
