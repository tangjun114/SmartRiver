package cn.jeeweb.modules.sm.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.sm.entity.McSmMonitorGroupType;

/**   
 * @Title: 监测组类别
 * @Description: 监测组类别
 * @author jerry
 * @date 2018-03-14 15:54:31
 * @version V1.0   
 *
 */
public interface IMcSmMonitorGroupTypeService extends ICommonService<McSmMonitorGroupType> {

}

