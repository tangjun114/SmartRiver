package cn.jeeweb.modules.sm.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.jeeweb.core.common.entity.TreeEntity;
import cn.jeeweb.modules.sys.entity.User;


/**   
 * @Title: 系统管理-参数设置
 * @Description: 系统管理-参数设置
 * @author shawloong
 * @date 2017-10-11 23:30:03
 * @version V1.0   
 *
 */
@TableName("mc_sm_param_config")
@SuppressWarnings("serial")
public class McSmParamConfig extends TreeEntity<McSmParamConfig> {
	
    /**字段主键*/
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**父节点*/
    /**父节点路径*/
    /**名称*/
    /**参数值*/
    @TableField(value = "para_value")
	private String paraValue;
    /**映射标签*/
    @TableField(value = "field_name")
	private String fieldName;
    /**映射字段*/
    @TableField(value = "field_mapper")
	private String fieldMapper;
    /**系统初始化数据*/
    @TableField(value = "sys_init")
	private String sysInit;
    /**排序号*/
    @TableField(value = "index")
	private String index;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
	
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  paraValue
	 *@return: String  参数值
	 */
	public String getParaValue(){
		return this.paraValue;
	}

	/**
	 * 设置  paraValue
	 *@param: paraValue  参数值
	 */
	public void setParaValue(String paraValue){
		this.paraValue = paraValue;
	}
	/**
	 * 获取  fieldName
	 *@return: String  映射标签
	 */
	public String getFieldName(){
		return this.fieldName;
	}

	/**
	 * 设置  fieldName
	 *@param: fieldName  映射标签
	 */
	public void setFieldName(String fieldName){
		this.fieldName = fieldName;
	}
	/**
	 * 获取  fieldMapper
	 *@return: String  映射字段
	 */
	public String getFieldMapper(){
		return this.fieldMapper;
	}

	/**
	 * 设置  fieldMapper
	 *@param: fieldMapper  映射字段
	 */
	public void setFieldMapper(String fieldMapper){
		this.fieldMapper = fieldMapper;
	}
	/**
	 * 获取  sysInit
	 *@return: String  系统初始化数据
	 */
	public String getSysInit(){
		return this.sysInit;
	}

	/**
	 * 设置  sysInit
	 *@param: sysInit  系统初始化数据
	 */
	public void setSysInit(String sysInit){
		this.sysInit = sysInit;
	}
	/**
	 * 获取  index
	 *@return: String  排序号
	 */
	public String getIndex(){
		return this.index;
	}

	/**
	 * 设置  index
	 *@param: index  排序号
	 */
	public void setIndex(String index){
		this.index = index;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}

}