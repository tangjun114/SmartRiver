package cn.jeeweb.modules.sm.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.sm.entity.McSmMonitorSubarea;

/**   
 * @Title: 系统管理-监测分区
 * @Description: 系统管理-监测分区
 * @author shawloong
 * @date 2017-11-05 18:34:12
 * @version V1.0   
 *
 */
public interface IMcSmMonitorSubareaService extends ICommonService<McSmMonitorSubarea> {

}

