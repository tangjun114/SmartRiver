package cn.jeeweb.modules.flood.client.entity;

import com.google.common.annotations.VisibleForTesting;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author:Wish
 * @Date: Created in 2018/12/14 9:46
 **/
public class ReqParam implements Serializable {
    private static final long serialVersionUID = 1L;
    private Map<String, Object> params;
    private String token;
    private String interfaceName;

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }
}
