package cn.jeeweb.modules.constant;

public class DataValidConst {

	public static String handing = "未处理";
	public static String valid = "有效";
	public static String invalid = "被毁无效";

	public static String retest = "重布观测";
	
	public static String stop_test = "监测受阻";
	public static String no_test = "未观测";
	public static String no_set = "未布点";
	public static String discard = "测点被毁";
	public static String reset = "回填";
}
