package cn.jeeweb.modules.sm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.sm.entity.McSmMonitorGroupType;
 
/**   
 * @Title: 监测组类别数据库控制层接口
 * @Description: 监测组类别数据库控制层接口
 * @author jerry
 * @date 2018-03-14 15:54:31
 * @version V1.0   
 *
 */
public interface McSmMonitorGroupTypeMapper extends BaseMapper<McSmMonitorGroupType> {
    
}