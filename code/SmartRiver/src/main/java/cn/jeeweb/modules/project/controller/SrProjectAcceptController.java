package cn.jeeweb.modules.project.controller;


import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.project.entity.SrProjectCheck;
import cn.jeeweb.modules.project.entity.SrProjectInfo;
import cn.jeeweb.modules.project.service.ISrProjectInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectAccept;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wsh
 * @version V1.0
 * @Title: 项目验收
 * @Description: 项目验收
 * @date 2019-03-31 21:56:15
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectaccept")
@RequiresPathPermission("project:srprojectaccept")
public class SrProjectAcceptController extends BaseCRUDController<SrProjectAccept, String> {
    @Autowired
    ISrProjectInfoService srProjectInfoService;

    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        super.preList(model, request, response);
        String proId = request.getParameter("proId");
        if (StringUtils.isNotEmpty(proId)) {
            model.addAttribute("proId", proId);
        }
    }


    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<SrProjectAccept> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
        super.preAjaxList(queryable, entityWrapper, request, response);
        String proId = request.getParameter("proId");
        if (StringUtils.isNotEmpty(proId)) {
            queryable.addCondition("proId", proId);
        }
        entityWrapper.orderBy("create_date", false);
    }

    @Override
    public void afterSave(SrProjectAccept entity, HttpServletRequest request, HttpServletResponse response) {
        super.afterSave(entity, request, response);
        if (entity.getIsAccept() == 1) {
            SrProjectInfo info = new SrProjectInfo();
            info.setId(entity.getProId());
            info.setStatus("wangong");
            srProjectInfoService.updateById(info);
        }
    }
}
