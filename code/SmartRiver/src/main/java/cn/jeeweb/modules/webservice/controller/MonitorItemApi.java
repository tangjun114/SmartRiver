package cn.jeeweb.modules.webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.web.controller.FFTableController;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;

@Controller
@RequestMapping("${api.url.prefix}/monitoritem")
public class MonitorItemApi extends FFTableController<McProItemMonitorItem>
{

	@Override
	public BaseRspJson<McProItemMonitorItem> update(@RequestBody BaseReqJson<McProItemMonitorItem> request) {


		McProItemMonitorItem obj = this.getObj(request,this.GetTableClass());
		
		McProItemMonitorItem old = this.baseService.selectById(obj.getId());
		
		if(null != obj.getMonitorCount())
		{
			old.setMonitorCount(obj.getMonitorCount());
		}
		if(null != obj.getAutoPara())
		{
			old.setAutoPara(obj.getAutoPara());
		}		
		if(null != obj.getAutoStartTime())
		{
			old.setAutoStartTime(obj.getAutoStartTime());
		}		
		if(null != obj.getAutoSwitch())
		{
			old.setAutoSwitch(obj.getAutoSwitch());
		}		
		if(null != obj.getAutoSendMessage())
		{
			old.setAutoSendMessage(obj.getAutoSendMessage());
		}
		if(null != obj.getDeviceTypeName())
		{
			old.setDeviceTypeName(obj.getDeviceTypeName());
		}
		if(null != obj.getDeviceModelName())
		{
			old.setDeviceModelName(obj.getDeviceModelName());
		}
		BaseRspJson<McProItemMonitorItem> rsp = new BaseRspJson<McProItemMonitorItem>();
		
		getService().updateById(old);
		
		rsp.setObj(old);
		
		return rsp;
	}
 
}
