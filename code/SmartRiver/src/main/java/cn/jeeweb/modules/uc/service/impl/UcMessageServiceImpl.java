package cn.jeeweb.modules.uc.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.uc.mapper.UcMessageMapper;
import cn.jeeweb.modules.uc.entity.UcMessage;
import cn.jeeweb.modules.uc.service.IUcMessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 个人中心消息
 * @Description: 个人中心消息
 * @author shawloong
 * @date 2017-09-30 00:04:45
 * @version V1.0   
 *
 */
@Transactional
@Service("ucMessageService")
public class UcMessageServiceImpl  extends CommonServiceImpl<UcMessageMapper,UcMessage> implements  IUcMessageService {

}
