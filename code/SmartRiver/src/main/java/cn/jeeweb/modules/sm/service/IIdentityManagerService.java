package cn.jeeweb.modules.sm.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.sm.entity.IdentityManager;

/**   
 * @Title: 身份管理
 * @Description: 身份管理
 * @author shawloong
 * @date 2017-09-29 00:44:22
 * @version V1.0   
 *
 */
public interface IIdentityManagerService extends ICommonService<IdentityManager> {

}

