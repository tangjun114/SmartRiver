package cn.jeeweb.modules.water.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.water.entity.WaterDetectStation;

/**   
 * @Title: 监测站管理
 * @Description: 监测站管理
 * @author liyonglei
 * @date 2018-11-28 19:41:24
 * @version V1.0   
 *
 */
public interface IWaterDetectStationService extends ICommonService<WaterDetectStation> {

}

