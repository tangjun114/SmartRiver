package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McMonitorResultStatistics;

/**   
 * @Title: 监测结果统计
 * @Description: 监测结果统计
 * @author aether
 * @date 2018-01-24 21:32:05
 * @version V1.0   
 *
 */
public interface IMcMonitorResultStatisticsService extends ICommonService<McMonitorResultStatistics> {

}

