package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McProItemAlarm;
 
/**   
 * @Title: 告警信息数据库控制层接口
 * @Description: 告警信息数据库控制层接口
 * @author Aether
 * @date 2017-12-02 12:58:21
 * @version V1.0   
 *
 */
public interface McProItemAlarmMapper extends BaseMapper<McProItemAlarm> {
    
}