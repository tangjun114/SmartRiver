package cn.jeeweb.modules.water.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;

/**   
 * @Title: 排污口检测报表
 * @Description: 排污口检测报表
 * @author wsh
 * @date 2019-03-19 19:09:33
 * @version V1.0   
 *
 */
@TableName("sr_water_drain_outlet_report")
@SuppressWarnings("serial")
public class WaterDrainOutletReport extends AbstractEntity<String> {

    /**主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**排污口名称*/
    @TableField(value = "outlet_name")
	private String outletName;
    /**监控类型*/
    @TableField(value = "monitor_type")
	private String monitorType;
    /**水质*/
    @TableField(value = "water_quality")
	private String waterQuality;
    /**count*/
    @TableField(value = "count")
	private Integer count;
	
	/**
	 * 获取  id
	 *@return: String  主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  outletName
	 *@return: String  排污口名称
	 */
	public String getOutletName(){
		return this.outletName;
	}

	/**
	 * 设置  outletName
	 *@param: outletName  排污口名称
	 */
	public void setOutletName(String outletName){
		this.outletName = outletName;
	}
	/**
	 * 获取  monitorType
	 *@return: String  监控类型
	 */
	public String getMonitorType(){
		return this.monitorType;
	}

	/**
	 * 设置  monitorType
	 *@param: monitorType  监控类型
	 */
	public void setMonitorType(String monitorType){
		this.monitorType = monitorType;
	}
	/**
	 * 获取  waterQuality
	 *@return: String  水质
	 */
	public String getWaterQuality(){
		return this.waterQuality;
	}

	/**
	 * 设置  waterQuality
	 *@param: waterQuality  水质
	 */
	public void setWaterQuality(String waterQuality){
		this.waterQuality = waterQuality;
	}
	/**
	 * 获取  count
	 *@return: Integer  count
	 */
	public Integer getCount(){
		return this.count;
	}

	/**
	 * 设置  count
	 *@param: count  count
	 */
	public void setCount(Integer count){
		this.count = count;
	}
	
}
