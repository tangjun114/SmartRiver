package cn.jeeweb.modules.water.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.water.entity.WaterDrainOutlet;
 
/**   
 * @Title: 排污口管理数据库控制层接口
 * @Description: 排污口管理数据库控制层接口
 * @author liyonglei
 * @date 2018-11-14 19:33:01
 * @version V1.0   
 *
 */
public interface WaterDrainOutletMapper extends BaseMapper<WaterDrainOutlet> {
    
}