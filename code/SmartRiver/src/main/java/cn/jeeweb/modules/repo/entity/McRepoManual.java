package cn.jeeweb.modules.repo.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 平台手册 manual
 * @Description: 平台手册 manual
 * @author shawloong
 * @date 2018-03-08 10:06:17
 * @version V1.0   
 *
 */
@TableName("mc_repo_manual")
@SuppressWarnings("serial")
public class McRepoManual extends AbstractEntity<String> {

    /**id*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**文件名*/
    @TableField(value = "name")
	private String name;
    /**上传时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**文件*/
    @TableField(value = "path")
	private String path;
    /**组织id*/
    @TableField(value = "monitor_id")
	private String monitorId;
	
	/**
	 * 获取  id
	 *@return: String  id
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  name
	 *@return: String  文件名
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  文件名
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  createDate
	 *@return: Date  上传时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  上传时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  path
	 *@return: String  文件
	 */
	public String getPath(){
		return this.path;
	}

	/**
	 * 设置  path
	 *@param: path  文件
	 */
	public void setPath(String path){
		this.path = path;
	}
	/**
	 * 获取  monitorId
	 *@return: String  组织id
	 */
	public String getMonitorId(){
		return this.monitorId;
	}

	/**
	 * 设置  monitorId
	 *@param: monitorId  组织id
	 */
	public void setMonitorId(String monitorId){
		this.monitorId = monitorId;
	}
	
}