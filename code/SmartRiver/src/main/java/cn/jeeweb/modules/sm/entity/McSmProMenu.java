package cn.jeeweb.modules.sm.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import cn.jeeweb.core.common.entity.TreeEntity;
import cn.jeeweb.modules.sys.entity.User;


/**   
 * @Title: 项目管理-监测菜单
 * @Description: 项目管理-监测菜单
 * @author shawloong
 * @date 2017-10-30 00:26:12
 * @version V1.0   
 *
 */
@TableName("mc_sm_pro_menu")
@SuppressWarnings("serial")
public class McSmProMenu extends TreeEntity<McSmProMenu> {
	
    /**字段主键*/
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**父节点*/
    /**父节点路径*/
    /**名称*/
    /**需要URL*/
    @TableField(value = "need_url")
	private String needUrl;
    /**链接*/
    @TableField(value = "url")
	private String url;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**序号*/
    @TableField(value = "index")
	private String index;
    /**备用字段1*/
    @TableField(value = "value1")
	private String value1;
    /**备用字段2*/
    @TableField(value = "value2")
	private String value2;
    /**备用字段3*/
    @TableField(value = "value3")
	private String value3;
    /**备用字段4*/
    @TableField(value = "value4")
	private String value4;
    /**备用字段5*/
    @TableField(value = "value5")
	private String value5;
	
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  needUrl
	 *@return: String  需要URL
	 */
	public String getNeedUrl(){
		return this.needUrl;
	}

	/**
	 * 设置  needUrl
	 *@param: needUrl  需要URL
	 */
	public void setNeedUrl(String needUrl){
		this.needUrl = needUrl;
	}
	/**
	 * 获取  url
	 *@return: String  链接
	 */
	public String getUrl(){
		return this.url;
	}

	/**
	 * 设置  url
	 *@param: url  链接
	 */
	public void setUrl(String url){
		this.url = url;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  index
	 *@return: String  序号
	 */
	public String getIndex(){
		return this.index;
	}

	/**
	 * 设置  index
	 *@param: index  序号
	 */
	public void setIndex(String index){
		this.index = index;
	}
	/**
	 * 获取  value1
	 *@return: String  备用字段1
	 */
	public String getValue1(){
		return this.value1;
	}

	/**
	 * 设置  value1
	 *@param: value1  备用字段1
	 */
	public void setValue1(String value1){
		this.value1 = value1;
	}
	/**
	 * 获取  value2
	 *@return: String  备用字段2
	 */
	public String getValue2(){
		return this.value2;
	}

	/**
	 * 设置  value2
	 *@param: value2  备用字段2
	 */
	public void setValue2(String value2){
		this.value2 = value2;
	}
	/**
	 * 获取  value3
	 *@return: String  备用字段3
	 */
	public String getValue3(){
		return this.value3;
	}

	/**
	 * 设置  value3
	 *@param: value3  备用字段3
	 */
	public void setValue3(String value3){
		this.value3 = value3;
	}
	/**
	 * 获取  value4
	 *@return: String  备用字段4
	 */
	public String getValue4(){
		return this.value4;
	}

	/**
	 * 设置  value4
	 *@param: value4  备用字段4
	 */
	public void setValue4(String value4){
		this.value4 = value4;
	}
	/**
	 * 获取  value5
	 *@return: String  备用字段5
	 */
	public String getValue5(){
		return this.value5;
	}

	/**
	 * 设置  value5
	 *@param: value5  备用字段5
	 */
	public void setValue5(String value5){
		this.value5 = value5;
	}

}