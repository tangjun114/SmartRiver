package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.flood.mapper.FloodSiteWarnMapper;
import cn.jeeweb.modules.flood.entity.FloodSiteWarn;
import cn.jeeweb.modules.flood.service.IFloodSiteWarnService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 站点警报阈值
 * @Description: 站点警报阈值
 * @author wsh
 * @date 2018-12-02 23:09:31
 * @version V1.0   
 *
 */
@Transactional
@Service("floodSiteWarnService")
public class FloodSiteWarnServiceImpl  extends CommonServiceImpl<FloodSiteWarnMapper,FloodSiteWarn> implements  IFloodSiteWarnService {

}
