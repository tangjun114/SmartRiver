package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 设备异常状况
 * @Description: 设备异常状况
 * @author Aether
 * @date 2018-04-19 21:46:24
 * @version V1.0   
 *
 */
@TableName("mc_pro_device_abnormal")
@SuppressWarnings("serial")
public class McProDeviceAbnormal extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**监测项id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**设备编码*/
    @TableField(value = "device_code")
	private String deviceCode;
    /**设备名称*/
    @TableField(value = "device_name")
	private String deviceName;
    /**异常信息*/
    @TableField(value = "abnormal_info")
	private String abnormalInfo;
    /**报警时间*/
    @TableField(value = "alarm_time")
	private Date alarmTime;
    /**状态*/
    @TableField(value = "status")
	private String status = "to_handle";
    /**处理人id*/
    @TableField(value = "handler_id")
	private String handlerId;
    /**处理人名称*/
    @TableField(value = "handler_name")
	private String handlerName;
    /**处理时间*/
    @TableField(value = "handle_time")
	private Date handleTime;
    /**处理措施*/
    @TableField(value = "handle_result")
	private String handleResult;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  deviceCode
	 *@return: String  设备编码
	 */
	public String getDeviceCode(){
		return this.deviceCode;
	}

	/**
	 * 设置  deviceCode
	 *@param: deviceCode  设备编码
	 */
	public void setDeviceCode(String deviceCode){
		this.deviceCode = deviceCode;
	}
	/**
	 * 获取  deviceName
	 *@return: String  设备名称
	 */
	public String getDeviceName(){
		return this.deviceName;
	}

	/**
	 * 设置  deviceName
	 *@param: deviceName  设备名称
	 */
	public void setDeviceName(String deviceName){
		this.deviceName = deviceName;
	}
	/**
	 * 获取  abnormalInfo
	 *@return: String  异常信息
	 */
	public String getAbnormalInfo(){
		return this.abnormalInfo;
	}

	/**
	 * 设置  abnormalInfo
	 *@param: abnormalInfo  异常信息
	 */
	public void setAbnormalInfo(String abnormalInfo){
		this.abnormalInfo = abnormalInfo;
	}
	/**
	 * 获取  alarmTime
	 *@return: Date  报警时间
	 */
	public Date getAlarmTime(){
		return this.alarmTime;
	}

	/**
	 * 设置  alarmTime
	 *@param: alarmTime  报警时间
	 */
	public void setAlarmTime(Date alarmTime){
		this.alarmTime = alarmTime;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  handlerId
	 *@return: String  处理人id
	 */
	public String getHandlerId(){
		return this.handlerId;
	}

	/**
	 * 设置  handlerId
	 *@param: handlerId  处理人id
	 */
	public void setHandlerId(String handlerId){
		this.handlerId = handlerId;
	}
	/**
	 * 获取  handlerName
	 *@return: String  处理人名称
	 */
	public String getHandlerName(){
		return this.handlerName;
	}

	/**
	 * 设置  handlerName
	 *@param: handlerName  处理人名称
	 */
	public void setHandlerName(String handlerName){
		this.handlerName = handlerName;
	}
	/**
	 * 获取  handleTime
	 *@return: Date  处理时间
	 */
	public Date getHandleTime(){
		return this.handleTime;
	}

	/**
	 * 设置  handleTime
	 *@param: handleTime  处理时间
	 */
	public void setHandleTime(Date handleTime){
		this.handleTime = handleTime;
	}
	/**
	 * 获取  handleResult
	 *@return: String  处理措施
	 */
	public String getHandleResult(){
		return this.handleResult;
	}

	/**
	 * 设置  handleResult
	 *@param: handleResult  处理措施
	 */
	public void setHandleResult(String handleResult){
		this.handleResult = handleResult;
	}
	
}
