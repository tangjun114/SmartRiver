package cn.jeeweb.modules.project.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;

import java.util.Date;

/**
 * @author wsh
 * @version V1.0
 * @Title: 工程水务管理
 * @Description: 工程水务管理
 * @date 2018-12-25 19:33:17
 */
@TableName("sr_project_flood_manager")
@SuppressWarnings("serial")
public class SrProjectFloodManager extends AbstractEntity<String> {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    /**
     * 创建者
     */
    @TableField(value = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField(value = "update_date")
    private Date updateDate;
    /**
     * 删除标记（0：正常；1：删除）
     */
    @TableField(value = "del_flag")
    private String delFlag;
    /**
     * 备注信息
     */
    @TableField(value = "remarks")
    private String remarks;
    /**
     * 项目id
     */
    @TableField(value = "pro_id")
    private String proId;
    /**
     * 项目名称
     */
    @TableField(value = "pro_name")
    private String proName;
    /**
     * 供水工程类别
     */
    @TableField(value = "type")
    private Integer type;
    /**
     * 运行状态
     */
    @TableField(value = "work_type")
    private Integer workType;
    @TableField(value = "name")
    private String name;

    /**
     * 获取  id
     *
     * @return: String  主键
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置  id
     *
     * @param: id  主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取  createBy
     *
     * @return: String  创建者
     */
    public String getCreateBy() {
        return this.createBy;
    }

    /**
     * 设置  createBy
     *
     * @param: createBy  创建者
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取  createDate
     *
     * @return: Date  创建时间
     */
    public Date getCreateDate() {
        return this.createDate;
    }

    /**
     * 设置  createDate
     *
     * @param: createDate  创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取  updateBy
     *
     * @return: String  更新者
     */
    public String getUpdateBy() {
        return this.updateBy;
    }

    /**
     * 设置  updateBy
     *
     * @param: updateBy  更新者
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取  updateDate
     *
     * @return: Date  更新时间
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 设置  updateDate
     *
     * @param: updateDate  更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取  delFlag
     *
     * @return: String  删除标记（0：正常；1：删除）
     */
    public String getDelFlag() {
        return this.delFlag;
    }

    /**
     * 设置  delFlag
     *
     * @param: delFlag  删除标记（0：正常；1：删除）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 获取  remarks
     *
     * @return: String  备注信息
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置  remarks
     *
     * @param: remarks  备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取  proId
     *
     * @return: String  项目id
     */
    public String getProId() {
        return this.proId;
    }

    /**
     * 设置  proId
     *
     * @param: proId  项目id
     */
    public void setProId(String proId) {
        this.proId = proId;
    }

    /**
     * 获取  proName
     *
     * @return: String  项目名称
     */
    public String getProName() {
        return this.proName;
    }

    /**
     * 设置  proName
     *
     * @param: proName  项目名称
     */
    public void setProName(String proName) {
        this.proName = proName;
    }

    /**
     * 获取  type
     *
     * @return: Integer  供水工程类别
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 设置  type
     *
     * @param: type  供水工程类别
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取  workType
     *
     * @return: Integer  运行状态
     */
    public Integer getWorkType() {
        return this.workType;
    }

    /**
     * 设置  workType
     *
     * @param: workType  运行状态
     */
    public void setWorkType(Integer workType) {
        this.workType = workType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
