package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolDetail;
 
/**   
 * @Title: 巡检详情数据库控制层接口
 * @Description: 巡检详情数据库控制层接口
 * @author Aether
 * @date 2018-02-03 11:28:27
 * @version V1.0   
 *
 */
public interface McMonitorPatrolDetailMapper extends BaseMapper<McMonitorPatrolDetail> {
    
}