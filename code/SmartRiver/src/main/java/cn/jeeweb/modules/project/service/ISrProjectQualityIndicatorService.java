package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectQualityIndicator;

/**   
 * @Title: 项目质量指标
 * @Description: 项目质量指标
 * @author jerry
 * @date 2018-11-14 17:57:30
 * @version V1.0   
 *
 */
public interface ISrProjectQualityIndicatorService extends ICommonService<SrProjectQualityIndicator> {

}

