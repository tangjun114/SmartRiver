package cn.jeeweb.modules.monitor.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.monitor.mapper.McMonitorOrgMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorOrg;
import cn.jeeweb.modules.monitor.service.IMcMonitorOrgService;
import cn.jeeweb.modules.monitor.entity.McMonitorCertificate;
import cn.jeeweb.modules.monitor.service.IMcMonitorCertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;

/**   
 * @Title: 监督检测机构
 * @Description: 监督检测机构
 * @author shawloong
 * @date 2017-10-05 22:28:45
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorOrgService")
public class McMonitorOrgServiceImpl  extends CommonServiceImpl<McMonitorOrgMapper,McMonitorOrg> implements  IMcMonitorOrgService {
	@Autowired
	private IMcMonitorCertificateService mcMonitorCertificateService;
	
	@Override
	public boolean insert(McMonitorOrg mcMonitorOrg) {
		// 保存主表
		super.insert(mcMonitorOrg);
		// 保存机构证书管理
		String mcMonitorCertificateListJson = StringEscapeUtils
				.unescapeHtml4(ServletUtils.getRequest().getParameter("mcMonitorCertificateListJson"));
		List<McMonitorCertificate> mcMonitorCertificateList = JSONObject.parseArray(mcMonitorCertificateListJson, McMonitorCertificate.class);
		if(null!=mcMonitorCertificateList&&mcMonitorCertificateList.size()>0){
		for (McMonitorCertificate mcMonitorCertificate : mcMonitorCertificateList) {
			// 保存字段列表
			mcMonitorCertificate.setMonitor(mcMonitorOrg);
			mcMonitorCertificateService.insert(mcMonitorCertificate);
		}
		}
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(McMonitorOrg mcMonitorOrg) {
		try {
			// 获得以前的数据
			List<McMonitorCertificate> oldMcMonitorCertificateList = null;
			//附表代码已经修改 勿动
			//oldMcMonitorCertificateList = mcMonitorCertificateService.selectList(new EntityWrapper<McMonitorCertificate>(McMonitorCertificate.class).eq("monitor.id",mcMonitorOrg.getId()));
			// 字段
			String mcMonitorCertificateListJson = null;
			//附表代码已经修改 勿动
			//mcMonitorCertificateListJson = StringEscapeUtils.unescapeHtml4(ServletUtils.getRequest().getParameter("mcMonitorCertificateListJson"));
					
			List<McMonitorCertificate> mcMonitorCertificateList = JSONObject.parseArray(mcMonitorCertificateListJson,
					McMonitorCertificate.class);
			// 更新主表
			super.insertOrUpdate(mcMonitorOrg);
			mcMonitorCertificateList = JSONObject.parseArray(mcMonitorCertificateListJson, McMonitorCertificate.class);
			List<String> newsMcMonitorCertificateIdList = new ArrayList<String>();
			// 保存或更新数据
			if(null!=mcMonitorCertificateList){
			for (McMonitorCertificate mcMonitorCertificate : mcMonitorCertificateList) {
				// 设置不变更的字段
				if (StringUtils.isEmpty(mcMonitorCertificate.getId())) {
					// 保存字段列表
					mcMonitorCertificate.setMonitor(mcMonitorOrg);
					mcMonitorCertificateService.insert(mcMonitorCertificate);
				} else {
					mcMonitorCertificateService.insertOrUpdate(mcMonitorCertificate);
				}
				newsMcMonitorCertificateIdList.add(mcMonitorCertificate.getId());
			}
			}

			// 删除老数据
			if(null!=oldMcMonitorCertificateList){
			for (McMonitorCertificate mcMonitorCertificate : oldMcMonitorCertificateList) {
				String mcMonitorCertificateId = mcMonitorCertificate.getId();
				if (!newsMcMonitorCertificateIdList.contains(mcMonitorCertificateId)) {
					mcMonitorCertificateService.deleteById(mcMonitorCertificateId);
				}
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}
	
	
	
}
