package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.flood.mapper.FloodWarnLogMapper;
import cn.jeeweb.modules.flood.entity.FloodWarnLog;
import cn.jeeweb.modules.flood.service.IFloodWarnLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 报警记录
 * @Description: 报警记录
 * @author wsh
 * @date 2018-12-05 19:47:58
 * @version V1.0   
 *
 */
@Transactional
@Service("floodWarnLogService")
public class FloodWarnLogServiceImpl  extends CommonServiceImpl<FloodWarnLogMapper,FloodWarnLog> implements  IFloodWarnLogService {

}
