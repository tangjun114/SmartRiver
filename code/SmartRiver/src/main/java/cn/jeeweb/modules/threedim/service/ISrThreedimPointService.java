package cn.jeeweb.modules.threedim.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.threedim.entity.SrThreedimPoint;

/**   
 * @Title: 实时影像地点
 * @Description: 实时影像地点
 * @author jerry
 * @date 2018-11-27 18:29:20
 * @version V1.0   
 *
 */
public interface ISrThreedimPointService extends ICommonService<SrThreedimPoint> {

}

