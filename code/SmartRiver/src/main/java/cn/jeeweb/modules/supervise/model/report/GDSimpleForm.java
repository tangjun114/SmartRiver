/**
 * 
 */
package cn.jeeweb.modules.supervise.model.report;

import java.util.Date;

/**
 * @author pc
 *
 */
public class GDSimpleForm extends BaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date startDate;
	private Date endDate;
	private String reportID;
	private String manageID;
	private String waterMark;
	private String showPatrol;
	private String showMonitorItem;
	private int conditionCount;
	private String monitorResultAnalysis;
	private String newAlarmHandle;
	private String historyAlarmHandle;
	private String suggest;
	private String sheetType;
	private String newVersion;

	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getReportID() {
		return reportID;
	}
	public void setReportID(String reportID) {
		this.reportID = reportID;
	}
	public String getManageID() {
		return manageID;
	}
	public void setManageID(String manageID) {
		this.manageID = manageID;
	}
	public String getWaterMark() {
		return waterMark;
	}
	public void setWaterMark(String waterMark) {
		this.waterMark = waterMark;
	}
	public String getShowPatrol() {
		return showPatrol;
	}
	public void setShowPatrol(String showPatrol) {
		this.showPatrol = showPatrol;
	}
	public String getShowMonitorItem() {
		return showMonitorItem;
	}
	public void setShowMonitorItem(String showMonitorItem) {
		this.showMonitorItem = showMonitorItem;
	}
	public int getConditionCount() {
		return conditionCount;
	}
	public void setConditionCount(int conditionCount) {
		this.conditionCount = conditionCount;
	}
	public String getMonitorResultAnalysis() {
		return monitorResultAnalysis;
	}
	public void setMonitorResultAnalysis(String monitorResultAnalysis) {
		this.monitorResultAnalysis = monitorResultAnalysis;
	}
	public String getNewAlarmHandle() {
		return newAlarmHandle;
	}
	public void setNewAlarmHandle(String newAlarmHandle) {
		this.newAlarmHandle = newAlarmHandle;
	}
	public String getHistoryAlarmHandle() {
		return historyAlarmHandle;
	}
	public void setHistoryAlarmHandle(String historyAlarmHandle) {
		this.historyAlarmHandle = historyAlarmHandle;
	}
	public String getSuggest() {
		return suggest;
	}
	public void setSuggest(String suggest) {
		this.suggest = suggest;
	}
	public String getSheetType() {
		return sheetType;
	}
	public void setSheetType(String sheetType) {
		this.sheetType = sheetType;
	}
	public String getNewVersion() {
		return newVersion;
	}
	public void setNewVersion(String newVersion) {
		this.newVersion = newVersion;
	}

	

	
}
