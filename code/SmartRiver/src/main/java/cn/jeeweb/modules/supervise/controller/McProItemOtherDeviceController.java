package cn.jeeweb.modules.supervise.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProItemOtherDevice;

/**   
 * @Title: 项目中的其他设备
 * @Description: 项目中的其他设备
 * @author jerry
 * @date 2018-01-28 18:52:59
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemotherdevice")
@RequiresPathPermission("supervise:mcproitemotherdevice")
public class McProItemOtherDeviceController extends McProjectBaseController<McProItemOtherDevice, String> {

}
