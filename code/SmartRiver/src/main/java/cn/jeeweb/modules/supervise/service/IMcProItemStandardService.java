package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemStandard;

/**   
 * @Title: 项目规范
 * @Description: 项目规范
 * @author jerry
 * @date 2017-12-25 15:25:05
 * @version V1.0   
 *
 */
public interface IMcProItemStandardService extends ICommonService<McProItemStandard> {

}

