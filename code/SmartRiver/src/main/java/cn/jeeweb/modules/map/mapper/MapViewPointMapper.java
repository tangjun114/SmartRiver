package cn.jeeweb.modules.map.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.map.entity.MapViewPoint;
 
/**   
 * @Title: 观察点管理数据库控制层接口
 * @Description: 观察点管理数据库控制层接口
 * @author liyonglei
 * @date 2018-11-14 20:50:41
 * @version V1.0   
 *
 */
public interface MapViewPointMapper extends BaseMapper<MapViewPoint> {
    
}