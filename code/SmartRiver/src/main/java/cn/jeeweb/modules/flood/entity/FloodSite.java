package cn.jeeweb.modules.flood.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author wsh
 * @version V1.0
 * @Title: 监控站点
 * @Description: 监控站点
 * @date 2018-11-27 18:04:01
 */
@TableName("sr_flood_site")
@SuppressWarnings("serial")
public class FloodSite extends AbstractEntity<String> {

    /**
     * 字段主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    /**
     * 创建者
     */
    @TableField(value = "create_by", el = "createBy.id")
    private User createBy;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField(value = "update_by", el = "updateBy.id")
    private User updateBy;
    /**
     * 更新时间
     */
    @TableField(value = "update_date")
    private Date updateDate;
    /**
     * 删除标记（0：正常；1：删除）
     */
    @TableField(value = "del_flag")
    private String delFlag;
    /**
     * 备注信息
     */
    @TableField(value = "remarks")
    private String remarks;
    /**
     * 站点名
     */
    @TableField(value = "site_name")
    private String siteName;
    /**
     * 坐标
     */
    @TableField(value = "latitude")
    private String latitude;
    /**
     * 纵坐标
     */
    @TableField(value = "longitude")
    private String longitude;
    /**
     * 站点类型(主干,支流)
     */
    @TableField(value = "type")
    private Integer type;
    /**
     * 站点类型(水文,水位)
     */
    @TableField(value = "kind")
    private Integer kind;
    /**
     * 区域
     */
    @TableField(value = "area_id")
    private String areaId;
    /**
     * 区域名
     */
    @TableField(value = "area_name")
    private String areaName;
    /**
     * 区域名
     */
    @TableField(value = "rainfall")
    private Double rainfall;
    /**
     * 区域名
     */
    @TableField(value = "water_level")
    private Double waterLevel;

    @TableField(value = "scid")
    private String scid;

    /**
     * 获取  id
     *
     * @return: String  字段主键
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置  id
     *
     * @param: id  字段主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取  createBy
     *
     * @return: User  创建者
     */
    public User getCreateBy() {
        return this.createBy;
    }

    /**
     * 设置  createBy
     *
     * @param: createBy  创建者
     */
    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取  createDate
     *
     * @return: Date  创建时间
     */
    public Date getCreateDate() {
        return this.createDate;
    }

    /**
     * 设置  createDate
     *
     * @param: createDate  创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取  updateBy
     *
     * @return: User  更新者
     */
    public User getUpdateBy() {
        return this.updateBy;
    }

    /**
     * 设置  updateBy
     *
     * @param: updateBy  更新者
     */
    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取  updateDate
     *
     * @return: Date  更新时间
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 设置  updateDate
     *
     * @param: updateDate  更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取  delFlag
     *
     * @return: String  删除标记（0：正常；1：删除）
     */
    public String getDelFlag() {
        return this.delFlag;
    }

    /**
     * 设置  delFlag
     *
     * @param: delFlag  删除标记（0：正常；1：删除）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 获取  remarks
     *
     * @return: String  备注信息
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置  remarks
     *
     * @param: remarks  备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取  siteName
     *
     * @return: String  站点名
     */
    public String getSiteName() {
        return this.siteName;
    }

    /**
     * 设置  siteName
     *
     * @param: siteName  站点名
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * 获取  type
     *
     * @return: Integer  站点类型(主干,支流)
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 设置  type
     *
     * @param: type  站点类型(主干,支流)
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取  kind
     *
     * @return: Integer  站点类型(水文,水位)
     */
    public Integer getKind() {
        return this.kind;
    }

    /**
     * 设置  kind
     *
     * @param: kind  站点类型(水文,水位)
     */
    public void setKind(Integer kind) {
        this.kind = kind;
    }

    /**
     * 获取  areaId
     *
     * @return: String  区域
     */
    public String getAreaId() {
        return this.areaId;
    }

    /**
     * 设置  areaId
     *
     * @param: areaId  区域
     */
    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    /**
     * 获取  areaName
     *
     * @return: String  区域名
     */
    public String getAreaName() {
        return this.areaName;
    }

    /**
     * 设置  areaName
     *
     * @param: areaName  区域名
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Double getRainfall() {
        return rainfall;
    }

    public void setRainfall(Double rainfall) {
        this.rainfall = rainfall;
    }

    public Double getWaterLevel() {
        return waterLevel;
    }

    public void setWaterLevel(Double waterLevel) {
        this.waterLevel = waterLevel;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }
}
