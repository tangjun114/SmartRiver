package cn.jeeweb.modules.flood.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 避难中心
 * @Description: 避难中心
 * @author liyonglei
 * @date 2018-11-09 11:19:20
 * @version V1.0   
 *
 */
@TableName("sr_flood_save_haven")
@SuppressWarnings("serial")
public class FloodSaveHaven extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**避险中心代码*/
    @TableField(value = "safe_haven_code")
	private String safeHavenCode;
    /**避险中心名称*/
    @TableField(value = "safe_haven_name")
	private String safeHavenName;
    /**地址*/
    @TableField(value = "address")
	private String address;
    /**联系方式*/
    @TableField(value = "phone")
	private String phone;
    /**联系人*/
    @TableField(value = "contacts")
	private String contacts;
    /**容纳人数*/
    @TableField(value = "contain_num")
	private Integer containNum;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  safeHavenCode
	 *@return: String  避险中心代码
	 */
	public String getSafeHavenCode(){
		return this.safeHavenCode;
	}

	/**
	 * 设置  safeHavenCode
	 *@param: safeHavenCode  避险中心代码
	 */
	public void setSafeHavenCode(String safeHavenCode){
		this.safeHavenCode = safeHavenCode;
	}
	/**
	 * 获取  safeHavenName
	 *@return: String  避险中心名称
	 */
	public String getSafeHavenName(){
		return this.safeHavenName;
	}

	/**
	 * 设置  safeHavenName
	 *@param: safeHavenName  避险中心名称
	 */
	public void setSafeHavenName(String safeHavenName){
		this.safeHavenName = safeHavenName;
	}
	/**
	 * 获取  address
	 *@return: String  地址
	 */
	public String getAddress(){
		return this.address;
	}

	/**
	 * 设置  address
	 *@param: address  地址
	 */
	public void setAddress(String address){
		this.address = address;
	}
	/**
	 * 获取  phone
	 *@return: String  联系方式
	 */
	public String getPhone(){
		return this.phone;
	}

	/**
	 * 设置  phone
	 *@param: phone  联系方式
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	/**
	 * 获取  contacts
	 *@return: String  联系人
	 */
	public String getContacts(){
		return this.contacts;
	}

	/**
	 * 设置  contacts
	 *@param: contacts  联系人
	 */
	public void setContacts(String contacts){
		this.contacts = contacts;
	}
	/**
	 * 获取  containNum
	 *@return: Integer  容纳人数
	 */
	public Integer getContainNum(){
		return this.containNum;
	}

	/**
	 * 设置  containNum
	 *@param: containNum  容纳人数
	 */
	public void setContainNum(Integer containNum){
		this.containNum = containNum;
	}
	
}
