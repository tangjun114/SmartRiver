package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectInfoMapper;
import cn.jeeweb.modules.project.entity.SrProjectInfo;
import cn.jeeweb.modules.project.service.ISrProjectInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目信息
 * @Description: 项目信息
 * @author jerry
 * @date 2018-11-13 21:30:12
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectInfoService")
public class SrProjectInfoServiceImpl  extends CommonServiceImpl<SrProjectInfoMapper,SrProjectInfo> implements  ISrProjectInfoService {

}
