package cn.jeeweb.modules.project.controller;


import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.project.entity.SrProjectInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectInfoChangeLog;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wsh
 * @version V1.0
 * @Title: 工程信息变动调整记录
 * @Description: 工程信息变动调整记录
 * @date 2019-03-31 14:34:53
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectinfochangelog")
@RequiresPathPermission("project:srprojectinfochangelog")
public class SrProjectInfoChangeLogController extends BaseCRUDController<SrProjectInfoChangeLog, String> {

    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        super.preList(model, request, response);
        String id = request.getParameter("id");
        if (StringUtils.isNotEmpty(id)) {
            model.addAttribute("status", id);
        }
    }


    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<SrProjectInfoChangeLog> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
        super.preAjaxList(queryable, entityWrapper, request, response);
        String id = request.getParameter("id");
        if (StringUtils.isNotEmpty(id)) {
            queryable.addCondition("id", id);
        }
        entityWrapper.orderBy("create_date", false);
    }
}
