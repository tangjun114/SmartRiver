package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemDeleteApply;

/**   
 * @Title: 原始文件删除申请
 * @Description: 原始文件删除申请
 * @author Aether
 * @date 2018-06-05 08:41:22
 * @version V1.0   
 *
 */
public interface IMcProItemDeleteApplyService extends ICommonService<McProItemDeleteApply> {

}

