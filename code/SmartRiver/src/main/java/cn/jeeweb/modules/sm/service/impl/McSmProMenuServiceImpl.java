package cn.jeeweb.modules.sm.service.impl;

import cn.jeeweb.core.common.service.impl.TreeCommonServiceImpl;
import cn.jeeweb.modules.sm.mapper.McSmProMenuMapper;
import cn.jeeweb.modules.sm.entity.McSmProMenu;
import cn.jeeweb.modules.sm.service.IMcSmProMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目管理-监测菜单
 * @Description: 项目管理-监测菜单
 * @author shawloong
 * @date 2017-10-30 00:26:12
 * @version V1.0   
 *
 */
@Transactional
@Service("mcSmProMenuService")
public class McSmProMenuServiceImpl  extends TreeCommonServiceImpl<McSmProMenuMapper,McSmProMenu,String> implements  IMcSmProMenuService {

}
