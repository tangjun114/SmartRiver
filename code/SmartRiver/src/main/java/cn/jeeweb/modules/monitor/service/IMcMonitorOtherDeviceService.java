package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McMonitorOtherDevice;

/**   
 * @Title: 其他设备
 * @Description: 其他设备
 * @author jerry
 * @date 2018-01-26 15:29:54
 * @version V1.0   
 *
 */
public interface IMcMonitorOtherDeviceService extends ICommonService<McMonitorOtherDevice> {

}

