package cn.jeeweb.modules.water.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ff.common.util.anno.Chart;

import java.util.Date;

/**
 * @author wsh
 * @version V1.0
 * @Title: 水质监测
 * @Description: 水质监测
 * @date 2019-01-23 17:15:42
 */
@TableName("sr_water_quality_monitor")
@SuppressWarnings("serial")
public class SrWaterQualityMonitor extends AbstractEntity<String> {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    /**
     * 创建者
     */
    @TableField(value = "create_by")
    private String createBy;
    /**
     * 创建者名称
     */
    @TableField(value = "create_by_name")
    private String createByName;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;
    /**
     * 更新者名称
     */
    @TableField(value = "update_by_name")
    private String updateByName;
    /**
     * 更新时间
     */
    @TableField(value = "update_date")
    private Date updateDate;
    /**
     * 删除标记（0：正常；1：删除）
     */
    @TableField(value = "del_flag")
    private String delFlag;
    /**
     * 备注信息
     */
    @TableField(value = "remarks")
    private String remarks;
    /**
     * 记录日期
     */
    @TableField(value = "record_time")
    @JsonProperty(value = "time_sam")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    private Date recordTime;
    /**
     * cod值
     */
    @TableField(value = "cod_meas")
    @JsonProperty(value = "cod_meas")
    @Chart(name = "cod值")
    private String codMeas;
    /**
     * 氨氮
     */
    @TableField(value = "nh4_meas")
    @JsonProperty(value = "nh4_meas")
    @Chart(name = "氨氮")
    private String nh4Meas;
    /**
     * 总磷
     */
    @TableField(value = "tp_meas")
    @JsonProperty(value = "tp_meas")
    @Chart(name = "总磷")
    private String tpMeas;
    /**
     * toc
     */
    @TableField(value = "toc_meas")
    @JsonProperty(value = "toc_meas")
    @Chart(name = "toc")
    private String tocMeas;
    /**
     * 电导率
     */
    @TableField(value = "tds_meas")
    @JsonProperty(value = "tds_meas")
    @Chart(name = "电导率")
    private String tdsMeas;
    /**
     * PH
     */
    @TableField(value = "ph_meas")
    @JsonProperty(value = "ph_meas")
    @Chart(name = "PH")
    private String phMeas;
    /**
     * 浊度
     */
    @TableField(value = "meas")
    @JsonProperty(value = "ss_meas")
    @Chart(name = "浊度")
    private String ss_meas;
    /**
     * 溶解氧
     */
    @TableField(value = "do_meas")
    @JsonProperty(value = "do_meas")
    @Chart(name = "溶解氧")
    private String doMeas;
    /**
     * 温度
     */
    @TableField(value = "tem_meas")
    @JsonProperty(value = "tem_meas")
    @Chart(name = "温度")
    private String temMeas;
    /**
     * 盐度
     */
    @TableField(value = "yd_meas")
    @JsonProperty(value = "yd_meas")
    @Chart(name = "盐度")
    private String ydMeas;
    /**
     * 站点ids
     */
    @TableField(value = "site_id")
    private String siteId;
    /**
     * 站点名
     */
    @TableField(value = "site_name")
    @JsonProperty(value = "point")
    private String siteName;

    /**
     * 获取  id
     *
     * @return: String  主键
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置  id
     *
     * @param: id  主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取  createBy
     *
     * @return: String  创建者
     */
    public String getCreateBy() {
        return this.createBy;
    }

    /**
     * 设置  createBy
     *
     * @param: createBy  创建者
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取  createByName
     *
     * @return: String  创建者名称
     */
    public String getCreateByName() {
        return this.createByName;
    }

    /**
     * 设置  createByName
     *
     * @param: createByName  创建者名称
     */
    public void setCreateByName(String createByName) {
        this.createByName = createByName;
    }

    /**
     * 获取  createDate
     *
     * @return: Date  创建时间
     */
    public Date getCreateDate() {
        return this.createDate;
    }

    /**
     * 设置  createDate
     *
     * @param: createDate  创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取  updateBy
     *
     * @return: String  更新者
     */
    public String getUpdateBy() {
        return this.updateBy;
    }

    /**
     * 设置  updateBy
     *
     * @param: updateBy  更新者
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取  updateByName
     *
     * @return: String  更新者名称
     */
    public String getUpdateByName() {
        return this.updateByName;
    }

    /**
     * 设置  updateByName
     *
     * @param: updateByName  更新者名称
     */
    public void setUpdateByName(String updateByName) {
        this.updateByName = updateByName;
    }

    /**
     * 获取  updateDate
     *
     * @return: Date  更新时间
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 设置  updateDate
     *
     * @param: updateDate  更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取  delFlag
     *
     * @return: String  删除标记（0：正常；1：删除）
     */
    public String getDelFlag() {
        return this.delFlag;
    }

    /**
     * 设置  delFlag
     *
     * @param: delFlag  删除标记（0：正常；1：删除）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 获取  remarks
     *
     * @return: String  备注信息
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置  remarks
     *
     * @param: remarks  备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取  recordTime
     *
     * @return: Date  记录日期
     */
    public Date getRecordTime() {
        return this.recordTime;
    }

    /**
     * 设置  recordTime
     *
     * @param: recordTime  记录日期
     */
    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    /**
     * 获取  codMeas
     *
     * @return: String  cod值
     */
    public String getCodMeas() {
        return this.codMeas;
    }

    /**
     * 设置  codMeas
     *
     * @param: codMeas  cod值
     */
    public void setCodMeas(String codMeas) {
        this.codMeas = codMeas;
    }

    /**
     * 获取  nh4Meas
     *
     * @return: String  氨氮
     */
    public String getNh4Meas() {
        return this.nh4Meas;
    }

    /**
     * 设置  nh4Meas
     *
     * @param: nh4Meas  氨氮
     */
    public void setNh4Meas(String nh4Meas) {
        this.nh4Meas = nh4Meas;
    }

    /**
     * 获取  tpMeas
     *
     * @return: String  总磷
     */
    public String getTpMeas() {
        return this.tpMeas;
    }

    /**
     * 设置  tpMeas
     *
     * @param: tpMeas  总磷
     */
    public void setTpMeas(String tpMeas) {
        this.tpMeas = tpMeas;
    }

    /**
     * 获取  tocMeas
     *
     * @return: String  toc
     */
    public String getTocMeas() {
        return this.tocMeas;
    }

    /**
     * 设置  tocMeas
     *
     * @param: tocMeas  toc
     */
    public void setTocMeas(String tocMeas) {
        this.tocMeas = tocMeas;
    }

    /**
     * 获取  tdsMeas
     *
     * @return: String  电导率
     */
    public String getTdsMeas() {
        return this.tdsMeas;
    }

    /**
     * 设置  tdsMeas
     *
     * @param: tdsMeas  电导率
     */
    public void setTdsMeas(String tdsMeas) {
        this.tdsMeas = tdsMeas;
    }

    /**
     * 获取  phMeas
     *
     * @return: String  PH
     */
    public String getPhMeas() {
        return this.phMeas;
    }

    /**
     * 设置  phMeas
     *
     * @param: phMeas  PH
     */
    public void setPhMeas(String phMeas) {
        this.phMeas = phMeas;
    }


    public String getSs_meas() {
        return ss_meas;
    }

    public void setSs_meas(String ss_meas) {
        this.ss_meas = ss_meas;
    }

    /**
     * 获取  doMeas
     *
     * @return: String  溶解氧
     */
    public String getDoMeas() {
        return this.doMeas;
    }

    /**
     * 设置  doMeas
     *
     * @param: doMeas  溶解氧
     */
    public void setDoMeas(String doMeas) {
        this.doMeas = doMeas;
    }

    /**
     * 获取  temMeas
     *
     * @return: String  温度
     */
    public String getTemMeas() {
        return this.temMeas;
    }

    /**
     * 设置  temMeas
     *
     * @param: temMeas  温度
     */
    public void setTemMeas(String temMeas) {
        this.temMeas = temMeas;
    }

    /**
     * 获取  ydMeas
     *
     * @return: String  盐度
     */
    public String getYdMeas() {
        return this.ydMeas;
    }

    /**
     * 设置  ydMeas
     *
     * @param: ydMeas  盐度
     */
    public void setYdMeas(String ydMeas) {
        this.ydMeas = ydMeas;
    }

    /**
     * 获取  siteId
     *
     * @return: String  站点ids
     */
    public String getSiteId() {
        return this.siteId;
    }

    /**
     * 设置  siteId
     *
     * @param: siteId  站点ids
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * 获取  siteName
     *
     * @return: String  站点名
     */
    public String getSiteName() {
        return this.siteName;
    }

    /**
     * 设置  siteName
     *
     * @param: siteName  站点名
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

}
