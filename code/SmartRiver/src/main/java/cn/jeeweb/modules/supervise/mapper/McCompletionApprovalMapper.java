package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McCompletionApproval;
 
/**   
 * @Title: 完工审批表数据库控制层接口
 * @Description: 完工审批表数据库控制层接口
 * @author shawloong
 * @date 2017-10-07 16:28:41
 * @version V1.0   
 *
 */
public interface McCompletionApprovalMapper extends BaseMapper<McCompletionApproval> {
    
}