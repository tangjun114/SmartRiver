package cn.jeeweb.modules.water.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.water.entity.SrWaterQualityMonitor;

/**   
 * @Title: 水质监测
 * @Description: 水质监测
 * @author wsh
 * @date 2019-01-23 17:15:42
 * @version V1.0   
 *
 */
public interface ISrWaterQualityMonitorService extends ICommonService<SrWaterQualityMonitor> {

}

