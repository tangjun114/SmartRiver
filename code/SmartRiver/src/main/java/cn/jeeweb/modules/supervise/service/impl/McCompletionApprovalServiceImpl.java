package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McCompletionApprovalMapper;
import cn.jeeweb.modules.supervise.entity.McCompletionApproval;
import cn.jeeweb.modules.supervise.service.IMcCompletionApprovalService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 完工审批表
 * @Description: 完工审批表
 * @author shawloong
 * @date 2017-10-07 16:28:41
 * @version V1.0   
 *
 */
@Transactional
@Service("mcCompletionApprovalService")
public class McCompletionApprovalServiceImpl  extends CommonServiceImpl<McCompletionApprovalMapper,McCompletionApproval> implements  IMcCompletionApprovalService {

}
