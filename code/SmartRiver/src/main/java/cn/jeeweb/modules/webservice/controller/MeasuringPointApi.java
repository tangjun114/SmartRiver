package cn.jeeweb.modules.webservice.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;

import com.ff.common.util.validate.ValidatorUtil;
import com.ff.common.web.controller.FFTableController;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

@Controller
@RequestMapping("${api.url.prefix}/measuringpoint")
public class MeasuringPointApi extends FFTableController<McProItemMeasuringPoint>
{
	
	@RequestMapping("/param")
	@ResponseBody
	public BaseRspJson<?> getParam(@RequestBody BaseReqJson<McProItemMeasuringPoint> request)
	{
		BaseRspJson rsp = new BaseRspJson();
		McProItemMeasuringPoint obj = request.getObj();
 		Queryable qy = QueryRequest.newQueryable();
		
		qy.addCondition("monitorItemId", obj.getMonitorItemId());

  		if(!ValidatorUtil.isEmpty(obj.getMeasuringPointCode()))
 		{
 			qy.addCondition("measuringPointCode", obj.getMeasuringPointCode());
 		}
 		else
 		{
 			//qy.addCondition("measuringPointCode", obj.getMeasuringPointCode());
 		}
   		List<McProItemMeasuringPoint> e = getService().listWithNoPage(qy);
		rsp.setObj(e);
   		return rsp;
	}
}
