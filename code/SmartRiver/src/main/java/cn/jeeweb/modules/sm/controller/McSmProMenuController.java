package cn.jeeweb.modules.sm.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.common.controller.BaseTreeController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.sm.entity.McSmProMenu;

/**   
 * @Title: 项目管理-监测菜单
 * @Description: 项目管理-监测菜单
 * @author shawloong
 * @date 2017-10-30 00:26:12
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/sm/mcsmpromenu")
@RequiresPathPermission("sm:mcsmpromenu")
public class McSmProMenuController extends BaseTreeController<McSmProMenu, String> {

}
