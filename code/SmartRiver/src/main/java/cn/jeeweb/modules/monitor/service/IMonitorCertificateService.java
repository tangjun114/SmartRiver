package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.MonitorCertificate;

/**   
 * @Title: 机构证书管理
 * @Description: 机构证书管理
 * @author shawloong
 * @date 2017-10-05 21:31:07
 * @version V1.0   
 *
 */
public interface IMonitorCertificateService extends ICommonService<MonitorCertificate> {

}

