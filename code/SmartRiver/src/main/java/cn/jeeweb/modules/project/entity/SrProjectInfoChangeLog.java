package cn.jeeweb.modules.project.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 工程信息变动调整记录
 * @Description: 工程信息变动调整记录
 * @author wsh
 * @date 2019-03-31 14:34:53
 * @version V1.0   
 *
 */
@TableName("sr_project_info_change_log")
@SuppressWarnings("serial")
public class SrProjectInfoChangeLog extends AbstractEntity<String> {

    /**主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目名称*/
    @TableField(value = "name")
	private String name;
    /**项目描述*/
    @TableField(value = "desc")
	private String desc;
    /**项目地址*/
    @TableField(value = "addr")
	private String addr;
    /**负责人*/
    @TableField(value = "duty_user_id")
	private String dutyUserId;
    /**负责人名称*/
    @TableField(value = "duty_user_name")
	private String dutyUserName;
    /**项目类别*/
    @TableField(value = "type")
	private String type;
    /**项目状态*/
    @TableField(value = "status")
	private String status;
    /**地图经度*/
    @TableField(value = "longitude")
	private String longitude;
    /**地图纬度*/
    @TableField(value = "latitude")
	private String latitude;
    /**进展情况*/
    @TableField(value = "progress")
	private String progress;
    /**责任单位*/
    @TableField(value = "duty_org")
	private String dutyOrg;
    /**建设单位*/
    @TableField(value = "build_org")
	private String buildOrg;
    /**设计单位*/
    @TableField(value = "design_org")
	private String designOrg;
    /**监测单位*/
    @TableField(value = "monitor_org")
	private String monitorOrg;
    /**河道或官网建设规模,单位:公里*/
    @TableField(value = "scale")
	private String scale;
    /**项目资金来源*/
    @TableField(value = "amt_source")
	private String amtSource;
    /**资金计划投入*/
    @TableField(value = "amt")
	private String amt;
    /**开工时间*/
    @TableField(value = "start_time")
	private Date startTime;
	
	/**
	 * 获取  id
	 *@return: String  主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  name
	 *@return: String  项目名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  项目名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  desc
	 *@return: String  项目描述
	 */
	public String getDesc(){
		return this.desc;
	}

	/**
	 * 设置  desc
	 *@param: desc  项目描述
	 */
	public void setDesc(String desc){
		this.desc = desc;
	}
	/**
	 * 获取  addr
	 *@return: String  项目地址
	 */
	public String getAddr(){
		return this.addr;
	}

	/**
	 * 设置  addr
	 *@param: addr  项目地址
	 */
	public void setAddr(String addr){
		this.addr = addr;
	}
	/**
	 * 获取  dutyUserId
	 *@return: String  负责人
	 */
	public String getDutyUserId(){
		return this.dutyUserId;
	}

	/**
	 * 设置  dutyUserId
	 *@param: dutyUserId  负责人
	 */
	public void setDutyUserId(String dutyUserId){
		this.dutyUserId = dutyUserId;
	}
	/**
	 * 获取  dutyUserName
	 *@return: String  负责人名称
	 */
	public String getDutyUserName(){
		return this.dutyUserName;
	}

	/**
	 * 设置  dutyUserName
	 *@param: dutyUserName  负责人名称
	 */
	public void setDutyUserName(String dutyUserName){
		this.dutyUserName = dutyUserName;
	}
	/**
	 * 获取  type
	 *@return: String  项目类别
	 */
	public String getType(){
		return this.type;
	}

	/**
	 * 设置  type
	 *@param: type  项目类别
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 * 获取  status
	 *@return: String  项目状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  项目状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  longitude
	 *@return: String  地图经度
	 */
	public String getLongitude(){
		return this.longitude;
	}

	/**
	 * 设置  longitude
	 *@param: longitude  地图经度
	 */
	public void setLongitude(String longitude){
		this.longitude = longitude;
	}
	/**
	 * 获取  latitude
	 *@return: String  地图纬度
	 */
	public String getLatitude(){
		return this.latitude;
	}

	/**
	 * 设置  latitude
	 *@param: latitude  地图纬度
	 */
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
	/**
	 * 获取  progress
	 *@return: String  进展情况
	 */
	public String getProgress(){
		return this.progress;
	}

	/**
	 * 设置  progress
	 *@param: progress  进展情况
	 */
	public void setProgress(String progress){
		this.progress = progress;
	}
	/**
	 * 获取  dutyOrg
	 *@return: String  责任单位
	 */
	public String getDutyOrg(){
		return this.dutyOrg;
	}

	/**
	 * 设置  dutyOrg
	 *@param: dutyOrg  责任单位
	 */
	public void setDutyOrg(String dutyOrg){
		this.dutyOrg = dutyOrg;
	}
	/**
	 * 获取  buildOrg
	 *@return: String  建设单位
	 */
	public String getBuildOrg(){
		return this.buildOrg;
	}

	/**
	 * 设置  buildOrg
	 *@param: buildOrg  建设单位
	 */
	public void setBuildOrg(String buildOrg){
		this.buildOrg = buildOrg;
	}
	/**
	 * 获取  designOrg
	 *@return: String  设计单位
	 */
	public String getDesignOrg(){
		return this.designOrg;
	}

	/**
	 * 设置  designOrg
	 *@param: designOrg  设计单位
	 */
	public void setDesignOrg(String designOrg){
		this.designOrg = designOrg;
	}
	/**
	 * 获取  monitorOrg
	 *@return: String  监测单位
	 */
	public String getMonitorOrg(){
		return this.monitorOrg;
	}

	/**
	 * 设置  monitorOrg
	 *@param: monitorOrg  监测单位
	 */
	public void setMonitorOrg(String monitorOrg){
		this.monitorOrg = monitorOrg;
	}
	/**
	 * 获取  scale
	 *@return: String  河道或官网建设规模,单位:公里
	 */
	public String getScale(){
		return this.scale;
	}

	/**
	 * 设置  scale
	 *@param: scale  河道或官网建设规模,单位:公里
	 */
	public void setScale(String scale){
		this.scale = scale;
	}
	/**
	 * 获取  amtSource
	 *@return: String  项目资金来源
	 */
	public String getAmtSource(){
		return this.amtSource;
	}

	/**
	 * 设置  amtSource
	 *@param: amtSource  项目资金来源
	 */
	public void setAmtSource(String amtSource){
		this.amtSource = amtSource;
	}
	/**
	 * 获取  amt
	 *@return: String  资金计划投入
	 */
	public String getAmt(){
		return this.amt;
	}

	/**
	 * 设置  amt
	 *@param: amt  资金计划投入
	 */
	public void setAmt(String amt){
		this.amt = amt;
	}
	/**
	 * 获取  startTime
	 *@return: Date  开工时间
	 */
	public Date getStartTime(){
		return this.startTime;
	}

	/**
	 * 设置  startTime
	 *@param: startTime  开工时间
	 */
	public void setStartTime(Date startTime){
		this.startTime = startTime;
	}
	
}
