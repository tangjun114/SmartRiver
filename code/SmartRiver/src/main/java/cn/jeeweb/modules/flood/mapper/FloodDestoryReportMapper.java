package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodDestoryReport;
 
/**   
 * @Title: 水毁报表数据库控制层接口
 * @Description: 水毁报表数据库控制层接口
 * @author wsh
 * @date 2018-11-27 18:19:56
 * @version V1.0   
 *
 */
public interface FloodDestoryReportMapper extends BaseMapper<FloodDestoryReport> {
    
}