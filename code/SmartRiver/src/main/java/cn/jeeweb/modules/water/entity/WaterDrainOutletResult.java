package cn.jeeweb.modules.water.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 排污口水质监控
 * @Description: 排污口水质监控
 * @author liyonglei
 * @date 2018-11-14 19:41:31
 * @version V1.0   
 *
 */
@TableName("sr_water_drain_outlet_result")
@SuppressWarnings("serial")
public class WaterDrainOutletResult extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**排污口名称*/
    @TableField(value = "outlet_name")
	private String outletName;
    /**监控类型*/
    @TableField(value = "monitor_type")
	private String monitorType;
    /**水量*/
    @TableField(value = "water_amount")
	private String waterAmount;
    /**水质*/
    @TableField(value = "water_quality")
	private String waterQuality;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  outletName
	 *@return: String  排污口名称
	 */
	public String getOutletName(){
		return this.outletName;
	}

	/**
	 * 设置  outletName
	 *@param: outletName  排污口名称
	 */
	public void setOutletName(String outletName){
		this.outletName = outletName;
	}
	/**
	 * 获取  monitorType
	 *@return: String  监控类型
	 */
	public String getMonitorType(){
		return this.monitorType;
	}

	/**
	 * 设置  monitorType
	 *@param: monitorType  监控类型
	 */
	public void setMonitorType(String monitorType){
		this.monitorType = monitorType;
	}
	/**
	 * 获取  waterAmount
	 *@return: String  水量
	 */
	public String getWaterAmount(){
		return this.waterAmount;
	}

	/**
	 * 设置  waterAmount
	 *@param: waterAmount  水量
	 */
	public void setWaterAmount(String waterAmount){
		this.waterAmount = waterAmount;
	}
	/**
	 * 获取  waterQuality
	 *@return: String  水质
	 */
	public String getWaterQuality(){
		return this.waterQuality;
	}

	/**
	 * 设置  waterQuality
	 *@param: waterQuality  水质
	 */
	public void setWaterQuality(String waterQuality){
		this.waterQuality = waterQuality;
	}
	
}
