package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorCertificate;
 
/**   
 * @Title: 机构证书管理数据库控制层接口
 * @Description: 机构证书管理数据库控制层接口
 * @author shawloong
 * @date 2017-10-05 22:28:45
 * @version V1.0   
 *
 */
public interface McMonitorCertificateMapper extends BaseMapper<McMonitorCertificate> {
    
}