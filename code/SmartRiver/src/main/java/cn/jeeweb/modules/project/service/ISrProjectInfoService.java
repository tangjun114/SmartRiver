package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectInfo;

/**   
 * @Title: 项目信息
 * @Description: 项目信息
 * @author jerry
 * @date 2018-11-13 21:30:12
 * @version V1.0   
 *
 */
public interface ISrProjectInfoService extends ICommonService<SrProjectInfo> {

}

