package cn.jeeweb.modules.water.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;

import java.util.Date;

/**
 * @author liyonglei
 * @version V1.0
 * @Title: 排污口管理
 * @Description: 排污口管理
 * @date 2018-11-14 19:33:01
 */
@TableName("sr_water_drain_outlet")
@SuppressWarnings("serial")
public class WaterDrainOutlet extends AbstractEntity<String> {

    /**
     * 字段主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    /**
     * 创建者
     */
    @TableField(value = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField(value = "update_date")
    private Date updateDate;
    /**
     * 删除标记（0：正常；1：删除）
     */
    @TableField(value = "del_flag")
    private String delFlag;
    /**
     * 备注信息
     */
    @TableField(value = "remarks")
    private String remarks;
    /**
     * 名称
     */
    @TableField(value = "outlet_name")
    private String outletName;
    /**
     * 地址
     */
    @TableField(value = "outlet_addr")
    private String outletAddr;
    /**
     * 经度
     */
    @TableField(value = "longitude")
    private String longitude;
    /**
     * 纬度
     */
    @TableField(value = "latitude")
    private String latitude;
    /**
     * 控制信息
     */
    @TableField(value = "water_contrl")
    private String waterContrl;
    /**
     * 用户信息
     */
    @TableField(value = "bind_user")
    private String bindUser;
    /**
     * 监控方式
     */
    @TableField(value = "monitor_mode")
    private String monitorMode;
    /**
     * 设计信息
     */
    @TableField(value = "design_info")
    private String designInfo;
    /**
     * 统计信息
     */
    @TableField(value = "statistics_info")
    private String statisticsInfo;
    /**
     * 水量
     */
    @TableField(value = "water_amont")
    private String waterAmont;
    /**
     * 水量
     */
    @TableField(value = "status")
    private String status;

    /**
     * 获取  id
     *
     * @return: String  字段主键
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置  id
     *
     * @param: id  字段主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取  createBy
     *
     * @return: String  创建者
     */
    public String getCreateBy() {
        return this.createBy;
    }

    /**
     * 设置  createBy
     *
     * @param: createBy  创建者
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取  createDate
     *
     * @return: Date  创建时间
     */
    public Date getCreateDate() {
        return this.createDate;
    }

    /**
     * 设置  createDate
     *
     * @param: createDate  创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取  updateBy
     *
     * @return: String  更新者
     */
    public String getUpdateBy() {
        return this.updateBy;
    }

    /**
     * 设置  updateBy
     *
     * @param: updateBy  更新者
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取  updateDate
     *
     * @return: Date  更新时间
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 设置  updateDate
     *
     * @param: updateDate  更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取  delFlag
     *
     * @return: String  删除标记（0：正常；1：删除）
     */
    public String getDelFlag() {
        return this.delFlag;
    }

    /**
     * 设置  delFlag
     *
     * @param: delFlag  删除标记（0：正常；1：删除）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 获取  remarks
     *
     * @return: String  备注信息
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置  remarks
     *
     * @param: remarks  备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取  outletName
     *
     * @return: String  名称
     */
    public String getOutletName() {
        return this.outletName;
    }

    /**
     * 设置  outletName
     *
     * @param: outletName  名称
     */
    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    /**
     * 获取  outletAddr
     *
     * @return: String  地址
     */
    public String getOutletAddr() {
        return this.outletAddr;
    }

    /**
     * 设置  outletAddr
     *
     * @param: outletAddr  地址
     */
    public void setOutletAddr(String outletAddr) {
        this.outletAddr = outletAddr;
    }

    /**
     * 获取  longitude
     *
     * @return: String  经度
     */
    public String getLongitude() {
        return this.longitude;
    }

    /**
     * 设置  longitude
     *
     * @param: longitude  经度
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 获取  latitude
     *
     * @return: String  纬度
     */
    public String getLatitude() {
        return this.latitude;
    }

    /**
     * 设置  latitude
     *
     * @param: latitude  纬度
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 获取  waterContrl
     *
     * @return: String  控制信息
     */
    public String getWaterContrl() {
        return this.waterContrl;
    }

    /**
     * 设置  waterContrl
     *
     * @param: waterContrl  控制信息
     */
    public void setWaterContrl(String waterContrl) {
        this.waterContrl = waterContrl;
    }

    /**
     * 获取  bindUser
     *
     * @return: String  用户信息
     */
    public String getBindUser() {
        return this.bindUser;
    }

    /**
     * 设置  bindUser
     *
     * @param: bindUser  用户信息
     */
    public void setBindUser(String bindUser) {
        this.bindUser = bindUser;
    }

    /**
     * 获取  monitorMode
     *
     * @return: String  监控方式
     */
    public String getMonitorMode() {
        return this.monitorMode;
    }

    /**
     * 设置  monitorMode
     *
     * @param: monitorMode  监控方式
     */
    public void setMonitorMode(String monitorMode) {
        this.monitorMode = monitorMode;
    }

    /**
     * 获取  designInfo
     *
     * @return: String  设计信息
     */
    public String getDesignInfo() {
        return this.designInfo;
    }

    /**
     * 设置  designInfo
     *
     * @param: designInfo  设计信息
     */
    public void setDesignInfo(String designInfo) {
        this.designInfo = designInfo;
    }

    /**
     * 获取  statisticsInfo
     *
     * @return: String  统计信息
     */
    public String getStatisticsInfo() {
        return this.statisticsInfo;
    }

    /**
     * 设置  statisticsInfo
     *
     * @param: statisticsInfo  统计信息
     */
    public void setStatisticsInfo(String statisticsInfo) {
        this.statisticsInfo = statisticsInfo;
    }

    /**
     * 获取  waterAmont
     *
     * @return: String  水量
     */
    public String getWaterAmont() {
        return this.waterAmont;
    }

    /**
     * 设置  waterAmont
     *
     * @param: waterAmont  水量
     */
    public void setWaterAmont(String waterAmont) {
        this.waterAmont = waterAmont;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
