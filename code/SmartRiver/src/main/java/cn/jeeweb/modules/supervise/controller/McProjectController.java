package cn.jeeweb.modules.supervise.controller;


import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.common.web.json.BaseReqJson;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.model.AjaxJson;
import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.ObjectUtils;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.entity.Menu;
import cn.jeeweb.modules.sys.service.IMenuService;
import cn.jeeweb.modules.sys.service.IOrganizationService;
import cn.jeeweb.modules.sys.utils.DictUtils;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 项目管理
 * @Description: 项目管理
 * @author shawloong
 * @date 2017-10-07 02:00:38
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproject")
@RequiresPathPermission("supervise:mcproject")
public class McProjectController extends BaseCRUDController<McProject, String> {
 
	/**
	 * 保存数据之前
	 * 
	 * @param entity
	 * @param request
	 * @param response
	 */
	@Override
	public void preSave(McProject data, HttpServletRequest request, HttpServletResponse response) {
		if(null==data.getId()&&null==data.getCreateBy()){
			data.setCreateBy(UserUtils.getUser());
		}
		if(null==data.getId()&&null==data.getCreateDate()){
			data.setCreateDate(new Date());
		}
		data.setUpdateBy(UserUtils.getUser());
		data.setUpdateDate(new Date());
		 
	
		super.preSave(data, request, response);
	}
	@Autowired
	private IOrganizationService organizationService;
	@Autowired IMcProjectService iMcProjectService;
	@Override
	public Queryable GetFilterCondition(BaseReqJson request) {
		// TODO Auto-generated method stub
		Queryable qy = super.GetFilterCondition(request);
		Condition conditon = iMcProjectService.getProjectContiondByUser(UserUtils.getUser());
		 qy.addCondition(conditon);
		return qy;
	}
	
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProject> entityWrapper, HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preAjaxList(queryable, entityWrapper, request, response);
 	 
		 
		Condition conditon = iMcProjectService.getProjectContiondByUser(UserUtils.getUser());
		queryable.addCondition(conditon);
	 
	 
	}




	 

	@Override
	public String showCreate(McProject entity, Model model, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return display("create");
	}

	@Autowired
	private IMenuService menuService;
	
	@RequestMapping(value = "/monitorlist")
	public String monitorlist(Model model, HttpServletRequest request, HttpServletResponse response) {
		preList(model, request, response);
		
		String nodeid = "7ee85f1d4de141639b8b00b1e9ca2780";
		EntityWrapper<Menu> entityWrapper = new EntityWrapper<Menu>(Menu.class);
		entityWrapper.setTableAlias("t.");
		entityWrapper.orderBy("t.sort ", true);
		if(!ObjectUtils.isNullOrEmpty(nodeid)){
			entityWrapper.eq("parentId", nodeid);
		}
		Queryable queryable = QueryRequest.newQueryable();
				
		List<Menu> treeNodeList = menuService.selectTreeList(queryable, entityWrapper);
		model.addAttribute("menus", treeNodeList);
		
		return display("monitorList");
	}
	
	@RequestMapping(value = "/detail")
	public String detail(Model model, HttpServletRequest request, HttpServletResponse response) {
		String projectId = request.getParameter("projectId");
		McProject entity = get(projectId);
		showUpdate(entity, model, request, response);
		//项目类别
		String proTypeLabel = DictUtils.getDictLabel(entity.getProjectType(), "XIANGMULB", "");
		entity.setProjectType(proTypeLabel);
		model.addAttribute("detail", entity);
		//String showView = showView(newModel(), model, request, response);
		
		return display("detail");
	}
	
	@RequestMapping(value = "/finish", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson finishProject(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String projectId = request.getParameter("gid");
		McProject entity = get(projectId);
		entity.setWorkStatus("FINISHING");
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.success("项目完工操作成功");
		try {
			iMcProjectService.insertOrUpdate(entity);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.fail("项目完工操作失败");
		}
		return ajaxJson;
	}

 
	
}
