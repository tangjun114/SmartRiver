package cn.jeeweb.modules.supervise.service.impl;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.util.validate.ValidatorUtil;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Condition.Operator;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.constant.AlarmTypeEnum;
import cn.jeeweb.modules.monitor.entity.McProItemAlarm;
import cn.jeeweb.modules.monitor.service.IMcProItemAlarmService;
import cn.jeeweb.modules.supervise.entity.McProItemMember;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.mapper.McProjectMapper;
import cn.jeeweb.modules.supervise.service.IMcProItemMemberService;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.entity.Organization;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.service.IOrganizationService;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 项目管理
 * @Description: 项目管理
 * @author shawloong
 * @date 2017-11-21 16:31:35
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProjectService")
public class McProjectServiceImpl  extends CommonServiceImpl<McProjectMapper,McProject> implements  IMcProjectService {
	
	@Autowired IMcProItemMemberService iMcProItemMemberService;
	
	@Autowired IMcProItemAlarmService alarmService;
	
	@Override
	public boolean insert(McProject mcProject) {
		// 保存主表
		
		mcProject.setMonitorId(UserUtils.getUser().getOrganizationIds());
		
		super.insert(mcProject);
		McProItemMember entity = new McProItemMember();
		entity.setProjectName(mcProject.getName());
		entity.setProjectId(mcProject.getId());
		User user = UserUtils.getUser();
		
		entity.setType(user.getType());
		entity.setEmail(user.getEmail());
		 
		entity.setUsername(user.getUsername());
		entity.setRealname(user.getRealname());
		entity.setPosition(user.getPositionName());
		entity.setDeptName(user.getDeptName());
		entity.setUserId(user.getId());
		entity.setPhone(user.getPhone());
		
		iMcProItemMemberService.insert(entity );
		
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(McProject mcProject) {
		try {
			// 更新主表
			super.insertOrUpdate(mcProject);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}
	
	@Autowired IMcProItemMemberService mcProItemMemberService;
	@Autowired
	private IOrganizationService organizationService;
	public Condition getProjectContiondByUser(User user)
	{
		Condition conditon = null;
		 boolean result =  SecurityUtils.getSubject().isPermitted("supervise:mcproject-all");
		 if(!result)
		 {
			 Queryable qy = QueryRequest.newQueryable();
			 qy.addCondition("userId", user.getId());
			 List<McProItemMember> memberList = mcProItemMemberService.listWithNoPage(qy);
			 List<?> idList = DataFilterUtil.getFieldValueFromList(memberList, "projectId");
			 conditon = new Condition().and(Operator.in, "id", idList);
 		 }
		 else
		 {
			 result =  SecurityUtils.getSubject().isPermitted("supervise:mcproject-supervise");
             if(result)
             {
             	 List<Organization> orgList = organizationService.getSubOrg(user.getOrganizationIds());
    			 List idList = DataFilterUtil.getFieldValueFromList(orgList, "id");
    			 
    			 idList.add(user.getOrganizationIds());
    			 conditon	= new Condition().and(Operator.in, "monitorOrgId", idList);
             }
             else
             {
 			 List<Organization> orgList = organizationService.getSubOrg(user.getOrganizationIds());
			 List idList = DataFilterUtil.getFieldValueFromList(orgList, "id");
			 idList.add(user.getOrganizationIds());
			 conditon	= new Condition().and(Operator.in, "monitorId", idList);
             }
		 }
		 return conditon;
	}

	public List<?> getProjectIdListByUser(User user)
	{
 		if(null == user)
		{
			user = UserUtils.getUser();
		}
			 
			 
		Queryable qy = QueryRequest.newQueryable();
		qy.setCondition(this.getProjectContiondByUser(user));
		List<McProject> projectList = this.listWithNoPage(qy);
		List<?> idList = DataFilterUtil.getFieldValueFromList(projectList, "id");
		
		return idList;
	}

	@Override
	public void updateCount(String projectId, String alarmTypeId) {
		// TODO Auto-generated method stub
		McProject project = selectById(projectId);
		if(project != null){
			EntityWrapper<McProItemAlarm> entityWrapper = new EntityWrapper<McProItemAlarm>(McProItemAlarm.class);
			entityWrapper.ne("new_project_count", '0');
			entityWrapper.eq("project_id", projectId);
			entityWrapper.eq("alarm_type_id", alarmTypeId);
	
			int count = alarmService.selectCount(entityWrapper);
			
			if(AlarmTypeEnum.ALARM.getValue().equals(alarmTypeId)) {
				project.setAlarmPointCount(count);
			} else if(AlarmTypeEnum.WARN.getValue().equals(alarmTypeId)) {
				project.setWarnPointCount(count);
			} else if (AlarmTypeEnum.OVERLOAD.getValue().equals(alarmTypeId)) {
				project.setControlPointCount(count);
			}
			updateById(project);
		}

	}

	
	
}
