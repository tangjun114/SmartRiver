package cn.jeeweb.modules.project.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectSchedule;

/**   
 * @Title: 项目进度
 * @Description: 项目进度
 * @author wsh
 * @date 2018-12-10 20:41:29
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectschedule")
@RequiresPathPermission("project:srprojectschedule")
public class SrProjectScheduleController extends BaseCRUDController<SrProjectSchedule, String> {

}
