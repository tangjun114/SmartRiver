package cn.jeeweb.modules.project.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectFloodManager;

/**   
 * @Title: 工程水务管理
 * @Description: 工程水务管理
 * @author wsh
 * @date 2018-12-25 19:33:17
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectfloodmanager")
@RequiresPathPermission("project:srprojectfloodmanager")
public class SrProjectFloodManagerController extends BaseCRUDController<SrProjectFloodManager, String> {

}
