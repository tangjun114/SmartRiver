package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemDevice;

/**   
 * @Title: 项目与设备管理表
 * @Description: 项目与设备管理表
 * @author jerry
 * @date 2018-01-29 17:01:28
 * @version V1.0   
 *
 */
public interface IMcProItemDeviceService extends ICommonService<McProItemDevice> {

}

