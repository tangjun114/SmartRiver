package cn.jeeweb.modules.sm.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.sm.mapper.IdentityManagerMapper;
import cn.jeeweb.modules.sm.entity.IdentityManager;
import cn.jeeweb.modules.sm.service.IIdentityManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;

/**   
 * @Title: 身份管理
 * @Description: 身份管理
 * @author shawloong
 * @date 2017-09-29 00:44:22
 * @version V1.0   
 *
 */
@Transactional
@Service("identityManagerService")
public class IdentityManagerServiceImpl  extends CommonServiceImpl<IdentityManagerMapper,IdentityManager> implements  IIdentityManagerService {
	
	@Override
	public boolean insert(IdentityManager identityManager) {
		// 保存主表
		super.insert(identityManager);
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(IdentityManager identityManager) {
		try {
			// 更新主表
			super.insertOrUpdate(identityManager);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}
	
	
	
}
