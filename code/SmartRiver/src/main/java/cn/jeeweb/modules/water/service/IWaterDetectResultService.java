package cn.jeeweb.modules.water.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.water.entity.WaterDetectResult;

/**   
 * @Title: 检测数据
 * @Description: 检测数据
 * @author shawloong
 * @date 2018-11-13 21:18:03
 * @version V1.0   
 *
 */
public interface IWaterDetectResultService extends ICommonService<WaterDetectResult> {

}

