package cn.jeeweb.modules.repo.controller;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.monitor.controller.McOrgBaseController;
import cn.jeeweb.modules.repo.entity.McRepoStandard;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 规范资料
 * @Description: 规范资料
 * @author shawloong
 * @date 2017-10-04 15:16:45
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/repo/mcrepostandard")
@RequiresPathPermission("repo:mcrepostandard")
public class McRepoStandardController extends McOrgBaseController<McRepoStandard, String> {
	
	@Override
	public void preEdit(McRepoStandard mcRepoStandard, Model model, HttpServletRequest request, HttpServletResponse response) {
	}
	
	/**
	 * 保存数据之前
	 * 
	 * @param entity
	 * @param request
	 * @param response
	 */
	@Override
	public void preSave(McRepoStandard entity, HttpServletRequest request, HttpServletResponse response) {
		if(null==entity.getCreateBy()){
			entity.setCreateBy(UserUtils.getUser());
		}
		
		if (null==entity.getCreateByName()){
			entity.setCreateByName(UserUtils.getUser().getUsername());
		}
		if(null==entity.getCreateDate()){
			entity.setCreateDate(new Date());
		}
		entity.setUpdateDate(new Date());
		super.preSave(entity, request, response);
	}
}
