package cn.jeeweb.modules.video.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.video.mapper.SrVideoPointMapper;
import cn.jeeweb.modules.video.entity.SrVideoPoint;
import cn.jeeweb.modules.video.service.ISrVideoPointService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 视频站点管理
 * @Description: 视频站点管理
 * @author wsh
 * @date 2019-04-01 14:30:31
 * @version V1.0   
 *
 */
@Transactional
@Service("srVideoPointService")
public class SrVideoPointServiceImpl  extends CommonServiceImpl<SrVideoPointMapper,SrVideoPoint> implements  ISrVideoPointService {

}
