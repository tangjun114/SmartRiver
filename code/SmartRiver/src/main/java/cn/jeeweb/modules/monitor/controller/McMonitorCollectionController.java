package cn.jeeweb.modules.monitor.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.util.meta.ReflectionUtil;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McMonitorCollection;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProjectService;

/**   
 * @Title: 采集仪器
 * @Description: 采集仪器
 * @author shawloong
 * @date 2017-10-10 21:42:22
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/monitor/mcmonitorcollection")
@RequiresPathPermission("monitor:mcmonitorcollection")
public class McMonitorCollectionController extends McOrgBaseController<McMonitorCollection, String> {
	
	@Override
	public void preEdit(McMonitorCollection mcMonitorCollection, Model model, HttpServletRequest request, HttpServletResponse response) {
	}
	
	
	@Autowired
	IMcProjectService projectService;
	
	@Override
	public void preAjaxList(Queryable queryable,EntityWrapper<McMonitorCollection> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
		String  projectId = request.getParameter("projectId");
		if(!StringUtils.isEmpty(projectId)){
			entityWrapper.eq("projectId", projectId);
			 
		}
 
		super.preAjaxList(queryable, entityWrapper, request, response);
	}
	
	@Override
	public void preSave(McMonitorCollection entity, HttpServletRequest request, HttpServletResponse response) {
		String projectId = request.getParameter("projectId");
		
		McProject project = projectService.selectById(projectId);
		if (null != project) {
			ReflectionUtil.setObjectField(entity, "projectId", project.getId());
			ReflectionUtil.setObjectField(entity, "projectName", project.getName());

		}
		super.preSave(entity, request, response);
	}
}
