package cn.jeeweb.modules.monitor.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.model.AjaxJson;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McMonitorRealdata;
import cn.jeeweb.modules.supervise.entity.McProItemDeleteApply;
import cn.jeeweb.modules.supervise.service.IMcProItemDeleteApplyService;
import cn.jeeweb.modules.sys.entity.Organization;
import cn.jeeweb.modules.sys.service.IOrganizationService;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 监测原始记录信息
 * @Description: 监测原始记录信息
 * @author Aether
 * @date 2017-11-29 23:54:11
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/monitor/mcmonitorrealdata")
@RequiresPathPermission("monitor:mcmonitorrealdata")
public class McMonitorRealdataController extends BaseCRUDController<McMonitorRealdata, String> {

	@Autowired 
	IMcProItemDeleteApplyService deleteApplyService;
	@Autowired
	private IOrganizationService orgService;
	
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McMonitorRealdata> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
		String  projectId = request.getParameter("projectId");
		if(!StringUtils.isEmpty(projectId)){
			entityWrapper.eq("project_id", projectId);
		}
		entityWrapper.orderBy("create_date", false);
	}

	
	@Override
	public AjaxJson doSave(McMonitorRealdata entity, HttpServletRequest request, HttpServletResponse response,
			BindingResult result) {
		// TODO Auto-generated method stub
		String op = request.getParameter("op");
		if("deleteapply".equals(op)) {
			AjaxJson ajaxJson = new AjaxJson();
			ajaxJson.success("删除申请提交成功");
			McMonitorRealdata data = get(entity.getId());
			long timeGap = System.currentTimeMillis()-data.getUploadTime().getTime();
			if (timeGap > 48*60*60*1000) {
				ajaxJson.fail("上传时间超过48小时，无法删除");
				return ajaxJson;
			}
			
			EntityWrapper<McProItemDeleteApply> entityWrapper = new EntityWrapper<McProItemDeleteApply>(McProItemDeleteApply.class);
			entityWrapper.eq("dataId", entity.getId());
			
			McProItemDeleteApply apply = deleteApplyService.selectOne(entityWrapper);
			if (apply != null) {
				ajaxJson.fail("删除申请已经提交，请勿重复提交");
				return ajaxJson;
			}
		
			apply = new McProItemDeleteApply();
			

			apply.setDataId(entity.getId());
			apply.setFileName(data.getFileName());
			apply.setFilePath(data.getFilePath());
			
			apply.setProjectId(data.getProjectId());
			apply.setProjectName(data.getProjectName());
			
			apply.setPoint(data.getMpId());
			apply.setPointNum(data.getMpCount());
			apply.setMeasureResult(data.getMeasureResult());

			apply.setMonitorCount(data.getMonitorCount());
			apply.setMonitorItemId(data.getMonitorItemId());
			apply.setMonitorItemName(data.getMonitorItemName());
			//机构信息
			apply.setMonitorId(UserUtils.getUser().getOrganizationIds());
		    Organization org = orgService.selectById(UserUtils.getUser().getOrganizationIds());   
		    if(org != null) {
		    	apply.setMonitorName(org.getName());
		    }
			
			apply.setApplyUserName(UserUtils.getUser().getRealname());
			apply.setApplyUserId(UserUtils.getUser().getId());
			//从页面获取
			apply.setReason(entity.getReason());
			apply.setApplyFile(entity.getApplyFile());
			apply.setStatus("to_approve");//待批准
			try {
				deleteApplyService.insertOrUpdate(apply);
			} catch (Exception e) {
				e.printStackTrace();
				ajaxJson.fail("删除申请提交失败");
			}
			
			return ajaxJson;
		} else {
			return super.doSave(entity, request, response, result);
		}
		
	}


	@RequestMapping(value = "/deleteapply", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson deleteApply(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		String dataId = request.getParameter("gid");
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.success("删除申请提交成功");
		
		McMonitorRealdata entity = get(dataId);
		if (entity == null) {
			ajaxJson.fail("数据不存在");
			return ajaxJson;
		}

		EntityWrapper<McProItemDeleteApply> entityWrapper = new EntityWrapper<McProItemDeleteApply>(McProItemDeleteApply.class);
		entityWrapper.eq("dataId", dataId);
		
		McProItemDeleteApply apply = deleteApplyService.selectOne(entityWrapper);
		if (apply != null) {
			ajaxJson.fail("删除申请已经提交，请勿重复提交");
			return ajaxJson;
		}
		apply = new McProItemDeleteApply();
		
		apply.setApplyUserName(UserUtils.getUser().getRealname());
		apply.setApplyUserId(UserUtils.getUser().getId());
		
		apply.setDataId(dataId);
		apply.setFileName(entity.getFileName());
		apply.setFilePath(entity.getFilePath());
		
		apply.setProjectId(entity.getProjectId());
		apply.setProjectName(entity.getProjectName());
		
		apply.setMeasureResult(entity.getMeasureResult());
		//apply.setMonitorCount(String.valueOf(entity.getMonitorCount()));
		
		//apply.setReason(reason);
		apply.setStatus("to_approve");//待批准
		try {
			deleteApplyService.insertOrUpdate(apply);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.fail("删除申请提交成功");
		}
		return ajaxJson;
	}

}
