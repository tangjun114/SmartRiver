package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectEvaluation;
 
/**   
 * @Title: 项目评比数据库控制层接口
 * @Description: 项目评比数据库控制层接口
 * @author wsh
 * @date 2019-03-31 22:54:37
 * @version V1.0   
 *
 */
public interface SrProjectEvaluationMapper extends BaseMapper<SrProjectEvaluation> {
    
}