package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McMonitorCertificate;

/**   
 * @Title: 机构证书管理
 * @Description: 机构证书管理
 * @author shawloong
 * @date 2017-10-05 22:28:45
 * @version V1.0   
 *
 */
public interface IMcMonitorCertificateService extends ICommonService<McMonitorCertificate> {

}

