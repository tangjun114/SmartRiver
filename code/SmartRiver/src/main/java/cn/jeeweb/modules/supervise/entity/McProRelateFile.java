package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.utils.UserUtils;

import java.util.Date;

/**   
 * @Title: 项目相关文件
 * @Description: 项目相关文件
 * @author aether
 * @date 2018-01-24 23:54:08
 * @version V1.0   
 *
 */
@TableName("mc_pro_relate_file")
@SuppressWarnings("serial")
public class McProRelateFile extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**文件名称*/
    @TableField(value = "file_name")
	private String fileName;
    /**文件类型*/
    @TableField(value = "file_type")
	private String fileType;
    /**文件路径*/
    @TableField(value = "file_path")
	private String filePath;
    /**上传者*/
    @TableField(value = "upload_user_name")
	private String uploadUserName = UserUtils.getUser().getRealname();
    /**上传者编号*/
    @TableField(value = "upload_user_id")
	private String uploadUserId = UserUtils.getUser().getId();
    /**上传时间*/
    @TableField(value = "upload_time")
	private Date uploadTime;
    /**项目ID*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  fileName
	 *@return: String  文件名称
	 */
	public String getFileName(){
		return this.fileName;
	}

	/**
	 * 设置  fileName
	 *@param: fileName  文件名称
	 */
	public void setFileName(String fileName){
		this.fileName = fileName;
	}
	/**
	 * 获取  fileType
	 *@return: String  文件类型
	 */
	public String getFileType(){
		return this.fileType;
	}

	/**
	 * 设置  fileType
	 *@param: fileType  文件类型
	 */
	public void setFileType(String fileType){
		this.fileType = fileType;
	}
	/**
	 * 获取  filePath
	 *@return: String  文件路径
	 */
	public String getFilePath(){
		return this.filePath;
	}

	/**
	 * 设置  filePath
	 *@param: filePath  文件路径
	 */
	public void setFilePath(String filePath){
		this.filePath = filePath;
	}
	/**
	 * 获取  uploadUserName
	 *@return: String  上传者
	 */
	public String getUploadUserName(){
		return this.uploadUserName;
	}

	/**
	 * 设置  uploadUserName
	 *@param: uploadUserName  上传者
	 */
	public void setUploadUserName(String uploadUserName){
		this.uploadUserName = uploadUserName;
	}
	/**
	 * 获取  uploadUserId
	 *@return: String  上传者编号
	 */
	public String getUploadUserId(){
		return this.uploadUserId;
	}

	/**
	 * 设置  uploadUserId
	 *@param: uploadUserId  上传者编号
	 */
	public void setUploadUserId(String uploadUserId){
		this.uploadUserId = uploadUserId;
	}
	/**
	 * 获取  uploadTime
	 *@return: Date  上传时间
	 */
	public Date getUploadTime(){
		return this.uploadTime;
	}

	/**
	 * 设置  uploadTime
	 *@param: uploadTime  上传时间
	 */
	public void setUploadTime(Date uploadTime){
		this.uploadTime = uploadTime;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目ID
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目ID
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	
}
