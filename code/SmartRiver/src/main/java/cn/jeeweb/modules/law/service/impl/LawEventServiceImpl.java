package cn.jeeweb.modules.law.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.law.mapper.LawEventMapper;
import cn.jeeweb.modules.law.entity.LawEvent;
import cn.jeeweb.modules.law.service.ILawEventService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 违法事件
 * @Description: 违法事件
 * @author liyonglei
 * @date 2018-11-13 08:40:53
 * @version V1.0   
 *
 */
@Transactional
@Service("lawEventService")
public class LawEventServiceImpl  extends CommonServiceImpl<LawEventMapper,LawEvent> implements  ILawEventService {

}
