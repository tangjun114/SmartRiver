package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectEvaluationMapper;
import cn.jeeweb.modules.project.entity.SrProjectEvaluation;
import cn.jeeweb.modules.project.service.ISrProjectEvaluationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目评比
 * @Description: 项目评比
 * @author wsh
 * @date 2019-03-31 22:54:37
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectEvaluationService")
public class SrProjectEvaluationServiceImpl  extends CommonServiceImpl<SrProjectEvaluationMapper,SrProjectEvaluation> implements  ISrProjectEvaluationService {

}
