package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProRelateFile;

/**   
 * @Title: 项目相关文件
 * @Description: 项目相关文件
 * @author aether
 * @date 2018-01-24 23:54:08
 * @version V1.0   
 *
 */
public interface IMcProRelateFileService extends ICommonService<McProRelateFile> {

}

