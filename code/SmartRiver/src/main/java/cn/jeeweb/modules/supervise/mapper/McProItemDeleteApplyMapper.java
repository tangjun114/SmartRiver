package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemDeleteApply;
 
/**   
 * @Title: 原始文件删除申请数据库控制层接口
 * @Description: 原始文件删除申请数据库控制层接口
 * @author Aether
 * @date 2018-06-05 08:41:22
 * @version V1.0   
 *
 */
public interface McProItemDeleteApplyMapper extends BaseMapper<McProItemDeleteApply> {
    
}