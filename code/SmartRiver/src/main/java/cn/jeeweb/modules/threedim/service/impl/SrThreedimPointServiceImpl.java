package cn.jeeweb.modules.threedim.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.threedim.mapper.SrThreedimPointMapper;
import cn.jeeweb.modules.threedim.entity.SrThreedimPoint;
import cn.jeeweb.modules.threedim.service.ISrThreedimPointService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 实时影像地点
 * @Description: 实时影像地点
 * @author jerry
 * @date 2018-11-27 18:29:20
 * @version V1.0   
 *
 */
@Transactional
@Service("srThreedimPointService")
public class SrThreedimPointServiceImpl  extends CommonServiceImpl<SrThreedimPointMapper,SrThreedimPoint> implements  ISrThreedimPointService {

}
