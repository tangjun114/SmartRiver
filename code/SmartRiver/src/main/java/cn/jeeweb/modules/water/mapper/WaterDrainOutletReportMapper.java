package cn.jeeweb.modules.water.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.water.entity.WaterDrainOutletReport;
 
/**   
 * @Title: 排污口检测报表数据库控制层接口
 * @Description: 排污口检测报表数据库控制层接口
 * @author wsh
 * @date 2019-03-19 19:09:33
 * @version V1.0   
 *
 */
public interface WaterDrainOutletReportMapper extends BaseMapper<WaterDrainOutletReport> {
    
}