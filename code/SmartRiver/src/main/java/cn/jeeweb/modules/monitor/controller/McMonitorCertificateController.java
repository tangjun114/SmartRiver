package cn.jeeweb.modules.monitor.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.enums.SqlLike;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McMonitorCertificate;
import cn.jeeweb.modules.monitor.entity.McMonitorOrg;
import cn.jeeweb.modules.sys.entity.Organization;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 机构证书管理
 * @Description: 机构证书管理
 * @author shawloong
 * @date 2017-10-05 21:50:46
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/monitor/mcmonitorcertificate")
@RequiresPathPermission("monitor:mcmonitorcertificate")
public class McMonitorCertificateController extends McOrgBaseController<McMonitorCertificate, String> {

	
	
	@Override
	public void preEdit(McMonitorCertificate data, Model model, HttpServletRequest request, HttpServletResponse response) {
		//获取组织信息
		String monitorId = getString("monitorId");
		if(!StringUtils.isEmpty(monitorId)){
			McMonitorOrg mcMonitorOrg = new McMonitorOrg();
			mcMonitorOrg.setId(monitorId);
			data.setMonitor(mcMonitorOrg);
			model.addAttribute("data",data);
		}
	}
	@Override
	public void preAjaxList(Queryable queryable,EntityWrapper<McMonitorCertificate> entityWrapper,HttpServletRequest request,
			HttpServletResponse response) {
		// 子查询
		String  monitorId= request.getParameter("monitorId");
		if (!StringUtils.isEmpty(monitorId)) {
			entityWrapper.eq("monitor_id", monitorId);
			
		}
	}
}
