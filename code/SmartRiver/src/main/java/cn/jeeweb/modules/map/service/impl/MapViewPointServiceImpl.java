package cn.jeeweb.modules.map.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.map.mapper.MapViewPointMapper;
import cn.jeeweb.modules.map.entity.MapViewPoint;
import cn.jeeweb.modules.map.service.IMapViewPointService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 观察点管理
 * @Description: 观察点管理
 * @author liyonglei
 * @date 2018-11-14 20:50:41
 * @version V1.0   
 *
 */
@Transactional
@Service("mapViewPointService")
public class MapViewPointServiceImpl  extends CommonServiceImpl<MapViewPointMapper,MapViewPoint> implements  IMapViewPointService {

}
