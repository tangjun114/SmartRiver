package cn.jeeweb.modules.water.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.water.entity.WaterEquipment;
 
/**   
 * @Title: 监测站设备数据库控制层接口
 * @Description: 监测站设备数据库控制层接口
 * @author liyonglei
 * @date 2018-11-13 21:09:06
 * @version V1.0   
 *
 */
public interface WaterEquipmentMapper extends BaseMapper<WaterEquipment> {
    
}