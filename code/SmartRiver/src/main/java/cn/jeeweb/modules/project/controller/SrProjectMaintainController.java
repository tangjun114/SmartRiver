package cn.jeeweb.modules.project.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectMaintain;

/**   
 * @Title: 项目维护
 * @Description: 项目维护
 * @author wsh
 * @date 2018-12-25 20:43:42
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectmaintain")
@RequiresPathPermission("project:srprojectmaintain")
public class SrProjectMaintainController extends BaseCRUDController<SrProjectMaintain, String> {

}
