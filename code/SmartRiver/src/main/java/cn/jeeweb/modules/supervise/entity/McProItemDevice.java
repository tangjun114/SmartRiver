package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 项目与设备管理表
 * @Description: 项目与设备管理表
 * @author jerry
 * @date 2018-01-29 17:01:28
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_device")
@SuppressWarnings("serial")
public class McProItemDevice extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目Id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**设备id*/
    @TableField(value = "device_id")
	private String deviceId;
    /**设备编码*/
    @TableField(value = "code")
	private String code;
    /**设备类型*/
    @TableField(value = "type")
	private String type;
    /**设备类型名称*/
    @TableField(value = "type_name")
	private String typeName;
    /**设备型号*/
    @TableField(value = "unit_type")
	private String unitType;
    /**设备型号名称*/
    @TableField(value = "unit_type_name")
	private String unitTypeName;
    /**是否自动化*/
    @TableField(value = "is_auto")
	private String isAuto;
    /**设备修正*/
    @TableField(value = "name_correction")
	private String nameCorrection;
    /**规格修正*/
    @TableField(value = "spec_revision")
	private String specRevision;
    /**校准日期*/
    @TableField(value = "correcting_date")
	private Date correctingDate;
    /**校准到期日期*/
    @TableField(value = "correcting_end_date")
	private Date correctingEndDate;
    /**使用状态*/
    @TableField(value = "status")
	private String status;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目Id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目Id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  deviceId
	 *@return: String  设备id
	 */
	public String getDeviceId(){
		return this.deviceId;
	}

	/**
	 * 设置  deviceId
	 *@param: deviceId  设备id
	 */
	public void setDeviceId(String deviceId){
		this.deviceId = deviceId;
	}
	/**
	 * 获取  code
	 *@return: String  设备编码
	 */
	public String getCode(){
		return this.code;
	}

	/**
	 * 设置  code
	 *@param: code  设备编码
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 * 获取  type
	 *@return: String  设备类型
	 */
	public String getType(){
		return this.type;
	}

	/**
	 * 设置  type
	 *@param: type  设备类型
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 * 获取  typeName
	 *@return: String  设备类型名称
	 */
	public String getTypeName(){
		return this.typeName;
	}

	/**
	 * 设置  typeName
	 *@param: typeName  设备类型名称
	 */
	public void setTypeName(String typeName){
		this.typeName = typeName;
	}
	/**
	 * 获取  unitType
	 *@return: String  设备型号
	 */
	public String getUnitType(){
		return this.unitType;
	}

	/**
	 * 设置  unitType
	 *@param: unitType  设备型号
	 */
	public void setUnitType(String unitType){
		this.unitType = unitType;
	}
	/**
	 * 获取  unitTypeName
	 *@return: String  设备型号名称
	 */
	public String getUnitTypeName(){
		return this.unitTypeName;
	}

	/**
	 * 设置  unitTypeName
	 *@param: unitTypeName  设备型号名称
	 */
	public void setUnitTypeName(String unitTypeName){
		this.unitTypeName = unitTypeName;
	}
	/**
	 * 获取  isAuto
	 *@return: String  是否自动化
	 */
	public String getIsAuto(){
		return this.isAuto;
	}

	/**
	 * 设置  isAuto
	 *@param: isAuto  是否自动化
	 */
	public void setIsAuto(String isAuto){
		this.isAuto = isAuto;
	}
	/**
	 * 获取  nameCorrection
	 *@return: String  设备修正
	 */
	public String getNameCorrection(){
		return this.nameCorrection;
	}

	/**
	 * 设置  nameCorrection
	 *@param: nameCorrection  设备修正
	 */
	public void setNameCorrection(String nameCorrection){
		this.nameCorrection = nameCorrection;
	}
	/**
	 * 获取  specRevision
	 *@return: String  规格修正
	 */
	public String getSpecRevision(){
		return this.specRevision;
	}

	/**
	 * 设置  specRevision
	 *@param: specRevision  规格修正
	 */
	public void setSpecRevision(String specRevision){
		this.specRevision = specRevision;
	}
	/**
	 * 获取  correctingDate
	 *@return: Date  校准日期
	 */
	public Date getCorrectingDate(){
		return this.correctingDate;
	}

	/**
	 * 设置  correctingDate
	 *@param: correctingDate  校准日期
	 */
	public void setCorrectingDate(Date correctingDate){
		this.correctingDate = correctingDate;
	}
	/**
	 * 获取  correctingEndDate
	 *@return: Date  校准到期日期
	 */
	public Date getCorrectingEndDate(){
		return this.correctingEndDate;
	}

	/**
	 * 设置  correctingEndDate
	 *@param: correctingEndDate  校准到期日期
	 */
	public void setCorrectingEndDate(Date correctingEndDate){
		this.correctingEndDate = correctingEndDate;
	}
	/**
	 * 获取  status
	 *@return: String  使用状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  使用状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	
}
