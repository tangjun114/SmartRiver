package cn.jeeweb.modules.flood.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.FloodWarnLog;

/**   
 * @Title: 报警记录
 * @Description: 报警记录
 * @author wsh
 * @date 2018-12-05 19:47:58
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/floodwarnlog")
@RequiresPathPermission("flood:floodwarnlog")
public class FloodWarnLogController extends BaseCRUDController<FloodWarnLog, String> {

}
