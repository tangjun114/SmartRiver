package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemInspectionMapper;
import cn.jeeweb.modules.supervise.entity.McProItemInspection;
import cn.jeeweb.modules.supervise.service.IMcProItemInspectionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 抽查记录
 * @Description: 抽查记录
 * @author jerry
 * @date 2018-06-04 10:36:48
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemInspectionService")
public class McProItemInspectionServiceImpl  extends CommonServiceImpl<McProItemInspectionMapper,McProItemInspection> implements  IMcProItemInspectionService {

}
