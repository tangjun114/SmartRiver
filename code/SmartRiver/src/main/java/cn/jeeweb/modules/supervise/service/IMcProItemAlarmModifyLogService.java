package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemAlarmModifyLog;

/**   
 * @Title: 报警修改日志
 * @Description: 报警修改日志
 * @author Aether
 * @date 2018-06-16 20:17:34
 * @version V1.0   
 *
 */
public interface IMcProItemAlarmModifyLogService extends ICommonService<McProItemAlarmModifyLog> {

}

