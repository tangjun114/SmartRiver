package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectAccept;
 
/**   
 * @Title: 项目验收数据库控制层接口
 * @Description: 项目验收数据库控制层接口
 * @author wsh
 * @date 2019-03-31 21:56:15
 * @version V1.0   
 *
 */
public interface SrProjectAcceptMapper extends BaseMapper<SrProjectAccept> {
    
}