package cn.jeeweb.modules.sm.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.sm.entity.ProjectType;

/**   
 * @Title: 项目类型
 * @Description: 项目类型
 * @author shawloong
 * @date 2017-09-28 00:43:37
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/sm/projecttype")
@RequiresPathPermission("sm:projecttype")
public class ProjectTypeController extends BaseCRUDController<ProjectType, String> {

}
