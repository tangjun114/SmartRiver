package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectFloodManager;
 
/**   
 * @Title: 工程水务管理数据库控制层接口
 * @Description: 工程水务管理数据库控制层接口
 * @author wsh
 * @date 2018-12-25 19:33:17
 * @version V1.0   
 *
 */
public interface SrProjectFloodManagerMapper extends BaseMapper<SrProjectFloodManager> {
    
}