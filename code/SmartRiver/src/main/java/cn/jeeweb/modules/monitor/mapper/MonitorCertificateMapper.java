package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.MonitorCertificate;
 
/**   
 * @Title: 机构证书管理数据库控制层接口
 * @Description: 机构证书管理数据库控制层接口
 * @author shawloong
 * @date 2017-10-05 21:31:07
 * @version V1.0   
 *
 */
public interface MonitorCertificateMapper extends BaseMapper<MonitorCertificate> {
    
}