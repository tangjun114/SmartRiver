package cn.jeeweb.modules.supervise.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.Wrapper;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.monitor.entity.McMonitorSensor;
import cn.jeeweb.modules.monitor.service.IMcMonitorSensorService;
import cn.jeeweb.modules.oa.entity.OaNotification;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;
import cn.jeeweb.modules.supervise.mapper.McProItemMeasuringPointMapper;
import cn.jeeweb.modules.supervise.service.IMcProItemMeasuringPointService;
import cn.jeeweb.modules.supervise.service.IMcProItemMonitorItemService;

/**   
 * @Title: 项目监控-测点设置-监测项-测点
 * @Description: 项目监控-测点设置-监测项-测点
 * @author shawloong
 * @date 2017-11-21 16:31:17
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemMeasuringPointService")
public class McProItemMeasuringPointServiceImpl  extends CommonServiceImpl<McProItemMeasuringPointMapper,McProItemMeasuringPoint> implements  IMcProItemMeasuringPointService {

	@Autowired
	private IMcMonitorSensorService sensorService;
	
	@Autowired
	private IMcProItemMonitorItemService iMcProItemMonitorItemService;
	
	@Override
	public boolean insert(McProItemMeasuringPoint entity) {
		// TODO Auto-generated method stub
		
		McProItemMonitorItem item = iMcProItemMonitorItemService.selectById(entity.getMonitorItemId());
		 
		entity.setMonitorItemTypeName(item.getGroupTypeName());
		entity.setMonitorItemTypeCode(item.getGroupTypeCode());

		boolean result = super.insert(entity);
		if(result)
		{
			sensorService.setPointInfo(entity);
	 
			iMcProItemMonitorItemService.refresh(entity.getMonitorItemId());
 		}
			
		return result;
	}

	@Override
	public boolean insertOrUpdate(McProItemMeasuringPoint entity) {
		// TODO Auto-generated method stub
		sensorService.setPointInfo(entity);
		return super.insertOrUpdate(entity);
	}

	@Override
	public boolean deleteById(Serializable id) {
		// TODO Auto-generated method stub
	 
		McProItemMeasuringPoint point = this.selectById(id);
  
		boolean result = super.deleteById(id);
		sensorService.clearPointInfo(point);
		iMcProItemMonitorItemService.refresh(point.getMonitorItemId());
  		
		return result;
	}

	@Override
	public boolean deleteBatchIds(List<? extends Serializable> idList) {
		// TODO Auto-generated method stub
		boolean rs = super.deleteBatchIds(idList);
		for(Serializable e :idList)
		{
			String id = (String) e;
			McProItemMeasuringPoint point = this.selectById(id);
			sensorService.clearPointInfo(point);
			iMcProItemMonitorItemService.refresh(point.getMonitorItemId());
		}
		return rs;
	}
	
	

	
}
