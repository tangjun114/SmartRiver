package cn.jeeweb.modules.project.controller;


import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.flood.entity.FloodSiteWarn;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectScheduleDetail;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wsh
 * @version V1.0
 * @Title: 进度拆分管理
 * @Description: 进度拆分管理
 * @date 2018-12-26 23:43:05
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectscheduledetail")
@RequiresPathPermission("project:srprojectschedule")
public class SrProjectScheduleDetailController extends BaseCRUDController<SrProjectScheduleDetail, String> {

    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        String scId = request.getParameter("scId");
        model.addAttribute("scId", scId);
    }


    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<SrProjectScheduleDetail> entityWrapper, HttpServletRequest request,
                            HttpServletResponse response) {
        String scId = request.getParameter("scId");
        queryable.addCondition("scId", scId);
    }

    @Override
    public void preEdit(SrProjectScheduleDetail entity, Model model, HttpServletRequest request, HttpServletResponse response) {
        String scId = request.getParameter("scId");
        model.addAttribute("scId", scId);
    }

}
