package cn.jeeweb.modules.repo.controller;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.monitor.controller.McOrgBaseController;
import cn.jeeweb.modules.repo.entity.McRepoMaterial;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 资料下载
 * @Description: 资料下载
 * @author shawloong
 * @date 2017-10-04 13:26:47
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/repo/mcrepomaterial")
@RequiresPathPermission("repo:mcrepomaterial")
public class McRepoMaterialController extends McOrgBaseController<McRepoMaterial, String> {
	
	@Override
	public void preEdit(McRepoMaterial mcRepoMaterial, Model model, HttpServletRequest request, HttpServletResponse response) {
	}
	
	/**
	 * 保存数据之前
	 * 
	 * @param entity
	 * @param request
	 * @param response
	 */
	@Override
	public void preSave(McRepoMaterial entity, HttpServletRequest request, HttpServletResponse response) {
		if(null==entity.getCreateBy()){
			entity.setCreateBy(UserUtils.getUser());
		}
		
		if (null==entity.getCreateByName()){
			entity.setCreateByName(UserUtils.getUser().getUsername());
		}
		if(null==entity.getCreateDate()){
			entity.setCreateDate(new Date());
		}
		super.preSave(entity, request, response);
	}
}
