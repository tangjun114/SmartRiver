package cn.jeeweb.modules.water.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 监测站设备
 * @Description: 监测站设备
 * @author liyonglei
 * @date 2018-11-13 21:09:06
 * @version V1.0   
 *
 */
@TableName("sr_water_equipment")
@SuppressWarnings("serial")
public class WaterEquipment extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**名称*/
    @TableField(value = "equipment_name")
	private String equipmentName;
    /**参数*/
    @TableField(value = "equipment_param")
	private String equipmentParam;
    /**图片*/
    @TableField(value = "equipment_img")
	private String equipmentImg;
    /**图纸*/
    @TableField(value = "equipment_design_doc")
	private String equipmentDesignDoc;
    /**公式*/
    @TableField(value = "equipment_formula")
	private String equipmentFormula;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  equipmentName
	 *@return: String  名称
	 */
	public String getEquipmentName(){
		return this.equipmentName;
	}

	/**
	 * 设置  equipmentName
	 *@param: equipmentName  名称
	 */
	public void setEquipmentName(String equipmentName){
		this.equipmentName = equipmentName;
	}
	/**
	 * 获取  equipmentParam
	 *@return: String  参数
	 */
	public String getEquipmentParam(){
		return this.equipmentParam;
	}

	/**
	 * 设置  equipmentParam
	 *@param: equipmentParam  参数
	 */
	public void setEquipmentParam(String equipmentParam){
		this.equipmentParam = equipmentParam;
	}
	/**
	 * 获取  equipmentImg
	 *@return: String  图片
	 */
	public String getEquipmentImg(){
		return this.equipmentImg;
	}

	/**
	 * 设置  equipmentImg
	 *@param: equipmentImg  图片
	 */
	public void setEquipmentImg(String equipmentImg){
		this.equipmentImg = equipmentImg;
	}
	/**
	 * 获取  equipmentDesignDoc
	 *@return: String  图纸
	 */
	public String getEquipmentDesignDoc(){
		return this.equipmentDesignDoc;
	}

	/**
	 * 设置  equipmentDesignDoc
	 *@param: equipmentDesignDoc  图纸
	 */
	public void setEquipmentDesignDoc(String equipmentDesignDoc){
		this.equipmentDesignDoc = equipmentDesignDoc;
	}
	/**
	 * 获取  equipmentFormula
	 *@return: String  公式
	 */
	public String getEquipmentFormula(){
		return this.equipmentFormula;
	}

	/**
	 * 设置  equipmentFormula
	 *@param: equipmentFormula  公式
	 */
	public void setEquipmentFormula(String equipmentFormula){
		this.equipmentFormula = equipmentFormula;
	}
	
}
