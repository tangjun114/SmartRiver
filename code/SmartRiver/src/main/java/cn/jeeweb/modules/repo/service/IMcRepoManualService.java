package cn.jeeweb.modules.repo.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.repo.entity.McRepoManual;

/**   
 * @Title: 平台手册 manual
 * @Description: 平台手册 manual
 * @author shawloong
 * @date 2017-10-04 14:28:20
 * @version V1.0   
 *
 */
public interface IMcRepoManualService extends ICommonService<McRepoManual> {

}

