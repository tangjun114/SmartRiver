package cn.jeeweb.modules.repo.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.repo.mapper.McRepoManualMapper;
import cn.jeeweb.modules.repo.entity.McRepoManual;
import cn.jeeweb.modules.repo.service.IMcRepoManualService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;

/**   
 * @Title: 平台手册 manual
 * @Description: 平台手册 manual
 * @author shawloong
 * @date 2017-10-04 14:28:20
 * @version V1.0   
 *
 */
@Transactional
@Service("mcRepoManualService")
public class McRepoManualServiceImpl  extends CommonServiceImpl<McRepoManualMapper,McRepoManual> implements  IMcRepoManualService {
	
	@Override
	public boolean insert(McRepoManual mcRepoManual) {
		// 保存主表
		super.insert(mcRepoManual);
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(McRepoManual mcRepoManual) {
		try {
			// 更新主表
			super.insertOrUpdate(mcRepoManual);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}
	
	
	
}
