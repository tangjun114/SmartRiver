package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemMessage;
 
/**   
 * @Title: 报警短信数据库控制层接口
 * @Description: 报警短信数据库控制层接口
 * @author jrrey
 * @date 2018-05-30 16:34:13
 * @version V1.0   
 *
 */
public interface McProItemMessageMapper extends BaseMapper<McProItemMessage> {
    
}