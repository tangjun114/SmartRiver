package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.flood.mapper.FloodSaveHavenMapper;
import cn.jeeweb.modules.flood.entity.FloodSaveHaven;
import cn.jeeweb.modules.flood.service.IFloodSaveHavenService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 避难中心
 * @Description: 避难中心
 * @author liyonglei
 * @date 2018-11-09 11:19:20
 * @version V1.0   
 *
 */
@Transactional
@Service("floodSaveHavenService")
public class FloodSaveHavenServiceImpl  extends CommonServiceImpl<FloodSaveHavenMapper,FloodSaveHaven> implements  IFloodSaveHavenService {

}
