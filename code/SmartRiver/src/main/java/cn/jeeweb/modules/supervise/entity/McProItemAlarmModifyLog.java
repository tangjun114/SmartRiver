package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 报警修改日志
 * @Description: 报警修改日志
 * @author Aether
 * @date 2018-06-28 08:08:19
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_alarm_modify_log")
@SuppressWarnings("serial")
public class McProItemAlarmModifyLog extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目Id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**机构id*/
    @TableField(value = "monitor_id")
	private String monitorId;
    /**机构名称*/
    @TableField(value = "monitor_name")
	private String monitorName;
    /**测点id*/
    @TableField(value = "point_id")
	private String pointId;
    /**测点名称*/
    @TableField(value = "point_code")
	private String pointCode;
    /**修改描述*/
    @TableField(value = "modify_desp")
	private String modifyDesp;
    /**修改文件说明*/
    @TableField(value = "modify_file_path")
	private String modifyFilePath;
    /**修改文件名称*/
    @TableField(value = "modify_file_name")
	private String modifyFileName;
    /**修改人员*/
    @TableField(value = "modify_user")
	private String modifyUser;
    /**监测项id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目Id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目Id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  monitorId
	 *@return: String  机构id
	 */
	public String getMonitorId(){
		return this.monitorId;
	}

	/**
	 * 设置  monitorId
	 *@param: monitorId  机构id
	 */
	public void setMonitorId(String monitorId){
		this.monitorId = monitorId;
	}
	/**
	 * 获取  monitorName
	 *@return: String  机构名称
	 */
	public String getMonitorName(){
		return this.monitorName;
	}

	/**
	 * 设置  monitorName
	 *@param: monitorName  机构名称
	 */
	public void setMonitorName(String monitorName){
		this.monitorName = monitorName;
	}
	/**
	 * 获取  pointId
	 *@return: String  测点id
	 */
	public String getPointId(){
		return this.pointId;
	}

	/**
	 * 设置  pointId
	 *@param: pointId  测点id
	 */
	public void setPointId(String pointId){
		this.pointId = pointId;
	}
	/**
	 * 获取  pointCode
	 *@return: String  测点名称
	 */
	public String getPointCode(){
		return this.pointCode;
	}

	/**
	 * 设置  pointCode
	 *@param: pointCode  测点名称
	 */
	public void setPointCode(String pointCode){
		this.pointCode = pointCode;
	}
	/**
	 * 获取  modifyDesp
	 *@return: String  修改描述
	 */
	public String getModifyDesp(){
		return this.modifyDesp;
	}

	/**
	 * 设置  modifyDesp
	 *@param: modifyDesp  修改描述
	 */
	public void setModifyDesp(String modifyDesp){
		this.modifyDesp = modifyDesp;
	}
	/**
	 * 获取  modifyFilePath
	 *@return: String  修改文件说明
	 */
	public String getModifyFilePath(){
		return this.modifyFilePath;
	}

	/**
	 * 设置  modifyFilePath
	 *@param: modifyFilePath  修改文件说明
	 */
	public void setModifyFilePath(String modifyFilePath){
		this.modifyFilePath = modifyFilePath;
	}
	/**
	 * 获取  modifyFileName
	 *@return: String  修改文件名称
	 */
	public String getModifyFileName(){
		return this.modifyFileName;
	}

	/**
	 * 设置  modifyFileName
	 *@param: modifyFileName  修改文件名称
	 */
	public void setModifyFileName(String modifyFileName){
		this.modifyFileName = modifyFileName;
	}
	/**
	 * 获取  modifyUser
	 *@return: String  修改人员
	 */
	public String getModifyUser(){
		return this.modifyUser;
	}

	/**
	 * 设置  modifyUser
	 *@param: modifyUser  修改人员
	 */
	public void setModifyUser(String modifyUser){
		this.modifyUser = modifyUser;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	
}
