package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolDetail;

/**   
 * @Title: 巡检详情
 * @Description: 巡检详情
 * @author Aether
 * @date 2018-02-03 11:28:27
 * @version V1.0   
 *
 */
public interface IMcMonitorPatrolDetailService extends ICommonService<McMonitorPatrolDetail> {

}

