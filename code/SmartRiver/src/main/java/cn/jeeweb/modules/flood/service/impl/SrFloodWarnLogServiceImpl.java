package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.flood.mapper.SrFloodWarnLogMapper;
import cn.jeeweb.modules.flood.entity.SrFloodWarnLog;
import cn.jeeweb.modules.flood.service.ISrFloodWarnLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 报警记录
 * @Description: 报警记录
 * @author wsh
 * @date 2019-03-17 14:20:32
 * @version V1.0   
 *
 */
@Transactional
@Service("srFloodWarnLogService")
public class SrFloodWarnLogServiceImpl  extends CommonServiceImpl<SrFloodWarnLogMapper,SrFloodWarnLog> implements  ISrFloodWarnLogService {

}
