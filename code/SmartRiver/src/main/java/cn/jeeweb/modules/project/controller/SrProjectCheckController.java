package cn.jeeweb.modules.project.controller;


import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.project.entity.SrProjectInfoChangeLog;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectCheck;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**   
 * @Title: 项目检查
 * @Description: 项目检查
 * @author wsh
 * @date 2019-03-31 16:40:32
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectcheck")
@RequiresPathPermission("project:srprojectcheck")
public class SrProjectCheckController extends BaseCRUDController<SrProjectCheck, String> {
    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        super.preList(model, request, response);
        String proId = request.getParameter("proId");
        if (StringUtils.isNotEmpty(proId)) {
            model.addAttribute("proId", proId);
        }
    }


    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<SrProjectCheck> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
        super.preAjaxList(queryable, entityWrapper, request, response);
        String proId = request.getParameter("proId");
        if (StringUtils.isNotEmpty(proId)) {
            queryable.addCondition("proId", proId);
        }
        entityWrapper.orderBy("create_date", false);
    }
}
