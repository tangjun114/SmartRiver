package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.modules.constant.FloodStockChangeTypeEnum;
import cn.jeeweb.modules.constant.FloodStockRecordStatusEnum;
import cn.jeeweb.modules.flood.entity.FloodAssetsStockRecord;
import cn.jeeweb.modules.flood.mapper.FloodAssetsStockMapper;
import cn.jeeweb.modules.flood.entity.FloodAssetsStock;
import cn.jeeweb.modules.flood.service.IFloodAssetsStockRecordService;
import cn.jeeweb.modules.flood.service.IFloodAssetsStockService;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.utils.UserUtils;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.ff.common.service.FFException;
import org.apache.log4j.Logger;
import org.jsoup.helper.DataUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;

/**   
 * @Title: 库存管理
 * @Description: 库存管理
 * @author liyonglei
 * @date 2018-11-09 14:42:38
 * @version V1.0   
 *
 */
@Transactional
@Service("floodAssetsStockService")
public class FloodAssetsStockServiceImpl  extends CommonServiceImpl<FloodAssetsStockMapper,FloodAssetsStock> implements  IFloodAssetsStockService {

    protected Logger log = Logger.getLogger(getClass());
    @Autowired
    private IFloodAssetsStockRecordService recordService;

    @Override
    public boolean insertOrUpdate(FloodAssetsStock entity) {
        log.info("flood assets stock check>>>>>>>>> FloodAssetsStockRecord is:"+JSON.toJSON(entity));
        User user = UserUtils.getUser();
        Queryable queryable = QueryRequest.newQueryable();
        queryable.addCondition("id",entity.getId());
        FloodAssetsStockRecord stockRecord1 =  recordService.get(queryable);
        log.info("stock record1 is:"+JSON.toJSON(stockRecord1));
        FloodAssetsStockRecord stockRecord2 = new FloodAssetsStockRecord();
        //审核通过
        if(FloodStockRecordStatusEnum.CHECK_SUCCESS.getValue().equals(entity.getStatus())){
            FloodAssetsStock oldAssetStock = new FloodAssetsStock();
            if(null != stockRecord1){
                Queryable queryableStock = QueryRequest.newQueryable();
                queryableStock.addCondition("assets_code",stockRecord1.getAssetsCode());
                queryableStock.addCondition("stock_location",stockRecord1.getStockLocation());
                oldAssetStock = super.get(queryableStock);
                //入库
                if(FloodStockChangeTypeEnum.IN.getName().equals(stockRecord1.getChangeType())){
                    if(null != oldAssetStock){
                        oldAssetStock.setStockQty(oldAssetStock.getStockQty().add(stockRecord1.getChangeQty()));
                        oldAssetStock.setUpdateDate(new Date());
                        oldAssetStock.setUpdateBy(user.getId());
                        super.updateById(oldAssetStock);
                    }
                    else{
                        FloodAssetsStock oldAssetStock1 = new FloodAssetsStock();
                        oldAssetStock1.setAssetsCode(stockRecord1.getAssetsCode());
                        oldAssetStock1.setAssetsName(stockRecord1.getAssetsName());
                        oldAssetStock1.setStockQty(stockRecord1.getChangeQty());
                        oldAssetStock1.setStockLocation(stockRecord1.getStockLocation());
                        oldAssetStock1.setUnit(stockRecord1.getUnit());
                        log.info("oldAssetStock is:"+JSON.toJSON(oldAssetStock1));
                        super.insert(oldAssetStock1);
                    }
                } else if(FloodStockChangeTypeEnum.OUT.getName().equals(stockRecord1.getChangeType())){
                    //出库
                    if(null == oldAssetStock || stockRecord1.getChangeQty().compareTo(oldAssetStock.getStockQty()) == 1){
                        throw new FFException("库存不足。");
                    }
                    else{
                        oldAssetStock.setStockQty(oldAssetStock.getStockQty().subtract(stockRecord1.getChangeQty()));
                        super.updateAllColumnById(oldAssetStock);
                    }
                }
                else {

                    Queryable queryable2 = QueryRequest.newQueryable();
                    queryable2.addCondition("group_id", stockRecord1.getGroupId());
                    stockRecord2 = recordService.get(queryable2);

                    Queryable queryableAsset = QueryRequest.newQueryable();
                    queryableAsset.addCondition("assets_code", stockRecord2.getAssetsCode());
                    queryableAsset.addCondition("stock_location", stockRecord2.getStockLocation());
                    FloodAssetsStock stock2 = super.get(queryableAsset);
                    //移库
                    if (FloodStockChangeTypeEnum.MOVE_OUT.getName().equals(stockRecord1.getChangeType())) {
                        if (null == oldAssetStock || stockRecord1.getChangeQty().compareTo(oldAssetStock.getStockQty()) == 1) {
                            throw new FFException("库存不足。");
                        } else {
                            oldAssetStock.setStockQty(oldAssetStock.getStockQty().subtract(stockRecord1.getChangeQty()));
                            super.updateAllColumnById(oldAssetStock);

                            stock2.setStockQty(stock2.getStockQty().add(stockRecord2.getChangeQty()));
                            super.updateAllColumnById(stock2);
                        }
                    } else if (FloodStockChangeTypeEnum.MOVE_IN.getName().equals(stockRecord1.getChangeType())) {
                        if (null == stock2 || stockRecord1.getChangeQty().compareTo(stock2.getStockQty()) == 1) {
                            throw new FFException("库存不足。");
                        } else {
                            stock2.setStockQty(oldAssetStock.getStockQty().subtract(stockRecord1.getChangeQty()));
                            super.updateAllColumnById(stock2);

                            oldAssetStock.setStockQty(oldAssetStock.getStockQty().add(stockRecord2.getChangeQty()));
                            super.updateAllColumnById(oldAssetStock);
                        }
                    }
                    stockRecord2.setCheckTime(new Date());
                    stockRecord2.setCheckUserId(user.getId());
                    stockRecord2.setCheckUserName(user.getUsername());
                    stockRecord2.setStatus(FloodStockRecordStatusEnum.CHECK_SUCCESS.getValue());
                    recordService.updateAllColumnById(stockRecord2);
                }
            }
            //更新操作记录状态
            stockRecord1.setStatus(FloodStockRecordStatusEnum.CHECK_SUCCESS.getValue());
        }
        else if(FloodStockRecordStatusEnum.CHECK_FAILED.getValue().equals(entity.getStatus())){               //审核不通过
            //更新操作记录状态
            stockRecord1.setStatus(FloodStockRecordStatusEnum.CHECK_FAILED.getValue());

            stockRecord2.setCheckTime(new Date());
            stockRecord2.setCheckUserId(user.getId());
            stockRecord2.setCheckUserName(user.getUsername());
            stockRecord2.setStatus(FloodStockRecordStatusEnum.CHECK_FAILED.getValue());
            recordService.updateAllColumnById(stockRecord2);
        }
        stockRecord1.setCheckTime(new Date());
        stockRecord1.setCheckUserId(user.getId());
        stockRecord1.setCheckUserName(user.getUsername());
        stockRecord1.setUpdateBy(user.getId());
        stockRecord1.setUpdateDate(new Date());
        recordService.updateById(stockRecord1);
        return true;
    }
}
