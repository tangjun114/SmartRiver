package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemMessage;

/**   
 * @Title: 报警短信
 * @Description: 报警短信
 * @author jrrey
 * @date 2018-05-30 16:34:13
 * @version V1.0   
 *
 */
public interface IMcProItemMessageService extends ICommonService<McProItemMessage> {

}

