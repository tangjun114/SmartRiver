package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodWarnLog;
 
/**   
 * @Title: 报警记录数据库控制层接口
 * @Description: 报警记录数据库控制层接口
 * @author wsh
 * @date 2018-12-05 19:47:58
 * @version V1.0   
 *
 */
public interface FloodWarnLogMapper extends BaseMapper<FloodWarnLog> {
    
}