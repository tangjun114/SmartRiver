package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemOtherDeviceMapper;
import cn.jeeweb.modules.supervise.entity.McProItemOtherDevice;
import cn.jeeweb.modules.supervise.service.IMcProItemOtherDeviceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目中的其他设备
 * @Description: 项目中的其他设备
 * @author jerry
 * @date 2018-01-28 18:52:59
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemOtherDeviceService")
public class McProItemOtherDeviceServiceImpl  extends CommonServiceImpl<McProItemOtherDeviceMapper,McProItemOtherDevice> implements  IMcProItemOtherDeviceService {

}
