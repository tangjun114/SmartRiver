package cn.jeeweb.modules.constant;

/*
 */

public enum FloodStockRecordStatusEnum {

    TO_CHECK("待审核","0"),
    CHECK_SUCCESS("审核通过","1"),
    CHECK_FAILED("审核不通过","2");

    private  String name;
    private String value;

    private FloodStockRecordStatusEnum(String name)
    {
        this.name = name;
    }

    private FloodStockRecordStatusEnum(String name, String value)
    {
        this.name = name;
        this.value = value;

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName()
    {
        return name;
    }
}
