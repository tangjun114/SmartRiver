package cn.jeeweb.modules.project.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 进度拆分管理
 * @Description: 进度拆分管理
 * @author wsh
 * @date 2018-12-26 23:43:05
 * @version V1.0   
 *
 */
@TableName("sr_project_schedule_detail")
@SuppressWarnings("serial")
public class SrProjectScheduleDetail extends AbstractEntity<String> {

    /**主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**父表id*/
    @TableField(value = "sc_id")
	private String scId;
    /**截止日期*/
    @TableField(value = "dead_time")
	private Date deadTime;
    /**描述*/
    @TableField(value = "desc")
	private String desc;
    /**所占比例 %*/
    @TableField(value = "rate")
	private String rate;
    /**完成状态(0:未完成,1完成)*/
    @TableField(value = "complete")
	private Short complete;
	
	/**
	 * 获取  id
	 *@return: String  主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  scId
	 *@return: String  父表id
	 */
	public String getScId(){
		return this.scId;
	}

	/**
	 * 设置  scId
	 *@param: scId  父表id
	 */
	public void setScId(String scId){
		this.scId = scId;
	}
	/**
	 * 获取  deadTime
	 *@return: Date  截止日期
	 */
	public Date getDeadTime(){
		return this.deadTime;
	}

	/**
	 * 设置  deadTime
	 *@param: deadTime  截止日期
	 */
	public void setDeadTime(Date deadTime){
		this.deadTime = deadTime;
	}
	/**
	 * 获取  desc
	 *@return: String  描述
	 */
	public String getDesc(){
		return this.desc;
	}

	/**
	 * 设置  desc
	 *@param: desc  描述
	 */
	public void setDesc(String desc){
		this.desc = desc;
	}
	/**
	 * 获取  rate
	 *@return: String  所占比例 %
	 */
	public String getRate(){
		return this.rate;
	}

	/**
	 * 设置  rate
	 *@param: rate  所占比例 %
	 */
	public void setRate(String rate){
		this.rate = rate;
	}
	/**
	 * 获取  complete
	 *@return: Short  完成状态(0:未完成,1完成)
	 */
	public Short getComplete(){
		return this.complete;
	}

	/**
	 * 设置  complete
	 *@param: complete  完成状态(0:未完成,1完成)
	 */
	public void setComplete(Short complete){
		this.complete = complete;
	}
	
}
