package cn.jeeweb.modules.supervise.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.DataEntity;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 项目监控-测点设置-监测项-测孔
 * @Description: 项目监控-测点设置-监测项-测孔
 * @author shawloong
 * @date 2017-11-15 23:22:45
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_measuring_hole")
@SuppressWarnings("serial")
public class McProItemMeasuringHole extends DataEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**调试模式*/
    @TableField(value = "debug")
	private String debug;
    /**测控编号*/
    @TableField(value = "measuring_point_code")
	private String measuringPointCode;
    /**继承id*/
    @TableField(value = "inherit_id")
	private String inheritId;
    /**继承名*/
    @TableField(value = "inherit_name")
	private String inheritName;
    /**继承*/
    @TableField(value = "inherit_check")
	private String inheriCheck;
    /**监测点类型Val*/
    @TableField(value = "measuring_point_type")
	private String measuringPointType;
    /**监测点类型*/
    @TableField(value = "measuring_point_type_name")
	private String measuringPointTypeName;
    /**监测仪器类型Val*/
    @TableField(value = "instrument_type")
	private String instrumentType;
    /**监测仪器类型*/
    @TableField(value = "instrument_type_name")
	private String instrumentTypeVal;
    /**监测仪器型号VaL*/
    @TableField(value = "instrument_model")
	private String instrumentModel;
    /**监测仪器型号*/
    @TableField(value = "instrument_model_name")
	private String instrumentModelName;
    /**初始值次数*/
    @TableField(value = "Initial_value_times")
	private String InitialValueTimes;
    /**报警值设置类型*/
    @TableField(value = "alarm_value_type")
	private String alarmValueType;
    /**预警值(mm)*/
    @TableField(value = "warning_value")
	private String warningValue;
    /**报警值(mm)*/
    @TableField(value = "alarm_value")
	private String alarmValue;
    /**控制值(mm)*/
    @TableField(value = "control_value")
	private String controlValue;
    /**速率报警值(mm/d)*/
    @TableField(value = "rate_alarm_value")
	private String rateAlarmValue;
    /**报警组选择Val*/
    @TableField(value = "alarm_group")
	private String alarmGroup;
    /**报警组选择*/
    @TableField(value = "alarm_group_name")
	private String alarmGroupName;
    /**断面起点VaL*/
    @TableField(value = "section_start")
	private String sectionStart ;
    /**断面起点*/
    @TableField(value = "section_start_name")
	private String sectionStartName;
    /**断面终点VaL*/
    @TableField(value = "section_end")
	private String sectionEnd;
    /**断面终点*/
    @TableField(value = "section_end_name")
	private String sectionEndName;
    /**断面方向选择*/
    @TableField(value = "direction_selection")
	private String directionSelection;
    /**初始累计值(mm)*/
    @TableField(value = "Initial_cumulative_value")
	private String InitialCumulativeValue;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**监测项目Id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项目名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**深度快速设置区*/
    @TableField(value = "deep_value")
	private String deepValue;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  debug
	 *@return: String  调试模式
	 */
	public String getDebug(){
		return this.debug;
	}

	/**
	 * 设置  debug
	 *@param: debug  调试模式
	 */
	public void setDebug(String debug){
		this.debug = debug;
	}
	/**
	 * 获取  measuringPointCode
	 *@return: String  测控编号
	 */
	public String getMeasuringPointCode(){
		return this.measuringPointCode;
	}

	/**
	 * 设置  measuringPointCode
	 *@param: measuringPointCode  测控编号
	 */
	public void setMeasuringPointCode(String measuringPointCode){
		this.measuringPointCode = measuringPointCode;
	}
	/**
	 * 获取  inheritId
	 *@return: String  继承id
	 */
	public String getInheritId(){
		return this.inheritId;
	}

	/**
	 * 设置  inheritId
	 *@param: inheritId  继承id
	 */
	public void setInheritId(String inheritId){
		this.inheritId = inheritId;
	}
	/**
	 * 获取  inheritName
	 *@return: String  继承名
	 */
	public String getInheritName(){
		return this.inheritName;
	}

	/**
	 * 设置  inheritName
	 *@param: inheritName  继承名
	 */
	public void setInheritName(String inheritName){
		this.inheritName = inheritName;
	}
	/**
	 * 获取  inheriCheck
	 *@return: String  继承
	 */
	public String getInheriCheck(){
		return this.inheriCheck;
	}

	/**
	 * 设置  inheriCheck
	 *@param: inheriCheck  继承
	 */
	public void setInheriCheck(String inheriCheck){
		this.inheriCheck = inheriCheck;
	}
	/**
	 * 获取  measuringPointType
	 *@return: String  监测点类型Val
	 */
	public String getMeasuringPointType(){
		return this.measuringPointType;
	}

	/**
	 * 设置  measuringPointType
	 *@param: measuringPointType  监测点类型Val
	 */
	public void setMeasuringPointType(String measuringPointType){
		this.measuringPointType = measuringPointType;
	}
	/**
	 * 获取  measuringPointTypeName
	 *@return: String  监测点类型
	 */
	public String getMeasuringPointTypeName(){
		return this.measuringPointTypeName;
	}

	/**
	 * 设置  measuringPointTypeName
	 *@param: measuringPointTypeName  监测点类型
	 */
	public void setMeasuringPointTypeName(String measuringPointTypeName){
		this.measuringPointTypeName = measuringPointTypeName;
	}
	/**
	 * 获取  instrumentType
	 *@return: String  监测仪器类型Val
	 */
	public String getInstrumentType(){
		return this.instrumentType;
	}

	/**
	 * 设置  instrumentType
	 *@param: instrumentType  监测仪器类型Val
	 */
	public void setInstrumentType(String instrumentType){
		this.instrumentType = instrumentType;
	}
	/**
	 * 获取  instrumentTypeVal
	 *@return: String  监测仪器类型
	 */
	public String getInstrumentTypeVal(){
		return this.instrumentTypeVal;
	}

	/**
	 * 设置  instrumentTypeVal
	 *@param: instrumentTypeVal  监测仪器类型
	 */
	public void setInstrumentTypeVal(String instrumentTypeVal){
		this.instrumentTypeVal = instrumentTypeVal;
	}
	/**
	 * 获取  instrumentModel
	 *@return: String  监测仪器型号VaL
	 */
	public String getInstrumentModel(){
		return this.instrumentModel;
	}

	/**
	 * 设置  instrumentModel
	 *@param: instrumentModel  监测仪器型号VaL
	 */
	public void setInstrumentModel(String instrumentModel){
		this.instrumentModel = instrumentModel;
	}
	/**
	 * 获取  instrumentModelName
	 *@return: String  监测仪器型号
	 */
	public String getInstrumentModelName(){
		return this.instrumentModelName;
	}

	/**
	 * 设置  instrumentModelName
	 *@param: instrumentModelName  监测仪器型号
	 */
	public void setInstrumentModelName(String instrumentModelName){
		this.instrumentModelName = instrumentModelName;
	}
	/**
	 * 获取  InitialValueTimes
	 *@return: String  初始值次数
	 */
	public String getInitialValueTimes(){
		return this.InitialValueTimes;
	}

	/**
	 * 设置  InitialValueTimes
	 *@param: InitialValueTimes  初始值次数
	 */
	public void setInitialValueTimes(String InitialValueTimes){
		this.InitialValueTimes = InitialValueTimes;
	}
	/**
	 * 获取  alarmValueType
	 *@return: String  报警值设置类型
	 */
	public String getAlarmValueType(){
		return this.alarmValueType;
	}

	/**
	 * 设置  alarmValueType
	 *@param: alarmValueType  报警值设置类型
	 */
	public void setAlarmValueType(String alarmValueType){
		this.alarmValueType = alarmValueType;
	}
	/**
	 * 获取  warningValue
	 *@return: String  预警值(mm)
	 */
	public String getWarningValue(){
		return this.warningValue;
	}

	/**
	 * 设置  warningValue
	 *@param: warningValue  预警值(mm)
	 */
	public void setWarningValue(String warningValue){
		this.warningValue = warningValue;
	}
	/**
	 * 获取  alarmValue
	 *@return: String  报警值(mm)
	 */
	public String getAlarmValue(){
		return this.alarmValue;
	}

	/**
	 * 设置  alarmValue
	 *@param: alarmValue  报警值(mm)
	 */
	public void setAlarmValue(String alarmValue){
		this.alarmValue = alarmValue;
	}
	/**
	 * 获取  controlValue
	 *@return: String  控制值(mm)
	 */
	public String getControlValue(){
		return this.controlValue;
	}

	/**
	 * 设置  controlValue
	 *@param: controlValue  控制值(mm)
	 */
	public void setControlValue(String controlValue){
		this.controlValue = controlValue;
	}
	/**
	 * 获取  rateAlarmValue
	 *@return: String  速率报警值(mm/d)
	 */
	public String getRateAlarmValue(){
		return this.rateAlarmValue;
	}

	/**
	 * 设置  rateAlarmValue
	 *@param: rateAlarmValue  速率报警值(mm/d)
	 */
	public void setRateAlarmValue(String rateAlarmValue){
		this.rateAlarmValue = rateAlarmValue;
	}
	/**
	 * 获取  alarmGroup
	 *@return: String  报警组选择Val
	 */
	public String getAlarmGroup(){
		return this.alarmGroup;
	}

	/**
	 * 设置  alarmGroup
	 *@param: alarmGroup  报警组选择Val
	 */
	public void setAlarmGroup(String alarmGroup){
		this.alarmGroup = alarmGroup;
	}
	/**
	 * 获取  alarmGroupName
	 *@return: String  报警组选择
	 */
	public String getAlarmGroupName(){
		return this.alarmGroupName;
	}

	/**
	 * 设置  alarmGroupName
	 *@param: alarmGroupName  报警组选择
	 */
	public void setAlarmGroupName(String alarmGroupName){
		this.alarmGroupName = alarmGroupName;
	}
	/**
	 * 获取  sectionStart 
	 *@return: String  断面起点VaL
	 */
	public String getSectionStart (){
		return this.sectionStart ;
	}

	/**
	 * 设置  sectionStart 
	 *@param: sectionStart   断面起点VaL
	 */
	public void setSectionStart (String sectionStart ){
		this.sectionStart  = sectionStart ;
	}
	/**
	 * 获取  sectionStartName
	 *@return: String  断面起点
	 */
	public String getSectionStartName(){
		return this.sectionStartName;
	}

	/**
	 * 设置  sectionStartName
	 *@param: sectionStartName  断面起点
	 */
	public void setSectionStartName(String sectionStartName){
		this.sectionStartName = sectionStartName;
	}
	/**
	 * 获取  sectionEnd
	 *@return: String  断面终点VaL
	 */
	public String getSectionEnd(){
		return this.sectionEnd;
	}

	/**
	 * 设置  sectionEnd
	 *@param: sectionEnd  断面终点VaL
	 */
	public void setSectionEnd(String sectionEnd){
		this.sectionEnd = sectionEnd;
	}
	/**
	 * 获取  sectionEndName
	 *@return: String  断面终点
	 */
	public String getSectionEndName(){
		return this.sectionEndName;
	}

	/**
	 * 设置  sectionEndName
	 *@param: sectionEndName  断面终点
	 */
	public void setSectionEndName(String sectionEndName){
		this.sectionEndName = sectionEndName;
	}
	/**
	 * 获取  directionSelection
	 *@return: String  断面方向选择
	 */
	public String getDirectionSelection(){
		return this.directionSelection;
	}

	/**
	 * 设置  directionSelection
	 *@param: directionSelection  断面方向选择
	 */
	public void setDirectionSelection(String directionSelection){
		this.directionSelection = directionSelection;
	}
	/**
	 * 获取  InitialCumulativeValue
	 *@return: String  初始累计值(mm)
	 */
	public String getInitialCumulativeValue(){
		return this.InitialCumulativeValue;
	}

	/**
	 * 设置  InitialCumulativeValue
	 *@param: InitialCumulativeValue  初始累计值(mm)
	 */
	public void setInitialCumulativeValue(String InitialCumulativeValue){
		this.InitialCumulativeValue = InitialCumulativeValue;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项目Id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项目Id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项目名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项目名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  deepValue
	 *@return: String  深度快速设置区
	 */
	public String getDeepValue(){
		return this.deepValue;
	}

	/**
	 * 设置  deepValue
	 *@param: deepValue  深度快速设置区
	 */
	public void setDeepValue(String deepValue){
		this.deepValue = deepValue;
	}
	
}
