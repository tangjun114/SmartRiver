package cn.jeeweb.modules.supervise.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProItemSceneImg;

/**   
 * @Title: 现场照片
 * @Description: 现场照片
 * @author shawloong
 * @date 2017-11-02 21:37:47
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemsceneimg")
@RequiresPathPermission("supervise:mcmonitorresultstatistics")
public class McProItemSceneImgController extends McProjectBaseController<McProItemSceneImg, String> {
	
	@RequestMapping("/datalist")
	public String showDataList(Model model, HttpServletRequest request,
			HttpServletResponse response) {

		return display("datalist");
	}
 
}
