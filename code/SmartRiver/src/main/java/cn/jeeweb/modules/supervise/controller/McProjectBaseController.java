package cn.jeeweb.modules.supervise.controller;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.ff.common.util.meta.ReflectionUtil;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.common.entity.AbstractEntity;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProjectService;

public class McProjectBaseController<Entity extends AbstractEntity<ID>, ID extends Serializable> extends BaseCRUDController<Entity,ID>
{

	@Autowired
	IMcProjectService projectService;
	
	@Override
	public void preAjaxList(Queryable queryable,EntityWrapper<Entity> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
		String  projectId = request.getParameter("projectId");
		if(!StringUtils.isEmpty(projectId)){
			entityWrapper.eq("projectId", projectId);
			
		}
		entityWrapper.orderBy("createDate", false);
	}
	
	@Override
	public void preSave(Entity entity, HttpServletRequest request, HttpServletResponse response) {
		String projectId = request.getParameter("projectId");
		
		McProject project = projectService.selectById(projectId);
		if (null != project) {
			ReflectionUtil.setObjectField(entity, "projectId", project.getId());
			ReflectionUtil.setObjectField(entity, "projectName", project.getName());
			ReflectionUtil.setObjectField(entity, "projectCode", project.getCode());

		}
	}
}
