package cn.jeeweb.modules.sys.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.sys.entity.Role;
import cn.jeeweb.modules.sys.entity.RoleMenu;
import cn.jeeweb.modules.sys.mapper.RoleMapper;
import cn.jeeweb.modules.sys.service.IRoleMenuService;
import cn.jeeweb.modules.sys.service.IRoleService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.ff.common.util.validate.ValidatorUtil;

@Transactional
@Service("roleService")
public class RoleServiceImpl extends CommonServiceImpl<RoleMapper, Role> implements IRoleService {

	@Override
	public List<Role> findListByUserId(String userid) {
		return baseMapper.findRoleByUserId(userid);
	}

	@Autowired
	private IRoleMenuService roleMenuService;
	@Override
	public boolean insert(Role entity) {

		boolean r = super.insert(entity);
		if(!ValidatorUtil.isEmpty(entity.getRemarks()))
		{
			Queryable queryable = QueryRequest.newQueryable();
			queryable.addCondition("name", entity.getRemarks());
			List<Role> roleList = this.listWithNoPage(queryable);
			if(!ValidatorUtil.isEmpty(roleList))
			{
				Wrapper<RoleMenu> wrapper = new EntityWrapper<>();
				wrapper.eq("role_id", roleList.get(0).getId());
				List<RoleMenu> menus = roleMenuService.selectList(wrapper );
				for(RoleMenu m : menus)
				{
					m.setRoleId(entity.getId());
				}
				roleMenuService.insertOrUpdateBatch(menus);

			}
		}

		return r;
	}
	
	

}
