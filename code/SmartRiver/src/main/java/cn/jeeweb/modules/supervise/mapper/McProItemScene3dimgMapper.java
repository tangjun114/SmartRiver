package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemScene3dimg;
 
/**   
 * @Title: 项目监控-项目三维图数据库控制层接口
 * @Description: 项目监控-项目三维图数据库控制层接口
 * @author shawloong
 * @date 2017-11-02 21:39:34
 * @version V1.0   
 *
 */
public interface McProItemScene3dimgMapper extends BaseMapper<McProItemScene3dimg> {
    
}