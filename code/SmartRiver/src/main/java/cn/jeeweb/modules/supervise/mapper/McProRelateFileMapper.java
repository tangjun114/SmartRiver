package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProRelateFile;
 
/**   
 * @Title: 项目相关文件数据库控制层接口
 * @Description: 项目相关文件数据库控制层接口
 * @author aether
 * @date 2018-01-24 23:54:08
 * @version V1.0   
 *
 */
public interface McProRelateFileMapper extends BaseMapper<McProRelateFile> {
    
}