package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodSaveHaven;

/**   
 * @Title: 避难中心
 * @Description: 避难中心
 * @author liyonglei
 * @date 2018-11-09 11:19:20
 * @version V1.0   
 *
 */
public interface IFloodSaveHavenService extends ICommonService<FloodSaveHaven> {

}

