package cn.jeeweb.modules.water.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.water.mapper.WaterDetectResultMapper;
import cn.jeeweb.modules.water.entity.WaterDetectResult;
import cn.jeeweb.modules.water.service.IWaterDetectResultService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 检测数据
 * @Description: 检测数据
 * @author shawloong
 * @date 2018-11-13 21:18:03
 * @version V1.0   
 *
 */
@Transactional
@Service("waterDetectResultService")
public class WaterDetectResultServiceImpl  extends CommonServiceImpl<WaterDetectResultMapper,WaterDetectResult> implements  IWaterDetectResultService {

}
