package cn.jeeweb.modules.law.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.law.entity.LawCheckResult;

/**   
 * @Title: 巡查结果
 * @Description: 巡查结果
 * @author liyonglei
 * @date 2018-11-13 20:40:25
 * @version V1.0   
 *
 */
public interface ILawCheckResultService extends ICommonService<LawCheckResult> {

}

