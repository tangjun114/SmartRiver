package cn.jeeweb.modules.flood.client.floodinfoapi;

import cn.jeeweb.modules.flood.client.GetFloodInfoClient;
import cn.jeeweb.modules.flood.client.entity.Conditions;
import cn.jeeweb.modules.flood.client.entity.Criterias;
import cn.jeeweb.modules.flood.client.entity.ReqParam;

import java.util.*;

/**
 * @Author:Wish
 * @Date: Created in 2018/12/14 10:08
 **/
public class FloodWaterLevelClient extends GetFloodInfoClient {

    private static String interfaceName = "sTRIVERRApi.query";


    private Map<String, Object> getMap(String STCD, String TM_12_time1, String TM_12_time2, String time8, String orders) {
        Conditions conditions = new Conditions();
        Criterias criterias = new Criterias();
        criterias.setStcd(STCD);
        List<String> TM12List = new ArrayList<>();
        TM12List.add(TM_12_time1);
        TM12List.add(TM_12_time2);
        criterias.setTm_12(TM12List);
        criterias.setTm_8(time8);
        conditions.setCriterias(criterias);
        conditions.setOrders(Arrays.asList(orders.split(",")));
        Map<String, Object> map = new HashMap<>();
        map.put("conditions", conditions);
        return map;
    }


    public FloodWaterLevelClient setReqParam(String STCD, String TM_12_time1, String TM_12_time2, String time8, String orders) {
        ReqParam reqParam = new ReqParam();
        reqParam.setInterfaceName(interfaceName);
        reqParam.setParams(getMap(STCD, TM_12_time1, TM_12_time2, time8, orders));
        reqParam.setToken(token);
        reqParams.add(reqParam);
        return this;
    }

    public static void main(String[] args) {
        GetFloodInfoClient client = new FloodWaterLevelClient()
                .setReqParam("81202701", "2018-11-12 08:00:00", "2018-11-27 17:00:00", "00", "TM");
        System.out.println(client.getFloodInfo());

    }
}
