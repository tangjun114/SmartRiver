package cn.jeeweb.modules.supervise.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.supervise.entity.McProItemMember;
import cn.jeeweb.modules.sys.entity.Dict;
import cn.jeeweb.modules.sys.utils.DictUtils;

/**   
 * @Title: 项目人员
 * @Description: 项目人员
 * @author Jerry
 * @date 2017-12-27 13:40:21
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemmember")
@RequiresPathPermission("supervise:mcproitemmember")
public class McProItemMemberController extends McProjectBaseController<McProItemMember, String> {
	
	
	@RequestMapping("/loadMemberType")
	@ResponseBody
	public BaseRspJson<List<Dict>> LoadMemberType(@RequestBody BaseReqJson request)
	{
		BaseRspJson<List<Dict>> rsp = new BaseRspJson<List<Dict>>();
		Queryable queryable = this.GetFilterCondition(request);
		List<Dict> obj = DictUtils.getDictList("projectMemberType");
		rsp.setObj(obj);
  		return rsp;
	}
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProItemMember> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
		String  newProjectCount = request.getParameter("type");
		if(!StringUtils.isEmpty(newProjectCount)){
			entityWrapper.eq("type", newProjectCount);
 		}
		super.preAjaxList(queryable, entityWrapper, request, response);
	}
	@Override
	public void preSave(McProItemMember entity, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		if(null == entity.getDataPerm())
		{
			entity.setDataPerm("");
		}
		if(null == entity.getMsgPerm())
		{
			entity.setMsgPerm("");
		}
		if(null == entity.getReportPerm())
		{
			entity.setReportPerm("");
		}
 
		super.preSave(entity, request, response);
	}

}
