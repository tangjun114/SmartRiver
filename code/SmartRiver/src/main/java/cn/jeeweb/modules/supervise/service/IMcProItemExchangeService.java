package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemExchange;

/**   
 * @Title: 项目沟通交流
 * @Description: 项目沟通交流
 * @author Aether
 * @date 2018-04-28 13:26:54
 * @version V1.0   
 *
 */
public interface IMcProItemExchangeService extends ICommonService<McProItemExchange> {

}

