package cn.jeeweb.modules.video.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.video.entity.SrVideoPoint;

/**   
 * @Title: 视频站点管理
 * @Description: 视频站点管理
 * @author wsh
 * @date 2019-04-01 14:30:31
 * @version V1.0   
 *
 */
public interface ISrVideoPointService extends ICommonService<SrVideoPoint> {

}

