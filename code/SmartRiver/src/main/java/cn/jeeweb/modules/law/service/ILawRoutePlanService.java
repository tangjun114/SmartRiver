package cn.jeeweb.modules.law.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.law.entity.LawRoutePlan;

/**   
 * @Title: 路线规划
 * @Description: 路线规划
 * @author 李永雷
 * @date 2018-11-13 09:08:03
 * @version V1.0   
 *
 */
public interface ILawRoutePlanService extends ICommonService<LawRoutePlan> {

}

