package cn.jeeweb.modules.repo.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 规范资料
 * @Description: 规范资料
 * @author shawloong
 * @date 2018-03-08 10:06:43
 * @version V1.0   
 *
 */
@TableName("mc_repo_standard")
@SuppressWarnings("serial")
public class McRepoStandard extends AbstractEntity<String> {

    /**id*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**规范编号*/
    @TableField(value = "standard_code")
	private String standardCode;
    /**规范名称*/
    @TableField(value = "name")
	private String name;
    /**上传人*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建日期*/
    @TableField(value = "create_date")
	private Date createDate;
    /**文件*/
    @TableField(value = "path")
	private String path;
    /**上传人*/
    @TableField(value = "create_by_name")
	private String createByName;
    /**更新日期*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**组织ID*/
    @TableField(value = "monitor_id")
	private String monitorId;
	
	/**
	 * 获取  id
	 *@return: String  id
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  standardCode
	 *@return: String  规范编号
	 */
	public String getStandardCode(){
		return this.standardCode;
	}

	/**
	 * 设置  standardCode
	 *@param: standardCode  规范编号
	 */
	public void setStandardCode(String standardCode){
		this.standardCode = standardCode;
	}
	/**
	 * 获取  name
	 *@return: String  规范名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  规范名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  createBy
	 *@return: User  上传人
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  上传人
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建日期
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建日期
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  path
	 *@return: String  文件
	 */
	public String getPath(){
		return this.path;
	}

	/**
	 * 设置  path
	 *@param: path  文件
	 */
	public void setPath(String path){
		this.path = path;
	}
	/**
	 * 获取  createByName
	 *@return: String  上传人
	 */
	public String getCreateByName(){
		return this.createByName;
	}

	/**
	 * 设置  createByName
	 *@param: createByName  上传人
	 */
	public void setCreateByName(String createByName){
		this.createByName = createByName;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新日期
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新日期
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  monitorId
	 *@return: String  组织ID
	 */
	public String getMonitorId(){
		return this.monitorId;
	}

	/**
	 * 设置  monitorId
	 *@param: monitorId  组织ID
	 */
	public void setMonitorId(String monitorId){
		this.monitorId = monitorId;
	}
	
}