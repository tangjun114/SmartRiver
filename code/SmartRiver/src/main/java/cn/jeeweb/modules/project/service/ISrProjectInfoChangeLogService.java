package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectInfoChangeLog;

/**   
 * @Title: 工程信息变动调整记录
 * @Description: 工程信息变动调整记录
 * @author wsh
 * @date 2019-03-31 14:34:53
 * @version V1.0   
 *
 */
public interface ISrProjectInfoChangeLogService extends ICommonService<SrProjectInfoChangeLog> {

}

