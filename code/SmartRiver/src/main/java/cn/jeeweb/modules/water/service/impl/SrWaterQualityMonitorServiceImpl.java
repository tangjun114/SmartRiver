package cn.jeeweb.modules.water.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.water.mapper.SrWaterQualityMonitorMapper;
import cn.jeeweb.modules.water.entity.SrWaterQualityMonitor;
import cn.jeeweb.modules.water.service.ISrWaterQualityMonitorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 水质监测
 * @Description: 水质监测
 * @author wsh
 * @date 2019-01-23 17:15:42
 * @version V1.0   
 *
 */
@Transactional
@Service("srWaterQualityMonitorService")
public class SrWaterQualityMonitorServiceImpl  extends CommonServiceImpl<SrWaterQualityMonitorMapper,SrWaterQualityMonitor> implements  ISrWaterQualityMonitorService {

}
