package cn.jeeweb.modules.sys.service.impl;

import cn.jeeweb.core.common.service.impl.TreeCommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.monitor.entity.McMonitorOrg;
import cn.jeeweb.modules.sys.entity.Organization;
import cn.jeeweb.modules.sys.mapper.OrganizationMapper;
import cn.jeeweb.modules.sys.service.IOrganizationService;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("organizationService")
public class OrganizationServiceImpl extends TreeCommonServiceImpl<OrganizationMapper, Organization, String>
		implements IOrganizationService {

	@Override
	public List<Organization> findListByUserId(String userid) {
		return baseMapper.findListByUserId(userid);
	}

	@Override
	public List<Organization> getSubOrg(String org) {
		EntityWrapper<Organization> wrapper = new EntityWrapper<>(Organization.class);
		wrapper.like("parent_ids", org+"%");
		List<Organization> dataList = this.selectList(wrapper);
		return dataList;
	}
}
