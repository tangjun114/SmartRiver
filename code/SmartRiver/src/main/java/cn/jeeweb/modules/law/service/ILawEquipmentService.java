package cn.jeeweb.modules.law.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.law.entity.LawEquipment;

/**   
 * @Title: 设备管理
 * @Description: 设备管理
 * @author liyonglei
 * @date 2018-11-13 08:50:52
 * @version V1.0   
 *
 */
public interface ILawEquipmentService extends ICommonService<LawEquipment> {

}

