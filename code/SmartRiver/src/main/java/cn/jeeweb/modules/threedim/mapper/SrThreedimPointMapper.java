package cn.jeeweb.modules.threedim.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.threedim.entity.SrThreedimPoint;
 
/**   
 * @Title: 实时影像地点数据库控制层接口
 * @Description: 实时影像地点数据库控制层接口
 * @author jerry
 * @date 2018-11-27 18:29:20
 * @version V1.0   
 *
 */
public interface SrThreedimPointMapper extends BaseMapper<SrThreedimPoint> {
    
}