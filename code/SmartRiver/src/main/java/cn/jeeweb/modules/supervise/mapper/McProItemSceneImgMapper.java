package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemSceneImg;
 
/**   
 * @Title: 现场照片数据库控制层接口
 * @Description: 现场照片数据库控制层接口
 * @author shawloong
 * @date 2017-11-02 21:37:47
 * @version V1.0   
 *
 */
public interface McProItemSceneImgMapper extends BaseMapper<McProItemSceneImg> {
    
}