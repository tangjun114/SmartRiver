package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 项目沟通交流
 * @Description: 项目沟通交流
 * @author Aether
 * @date 2018-04-28 13:26:54
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_exchange")
@SuppressWarnings("serial")
public class McProItemExchange extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**创建者名称*/
    @TableField(value = "create_by_name")
	private String createByName;
    /**可见类型*/
    @TableField(value = "visible_type")
	private String visibleType;
    /**内容类型*/
    @TableField(value = "info_type")
	private String infoType;
    /**附件路径*/
    @TableField(value = "file_path")
	private String filePath;
    /**图片路径*/
    @TableField(value = "img_path")
	private String imgPath;
    /**交流内容*/
    @TableField(value = "content")
	private String content;
    /**指定人id*/
    @TableField(value = "to_user_id")
	private String toUserId;
    /**指定人名称*/
    @TableField(value = "to_user_name")
	private String toUserName;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  createByName
	 *@return: String  创建者名称
	 */
	public String getCreateByName(){
		return this.createByName;
	}

	/**
	 * 设置  createByName
	 *@param: createByName  创建者名称
	 */
	public void setCreateByName(String createByName){
		this.createByName = createByName;
	}
	/**
	 * 获取  visibleType
	 *@return: String  可见类型
	 */
	public String getVisibleType(){
		return this.visibleType;
	}

	/**
	 * 设置  visibleType
	 *@param: visibleType  可见类型
	 */
	public void setVisibleType(String visibleType){
		this.visibleType = visibleType;
	}
	/**
	 * 获取  infoType
	 *@return: String  内容类型
	 */
	public String getInfoType(){
		return this.infoType;
	}

	/**
	 * 设置  infoType
	 *@param: infoType  内容类型
	 */
	public void setInfoType(String infoType){
		this.infoType = infoType;
	}
	/**
	 * 获取  filePath
	 *@return: String  附件路径
	 */
	public String getFilePath(){
		return this.filePath;
	}

	/**
	 * 设置  filePath
	 *@param: filePath  附件路径
	 */
	public void setFilePath(String filePath){
		this.filePath = filePath;
	}
	/**
	 * 获取  imgPath
	 *@return: String  图片路径
	 */
	public String getImgPath(){
		return this.imgPath;
	}

	/**
	 * 设置  imgPath
	 *@param: imgPath  图片路径
	 */
	public void setImgPath(String imgPath){
		this.imgPath = imgPath;
	}
	/**
	 * 获取  content
	 *@return: String  交流内容
	 */
	public String getContent(){
		return this.content;
	}

	/**
	 * 设置  content
	 *@param: content  交流内容
	 */
	public void setContent(String content){
		this.content = content;
	}
	/**
	 * 获取  toUserId
	 *@return: String  指定人id
	 */
	public String getToUserId(){
		return this.toUserId;
	}

	/**
	 * 设置  toUserId
	 *@param: toUserId  指定人id
	 */
	public void setToUserId(String toUserId){
		this.toUserId = toUserId;
	}
	/**
	 * 获取  toUserName
	 *@return: String  指定人名称
	 */
	public String getToUserName(){
		return this.toUserName;
	}

	/**
	 * 设置  toUserName
	 *@param: toUserName  指定人名称
	 */
	public void setToUserName(String toUserName){
		this.toUserName = toUserName;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	
}
