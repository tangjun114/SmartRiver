package cn.jeeweb.modules.supervise.controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.ff.bi.model.RptDimDataJson;
import com.ff.common.util.format.JsonConvert;
import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.util.validate.ValidatorUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.model.PageJson;
import cn.jeeweb.core.query.annotation.PageableDefaults;
import cn.jeeweb.core.query.data.PropertyPreFilterable;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort.Direction;
import cn.jeeweb.core.query.parse.QueryToWrapper;
import cn.jeeweb.core.query.utils.QueryableConvertUtils;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.constant.DataResultConst;
import cn.jeeweb.modules.monitor.entity.McProItemAlarm;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;
import cn.jeeweb.modules.supervise.model.SumDataModel;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeMonDataService;

/**   
 * @Title: 项目监控-监测情况-实时监测数据
 * @Description: 项目监控-监测情况-实时监测数据
 * @author shawloong
 * @date 2017-11-07 22:21:56
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemrealtimemondata")
@RequiresPathPermission("supervise:mcproitemrealtimemondata")
public class McProItemRealTimeMonDataController extends McProjectBaseController<McProItemRealTimeMonData, String> {
 
	@Autowired
	private IMcProItemRealTimeMonDataService iMcProItemRealTimeMonDataService;
	@Override
	public Queryable GetFilterCondition(BaseReqJson request) {
		// TODO Auto-generated method stub
		Queryable qy = super.GetFilterCondition(request);
		qy.addOrder(Direction.DESC, "collectTime");
		return qy;
	}
	@Override
	public void preAjaxList(Queryable queryable,EntityWrapper<McProItemRealTimeMonData> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
		
		entityWrapper.orderBy("measuringPointCode", true);
		super.preAjaxList(queryable, entityWrapper, request, response);
		//entityWrapper.eq("data_result", DataResultConst.use);
		
 
	}
	
	@RequestMapping("/sum")
	@ResponseBody
	public BaseRspJson<List<SumDataModel>> getSum(@RequestBody BaseReqJson<McProItemRealTimeMonData> request)
	{
		BaseRspJson<List<SumDataModel>> rsp = new BaseRspJson<List<SumDataModel>>();
 
		McProItemRealTimeMonData obj = this.getObj(request,McProItemRealTimeMonData.class);
		String count = null;
		if(null != obj.getProjectCount())
		{
			count = String.valueOf(obj.getProjectCount());
		}
   		List<SumDataModel> e = iMcProItemRealTimeMonDataService.getSumData(obj.getProjectId(),obj.getMonitorItemId(),count);
		rsp.setObj(e);
   		return rsp;
	}
	
	@RequestMapping("/project/count")
	@ResponseBody
	public BaseRspJson<List<McProItemRealTimeMonData>> getProjectCount(@RequestBody BaseReqJson<McProItemRealTimeMonData> request)
	{
		BaseRspJson<List<McProItemRealTimeMonData>> rsp = new BaseRspJson<List<McProItemRealTimeMonData>>();
 
  		Queryable qy = this.GetFilterCondition(request);
		EntityWrapper<McProItemRealTimeMonData> entityWrapper = new EntityWrapper<McProItemRealTimeMonData>(McProItemRealTimeMonData.class);
		
		QueryToWrapper<McProItemRealTimeMonData> queryToWrapper = new QueryToWrapper<McProItemRealTimeMonData>();
		queryToWrapper.parseCondition(entityWrapper, qy);
		entityWrapper.setSqlSelect("distinct project_count as projectCount");
		entityWrapper.orderBy("project_count", false);
		 
  		List<McProItemRealTimeMonData> dataList = iMcProItemRealTimeMonDataService.selectList(entityWrapper);
		rsp.setObj(dataList);
   		return rsp;
	}
	@RequestMapping(value = "detail/ajaxList", method = { RequestMethod.GET, RequestMethod.POST })
	@PageableDefaults(sort = "id=desc")
	private void ajaxList(Queryable queryable, PropertyPreFilterable propertyPreFilterable, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		EntityWrapper<McProItemRealTimeMonData> entityWrapper = new EntityWrapper<McProItemRealTimeMonData>(entityClass);
		preAjaxList(queryable,entityWrapper, request, response);
		propertyPreFilterable.addQueryProperty("id");
		// 预处理
		QueryableConvertUtils.convertQueryValueToEntityValue(queryable, entityClass);
		SerializeFilter filter = propertyPreFilterable.constructFilter(entityClass);
	    String id = request.getParameter("id");
	    
		PageJson<McProItemRealTimeMonData> pagejson = new PageJson<McProItemRealTimeMonData>();

	    McProItemRealTimeMonData obj = commonService.selectById(id);
	    if(null != id)
	    {
	    	List<McProItemRealTimeMonData> dataList = JsonConvert.jsonToObject(obj.getDepthExt(), List.class,McProItemRealTimeMonData.class);
	    	if(!ValidatorUtil.isEmpty(dataList))
	    	{
	    		pagejson.setResults(dataList);
	    		pagejson.setPage(1);
	    		pagejson.setRows(dataList.size());
	    		pagejson.setTotal(dataList.size());
	    	}
	    }
	    
		String content = JSON.toJSONString(pagejson, filter);
		StringUtils.printJson(response, content);
	}
	
	@RequestMapping("/detail/chart")
	@ResponseBody
 	public BaseRspJson chartData(@RequestBody BaseReqJson<String> request) {
		// TODO Auto-generated method stub
		BaseRspJson<RptDimDataJson> rsp = new BaseRspJson<RptDimDataJson>();
		
		String id = this.getObj(request, String.class);
		McProItemRealTimeMonData data = commonService.selectById(id);
		
		RptDimDataJson obj = new RptDimDataJson();
		List<McProItemRealTimeMonData> dataList = JsonConvert.jsonToObject(data.getDepthExt(), List.class,McProItemRealTimeMonData.class);

		obj.setX_name("累计变化量(mm)");
		obj.setY_name("深度m");
		List<?> y_data =  DataFilterUtil.getFieldValueFromList(dataList, "depthExt");
		List<?> x_data =  DataFilterUtil.getFieldValueFromList(dataList, "sumValue");

		obj.setX_data(x_data);
		obj.setY_data(y_data);
	 
		rsp.setObj(obj);
		
		
		return rsp;
	}
	
}
