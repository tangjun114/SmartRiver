package cn.jeeweb.modules.project.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectDesign;

/**   
 * @Title: 项目设计方案
 * @Description: 项目设计方案
 * @author wsh
 * @date 2018-12-10 18:23:54
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectdesign")
@RequiresPathPermission("project:srprojectdesign")
public class SrProjectDesignController extends BaseCRUDController<SrProjectDesign, String> {

}
