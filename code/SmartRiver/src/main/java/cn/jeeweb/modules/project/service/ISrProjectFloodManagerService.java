package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectFloodManager;

/**   
 * @Title: 工程水务管理
 * @Description: 工程水务管理
 * @author wsh
 * @date 2018-12-25 19:33:17
 * @version V1.0   
 *
 */
public interface ISrProjectFloodManagerService extends ICommonService<SrProjectFloodManager> {

}

