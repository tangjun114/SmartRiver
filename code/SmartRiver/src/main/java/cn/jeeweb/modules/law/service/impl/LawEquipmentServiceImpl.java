package cn.jeeweb.modules.law.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.law.mapper.LawEquipmentMapper;
import cn.jeeweb.modules.law.entity.LawEquipment;
import cn.jeeweb.modules.law.service.ILawEquipmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 设备管理
 * @Description: 设备管理
 * @author liyonglei
 * @date 2018-11-13 08:50:52
 * @version V1.0   
 *
 */
@Transactional
@Service("lawEquipmentService")
public class LawEquipmentServiceImpl  extends CommonServiceImpl<LawEquipmentMapper,LawEquipment> implements  ILawEquipmentService {

}
