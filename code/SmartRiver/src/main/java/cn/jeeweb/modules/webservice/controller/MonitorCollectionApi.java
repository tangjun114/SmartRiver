package cn.jeeweb.modules.webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.web.controller.FFTableController;

import cn.jeeweb.modules.monitor.entity.McMonitorCollection;

@Controller
@RequestMapping("${api.url.prefix}/monitorcollection")
public class MonitorCollectionApi extends FFTableController<McMonitorCollection>

{

}
