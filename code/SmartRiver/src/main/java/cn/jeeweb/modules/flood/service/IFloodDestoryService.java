package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodDestory;

/**   
 * @Title: 水毁信息表
 * @Description: 水毁信息表
 * @author wsh
 * @date 2018-11-26 21:01:04
 * @version V1.0   
 *
 */
public interface IFloodDestoryService extends ICommonService<FloodDestory> {

}

