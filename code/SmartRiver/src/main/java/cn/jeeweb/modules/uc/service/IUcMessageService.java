package cn.jeeweb.modules.uc.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.uc.entity.UcMessage;

/**   
 * @Title: 个人中心消息
 * @Description: 个人中心消息
 * @author shawloong
 * @date 2017-09-30 00:04:45
 * @version V1.0   
 *
 */
public interface IUcMessageService extends ICommonService<UcMessage> {

}

