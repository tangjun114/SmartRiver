package cn.jeeweb.modules.law.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.law.entity.LawRoutePlan;
 
/**   
 * @Title: 路线规划数据库控制层接口
 * @Description: 路线规划数据库控制层接口
 * @author 李永雷
 * @date 2018-11-13 09:08:03
 * @version V1.0   
 *
 */
public interface LawRoutePlanMapper extends BaseMapper<LawRoutePlan> {
    
}