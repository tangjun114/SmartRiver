package cn.jeeweb.modules.law.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 路线规划
 * @Description: 路线规划
 * @author 李永雷
 * @date 2018-11-13 09:08:03
 * @version V1.0   
 *
 */
@TableName("sr_law_route_plan")
@SuppressWarnings("serial")
public class LawRoutePlan extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**路线名称*/
    @TableField(value = "route_name")
	private String routeName;
    /**位置1*/
    @TableField(value = "route_addr1")
	private String routeAddr1;
    /**位置2*/
    @TableField(value = "route_addr2")
	private String routeAddr2;
    /**位置3*/
    @TableField(value = "route_addr3")
	private String routeAddr3;
    /**位置4*/
    @TableField(value = "route_addr4")
	private String routeAddr4;
    /**位置5*/
    @TableField(value = "route_addr5")
	private String routeAddr5;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  routeName
	 *@return: String  路线名称
	 */
	public String getRouteName(){
		return this.routeName;
	}

	/**
	 * 设置  routeName
	 *@param: routeName  路线名称
	 */
	public void setRouteName(String routeName){
		this.routeName = routeName;
	}
	/**
	 * 获取  routeAddr1
	 *@return: String  位置1
	 */
	public String getRouteAddr1(){
		return this.routeAddr1;
	}

	/**
	 * 设置  routeAddr1
	 *@param: routeAddr1  位置1
	 */
	public void setRouteAddr1(String routeAddr1){
		this.routeAddr1 = routeAddr1;
	}
	/**
	 * 获取  routeAddr2
	 *@return: String  位置2
	 */
	public String getRouteAddr2(){
		return this.routeAddr2;
	}

	/**
	 * 设置  routeAddr2
	 *@param: routeAddr2  位置2
	 */
	public void setRouteAddr2(String routeAddr2){
		this.routeAddr2 = routeAddr2;
	}
	/**
	 * 获取  routeAddr3
	 *@return: String  位置3
	 */
	public String getRouteAddr3(){
		return this.routeAddr3;
	}

	/**
	 * 设置  routeAddr3
	 *@param: routeAddr3  位置3
	 */
	public void setRouteAddr3(String routeAddr3){
		this.routeAddr3 = routeAddr3;
	}
	/**
	 * 获取  routeAddr4
	 *@return: String  位置4
	 */
	public String getRouteAddr4(){
		return this.routeAddr4;
	}

	/**
	 * 设置  routeAddr4
	 *@param: routeAddr4  位置4
	 */
	public void setRouteAddr4(String routeAddr4){
		this.routeAddr4 = routeAddr4;
	}
	/**
	 * 获取  routeAddr5
	 *@return: String  位置5
	 */
	public String getRouteAddr5(){
		return this.routeAddr5;
	}

	/**
	 * 设置  routeAddr5
	 *@param: routeAddr5  位置5
	 */
	public void setRouteAddr5(String routeAddr5){
		this.routeAddr5 = routeAddr5;
	}
	
}
