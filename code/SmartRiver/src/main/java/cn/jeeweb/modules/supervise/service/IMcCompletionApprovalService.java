package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McCompletionApproval;

/**   
 * @Title: 完工审批表
 * @Description: 完工审批表
 * @author shawloong
 * @date 2017-10-07 16:28:41
 * @version V1.0   
 *
 */
public interface IMcCompletionApprovalService extends ICommonService<McCompletionApproval> {

}

