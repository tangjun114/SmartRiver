package cn.jeeweb.modules.supervise.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProRelateFile;

/**   
 * @Title: 项目相关文件
 * @Description: 项目相关文件
 * @author aether
 * @date 2018-01-24 23:54:08
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcprorelatefile")
@RequiresPathPermission("supervise:mcprorelatefile")
public class McProRelateFileController extends McProjectBaseController<McProRelateFile, String> {

}
