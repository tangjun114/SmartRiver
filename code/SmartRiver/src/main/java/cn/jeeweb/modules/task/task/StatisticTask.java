package cn.jeeweb.modules.task.task;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.ff.common.util.format.DateUtil;
import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.util.validate.ValidatorUtil;

import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.SpringContextHolder;
import cn.jeeweb.modules.constant.DataResultConst;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeTotalData;
import cn.jeeweb.modules.supervise.service.IMcProItemMonitorItemService;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeMonDataService;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeTotalDataService;

@Component("statisticTask")
public class StatisticTask {
	public final Logger log = Logger.getLogger(this.getClass());
 
 
	
	public void run() {
		log.info(" run start......................................" + (new Date()));
		Date yesDay = DateUtil.dayCalculate(DateUtil.getCurrentDate(), -1);
		sum(null,DateUtil.DateToString(yesDay, "yyy-MM-dd"));
		log.error("run finish");
		
	}
	public List<McProItemRealTimeTotalData> sum(String projectId,String day)
	{
		if(ValidatorUtil.isEmpty(day))
		{
			day = DateUtil.DateToString(DateUtil.getCurrentDate(), "yyy-MM-dd");
		}
		
		List<McProItemRealTimeTotalData> objList = new ArrayList<McProItemRealTimeTotalData>();

		try
		{
			IMcProItemRealTimeTotalDataService iMcProItemRealTimeTotalDataService = SpringContextHolder.getBean("mcProItemRealTimeTotalDataService");
			IMcProItemRealTimeMonDataService iMcProItemRealTimeMonDataService = SpringContextHolder.getBean("mcProItemRealTimeMonDataService");

//			IMcProItemMonitorItemService iMcProItemMonitorItemService = SpringContextHolder.getBean("mcProItemMonitorItemService");
//			EntityWrapper<McProItemMonitorItem> itemWrapper = new EntityWrapper<McProItemMonitorItem>(McProItemMonitorItem.class);
//	 
//			if(!ValidatorUtil.isEmpty(projectId))
//			{
//				itemWrapper.eq("projectId", projectId);
//			}
//			List<McProItemMonitorItem> itemList = iMcProItemMonitorItemService.selectList(itemWrapper);
//			 
 		 
			String end = day + " 23:59:59";
			
			
			EntityWrapper<McProItemRealTimeMonData> entityWrapper = new EntityWrapper<McProItemRealTimeMonData>(McProItemRealTimeMonData.class);
			entityWrapper.lt("collect_time", end);
			entityWrapper.ge("collect_time", day);
			entityWrapper.eq("data_result", DataResultConst.use);
			if(!ValidatorUtil.isEmpty(projectId))
			{
				entityWrapper.eq("projectId", projectId);
			}
			entityWrapper.setSqlSelect(" count(id) as projectCount,"
					+ "monitor_item_id as monitorItemId, "
					+ "monitor_item_name as monitorItemName, "
					+ "monitor_item_type_code as monitorItemTypeCode, "
					+ "monitor_item_type_name as monitorItemTypeName, "
					+ "project_id as projectId, "
					+ "project_name as projectName");
			entityWrapper.groupBy("monitor_item_id");
			List<McProItemRealTimeMonData> dataList = iMcProItemRealTimeMonDataService.selectList(entityWrapper );
			
	 
			EntityWrapper<McProItemRealTimeTotalData> wrapper = new EntityWrapper<McProItemRealTimeTotalData>(McProItemRealTimeTotalData.class);
		 
			//Map<String,McProItemRealTimeMonData> map = DataFilterUtil.buildMap("monitorItemId", dataList);
			wrapper.eq("day", day);
			if(!ValidatorUtil.isEmpty(projectId))
			{
				wrapper.eq("projectId", projectId);
			}
			iMcProItemRealTimeTotalDataService.delete(wrapper);
			
			for(McProItemRealTimeMonData e :dataList)
			{
				McProItemRealTimeTotalData data = new McProItemRealTimeTotalData();
  				data.setMonitorItemId(e.getMonitorItemId());
				data.setMonitorItemName(e.getMonitorItemName());
				data.setMonitorItemTypeCode(e.getMonitorItemTypeCode());
				data.setMonitorItemTypeName(e.getMonitorItemTypeName());
				data.setProjectId(e.getProjectId());
				data.setProjectName(e.getProjectName());
				
				data.setCount(e.getProjectCount());
				data.setDay(day);
				objList.add(data);
			}
//			for(McProItemMonitorItem e :itemList)
//			{
//				McProItemRealTimeTotalData data = new McProItemRealTimeTotalData();
//				McProItemRealTimeMonData temp = map.get(e.getId());
//				if(null == temp)
//				{
//					data.setCount(0);
//
//				}
//				else
//				{
//					data.setCount(temp.getProjectCount());
//				}
//				
//				data.setMonitorItemId(e.getId());
//				data.setMonitorItemName(e.getMonitorItemName());
//				data.setMonitorItemTypeCode(e.getGroupTypeCode());
//				data.setMonitorItemTypeName(e.getGroupTypeName());
//				data.setProjectId(e.getProjectId());
//				data.setProjectName(e.getProjectName());
//				data.setDay(day);
//				objList.add(data);
//			}
			iMcProItemRealTimeTotalDataService.insertBatch(objList);
		}
		catch(Exception e)
		{
			log.error("run error",e);
			
		}
		return objList;
	}

	public void run1() {
		for (int i = 0; i < 10; i++) {
			log.info(i+" run1......................................" + (new Date()));
		}
	}
}
