package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 报警短信
 * @Description: 报警短信
 * @author jrrey
 * @date 2018-05-30 16:34:13
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_message")
@SuppressWarnings("serial")
public class McProItemMessage extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**监测项ID*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**测点列表*/
    @TableField(value = "point_list")
	private String ponitList;
    /**用户名*/
    @TableField(value = "user_name")
	private String userName;
    /**用户id*/
    @TableField(value = "user_id")
	private String userId;
    /**用户电话*/
    @TableField(value = "phone")
	private String phone;
    /**短信内容*/
    @TableField(value = "content")
	private String content;
    /**结果*/
    @TableField(value = "result")
	private String result;
    /**消息条数*/
    @TableField(value = "msg_num")
	private String msgNum;
    /**消息人数*/
    @TableField(value = "msg_info")
	private String msgInfo;
    /**状态*/
    @TableField(value = "status")
	private String status;
    /**类型*/
    @TableField(value = "type")
	private String type;
    /**组织id*/
    @TableField(value = "monitor_id")
	private String monitorId;
    /**组织名称*/
    @TableField(value = "monitor_name")
	private String monitorName;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项ID
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项ID
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  ponitList
	 *@return: String  测点列表
	 */
	public String getPonitList(){
		return this.ponitList;
	}

	/**
	 * 设置  ponitList
	 *@param: ponitList  测点列表
	 */
	public void setPonitList(String ponitList){
		this.ponitList = ponitList;
	}
	/**
	 * 获取  userName
	 *@return: String  用户名
	 */
	public String getUserName(){
		return this.userName;
	}

	/**
	 * 设置  userName
	 *@param: userName  用户名
	 */
	public void setUserName(String userName){
		this.userName = userName;
	}
	/**
	 * 获取  userId
	 *@return: String  用户id
	 */
	public String getUserId(){
		return this.userId;
	}

	/**
	 * 设置  userId
	 *@param: userId  用户id
	 */
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**
	 * 获取  phone
	 *@return: String  用户电话
	 */
	public String getPhone(){
		return this.phone;
	}

	/**
	 * 设置  phone
	 *@param: phone  用户电话
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	/**
	 * 获取  content
	 *@return: String  短信内容
	 */
	public String getContent(){
		return this.content;
	}

	/**
	 * 设置  content
	 *@param: content  短信内容
	 */
	public void setContent(String content){
		this.content = content;
	}
	/**
	 * 获取  result
	 *@return: String  结果
	 */
	public String getResult(){
		return this.result;
	}

	/**
	 * 设置  result
	 *@param: result  结果
	 */
	public void setResult(String result){
		this.result = result;
	}
	/**
	 * 获取  msgNum
	 *@return: String  消息条数
	 */
	public String getMsgNum(){
		return this.msgNum;
	}

	/**
	 * 设置  msgNum
	 *@param: msgNum  消息条数
	 */
	public void setMsgNum(String msgNum){
		this.msgNum = msgNum;
	}
	/**
	 * 获取  msgInfo
	 *@return: String  消息人数
	 */
	public String getMsgInfo(){
		return this.msgInfo;
	}

	/**
	 * 设置  msgInfo
	 *@param: msgInfo  消息人数
	 */
	public void setMsgInfo(String msgInfo){
		this.msgInfo = msgInfo;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  type
	 *@return: String  类型
	 */
	public String getType(){
		return this.type;
	}

	/**
	 * 设置  type
	 *@param: type  类型
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 * 获取  monitorId
	 *@return: String  组织id
	 */
	public String getMonitorId(){
		return this.monitorId;
	}

	/**
	 * 设置  monitorId
	 *@param: monitorId  组织id
	 */
	public void setMonitorId(String monitorId){
		this.monitorId = monitorId;
	}
	/**
	 * 获取  monitorName
	 *@return: String  组织名称
	 */
	public String getMonitorName(){
		return this.monitorName;
	}

	/**
	 * 设置  monitorName
	 *@param: monitorName  组织名称
	 */
	public void setMonitorName(String monitorName){
		this.monitorName = monitorName;
	}
	
}
