package cn.jeeweb.modules.flood.controller;


import cn.jeeweb.core.model.AjaxJson;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.flood.entity.FloodAssetsStock;
import cn.jeeweb.modules.flood.service.IFloodAssetsStockRecordService;
import cn.jeeweb.modules.flood.service.IFloodAssetsStockService;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.FloodAssetsStockRecord;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**   
 * @Title: 库存统计查询
 * @Description: 库存统计查询
 * @author liyonglei
 * @date 2018-11-23 15:27:46
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/floodassetsstockrecord")
@RequiresPathPermission("flood:floodassetsstockrecord")
public class FloodAssetsStockRecordController extends BaseCRUDController<FloodAssetsStockRecord, String> {

    @Autowired
    private IFloodAssetsStockService floodAssetsStockService;

    @Override
    public AjaxJson update(Model model, FloodAssetsStockRecord entity, BindingResult result, HttpServletRequest request, HttpServletResponse response) {
        log.info("Update Controller>>>>>>>> entity is :"+JSON.toJSON(entity));
        AjaxJson ajaxJson = new AjaxJson();
        ajaxJson.success("保存成功");
        if (hasError(entity, result)) {
            // 错误提示
            String errorMsg = errorMsg(result);
            if (!StringUtils.isEmpty(errorMsg)) {
                ajaxJson.fail(errorMsg);
            } else {
                ajaxJson.fail("保存失败");
            }
            return ajaxJson;
        }
        try {
            preSave(entity, request, response);
            FloodAssetsStock stock = new FloodAssetsStock();
            stock.setId(entity.getId());
            stock.setStatus(entity.getStatus());
            floodAssetsStockService.insertOrUpdate(stock);
            afterSave(entity, request, response);
        } catch (Exception e) {
        	log.error("1",e);
            ajaxJson.fail("保存失败!<br />原因:" + e.getMessage());
        }
        return ajaxJson;
    }
}
