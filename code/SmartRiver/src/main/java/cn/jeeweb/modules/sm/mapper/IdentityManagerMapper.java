package cn.jeeweb.modules.sm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.sm.entity.IdentityManager;
 
/**   
 * @Title: 身份管理数据库控制层接口
 * @Description: 身份管理数据库控制层接口
 * @author shawloong
 * @date 2017-09-29 00:44:22
 * @version V1.0   
 *
 */
public interface IdentityManagerMapper extends BaseMapper<IdentityManager> {
    
}