package cn.jeeweb.modules.webservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.web.controller.FFTableController;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;
import com.ff.common.web.json.PageDataJson;

import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Condition.Operator;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProItemMeasuringPointService;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.utils.UserUtils;


@Controller
@RequestMapping("${api.url.prefix}/project")
public class ProjectApi extends FFTableController<McProject>
{

	@Autowired IMcProjectService iMcProjectService;
	@Override
	public Queryable GetFilterCondition(BaseReqJson request) {
		// TODO Auto-generated method stub
		Queryable qy = super.GetFilterCondition(request);
		 String userId = request.getUserId();
		 if(null != userId)
		 {
			    User user = new User();
			    user.setId(userId);
				{
					qy.setCondition(new Condition().and(Operator.in, "id", iMcProjectService.getProjectIdListByUser(user)));
 		 		}
		 }

		return qy;
	}
	@Override
	public BaseRspJson<McProject> update(@RequestBody BaseReqJson<McProject> request) {

		BaseRspJson<McProject> rsp = new BaseRspJson<McProject>();
		 
		McProject obj = this.getObj(request,this.GetTableClass());
		McProject old = this.baseService.selectById(obj.getId());
		if(null != old )
		{
			if(null != obj.getWorkStatus())
			{
				old.setWorkStatus(obj.getWorkStatus());
			}
		}
		getService().updateById(old);
		rsp.setObj(old);
 
  		return rsp;
	}
	
	

 
}
