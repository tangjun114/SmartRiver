package cn.jeeweb.modules.supervise.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McMonitorDevice;
import cn.jeeweb.modules.supervise.entity.McProItemDevice;

/**   
 * @Title: 项目与设备管理表
 * @Description: 项目与设备管理表
 * @author jerry
 * @date 2018-01-29 17:01:28
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemdevice")
@RequiresPathPermission("supervise:mcproitemdevice")
public class McProItemDeviceController extends McProjectBaseController<McProItemDevice, String> {

	@Override
	public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
	
       String deviceTypeName = request.getParameter("typeName");
       if(!StringUtils.isEmpty(deviceTypeName)){
    	   model.addAttribute("typeName", deviceTypeName);
		}
       super.preList(model, request, response);
       
	}
	
	
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProItemDevice> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
		String  deviceTypeName = request.getParameter("typeName");
		if(!StringUtils.isEmpty(deviceTypeName)){
			entityWrapper.eq("typeName", deviceTypeName);
		}
		super.preAjaxList(queryable, entityWrapper, request, response);
	}
}
