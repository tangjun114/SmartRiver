package cn.jeeweb.modules.sm.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.DataEntity;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 项目类型
 * @Description: 项目类型
 * @author shawloong
 * @date 2017-09-28 00:43:37
 * @version V1.0   
 *
 */
@TableName("sm_project_type")
@SuppressWarnings("serial")
public class ProjectType extends DataEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
 
    /**类型名称*/
    @TableField(value = "name")
	private String name;
    /**工况编号*/
    @TableField(value = "need_wcno")
	private String need_wcno;
    /**监测目的与意义*/
    @TableField(value = "purpose_desc")
	private String purpose_desc;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  name
	 *@return: String  类型名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  类型名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  need_wcno
	 *@return: String  工况编号
	 */
	public String getNeed_wcno(){
		return this.need_wcno;
	}

	/**
	 * 设置  need_wcno
	 *@param: need_wcno  工况编号
	 */
	public void setNeed_wcno(String need_wcno){
		this.need_wcno = need_wcno;
	}
	/**
	 * 获取  purpose_desc
	 *@return: String  监测目的与意义
	 */
	public String getPurpose_desc(){
		return this.purpose_desc;
	}

	/**
	 * 设置  purpose_desc
	 *@param: purpose_desc  监测目的与意义
	 */
	public void setPurpose_desc(String purpose_desc){
		this.purpose_desc = purpose_desc;
	}
	
}
