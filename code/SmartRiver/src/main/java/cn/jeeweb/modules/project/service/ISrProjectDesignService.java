package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectDesign;

/**   
 * @Title: 项目设计方案
 * @Description: 项目设计方案
 * @author wsh
 * @date 2018-12-10 18:23:54
 * @version V1.0   
 *
 */
public interface ISrProjectDesignService extends ICommonService<SrProjectDesign> {

}

