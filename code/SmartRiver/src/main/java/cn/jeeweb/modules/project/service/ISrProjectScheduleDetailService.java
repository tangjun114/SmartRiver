package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectScheduleDetail;

/**   
 * @Title: 进度拆分管理
 * @Description: 进度拆分管理
 * @author wsh
 * @date 2018-12-26 23:43:05
 * @version V1.0   
 *
 */
public interface ISrProjectScheduleDetailService extends ICommonService<SrProjectScheduleDetail> {

}

