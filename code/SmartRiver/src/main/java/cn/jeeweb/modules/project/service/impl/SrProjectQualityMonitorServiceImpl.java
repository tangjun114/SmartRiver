package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectQualityMonitorMapper;
import cn.jeeweb.modules.project.entity.SrProjectQualityMonitor;
import cn.jeeweb.modules.project.service.ISrProjectQualityMonitorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 工程质量检测
 * @Description: 工程质量检测
 * @author wsh
 * @date 2018-12-13 19:29:51
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectQualityMonitorService")
public class SrProjectQualityMonitorServiceImpl  extends CommonServiceImpl<SrProjectQualityMonitorMapper,SrProjectQualityMonitor> implements  ISrProjectQualityMonitorService {

}
