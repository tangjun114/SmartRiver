package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemDeleteApplyMapper;
import cn.jeeweb.modules.supervise.entity.McProItemDeleteApply;
import cn.jeeweb.modules.supervise.service.IMcProItemDeleteApplyService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 原始文件删除申请
 * @Description: 原始文件删除申请
 * @author Aether
 * @date 2018-06-05 08:41:22
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemDeleteApplyService")
public class McProItemDeleteApplyServiceImpl  extends CommonServiceImpl<McProItemDeleteApplyMapper,McProItemDeleteApply> implements  IMcProItemDeleteApplyService {

}
