package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodDestory;
 
/**   
 * @Title: 水毁信息表数据库控制层接口
 * @Description: 水毁信息表数据库控制层接口
 * @author wsh
 * @date 2018-11-26 21:01:04
 * @version V1.0   
 *
 */
public interface FloodDestoryMapper extends BaseMapper<FloodDestory> {
    
}