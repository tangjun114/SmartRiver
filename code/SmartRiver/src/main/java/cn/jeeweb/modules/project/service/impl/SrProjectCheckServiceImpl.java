package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectCheckMapper;
import cn.jeeweb.modules.project.entity.SrProjectCheck;
import cn.jeeweb.modules.project.service.ISrProjectCheckService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目检查
 * @Description: 项目检查
 * @author wsh
 * @date 2019-03-31 16:40:32
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectCheckService")
public class SrProjectCheckServiceImpl  extends CommonServiceImpl<SrProjectCheckMapper,SrProjectCheck> implements  ISrProjectCheckService {

}
