package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectDesignMapper;
import cn.jeeweb.modules.project.entity.SrProjectDesign;
import cn.jeeweb.modules.project.service.ISrProjectDesignService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目设计方案
 * @Description: 项目设计方案
 * @author wsh
 * @date 2018-12-10 18:23:54
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectDesignService")
public class SrProjectDesignServiceImpl  extends CommonServiceImpl<SrProjectDesignMapper,SrProjectDesign> implements  ISrProjectDesignService {

}
