package cn.jeeweb.modules.monitor.service;

import java.util.List;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McProItemAlarm;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;

/**   
 * @Title: 告警信息
 * @Description: 告警信息
 * @author Aether
 * @date 2017-12-02 12:58:21
 * @version V1.0   
 *
 */
public interface IMcProItemAlarmService extends ICommonService<McProItemAlarm> {
	public void sendMessage(McProItemMonitorItem item ,List<McProItemAlarm> alarmList);
}

