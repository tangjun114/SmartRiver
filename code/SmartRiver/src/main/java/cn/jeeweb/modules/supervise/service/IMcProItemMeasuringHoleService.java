package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringHole;

/**   
 * @Title: 项目监控-测点设置-监测项-测孔
 * @Description: 项目监控-测点设置-监测项-测孔
 * @author shawloong
 * @date 2017-11-15 23:22:45
 * @version V1.0   
 *
 */
public interface IMcProItemMeasuringHoleService extends ICommonService<McProItemMeasuringHole> {

}

