package cn.jeeweb.modules.supervise.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.AbstractEntity;
import cn.jeeweb.core.common.entity.DataEntity;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 现场照片
 * @Description: 现场照片
 * @author shawloong
 * @date 2017-11-02 21:37:47
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_scene_img")
@SuppressWarnings("serial")
public class McProItemSceneImg extends AbstractEntity<String> {

    /**ID*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**图片路径*/
    @TableField(value = "img_path")
	private String imgPath;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**创建人*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**异常描述*/
    @TableField(value = "unusual_desc")
	private String UnusualDesc;
    /**工程进度描述*/
    @TableField(value = "project_schedule")
	private String ProjectDc;
	
	/**
	 * 获取  id
	 *@return: String  ID
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  ID
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  imgPath
	 *@return: String  图片路径
	 */
	public String getImgPath(){
		return this.imgPath;
	}

	/**
	 * 设置  imgPath
	 *@param: imgPath  图片路径
	 */
	public void setImgPath(String imgPath){
		this.imgPath = imgPath;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建人
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建人
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  UnusualDesc
	 *@return: String  异常描述
	 */
	public String getUnusualDesc(){
		return this.UnusualDesc;
	}

	/**
	 * 设置  UnusualDesc
	 *@param: UnusualDesc  异常描述
	 */
	public void setUnusualDesc(String UnusualDesc){
		this.UnusualDesc = UnusualDesc;
	}
	/**
	 * 获取  ProjectDc
	 *@return: String  工程进度描述
	 */
	public String getProjectDc(){
		return this.ProjectDc;
	}

	/**
	 * 设置  ProjectDc
	 *@param: ProjectDc  工程进度描述
	 */
	public void setProjectDc(String ProjectDc){
		this.ProjectDc = ProjectDc;
	}
	
}
