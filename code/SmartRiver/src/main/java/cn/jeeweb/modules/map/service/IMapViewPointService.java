package cn.jeeweb.modules.map.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.map.entity.MapViewPoint;

/**   
 * @Title: 观察点管理
 * @Description: 观察点管理
 * @author liyonglei
 * @date 2018-11-14 20:50:41
 * @version V1.0   
 *
 */
public interface IMapViewPointService extends ICommonService<MapViewPoint> {

}

