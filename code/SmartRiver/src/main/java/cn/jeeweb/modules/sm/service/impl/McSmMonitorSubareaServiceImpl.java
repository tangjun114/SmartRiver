package cn.jeeweb.modules.sm.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.sm.mapper.McSmMonitorSubareaMapper;
import cn.jeeweb.modules.monitor.service.IMcMonitorSensorService;
import cn.jeeweb.modules.sm.entity.McSmMonitorSubarea;
import cn.jeeweb.modules.sm.service.IMcSmMonitorSubareaService;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProjectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 系统管理-监测分区
 * @Description: 系统管理-监测分区
 * @author shawloong
 * @date 2017-11-05 18:34:12
 * @version V1.0   
 *
 */
@Transactional
@Service("mcSmMonitorSubareaService")
public class McSmMonitorSubareaServiceImpl  extends CommonServiceImpl<McSmMonitorSubareaMapper,McSmMonitorSubarea> implements  IMcSmMonitorSubareaService {

	@Autowired
	private IMcProjectService peojectService;

	@Override
	public boolean insert(McSmMonitorSubarea entity) {
		if(!StringUtils.isEmpty(entity.getProjectId())){
			McProject mcProject = peojectService.selectById(entity.getProjectId());
			if(!StringUtils.isEmpty(mcProject.getName())){
				entity.setProjectName(mcProject.getName());
			}
		}
		return super.insert(entity);
	}
	
	
}
