package cn.jeeweb.modules.repo.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.repo.entity.McRepoReference;

/**   
 * @Title: 参考文献
 * @Description: 参考文献
 * @author shawloong
 * @date 2017-10-04 14:38:30
 * @version V1.0   
 *
 */
public interface IMcRepoReferenceService extends ICommonService<McRepoReference> {

}

