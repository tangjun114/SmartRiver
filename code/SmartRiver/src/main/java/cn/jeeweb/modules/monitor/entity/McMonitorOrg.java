package cn.jeeweb.modules.monitor.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;

/**   
 * @Title: 监督检测机构
 * @Description: 监督检测机构
 * @author shawloong
 * @date 2018-05-01 15:30:32
 * @version V1.0   
 *
 */
@TableName("mc_monitor_org")
@SuppressWarnings("serial")
public class McMonitorOrg extends AbstractEntity<String> {

    /**id*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**机构名称*/
    @TableField(value = "name")
	private String name;
    /**邮编*/
    @TableField(value = "zip_code")
	private String zipCode;
    /**机构传真*/
    @TableField(value = "fax")
	private String fax;
    /**机构地址*/
    @TableField(value = "address")
	private String address;
    /**机构网址*/
    @TableField(value = "url")
	private String url;
    /**办公面积*/
    @TableField(value = "acreage")
	private String acreage;
    /**设备总数*/
    @TableField(value = "device")
	private String device;
    /**机构邮箱*/
    @TableField(value = "email")
	private String email;
    /**注册资金*/
    @TableField(value = "reg_capital")
	private String regCapital;
    /**联系人电话*/
    @TableField(value = "phone")
	private String phone;
    /**技术人员*/
    @TableField(value = "artisan")
	private String artisan;
    /**机构座机号*/
    @TableField(value = "org_phone")
	private String orgPhone;
    /**初级职称*/
    @TableField(value = "primary_job_title")
	private String primaryJobTitle;
    /**法人代表*/
    @TableField(value = "representative")
	private String representative;
    /**中级职称*/
    @TableField(value = "mid_job_title")
	private String midJobTitle;
    /**技术总负责人*/
    @TableField(value = "chief_tech_officer")
	private String chiefTechOfficer;
    /**高级职称*/
    @TableField(value = "senior_job_title")
	private String seniorJobTitle ;
    /**关联组织id*/
    @TableField(value = "org")
	private String org;
    /**机构图片*/
    @TableField(value = "logo")
	private String logo;
    /**机构类型*/
    @TableField(value = "type")
	private String type;
    /**组织父级别ID*/
    @TableField(value = "org_parent_id")
	private String orgParentId;
    /**企业性质*/
    @TableField(value = "company_type")
	private String companyType;
    /**省*/
    @TableField(value = "province")
	private String province;
    /**市*/
    @TableField(value = "city")
	private String city;
    /**区*/
    @TableField(value = "district")
	private String district;
    /**经度*/
    @TableField(value = "longitude")
	private String longitude;
    /**纬度*/
    @TableField(value = "latitude")
	private String latitude;
    /**模板权限*/
    @TableField(value = "template_perm")
	private String templatePerm;
	
	/**
	 * 获取  id
	 *@return: String  id
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  name
	 *@return: String  机构名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  机构名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  zipCode
	 *@return: String  邮编
	 */
	public String getZipCode(){
		return this.zipCode;
	}

	/**
	 * 设置  zipCode
	 *@param: zipCode  邮编
	 */
	public void setZipCode(String zipCode){
		this.zipCode = zipCode;
	}
	/**
	 * 获取  fax
	 *@return: String  机构传真
	 */
	public String getFax(){
		return this.fax;
	}

	/**
	 * 设置  fax
	 *@param: fax  机构传真
	 */
	public void setFax(String fax){
		this.fax = fax;
	}
	/**
	 * 获取  address
	 *@return: String  机构地址
	 */
	public String getAddress(){
		return this.address;
	}

	/**
	 * 设置  address
	 *@param: address  机构地址
	 */
	public void setAddress(String address){
		this.address = address;
	}
	/**
	 * 获取  url
	 *@return: String  机构网址
	 */
	public String getUrl(){
		return this.url;
	}

	/**
	 * 设置  url
	 *@param: url  机构网址
	 */
	public void setUrl(String url){
		this.url = url;
	}
	/**
	 * 获取  acreage
	 *@return: String  办公面积
	 */
	public String getAcreage(){
		return this.acreage;
	}

	/**
	 * 设置  acreage
	 *@param: acreage  办公面积
	 */
	public void setAcreage(String acreage){
		this.acreage = acreage;
	}
	/**
	 * 获取  device
	 *@return: String  设备总数
	 */
	public String getDevice(){
		return this.device;
	}

	/**
	 * 设置  device
	 *@param: device  设备总数
	 */
	public void setDevice(String device){
		this.device = device;
	}
	/**
	 * 获取  email
	 *@return: String  机构邮箱
	 */
	public String getEmail(){
		return this.email;
	}

	/**
	 * 设置  email
	 *@param: email  机构邮箱
	 */
	public void setEmail(String email){
		this.email = email;
	}
	/**
	 * 获取  regCapital
	 *@return: String  注册资金
	 */
	public String getRegCapital(){
		return this.regCapital;
	}

	/**
	 * 设置  regCapital
	 *@param: regCapital  注册资金
	 */
	public void setRegCapital(String regCapital){
		this.regCapital = regCapital;
	}
	/**
	 * 获取  phone
	 *@return: String  联系人电话
	 */
	public String getPhone(){
		return this.phone;
	}

	/**
	 * 设置  phone
	 *@param: phone  联系人电话
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	/**
	 * 获取  artisan
	 *@return: String  技术人员
	 */
	public String getArtisan(){
		return this.artisan;
	}

	/**
	 * 设置  artisan
	 *@param: artisan  技术人员
	 */
	public void setArtisan(String artisan){
		this.artisan = artisan;
	}
	/**
	 * 获取  orgPhone
	 *@return: String  机构座机号
	 */
	public String getOrgPhone(){
		return this.orgPhone;
	}

	/**
	 * 设置  orgPhone
	 *@param: orgPhone  机构座机号
	 */
	public void setOrgPhone(String orgPhone){
		this.orgPhone = orgPhone;
	}
	/**
	 * 获取  primaryJobTitle
	 *@return: String  初级职称
	 */
	public String getPrimaryJobTitle(){
		return this.primaryJobTitle;
	}

	/**
	 * 设置  primaryJobTitle
	 *@param: primaryJobTitle  初级职称
	 */
	public void setPrimaryJobTitle(String primaryJobTitle){
		this.primaryJobTitle = primaryJobTitle;
	}
	/**
	 * 获取  representative
	 *@return: String  法人代表
	 */
	public String getRepresentative(){
		return this.representative;
	}

	/**
	 * 设置  representative
	 *@param: representative  法人代表
	 */
	public void setRepresentative(String representative){
		this.representative = representative;
	}
	/**
	 * 获取  midJobTitle
	 *@return: String  中级职称
	 */
	public String getMidJobTitle(){
		return this.midJobTitle;
	}

	/**
	 * 设置  midJobTitle
	 *@param: midJobTitle  中级职称
	 */
	public void setMidJobTitle(String midJobTitle){
		this.midJobTitle = midJobTitle;
	}
	/**
	 * 获取  chiefTechOfficer
	 *@return: String  技术总负责人
	 */
	public String getChiefTechOfficer(){
		return this.chiefTechOfficer;
	}

	/**
	 * 设置  chiefTechOfficer
	 *@param: chiefTechOfficer  技术总负责人
	 */
	public void setChiefTechOfficer(String chiefTechOfficer){
		this.chiefTechOfficer = chiefTechOfficer;
	}
	/**
	 * 获取  seniorJobTitle 
	 *@return: String  高级职称
	 */
	public String getSeniorJobTitle (){
		return this.seniorJobTitle ;
	}

	/**
	 * 设置  seniorJobTitle 
	 *@param: seniorJobTitle   高级职称
	 */
	public void setSeniorJobTitle (String seniorJobTitle ){
		this.seniorJobTitle  = seniorJobTitle ;
	}
	/**
	 * 获取  org
	 *@return: String  关联组织id
	 */
	public String getOrg(){
		return this.org;
	}

	/**
	 * 设置  org
	 *@param: org  关联组织id
	 */
	public void setOrg(String org){
		this.org = org;
	}
	/**
	 * 获取  logo
	 *@return: String  机构图片
	 */
	public String getLogo(){
		return this.logo;
	}

	/**
	 * 设置  logo
	 *@param: logo  机构图片
	 */
	public void setLogo(String logo){
		this.logo = logo;
	}
	/**
	 * 获取  type
	 *@return: String  机构类型
	 */
	public String getType(){
		return this.type;
	}

	/**
	 * 设置  type
	 *@param: type  机构类型
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 * 获取  orgParentId
	 *@return: String  组织父级别ID
	 */
	public String getOrgParentId(){
		return this.orgParentId;
	}

	/**
	 * 设置  orgParentId
	 *@param: orgParentId  组织父级别ID
	 */
	public void setOrgParentId(String orgParentId){
		this.orgParentId = orgParentId;
	}
	/**
	 * 获取  companyType
	 *@return: String  企业性质
	 */
	public String getCompanyType(){
		return this.companyType;
	}

	/**
	 * 设置  companyType
	 *@param: companyType  企业性质
	 */
	public void setCompanyType(String companyType){
		this.companyType = companyType;
	}
	/**
	 * 获取  province
	 *@return: String  省
	 */
	public String getProvince(){
		return this.province;
	}

	/**
	 * 设置  province
	 *@param: province  省
	 */
	public void setProvince(String province){
		this.province = province;
	}
	/**
	 * 获取  city
	 *@return: String  市
	 */
	public String getCity(){
		return this.city;
	}

	/**
	 * 设置  city
	 *@param: city  市
	 */
	public void setCity(String city){
		this.city = city;
	}
	/**
	 * 获取  district
	 *@return: String  区
	 */
	public String getDistrict(){
		return this.district;
	}

	/**
	 * 设置  district
	 *@param: district  区
	 */
	public void setDistrict(String district){
		this.district = district;
	}
	/**
	 * 获取  longitude
	 *@return: String  经度
	 */
	public String getLongitude(){
		return this.longitude;
	}

	/**
	 * 设置  longitude
	 *@param: longitude  经度
	 */
	public void setLongitude(String longitude){
		this.longitude = longitude;
	}
	/**
	 * 获取  latitude
	 *@return: String  纬度
	 */
	public String getLatitude(){
		return this.latitude;
	}

	/**
	 * 设置  latitude
	 *@param: latitude  纬度
	 */
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
	/**
	 * 获取  templatePerm
	 *@return: String  模板权限
	 */
	public String getTemplatePerm(){
		return this.templatePerm;
	}

	/**
	 * 设置  templatePerm
	 *@param: templatePerm  模板权限
	 */
	public void setTemplatePerm(String templatePerm){
		this.templatePerm = templatePerm;
	}
	
}