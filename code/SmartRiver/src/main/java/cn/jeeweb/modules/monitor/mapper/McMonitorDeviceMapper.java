package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorDevice;
 
/**   
 * @Title: 设备信息数据库控制层接口
 * @Description: 设备信息数据库控制层接口
 * @author shawloong
 * @date 2017-10-14 00:12:17
 * @version V1.0   
 *
 */
public interface McMonitorDeviceMapper extends BaseMapper<McMonitorDevice> {
    
}