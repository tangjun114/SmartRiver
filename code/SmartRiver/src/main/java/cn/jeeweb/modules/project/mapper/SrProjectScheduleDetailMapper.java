package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectScheduleDetail;
 
/**   
 * @Title: 进度拆分管理数据库控制层接口
 * @Description: 进度拆分管理数据库控制层接口
 * @author wsh
 * @date 2018-12-26 23:43:05
 * @version V1.0   
 *
 */
public interface SrProjectScheduleDetailMapper extends BaseMapper<SrProjectScheduleDetail> {
    
}