package cn.jeeweb.modules.water.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.water.mapper.WaterDrainOutletReportMapper;
import cn.jeeweb.modules.water.entity.WaterDrainOutletReport;
import cn.jeeweb.modules.water.service.IWaterDrainOutletReportService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 排污口检测报表
 * @Description: 排污口检测报表
 * @author wsh
 * @date 2019-03-19 19:09:33
 * @version V1.0   
 *
 */
@Transactional
@Service("waterDrainOutletReportService")
public class WaterDrainOutletReportServiceImpl  extends CommonServiceImpl<WaterDrainOutletReportMapper,WaterDrainOutletReport> implements  IWaterDrainOutletReportService {

}
