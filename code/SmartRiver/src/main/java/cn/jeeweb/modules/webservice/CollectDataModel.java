package cn.jeeweb.modules.webservice;

public class CollectDataModel 
{
	private String depth;
	private String nowValue;
	private String lastValue;
	private String minusValue;
	private String sumValue;
	private String changeRate;
	private String warnValue;
	private String controlValue;
 
	public String getDepth() {
		return depth;
	}
	public void setDepth(String depth) {
		this.depth = depth;
	}
	public String getNowValue() {
		return nowValue;
	}
	public void setNowValue(String nowValue) {
		this.nowValue = nowValue;
	}
	public String getLastValue() {
		return lastValue;
	}
	public void setLastValue(String lastValue) {
		this.lastValue = lastValue;
	}
	public String getMinusValue() {
		return minusValue;
	}
	public void setMinusValue(String minusValue) {
		this.minusValue = minusValue;
	}
	public String getSumValue() {
		return sumValue;
	}
	public void setSumValue(String sumValue) {
		this.sumValue = sumValue;
	}
	public String getChangeRate() {
		return changeRate;
	}
	public void setChangeRate(String changeRate) {
		this.changeRate = changeRate;
	}
	public String getWarnValue() {
		return warnValue;
	}
	public void setWarnValue(String warnValue) {
		this.warnValue = warnValue;
	}
	public String getControlValue() {
		return controlValue;
	}
	public void setControlValue(String controlValue) {
		this.controlValue = controlValue;
	}
	
	
	

}
