package cn.jeeweb.modules.supervise.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.util.meta.ReflectionUtil;
import com.ff.common.util.validate.ValidatorUtil;

import cn.jeeweb.core.query.data.Page;
import cn.jeeweb.core.query.data.PageRequest;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort.Direction;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProItemImplementLog;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProItemImplementLogService;
import cn.jeeweb.modules.sys.utils.DictUtils;

/**   
 * @Title: 项目监控-实施日志
 * @Description: 项目监控-实施日志
 * @author shawloong
 * @date 2017-11-15 00:48:21
 * @version V1.0   
 * 
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemimplementlog")
@RequiresPathPermission("supervise:mcproitemimplementlog")
public class McProItemImplementLogController extends McProjectBaseController<McProItemImplementLog, String> {
 
	@Autowired
	private IMcProItemImplementLogService implementService;

	@RequestMapping(value = "/latest")
	public String latest(Model model, HttpServletRequest request, HttpServletResponse response) {
		String projectId = request.getParameter("projectId");

		Queryable query = QueryRequest.newQueryable();
		query.addCondition("projectId", projectId);
		
		query.addOrder(Direction.DESC,"createDate");
	
		query.setPageable(new PageRequest(1,1));
		
		Page<McProItemImplementLog> dataList = implementService.list(query);
		McProItemImplementLog entity;
		if (dataList != null && !ValidatorUtil.isEmpty(dataList.getContent())) {
			entity = dataList.getContent().get(0);
			String logTypeVal = DictUtils.getDictLabel(entity.getImplementLogType(), "implementLogType", "");
			entity.setImplementLogType(logTypeVal);
			String visibleType = DictUtils.getDictLabel(entity.getVisibleType(), "visibleType", "");
			entity.setVisibleType(visibleType);
		} else {
			entity = new McProItemImplementLog();
		} 

		model.addAttribute("detail", entity);
		
		return display("detail");
	}

	@Override
	public void preSave(McProItemImplementLog entity, HttpServletRequest request, HttpServletResponse response) {
		String projectId = request.getParameter("projectId");
		
		McProject project = projectService.selectById(projectId);
		if (null != project) {
			ReflectionUtil.setObjectField(entity, "projectId", project.getId());
			ReflectionUtil.setObjectField(entity, "projectName", project.getName());
			project.setImplementLatestLog(entity.getContent());
			projectService.updateById(project);
		}
	}
	
	
 
}
