package cn.jeeweb.modules.supervise.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;

import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;
 
/**   
 * @Title: 项目监控-监测情况-实时监测数据数据库控制层接口
 * @Description: 项目监控-监测情况-实时监测数据数据库控制层接口
 * @author shawloong
 * @date 2017-11-21 16:32:11
 * @version V1.0   
 *
 */
public interface McProItemRealTimeMonDataMapper extends BaseMapper<McProItemRealTimeMonData> {
    
	List<McProItemRealTimeMonData> getSumData(@Param("ew") Wrapper<McProItemRealTimeMonData> wrapper);
}