package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodArea;

/**   
 * @Title: 水清监测区域
 * @Description: 水情监测区域
 * @author wsh
 * @date 2018-11-30 16:11:01
 * @version V1.0   
 *
 */
public interface IFloodAreaService extends ICommonService<FloodArea> {

}

