package cn.jeeweb.modules.law.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 巡检计划
 * @Description: 巡检计划
 * @author liyonglei
 * @date 2018-11-13 20:27:22
 * @version V1.0   
 *
 */
@TableName("sr_law_check_plan")
@SuppressWarnings("serial")
public class LawCheckPlan extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**计划名称*/
    @TableField(value = "plan_name")
	private String planName;
    /**路线名称*/
    @TableField(value = "route_name")
	private String routeName;
    /**巡检人员名称*/
    @TableField(value = "user_name")
	private String userName;
    /**计划次数*/
    @TableField(value = "plan_count")
	private Integer planCount;
    /**完成次数*/
    @TableField(value = "complete_count")
	private Integer completeCount;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  planName
	 *@return: String  计划名称
	 */
	public String getPlanName(){
		return this.planName;
	}

	/**
	 * 设置  planName
	 *@param: planName  计划名称
	 */
	public void setPlanName(String planName){
		this.planName = planName;
	}
	/**
	 * 获取  routeName
	 *@return: String  路线名称
	 */
	public String getRouteName(){
		return this.routeName;
	}

	/**
	 * 设置  routeName
	 *@param: routeName  路线名称
	 */
	public void setRouteName(String routeName){
		this.routeName = routeName;
	}
	/**
	 * 获取  userName
	 *@return: String  巡检人员名称
	 */
	public String getUserName(){
		return this.userName;
	}

	/**
	 * 设置  userName
	 *@param: userName  巡检人员名称
	 */
	public void setUserName(String userName){
		this.userName = userName;
	}
	/**
	 * 获取  planCount
	 *@return: Integer  计划次数
	 */
	public Integer getPlanCount(){
		return this.planCount;
	}

	/**
	 * 设置  planCount
	 *@param: planCount  计划次数
	 */
	public void setPlanCount(Integer planCount){
		this.planCount = planCount;
	}
	/**
	 * 获取  completeCount
	 *@return: Integer  完成次数
	 */
	public Integer getCompleteCount(){
		return this.completeCount;
	}

	/**
	 * 设置  completeCount
	 *@param: completeCount  完成次数
	 */
	public void setCompleteCount(Integer completeCount){
		this.completeCount = completeCount;
	}
	
}
