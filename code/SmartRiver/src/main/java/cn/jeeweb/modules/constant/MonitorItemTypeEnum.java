package cn.jeeweb.modules.constant;

public enum MonitorItemTypeEnum 
{
	 
		T001("T001","kN","拉力最大值","压力最大值"),//支撑内力
		T002("T002","mm","累计向内位移最大值","累计向外位移最大值"),//水平位移
		T003("T003","mm","累计向内位移最大值","累计向外位移最大值"),//测斜
		T004("T004","mm","累计上升最大值","累计下沉最大值"),//竖向位移
		T005("T005","mm","累计上升最大值","累计下降最大值"),//地下水位
		T006("T006","kPa","累计变化最大值","累计变化最小值"),//土压力
		T007("T007","kPa","累计变化最大值","累计变化最小值"),//孔隙水压力
		T008("T008","kN","拉力最大值","压力最大值"),//结构内力
		T009("T009","mm","累计向内位移最大值","累计向外位移最大值"),//收敛
		T010("T010","kN","锚杆拉力最大值","锚杆压力最大值"),//锚杆内力
		T011("T011","kN","锚索拉力最大值","锚索拉力最小值"),//锚索拉力
		 
		T012("T012","mm","累计向内位移最大值","累计向外位移最大值");//测点相对位移
 
	
 
	private MonitorItemTypeEnum(String typeCode, String unit, String maxName, String minName) {
		  
		this.typeCode = typeCode;
		this.unit = unit;
		this.maxName = maxName;
		this.minName = minName;
	}
	private String typeCode;
	private String unit;
	private String maxName;
	private String minName;
	
	public static MonitorItemTypeEnum getEnumByCode(String strValue)
	{
		for (MonitorItemTypeEnum c : MonitorItemTypeEnum.values())
		{
			if (c.typeCode.equals(strValue))
			{
				return c;
			}
		}
		return T001;
	}
	
	public static String addUnit(String name,String code)
	{
		name = name + "(" + getEnumByCode(code).getUnit() + ")";
		return name;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public String getUnit() {
		return unit;
	}

	public String getMaxName() {
		return maxName;
	}
	
	public String getMaxNameUnit()
	{
		return addUnit(maxName, typeCode);
	}

	public String getMinName() {
		return minName;
	}
	public String getMinNameUnit()
	{
		return addUnit(minName, typeCode);
	}
	

}
