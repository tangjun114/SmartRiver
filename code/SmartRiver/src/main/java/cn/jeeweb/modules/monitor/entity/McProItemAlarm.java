package cn.jeeweb.modules.monitor.entity;

import cn.jeeweb.cache.MonitorTypeParaCache;
import cn.jeeweb.core.common.entity.AbstractEntity;
import cn.jeeweb.core.utils.SpringContextHolder;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;

import cn.jeeweb.modules.constant.AlarmTypeEnum;
import cn.jeeweb.modules.constant.MonitorItemTypeEnum;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

import org.omg.IOP.ServiceContextHelper;

/**   
 * @Title: 告警信息
 * @Description: 告警信息
 * @author Aether
 * @date 2018-01-27 08:52:50
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_alarm")
@SuppressWarnings("serial")
public class McProItemAlarm extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目Id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**监控项id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监控项名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**监测项类别code*/
    @TableField(value = "monitor_type_code")
	private String monitorTypeCode;
    /**监测项类别名称*/
    @TableField(value = "monitor_type_name")
	private String monitorTypeName;
    /**点号*/
    @TableField(value = "measuring_point_code")
	private String measuringPointCode;
    /**点id*/
    @TableField(value = "measuring_point_id")
	private String measuringPointId;
    /**当前值*/
    @TableField(value = "now_value")
	private Double nowValue;
    /**分区id*/
    @TableField(value = "subarea_id")
	private String subareaId;
    /**分区名称*/
    @TableField(value = "subarea_name")
	private String subareaName;
    /**告警类型id*/
    @TableField(value = "alarm_type_id")
	private String alarmTypeId;
    /**告警类型名称*/
    @TableField(value = "alarm_type_name")
	private String alarmTypeName;
    /**告警值*/
    @TableField(value = "alarm_value")
	private Double alarmValue;
    /**预警值*/
    @TableField(value = "warn_value")
	private Double warnValue;
    /**控制值*/
    @TableField(value = "control_value")
	private Double controlValue;
    /**测量次数*/
    @TableField(value = "test_count")
	private Integer testCount;
    /**场次*/
    @TableField(value = "project_count")
	private Integer projectCount;
    /**上次结果*/
    @TableField(value = "last_value")
	private Double lastValue;
    /**差值*/
    @TableField(value = "minus_value")
	private Double minusValue;
    /**累计值*/
    @TableField(value = "sum_value")
	private Double sumValue;
    /**变化率*/
    @TableField(value = "change_rate")
	private Double changeRate;
    /**初始值*/
    @TableField(value = "init_value")
	private Double initValue;
    /**处理人*/
    @TableField(value = "handler")
	private String handler;
    /**处理结果*/
    @TableField(value = "handle_result")
	private String handleResult;
    /**处理状态*/
    @TableField(value = "handle_status")
	private String handleStatus;
    /**是否最新场次*/
    @TableField(value = "new_project_count")
	private String newProjectCount;
    /**处理图片*/
    @TableField(value = "handle_img")
	private String handleImg;
    /**速率告警值*/
    @TableField(value = "rate_alarm_value")
	private Double rateAlarmValue;
    /**产生数据的id*/
    @TableField(value = "data_id")
	private String dataId;
    /**告警种类*/
    @TableField(value = "alarm_kind")
	private String alarmKind;
    @TableField(exist=false)
    private String alarmDesp;
	/**
	 * 获取  id
	 *@return: String  主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目Id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目Id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监控项id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监控项id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监控项名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监控项名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  monitorTypeCode
	 *@return: String  监测项类别code
	 */
	public String getMonitorTypeCode(){
		return this.monitorTypeCode;
	}

	/**
	 * 设置  monitorTypeCode
	 *@param: monitorTypeCode  监测项类别code
	 */
	public void setMonitorTypeCode(String monitorTypeCode){
		this.monitorTypeCode = monitorTypeCode;
	}
	/**
	 * 获取  monitorTypeName
	 *@return: String  监测项类别名称
	 */
	public String getMonitorTypeName(){
		return this.monitorTypeName;
	}

	/**
	 * 设置  monitorTypeName
	 *@param: monitorTypeName  监测项类别名称
	 */
	public void setMonitorTypeName(String monitorTypeName){
		this.monitorTypeName = monitorTypeName;
	}
	/**
	 * 获取  measuringPointCode
	 *@return: String  点号
	 */
	public String getMeasuringPointCode(){
		return this.measuringPointCode;
	}

	/**
	 * 设置  measuringPointCode
	 *@param: measuringPointCode  点号
	 */
	public void setMeasuringPointCode(String measuringPointCode){
		this.measuringPointCode = measuringPointCode;
	}
	/**
	 * 获取  measuringPointId
	 *@return: String  点id
	 */
	public String getMeasuringPointId(){
		return this.measuringPointId;
	}

	/**
	 * 设置  measuringPointId
	 *@param: measuringPointId  点id
	 */
	public void setMeasuringPointId(String measuringPointId){
		this.measuringPointId = measuringPointId;
	}
	/**
	 * 获取  nowValue
	 *@return: Double  当前值
	 */
	public Double getNowValue(){
		return this.nowValue;
	}

	/**
	 * 设置  nowValue
	 *@param: nowValue  当前值
	 */
	public void setNowValue(Double nowValue){
		this.nowValue = nowValue;
	}
	/**
	 * 获取  subareaId
	 *@return: String  分区id
	 */
	public String getSubareaId(){
		return this.subareaId;
	}

	/**
	 * 设置  subareaId
	 *@param: subareaId  分区id
	 */
	public void setSubareaId(String subareaId){
		this.subareaId = subareaId;
	}
	/**
	 * 获取  subareaName
	 *@return: String  分区名称
	 */
	public String getSubareaName(){
		return this.subareaName;
	}

	/**
	 * 设置  subareaName
	 *@param: subareaName  分区名称
	 */
	public void setSubareaName(String subareaName){
		this.subareaName = subareaName;
	}
	/**
	 * 获取  alarmTypeId
	 *@return: String  告警类型id
	 */
	public String getAlarmTypeId(){
		return this.alarmTypeId;
	}

	/**
	 * 设置  alarmTypeId
	 *@param: alarmTypeId  告警类型id
	 */
	public void setAlarmTypeId(String alarmTypeId){
		this.alarmTypeId = alarmTypeId;
	}
	/**
	 * 获取  alarmTypeName
	 *@return: String  告警类型名称
	 */
	public String getAlarmTypeName(){
		return this.alarmTypeName;
	}

	/**
	 * 设置  alarmTypeName
	 *@param: alarmTypeName  告警类型名称
	 */
	public void setAlarmTypeName(String alarmTypeName){
		this.alarmTypeName = alarmTypeName;
	}
	/**
	 * 获取  alarmValue
	 *@return: Double  告警值
	 */
	public Double getAlarmValue(){
		return this.alarmValue;
	}

	/**
	 * 设置  alarmValue
	 *@param: alarmValue  告警值
	 */
	public void setAlarmValue(Double alarmValue){
		this.alarmValue = alarmValue;
	}
	/**
	 * 获取  warnValue
	 *@return: Double  预警值
	 */
	public Double getWarnValue(){
		return this.warnValue;
	}

	/**
	 * 设置  warnValue
	 *@param: warnValue  预警值
	 */
	public void setWarnValue(Double warnValue){
		this.warnValue = warnValue;
	}
	/**
	 * 获取  controlValue
	 *@return: Double  控制值
	 */
	public Double getControlValue(){
		return this.controlValue;
	}

	/**
	 * 设置  controlValue
	 *@param: controlValue  控制值
	 */
	public void setControlValue(Double controlValue){
		this.controlValue = controlValue;
	}
	/**
	 * 获取  testCount
	 *@return: Integer  测量次数
	 */
	public Integer getTestCount(){
		return this.testCount;
	}

	/**
	 * 设置  testCount
	 *@param: testCount  测量次数
	 */
	public void setTestCount(Integer testCount){
		this.testCount = testCount;
	}
	/**
	 * 获取  projectCount
	 *@return: Integer  场次
	 */
	public Integer getProjectCount(){
		return this.projectCount;
	}

	/**
	 * 设置  projectCount
	 *@param: projectCount  场次
	 */
	public void setProjectCount(Integer projectCount){
		this.projectCount = projectCount;
	}
	/**
	 * 获取  lastValue
	 *@return: Double  上次结果
	 */
	public Double getLastValue(){
		return this.lastValue;
	}

	/**
	 * 设置  lastValue
	 *@param: lastValue  上次结果
	 */
	public void setLastValue(Double lastValue){
		this.lastValue = lastValue;
	}
	/**
	 * 获取  minusValue
	 *@return: Double  差值
	 */
	public Double getMinusValue(){
		return this.minusValue;
	}

	/**
	 * 设置  minusValue
	 *@param: minusValue  差值
	 */
	public void setMinusValue(Double minusValue){
		this.minusValue = minusValue;
	}
	/**
	 * 获取  sumValue
	 *@return: Double  累计值
	 */
	public Double getSumValue(){
		return this.sumValue;
	}

	/**
	 * 设置  sumValue
	 *@param: sumValue  累计值
	 */
	public void setSumValue(Double sumValue){
		this.sumValue = sumValue;
	}
	/**
	 * 获取  changeRate
	 *@return: Double  变化率
	 */
	public Double getChangeRate(){
		return this.changeRate;
	}

	/**
	 * 设置  changeRate
	 *@param: changeRate  变化率
	 */
	public void setChangeRate(Double changeRate){
		this.changeRate = changeRate;
	}
	/**
	 * 获取  initValue
	 *@return: Double  初始值
	 */
	public Double getInitValue(){
		return this.initValue;
	}

	/**
	 * 设置  initValue
	 *@param: initValue  初始值
	 */
	public void setInitValue(Double initValue){
		this.initValue = initValue;
	}
	/**
	 * 获取  handler
	 *@return: String  处理人
	 */
	public String getHandler(){
		return this.handler;
	}

	/**
	 * 设置  handler
	 *@param: handler  处理人
	 */
	public void setHandler(String handler){
		this.handler = handler;
	}
	/**
	 * 获取  handleResult
	 *@return: String  处理结果
	 */
	public String getHandleResult(){
		return this.handleResult;
	}

	/**
	 * 设置  handleResult
	 *@param: handleResult  处理结果
	 */
	public void setHandleResult(String handleResult){
		this.handleResult = handleResult;
	}
	/**
	 * 获取  handleStatus
	 *@return: String  处理状态
	 */
	public String getHandleStatus(){
		return this.handleStatus;
	}

	/**
	 * 设置  handleStatus
	 *@param: handleStatus  处理状态
	 */
	public void setHandleStatus(String handleStatus){
		this.handleStatus = handleStatus;
	}
	/**
	 * 获取  newProjectCount
	 *@return: String  是否最新场次
	 */
	public String getNewProjectCount(){
		return this.newProjectCount;
	}

	/**
	 * 设置  newProjectCount
	 *@param: newProjectCount  是否最新场次
	 */
	public void setNewProjectCount(String newProjectCount){
		this.newProjectCount = newProjectCount;
	}
	/**
	 * 获取  handleImg
	 *@return: String  处理图片
	 */
	public String getHandleImg(){
		return this.handleImg;
	}

	/**
	 * 设置  handleImg
	 *@param: handleImg  处理图片
	 */
	public void setHandleImg(String handleImg){
		this.handleImg = handleImg;
	}
	/**
	 * 获取  rateAlarmValue
	 *@return: String  速率告警值
	 */
	public Double getRateAlarmValue(){
		return this.rateAlarmValue;
	}

	/**
	 * 设置  rateAlarmValue
	 *@param: rateAlarmValue  速率告警值
	 */
	public void setRateAlarmValue(Double rateAlarmValue){
		this.rateAlarmValue = rateAlarmValue;
	}

	/**
	 * 获取  dataId
	 *@return: String  产生数据的id
	 */
	public String getDataId(){
		return this.dataId;
	}

	/**
	 * 设置  dataId
	 *@param: dataId  产生数据的id
	 */
	public void setDataId(String dataId){
		this.dataId = dataId;
	}
	/**
	 * 获取  alarmKind
	 *@return: String  告警种类
	 */
	public String getAlarmKind(){
		return this.alarmKind;
	}

	/**
	 * 设置  alarmKind
	 *@param: alarmKind  告警种类
	 */
	public void setAlarmKind(String alarmKind){
		this.alarmKind = alarmKind;
	}
 

	public String getAlarmDesp() {
		alarmDesp = "";
		if(AlarmTypeEnum.SUM_ALARM.equals(this.alarmKind))
		{
			alarmDesp= "累计值";
			if (AlarmTypeEnum.OVERLOAD.getValue().equals(this.alarmTypeId)){
		       	 //超控
		        	alarmDesp += "超出控制值"+formatData(Math.abs(this.controlValue- Math.abs(this.sumValue)));
		        } else if (AlarmTypeEnum.ALARM.getValue().equals(this.alarmTypeId)) {
		       	 //报警
		        	alarmDesp += "超出报警值"+formatData(Math.abs(this.alarmValue- Math.abs(this.sumValue)));
		        } else if (AlarmTypeEnum.WARN.getValue().equals(this.alarmTypeId)) {
		       	 //预警
		        	alarmDesp += "超出预警值"+formatData(Math.abs(this.warnValue- Math.abs(this.sumValue)));
		        } else {
		       	 
		        }
	        alarmDesp += SpringContextHolder.getBean(MonitorTypeParaCache.class).get(this.getMonitorTypeCode()).getUnit();

		}
		else
		{
			alarmDesp = "速率值";
			if (AlarmTypeEnum.OVERLOAD.getValue().equals(this.alarmTypeId)){
		       	 //超控
		        	alarmDesp += "超出控制值"+formatData(Math.abs(this.changeRate- this.rateAlarmValue));
		        } else if (AlarmTypeEnum.ALARM.getValue().equals(this.alarmTypeId)) {
		       	 //报警
		        	alarmDesp += "超出报警值"+formatData(Math.abs(this.changeRate- this.rateAlarmValue));
		        } else if (AlarmTypeEnum.WARN.getValue().equals(this.alarmTypeId)) {
		       	 //预警
		        	alarmDesp += "超出预警值"+formatData(Math.abs(this.changeRate- this.rateAlarmValue));
		        } else {
		       	 
		        }
	        alarmDesp += ""+SpringContextHolder.getBean(MonitorTypeParaCache.class).get(this.getMonitorTypeCode()).getUnit() + "/d";

		}
		
        
		return alarmDesp;
	}
	
	private String formatData(double d) {
		String result = "";
		result = String.format("%.2f", d);
		return result;
	}
	
}
