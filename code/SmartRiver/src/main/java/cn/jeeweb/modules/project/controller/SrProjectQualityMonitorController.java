package cn.jeeweb.modules.project.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectQualityMonitor;

/**   
 * @Title: 工程质量检测
 * @Description: 工程质量检测
 * @author wsh
 * @date 2018-12-13 19:29:51
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectqualitymonitor")
@RequiresPathPermission("project:srprojectqualitymonitor")
public class SrProjectQualityMonitorController extends BaseCRUDController<SrProjectQualityMonitor, String> {

}
