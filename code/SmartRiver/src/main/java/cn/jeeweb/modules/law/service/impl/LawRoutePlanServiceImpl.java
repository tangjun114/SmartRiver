package cn.jeeweb.modules.law.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.law.mapper.LawRoutePlanMapper;
import cn.jeeweb.modules.law.entity.LawRoutePlan;
import cn.jeeweb.modules.law.service.ILawRoutePlanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 路线规划
 * @Description: 路线规划
 * @author 李永雷
 * @date 2018-11-13 09:08:03
 * @version V1.0   
 *
 */
@Transactional
@Service("lawRoutePlanService")
public class LawRoutePlanServiceImpl  extends CommonServiceImpl<LawRoutePlanMapper,LawRoutePlan> implements  ILawRoutePlanService {

}
