package cn.jeeweb.modules.task.task;


import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.SpringContextHolder;
import cn.jeeweb.modules.constant.DataResultConst;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeTotalData;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeMonDataService;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeTotalDataService;
import cn.jeeweb.modules.water.entity.WaterDrainOutletReport;
import cn.jeeweb.modules.water.entity.WaterDrainOutletResult;
import cn.jeeweb.modules.water.service.IWaterDetectResultService;
import cn.jeeweb.modules.water.service.IWaterDrainOutletReportService;
import cn.jeeweb.modules.water.service.IWaterDrainOutletResultService;
import com.ff.common.util.format.DateUtil;
import com.ff.common.util.validate.ValidatorUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component("WaterOutReporTask")
public class WaterOutReporTask {
    @Autowired
    IWaterDrainOutletReportService waterDrainOutletReportService;
    @Autowired
    IWaterDrainOutletResultService waterDrainOutletResultService;

    public final Logger log = Logger.getLogger(this.getClass());


    public void run() {
        log.info(" run start......................................" + (new Date()));
        Date yesDay = DateUtil.dayCalculate(DateUtil.getCurrentDate(), -1);
        sum(DateUtil.DateToString(yesDay, "yyy-MM-dd"));
        log.error("run finish");

    }

    public void sum(String day) {
        if (ValidatorUtil.isEmpty(day)) {
            day = DateUtil.DateToString(DateUtil.getCurrentDate(), "yyy-MM-dd");
        }

        List<WaterDrainOutletReport> objList = new ArrayList<>();

        try {


            String end = day + " 23:59:59";


            EntityWrapper<WaterDrainOutletResult> entityWrapper = new EntityWrapper<WaterDrainOutletResult>(WaterDrainOutletResult.class);
            entityWrapper.lt("create_time", end);
            entityWrapper.ge("create_time", day);
            entityWrapper.setSqlSelect(" count(id) as waterAmount,"
                    + "outlet_name as outletName, "
                    + "monitor_type as monitorType, "
                    + "water_quality as waterQuality ");
            entityWrapper.groupBy("outlet_name");
            entityWrapper.groupBy("monitor_type");
            entityWrapper.groupBy("water_quality");
            List<WaterDrainOutletResult> dataList = waterDrainOutletResultService.selectList(entityWrapper);

            for (WaterDrainOutletResult temp : dataList) {
                EntityWrapper<WaterDrainOutletReport> entityWrapper1 = new EntityWrapper<>(WaterDrainOutletReport.class);
                entityWrapper1.eq("outlet_name", temp.getOutletName());
                entityWrapper1.eq("monitor_type", temp.getMonitorType());
                entityWrapper1.eq("water_quality", temp.getWaterQuality());
                Object obj = waterDrainOutletReportService.selectObj(entityWrapper1);
                if (obj != null) {
                    WaterDrainOutletReport report = (WaterDrainOutletReport) obj;
                    report.setCount(report.getCount() + Integer.valueOf(temp.getWaterAmount()));
                    waterDrainOutletReportService.updateById(report);
                } else {
                    WaterDrainOutletReport report = new WaterDrainOutletReport();
                    report.setCount(Integer.valueOf(temp.getWaterAmount()));
                    report.setMonitorType(temp.getMonitorType());
                    report.setOutletName(temp.getOutletName());
                    report.setWaterQuality(temp.getWaterQuality());
                    waterDrainOutletReportService.insert(report);
                }

            }


        } catch (Exception e) {
            log.error("run error", e);

        }
    }

    public void run1() {
        for (int i = 0; i < 10; i++) {
            log.info(i + " run1......................................" + (new Date()));
        }
    }
}
