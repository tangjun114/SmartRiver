package cn.jeeweb.modules.flood.client.floodinfoapi;

import cn.jeeweb.modules.flood.client.GetFloodInfoClient;
import cn.jeeweb.modules.flood.client.entity.ReqParam;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author:Wish
 * @Date: Created in 2018/12/14 10:08
 **/
public class FloodRainFullClient extends GetFloodInfoClient {

    private static String interfaceName = "rainningApi.getHourRainDetail";


    private Map<String, Object> getMap(String stcd, String startTime, String endTime) {
        Map<String, Object> map = new HashMap<>();
        map.put("stcd", stcd);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        return map;
    }


    public FloodRainFullClient setReqParam(String stcd, String startTime, String endTime) {
        ReqParam reqParam = new ReqParam();
        reqParam.setInterfaceName(interfaceName);
        reqParam.setParams(getMap(stcd, startTime, endTime));
        reqParam.setToken(token);
        reqParams.add(reqParam);
        return this;
    }

    public static void main(String[] args) {
        GetFloodInfoClient client = new FloodRainFullClient()
                .setReqParam("81202701", "2018-11-12 08:00", "2018-11-27 17:00");
        System.out.println(client.getFloodInfo());

    }
}
