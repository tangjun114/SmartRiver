package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemImplementLogMapper;
import cn.jeeweb.modules.supervise.entity.McProItemImplementLog;
import cn.jeeweb.modules.supervise.service.IMcProItemImplementLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目监控-实施日志
 * @Description: 项目监控-实施日志
 * @author shawloong
 * @date 2017-11-15 00:48:21
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemImplementLogService")
public class McProItemImplementLogServiceImpl  extends CommonServiceImpl<McProItemImplementLogMapper,McProItemImplementLog> implements  IMcProItemImplementLogService {

}
