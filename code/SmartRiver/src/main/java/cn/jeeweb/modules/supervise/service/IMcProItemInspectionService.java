package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemInspection;

/**   
 * @Title: 抽查记录
 * @Description: 抽查记录
 * @author jerry
 * @date 2018-06-04 10:36:48
 * @version V1.0   
 *
 */
public interface IMcProItemInspectionService extends ICommonService<McProItemInspection> {

}

