package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectScheduleMapper;
import cn.jeeweb.modules.project.entity.SrProjectSchedule;
import cn.jeeweb.modules.project.service.ISrProjectScheduleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目进度
 * @Description: 项目进度
 * @author wsh
 * @date 2018-12-10 20:41:29
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectScheduleService")
public class SrProjectScheduleServiceImpl  extends CommonServiceImpl<SrProjectScheduleMapper,SrProjectSchedule> implements  ISrProjectScheduleService {

}
