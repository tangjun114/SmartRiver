package cn.jeeweb.modules.sm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.sm.entity.McSmMonitorSubarea;
 
/**   
 * @Title: 系统管理-监测分区数据库控制层接口
 * @Description: 系统管理-监测分区数据库控制层接口
 * @author shawloong
 * @date 2017-11-05 18:34:12
 * @version V1.0   
 *
 */
public interface McSmMonitorSubareaMapper extends BaseMapper<McSmMonitorSubarea> {
    
}