package cn.jeeweb.modules.sm.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.sm.entity.McSmMonitorGroupType;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.entity.Dict;
import cn.jeeweb.modules.sys.utils.DictUtils;

/**   
 * @Title: 监测组类别
 * @Description: 监测组类别
 * @author jerry
 * @date 2018-03-14 15:54:31
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/sm/mcsmmonitorgrouptype")
@RequiresPathPermission("sm:mcsmmonitorgrouptype")
public class McSmMonitorGroupTypeController extends BaseCRUDController<McSmMonitorGroupType, String> {

	@Autowired
	IMcProjectService projectServce;
	@RequestMapping("/loadbyproject")
	@ResponseBody
	public BaseRspJson<List<McSmMonitorGroupType>> loadByProject(@RequestBody BaseReqJson request) {

	 
		BaseRspJson<List<McSmMonitorGroupType>> rsp = new BaseRspJson<List<McSmMonitorGroupType>>();
		Queryable queryable = this.GetFilterCondition(request);
		String projectId = (String) queryable.getValue("projectId");
		McProject project = projectServce.selectById(projectId);
		Dict dict = DictUtils.getDict("XIANGMULB",project.getProjectType());
		Wrapper<McSmMonitorGroupType> wrapper = new EntityWrapper<McSmMonitorGroupType>();
		if(null != dict)
		{
 			wrapper.in("code", dict.getRemarks());
		}
		List<McSmMonitorGroupType> obj = this.commonService.selectList(wrapper);
		rsp.setObj(obj);
		
		return rsp;
	}

	
}
