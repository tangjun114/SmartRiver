package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodAssetsStockRecord;
 
/**   
 * @Title: 库存统计查询数据库控制层接口
 * @Description: 库存统计查询数据库控制层接口
 * @author liyonglei
 * @date 2018-11-23 15:27:46
 * @version V1.0   
 *
 */
public interface FloodAssetsStockRecordMapper extends BaseMapper<FloodAssetsStockRecord> {
    
}