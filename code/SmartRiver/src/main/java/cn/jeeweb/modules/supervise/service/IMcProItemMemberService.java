package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemMember;

/**   
 * @Title: 项目人员
 * @Description: 项目人员
 * @author Jerry
 * @date 2017-12-27 13:40:21
 * @version V1.0   
 *
 */
public interface IMcProItemMemberService extends ICommonService<McProItemMember> {

}

