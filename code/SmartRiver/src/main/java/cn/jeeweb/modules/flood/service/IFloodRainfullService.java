package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodRainfull;

/**   
 * @Title: 雨量监测
 * @Description: 雨量监测
 * @author wsh
 * @date 2018-11-27 18:15:03
 * @version V1.0   
 *
 */
public interface IFloodRainfullService extends ICommonService<FloodRainfull> {

}

