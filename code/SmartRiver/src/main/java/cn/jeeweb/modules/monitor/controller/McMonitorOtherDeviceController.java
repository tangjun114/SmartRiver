package cn.jeeweb.modules.monitor.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.monitor.entity.McMonitorOtherDevice;

/**   
 * @Title: 其他设备
 * @Description: 其他设备
 * @author jerry
 * @date 2018-01-26 15:29:54
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/monitor/mcmonitorotherdevice")
@RequiresPathPermission("monitor:mcmonitorotherdevice")
public class McMonitorOtherDeviceController extends McOrgBaseController<McMonitorOtherDevice, String> {

}
