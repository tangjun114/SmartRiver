package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringHole;
 
/**   
 * @Title: 项目监控-测点设置-监测项-测孔数据库控制层接口
 * @Description: 项目监控-测点设置-监测项-测孔数据库控制层接口
 * @author shawloong
 * @date 2017-11-15 23:22:45
 * @version V1.0   
 *
 */
public interface McProItemMeasuringHoleMapper extends BaseMapper<McProItemMeasuringHole> {
    
}