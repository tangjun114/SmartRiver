package cn.jeeweb.modules.law.controller;


import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.core.utils.sms.SmsManager;
import cn.jeeweb.core.utils.sms.data.SmsTemplate;
import com.ff.common.util.format.DateUtil;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.law.entity.LawEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author liyonglei
 * @version V1.0
 * @Title: 违法事件
 * @Description: 违法事件
 * @date 2018-11-13 08:40:53
 */
@Controller
@RequestMapping("${admin.url.prefix}/law/lawevent")
@RequiresPathPermission("law:lawevent")
public class LawEventController extends BaseCRUDController<LawEvent, String> {

    private String phone = "";


    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<LawEvent> entityWrapper, HttpServletRequest request, HttpServletResponse response) {

        super.preAjaxList(queryable, entityWrapper, request, response);
        String sort = request.getParameter("sort");
        String order = request.getParameter("order");
        if (sort.equals("id") && order.equals("asc")) {
            queryable.addOrder(Sort.Direction.DESC, "occurTime");
            queryable.addOrder(Sort.Direction.DESC, "eventLevel");
        }
    }

    @Override
    public void afterSave(LawEvent entity, HttpServletRequest request, HttpServletResponse response) {
        super.afterSave(entity, request, response);
        if (entity.getEventLevel().equals("严重")) {
            try {
                String templateContent = "[违法事件通知] 于{1}发生严重违法事件：{2}。";
                SmsTemplate smsTemplate = SmsTemplate.newTemplateByContent(templateContent);
                SmsManager.getSmsManager().send(phone, smsTemplate, DateUtil.DateToString(entity.getOccurTime()), entity.getEventName());
            } catch (Exception e) {
                log.error("发送违法事件短信出错" + ExceptionUtils.getFullStackTrace(e));
            }
        }
    }
}
