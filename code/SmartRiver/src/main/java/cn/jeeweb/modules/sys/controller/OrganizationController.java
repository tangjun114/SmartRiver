package cn.jeeweb.modules.sys.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.enums.SqlLike;

import cn.jeeweb.core.common.controller.BaseTreeController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.monitor.entity.McMonitorOrg;
import cn.jeeweb.modules.monitor.service.IMcMonitorOrgService;
import cn.jeeweb.modules.sys.entity.Organization;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.service.IOrganizationService;
import cn.jeeweb.modules.sys.utils.UserUtils;

@Controller
@RequestMapping("${admin.url.prefix}/sys/organization")
@RequiresPathPermission("sys:organization")
public class OrganizationController extends BaseTreeController<Organization, String> {
	@Autowired
	private IOrganizationService organizationService;
	@Autowired
	private IMcMonitorOrgService mcMonitorOrgService;
	/**
	 * 保存数据之后
	 * 
	 * @param entity
	 * @param request
	 * @param response
	 */
	@Override
	public void afterSave(Organization organiz, HttpServletRequest request, HttpServletResponse response) {
		EntityWrapper<Organization> treeEntityWrapper = new EntityWrapper<Organization>();
		treeEntityWrapper.setTableAlias("t.");
		treeEntityWrapper.eq("id", organiz.getId());
		List<Organization> organizationList = this.organizationService.selectTreeList(treeEntityWrapper);
		Organization organization = null;
		if(organizationList.size()>0){//重新从数据库查询的目的是为了判断是否是叶子节点  只有也自己点才有机构
			organization=organizationList.get(0);
		}else{
			organization = organiz;
		}
		//判断是否是检测机构
		if(null!=organization.getIsMonitor()&&"1".equals(organization.getIsMonitor())){//判断是否是检测机构
			McMonitorOrg monitorOrg = new McMonitorOrg();
			monitorOrg.setId(organization.getId());//由于是对应的关系故将其设为同组织一个id
			monitorOrg.setOrgParentId(organization.getParentId());
			monitorOrg.setOrg(organization.getId());
			monitorOrg.setName(organization.getName());
			EntityWrapper<McMonitorOrg> wrapper = new EntityWrapper<McMonitorOrg>();
			wrapper.eq("org", organization.getId());
			List<McMonitorOrg> monitorList = mcMonitorOrgService.selectList(wrapper);
			if(monitorList.size()>0){//有则修改
				mcMonitorOrgService.updateById(monitorOrg);
			}else{//无则添加
				mcMonitorOrgService.insert(monitorOrg);
			}
		}
		
	}
	 
 

	@Override
	public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preList(model, request, response);
		 
	}



	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<Organization> entityWrapper, HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preAjaxList(queryable, entityWrapper, request, response);
		 
		{
			User user = UserUtils.getUser();
 			String organizationIds = user.getOrganizationIds();
 			entityWrapper.eq("id", organizationIds);
			entityWrapper.or();
			entityWrapper.like(true,"parent_ids", organizationIds,SqlLike.LEFT);
			entityWrapper.andNew();
 		}
	}
	
 


	
}
