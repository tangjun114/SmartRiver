package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemCollection;

/**   
 * @Title: 项目中的通信设备
 * @Description: 项目中的通信设备
 * @author shawloong
 * @date 2018-03-20 11:42:24
 * @version V1.0   
 *
 */
public interface IMcProItemCollectionService extends ICommonService<McProItemCollection> {

}

