package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorRealdata;
 
/**   
 * @Title: 监测原始记录信息数据库控制层接口
 * @Description: 监测原始记录信息数据库控制层接口
 * @author Aether
 * @date 2017-11-29 23:54:11
 * @version V1.0   
 *
 */
public interface McMonitorRealdataMapper extends BaseMapper<McMonitorRealdata> {
    
}