package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McMonitorResultStatistics;
 
/**   
 * @Title: 监测结果统计数据库控制层接口
 * @Description: 监测结果统计数据库控制层接口
 * @author aether
 * @date 2018-01-24 21:32:05
 * @version V1.0   
 *
 */
public interface McMonitorResultStatisticsMapper extends BaseMapper<McMonitorResultStatistics> {
    
}