package cn.jeeweb.modules.repo.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.repo.entity.McRepoStandard;

/**   
 * @Title: 规范资料
 * @Description: 规范资料
 * @author shawloong
 * @date 2017-10-04 15:16:45
 * @version V1.0   
 *
 */
public interface IMcRepoStandardService extends ICommonService<McRepoStandard> {

}

