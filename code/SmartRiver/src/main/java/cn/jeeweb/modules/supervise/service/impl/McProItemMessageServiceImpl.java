package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemMessageMapper;
import cn.jeeweb.modules.supervise.entity.McProItemMessage;
import cn.jeeweb.modules.supervise.service.IMcProItemMessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 报警短信
 * @Description: 报警短信
 * @author jrrey
 * @date 2018-05-30 16:34:13
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemMessageService")
public class McProItemMessageServiceImpl  extends CommonServiceImpl<McProItemMessageMapper,McProItemMessage> implements  IMcProItemMessageService {

}
