package cn.jeeweb.modules.flood.client.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

/**
 * @Author:Wish
 * @Date: Created in 2018/12/14 11:27
 **/
public class Criterias implements Serializable {
    private static final long serialVersionUID = 1L;
    @JSONField(name = "STCD")
    private String stcd;
    @JSONField(name = "TM_12")
    private List<String> tm_12;
    @JSONField(name = "TM_8")
    private String tm_8;

    public String getStcd() {
        return stcd;
    }

    public void setStcd(String stcd) {
        this.stcd = stcd;
    }

    public List<String> getTm_12() {
        return tm_12;
    }

    public void setTm_12(List<String> tm_12) {
        this.tm_12 = tm_12;
    }

    public String getTm_8() {
        return tm_8;
    }

    public void setTm_8(String tm_8) {
        this.tm_8 = tm_8;
    }
}
