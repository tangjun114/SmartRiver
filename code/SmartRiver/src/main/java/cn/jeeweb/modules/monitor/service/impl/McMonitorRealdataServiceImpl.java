package cn.jeeweb.modules.monitor.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.monitor.mapper.McMonitorRealdataMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorRealdata;
import cn.jeeweb.modules.monitor.service.IMcMonitorRealdataService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 监测原始记录信息
 * @Description: 监测原始记录信息
 * @author Aether
 * @date 2017-11-29 23:54:11
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorRealdataService")
public class McMonitorRealdataServiceImpl  extends CommonServiceImpl<McMonitorRealdataMapper,McMonitorRealdata> implements  IMcMonitorRealdataService {

}
