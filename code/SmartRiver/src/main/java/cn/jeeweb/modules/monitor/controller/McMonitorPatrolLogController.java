package cn.jeeweb.modules.monitor.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.query.data.Page;
import cn.jeeweb.core.query.data.PageRequest;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort.Direction;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolLog;
import cn.jeeweb.modules.monitor.service.IMcMonitorPatrolLogService;
import cn.jeeweb.modules.supervise.controller.McProjectBaseController;
import cn.jeeweb.modules.sys.utils.DictUtils;

/**   
 * @Title: 巡检日志
 * @Description: 巡检日志
 * @author shawloong
 * @date 2017-10-15 23:43:36
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/monitor/mcmonitorpatrollog")
@RequiresPathPermission("monitor:mcmonitorpatroldetail")
public class McMonitorPatrolLogController extends McProjectBaseController<McMonitorPatrolLog, String> {
	
	@Autowired
	private IMcMonitorPatrolLogService logService;

	//巡检表格没有create_date
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McMonitorPatrolLog> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
		String  projectId = request.getParameter("projectId");
		if(!StringUtils.isEmpty(projectId)){
			entityWrapper.eq("projectId", projectId);
			entityWrapper.orderBy("patrolDate", false);
		}
	}

	@RequestMapping(value = "/latest")
	public String latest(Model model, HttpServletRequest request, HttpServletResponse response) {
		String projectId = request.getParameter("projectId");

		Queryable query = QueryRequest.newQueryable();
		query.addCondition("projectId", projectId);
		
		query.addOrder(Direction.DESC,"patrolDate");
	
		query.setPageable(new PageRequest(1,1));
		
		Page<McMonitorPatrolLog> dataList = logService.list(query);
		McMonitorPatrolLog entity;
		if (dataList != null && dataList.getSize()>0 && dataList.getContent().size()>0) {
			entity = dataList.getContent().get(0);
			String status = DictUtils.getDictLabel(entity.getStatus(), "mc_monitor_patrol_status", "");
			entity.setStatus(status);
		} else {
			entity = new McMonitorPatrolLog();
		} 

		model.addAttribute("detail", entity);
		
		return display("detail");
	}

	@Override
	public String showIndex(Model model, HttpServletRequest request, HttpServletResponse response) {
		String projectId = request.getParameter("projectId");

		Queryable query = QueryRequest.newQueryable();
		//Queryable qy = super.GetFilterCondition(request);
		//qy.addOrder(Direction.ASC, "collectTime");
		query.addCondition("projectId", projectId);
		
		query.addOrder(Direction.DESC,"patrolDate");
	
		query.setPageable(new PageRequest(1,1));
		
		Page<McMonitorPatrolLog> dataList = logService.list(query);
		McMonitorPatrolLog entity;
		if (dataList != null && dataList.getSize()>0 && dataList.getContent().size()>0) {
			entity = dataList.getContent().get(0);
			String status = DictUtils.getDictLabel(entity.getStatus(), "mc_monitor_patrol_status", "");
			entity.setStatus(status);
		} else {
			entity = new McMonitorPatrolLog();
		} 

		model.addAttribute("detail", entity);
		return super.showIndex(model, request, response);
	}

	
}
