package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemCollection;
 
/**   
 * @Title: 项目中的通信设备数据库控制层接口
 * @Description: 项目中的通信设备数据库控制层接口
 * @author shawloong
 * @date 2018-03-20 11:42:24
 * @version V1.0   
 *
 */
public interface McProItemCollectionMapper extends BaseMapper<McProItemCollection> {
    
}