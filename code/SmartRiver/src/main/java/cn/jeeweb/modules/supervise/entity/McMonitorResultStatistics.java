package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 监测结果统计
 * @Description: 监测结果统计
 * @author aether
 * @date 2018-01-24 21:32:05
 * @version V1.0   
 *
 */
@TableName("mc_monitor_result_statistics")
@SuppressWarnings("serial")
public class McMonitorResultStatistics extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**监测项id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**最大值*/
    @TableField(value = "max_value")
	private Double maxValue;
    /**数值*/
    @TableField(value = "now_value")
	private Double nowValue;
    /**测点id*/
    @TableField(value = "measuring_point_id")
	private String measuringPointId;
    /**测点编号*/
    @TableField(value = "measuring_point_code")
	private String measuringPointCode;
    /**报警值*/
    @TableField(value = "alarm_value")
	private Double alarmValue;
    /**控制值*/
    @TableField(value = "control_value")
	private Double controlValue;
    /**累计值*/
    @TableField(value = "sum_value")
	private Double sumValue;
    /**变化率*/
    @TableField(value = "change_rate")
	private Double changeRate;
    /**状态*/
    @TableField(value = "status")
	private String status;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  maxValue
	 *@return: Double  最大值
	 */
	public Double getMaxValue(){
		return this.maxValue;
	}

	/**
	 * 设置  maxValue
	 *@param: maxValue  最大值
	 */
	public void setMaxValue(Double maxValue){
		this.maxValue = maxValue;
	}
	/**
	 * 获取  nowValue
	 *@return: Double  数值
	 */
	public Double getNowValue(){
		return this.nowValue;
	}

	/**
	 * 设置  nowValue
	 *@param: nowValue  数值
	 */
	public void setNowValue(Double nowValue){
		this.nowValue = nowValue;
	}
	/**
	 * 获取  measuringPointId
	 *@return: String  测点id
	 */
	public String getMeasuringPointId(){
		return this.measuringPointId;
	}

	/**
	 * 设置  measuringPointId
	 *@param: measuringPointId  测点id
	 */
	public void setMeasuringPointId(String measuringPointId){
		this.measuringPointId = measuringPointId;
	}
	/**
	 * 获取  measuringPointCode
	 *@return: String  测点编号
	 */
	public String getMeasuringPointCode(){
		return this.measuringPointCode;
	}

	/**
	 * 设置  measuringPointCode
	 *@param: measuringPointCode  测点编号
	 */
	public void setMeasuringPointCode(String measuringPointCode){
		this.measuringPointCode = measuringPointCode;
	}
	/**
	 * 获取  alarmValue
	 *@return: Double  报警值
	 */
	public Double getAlarmValue(){
		return this.alarmValue;
	}

	/**
	 * 设置  alarmValue
	 *@param: alarmValue  报警值
	 */
	public void setAlarmValue(Double alarmValue){
		this.alarmValue = alarmValue;
	}
	/**
	 * 获取  controlValue
	 *@return: Double  控制值
	 */
	public Double getControlValue(){
		return this.controlValue;
	}

	/**
	 * 设置  controlValue
	 *@param: controlValue  控制值
	 */
	public void setControlValue(Double controlValue){
		this.controlValue = controlValue;
	}
	/**
	 * 获取  sumValue
	 *@return: Double  累计值
	 */
	public Double getSumValue(){
		return this.sumValue;
	}

	/**
	 * 设置  sumValue
	 *@param: sumValue  累计值
	 */
	public void setSumValue(Double sumValue){
		this.sumValue = sumValue;
	}
	/**
	 * 获取  changeRate
	 *@return: Double  变化率
	 */
	public Double getChangeRate(){
		return this.changeRate;
	}

	/**
	 * 设置  changeRate
	 *@param: changeRate  变化率
	 */
	public void setChangeRate(Double changeRate){
		this.changeRate = changeRate;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	
}
