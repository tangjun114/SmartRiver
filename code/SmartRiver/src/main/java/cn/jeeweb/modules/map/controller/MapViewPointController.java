package cn.jeeweb.modules.map.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.map.entity.MapViewPoint;

/**   
 * @Title: 观察点管理
 * @Description: 观察点管理
 * @author liyonglei
 * @date 2018-11-14 20:50:41
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/map/mapviewpoint")
@RequiresPathPermission("map:mapviewpoint")
public class MapViewPointController extends BaseCRUDController<MapViewPoint, String> {

}
