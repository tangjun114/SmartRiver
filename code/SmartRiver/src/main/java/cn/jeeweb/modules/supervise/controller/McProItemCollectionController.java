package cn.jeeweb.modules.supervise.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProItemCollection;

/**   
 * @Title: 项目中的通信设备
 * @Description: 项目中的通信设备
 * @author shawloong
 * @date 2018-03-20 11:42:24
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemcollection")
@RequiresPathPermission("supervise:mcproitemcollection")
public class McProItemCollectionController extends McProjectBaseController<McProItemCollection, String> {

}
