package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodSiteWarn;
 
/**   
 * @Title: 站点警报阈值数据库控制层接口
 * @Description: 站点警报阈值数据库控制层接口
 * @author wsh
 * @date 2018-12-02 23:09:31
 * @version V1.0   
 *
 */
public interface FloodSiteWarnMapper extends BaseMapper<FloodSiteWarn> {
    
}