package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectFundsMapper;
import cn.jeeweb.modules.project.entity.SrProjectFunds;
import cn.jeeweb.modules.project.service.ISrProjectFundsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 工程经费
 * @Description: 工程经费
 * @author wsh
 * @date 2018-12-10 20:42:10
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectFundsService")
public class SrProjectFundsServiceImpl  extends CommonServiceImpl<SrProjectFundsMapper,SrProjectFunds> implements  ISrProjectFundsService {

}
