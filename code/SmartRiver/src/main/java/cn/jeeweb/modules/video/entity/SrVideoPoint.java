package cn.jeeweb.modules.video.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 视频站点管理
 * @Description: 视频站点管理
 * @author wsh
 * @date 2019-04-01 14:30:31
 * @version V1.0   
 *
 */
@TableName("sr_video_point")
@SuppressWarnings("serial")
public class SrVideoPoint extends AbstractEntity<String> {

    /**主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建者名称*/
    @TableField(value = "create_by_name")
	private String createByName;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新者名称*/
    @TableField(value = "update_by_name")
	private String updateByName;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**名称*/
    @TableField(value = "name")
	private String name;
    /**精度*/
    @TableField(value = "longitude")
	private String longitude;
    /**纬度*/
    @TableField(value = "latitude")
	private String latitude;
    /**地址*/
    @TableField(value = "addr")
	private String addr;
    /**视频地址*/
    @TableField(value = "url")
	private String url;
    /**account*/
    @TableField(value = "account")
	private String account;
    /**password*/
    @TableField(value = "password")
	private String password;
	
	/**
	 * 获取  id
	 *@return: String  主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createByName
	 *@return: String  创建者名称
	 */
	public String getCreateByName(){
		return this.createByName;
	}

	/**
	 * 设置  createByName
	 *@param: createByName  创建者名称
	 */
	public void setCreateByName(String createByName){
		this.createByName = createByName;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateByName
	 *@return: String  更新者名称
	 */
	public String getUpdateByName(){
		return this.updateByName;
	}

	/**
	 * 设置  updateByName
	 *@param: updateByName  更新者名称
	 */
	public void setUpdateByName(String updateByName){
		this.updateByName = updateByName;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  name
	 *@return: String  名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  longitude
	 *@return: String  精度
	 */
	public String getLongitude(){
		return this.longitude;
	}

	/**
	 * 设置  longitude
	 *@param: longitude  精度
	 */
	public void setLongitude(String longitude){
		this.longitude = longitude;
	}
	/**
	 * 获取  latitude
	 *@return: String  纬度
	 */
	public String getLatitude(){
		return this.latitude;
	}

	/**
	 * 设置  latitude
	 *@param: latitude  纬度
	 */
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
	/**
	 * 获取  addr
	 *@return: String  地址
	 */
	public String getAddr(){
		return this.addr;
	}

	/**
	 * 设置  addr
	 *@param: addr  地址
	 */
	public void setAddr(String addr){
		this.addr = addr;
	}
	/**
	 * 获取  url
	 *@return: String  视频地址
	 */
	public String getUrl(){
		return this.url;
	}

	/**
	 * 设置  url
	 *@param: url  视频地址
	 */
	public void setUrl(String url){
		this.url = url;
	}
	/**
	 * 获取  account
	 *@return: String  account
	 */
	public String getAccount(){
		return this.account;
	}

	/**
	 * 设置  account
	 *@param: account  account
	 */
	public void setAccount(String account){
		this.account = account;
	}
	/**
	 * 获取  password
	 *@return: String  password
	 */
	public String getPassword(){
		return this.password;
	}

	/**
	 * 设置  password
	 *@param: password  password
	 */
	public void setPassword(String password){
		this.password = password;
	}
	
}
