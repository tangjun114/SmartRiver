package cn.jeeweb.modules.repo.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.repo.mapper.McRepoMaterialMapper;
import cn.jeeweb.modules.repo.entity.McRepoMaterial;
import cn.jeeweb.modules.repo.service.IMcRepoMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;

/**   
 * @Title: 资料下载
 * @Description: 资料下载
 * @author shawloong
 * @date 2017-10-04 13:26:47
 * @version V1.0   
 *
 */
@Transactional
@Service("mcRepoMaterialService")
public class McRepoMaterialServiceImpl  extends CommonServiceImpl<McRepoMaterialMapper,McRepoMaterial> implements  IMcRepoMaterialService {
	
	@Override
	public boolean insert(McRepoMaterial mcRepoMaterial) {
		// 保存主表
		super.insert(mcRepoMaterial);
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(McRepoMaterial mcRepoMaterial) {
		try {
			// 更新主表
			super.insertOrUpdate(mcRepoMaterial);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}
	
	
	
}
