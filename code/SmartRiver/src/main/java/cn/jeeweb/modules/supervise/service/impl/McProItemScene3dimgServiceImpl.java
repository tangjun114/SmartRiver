package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemScene3dimgMapper;
import cn.jeeweb.modules.supervise.entity.McProItemScene3dimg;
import cn.jeeweb.modules.supervise.service.IMcProItemScene3dimgService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目监控-项目三维图
 * @Description: 项目监控-项目三维图
 * @author shawloong
 * @date 2017-11-02 21:39:34
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemScene3dimgService")
public class McProItemScene3dimgServiceImpl  extends CommonServiceImpl<McProItemScene3dimgMapper,McProItemScene3dimg> implements  IMcProItemScene3dimgService {

}
