package cn.jeeweb.modules.water.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.water.entity.WaterDrainOutlet;

/**   
 * @Title: 排污口管理
 * @Description: 排污口管理
 * @author liyonglei
 * @date 2018-11-14 19:33:01
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/water/waterdrainoutlet")
@RequiresPathPermission("water:waterdrainoutlet")
public class WaterDrainOutletController extends BaseCRUDController<WaterDrainOutlet, String> {

}
