package cn.jeeweb.modules.webservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.common.web.controller.FFTableController;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeMonDataService;
import cn.jeeweb.modules.webservice.UploadDataModel;

@Controller
@RequestMapping("${api.url.prefix}/realtimemondata")
public class RealTimeMonDataApi extends FFTableController<McProItemRealTimeMonData>
{
	@Autowired
	private IMcProItemRealTimeMonDataService dataSerivice;
	@RequestMapping("/upload")
	@ResponseBody
	public BaseRspJson<UploadDataModel> upload(@RequestBody BaseReqJson<UploadDataModel> request)
	{
		BaseRspJson<UploadDataModel> rsp = new BaseRspJson<UploadDataModel>();
 
		UploadDataModel obj = this.getObj(request,UploadDataModel.class);
		
		dataSerivice.create(obj);;
		
 		rsp.setObj(obj);
   		return rsp;
	}
	
	@RequestMapping("/valid")
	@ResponseBody
	public BaseRspJson<UploadDataModel> valid(@RequestBody BaseReqJson<UploadDataModel> request)
	{
		BaseRspJson<UploadDataModel> rsp = new BaseRspJson<UploadDataModel>();
 
		UploadDataModel  obj = this.getObj(request,UploadDataModel.class);
		
		dataSerivice.valid(obj);
		
 	 
   		return rsp;
	}
	
}
