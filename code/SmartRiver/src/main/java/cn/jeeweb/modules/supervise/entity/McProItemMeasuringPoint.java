package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 项目监控-测点设置-监测项-测点
 * @Description: 项目监控-测点设置-监测项-测点
 * @author shawloong
 * @date 2018-02-23 17:16:30
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_measuring_point")
@SuppressWarnings("serial")
public class McProItemMeasuringPoint extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**调试模式*/
    @TableField(value = "debug")
	private String debug;
    /**测点编号*/
    @TableField(value = "measuring_point_code")
	private String measuringPointCode;
    /**继承id*/
    @TableField(value = "inherit_id")
	private String inheritId;
    /**继承名*/
    @TableField(value = "inherit_name")
	private String inheritName;
    /**继承*/
    @TableField(value = "inherit_check")
	private String inheriCheck;
    /**监测点类型Val*/
    @TableField(value = "measuring_point_type")
	private String measuringPointType;
    /**监测点类型*/
    @TableField(value = "measuring_point_type_name")
	private String measuringPointTypeName;
    /**监测仪器类型Val*/
    @TableField(value = "device_type_id")
	private String deviceTypeId;
    /**监测仪器类型*/
    @TableField(value = "device_type_name")
	private String deviceTypeName;
    /**监测仪器型号VaL*/
    @TableField(value = "device_model")
	private String deviceModel;
    /**监测仪器型号*/
    @TableField(value = "device_model_name")
	private String deviceModelName;
    /**极坐标方向*/
    @TableField(value = "polar_coordinates")
	private String polarCoordinates;
    /**初始值次数*/
    @TableField(value = "initial_value_times")
	private String initialValueTimes;
    /**报警值设置类型*/
    @TableField(value = "alarm_value_type")
	private String alarmValueType;
    /**预警值(mm)*/
    @TableField(value = "warning_value")
	private String warningValue;
    /**报警值(mm)*/
    @TableField(value = "alarm_value")
	private String alarmValue;
    /**控制值(mm)*/
    @TableField(value = "control_value")
	private String controlValue;
    /**速率报警值(mm/d)*/
    @TableField(value = "rate_alarm_value")
	private String rateAlarmValue;
    /**报警组选择Val*/
    @TableField(value = "alarm_group")
	private String alarmGroup;
    /**报警组选择*/
    @TableField(value = "alarm_group_name")
	private String alarmGroupName;
    /**断面起点VaL*/
    @TableField(value = "section_start")
	private String sectionStart ;
    /**断面起点*/
    @TableField(value = "section_start_name")
	private String sectionStartName;
    /**断面终点VaL*/
    @TableField(value = "section_end")
	private String sectionEnd;
    /**断面终点*/
    @TableField(value = "section_end_name")
	private String sectionEndName;
    /**断面方向选择*/
    @TableField(value = "direction_selection")
	private String directionSelection;
    /**初始累计值(mm)*/
    @TableField(value = "initial_cumulative_value")
	private Double initialCumulativeValue = 0.0;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**监测项目Id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项目名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**初始累计值设*/
    @TableField(value = "initial_setting_extend")
	private String initialSettingExtend ="0";
    /**报警值设置扩展*/
    @TableField(value = "alarm_range_extend")
	private String alarmRangeExtend;
    /**支撑参数设置*/
    @TableField(value = "support_param")
	private String supportParam;
    /**对角点设置*/
    @TableField(value = "diagonal_setting")
	private String diagonalSetting;
    /**支撑类型Val*/
    @TableField(value = "support_type")
	private String supportType;
    /**支撑类型*/
    @TableField(value = "support_type_name")
	private String supportTypeName;
    /**传感器数量*/
    @TableField(value = "sensor_quantity")
	private String sensorQuantity;
    /**原始数据类型*/
    @TableField(value = "raw_data_type")
	private String rawDataType;
    /**传感器类别*/
    @TableField(value = "sensor_type")
	private String sensorType;
    /**传感器类别名称*/
    @TableField(value = "sensor_type_name")
	private String sensorTypeName;
    /**监测项类型编码*/
    @TableField(value = "monitor_item_type_code")
	private String monitorItemTypeCode;
    /**监测项类型名称*/
    @TableField(value = "monitor_item_type_name")
	private String monitorItemTypeName;
    /**支撑截面积*/
    @TableField(value = "support_zcjmj")
	private String supportZcjmj;
    /**钢筋截面积(mm)*/
    @TableField(value = "support_gjjmj")
	private String supportGjjmj;
    /**混凝土弹性模量*/
    @TableField(value = "support_hnttxml")
	private String supportHnttxml;
    /**钢筋弹性模量*/
    @TableField(value = "support_gjtxml")
	private String supportGjtxml;
    /**钢筋计截面积(mm²)*/
    @TableField(value = "support_gjjjmj")
	private String supportGjjjmj;
    /**传感器id1*/
    @TableField(value = "sensor_id1")
	private String sensorId1;
    /**传感器id2*/
    @TableField(value = "sensor_id2")
	private String sensorId2;
    /**传感器id3*/
    @TableField(value = "sensor_id3")
	private String sensorId3;
    /**传感器id4*/
    @TableField(value = "sensor_id4")
	private String sensorId4;
    /**传感器编号1*/
    @TableField(value = "sensor_num1")
	private String sensorNum1;
    /**传感器编号2*/
    @TableField(value = "sensor_num2")
	private String sensorNum2;
    /**传感器编号3*/
    @TableField(value = "sensor_num3")
	private String sensorNum3;
    /**传感器编号4*/
    @TableField(value = "sensor_num4")
	private String sensorNum4;
    /**传感器1安装后初始频率*/
    @TableField(value = "sensor_fq1")
	private String sensorFq1;
    /**传感器2安装后初始频率*/
    @TableField(value = "sensor_fq2")
	private String sensorFq2;
    /**传感器3安装后初始频率*/
    @TableField(value = "sensor_fq3")
	private String sensorFq3;
    /**传感器4安装后初始频率*/
    @TableField(value = "sensor_fq4")
	private String sensorFq4;
    /**初始值*/
    @TableField(value = "init_value")
	private Double initValue = 0.0;
    /**初始测量Id*/
    @TableField(value = "init_value_id")
	private String initValueId;
    /**监测分区ID*/
    @TableField(value = "subarea_id")
	private String subareaId;
    /**监测分区*/
    @TableField(value = "subarea_name")
	private String subareaName;
    /**监测仪器Id*/
    @TableField(value = "device_id")
	private String deviceId;
    /**监测仪器名称*/
    @TableField(value = "device_name")
	private String deviceName;
    /**负向预警值(mm)*/
    @TableField(value = "warn_value_ng")
	private String warnValueNg;
    /**负向报警值(mm)*/
    @TableField(value = "alarm_value_ng")
	private String alarmValueNg;
    /**负向控制值(mm)*/
    @TableField(value = "control_value_ng")
	private String controlValueNg;
    /**测控深度*/
    @TableField(value = "point_hole_depth")
	private String pointHoleDepth;
    /**传感器深度*/
    @TableField(value = "sensor1_depth")
	private String sensor1Depth;
    /**管口高程*/
    @TableField(value = "pipe_range_gkgc")
	private String pipeRangeGkgc;
    /**x值*/
    @TableField(value = "xvalue")
	private Double xvalue;
    /**y值*/
    @TableField(value = "yvalue")
	private Double yvalue;
    /**数据上传单元型号ID*/
    @TableField(value = "data_upload_unit_id")
	private String dataUploadUnitId;
    /**数据上传单元型号*/
    @TableField(value = "data_upload_unit_name")
	private String dataUploadUnitName;
    /**修正值*/
    @TableField(value = "update_value")
	private Double updateValue;
    /**存储深度对应的字段*/
    @TableField(value = "depth_ext")
	private String depthExt;
    /**开始深度*/
    @TableField(value = "start_depth")
	private String startDepth ="0.5";
    /**终止深度*/
    @TableField(value = "end_depth")
	private String endDepth = "20";
    /**步长*/
    @TableField(value = "step_depth")
	private String stepDepth ="0.5";
    /**裂缝位置*/
    @TableField(value = "rupture_position")
	private String rupturePosition;
    /**裂缝走向*/
    @TableField(value = "rupture_direction")
	private String ruptureDirection;
    //非持久
    @TableField(exist=false)    
	private String modifyDesp;
    /**修改文件说明*/
    @TableField(exist=false)
	private String modifyFilePath;
    /**修改文件名称*/
    @TableField(exist=false)
	private String modifyFileName;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  debug
	 *@return: String  调试模式
	 */
	public String getDebug(){
		return this.debug;
	}

	/**
	 * 设置  debug
	 *@param: debug  调试模式
	 */
	public void setDebug(String debug){
		this.debug = debug;
	}
	/**
	 * 获取  measuringPointCode
	 *@return: String  测点编号
	 */
	public String getMeasuringPointCode(){
		return this.measuringPointCode;
	}

	/**
	 * 设置  measuringPointCode
	 *@param: measuringPointCode  测点编号
	 */
	public void setMeasuringPointCode(String measuringPointCode){
		this.measuringPointCode = measuringPointCode;
	}
	/**
	 * 获取  inheritId
	 *@return: String  继承id
	 */
	public String getInheritId(){
		return this.inheritId;
	}

	/**
	 * 设置  inheritId
	 *@param: inheritId  继承id
	 */
	public void setInheritId(String inheritId){
		this.inheritId = inheritId;
	}
	/**
	 * 获取  inheritName
	 *@return: String  继承名
	 */
	public String getInheritName(){
		return this.inheritName;
	}

	/**
	 * 设置  inheritName
	 *@param: inheritName  继承名
	 */
	public void setInheritName(String inheritName){
		this.inheritName = inheritName;
	}
	/**
	 * 获取  inheriCheck
	 *@return: String  继承
	 */
	public String getInheriCheck(){
		return this.inheriCheck;
	}

	/**
	 * 设置  inheriCheck
	 *@param: inheriCheck  继承
	 */
	public void setInheriCheck(String inheriCheck){
		this.inheriCheck = inheriCheck;
	}
	/**
	 * 获取  measuringPointType
	 *@return: String  监测点类型Val
	 */
	public String getMeasuringPointType(){
		return this.measuringPointType;
	}

	/**
	 * 设置  measuringPointType
	 *@param: measuringPointType  监测点类型Val
	 */
	public void setMeasuringPointType(String measuringPointType){
		this.measuringPointType = measuringPointType;
	}
	/**
	 * 获取  measuringPointTypeName
	 *@return: String  监测点类型
	 */
	public String getMeasuringPointTypeName(){
		return this.measuringPointTypeName;
	}

	/**
	 * 设置  measuringPointTypeName
	 *@param: measuringPointTypeName  监测点类型
	 */
	public void setMeasuringPointTypeName(String measuringPointTypeName){
		this.measuringPointTypeName = measuringPointTypeName;
	}
	/**
	 * 获取  deviceTypeId
	 *@return: String  监测仪器类型Val
	 */
	public String getDeviceTypeId(){
		return this.deviceTypeId;
	}

	/**
	 * 设置  deviceTypeId
	 *@param: deviceTypeId  监测仪器类型Val
	 */
	public void setDeviceTypeId(String deviceTypeId){
		this.deviceTypeId = deviceTypeId;
	}
	/**
	 * 获取  deviceTypeName
	 *@return: String  监测仪器类型
	 */
	public String getDeviceTypeName(){
		return this.deviceTypeName;
	}

	/**
	 * 设置  deviceTypeName
	 *@param: deviceTypeName  监测仪器类型
	 */
	public void setDeviceTypeName(String deviceTypeName){
		this.deviceTypeName = deviceTypeName;
	}
	/**
	 * 获取  deviceModel
	 *@return: String  监测仪器型号VaL
	 */
	public String getDeviceModel(){
		return this.deviceModel;
	}

	/**
	 * 设置  deviceModel
	 *@param: deviceModel  监测仪器型号VaL
	 */
	public void setDeviceModel(String deviceModel){
		this.deviceModel = deviceModel;
	}
	/**
	 * 获取  deviceModelName
	 *@return: String  监测仪器型号
	 */
	public String getDeviceModelName(){
		return this.deviceModelName;
	}

	/**
	 * 设置  deviceModelName
	 *@param: deviceModelName  监测仪器型号
	 */
	public void setDeviceModelName(String deviceModelName){
		this.deviceModelName = deviceModelName;
	}
	/**
	 * 获取  polarCoordinates
	 *@return: String  极坐标方向
	 */
	public String getPolarCoordinates(){
		return this.polarCoordinates;
	}

	/**
	 * 设置  polarCoordinates
	 *@param: polarCoordinates  极坐标方向
	 */
	public void setPolarCoordinates(String polarCoordinates){
		this.polarCoordinates = polarCoordinates;
	}
	/**
	 * 获取  initialValueTimes
	 *@return: String  初始值次数
	 */
	public String getInitialValueTimes(){
		return this.initialValueTimes;
	}

	/**
	 * 设置  initialValueTimes
	 *@param: initialValueTimes  初始值次数
	 */
	public void setInitialValueTimes(String initialValueTimes){
		this.initialValueTimes = initialValueTimes;
	}
	/**
	 * 获取  alarmValueType
	 *@return: String  报警值设置类型
	 */
	public String getAlarmValueType(){
		return this.alarmValueType;
	}

	/**
	 * 设置  alarmValueType
	 *@param: alarmValueType  报警值设置类型
	 */
	public void setAlarmValueType(String alarmValueType){
		this.alarmValueType = alarmValueType;
	}
	/**
	 * 获取  warningValue
	 *@return: String  预警值(mm)
	 */
	public String getWarningValue(){
		return this.warningValue;
	}

	/**
	 * 设置  warningValue
	 *@param: warningValue  预警值(mm)
	 */
	public void setWarningValue(String warningValue){
		this.warningValue = warningValue;
	}
	/**
	 * 获取  alarmValue
	 *@return: String  报警值(mm)
	 */
	public String getAlarmValue(){
		return this.alarmValue;
	}

	/**
	 * 设置  alarmValue
	 *@param: alarmValue  报警值(mm)
	 */
	public void setAlarmValue(String alarmValue){
		this.alarmValue = alarmValue;
	}
	/**
	 * 获取  controlValue
	 *@return: String  控制值(mm)
	 */
	public String getControlValue(){
		return this.controlValue;
	}

	/**
	 * 设置  controlValue
	 *@param: controlValue  控制值(mm)
	 */
	public void setControlValue(String controlValue){
		this.controlValue = controlValue;
	}
	/**
	 * 获取  rateAlarmValue
	 *@return: String  速率报警值(mm/d)
	 */
	public String getRateAlarmValue(){
		return this.rateAlarmValue;
	}

	/**
	 * 设置  rateAlarmValue
	 *@param: rateAlarmValue  速率报警值(mm/d)
	 */
	public void setRateAlarmValue(String rateAlarmValue){
		this.rateAlarmValue = rateAlarmValue;
	}
	/**
	 * 获取  alarmGroup
	 *@return: String  报警组选择Val
	 */
	public String getAlarmGroup(){
		return this.alarmGroup;
	}

	/**
	 * 设置  alarmGroup
	 *@param: alarmGroup  报警组选择Val
	 */
	public void setAlarmGroup(String alarmGroup){
		this.alarmGroup = alarmGroup;
	}
	/**
	 * 获取  alarmGroupName
	 *@return: String  报警组选择
	 */
	public String getAlarmGroupName(){
		return this.alarmGroupName;
	}

	/**
	 * 设置  alarmGroupName
	 *@param: alarmGroupName  报警组选择
	 */
	public void setAlarmGroupName(String alarmGroupName){
		this.alarmGroupName = alarmGroupName;
	}
	/**
	 * 获取  sectionStart 
	 *@return: String  断面起点VaL
	 */
	public String getSectionStart (){
		return this.sectionStart ;
	}

	/**
	 * 设置  sectionStart 
	 *@param: sectionStart   断面起点VaL
	 */
	public void setSectionStart (String sectionStart ){
		this.sectionStart  = sectionStart ;
	}
	/**
	 * 获取  sectionStartName
	 *@return: String  断面起点
	 */
	public String getSectionStartName(){
		return this.sectionStartName;
	}

	/**
	 * 设置  sectionStartName
	 *@param: sectionStartName  断面起点
	 */
	public void setSectionStartName(String sectionStartName){
		this.sectionStartName = sectionStartName;
	}
	/**
	 * 获取  sectionEnd
	 *@return: String  断面终点VaL
	 */
	public String getSectionEnd(){
		return this.sectionEnd;
	}

	/**
	 * 设置  sectionEnd
	 *@param: sectionEnd  断面终点VaL
	 */
	public void setSectionEnd(String sectionEnd){
		this.sectionEnd = sectionEnd;
	}
	/**
	 * 获取  sectionEndName
	 *@return: String  断面终点
	 */
	public String getSectionEndName(){
		return this.sectionEndName;
	}

	/**
	 * 设置  sectionEndName
	 *@param: sectionEndName  断面终点
	 */
	public void setSectionEndName(String sectionEndName){
		this.sectionEndName = sectionEndName;
	}
	/**
	 * 获取  directionSelection
	 *@return: String  断面方向选择
	 */
	public String getDirectionSelection(){
		return this.directionSelection;
	}

	/**
	 * 设置  directionSelection
	 *@param: directionSelection  断面方向选择
	 */
	public void setDirectionSelection(String directionSelection){
		this.directionSelection = directionSelection;
	}
	/**
	 * 获取  initialCumulativeValue
	 *@return: Double  初始累计值(mm)
	 */
	public Double getInitialCumulativeValue(){
		return this.initialCumulativeValue;
	}

	/**
	 * 设置  initialCumulativeValue
	 *@param: initialCumulativeValue  初始累计值(mm)
	 */
	public void setInitialCumulativeValue(Double initialCumulativeValue){
		this.initialCumulativeValue = initialCumulativeValue;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项目Id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项目Id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项目名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项目名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  initialSettingExtend
	 *@return: String  初始累计值设
	 */
	public String getInitialSettingExtend(){
		return this.initialSettingExtend;
	}

	/**
	 * 设置  initialSettingExtend
	 *@param: initialSettingExtend  初始累计值设
	 */
	public void setInitialSettingExtend(String initialSettingExtend){
		this.initialSettingExtend = initialSettingExtend;
	}
	/**
	 * 获取  alarmRangeExtend
	 *@return: String  报警值设置扩展
	 */
	public String getAlarmRangeExtend(){
		return this.alarmRangeExtend;
	}

	/**
	 * 设置  alarmRangeExtend
	 *@param: alarmRangeExtend  报警值设置扩展
	 */
	public void setAlarmRangeExtend(String alarmRangeExtend){
		this.alarmRangeExtend = alarmRangeExtend;
	}
	/**
	 * 获取  supportParam
	 *@return: String  支撑参数设置
	 */
	public String getSupportParam(){
		return this.supportParam;
	}

	/**
	 * 设置  supportParam
	 *@param: supportParam  支撑参数设置
	 */
	public void setSupportParam(String supportParam){
		this.supportParam = supportParam;
	}
	/**
	 * 获取  diagonalSetting
	 *@return: String  对角点设置
	 */
	public String getDiagonalSetting(){
		return this.diagonalSetting;
	}

	/**
	 * 设置  diagonalSetting
	 *@param: diagonalSetting  对角点设置
	 */
	public void setDiagonalSetting(String diagonalSetting){
		this.diagonalSetting = diagonalSetting;
	}
	/**
	 * 获取  supportType
	 *@return: String  支撑类型Val
	 */
	public String getSupportType(){
		return this.supportType;
	}

	/**
	 * 设置  supportType
	 *@param: supportType  支撑类型Val
	 */
	public void setSupportType(String supportType){
		this.supportType = supportType;
	}
	/**
	 * 获取  supportTypeName
	 *@return: String  支撑类型
	 */
	public String getSupportTypeName(){
		return this.supportTypeName;
	}

	/**
	 * 设置  supportTypeName
	 *@param: supportTypeName  支撑类型
	 */
	public void setSupportTypeName(String supportTypeName){
		this.supportTypeName = supportTypeName;
	}
	/**
	 * 获取  sensorQuantity
	 *@return: String  传感器数量
	 */
	public String getSensorQuantity(){
		return this.sensorQuantity;
	}

	/**
	 * 设置  sensorQuantity
	 *@param: sensorQuantity  传感器数量
	 */
	public void setSensorQuantity(String sensorQuantity){
		this.sensorQuantity = sensorQuantity;
	}
	/**
	 * 获取  rawDataType
	 *@return: String  原始数据类型
	 */
	public String getRawDataType(){
		return this.rawDataType;
	}

	/**
	 * 设置  rawDataType
	 *@param: rawDataType  原始数据类型
	 */
	public void setRawDataType(String rawDataType){
		this.rawDataType = rawDataType;
	}
	/**
	 * 获取  sensorType
	 *@return: String  传感器类别
	 */
	public String getSensorType(){
		return this.sensorType;
	}

	/**
	 * 设置  sensorType
	 *@param: sensorType  传感器类别
	 */
	public void setSensorType(String sensorType){
		this.sensorType = sensorType;
	}
	/**
	 * 获取  sensorTypeName
	 *@return: String  传感器类别名称
	 */
	public String getSensorTypeName(){
		return this.sensorTypeName;
	}

	/**
	 * 设置  sensorTypeName
	 *@param: sensorTypeName  传感器类别名称
	 */
	public void setSensorTypeName(String sensorTypeName){
		this.sensorTypeName = sensorTypeName;
	}
	/**
	 * 获取  monitorItemTypeCode
	 *@return: String  监测项类型编码
	 */
	public String getMonitorItemTypeCode(){
		return this.monitorItemTypeCode;
	}

	/**
	 * 设置  monitorItemTypeCode
	 *@param: monitorItemTypeCode  监测项类型编码
	 */
	public void setMonitorItemTypeCode(String monitorItemTypeCode){
		this.monitorItemTypeCode = monitorItemTypeCode;
	}
	/**
	 * 获取  monitorItemTypeName
	 *@return: String  监测项类型名称
	 */
	public String getMonitorItemTypeName(){
		return this.monitorItemTypeName;
	}

	/**
	 * 设置  monitorItemTypeName
	 *@param: monitorItemTypeName  监测项类型名称
	 */
	public void setMonitorItemTypeName(String monitorItemTypeName){
		this.monitorItemTypeName = monitorItemTypeName;
	}
	/**
	 * 获取  supportZcjmj
	 *@return: String  支撑截面积
	 */
	public String getSupportZcjmj(){
		return this.supportZcjmj;
	}

	/**
	 * 设置  supportZcjmj
	 *@param: supportZcjmj  支撑截面积
	 */
	public void setSupportZcjmj(String supportZcjmj){
		this.supportZcjmj = supportZcjmj;
	}
	/**
	 * 获取  supportGjjmj
	 *@return: String  钢筋截面积(mm)
	 */
	public String getSupportGjjmj(){
		return this.supportGjjmj;
	}

	/**
	 * 设置  supportGjjmj
	 *@param: supportGjjmj  钢筋截面积(mm)
	 */
	public void setSupportGjjmj(String supportGjjmj){
		this.supportGjjmj = supportGjjmj;
	}
	/**
	 * 获取  supportHnttxml
	 *@return: String  混凝土弹性模量
	 */
	public String getSupportHnttxml(){
		return this.supportHnttxml;
	}

	/**
	 * 设置  supportHnttxml
	 *@param: supportHnttxml  混凝土弹性模量
	 */
	public void setSupportHnttxml(String supportHnttxml){
		this.supportHnttxml = supportHnttxml;
	}
	/**
	 * 获取  supportGjtxml
	 *@return: String  钢筋弹性模量
	 */
	public String getSupportGjtxml(){
		return this.supportGjtxml;
	}

	/**
	 * 设置  supportGjtxml
	 *@param: supportGjtxml  钢筋弹性模量
	 */
	public void setSupportGjtxml(String supportGjtxml){
		this.supportGjtxml = supportGjtxml;
	}
	/**
	 * 获取  supportGjjjmj
	 *@return: String  钢筋计截面积(mm²)
	 */
	public String getSupportGjjjmj(){
		return this.supportGjjjmj;
	}

	/**
	 * 设置  supportGjjjmj
	 *@param: supportGjjjmj  钢筋计截面积(mm²)
	 */
	public void setSupportGjjjmj(String supportGjjjmj){
		this.supportGjjjmj = supportGjjjmj;
	}
	/**
	 * 获取  sensorId1
	 *@return: String  传感器id1
	 */
	public String getSensorId1(){
		return this.sensorId1;
	}

	/**
	 * 设置  sensorId1
	 *@param: sensorId1  传感器id1
	 */
	public void setSensorId1(String sensorId1){
		this.sensorId1 = sensorId1;
	}
	/**
	 * 获取  sensorId2
	 *@return: String  传感器id2
	 */
	public String getSensorId2(){
		return this.sensorId2;
	}

	/**
	 * 设置  sensorId2
	 *@param: sensorId2  传感器id2
	 */
	public void setSensorId2(String sensorId2){
		this.sensorId2 = sensorId2;
	}
	/**
	 * 获取  sensorId3
	 *@return: String  传感器id3
	 */
	public String getSensorId3(){
		return this.sensorId3;
	}

	/**
	 * 设置  sensorId3
	 *@param: sensorId3  传感器id3
	 */
	public void setSensorId3(String sensorId3){
		this.sensorId3 = sensorId3;
	}
	/**
	 * 获取  sensorId4
	 *@return: String  传感器id4
	 */
	public String getSensorId4(){
		return this.sensorId4;
	}

	/**
	 * 设置  sensorId4
	 *@param: sensorId4  传感器id4
	 */
	public void setSensorId4(String sensorId4){
		this.sensorId4 = sensorId4;
	}
	/**
	 * 获取  sensorNum1
	 *@return: String  传感器编号1
	 */
	public String getSensorNum1(){
		return this.sensorNum1;
	}

	/**
	 * 设置  sensorNum1
	 *@param: sensorNum1  传感器编号1
	 */
	public void setSensorNum1(String sensorNum1){
		this.sensorNum1 = sensorNum1;
	}
	/**
	 * 获取  sensorNum2
	 *@return: String  传感器编号2
	 */
	public String getSensorNum2(){
		return this.sensorNum2;
	}

	/**
	 * 设置  sensorNum2
	 *@param: sensorNum2  传感器编号2
	 */
	public void setSensorNum2(String sensorNum2){
		this.sensorNum2 = sensorNum2;
	}
	/**
	 * 获取  sensorNum3
	 *@return: String  传感器编号3
	 */
	public String getSensorNum3(){
		return this.sensorNum3;
	}

	/**
	 * 设置  sensorNum3
	 *@param: sensorNum3  传感器编号3
	 */
	public void setSensorNum3(String sensorNum3){
		this.sensorNum3 = sensorNum3;
	}
	/**
	 * 获取  sensorNum4
	 *@return: String  传感器编号4
	 */
	public String getSensorNum4(){
		return this.sensorNum4;
	}

	/**
	 * 设置  sensorNum4
	 *@param: sensorNum4  传感器编号4
	 */
	public void setSensorNum4(String sensorNum4){
		this.sensorNum4 = sensorNum4;
	}
	/**
	 * 获取  sensorFq1
	 *@return: String  传感器1安装后初始频率
	 */
	public String getSensorFq1(){
		return this.sensorFq1;
	}

	/**
	 * 设置  sensorFq1
	 *@param: sensorFq1  传感器1安装后初始频率
	 */
	public void setSensorFq1(String sensorFq1){
		this.sensorFq1 = sensorFq1;
	}
	/**
	 * 获取  sensorFq2
	 *@return: String  传感器2安装后初始频率
	 */
	public String getSensorFq2(){
		return this.sensorFq2;
	}

	/**
	 * 设置  sensorFq2
	 *@param: sensorFq2  传感器2安装后初始频率
	 */
	public void setSensorFq2(String sensorFq2){
		this.sensorFq2 = sensorFq2;
	}
	/**
	 * 获取  sensorFq3
	 *@return: String  传感器3安装后初始频率
	 */
	public String getSensorFq3(){
		return this.sensorFq3;
	}

	/**
	 * 设置  sensorFq3
	 *@param: sensorFq3  传感器3安装后初始频率
	 */
	public void setSensorFq3(String sensorFq3){
		this.sensorFq3 = sensorFq3;
	}
	/**
	 * 获取  sensorFq4
	 *@return: String  传感器4安装后初始频率
	 */
	public String getSensorFq4(){
		return this.sensorFq4;
	}

	/**
	 * 设置  sensorFq4
	 *@param: sensorFq4  传感器4安装后初始频率
	 */
	public void setSensorFq4(String sensorFq4){
		this.sensorFq4 = sensorFq4;
	}
	/**
	 * 获取  initValue
	 *@return: Double  初始值
	 */
	public Double getInitValue(){
		return this.initValue;
	}

	/**
	 * 设置  initValue
	 *@param: initValue  初始值
	 */
	public void setInitValue(Double initValue){
		this.initValue = initValue;
	}
	/**
	 * 获取  initValueId
	 *@return: String  初始测量Id
	 */
	public String getInitValueId(){
		return this.initValueId;
	}

	/**
	 * 设置  initValueId
	 *@param: initValueId  初始测量Id
	 */
	public void setInitValueId(String initValueId){
		this.initValueId = initValueId;
	}
	/**
	 * 获取  subareaId
	 *@return: String  监测分区ID
	 */
	public String getSubareaId(){
		return this.subareaId;
	}

	/**
	 * 设置  subareaId
	 *@param: subareaId  监测分区ID
	 */
	public void setSubareaId(String subareaId){
		this.subareaId = subareaId;
	}
	/**
	 * 获取  subareaName
	 *@return: String  监测分区
	 */
	public String getSubareaName(){
		return this.subareaName;
	}

	/**
	 * 设置  subareaName
	 *@param: subareaName  监测分区
	 */
	public void setSubareaName(String subareaName){
		this.subareaName = subareaName;
	}
	/**
	 * 获取  deviceId
	 *@return: String  监测仪器Id
	 */
	public String getDeviceId(){
		return this.deviceId;
	}

	/**
	 * 设置  deviceId
	 *@param: deviceId  监测仪器Id
	 */
	public void setDeviceId(String deviceId){
		this.deviceId = deviceId;
	}
	/**
	 * 获取  deviceName
	 *@return: String  监测仪器名称
	 */
	public String getDeviceName(){
		return this.deviceName;
	}

	/**
	 * 设置  deviceName
	 *@param: deviceName  监测仪器名称
	 */
	public void setDeviceName(String deviceName){
		this.deviceName = deviceName;
	}
	/**
	 * 获取  warnValueNg
	 *@return: String  负向预警值(mm)
	 */
	public String getWarnValueNg(){
		return this.warnValueNg;
	}

	/**
	 * 设置  warnValueNg
	 *@param: warnValueNg  负向预警值(mm)
	 */
	public void setWarnValueNg(String warnValueNg){
		this.warnValueNg = warnValueNg;
	}
	/**
	 * 获取  alarmValueNg
	 *@return: String  负向报警值(mm)
	 */
	public String getAlarmValueNg(){
		return this.alarmValueNg;
	}

	/**
	 * 设置  alarmValueNg
	 *@param: alarmValueNg  负向报警值(mm)
	 */
	public void setAlarmValueNg(String alarmValueNg){
		this.alarmValueNg = alarmValueNg;
	}
	/**
	 * 获取  controlValueNg
	 *@return: String  负向控制值(mm)
	 */
	public String getControlValueNg(){
		return this.controlValueNg;
	}

	/**
	 * 设置  controlValueNg
	 *@param: controlValueNg  负向控制值(mm)
	 */
	public void setControlValueNg(String controlValueNg){
		this.controlValueNg = controlValueNg;
	}
	/**
	 * 获取  pointHoleDepth
	 *@return: String  测控深度
	 */
	public String getPointHoleDepth(){
		return this.pointHoleDepth;
	}

	/**
	 * 设置  pointHoleDepth
	 *@param: pointHoleDepth  测控深度
	 */
	public void setPointHoleDepth(String pointHoleDepth){
		this.pointHoleDepth = pointHoleDepth;
	}
	/**
	 * 获取  sensor1Depth
	 *@return: String  传感器深度
	 */
	public String getSensor1Depth(){
		return this.sensor1Depth;
	}

	/**
	 * 设置  sensor1Depth
	 *@param: sensor1Depth  传感器深度
	 */
	public void setSensor1Depth(String sensor1Depth){
		this.sensor1Depth = sensor1Depth;
	}
	/**
	 * 获取  pipeRangeGkgc
	 *@return: String  管口高程
	 */
	public String getPipeRangeGkgc(){
		return this.pipeRangeGkgc;
	}

	/**
	 * 设置  pipeRangeGkgc
	 *@param: pipeRangeGkgc  管口高程
	 */
	public void setPipeRangeGkgc(String pipeRangeGkgc){
		this.pipeRangeGkgc = pipeRangeGkgc;
	}
	/**
	 * 获取  xvalue
	 *@return: Double  x值
	 */
	public Double getXvalue(){
		return this.xvalue;
	}

	/**
	 * 设置  xvalue
	 *@param: xvalue  x值
	 */
	public void setXvalue(Double xvalue){
		this.xvalue = xvalue;
	}
	/**
	 * 获取  yvalue
	 *@return: Double  y值
	 */
	public Double getYvalue(){
		return this.yvalue;
	}

	/**
	 * 设置  yvalue
	 *@param: yvalue  y值
	 */
	public void setYvalue(Double yvalue){
		this.yvalue = yvalue;
	}
	/**
	 * 获取  dataUploadUnitId
	 *@return: String  数据上传单元型号ID
	 */
	public String getDataUploadUnitId(){
		return this.dataUploadUnitId;
	}

	/**
	 * 设置  dataUploadUnitId
	 *@param: dataUploadUnitId  数据上传单元型号ID
	 */
	public void setDataUploadUnitId(String dataUploadUnitId){
		this.dataUploadUnitId = dataUploadUnitId;
	}
	/**
	 * 获取  dataUploadUnitName
	 *@return: String  数据上传单元型号
	 */
	public String getDataUploadUnitName(){
		return this.dataUploadUnitName;
	}

	/**
	 * 设置  dataUploadUnitName
	 *@param: dataUploadUnitName  数据上传单元型号
	 */
	public void setDataUploadUnitName(String dataUploadUnitName){
		this.dataUploadUnitName = dataUploadUnitName;
	}
	/**
	 * 获取  updateValue
	 *@return: Double  修正值
	 */
	public Double getUpdateValue(){
		return this.updateValue;
	}

	/**
	 * 设置  updateValue
	 *@param: updateValue  修正值
	 */
	public void setUpdateValue(Double updateValue){
		this.updateValue = updateValue;
	}
	/**
	 * 获取  depthExt
	 *@return: String  存储深度对应的字段
	 */
	public String getDepthExt(){
		return this.depthExt;
	}

	/**
	 * 设置  depthExt
	 *@param: depthExt  存储深度对应的字段
	 */
	public void setDepthExt(String depthExt){
		this.depthExt = depthExt;
	}
	/**
	 * 获取  startDepth
	 *@return: String  开始深度
	 */
	public String getStartDepth(){
		return this.startDepth;
	}

	/**
	 * 设置  startDepth
	 *@param: startDepth  开始深度
	 */
	public void setStartDepth(String startDepth){
		this.startDepth = startDepth;
	}
	/**
	 * 获取  endDepth
	 *@return: String  终止深度
	 */
	public String getEndDepth(){
		return this.endDepth;
	}

	/**
	 * 设置  endDepth
	 *@param: endDepth  终止深度
	 */
	public void setEndDepth(String endDepth){
		this.endDepth = endDepth;
	}
	/**
	 * 获取  stepDepth
	 *@return: String  步长
	 */
	public String getStepDepth(){
		return this.stepDepth;
	}

	/**
	 * 设置  stepDepth
	 *@param: stepDepth  步长
	 */
	public void setStepDepth(String stepDepth){
		this.stepDepth = stepDepth;
	}
	/**
	 * 获取  rupturePosition
	 *@return: String  裂缝位置
	 */
	public String getRupturePosition(){
		return this.rupturePosition;
	}

	/**
	 * 设置  rupturePosition
	 *@param: rupturePosition  裂缝位置
	 */
	public void setRupturePosition(String rupturePosition){
		this.rupturePosition = rupturePosition;
	}
	/**
	 * 获取  ruptureDirection
	 *@return: String  裂缝走向
	 */
	public String getRuptureDirection(){
		return this.ruptureDirection;
	}

	/**
	 * 设置  ruptureDirection
	 *@param: ruptureDirection  裂缝走向
	 */
	public void setRuptureDirection(String ruptureDirection){
		this.ruptureDirection = ruptureDirection;
	}

	public String getModifyDesp() {
		return modifyDesp;
	}

	public void setModifyDesp(String modifyDesp) {
		this.modifyDesp = modifyDesp;
	}

	public String getModifyFilePath() {
		return modifyFilePath;
	}

	public void setModifyFilePath(String modifyFilePath) {
		this.modifyFilePath = modifyFilePath;
	}

	public String getModifyFileName() {
		return modifyFileName;
	}

	public void setModifyFileName(String modifyFileName) {
		this.modifyFileName = modifyFileName;
	}
	
	
}
