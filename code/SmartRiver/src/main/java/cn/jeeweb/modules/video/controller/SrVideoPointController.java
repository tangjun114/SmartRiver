package cn.jeeweb.modules.video.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.video.entity.SrVideoPoint;

/**   
 * @Title: 视频站点管理
 * @Description: 视频站点管理
 * @author wsh
 * @date 2019-04-01 14:30:31
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/video/srvideopoint")
@RequiresPathPermission("video:srvideopoint")
public class SrVideoPointController extends BaseCRUDController<SrVideoPoint, String> {

}
