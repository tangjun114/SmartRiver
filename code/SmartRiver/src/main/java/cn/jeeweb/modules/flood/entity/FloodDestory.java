package cn.jeeweb.modules.flood.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;

import java.util.Date;

/**
 * @author wsh
 * @version V1.0
 * @Title: 水毁信息表
 * @Description: 水毁信息表
 * @date 2018-11-26 21:01:04
 */
@TableName("sr_flood_destory")
@SuppressWarnings("serial")
public class FloodDestory extends AbstractEntity<String> {

    /**
     * 字段主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    /**
     * 创建者
     */
    @TableField(value = "create_by", el = "createBy.id")
    private User createBy;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField(value = "update_by", el = "updateBy.id")
    private User updateBy;
    /**
     * 更新时间
     */
    @TableField(value = "update_date")
    private Date updateDate;
    /**
     * 删除标记（0：正常；1：删除）
     */
    @TableField(value = "del_flag")
    private String delFlag;
    /**
     * 备注信息
     */
    @TableField(value = "remarks")
    private String remarks;
    /**
     * 图像
     */
    @TableField(value = "img_path")
    private String imgPath;
    /**
     * 音频
     */
    @TableField(value = "audio")
    private String audio;
    /**
     * 严重程度
     */
    @TableField(value = "grade")
    private String grade;
    /**
     * 坐标
     */
    @TableField(value = "latitude")
    private String latitude;
    /**
     * 经济损失
     */
    @TableField(value = "econ_loss")
    private Double econLoss;
    /**
     * 文字描述
     */
    @TableField(value = "details")
    private String details;
    /**
     * 标题
     */
    @TableField(value = "title")
    private String title;
    /**
     * 所属区域id
     */
    @TableField(value = "area_id")
    private String areaId;
    /**
     * 所属区域name
     */
    @TableField(value = "area_name")
    private String areaName;
    /**
     * 纵坐标
     */
    @TableField(value = "longitude")
    private String longitude;

    @TableField(value = "record_time")
    private Date recordTime;


    /**
     * 获取  id
     *
     * @return: String  字段主键
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置  id
     *
     * @param: id  字段主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取  createBy
     *
     * @return: User  创建者
     */
    public User getCreateBy() {
        return this.createBy;
    }

    /**
     * 设置  createBy
     *
     * @param: createBy  创建者
     */
    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取  createDate
     *
     * @return: Date  创建时间
     */
    public Date getCreateDate() {
        return this.createDate;
    }

    /**
     * 设置  createDate
     *
     * @param: createDate  创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取  updateBy
     *
     * @return: User  更新者
     */
    public User getUpdateBy() {
        return this.updateBy;
    }

    /**
     * 设置  updateBy
     *
     * @param: updateBy  更新者
     */
    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取  updateDate
     *
     * @return: Date  更新时间
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 设置  updateDate
     *
     * @param: updateDate  更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取  delFlag
     *
     * @return: String  删除标记（0：正常；1：删除）
     */
    public String getDelFlag() {
        return this.delFlag;
    }

    /**
     * 设置  delFlag
     *
     * @param: delFlag  删除标记（0：正常；1：删除）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 获取  remarks
     *
     * @return: String  备注信息
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置  remarks
     *
     * @param: remarks  备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取  audio
     *
     * @return: String  音频
     */
    public String getAudio() {
        return this.audio;
    }

    /**
     * 设置  audio
     *
     * @param: audio  音频
     */
    public void setAudio(String audio) {
        this.audio = audio;
    }

    /**
     * 获取  grade
     *
     * @return: Integer  严重程度
     */
    public String getGrade() {
        return this.grade;
    }

    /**
     * 设置  grade
     *
     * @param: grade  严重程度
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * 获取  latitude
     *
     * @return: String  坐标
     */
    public String getLatitude() {
        return this.latitude;
    }

    /**
     * 设置  latitude
     *
     * @param: latitude  坐标
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 获取  econLoss
     *
     * @return: Double  经济损失
     */
    public Double getEconLoss() {
        return this.econLoss;
    }

    /**
     * 设置  econLoss
     *
     * @param: econLoss  经济损失
     */
    public void setEconLoss(Double econLoss) {
        this.econLoss = econLoss;
    }

    /**
     * 获取  details
     *
     * @return: String  文字描述
     */
    public String getDetails() {
        return this.details;
    }

    /**
     * 设置  details
     *
     * @param: details  文字描述
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * 获取  title
     *
     * @return: String  标题
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * 设置  title
     *
     * @param: title  标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取  areaId
     *
     * @return: String  所属区域id
     */
    public String getAreaId() {
        return this.areaId;
    }

    /**
     * 设置  areaId
     *
     * @param: areaId  所属区域id
     */
    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    /**
     * 获取  areaName
     *
     * @return: String  所属区域name
     */
    public String getAreaName() {
        return this.areaName;
    }

    /**
     * 设置  areaName
     *
     * @param: areaName  所属区域name
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     * 获取  longitude
     *
     * @return: String  纵坐标
     */
    public String getLongitude() {
        return this.longitude;
    }

    /**
     * 设置  longitude
     *
     * @param: longitude  纵坐标
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

}
