package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectQualityIndicatorMapper;
import cn.jeeweb.modules.project.entity.SrProjectQualityIndicator;
import cn.jeeweb.modules.project.service.ISrProjectQualityIndicatorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目质量指标
 * @Description: 项目质量指标
 * @author jerry
 * @date 2018-11-14 17:57:30
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectQualityIndicatorService")
public class SrProjectQualityIndicatorServiceImpl  extends CommonServiceImpl<SrProjectQualityIndicatorMapper,SrProjectQualityIndicator> implements  ISrProjectQualityIndicatorService {

}
