package cn.jeeweb.modules.supervise.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;

import com.ff.common.web.json.BaseReqJson;

import cn.jeeweb.core.model.AjaxJson;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort.Direction;
import cn.jeeweb.core.query.data.Sort.Order;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.supervise.entity.McProItemAlarmModifyLog;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;
import cn.jeeweb.modules.supervise.service.IMcProItemAlarmModifyLogService;
import cn.jeeweb.modules.sys.entity.Organization;
import cn.jeeweb.modules.sys.service.IOrganizationService;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 项目监控-测点设置-监测项-测点
 * @Description: 项目监控-测点设置-监测项-测点
 * @author shawloong
 * @date 2017-11-12 21:06:58
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemmeasuringpoint")
@RequiresPathPermission("supervise:mcproitemmeasuringpoint")
public class McProItemMeasuringPointController extends McProjectBaseController<McProItemMeasuringPoint, String> {
	
	@Autowired IMcProItemAlarmModifyLogService alarmModifyService;
	@Autowired
	private IOrganizationService organizationService;
	@Override
	public String display(String suffixName) {
		if("edit".endsWith(suffixName)){
			suffixName = "edit_self";
		}
		if (!suffixName.startsWith("/")) {
			
			suffixName = "/" + suffixName;
		}
		return getViewPrefix().toLowerCase() + suffixName;
	}

	@Override
	public void preSave(McProItemMeasuringPoint entity, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preSave(entity, request, response);
		String temp = HtmlUtils.htmlUnescape(entity.getDepthExt());
		entity.setDepthExt(temp);
	}
	
	
	@Override
	public void preCopy(McProItemMeasuringPoint entity, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preCopy(entity, model, request, response);
		entity.setInitValue((double) 0);
		
	}

	@Override
	public Queryable GetFilterCondition(BaseReqJson request) {
		// TODO Auto-generated method stub
		Queryable qy = super.GetFilterCondition(request);
		 qy.addOrder(new Order(Direction.ASC, "measuringPointCode"));
		return qy ;
	}

	@Override
	public void preAjaxList(Queryable queryable,EntityWrapper<McProItemMeasuringPoint> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
	 
		entityWrapper.orderBy("measuringPointCode", true);
		super.preAjaxList(queryable, entityWrapper, request, response);
	}

	
	@Override
	public AjaxJson doSave(McProItemMeasuringPoint entity, HttpServletRequest request, HttpServletResponse response,
			BindingResult result) {
		// TODO Auto-generated method stub
		return super.doSave(entity, request, response, result);
	}

	@Override
	public void afterSave(McProItemMeasuringPoint entity, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.afterSave(entity, request, response);
		if (!StringUtils.isEmpty(entity.getModifyDesp())) {
			McProItemAlarmModifyLog mlog  = new McProItemAlarmModifyLog();
			mlog.setModifyDesp(entity.getModifyDesp());
			mlog.setModifyFileName(entity.getModifyFileName());
			mlog.setModifyFilePath(entity.getModifyFilePath());
			mlog.setMonitorItemId(entity.getMonitorItemId());
			mlog.setMonitorItemName(entity.getMonitorItemName());
			
			mlog.setMonitorId(UserUtils.getUser().getOrganizationIds());
			Organization org = organizationService.selectById(UserUtils.getUser().getOrganizationIds());
			if (org != null) {
				mlog.setMonitorName(org.getName());
			}
			mlog.setModifyUser(UserUtils.getUser().getRealname());
			
			mlog.setPointCode(entity.getMeasuringPointCode());
			mlog.setPointId(entity.getId());
			mlog.setProjectId(entity.getProjectId());
			mlog.setProjectName(entity.getProjectName());
	
			alarmModifyService.insert(mlog);
		}
	}

	
	
	

}
