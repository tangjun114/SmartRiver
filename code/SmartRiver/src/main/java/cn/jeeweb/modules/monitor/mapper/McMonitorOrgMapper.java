package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorOrg;
 
/**   
 * @Title: 监督检测机构数据库控制层接口
 * @Description: 监督检测机构数据库控制层接口
 * @author shawloong
 * @date 2017-10-05 22:28:45
 * @version V1.0   
 *
 */
public interface McMonitorOrgMapper extends BaseMapper<McMonitorOrg> {
    
}