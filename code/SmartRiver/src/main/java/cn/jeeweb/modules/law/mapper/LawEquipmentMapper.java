package cn.jeeweb.modules.law.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.law.entity.LawEquipment;
 
/**   
 * @Title: 设备管理数据库控制层接口
 * @Description: 设备管理数据库控制层接口
 * @author liyonglei
 * @date 2018-11-13 08:50:52
 * @version V1.0   
 *
 */
public interface LawEquipmentMapper extends BaseMapper<LawEquipment> {
    
}