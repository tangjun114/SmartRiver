package cn.jeeweb.modules.webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.web.controller.FFTableController;

import cn.jeeweb.modules.sm.entity.McSmParamConfig;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;


@Controller
@RequestMapping("${api.url.prefix}/syspara")
public class SysParaApi extends FFTableController<McSmParamConfig>
{

}
