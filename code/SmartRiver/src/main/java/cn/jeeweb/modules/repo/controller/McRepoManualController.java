package cn.jeeweb.modules.repo.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.query.wrapper.EntityWrapper;

import java.util.Date;
import java.util.List;

import cn.jeeweb.modules.repo.entity.McRepoManual;
import cn.jeeweb.modules.repo.entity.McRepoStandard;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 平台手册 manual
 * @Description: 平台手册 manual
 * @author shawloong
 * @date 2017-10-04 14:28:20
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/repo/mcrepomanual")
@RequiresPathPermission("repo:mcrepomanual")
public class McRepoManualController extends BaseCRUDController<McRepoManual, String> {
	
	@Override
	public void preEdit(McRepoManual mcRepoManual, Model model, HttpServletRequest request, HttpServletResponse response) {
	}
	
	/**
	 * 保存数据之前
	 * 
	 * @param entity
	 * @param request
	 * @param response
	 */
	@Override
	public void preSave(McRepoManual entity, HttpServletRequest request, HttpServletResponse response) {
		if(null==entity.getCreateDate()){
			entity.setCreateDate(new Date());
		}
		super.preSave(entity, request, response);
	}
}
