package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 项目规范
 * @Description: 项目规范
 * @author jerry
 * @date 2018-01-23 18:51:47
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_standard")
@SuppressWarnings("serial")
public class McProItemStandard extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**规范id*/
    @TableField(value = "standard_id")
	private String standardId;
    /**规范编码*/
    @TableField(value = "standard_code")
	private String standardCode;
    /**规范名称*/
    @TableField(value = "standard_name")
	private String standardName;
    /**规范路径*/
    @TableField(value = "standard_path")
	private String standardPath;
    /**项目Id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**创建者名称*/
    @TableField(value = "create_by_name")
	private String createByName;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  standardId
	 *@return: String  规范id
	 */
	public String getStandardId(){
		return this.standardId;
	}

	/**
	 * 设置  standardId
	 *@param: standardId  规范id
	 */
	public void setStandardId(String standardId){
		this.standardId = standardId;
	}
	/**
	 * 获取  standardCode
	 *@return: String  规范编码
	 */
	public String getStandardCode(){
		return this.standardCode;
	}

	/**
	 * 设置  standardCode
	 *@param: standardCode  规范编码
	 */
	public void setStandardCode(String standardCode){
		this.standardCode = standardCode;
	}
	/**
	 * 获取  standardName
	 *@return: String  规范名称
	 */
	public String getStandardName(){
		return this.standardName;
	}

	/**
	 * 设置  standardName
	 *@param: standardName  规范名称
	 */
	public void setStandardName(String standardName){
		this.standardName = standardName;
	}
	/**
	 * 获取  standardPath
	 *@return: String  规范路径
	 */
	public String getStandardPath(){
		return this.standardPath;
	}

	/**
	 * 设置  standardPath
	 *@param: standardPath  规范路径
	 */
	public void setStandardPath(String standardPath){
		this.standardPath = standardPath;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目Id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目Id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  createByName
	 *@return: String  创建者名称
	 */
	public String getCreateByName(){
		return this.createByName;
	}

	/**
	 * 设置  createByName
	 *@param: createByName  创建者名称
	 */
	public void setCreateByName(String createByName){
		this.createByName = createByName;
	}
	
}
