package cn.jeeweb.modules.law.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.law.entity.LawEquipment;

/**   
 * @Title: 设备管理
 * @Description: 设备管理
 * @author liyonglei
 * @date 2018-11-13 08:50:52
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/law/lawequipment")
@RequiresPathPermission("law:lawequipment")
public class LawEquipmentController extends BaseCRUDController<LawEquipment, String> {

}
