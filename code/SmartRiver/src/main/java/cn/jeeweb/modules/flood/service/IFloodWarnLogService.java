package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodWarnLog;

/**   
 * @Title: 报警记录
 * @Description: 报警记录
 * @author wsh
 * @date 2018-12-05 19:47:58
 * @version V1.0   
 *
 */
public interface IFloodWarnLogService extends ICommonService<FloodWarnLog> {

}

