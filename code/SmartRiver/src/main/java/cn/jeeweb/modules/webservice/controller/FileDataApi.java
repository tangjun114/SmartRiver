package cn.jeeweb.modules.webservice.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.ff.common.service.FFException;
import com.ff.common.util.format.DateUtil;
import com.ff.common.util.validate.ValidatorUtil;
import com.ff.common.web.controller.FFTableController;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.modules.monitor.entity.McMonitorRealdata;
import cn.jeeweb.modules.monitor.service.IMcMonitorRealdataService;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;
import cn.jeeweb.modules.supervise.service.IMcProItemMeasuringPointService;
import cn.jeeweb.modules.supervise.service.IMcProItemMonitorItemService;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.entity.Attachment;
import cn.jeeweb.modules.sys.service.IAttachmentService;

@Controller
@RequestMapping("${api.url.prefix}/file")
public class FileDataApi   extends FFTableController<McMonitorRealdata>// FFTableController<McMonitorRealdata>  
{
	protected Logger log = Logger.getLogger(getClass());

	@Autowired
	private IAttachmentService attachmentService;
	
	@Autowired
	private IMcMonitorRealdataService mcMonitorRealdataService;
	
	@Autowired
	private IMcProItemMeasuringPointService pointService;
	@Autowired
	private IMcProItemMonitorItemService itemSerivce;
	@Autowired
	private IMcProjectService projectSerivce;
	
	
	@RequestMapping("/upload")
	@ResponseBody
	public BaseRspJson<McMonitorRealdata> upload(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/plain");
		BaseRspJson<McMonitorRealdata> rsp = new BaseRspJson<McMonitorRealdata>();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
				request.getSession().getServletContext());
		String  monitorId = request.getParameter("monitorId");
		String  testPoint = request.getParameter("testPoint");
		String  collectTime = request.getParameter("collectTime");
		String  count = request.getParameter("monitorCount");
		String  mpCount = request.getParameter("mpCount");
		String measureResult = request.getParameter("measureResult");
		String uploadPerson = request.getParameter("uploadPerson");

		McMonitorRealdata obj = new McMonitorRealdata();
		
		Date now = DateUtil.getCurrentDate();
		obj.setCreateDate(now);
		obj.setUpdateDate(now);
		obj.setUploadTime(now);
		
		if(!ValidatorUtil.isEmpty(count))
		{
			obj.setMonitorCount(Integer.valueOf(count));
 		}
		
		McProItemMonitorItem item = itemSerivce.selectById(monitorId);
		obj.setMonitorItemId(monitorId);
		if(null != item)
		{
			obj.setMonitorItemName(item.getMonitorItemName());
			obj.setMonitorTypeId(item.getGroupTypeId());
			obj.setMonitorTypeName(item.getGroupTypeName());
			obj.setProjectId(item.getProjectId());
			obj.setProjectName(item.getProjectName());
			obj.setUploadPerson(uploadPerson);
			obj.setMeasureResult(measureResult);
			obj.setMpCount(Integer.valueOf(mpCount));
 		}
		else
		{
			log.error("the monitor item is not exist " + monitorId);
			throw new FFException("监测项不存在");
		}
		
		 
		obj.setCollectTime(DateUtil.stringToDate(collectTime));
		
		
		List<Attachment> attachmentList = new ArrayList<Attachment>();
 		if (multipartResolver.isMultipart(request)) { // 判断request是否有文件上传
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			Iterator<String> ite = multiRequest.getFileNames();
			while (ite.hasNext()) {
				MultipartFile file = multiRequest.getFile(ite.next());
 					Attachment attachment = attachmentService.uploadDataFile(request, file);
 					
 					obj.setFileName(attachment.getFilename());
 					obj.setFilePath(attachment.getFilepath());
					attachmentList.add(attachment);
					mcMonitorRealdataService.insert(obj);
  			}
  		}
 		
 		
 		rsp.setObj(obj);
		return rsp;
	}
	@RequestMapping("/file")
	@ResponseBody
	public BaseRspJson<List<Attachment>> file(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/plain");
		BaseRspJson<List<Attachment>> rsp = new BaseRspJson<List<Attachment>>();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
				request.getSession().getServletContext());
  		
		List<Attachment> attachmentList = new ArrayList<Attachment>();
 		if (multipartResolver.isMultipart(request)) { // 判断request是否有文件上传
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			Iterator<String> ite = multiRequest.getFileNames();
			while (ite.hasNext()) {
				MultipartFile file = multiRequest.getFile(ite.next());
  				try
  				{
  					Attachment attachment = attachmentService.upload(request, file);
					attachmentList.add(attachment);
  				}catch(Exception e)
  				{
  					log.error("upload error" ,e);
  				}
				
 					 
  			}
  		}
 		
 		
 		rsp.setObj(attachmentList);
		return rsp;
	}
}
