package cn.jeeweb.modules.uc.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.uc.entity.UcMessage;

/**   
 * @Title: 个人中心消息
 * @Description: 个人中心消息
 * @author shawloong
 * @date 2017-09-30 00:04:45
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/uc/ucmessage")
@RequiresPathPermission("uc:ucmessage")
public class UcMessageController extends BaseCRUDController<UcMessage, String> {

}
