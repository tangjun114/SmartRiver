package cn.jeeweb.modules.water.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.water.entity.SrWaterQualityMonitor;
 
/**   
 * @Title: 水质监测数据库控制层接口
 * @Description: 水质监测数据库控制层接口
 * @author wsh
 * @date 2019-01-23 17:15:42
 * @version V1.0   
 *
 */
public interface SrWaterQualityMonitorMapper extends BaseMapper<SrWaterQualityMonitor> {
    
}