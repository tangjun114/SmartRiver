package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;
 
/**   
 * @Title: 项目监控-测点设置-监测项数据库控制层接口
 * @Description: 项目监控-测点设置-监测项数据库控制层接口
 * @author shawloong
 * @date 2017-11-19 12:08:14
 * @version V1.0   
 *
 */
public interface McProItemMonitorItemMapper extends BaseMapper<McProItemMonitorItem> {
    
}