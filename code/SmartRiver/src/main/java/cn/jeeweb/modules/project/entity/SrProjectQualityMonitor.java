package cn.jeeweb.modules.project.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 工程质量检测
 * @Description: 工程质量检测
 * @author wsh
 * @date 2018-12-13 19:29:51
 * @version V1.0   
 *
 */
@TableName("sr_project_quality_monitor")
@SuppressWarnings("serial")
public class SrProjectQualityMonitor extends AbstractEntity<String> {

    /**主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目id*/
    @TableField(value = "pro_id")
	private String proId;
    /**项目名称*/
    @TableField(value = "pro_name")
	private String proName;
    /**指标名称*/
    @TableField(value = "qua_id")
	private String quaId;
    /**指标name*/
    @TableField(value = "qua_name")
	private String quaName;
    /**指标类型*/
    @TableField(value = "qua_type")
	private String quaType;
    /**监测值*/
    @TableField(value = "value")
	private Double value;
    /**是否达标(0:不达标,1:达标)*/
    @TableField(value = "is_reach")
	private Short isReach;
	
	/**
	 * 获取  id
	 *@return: String  主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  proId
	 *@return: String  项目id
	 */
	public String getProId(){
		return this.proId;
	}

	/**
	 * 设置  proId
	 *@param: proId  项目id
	 */
	public void setProId(String proId){
		this.proId = proId;
	}
	/**
	 * 获取  proName
	 *@return: String  项目名称
	 */
	public String getProName(){
		return this.proName;
	}

	/**
	 * 设置  proName
	 *@param: proName  项目名称
	 */
	public void setProName(String proName){
		this.proName = proName;
	}
	/**
	 * 获取  quaId
	 *@return: String  指标名称
	 */
	public String getQuaId(){
		return this.quaId;
	}

	/**
	 * 设置  quaId
	 *@param: quaId  指标名称
	 */
	public void setQuaId(String quaId){
		this.quaId = quaId;
	}
	/**
	 * 获取  quaName
	 *@return: String  指标name
	 */
	public String getQuaName(){
		return this.quaName;
	}

	/**
	 * 设置  quaName
	 *@param: quaName  指标name
	 */
	public void setQuaName(String quaName){
		this.quaName = quaName;
	}
	/**
	 * 获取  quaType
	 *@return: String  指标类型
	 */
	public String getQuaType(){
		return this.quaType;
	}

	/**
	 * 设置  quaType
	 *@param: quaType  指标类型
	 */
	public void setQuaType(String quaType){
		this.quaType = quaType;
	}
	/**
	 * 获取  value
	 *@return: Double  监测值
	 */
	public Double getValue(){
		return this.value;
	}

	/**
	 * 设置  value
	 *@param: value  监测值
	 */
	public void setValue(Double value){
		this.value = value;
	}
	/**
	 * 获取  isReach
	 *@return: Short  是否达标(0:不达标,1:达标)
	 */
	public Short getIsReach(){
		return this.isReach;
	}

	/**
	 * 设置  isReach
	 *@param: isReach  是否达标(0:不达标,1:达标)
	 */
	public void setIsReach(Short isReach){
		this.isReach = isReach;
	}
	
}
