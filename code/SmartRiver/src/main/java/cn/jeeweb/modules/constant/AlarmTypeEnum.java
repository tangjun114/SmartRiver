/**   
* @Title: QueryCondition.java 
* @Package cn.fuego.misp.contanst 
* @Description: TODO
* @author Tang Jun   
* @date 2014-9-24 下午04:11:10 
* @version V1.0   
*/ 
package cn.jeeweb.modules.constant;


/**
 * 
 * @Description 
 * @author tangjun
 * @date 2016年7月5日
 */
public enum  AlarmTypeEnum
{

	NORMAL("正常","0"),
	WARN("预警","1"),
	ALARM("报警","2"), 
	OVERLOAD("超控","3");
 
 
	public static  String SUM_ALARM = "累计值报警";
	public static  String RATE_ALARM = "速率报警";
    // 成员变量  
	private  String name;  
    private String value;
 
    private AlarmTypeEnum(String name)
    {
    	this.name = name;
    }
    
    private AlarmTypeEnum(String name,String value)
    {
    	this.name = name;
     	this.value = value;
 
    }

 
 
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}



	public static AlarmTypeEnum getEnumByStr(String strValue)
	{
		for (AlarmTypeEnum c : AlarmTypeEnum.values())
		{
			if (c.name.equals(strValue))
			{
				return c;
			}
		}
		return null;
	}
 
 
 
}
