package cn.jeeweb.modules.sm.service;

import cn.jeeweb.core.common.service.ITreeCommonService;
import cn.jeeweb.modules.sm.entity.McSmParamConfig;

/**   
 * @Title: 系统管理-参数设置
 * @Description: 系统管理-参数设置
 * @author shawloong
 * @date 2017-10-11 23:30:03
 * @version V1.0   
 *
 */
public interface IMcSmParamConfigService extends ITreeCommonService<McSmParamConfig,String>{

}

