package cn.jeeweb.modules.webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.web.controller.FFTableController;

import cn.jeeweb.modules.monitor.entity.McMonitorSensor;
 
@Controller
@RequestMapping("${api.url.prefix}/monitorsensor")
public class MonitorSensorApi extends FFTableController<McMonitorSensor>
{

}
