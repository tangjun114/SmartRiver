package cn.jeeweb.modules.webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.web.controller.FFTableController;

import cn.jeeweb.modules.supervise.entity.McProItemInspection;

@Controller
@RequestMapping("${api.url.prefix}/inspection")
public class InspectionApi extends FFTableController<McProItemInspection>
{

}
