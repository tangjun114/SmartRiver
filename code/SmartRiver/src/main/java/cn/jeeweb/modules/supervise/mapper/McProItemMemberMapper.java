package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemMember;
 
/**   
 * @Title: 项目人员数据库控制层接口
 * @Description: 项目人员数据库控制层接口
 * @author Jerry
 * @date 2017-12-27 13:40:21
 * @version V1.0   
 *
 */
public interface McProItemMemberMapper extends BaseMapper<McProItemMember> {
    
}