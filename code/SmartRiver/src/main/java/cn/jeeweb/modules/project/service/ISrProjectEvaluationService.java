package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectEvaluation;

/**   
 * @Title: 项目评比
 * @Description: 项目评比
 * @author wsh
 * @date 2019-03-31 22:54:37
 * @version V1.0   
 *
 */
public interface ISrProjectEvaluationService extends ICommonService<SrProjectEvaluation> {

}

