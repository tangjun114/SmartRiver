package cn.jeeweb.modules.flood.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import com.ff.common.service.model.FFExcelMetaAnno;
import sun.awt.SunHints;

import java.util.Date;

/**
 * @author wsh
 * @version V1.0
 * @Title: 雨量监测
 * @Description: 雨量监测
 * @date 2018-11-27 18:15:03
 */
@TableName("sr_flood_rainfull")
@SuppressWarnings("serial")
public class FloodRainfull extends AbstractEntity<String> {

    /**
     * 字段主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    /**
     * 创建者
     */
    @TableField(value = "create_by", el = "createBy.id")
    private User createBy;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField(value = "update_by", el = "updateBy.id")
    private User updateBy;
    /**
     * 更新时间
     */
    @TableField(value = "update_date")
    private Date updateDate;
    /**
     * 删除标记（0：正常；1：删除）
     */
    @TableField(value = "del_flag")
    private String delFlag;
    /**
     * 备注信息
     */
    @TableField(value = "remarks")
    private String remarks;
    /**
     * 站点
     */
    @TableField(value = "site_id")
    private String siteId;
    /**
     * 站点名
     */
    @TableField(value = "site_name")
    @FFExcelMetaAnno(name = "站点")
    private String siteName;
    /**
     * 站点类型(干流,支流)
     */
    @TableField(value = "type")
    @FFExcelMetaAnno(name = "站点类型")
    private Integer type;
    /**
     * 站点类型(水文,水位)
     */
    @TableField(value = "kind")
    @FFExcelMetaAnno(name = "站点类别")
    private Integer kind;
    /**
     * 雨量(mm)
     */
    @TableField(value = "rainfull")
    @FFExcelMetaAnno(name = "雨量/mm")
    private Double rainfull;
    /**
     * 水位(m)
     */
    @TableField(value = "water_level")
    @FFExcelMetaAnno(name = "水位/mm")
    private Double waterLevel;
    /**
     * 流量(m3/s)
     */
    @TableField(value = "flow")
    private Double flow;
    /**
     * 记录时间
     */
    @TableField(value = "record_time")
    @FFExcelMetaAnno(name = "记录时间")
    private Date recordTime;
    /**
     * 记录类型
     */
    @TableField(value = "record_type")
    private Integer recordType;

    /**
     * 获取  id
     *
     * @return: String  字段主键
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置  id
     *
     * @param: id  字段主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取  createBy
     *
     * @return: User  创建者
     */
    public User getCreateBy() {
        return this.createBy;
    }

    /**
     * 设置  createBy
     *
     * @param: createBy  创建者
     */
    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取  createDate
     *
     * @return: Date  创建时间
     */
    public Date getCreateDate() {
        return this.createDate;
    }

    /**
     * 设置  createDate
     *
     * @param: createDate  创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取  updateBy
     *
     * @return: User  更新者
     */
    public User getUpdateBy() {
        return this.updateBy;
    }

    /**
     * 设置  updateBy
     *
     * @param: updateBy  更新者
     */
    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取  updateDate
     *
     * @return: Date  更新时间
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 设置  updateDate
     *
     * @param: updateDate  更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取  delFlag
     *
     * @return: String  删除标记（0：正常；1：删除）
     */
    public String getDelFlag() {
        return this.delFlag;
    }

    /**
     * 设置  delFlag
     *
     * @param: delFlag  删除标记（0：正常；1：删除）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 获取  remarks
     *
     * @return: String  备注信息
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置  remarks
     *
     * @param: remarks  备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取  siteId
     *
     * @return: String  站点
     */
    public String getSiteId() {
        return this.siteId;
    }

    /**
     * 设置  siteId
     *
     * @param: siteId  站点
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * 获取  siteName
     *
     * @return: String  站点名
     */
    public String getSiteName() {
        return this.siteName;
    }

    /**
     * 设置  siteName
     *
     * @param: siteName  站点名
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * 获取  type
     *
     * @return: Integer  站点类型(干流,支流)
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 设置  type
     *
     * @param: type  站点类型(干流,支流)
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取  kind
     *
     * @return: Integer  站点类型(水文,水位)
     */
    public Integer getKind() {
        return this.kind;
    }

    /**
     * 设置  kind
     *
     * @param: kind  站点类型(水文,水位)
     */
    public void setKind(Integer kind) {
        this.kind = kind;
    }

    /**
     * 获取  rainfull
     *
     * @return: Integer  雨量(mm)
     */
    public Double getRainfull() {
        return this.rainfull;
    }

    /**
     * 设置  rainfull
     *
     * @param: rainfull  雨量(mm)
     */
    public void setRainfull(Double rainfull) {
        this.rainfull = rainfull;
    }

    /**
     * 获取  waterLevel
     *
     * @return: Double  水位(m)
     */
    public Double getWaterLevel() {
        return this.waterLevel;
    }

    /**
     * 设置  waterLevel
     *
     * @param: waterLevel  水位(m)
     */
    public void setWaterLevel(Double waterLevel) {
        this.waterLevel = waterLevel;
    }

    /**
     * 获取  flow
     *
     * @return: Double  流量(m3/s)
     */
    public Double getFlow() {
        return this.flow;
    }

    /**
     * 设置  flow
     *
     * @param: flow  流量(m3/s)
     */
    public void setFlow(Double flow) {
        this.flow = flow;
    }

    /**
     * 获取  recordTime
     *
     * @return: Date  记录时间
     */
    public Date getRecordTime() {
        return this.recordTime;
    }

    /**
     * 设置  recordTime
     *
     * @param: recordTime  记录时间
     */
    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public Integer getRecordType() {
        return recordType;
    }

    public void setRecordType(Integer recordType) {
        this.recordType = recordType;
    }
}
