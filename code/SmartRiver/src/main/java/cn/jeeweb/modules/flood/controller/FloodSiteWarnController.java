package cn.jeeweb.modules.flood.controller;


import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.FloodSiteWarn;
import cn.jeeweb.modules.flood.service.IFloodSiteWarnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wsh
 * @version V1.0
 * @Title: 站点警报阈值
 * @Description: 站点警报阈值
 * @date 2018-12-02 23:09:31
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/floodsitewarn")
@RequiresPathPermission("flood:floodsite")
public class FloodSiteWarnController extends BaseCRUDController<FloodSiteWarn, String> {
    @Autowired
    IFloodSiteWarnService floodSiteWarnService;

    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        String parentId = request.getParameter("parentId");
        model.addAttribute("parentId", parentId);
    }


    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<FloodSiteWarn> entityWrapper, HttpServletRequest request,
                            HttpServletResponse response) {
        String parentId = request.getParameter("parentId");
        queryable.addCondition("parentId", parentId);
    }

    @Override
    public void preEdit(FloodSiteWarn entity, Model model, HttpServletRequest request, HttpServletResponse response) {
        String parentId = request.getParameter("parentId");
        model.addAttribute("parentId", parentId);
    }


}
