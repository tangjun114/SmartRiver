package cn.jeeweb.modules.law.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.law.entity.LawEvent;

/**   
 * @Title: 违法事件
 * @Description: 违法事件
 * @author liyonglei
 * @date 2018-11-13 08:40:53
 * @version V1.0   
 *
 */
public interface ILawEventService extends ICommonService<LawEvent> {

}

