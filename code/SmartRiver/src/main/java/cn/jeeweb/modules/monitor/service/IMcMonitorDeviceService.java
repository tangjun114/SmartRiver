package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McMonitorDevice;

/**   
 * @Title: 设备信息
 * @Description: 设备信息
 * @author shawloong
 * @date 2017-10-14 00:12:17
 * @version V1.0   
 *
 */
public interface IMcMonitorDeviceService extends ICommonService<McMonitorDevice> {

}

