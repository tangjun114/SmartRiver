package cn.jeeweb.modules.supervise.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.bi.model.RptDimDataJson;
import com.ff.common.util.format.DateUtil;
import com.ff.common.util.format.JsonConvert;
import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.web.controller.FFRptBaseController;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort.Direction;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.constant.DataResultConst;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;


@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcmonitordatachart")
@RequiresPathPermission("supervise:mcmonitordatachart")
public class McMonitorDataChartController extends FFRptBaseController<McProItemRealTimeMonData>
{

	@Override
	public Queryable GetFilterCondition(BaseReqJson request) {
		// TODO Auto-generated method stub
		Queryable qy = super.GetFilterCondition(request);
		
		qy.addCondition("data_result", DataResultConst.use);
		qy.addOrder(Direction.ASC, "collectTime");
		return qy;
	}

	@Override
	public BaseRspJson Load(@RequestBody BaseReqJson<?> request) {
		// TODO Auto-generated method stub
		BaseRspJson<List<RptDimDataJson> > rsp = new BaseRspJson<List<RptDimDataJson> >();
		List<McProItemRealTimeMonData>  dataList = this.baseService.listWithNoPage(this.GetFilterCondition(request));
		
		 RptDimDataJson req = this.getObj(request, RptDimDataJson.class);
		
		List<RptDimDataJson> obj = new ArrayList<RptDimDataJson>();
		Queryable qy = super.GetFilterCondition(request);
		List<String> points = (List<String>)qy.getValue("measuring_point_code");
		if(null != points)
		{
			//String[] points = point.split(",");
			for(String code : points)
			{
				RptDimDataJson rpt = new RptDimDataJson();
				rpt.setY_name(code);
				rpt.setX_name("时间");
				List<McProItemRealTimeMonData> temp = DataFilterUtil.getObjListFromList(dataList, "measuringPointCode", code);
				List<?> x_data =  DataFilterUtil.getFieldValueFromList(temp, "collectTime");
				List<?> y_data =  DataFilterUtil.getFieldValueFromList(temp, req.getIndicator());
				rpt.setX_data(x_data);
				rpt.setY_data(y_data);
				obj.add(rpt);
			}
		}
		

		 
	 
		rsp.setObj(obj);
		
		
		return rsp;
	}
 	@RequestMapping("/shift")
	public String shift(ModelMap map) {
	 
	 
 
		return ReturnPage();
	}
 	
 	
 	@RequestMapping("/shiftData")
 	@ResponseBody
	public BaseRspJson shiftData(@RequestBody BaseReqJson<?> request) {
		// TODO Auto-generated method stub
		BaseRspJson<List<RptDimDataJson> > rsp = new BaseRspJson<List<RptDimDataJson> >();
		List<McProItemRealTimeMonData>  dataList = this.baseService.listWithNoPage(this.GetFilterCondition(request));
		
		List<RptDimDataJson> obj = new ArrayList<RptDimDataJson>();
		RptDimDataJson req = this.getObj(request, RptDimDataJson.class);
		if(null != dataList)
		{
 			for(McProItemRealTimeMonData e : dataList)
			{
				RptDimDataJson rpt = new RptDimDataJson();
				List<McProItemRealTimeMonData> temp = JsonConvert.jsonToObject(e.getDepthExt(), List.class,McProItemRealTimeMonData.class);

				rpt.setX_name(DateUtil.DateToString(e.getCollectTime()));
				rpt.setY_name("深度m");
				List<?> y_data =  DataFilterUtil.getFieldValueFromList(temp, "depthExt");
				List<?> x_data =  DataFilterUtil.getFieldValueFromList(temp, req.getIndicator());

				rpt.setX_data(x_data);
				rpt.setY_data(y_data);
				obj.add(rpt);
			}
		}
   	 
		rsp.setObj(obj);
		
 		return rsp;
	}
	
	
}
