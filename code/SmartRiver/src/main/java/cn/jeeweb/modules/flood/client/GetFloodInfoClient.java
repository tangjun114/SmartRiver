package cn.jeeweb.modules.flood.client;

import cn.jeeweb.core.utils.http.HttpRequestUtils;
import cn.jeeweb.modules.flood.client.entity.ReqParam;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author:Wish
 * @Date: Created in 2018/12/14 9:50
 **/
public class GetFloodInfoClient {

    private static Logger log = LoggerFactory.getLogger(GetFloodInfoClient.class);


    private String url = "http://shenzhen.cjh.com.cn:8016/swjapp/call.nut";

    protected String token = "d58e76d81a1a4bcd8cd385159e12dc64";


    protected List<ReqParam> reqParams = new ArrayList<>();


    private Map<String, Object> jsonParam() {
        Map<String, Object> map = new HashMap<>();
        map.put("requests", reqParams);
        return map;
    }

    public Map<String, Object> getFloodInfo() {
        String json = JSON.toJSONString(jsonParam());
        String result = HttpRequestUtils.httpPost(url, json);
        System.out.println(result);
        log.info("============>post result:{}", result);
        Map<String, Object> map = (Map<String, Object>) JSON.parse(result);
        return map;
    }


}
