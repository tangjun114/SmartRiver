package cn.jeeweb.modules.sys.entity;

import cn.jeeweb.core.common.entity.DataEntity;
import java.lang.String;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @Title 用户实体
 * @Description:
 * @author 王存见
 * @date 2016-12-03 21:31:50
 * @version V1.0
 *
 */
@TableName("sys_user")
@SuppressWarnings("serial")
public class User extends DataEntity<String> {

	/**
	 * 是否锁定（1：正常；-1：删除；0：锁定；）
	 */
	public static final String STATUS_DELETE = "-1";
	public static final String STATUS_LOCKED = "0";
	public static final String STATUS_NORMAL = "1";

	/** id */
	@TableId(value = "id", type = IdType.UUID)
	private String id;
	// 姓名
	private String username;
	// 用户名
	private String realname;
	// 头像
	private String portrait;
	// 密码
	private String password;
	// 盐
	private String salt;
	// 邮件
	private String email;
	// 联系电话
	private String phone;

	//qq 微信
	private String qqWx;
	
	// 性别
	private String gender= "1";
	
	//职位名字
	private String positionName;
	
	//检测机构名称
	private String monitorName;
	
	//检测机构Id
	private String monitorId;
	
	//部门
	private String deptName;
	/**
	 * 系统用户的状态
	 */
	private String status = STATUS_NORMAL;
	
	
	private String type;
	
	private String subType;
	
	//检测机构名称
	private String md5;
	private String appToken;
	
	//冗余字段
	@TableField(exist=false)
	private String organizationIds;

	/**
	 * 获取 username
	 *
	 * @return: String username
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * 设置 username
	 *
	 * @param: username
	 *             username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 获取 password
	 *
	 * @return: String password
	 */
	public String getPassword() {
		return this.password;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	/**
	 * 设置 password
	 *
	 * @param: password
	 *             password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 获取 id
	 *
	 * @return: String id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * 设置 id
	 *
	 * @param: id
	 *             id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 获取 salt
	 *
	 * @return: String salt
	 */
	public String getSalt() {
		return this.salt;
	}

	/**
	 * 设置 salt
	 *
	 * @param: salt
	 *             salt
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getCredentialsSalt() {
		return username + salt;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the qqWx
	 */
	public String getQqWx() {
		return qqWx;
	}

	/**
	 * @param qqWx the qqWx to set
	 */
	public void setQqWx(String qqWx) {
		this.qqWx = qqWx;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the positionName
	 */
	public String getPositionName() {
		return positionName;
	}

	/**
	 * @param positionName the positionName to set
	 */
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	/**
	 * @return the monitorName
	 */
	public String getMonitorName() {
		return monitorName;
	}

	/**
	 * @param monitorName the monitorName to set
	 */
	public void setMonitorName(String monitorName) {
		this.monitorName = monitorName;
	}

	/**
	 * @return the monitorId
	 */
	public String getMonitorId() {
		return monitorId;
	}

	/**
	 * @param monitorId the monitorId to set
	 */
	public void setMonitorId(String monitorId) {
		this.monitorId = monitorId;
	}

	/**
	 * @return the deptName
	 */
	public String getDeptName() {
		return deptName;
	}

	/**
	 * @param deptName the deptName to set
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
	 * @return the organizationIds
	 */
	public String getOrganizationIds() {
		return organizationIds;
	}

	/**
	 * @param organizationIds the organizationIds to set
	 */
	public void setOrganizationIds(String organizationIds) {
		this.organizationIds = organizationIds;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}
	
	
}
