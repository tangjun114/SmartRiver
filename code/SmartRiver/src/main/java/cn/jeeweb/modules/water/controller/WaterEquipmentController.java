package cn.jeeweb.modules.water.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.water.entity.WaterEquipment;

/**   
 * @Title: 监测站设备
 * @Description: 监测站设备
 * @author liyonglei
 * @date 2018-11-13 21:09:06
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/water/waterequipment")
@RequiresPathPermission("water:waterequipment")
public class WaterEquipmentController extends BaseCRUDController<WaterEquipment, String> {

}
