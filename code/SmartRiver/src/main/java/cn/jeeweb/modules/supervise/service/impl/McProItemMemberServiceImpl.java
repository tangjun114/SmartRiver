package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemMemberMapper;
import cn.jeeweb.modules.supervise.entity.McProItemMember;
import cn.jeeweb.modules.supervise.service.IMcProItemMemberService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目人员
 * @Description: 项目人员
 * @author Jerry
 * @date 2017-12-27 13:40:21
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemMemberService")
public class McProItemMemberServiceImpl  extends CommonServiceImpl<McProItemMemberMapper,McProItemMember> implements  IMcProItemMemberService {

}
