package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McMonitorResultStatisticsMapper;
import cn.jeeweb.modules.supervise.entity.McMonitorResultStatistics;
import cn.jeeweb.modules.supervise.service.IMcMonitorResultStatisticsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 监测结果统计
 * @Description: 监测结果统计
 * @author aether
 * @date 2018-01-24 21:32:05
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorResultStatisticsService")
public class McMonitorResultStatisticsServiceImpl  extends CommonServiceImpl<McMonitorResultStatisticsMapper,McMonitorResultStatistics> implements  IMcMonitorResultStatisticsService {

}
