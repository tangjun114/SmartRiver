package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodAssetsStock;
 
/**   
 * @Title: 库存管理数据库控制层接口
 * @Description: 库存管理数据库控制层接口
 * @author liyonglei
 * @date 2018-11-09 16:59:23
 * @version V1.0   
 *
 */
public interface FloodAssetsStockMapper extends BaseMapper<FloodAssetsStock> {
    
}