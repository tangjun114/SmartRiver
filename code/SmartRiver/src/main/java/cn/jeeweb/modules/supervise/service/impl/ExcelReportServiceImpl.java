package cn.jeeweb.modules.supervise.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.ff.common.util.chart.ChartUtil;
import com.ff.common.util.format.DateUtil;
import com.ff.common.util.format.JsonConvert;
import com.ff.common.util.log.FFLogFactory;
import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.util.meta.ReflectionUtil;
import com.ff.common.util.validate.ValidatorUtil;

import cn.jeeweb.cache.MonitorTypeParaCache;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.SpringContextHolder;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolDetail;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolLog;
import cn.jeeweb.modules.monitor.entity.McProItemAlarm;
import cn.jeeweb.modules.supervise.entity.McProItemImplementLog;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;
import cn.jeeweb.modules.supervise.entity.McProItemStandard;
import cn.jeeweb.modules.supervise.entity.McProReport;
import cn.jeeweb.modules.supervise.model.SumDataModel;
import cn.jeeweb.modules.supervise.model.report.GDSimpleForm;

@Scope("prototype")
@Component("excelReportService")
public class ExcelReportServiceImpl extends ExcelReportServiceAbastracImpl
{
	
	protected GDSimpleForm form ;

	protected Logger log = FFLogFactory.getLog(ExcelReportServiceImpl.class);


	@Override
	public String getTemplate() {
		// TODO Auto-generated method stub
		return this.getBasePath()+"/template/template.xlsx";
	}
	
	@Override
	public void setData(McProReport report) {
		// TODO Auto-generated method stub
		super.setData(report);
		form = JsonConvert.jsonToObject(report.getReportExtend(), GDSimpleForm.class);
		if(null == form)
		{
			form = new GDSimpleForm();
		}
	}

	public String getOutFile()
	{
		String path="/data/report/"+UUID.randomUUID().toString()+".xlsx";
		return path;
	}
	public String getOutImage()
	{
		String path=this.getBasePath() + "/data/image/"+UUID.randomUUID().toString()+".jpg";
		return path;
	}

	public void createIndex(Workbook workbook)
	{
		defualtStyle = null;
		Sheet sheet = workbook.getSheetAt(0);
		
		createCell(1, 1, this.project.getProjectType(), sheet);
 		createCell(7, 2, this.project.getName(), sheet);
 		createCell(8, 2, this.project.getFullAddress(), sheet);
 		createCell(9, 2, this.project.getEntrustUnit(), sheet);
 		String date = DateUtil.DateToString(form.getStartDate(), "yyyy年MM月dd日") + "～";
 		date +=DateUtil.DateToString(form.getEndDate(), "yyyy年MM月dd日");
 		createCell(10, 2, date, sheet);
 		//createCell(11, 2, this.project.getName(), sheet);
 		createCell(12, 2, this.form.getReportID(), sheet);
 		
 		createCell(15, 0, this.org.getName(), sheet);
 		createCell(16, 0, DateUtil.getUpperDate(DateUtil.getCurrentDate()), sheet);
	}

	public void createTitlePage(Workbook workbook)
	{
		Sheet sheet = workbook.getSheetAt(1);
		createCell(15, 1, this.org.getAddress(), sheet);
		createCell(15, 4, this.org.getEmail(), sheet);
 		createCell(16, 1, this.org.getPhone(), sheet);
 		createCell(16, 4, this.org.getFax(), sheet);
  		createCell(18, 3, DateUtil.getUpperDate(DateUtil.getCurrentDate()), sheet);
	}
	
	public void createCatalog(Workbook workbook)
	{
		Sheet sheet = workbook.getSheetAt(2);
		
		createCell(8, 2, this.project.getFullAddress(), sheet);
	}
	
	public void createMain(Workbook workbook)
	{
		Sheet sheet = workbook.getSheetAt(3);
		int row = 0;
		createCell(row, 0, row, 8, project.getName(), sheet);

		row = 4;
		
		//Row temp = sheet.getRow(row);
		//temp.setHeight((short) (25 * 80));
		createCell(row, 0, row, 8, project.getNote(), sheet);
		
		createCell(6, 2,   project.getName(), sheet);
		createCell(7, 2,   project.getFullAddress(), sheet);
		createCell(8, 2,   project.getBuildUnit(), sheet);
		createCell(9, 2,   project.getDesignUnit(), sheet);
		createCell(10, 2,   project.getMainUnit(), sheet);

		createCell(11, 2,   project.getImplementUnit(), sheet);
		createCell(12, 2,   project.getEntrustUnit(), sheet);
		createCell(13, 2,   project.getSupervisingUnit(), sheet);
		createCell(14, 2,   project.getMonitorOrgName(), sheet);

		createCell(15, 2,   project.getProtectType(), sheet);
		createCell(15, 7,  DateUtil.DateToString(project.getStartDate(),"yyyy-MM-dd"), sheet);
		
		createCell(16, 2,   project.getArea(), sheet);
		createCell(16, 7,   project.getDepth(), sheet);
		
		createCell(17, 2,   project.getLevel(), sheet);
		createCell(17, 2,   project.getRemarks(), sheet);
		
		Wrapper<McProItemMonitorItem> wrapper = new EntityWrapper<McProItemMonitorItem>(McProItemMonitorItem.class);
		wrapper.setSqlSelect("distinct(standard_id) as standardId,"
				              + "standard_name as standardName ");
 		wrapper.eq("projectId", project.getId());
 		List<McProItemMonitorItem> starndPointList = itemService.selectList(wrapper );
 		
 		
		Wrapper<McProItemStandard> standardWrap = new EntityWrapper<McProItemStandard>(McProItemStandard.class);
	 
		standardWrap.in("id", DataFilterUtil.getFieldValueFromList(starndPointList, "standardId"));
 		List<McProItemStandard> starndItemList = itemStandardService.selectList(standardWrap );
  	 
 		String standardStr = "";
 		int i =0;
 		for(i=0;i<starndItemList.size();i++)
 		{
 			standardStr += "(" + (i+1) + ") 《" + starndItemList.get(i).getStandardName() +"》" + "("+starndItemList.get(i).getStandardCode()+")；"+"\n";
 		}
 		CellStyle style = workbook.createCellStyle();
 	
 		Font fontStyle = workbook.createFont(); // 字体样式  
	    fontStyle.setFontName("宋体"); // 字体  
	    fontStyle.setFontHeightInPoints((short) 11);
	    style.setFont(fontStyle);
 		style.setAlignment(CellStyle.ALIGN_LEFT); 
 		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
 		style.setWrapText(true);
 		Row temp = sheet.getRow(22);
 		temp.setHeight((short)((i+2)*11*25));
		createCell(22, 0, 22,8,  standardStr, sheet,style);

		Wrapper<McProItemMeasuringPoint> pointWrapper = new EntityWrapper<McProItemMeasuringPoint>(McProItemMeasuringPoint.class);


		pointWrapper.eq("projectId", project.getId());
		pointWrapper.setSqlSelect(" count(id) as id,"
				+ "monitor_item_id as monitorItemId, "
				+ "monitor_item_name as monitorItemName  "
				 );
		pointWrapper.groupBy("monitorItemId");
 		List<McProItemMeasuringPoint> pointList = pointService.selectList(pointWrapper );
 		
		
 		Wrapper<McProItemRealTimeMonData> dataWrap = new EntityWrapper<McProItemRealTimeMonData>(McProItemRealTimeMonData.class);
 		dataWrap.eq("projectId", project.getId());
 		dataWrap.setSqlSelect(" count(id) as projectCount,"
				+ "monitor_item_id as monitorItemId, "
				+ "monitor_item_name as monitorItemName  "
				 );
 		dataWrap.groupBy("monitorItemId");
 		List<McProItemRealTimeMonData> dataList = dataService.selectList(dataWrap );
 		
 
 		int start = 26;
 		i=1;
 		for(McProItemMeasuringPoint point :pointList)
 		{
 			row=i+start;
 			//insertRow(row, sheet);
 			copy(start, start, row, sheet, workbook);
 			createCell(row, 0,   String.valueOf(i), sheet);

 			createCell(row, 1,   point.getMonitorItemName(), sheet);
 			createCell(row, 3,   point.getMonitorItemName(), sheet);
 			createCell(row, 5,   point.getId(), sheet);
 			McProItemRealTimeMonData data = DataFilterUtil.getObjFromList(dataList, "monitorItemId", point.getMonitorItemId());
 			int count = 0;
 			if(null != data)
 			{
 				count = data.getProjectCount();
 			}
 			createCell(row, 7,   String.valueOf(count), sheet);	
 			
 			i++;
 		}
   		
 		
		Wrapper<McProItemMeasuringPoint> devcieWrapper = new EntityWrapper<McProItemMeasuringPoint>(McProItemMeasuringPoint.class);


		devcieWrapper.setSqlSelect("distinct(device_id) as deviceId,"
	            + "device_type_name as deviceTypeName, "
	            + "device_model_name as deviceModelName, "
	            + "monitor_item_type_name as monitorItemTypeName, "
				+ "device_name as deviceName ");
		
		devcieWrapper.eq("projectId", project.getId());
 		List<McProItemMeasuringPoint> deviceList = pointService.selectList(devcieWrapper );
 		
 		row += 4; 
 		start = row;
 		i=1;
 		for(McProItemMeasuringPoint point :deviceList)
 		{
 			row= start + i;
 			//insertRow(row, sheet);
 			copy(start, start, row, sheet, workbook);
 			createCell(row, 0,   String.valueOf(i), sheet);

 			createCell(row, 1,   point.getDeviceTypeName(), sheet);
 			createCell(row, 3,   point.getDeviceModelName(), sheet);
 			createCell(row, 5,   point.getDeviceName(), sheet);
  			createCell(row, 7,   point.getMonitorItemTypeName(), sheet);	
  			i++;
 		}
 		
 		
		Wrapper<McProItemImplementLog> implWrapper = new EntityWrapper<McProItemImplementLog>(McProItemImplementLog.class);
		implWrapper.le("createDate", form.getEndDate());

 		implWrapper.eq("projectId", project.getId());
 		implWrapper.orderBy("serial",true);
 		List<McProItemImplementLog> logList = implementLogService.selectList(implWrapper );
 		
 		row += 4; 
 		start = row;
 		i=1;
 		for(McProItemImplementLog e :logList)
 		{
 			row= start + i;
 			//insertRow(row, sheet);
 			copy(start, start, row, sheet, workbook);
 			createCell(row, 0,   e.getSerial(), sheet);

 			style = workbook.createCellStyle();
 			style.setWrapText(true);
 			style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
 			style.setAlignment(CellStyle.ALIGN_LEFT); 
 			style.setBorderBottom(CellStyle.BORDER_THIN);
 			style.setBorderLeft(CellStyle.BORDER_THIN);
 			style.setBorderRight(CellStyle.BORDER_THIN);
 			style.setBorderTop(CellStyle.BORDER_THIN);
 			createCell(row, 1,row,6,   e.getContent(), sheet,style);
   			createCell(row, 7,   DateUtil.DateToString(e.getCreateDate(), "yyyy-MM-dd"), sheet);	
  			i++;
 		}
 		
 		row = mainCheck(row, sheet, workbook);
 		row += 2;
 		List<SumDataModel> sumList = dataService.getSumData(this.project.getId(),null,null);
 		
 		row += 2;
 		start = row;
 		i=0;
 		for(SumDataModel sum :sumList)
 		{
 			row = start +i*3;
 			if(i<sumList.size()-1)
 			{
 				copy(start, start+2, row+3, sheet, workbook);
 			}
 			
 			style = workbook.createCellStyle();
 			style.setAlignment(CellStyle.ALIGN_CENTER); 
 			style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
 			style.setWrapText(true);
 			style.setBorderBottom(CellStyle.BORDER_THIN);
 			style.setBorderLeft(CellStyle.BORDER_THIN);
 			style.setBorderRight(CellStyle.BORDER_THIN);
 			style.setBorderTop(CellStyle.BORDER_THIN);
 			createCell(row, 0,row+2,0,  String.valueOf(i), sheet,style);
 			createCell(row, 1,row+2,1,  sum.getMonitorItemName(), sheet,style);
 			createCell(row, 2,row,2+1,  sum.getMaxName(), sheet);
 			createCell(row, 4,  sum.getMaxValue(), sheet);
 			createCell(row, 5,  sum.getMaxPoint(), sheet);
 			createCell(row, 6,row+1, 6,  sum.getAlarmValue(), sheet);
 			createCell(row, 7,row+1, 7, sum.getControlValue(), sheet);
 			createCell(row, 8,row+2, 8,  sum.getAlarmPoint(), sheet);
 			
 			createCell(row+1, 2,row+1, 2+1,  sum.getMinName(), sheet);
 			createCell(row+1, 4,  sum.getMinValue(), sheet);
 			createCell(row+1, 5,  sum.getMinPoint(), sheet);
 			
 			createCell(row+2, 2, row+2, 2+1,  sum.getRateName(), sheet);
 			createCell(row+2, 4,  sum.getRateValue(), sheet);
 			createCell(row+2, 5,  sum.getRatePoint(), sheet);
 			createCell(row+2, 6,  sum.getRateAlarmValue(), sheet);
 			createCell(row+2, 7,  sum.getRateControlValue(), sheet);
  			
  			i++;
 		}
 		
 		String note = "备注:\n";
 		for(i=0;i<itemList.size();i++)
 		{
 			note +="("+(i+1)+")"+ itemList.get(i).getMonitorItemName() + "：" + itemList.get(i).getRemarks() + "\n";
  		}
 		row +=3;
 		{
 		temp = sheet.getRow(row);
 		temp.setHeight((short)((i+1)*15*25));
 		style = createDefaultStyle(workbook);
 	    fontStyle = workbook.createFont(); // 字体样式  
	    fontStyle.setFontName("宋体"); // 字体  
	    fontStyle.setFontHeightInPoints((short) 12);
	    style.setFont(fontStyle);
 		style.setAlignment(CellStyle.ALIGN_LEFT); 
 		style.setWrapText(true);
 		createCell(row, 0,row,8, note, sheet,style);
 		}
 		
 		row += 5;
 		createCell(row, 0, form.getMonitorResultAnalysis(), sheet);
 		
 		
 		
 		Wrapper<McProItemAlarm> alarmWrapper = new EntityWrapper<McProItemAlarm>(McProItemAlarm.class);
  		alarmWrapper.between("createDate", form.getStartDate(), form.getEndDate());
 		alarmWrapper.eq("projectId", project.getId());
 		List<McProItemAlarm> newAlarmList = alarmService.selectList(alarmWrapper);
  		row += 1;
  		
  		CellStyle alarmStyle = workbook.createCellStyle();
  		alarmStyle.setWrapText(true);
  		fontStyle = workbook.createFont(); 
  		fontStyle.setBold(true);
  		alarmStyle.setFont(fontStyle);
  		for(i=0 ;i <newAlarmList.size();i++)
 		{
 			row++;
 			String val = this.getAlarmDesp(i+1,newAlarmList.get(i));
 			createCell(row, 0,row,8,val, sheet,alarmStyle);
 			Row r = sheet.getRow(row);
 			r.setHeight((short)(25*28));
 			insertRow(row+1,sheet);
 		}
 		
  		
 		alarmWrapper = new EntityWrapper<McProItemAlarm>(McProItemAlarm.class);
  		alarmWrapper.lt("createDate", form.getStartDate());
 		alarmWrapper.eq("projectId", project.getId());
 	    newAlarmList = alarmService.selectList(alarmWrapper);
  		row += 2;
  		for(i=0 ;i <newAlarmList.size();i++)
 		{
 			row++;
 			String val = this.getAlarmDesp(i+1,newAlarmList.get(i));
 			createCell(row, 0,row,8,val, sheet,alarmStyle);
 			Row r = sheet.getRow(row);
 			r.setHeight((short)(25*28));
 			insertRow(row+1,sheet);
 			//copy(row, row, row+1, sheet, workbook);
 		}
 		
 		row += 3;
  		
 		createCell(row, 0, form.getSuggest(), sheet);
 		
 		
 		row += 4;
 		createCell(row, 6, DateUtil.getUpperDate(DateUtil.getCurrentDate()), sheet);

 	}
	private String getAlarmDesp(int i,McProItemAlarm alarm)
	{
		String unit = SpringContextHolder.getBean(MonitorTypeParaCache.class).get(alarm.getMonitorTypeCode()).getUnit();
		String val = "(" + i +")"+ alarm.getMonitorItemName();
		val +=  "测点" + alarm.getMeasuringPointCode() +"于" + DateUtil.DateToString(alarm.getCreateDate(), "yyyy年MM月dd日");
		val += "累计变化量为" + alarm.getSumValue() +unit + ",";
		val += alarm.getAlarmDesp() + ",";
		val += "当前值为" + String.format("%.1f", alarm.getNowValue()) + unit;

		return val;
	}
	private int mainCheck(int row,Sheet sheet,Workbook workbook)
	{

		 CellStyle style = workbook.createCellStyle(); // 单元格样式  
		 style.setBorderBottom(CellStyle.BORDER_THIN);
		 style.setBorderLeft(CellStyle.BORDER_THIN);
		 style.setBorderRight(CellStyle.BORDER_THIN);
		 style.setBorderTop(CellStyle.BORDER_THIN);
		 
		Wrapper<McMonitorPatrolLog> wrapper = new EntityWrapper<McMonitorPatrolLog>(McMonitorPatrolLog.class);
		wrapper.between("patrol_date", form.getStartDate(), form.getEndDate());
		wrapper.eq("projectId", project.getId());
		wrapper.orderBy("patrol_date",true);
 		List<McMonitorPatrolLog> logList = mcMonitorPatrolLogService.selectList(wrapper );
 		
		Wrapper<McMonitorPatrolDetail> detailWrap = new EntityWrapper<McMonitorPatrolDetail>(McMonitorPatrolDetail.class);
		wrapper.between("collect_time", form.getStartDate(), form.getEndDate());
 		detailWrap.eq("projectId", project.getId());
 		List<McMonitorPatrolDetail> detailList = mcMonitorPatrolDetailService.selectList(detailWrap );
 		row += 3;
 		int start = row;
 		
 		for(int i=0;i<logList.size();i++)
 		{
 			
 			
 			McMonitorPatrolLog log = logList.get(i);

 			Map<String,List<McMonitorPatrolDetail>> mapDetail = new HashMap<String,List<McMonitorPatrolDetail>>();
 			
 			
 			for(McMonitorPatrolDetail detail :detailList)
 			{
 				if(detail.getPatrolId().equals(log.getId()))
 				{
 	 				List<McMonitorPatrolDetail> temp = mapDetail.get(detail.getSituationTypeName());
 	 				if(null == temp)
 	 				{
 	 					temp = new ArrayList<McMonitorPatrolDetail>();
 	 				}
 	 				temp.add(detail);
 	 				mapDetail.put(detail.getSituationTypeName(), temp);
 				}
  			}
 			if(mapDetail.isEmpty())
 			{
 				continue;
 			}
 			if(i==0)
 			{
 				
 			}
 			else
 			{
 				copy(start, start, row, sheet, workbook);
 			}
 			createCell(row, 2,  DateUtil.DateToString(log.getPatrolDate(), "yyyy-MM-dd"), sheet);
 			row++;
 			for(String key : mapDetail.keySet())
 			{
 				List<String> picList = new ArrayList<String>();
 				int now = row;
 				for(McMonitorPatrolDetail e :mapDetail.get(key))
 				{
 					String temp = e.getDetailName() + ":" + e.getAbnormalResult();
 					if(!ValidatorUtil.isEmpty(e.getAbnormalDesc()))
 					{
 						temp += e.getAbnormalDesc();
 					}
  					insertRow(row, sheet);
  					
  					style = workbook.createCellStyle();
  	 				style.setWrapText(true);
	  	 			 style.setBorderBottom(CellStyle.BORDER_THIN);
	  	 			 style.setBorderLeft(CellStyle.BORDER_THIN);
	  	 			 style.setBorderRight(CellStyle.BORDER_THIN);
	  	 			 style.setBorderTop(CellStyle.BORDER_THIN);
  	 				style.setAlignment(CellStyle.ALIGN_LEFT);
 					createCell(row, 2, row,8,temp, sheet,style);
 					if(!ValidatorUtil.isEmpty(e.getAbnormalImgs()))
 					{
 						String[] pic = e.getAbnormalImgs().split(","); 
 						picList.addAll(Arrays.asList(pic));
 					}
 					row++;
  				}
 				for(int j=0;j<picList.size();j += 2)
 				{
 					insertRow(row, sheet);
 					Row temp = sheet.getRow(row);
 					temp.setHeight((short) (25 * 80));
 					
 					
 					
 					createImage(row, 2, row, 5, this.getBasePath()+"/"+picList.get(j), sheet,0.9,1);
 
 					
 					if(j+1<picList.size())
 					{
 	 					createImage(row, 6, row, 8, this.getBasePath()+"/"+picList.get(j+1), sheet);
  					}
 					else
 					{
 						
 					}
 					createCell(row, 2, row, 8, "", sheet,style);
 					row++;
 				}
 				style = workbook.createCellStyle();
 				style.setWrapText(true);
 				style.setAlignment(CellStyle.ALIGN_CENTER);
 				style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
 				style.setAlignment(CellStyle.ALIGN_LEFT); 
 	 			style.setBorderBottom(CellStyle.BORDER_THIN);
 	 			style.setBorderLeft(CellStyle.BORDER_THIN);
 	 			style.setBorderRight(CellStyle.BORDER_THIN);
 	 			style.setBorderTop(CellStyle.BORDER_THIN);
 				createCell(now, 0,row-1,1, key, sheet,style);
 			}
 	}
 		if(start==row)
 		{
 			row++;
 		}
 		
 		return row;
	}
	public void createContent(Workbook workbook)
	{
		 
		defualtStyle = createDefaultStyle(workbook);	
	        

 		for(int i=0;i<itemList.size();i++)
 		{
 			if(monitorTypeParaCache.isShift(itemList.get(i).getGroupTypeCode()))
 			{
 				createShiftSheet(i+1,itemList.get(i),workbook);
  			}
 			else
 			{
 				
 	 			 createItemSheet(i+1,itemList.get(i),workbook);
 			}
 		}
		
	}
	private void createShiftSheet(int sheetNum,McProItemMonitorItem item,Workbook workbook)
	{
		Sheet sheet = workbook.createSheet(item.getMonitorItemName()+"观测结果表");
		
		for(int i=0;i<25;i++)
		{
			sheet.setColumnWidth(i, 6*256);
		}
		sheet.setColumnWidth(0, 8*256);
		
		Wrapper<McProItemMeasuringPoint> wrapper1 = new EntityWrapper<McProItemMeasuringPoint>(McProItemMeasuringPoint.class);
		wrapper1.eq("monitorItemId", item.getId());
		wrapper1.setSqlSelect(" distinct(device_id) as device_id,CONCAT(device_model_name,'(',device_name,')') as deviceName");
 		List<McProItemMeasuringPoint> pointList1 = pointService.selectList(wrapper1 );
 		String device = "";
 		for(McProItemMeasuringPoint point:pointList1)
 		{
 			if(null != point.getDeviceName())
 			{
 				device +=point.getDeviceName() + ",";
 			}
 			
 		}
 		item.setDeviceModelName(device);
 		
		Wrapper<McProItemMeasuringPoint> wrapper = new EntityWrapper<McProItemMeasuringPoint>(McProItemMeasuringPoint.class);
		wrapper.eq("monitorItemId", item.getId());
		wrapper.orderBy("measuringPointCode", true);
 		List<McProItemMeasuringPoint> pointList = pointService.selectList(wrapper );
 		
 		int startRow = 0;
 		int page = 1;
 		for(int k=0;k<pointList.size();k++)
 		{
 			McProItemMeasuringPoint point = pointList.get(k);
 			List<McProItemMeasuringPoint> depthList = JsonConvert.jsonToObject(point.getDepthExt(), List.class,McProItemMeasuringPoint.class);
 			if(null == depthList)
 			{
 				depthList = new ArrayList<McProItemMeasuringPoint>();
 			}
 			
 			for(McProItemMeasuringPoint e: depthList)
 			{
 				e.setMeasuringPointCode(e.getDepthExt());
 			}
 			
 	 		Wrapper<McProItemRealTimeMonData> dataWrap = new EntityWrapper<McProItemRealTimeMonData>(McProItemRealTimeMonData.class);
 	 		dataWrap.eq("measuring_point_id", point.getId());
 	 		dataWrap.between("collect_time", form.getStartDate(), form.getEndDate());
  	 		List<McProItemRealTimeMonData> dataList = dataService.selectList(dataWrap );
  	 		List<McProItemRealTimeMonData> newDataList =  new ArrayList<McProItemRealTimeMonData>();
  	 		for(McProItemRealTimeMonData e :dataList)
  	 		{
  	 			List<McProItemRealTimeMonData> temp = JsonConvert.jsonToObject(e.getDepthExt(),List.class, McProItemRealTimeMonData.class);
  	 		    if(null != temp)
  	 		    {
  	 		    	for(McProItemRealTimeMonData t: temp)
  	 		    	{
  	 		    		t.setMeasuringPointCode(t.getDepthExt());
  	 		    		t.setProjectCount(e.getProjectCount());
  	 		    	}
  	 		    	newDataList.addAll(temp);
  	 		    }
  	 		}
  	 		Map<String,McProItemRealTimeMonData> dataMap = listToMap(newDataList);

  	 		Wrapper<McProItemRealTimeMonData> countWrap = new EntityWrapper<McProItemRealTimeMonData>(McProItemRealTimeMonData.class);
  	 		countWrap.eq("measuring_point_id", point.getId());
  	 		countWrap.between("collect_time", form.getStartDate(), form.getEndDate());
  	  		countWrap.orderBy("project_count", true);
  	 		countWrap.setSqlSelect("distinct(project_count) as projectCount,collect_time as collectTime");
  	 		List<McProItemRealTimeMonData> countList = dataService.selectList(countWrap );
  	 		
 
  	 		
  	 		int xSize = 3;
  	 		int ySize = 50;
  	 		int pageSize = 64;
  	 		int index = 1;
  	 		for(int i=0;i<depthList.size();i=i+ySize)
  	 		{
  	 			List<McProItemMeasuringPoint> temp;
  	 			if(i+ySize>depthList.size())
  	 			{
  	 				temp = depthList.subList(i, depthList.size());
  	 			}
  	 			else
  	 			{
  	 				temp = depthList.subList(i, i+ySize);
  	 			}
  	 			String name = "附图表   " +sheetNum + "-"+ (k+1) +"-" + index + "  "+ point.getMeasuringPointCode() +"号"+ item.getMonitorItemName();

  	 			if(ValidatorUtil.isEmpty(countList))
  	 			{
  	 				createTitle(startRow,item, name,sheet);
  		 	 		createData(startRow+4,temp,null,dataMap,item,sheet);
  		 	 		startRow = page*pageSize;
  		 	 		page++;
  		 	 	    index++;
  	 			}
  	 			else
  	 			{
  	 				for(int j=0;j<countList.size();j+=xSize)
  	 	 			{
  	 	 				List<McProItemRealTimeMonData> temp2;
  	 	 				if(j+xSize>countList.size())
  	 	 	 			{
  	 	 	 				temp2 = countList.subList(j, countList.size());
  	 	 	 			}
  	 	 	 			else
  	 	 	 			{
  	 	 	 				temp2 = countList.subList(j, j+xSize);
  	 	 	 			}
  	 	  	 			 name = "附图表   " +sheetNum + "-"+ (k+1) +"-" + index + "  "+ point.getMeasuringPointCode() +"号"+ item.getMonitorItemName();

  	 	 				createTitle(startRow, item, name,sheet);
  	 	 	 			createData(startRow+4,temp,temp2,dataMap,item,sheet);
  	 	 	 			startRow = page*pageSize;
  	 	 	 			page++;
  	 	 	 	   	    index++;
  	 	  			}
  	 			}
  	 			
  	 		}
 		}

	}
	private void createItemSheet(int sheetNum,McProItemMonitorItem item,Workbook workbook)
	{
		Sheet sheet = workbook.createSheet(item.getMonitorItemName()+"观测结果表");
 
		for(int i=0;i<25;i++)
		{
			sheet.setColumnWidth(i, 6*256);
		}
		sheet.setColumnWidth(0, 8*256);
		
		Wrapper<McProItemMeasuringPoint> wrapper1 = new EntityWrapper<McProItemMeasuringPoint>(McProItemMeasuringPoint.class);
		wrapper1.eq("monitorItemId", item.getId());
		wrapper1.setSqlSelect(" distinct(device_id) as device_id,CONCAT(device_model_name,'(',device_name,')') as deviceName");
 		List<McProItemMeasuringPoint> pointList1 = pointService.selectList(wrapper1 );
 		String device = "";
 		for(McProItemMeasuringPoint point:pointList1)
 		{
 			if(null != point.getDeviceName())
 			{
 				device +=point.getDeviceName() + ",";
 			}
 			
 		}
 		item.setDeviceModelName(device);
		
		
		
		Wrapper<McProItemMeasuringPoint> wrapper = new EntityWrapper<McProItemMeasuringPoint>(McProItemMeasuringPoint.class);
		wrapper.eq("monitorItemId", item.getId());
		wrapper.orderBy("measuringPointCode", true);
 		List<McProItemMeasuringPoint> pointList = pointService.selectList(wrapper );
 		
 		
 		Wrapper<McProItemRealTimeMonData> dataWrap = new EntityWrapper<McProItemRealTimeMonData>(McProItemRealTimeMonData.class);
 		dataWrap.eq("monitorItemId", item.getId());
 		dataWrap.between("collect_time", form.getStartDate(), form.getEndDate());
 		List<McProItemRealTimeMonData> dataList = dataService.selectList(dataWrap );
 		Map<String,McProItemRealTimeMonData> dataMap = listToMap(dataList);
 		
 		Wrapper<McProItemRealTimeMonData> countWrap = new EntityWrapper<McProItemRealTimeMonData>(McProItemRealTimeMonData.class);
 		countWrap.eq("monitorItemId", item.getId());
 		countWrap.between("collect_time", form.getStartDate(), form.getEndDate());
  		countWrap.orderBy("project_count", true);
 		countWrap.setSqlSelect("distinct(project_count) as projectCount,collect_time as collectTime");
 		List<McProItemRealTimeMonData> countList = dataService.selectList(countWrap );
 		
 		
 		int startRow = 0;
 		int page = 1;
 		
 		int xSize = 4;
 		int ySize = 20;
 		int pageSize = 47;
 		
 		for(int i=0;i<pointList.size();i=i+ySize)
 		{
 			List<McProItemMeasuringPoint> temp;
 			if(i+ySize>pointList.size())
 			{
 				temp = pointList.subList(i, pointList.size());
 			}
 			else
 			{
 				temp = pointList.subList(i, i+ySize);
 			}
 			
 			String name = "附图表   " +sheetNum + "-1-" + page + "  "+ item.getMonitorItemName();
 			if(ValidatorUtil.isEmpty(countList))
 			{
 				createTitle(startRow,item, name,sheet);
	 	 		createData(startRow+4,temp,null,dataMap,item,sheet);
	 	 		startRow = page*pageSize;
	 	 		page++;
 			}
 			else
 			{
 				for(int j=0;j<countList.size();j+=xSize)
 	 			{
 	 				List<McProItemRealTimeMonData> temp2;
 	 				if(j+xSize>countList.size())
 	 	 			{
 	 	 				temp2 = countList.subList(j, countList.size());
 	 	 			}
 	 	 			else
 	 	 			{
 	 	 				temp2 = countList.subList(j, j+xSize);
 	 	 			}
 	 	 		    name = "附图表   " +sheetNum + "-1-" + page + "  "+ item.getMonitorItemName();

 	 				createTitle(startRow, item, name,sheet);
 	 	 			createData(startRow+4,temp,temp2,dataMap,item,sheet);
 	 	 			startRow = page*pageSize;
 	 	 			page++;
 	  			}
 			}
 			
 		}
 	} 
	
	private Map<String,McProItemRealTimeMonData> listToMap(List<McProItemRealTimeMonData> dataList)
	{
		Map<String,McProItemRealTimeMonData> dataMap =new HashMap<String,McProItemRealTimeMonData>();
 		
	 
		for(McProItemRealTimeMonData data :dataList)
		{
			String key = data.getProjectCount()+data.getMeasuringPointCode();
			String maxKey = data.getProjectCount() + "max";
			String minKey = data.getProjectCount() + "min";
			String maxRateKey = data.getProjectCount() + "maxRate";
			if(null == dataMap.get(key))
			{
				dataMap.put(key, data);
			}
			
			if(data.getSumValue()>0)
			{
				if(null == dataMap.get(maxKey))
				{
					dataMap.put(maxKey, data);
				}
				else
				{
					if(dataMap.get(maxKey).getSumValue() < data.getSumValue())
					{
						dataMap.put(maxKey, data);
					}
				}
			}
			
			
			if(data.getSumValue()<0)
			{
				if(null == dataMap.get(minKey))
				{
					dataMap.put(minKey, data);
				}
				else
				{
					if(dataMap.get(minKey).getSumValue() > data.getSumValue())
					{
						dataMap.put(minKey, data);
					}
				}
			}
			
			
			if(null == dataMap.get(maxRateKey))
			{
				dataMap.put(maxRateKey, data);
			}
			else
			{
				if(dataMap.get(maxRateKey).getChangeRate() < data.getChangeRate())
				{
					dataMap.put(maxRateKey, data);
				}
			}
			
			 
		}
		return dataMap;
	}
	
	
	private void createTitle(int startRow,McProItemMonitorItem item,String name,Sheet sheet)
	{
		log.info("create excel " + name);
		int row = startRow;
		{
	        CellStyle cellStyle  = createDefaultStyle(sheet.getWorkbook());
	        Font fontStyle = sheet.getWorkbook().createFont(); // 字体样式  
	        fontStyle.setBoldweight(Font.BOLDWEIGHT_BOLD); // 加粗  
 	        fontStyle.setFontHeightInPoints((short) 14); // 大小  
 	        cellStyle.setFont(fontStyle);  
	 
  			createCell(row, 0, row, 13,name, sheet,cellStyle);
		}
		
		{
 
		    row++;
			createCell(row, 0, row, 1, "工程名称:", sheet);
			createCell(row, 2, row, 6, project.getName(), sheet);
			createCell(row, 7, row, 8, "监测项目:", sheet);
			createCell(row, 9, row, 13, item.getMonitorItemName(), sheet);
		}
		{
			 
		 
			row++;
			createCell(row, 0, row, 1, "工程地点:", sheet);
			createCell(row, 2, row, 6, project.getFullAddress(), sheet);
			createCell(row, 7, row, 8, "监测仪器:", sheet);
			createCell(row, 9, row, 13, item.getDeviceModelName(), sheet);
		}
		{
			 
			row++;
			createCell(row, 0, row, 1, "依据规范:", sheet);
			createCell(row, 2, row, 6, item.getStandardName(), sheet);
			createCell(row, 7, row, 8, "单    位:", sheet);
			createCell(row, 9, row, 13, monitorTypeParaCache.get(item.getGroupTypeCode()).getUnit(), sheet);
		}
	 
	}
	
	private String getValue(String key,String field,Map<String,McProItemRealTimeMonData> dataMap)
	{
		String value = "\\";
		if(key.endsWith("min") || key.endsWith("max") || key.endsWith("maxRate") )
		{
		  value = "--";
		}
		
		
		
		McProItemRealTimeMonData data = dataMap.get(key);
		
		//java.text.DecimalFormat   df   =  new   java.text.DecimalFormat("#.00");  
		if(null != data)
		{
			Double tmp = (Double) ReflectionUtil.getValueByFieldName(data, field);
			//value = df.format(tmp);
			if(key.endsWith("maxRate"))
			{
				value = String.format("%.2f", tmp);
			}
			else
			{
				value = String.format("%.1f", tmp);
			}
			
			if(key.endsWith("min") || key.endsWith("max") || key.endsWith("maxRate") )
			{
			  value += "("+data.getMeasuringPointCode()+ ")";
			}
		}
		return value;
	}
	
	
	private void createData(int startRow,List<McProItemMeasuringPoint> pointList,List<McProItemRealTimeMonData> countList,Map<String,McProItemRealTimeMonData> dataMap,McProItemMonitorItem item,Sheet sheet)
	{
		int rowSize = 20;
		int colSize = 4;
		int startCol = 2;
		if(monitorTypeParaCache.isShift(item.getGroupTypeCode()))
		{
			rowSize = 50;
			colSize = 3;
			startCol = 1;
		}
		
		
		int row = startRow;
		{
	        CellStyle cellStyle = sheet.getWorkbook().createCellStyle(); // 单元格样式  
	        Font fontStyle = sheet.getWorkbook().createFont(); // 字体样式  
	        fontStyle.setBoldweight(Font.BOLDWEIGHT_BOLD); // 加粗  
	        fontStyle.setFontName("宋体"); // 字体  
	        fontStyle.setFontHeightInPoints((short) 11); // 大小  
 	        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);  
	        cellStyle.setFont(fontStyle);  
	        cellStyle.setFillBackgroundColor(HSSFColor.GREY_40_PERCENT.index);
 			createCell(row, 0, row, 13, "监测数据成果汇总", sheet,cellStyle);
		}
		//表头
		{
			row++;
			
			CellStyle cellStyle  = createDefaultStyle(sheet.getWorkbook());
	        Font fontStyle = sheet.getWorkbook().createFont(); // 字体样式  
 	        fontStyle.setFontName("宋体"); // 字体  
	        fontStyle.setFontHeightInPoints((short) 8); // 大小  
			cellStyle.setWrapText(true);
			cellStyle.setAlignment(CellStyle.ALIGN_CENTER); //水平布局：居中
			if(monitorTypeParaCache.isShift(item.getGroupTypeCode()))
			{
				createCell(row, 0, row+1,0, "时间", sheet);
				createCell(row+2,0, row+3, 0, "深度", sheet);
 			}
			else
			{
				createCell(row, 0, row+1,0, "时间", sheet);
				createCell(row+2,0, row+3, 0, "测点", sheet);
  				createCell(row,1, row+3, 1, "初始\n值", sheet,cellStyle);
			}

			if(monitorTypeParaCache.isShift(item.getGroupTypeCode()))
			{
				Row r = sheet.getRow(row);
	 			r.setHeight((short)(220));
	 			r = sheet.getRow(row+1);
	 			r.setHeight((short)(220));
	 			r = sheet.getRow(row+2);
	 			r.setHeight((short)(220));
	 			r = sheet.getRow(row+3);
	 			r.setHeight((short)(220));
	 			 
		        Font f = sheet.getWorkbook().createFont(); // 字体样式  
	 	        f.setFontName("宋体"); // 字体  
		        f.setFontHeightInPoints((short)7); // 大小  
		        defualtStyle.setFont(f);
 			}
			
			for(int i=0;i<colSize;i++)
			{

				String countName = "";
				String day = "";
				String nowName = "";
				String sumName = "";
				String changeName = "";
				if(null != countList &&i<countList.size())
				{
					McProItemRealTimeMonData count = countList.get(i);
  					count = countList.get(i);
 					countName = "第"+count.getProjectCount()+"次";
 					day = DateUtil.DateToString(count.getCollectTime(),"yyyy-MM-dd");
 					nowName = "本次";
 					sumName = "累计";
 					changeName = "变化\n速率";
 	             }
				
				
				 createCell(row, startCol+i*3, row, startCol+2+i*3,countName , sheet);
				 createCell(row+1,startCol+i*3, row+1, startCol+2+i*3,day , sheet);
 				 createCell(row+2, startCol+i*3,row+3,startCol+i*3, nowName, sheet);
				 createCell(row+2, startCol+1+i*3,row+3,startCol+1+i*3, sumName, sheet);
				 createCell(row+2, startCol+2+i*3,row+3,startCol+2+i*3, changeName, sheet,cellStyle);
		
				
 			}
		 
		}
		log.info("now create monitor data");
		{
			row += 4;
			for(int j=0;j<rowSize;j++)
			{
				String pointCode = "";
				String initValue = "";
				if(null != pointList && j<pointList.size())
				{
					McProItemMeasuringPoint  point = pointList.get(j);
					pointCode = point.getMeasuringPointCode();
					
					if(!ValidatorUtil.isEmpty(countList))
					{
						McProItemRealTimeMonData count = countList.get(0);
	 					String key = count.getProjectCount() + pointCode;
	 					McProItemRealTimeMonData data = dataMap.get(key);
	 					if(null != data)
	 					{
	 						
	 						initValue = String.format("%.1f",  data.getInitValue());
	 					}
 					}
					else
					{
						initValue = String.format("%.1f", point.getInitValue()); 
 					}
					
 					
							
				}
				
	 			
	 			
				createCell(row, 0, row, 0, pointCode, sheet);
				createCell(row, 1, row, 1, initValue, sheet);
				if(monitorTypeParaCache.isShift(item.getGroupTypeCode()))
				{
					Row r = sheet.getRow(row);
		 			r.setHeight((short)(220));
	 			}
				for (int i = 0; i < colSize; i++) 
				{
					String minusValue = "";
					String sumValue = "";
					String changeRate = "";
					if (null != countList && i < countList.size() && !ValidatorUtil.isEmpty(pointCode)) 
					{
						McProItemRealTimeMonData count = countList.get(i);

						String key = count.getProjectCount() + pointCode;
						minusValue = getValue(key, "minusValue", dataMap);
						sumValue = getValue(key, "sumValue", dataMap);
						changeRate = getValue(key, "changeRate", dataMap);

					}
					createCell(row, startCol + i * 3, minusValue, sheet);
					createCell(row, startCol+1 + i * 3, sumValue, sheet);
					createCell(row, startCol+2 + i * 3, changeRate, sheet);
				}
 				row++;
			}
  		}
		log.info("now create max min data");
		{
			for(int i=0;i<colSize;i++)
			{
				String minValue = "";
				String maxValue = "";
				String changeRate = "";
				String day = "";
				if(null != countList &&i<countList.size())
				{
					McProItemRealTimeMonData count = countList.get(i);
  					String keyMin = count.getProjectCount() + "min";
	 				String keyMax = count.getProjectCount() + "max";
					String keyRate = count.getProjectCount() + "maxRate";
	
					minValue = getValue(keyMin,"sumValue",dataMap);
					maxValue = getValue(keyMax,"sumValue",dataMap);
					changeRate = getValue(keyRate,"changeRate",dataMap);
					
					day = DateUtil.DateToString(count.getCollectTime(),"yyyy-MM-dd");
  				}
				McProItemImplementLog log = implLogMap.get(day);
				String serial = "";
				if(null != log)
				{
					serial = log.getSerial();
				}
				createCell(row, startCol+i*3,row,startCol+2+i*3,serial,sheet);
				createCell(row+1, startCol+i*3,row+1,startCol+2+i*3,minValue,sheet);
				createCell(row+2, startCol+i*3,row+2,startCol+2+i*3,maxValue,sheet);
				createCell(row+3, startCol+i*3,row+3,startCol+2+i*3,changeRate,sheet);
  			}
			
			createCell(row, 0,row,startCol-1, "施工进度编号", sheet);
			row++;
			createCell(row, 0,row,startCol-1, monitorTypeParaCache.getMinNameUnit(item.getGroupTypeCode()), sheet);
			row++;
			createCell(row, 0,row,startCol-1, monitorTypeParaCache.getMaxNameUnit(item.getGroupTypeCode()), sheet);
			row++;
			createCell(row, 0,row,startCol-1, monitorTypeParaCache.getMaxRateUnit(item.getGroupTypeCode()), sheet);
		
			if(monitorTypeParaCache.isShift(item.getGroupTypeCode()))
			{
				Row r = sheet.getRow(row);
	 			r.setHeight((short)(220));
	 			r = sheet.getRow(row-1);
	 			r.setHeight((short)(220));
	 			r = sheet.getRow(row-2);
	 			r.setHeight((short)(220));
	 			r = sheet.getRow(row-3);
	 			r.setHeight((short)(220));
 			}
		}
		{
			row++;
			CellStyle cellStyle  = createDefaultStyle(sheet.getWorkbook());
			Font f = sheet.getWorkbook().createFont(); // 字体样式  
 	        f.setFontName("宋体"); // 字体  
	        f.setFontHeightInPoints((short)6); // 大小  
			cellStyle.setFont(f);
			cellStyle.setWrapText(true);
			String note = "备注:1 ）表中次数为进场次数；2 ）\"∕\"表示未布点，\"\\\"表示未观测：3）“*”表示观测受阻，“#”表示被毁重布或更换仪器；4）“--”表示空缺值，“— —”表示回填；5 ）";
			note += item.getRemarks();
			createCell(row, 0,row,13, note, sheet,cellStyle);
			Row temp = sheet.getRow(row);
			temp.setHeight((short) (720));
		}
		
 		log.info("now create image");
		if(monitorTypeParaCache.isShift(item.getGroupTypeCode()))
        {
			String path = getOutImage();
			XYSeriesCollection  dataset = new XYSeriesCollection() ; 
			if(null != countList)
			{
				for(McProItemRealTimeMonData e: countList)
				{
					XYSeries series = new XYSeries(DateUtil.DateToString(e.getCollectTime(), "yyyy-MM-dd"));
					for(String key : dataMap.keySet())
					{
						McProItemRealTimeMonData data = dataMap.get(key);
						if(null != data && data.getProjectCount().equals(e.getProjectCount()))
						{
							if(!ValidatorUtil.isEmpty(data.getDepthExt()))
							{
								Double v = 0.0;
								try
								{
									v = Double.valueOf(data.getDepthExt());
								}catch(Exception ex)
								{
									log.warn("change failed."+data.getDepthExt());
								}
								series.add(v, data.getSumValue());
							}
	 					}
					}
					dataset.addSeries(series);
				}
				log.info("create chart");
				ChartUtil.createYChart(path,"深度(m)","累计变化量("+monitorTypeParaCache.get(item.getGroupTypeCode()).getUnit() +")",dataset);
				log.info("insert image");
				createImage(startRow+1, 10, startRow+58, 13, path, sheet);
			}
			
		}
	    else
 		{
			{
				row++;
		        CellStyle cellStyle = sheet.getWorkbook().createCellStyle(); // 单元格样式  
		        Font fontStyle = sheet.getWorkbook().createFont(); // 字体样式  
		        fontStyle.setBoldweight(Font.BOLDWEIGHT_BOLD); // 加粗  
		        fontStyle.setFontName("黑体"); // 字体  
		        fontStyle.setFontHeightInPoints((short) 11); // 大小  
	 	        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);  
		        cellStyle.setFont(fontStyle);  
		        cellStyle.setFillBackgroundColor(HSSFColor.GREY_40_PERCENT.index);
	 			createCell(row, 0, row, 13, "监测成果曲线图", sheet,cellStyle);
			}
			
			row++;
			String path = getOutImage();  
 
			
			TimeSeriesCollection  dataset = new TimeSeriesCollection (); 

			if(null != pointList)
			{
				for(McProItemMeasuringPoint point: pointList)
				{
					TimeSeries series = new TimeSeries(point.getMeasuringPointCode());
					for(String key : dataMap.keySet())
					{
						 McProItemRealTimeMonData data = dataMap.get(key);
						 if(null != data && data.getMeasuringPointCode().equals(point.getMeasuringPointCode()))
						 {
							 try
							 {
								 Calendar c = Calendar.getInstance();
			  						c.setTime(data.getCollectTime());
			 						Second second =   new Second(c.get(Calendar.SECOND),c.get(Calendar.MINUTE),c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.DAY_OF_MONTH),c.get(Calendar.MONTH)+1,c.get(Calendar.YEAR));
			 						series.addOrUpdate(second , data.getSumValue());
							 }
							 catch(Exception e)
							 {
								 log.error(""+JsonConvert.ObjectToJson(data),e);
							 }
	   						
		  			     }
		 			}
					dataset.addSeries(series );
					
				}
				log.info("create chart");
				ChartUtil.createChart(path,"累计变化量("+monitorTypeParaCache.get(item.getGroupTypeCode()).getUnit() +")",dataset);
				log.info("insert image");
				createImage(row, 0, row+11, 13, path, sheet);
			}
			
			log.info("one section done");
			 
			
		}
	}
	

	
}
