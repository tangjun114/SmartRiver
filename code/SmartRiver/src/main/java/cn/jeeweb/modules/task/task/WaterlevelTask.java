package cn.jeeweb.modules.task.task;

import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.SpringContextHolder;
import cn.jeeweb.modules.flood.client.GetFloodInfoClient;
import cn.jeeweb.modules.flood.client.floodinfoapi.FloodRainFullClient;
import cn.jeeweb.modules.flood.client.floodinfoapi.FloodWaterLevelClient;
import cn.jeeweb.modules.flood.entity.FloodRainfull;
import cn.jeeweb.modules.flood.entity.FloodSite;
import cn.jeeweb.modules.flood.service.IFloodRainfullService;
import cn.jeeweb.modules.flood.service.IFloodSiteService;
import com.ff.common.util.format.DateUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author:Wish
 * @Date: Created in 2018/12/16 15:42
 **/
@Component
public class WaterlevelTask {
    public final Logger log = Logger.getLogger(this.getClass());


    public void run() {
        log.info(" run start......................................" + (new Date()));
        Date yesDay = DateUtil.dayCalculate(DateUtil.getCurrentDate(), -3);
        IFloodSiteService floodSiteService = SpringContextHolder.getBean("floodSiteService");
        List<FloodSite> floodSites = floodSiteService.listWithNoPage(new QueryRequest());
        for (FloodSite temp : floodSites) {
            getWaterLevel(temp, DateUtil.DateToString(yesDay, "yyyy-MM-dd"), DateUtil.DateToString(new Date(), "yyyy-MM-dd"));
        }
        log.error("run finish");

    }

    public void getWaterLevel(FloodSite floodSite, String starTime, String endTime) {
        try {
            IFloodRainfullService floodRainfullService = SpringContextHolder.getBean("floodRainfullService");
            log.info("waterlevel-----------------site:" + floodSite.getSiteName() + "begin-------------------");
            GetFloodInfoClient client = new FloodWaterLevelClient()
                    .setReqParam(floodSite.getScid(), starTime, endTime, "00", "TM");
            Map<String, Object> responseMap = client.getFloodInfo();
            floodRainfullService.delete(new EntityWrapper<FloodRainfull>().between("record_time", starTime, endTime)
                    .eq("record_type", 1)
                    .eq("site_id", floodSite.getId()));
            if (null != responseMap) {
                Map<String, Object> response = ((List<Map<String, Object>>) responseMap.get("responses")).get(0);
                if (null != response) {
                    List<Map<String, Object>> data = (List<Map<String, Object>>) response.get("data");
                    if (null != data) {
                        for (int i = 0; i < data.size(); i++) {
                            Map<String, Object> temp = data.get(i);
                            FloodRainfull entity = new FloodRainfull();
                            BeanUtils.copyProperties(floodSite, entity);
                            entity.setSiteId(floodSite.getId());
                            entity.setId(null);
                            entity.setRecordTime(DateUtil.stringToDate((String) temp.get("TM")));
                            entity.setWaterLevel(((BigDecimal) temp.get("Z")).doubleValue());
                            entity.setRecordType(1);
                            floodRainfullService.insert(entity);
                            if (i == data.size() - 1) {
                                floodSite.setWaterLevel(((BigDecimal) temp.get("Z")).doubleValue());
                                IFloodSiteService floodSiteService = SpringContextHolder.getBean("floodSiteService");
                                floodSiteService.updateById(floodSite);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("获取信息失败:" + e.getMessage());
        }
    }

    public static void main(String[] args) {
        Date yesDay = DateUtil.dayCalculate(DateUtil.getCurrentDate(), -1);
        System.out.println(DateUtil.DateToString(yesDay, "yyy-MM-dd"));
        System.out.println(DateUtil.DateToString(new Date(), "yyy-MM-dd"));
        GetFloodInfoClient client = new FloodWaterLevelClient()
                .setReqParam("81202701", "2019-01-08 22:00:00", "2019-01-12", "00", "TM");
        Map<String, Object> responseMap = client.getFloodInfo();
        System.out.println(client.getFloodInfo());
        client = new FloodRainFullClient()
                .setReqParam("81202701", "2019-01-08 22:00:00", "2019-01-12");
        System.out.println(client.getFloodInfo());
    }


}
