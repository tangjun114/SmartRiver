package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemExchange;
 
/**   
 * @Title: 项目沟通交流数据库控制层接口
 * @Description: 项目沟通交流数据库控制层接口
 * @author Aether
 * @date 2018-04-28 13:26:54
 * @version V1.0   
 *
 */
public interface McProItemExchangeMapper extends BaseMapper<McProItemExchange> {
    
}