package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProDeviceAbnormal;

/**   
 * @Title: 设备异常状况
 * @Description: 设备异常状况
 * @author Aether
 * @date 2018-04-19 21:46:24
 * @version V1.0   
 *
 */
public interface IMcProDeviceAbnormalService extends ICommonService<McProDeviceAbnormal> {

}

