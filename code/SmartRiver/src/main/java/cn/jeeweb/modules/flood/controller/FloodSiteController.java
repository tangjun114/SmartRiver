package cn.jeeweb.modules.flood.controller;


import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.data.Pageable;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.flood.entity.FloodRainfull;
import cn.jeeweb.modules.flood.service.IFloodRainfullService;
import com.ff.common.util.format.DateUtil;
import com.ff.common.util.validate.ValidatorUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.FloodSite;

import java.util.*;

/**
 * @author wsh
 * @version V1.0
 * @Title: 监控站点
 * @Description: 监控站点
 * @date 2018-11-27 18:04:01
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/floodsite")
@RequiresPathPermission("flood:floodsite")
public class FloodSiteController extends BaseCRUDController<FloodSite, String> {
    @Autowired
    IFloodRainfullService floodRainfullService;

    @Override
    public BaseRspJson<List<FloodSite>> LoadAll(@RequestBody BaseReqJson request) {
        BaseRspJson<List<FloodSite>> rsp = new BaseRspJson<>();
        Queryable queryable = this.GetFilterCondition(request);
        List<FloodSite> obj = this.commonService.listWithNoPage(queryable);

        int type = obj.get(0).getType();
        if (type == 0 && null != obj && obj.size() > 0) {

            List<String> ids = new ArrayList<>();
            for (FloodSite temp : obj) {
                ids.add(temp.getId());
            }
            EntityWrapper<FloodRainfull> entityWrapper1 = new EntityWrapper<FloodRainfull>();
            entityWrapper1.in("site_id", ids);
            entityWrapper1.gt("record_time", DateUtil.dayCalculate(new Date(), -1));
            entityWrapper1.lt("record_time", new Date());
            entityWrapper1.eq("record_type", type);
            entityWrapper1.setSqlSelect("sum(rainfull) as rainfull,sum(water_level)as waterLevel,site_id as siteId");
            entityWrapper1.groupBy("site_id");
            List<FloodRainfull> dataList = floodRainfullService.selectList(entityWrapper1);
            if (!ValidatorUtil.isEmpty(dataList)) {
                Map<String, FloodRainfull> map = new HashMap<>();
                for (FloodRainfull temp : dataList) {
                    map.put(temp.getSiteId(), temp);
                }
                for (FloodSite temp : obj) {
                    FloodRainfull rainfull = map.get(temp.getId());
                    if (null != rainfull) {
                        temp.setRainfall(rainfull.getRainfull());
                        temp.setWaterLevel(rainfull.getWaterLevel());
                    }
                }
            }
        }

        rsp.setObj(obj);
        return rsp;
    }
}
