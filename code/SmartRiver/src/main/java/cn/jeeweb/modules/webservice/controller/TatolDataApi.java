package cn.jeeweb.modules.webservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.common.web.controller.FFTableController;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.modules.supervise.entity.McProItemRealTimeTotalData;
import cn.jeeweb.modules.task.task.StatisticTask;


@Controller
@RequestMapping("${api.url.prefix}/total")
public class TatolDataApi extends FFTableController<McProItemRealTimeTotalData>
{
	@RequestMapping("/run")
	@ResponseBody
	public BaseRspJson<List<McProItemRealTimeTotalData>> run(@RequestBody BaseReqJson<McProItemRealTimeTotalData> request)
	{
	    BaseRspJson<List<McProItemRealTimeTotalData>> rsp = new BaseRspJson<List<McProItemRealTimeTotalData>>();
	    McProItemRealTimeTotalData obj = this.getObj(request, McProItemRealTimeTotalData.class);
	    StatisticTask task = new StatisticTask();
		List<McProItemRealTimeTotalData> objList =  task.sum(obj.getProjectId(),obj.getDay());
		rsp.setObj(objList);
  		return rsp;
 	}
 
}
