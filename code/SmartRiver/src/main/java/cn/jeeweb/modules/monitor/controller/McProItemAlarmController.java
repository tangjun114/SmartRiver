package cn.jeeweb.modules.monitor.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.util.validate.ValidatorUtil;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McProItemAlarm;
import cn.jeeweb.modules.monitor.service.IMcProItemAlarmService;
import cn.jeeweb.modules.supervise.controller.McProjectBaseController;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 告警信息
 * @Description: 告警信息
 * @author Aether
 * @date 2017-12-02 12:58:21
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/monitor/mcproitemalarm")
@RequiresPathPermission("monitor:mcproitemalarm")
public class McProItemAlarmController extends McProjectBaseController<McProItemAlarm, String> {

	@Autowired
	private IMcProItemAlarmService alarmService;
	@Autowired
	private IMcProjectService iMcProjectService;
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProItemAlarm> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
		String  newProjectCount = request.getParameter("newProjectCount");
		String  alarmTypeId = request.getParameter("alarmTypeId");
		if(!StringUtils.isEmpty(newProjectCount)){
			entityWrapper.eq("newProjectCount", newProjectCount);
 		}
		if(!StringUtils.isEmpty(alarmTypeId)){
			entityWrapper.eq("alarmTypeId", alarmTypeId);
 		}
		entityWrapper.in("projectId",  iMcProjectService.getProjectIdListByUser(UserUtils.getUser()));
		entityWrapper.orderBy("projectId");
		entityWrapper.orderBy("monitorItemId");
		super.preAjaxList(queryable, entityWrapper, request, response);
	}


	@RequestMapping(value = "/latest")
	public String latest(Model model, HttpServletRequest request, HttpServletResponse response) {
 
		super.preList(model, request, response);
		
		return display("latest");
	}
	



	@Override
	public void preSave(McProItemAlarm entity, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preSave(entity, request, response);
		
		entity.setHandler(UserUtils.getUser().getRealname());
		entity.setHandleStatus("alarm_handled");
	}


	@Override
	public void afterSave(McProItemAlarm entity, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		//super.afterSave(entity, request, response);
		String clearVal = request.getParameter("clear");
		String  newProjectCount = request.getParameter("newProjectCount");
		if ("monitorItem".equals(clearVal)) {
			//清除该监测项下所有告警记录
					
			EntityWrapper<McProItemAlarm> wrapper = new EntityWrapper<McProItemAlarm>();
			wrapper.eq("project_id", entity.getProjectId());
			wrapper.eq("monitor_item_id", entity.getMonitorItemId());
			if(!StringUtils.isEmpty(newProjectCount)){
				//entityWrapper.eq("newProjectCount", newProjectCount);
				wrapper.eq("new_project_count", newProjectCount);
	 		}
						
			List<McProItemAlarm> alarmList = alarmService.selectList(wrapper);
			if (null != alarmList && alarmList.size()>0) {
				for (McProItemAlarm alrm : alarmList) {
					alrm.setHandleImg(entity.getHandleImg());
					alrm.setHandler(entity.getHandler());
					alrm.setHandleResult(entity.getHandleResult());
					alrm.setHandleStatus(entity.getHandleStatus());
				}
				alarmService.updateBatchById(alarmList);
			}
			

			
		}
	}

	
	
}
