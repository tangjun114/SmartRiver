package cn.jeeweb.modules.supervise.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.ff.common.constant.FFErrorCode;
import com.ff.common.util.format.JsonConvert;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McMonitorOrg;
import cn.jeeweb.modules.monitor.service.IMcMonitorOrgService;
import cn.jeeweb.modules.sm.entity.McSmMonitorGroupType;
import cn.jeeweb.modules.supervise.entity.McProItemMember;
import cn.jeeweb.modules.supervise.entity.McProReport;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.model.report.BaseForm;
import cn.jeeweb.modules.supervise.model.report.GDSimpleForm;
import cn.jeeweb.modules.supervise.service.IMcProItemMemberService;
import cn.jeeweb.modules.supervise.service.IMcProReportService;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.entity.Dict;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.service.IDictService;
import cn.jeeweb.modules.sys.utils.DictUtils;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 报告列表
 * @Description: 报告列表
 * @author Aether
 * @date 2018-03-10 22:42:24
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproreport")
@RequiresPathPermission("supervise:mcproreport")
public class McProReportController extends McProjectBaseController<McProReport, String> {

	@Autowired
	private IMcProItemMemberService memberService;
	
	@Autowired
	private IMcProReportService reportService;
	
	@Override
	protected McProReport newModel() {
		// TODO Auto-generated method stub
		McProReport report = new McProReport();
		report.setExtendForm(new GDSimpleForm());
		return report;
	}
	
	@Autowired
	IMcProjectService iMcProjectService;

	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProReport> entityWrapper, HttpServletRequest request,
			HttpServletResponse response) {
		String  projectId = request.getParameter("projectId");
		// TODO Auto-generated method stub
		if(StringUtils.isEmpty(projectId)){
			User user = UserUtils.getUser();
			entityWrapper.in("projectId", iMcProjectService.getProjectIdListByUser(user));

			entityWrapper.eq("remarks", "报告生成完毕");
			
		}
		super.preAjaxList(queryable, entityWrapper, request, response);
 	}



	@Override
	public void preSave(McProReport entity, HttpServletRequest request, HttpServletResponse response) {
		super.preSave(entity, request, response);
		BaseForm f = entity.getExtendForm();
		if (f instanceof GDSimpleForm) {
			GDSimpleForm form = (GDSimpleForm) f;
			entity.setReportExtend(JsonConvert.ObjectToJson(form));
		}
		
		
	}

	@Override
	public void preEdit(McProReport entity, Model model, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preEdit(entity, model, request, response);
		entity.setExtendForm(JsonConvert.jsonToObject(entity.getReportExtend(), GDSimpleForm.class));
	}



	@RequestMapping(value = "/all")
	public String showAll(Model model, HttpServletRequest request, HttpServletResponse response) {

		return display("listAll");
	}
	
	@RequestMapping(value = "/check", method = RequestMethod.POST)
	@ResponseBody
	public BaseRspJson<?> checkReport(@RequestBody BaseReqJson request) {
		BaseRspJson<?> rsp = new BaseRspJson<>();
		String reportId = this.getObj(request,String.class);
		McProReport report = this.get(reportId);
		
		String userId = UserUtils.getUser().getId();
		Queryable queryable = QueryRequest.newQueryable();
		queryable.addCondition("userId", userId);
		queryable.addCondition("projectId", report.getProjectId());
		McProItemMember member = memberService.get(queryable);
		
		if(member != null && report !=null) {
			if ("to_check".equals(report.getStatus())){
				if(member.getReportPerm().contains("valid")){
					report.setStatus("to_approve");
					reportService.updateById(report);
				} else {
					rsp.setErrorCode(FFErrorCode.FAIL);
					rsp.setMessage("您没有报告校核权限");
				}
			} else if ("to_approve".equals(report.getStatus())) {
				if(member.getReportPerm().contains("approval")){
					report.setStatus("approved");
					reportService.updateById(report);
				} else {
					rsp.setErrorCode(FFErrorCode.FAIL);
					rsp.setMessage("您没有报告审核权限");
				}
			}

			
		} else {
			rsp.setErrorCode(FFErrorCode.FAIL);
			rsp.setMessage("数据异常");
		}

		return rsp;
	}
	
	@Autowired
	IMcProjectService projectServce;
	
	@Autowired
	IMcMonitorOrgService orgServce;
	@Autowired
	IDictService dictService;
	@RequestMapping("/loadtemplate")
	@ResponseBody
	public BaseRspJson<List<Dict>> loadByProject(@RequestBody BaseReqJson request) {

	 
		BaseRspJson<List<Dict>> rsp = new BaseRspJson<List<Dict>>();
		Queryable queryable = this.GetFilterCondition(request);
		String projectId = (String) queryable.getValue("projectId");
		McProject project = projectServce.selectById(projectId);
		McMonitorOrg org = orgServce.selectById(project.getMonitorId());
		Wrapper<Dict> wrapper = new EntityWrapper<Dict>();
		wrapper.in("value", org.getTemplatePerm());
		wrapper.eq("gid", "ffee65c899f747188d2a954f29c250e3");
		List<Dict> obj = this.dictService.selectList(wrapper);
		rsp.setObj(obj);
		
		return rsp;
	}

}
