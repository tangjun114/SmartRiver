package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProRelateFileMapper;
import cn.jeeweb.modules.supervise.entity.McProRelateFile;
import cn.jeeweb.modules.supervise.service.IMcProRelateFileService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目相关文件
 * @Description: 项目相关文件
 * @author aether
 * @date 2018-01-24 23:54:08
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProRelateFileService")
public class McProRelateFileServiceImpl  extends CommonServiceImpl<McProRelateFileMapper,McProRelateFile> implements  IMcProRelateFileService {

}
