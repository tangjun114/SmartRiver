package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorCollection;
 
/**   
 * @Title: 通信设备数据库控制层接口
 * @Description: 通信设备数据库控制层接口
 * @author shawloong
 * @date 2018-01-28 21:44:38
 * @version V1.0   
 *
 */
public interface McMonitorCollectionMapper extends BaseMapper<McMonitorCollection> {
    
}