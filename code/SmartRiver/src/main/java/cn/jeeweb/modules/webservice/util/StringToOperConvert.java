package cn.jeeweb.modules.webservice.util;

 

import org.springframework.core.convert.converter.Converter;
 

import cn.jeeweb.core.query.data.Condition.Operator;

public class StringToOperConvert implements Converter<String, Operator>{  
	  
    /* (non-Javadoc) 
     * @see com.fasterxml.jackson.databind.util.Converter#convert(java.lang.Object) 
     * @author: ShangJianguo 
     * 2014-6-12 下午4:56:30 
     */  
    @Override  
    public Operator convert(String source) {  
        String value = source.trim();  
        if ("".equals(value)) {  
            return null;  
        }  
        return Operator.fromString(source);  
  
    }
 
}  