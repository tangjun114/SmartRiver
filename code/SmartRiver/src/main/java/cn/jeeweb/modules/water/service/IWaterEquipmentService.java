package cn.jeeweb.modules.water.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.water.entity.WaterEquipment;

/**   
 * @Title: 监测站设备
 * @Description: 监测站设备
 * @author liyonglei
 * @date 2018-11-13 21:09:06
 * @version V1.0   
 *
 */
public interface IWaterEquipmentService extends ICommonService<WaterEquipment> {

}

