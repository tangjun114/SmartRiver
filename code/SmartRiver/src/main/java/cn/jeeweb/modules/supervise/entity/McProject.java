package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 项目管理
 * @Description: 项目管理
 * @author shawloong
 * @date 2018-04-11 20:13:30
 * @version V1.0   
 *
 */
@TableName("mc_project")
@SuppressWarnings("serial")
public class McProject extends AbstractEntity<String> {

    /**id*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**项目编号*/
    @TableField(value = "code")
	private String code;
    /**项目名称*/
    @TableField(value = "name")
	private String name;
    /**项目类别*/
    @TableField(value = "projectType")
	private String projectType;
    /**项目详细*/
    @TableField(value = "full_address")
	private String fullAddress;
    /**地图经度*/
    @TableField(value = "longitude")
	private String longitude;
    /**地图纬度*/
    @TableField(value = "latitude")
	private String latitude;
    /**项目负责id*/
    @TableField(value = "person_in_charge_id")
	private String personInCharge;
    /**项目负责人*/
    @TableField(value = "person_in_charge_name")
	private String personInChargeName;
    /**监测单位*/
    @TableField(value = "monitor_org_name")
	private String monitorOrgName;
    /**监测单位id*/
    @TableField(value = "monitor_org_id")
	private String monitorOrgId;
    /**监督人员id*/
    @TableField(value = "monitor_by")
	private String monitorBy;
    /**监督人员*/
    @TableField(value = "monitor_by_name")
	private String monitorByName;
    /**项目概况*/
    @TableField(value = "desc")
	private String desc;
    /**监测频率简述*/
    @TableField(value = "frequency_desc")
	private String frequencyDesc;
    /**合同类型*/
    @TableField(value = "contract_type")
	private String contractDype;
    /**数据更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**合同额(￥)*/
    @TableField(value = "contract_limit")
	private String contractLimit;
    /**进展情况*/
    @TableField(value = "progress")
	private String progress;
    /**安全状态*/
    @TableField(value = "safe_status")
	private String safeStatus;
    /**工作状态*/
    @TableField(value = "work_status")
	private String workStatus;
    /**创建人*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新人*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**删除标识（0正常，1删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**场次*/
    @TableField(value = "project_count")
	private Integer projectCount;
    /**备注*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目简称*/
    @TableField(value = "simple_name")
	private String simpleName;
    /**详细地址*/
    @TableField(value = "detail_addr")
	private String detailAddr;
    /**项目概况*/
    @TableField(value = "note")
	private String note;
    /**支护形式*/
    @TableField(value = "protect_type")
	private String protectType;
    /**开工日期*/
    @TableField(value = "start_date")
	private Date startDate;
    /**开挖面积(㎡)*/
    @TableField(value = "area")
	private String area;
    /**开挖深度(m)*/
    @TableField(value = "depth")
	private String depth;
    /**安全等级*/
    @TableField(value = "level")
	private String level;
    /**监测负责人Id*/
    @TableField(value = "measure_user_id")
	private String measureUserId;
    /**监测负责人*/
    @TableField(value = "measure_user_name")
	private String measureUserName;
    /**技术负责人*/
    @TableField(value = "tech_user_id")
	private String techUserId;
    /**技术负责人*/
    @TableField(value = "tech_user_name")
	private String techUserName;
    /**建设单位负责人*/
    @TableField(value = "build_unit_user_id")
	private String buildUnitUserId;
    /**建设单位负责人*/
    @TableField(value = "build_unit_user_name")
	private String buildUnitUserName;
    /**设计单位负责人*/
    @TableField(value = "design_unit_user_id")
	private String designUnitUserId;
    /**设计单位负责人*/
    @TableField(value = "design_unit_user_name")
	private String designUnitUserName;
    /**委托单位负责人*/
    @TableField(value = "entrust_unit_user_id")
	private String entrustUnitUserId;
    /**委托单位负责人*/
    @TableField(value = "entrust_unit_user_name")
	private String entrustUnitUserName;
    /**监理单位负责人*/
    @TableField(value = "supervising_unit_user_id")
	private String supervisingUnitUserId;
    /**监理单位负责人*/
    @TableField(value = "supervising_unit_user_name")
	private String supervisingUnitUserName;
    /**总包单位负 责人  -*/
    @TableField(value = "main_unit_user_id")
	private String mainUnitUserId;
    /**总包单位负 责人  -*/
    @TableField(value = "main_unit_user_name")
	private String mainUnitUserName;
    /**支护施工单位负责人*/
    @TableField(value = "implement_unit_user_id")
	private String implementUnitUserId;
    /**支护施工单位负责人*/
    @TableField(value = "implement_unit_user_name")
	private String implementUnitUserName;
    /**建设单位*/
    @TableField(value = "build_unit")
	private String buildUnit;
    /**设计单位*/
    @TableField(value = "design_unit")
	private String designUnit;
    /**委托单位*/
    @TableField(value = "entrust_unit")
	private String entrustUnit;
    /**监理单位*/
    @TableField(value = "supervising_unit")
	private String supervisingUnit;
    /**总包单位*/
    @TableField(value = "main_unit")
	private String mainUnit;
    /**支护施工单位*/
    @TableField(value = "implement_unit")
	private String implementUnit;
    /**委托单位地址*/
    @TableField(value = "entrust_addr")
	private String entrustAddr;
    /**测点保护措施*/
    @TableField(value = "point_protect")
	private String pointProtect;
    /**报警指标*/
    @TableField(value = "alarm_index")
	private String alarmIndex;
    /**组织ID*/
    @TableField(value = "monitor_id")
	private String monitorId;
    /**组织名称*/
    @TableField(value = "monitor_name")
	private String monitorName;
    /**预警测点数统计*/
    @TableField(value = "warn_point_count")
	private Integer warnPointCount = 0;
    /**告警测点数统计*/
    @TableField(value = "alarm_point_count")
	private Integer alarmPointCount = 0;
    /**超控测点数统计*/
    @TableField(value = "control_point_count")
	private Integer controlPointCount = 0;
    /**施工最新进度*/
    @TableField(value = "implement_latest_log")
	private String implementLatestLog;
    /**省*/
    @TableField(value = "province")
	private String province;
    /**市*/
    @TableField(value = "city")
	private String city;
    /**区*/
    @TableField(value = "district")
	private String district;
	
	/**
	 * 获取  id
	 *@return: String  id
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  code
	 *@return: String  项目编号
	 */
	public String getCode(){
		return this.code;
	}

	/**
	 * 设置  code
	 *@param: code  项目编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 * 获取  name
	 *@return: String  项目名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  项目名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  projectType
	 *@return: String  项目类别
	 */
	public String getProjectType(){
		return this.projectType;
	}

	/**
	 * 设置  projectType
	 *@param: projectType  项目类别
	 */
	public void setProjectType(String projectType){
		this.projectType = projectType;
	}
	/**
	 * 获取  fullAddress
	 *@return: String  项目详细
	 */
	public String getFullAddress(){
		return this.fullAddress;
	}

	/**
	 * 设置  fullAddress
	 *@param: fullAddress  项目详细
	 */
	public void setFullAddress(String fullAddress){
		this.fullAddress = fullAddress;
	}
	/**
	 * 获取  longitude
	 *@return: String  地图经度
	 */
	public String getLongitude(){
		return this.longitude;
	}

	/**
	 * 设置  longitude
	 *@param: longitude  地图经度
	 */
	public void setLongitude(String longitude){
		this.longitude = longitude;
	}
	/**
	 * 获取  latitude
	 *@return: String  地图纬度
	 */
	public String getLatitude(){
		return this.latitude;
	}

	/**
	 * 设置  latitude
	 *@param: latitude  地图纬度
	 */
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
	/**
	 * 获取  personInCharge
	 *@return: String  项目负责id
	 */
	public String getPersonInCharge(){
		return this.personInCharge;
	}

	/**
	 * 设置  personInCharge
	 *@param: personInCharge  项目负责id
	 */
	public void setPersonInCharge(String personInCharge){
		this.personInCharge = personInCharge;
	}
	/**
	 * 获取  personInChargeName
	 *@return: String  项目负责人
	 */
	public String getPersonInChargeName(){
		return this.personInChargeName;
	}

	/**
	 * 设置  personInChargeName
	 *@param: personInChargeName  项目负责人
	 */
	public void setPersonInChargeName(String personInChargeName){
		this.personInChargeName = personInChargeName;
	}
	/**
	 * 获取  monitorOrgName
	 *@return: String  监测单位
	 */
	public String getMonitorOrgName(){
		return this.monitorOrgName;
	}

	/**
	 * 设置  monitorOrgName
	 *@param: monitorOrgName  监测单位
	 */
	public void setMonitorOrgName(String monitorOrgName){
		this.monitorOrgName = monitorOrgName;
	}
	/**
	 * 获取  monitorOrgId
	 *@return: String  监测单位id
	 */
	public String getMonitorOrgId(){
		return this.monitorOrgId;
	}

	/**
	 * 设置  monitorOrgId
	 *@param: monitorOrgId  监测单位id
	 */
	public void setMonitorOrgId(String monitorOrgId){
		this.monitorOrgId = monitorOrgId;
	}
	/**
	 * 获取  monitorBy
	 *@return: String  监督人员id
	 */
	public String getMonitorBy(){
		return this.monitorBy;
	}

	/**
	 * 设置  monitorBy
	 *@param: monitorBy  监督人员id
	 */
	public void setMonitorBy(String monitorBy){
		this.monitorBy = monitorBy;
	}
	/**
	 * 获取  monitorByName
	 *@return: String  监督人员
	 */
	public String getMonitorByName(){
		return this.monitorByName;
	}

	/**
	 * 设置  monitorByName
	 *@param: monitorByName  监督人员
	 */
	public void setMonitorByName(String monitorByName){
		this.monitorByName = monitorByName;
	}
	/**
	 * 获取  desc
	 *@return: String  项目概况
	 */
	public String getDesc(){
		return this.desc;
	}

	/**
	 * 设置  desc
	 *@param: desc  项目概况
	 */
	public void setDesc(String desc){
		this.desc = desc;
	}
	/**
	 * 获取  frequencyDesc
	 *@return: String  监测频率简述
	 */
	public String getFrequencyDesc(){
		return this.frequencyDesc;
	}

	/**
	 * 设置  frequencyDesc
	 *@param: frequencyDesc  监测频率简述
	 */
	public void setFrequencyDesc(String frequencyDesc){
		this.frequencyDesc = frequencyDesc;
	}
	/**
	 * 获取  contractDype
	 *@return: String  合同类型
	 */
	public String getContractDype(){
		return this.contractDype;
	}

	/**
	 * 设置  contractDype
	 *@param: contractDype  合同类型
	 */
	public void setContractDype(String contractDype){
		this.contractDype = contractDype;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  数据更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  数据更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  contractLimit
	 *@return: String  合同额(￥)
	 */
	public String getContractLimit(){
		return this.contractLimit;
	}

	/**
	 * 设置  contractLimit
	 *@param: contractLimit  合同额(￥)
	 */
	public void setContractLimit(String contractLimit){
		this.contractLimit = contractLimit;
	}
	/**
	 * 获取  progress
	 *@return: String  进展情况
	 */
	public String getProgress(){
		return this.progress;
	}

	/**
	 * 设置  progress
	 *@param: progress  进展情况
	 */
	public void setProgress(String progress){
		this.progress = progress;
	}
	/**
	 * 获取  safeStatus
	 *@return: String  安全状态
	 */
	public String getSafeStatus(){
		return this.safeStatus;
	}

	/**
	 * 设置  safeStatus
	 *@param: safeStatus  安全状态
	 */
	public void setSafeStatus(String safeStatus){
		this.safeStatus = safeStatus;
	}
	/**
	 * 获取  workStatus
	 *@return: String  工作状态
	 */
	public String getWorkStatus(){
		return this.workStatus;
	}

	/**
	 * 设置  workStatus
	 *@param: workStatus  工作状态
	 */
	public void setWorkStatus(String workStatus){
		this.workStatus = workStatus;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建人
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建人
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新人
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新人
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标识（0正常，1删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标识（0正常，1删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  projectCount
	 *@return: Integer  场次
	 */
	public Integer getProjectCount(){
		return this.projectCount;
	}

	/**
	 * 设置  projectCount
	 *@param: projectCount  场次
	 */
	public void setProjectCount(Integer projectCount){
		this.projectCount = projectCount;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  simpleName
	 *@return: String  项目简称
	 */
	public String getSimpleName(){
		return this.simpleName;
	}

	/**
	 * 设置  simpleName
	 *@param: simpleName  项目简称
	 */
	public void setSimpleName(String simpleName){
		this.simpleName = simpleName;
	}
	/**
	 * 获取  detailAddr
	 *@return: String  详细地址
	 */
	public String getDetailAddr(){
		return this.detailAddr;
	}

	/**
	 * 设置  detailAddr
	 *@param: detailAddr  详细地址
	 */
	public void setDetailAddr(String detailAddr){
		this.detailAddr = detailAddr;
	}
	/**
	 * 获取  note
	 *@return: String  项目概况
	 */
	public String getNote(){
		return this.note;
	}

	/**
	 * 设置  note
	 *@param: note  项目概况
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 * 获取  protectType
	 *@return: String  支护形式
	 */
	public String getProtectType(){
		return this.protectType;
	}

	/**
	 * 设置  protectType
	 *@param: protectType  支护形式
	 */
	public void setProtectType(String protectType){
		this.protectType = protectType;
	}
	/**
	 * 获取  startDate
	 *@return: Date  开工日期
	 */
	public Date getStartDate(){
		return this.startDate;
	}

	/**
	 * 设置  startDate
	 *@param: startDate  开工日期
	 */
	public void setStartDate(Date startDate){
		this.startDate = startDate;
	}
	/**
	 * 获取  area
	 *@return: String  开挖面积(㎡)
	 */
	public String getArea(){
		return this.area;
	}

	/**
	 * 设置  area
	 *@param: area  开挖面积(㎡)
	 */
	public void setArea(String area){
		this.area = area;
	}
	/**
	 * 获取  depth
	 *@return: String  开挖深度(m)
	 */
	public String getDepth(){
		return this.depth;
	}

	/**
	 * 设置  depth
	 *@param: depth  开挖深度(m)
	 */
	public void setDepth(String depth){
		this.depth = depth;
	}
	/**
	 * 获取  level
	 *@return: String  安全等级
	 */
	public String getLevel(){
		return this.level;
	}

	/**
	 * 设置  level
	 *@param: level  安全等级
	 */
	public void setLevel(String level){
		this.level = level;
	}
	/**
	 * 获取  measureUserId
	 *@return: String  监测负责人Id
	 */
	public String getMeasureUserId(){
		return this.measureUserId;
	}

	/**
	 * 设置  measureUserId
	 *@param: measureUserId  监测负责人Id
	 */
	public void setMeasureUserId(String measureUserId){
		this.measureUserId = measureUserId;
	}
	/**
	 * 获取  measureUserName
	 *@return: String  监测负责人
	 */
	public String getMeasureUserName(){
		return this.measureUserName;
	}

	/**
	 * 设置  measureUserName
	 *@param: measureUserName  监测负责人
	 */
	public void setMeasureUserName(String measureUserName){
		this.measureUserName = measureUserName;
	}
	/**
	 * 获取  techUserId
	 *@return: String  技术负责人
	 */
	public String getTechUserId(){
		return this.techUserId;
	}

	/**
	 * 设置  techUserId
	 *@param: techUserId  技术负责人
	 */
	public void setTechUserId(String techUserId){
		this.techUserId = techUserId;
	}
	/**
	 * 获取  techUserName
	 *@return: String  技术负责人
	 */
	public String getTechUserName(){
		return this.techUserName;
	}

	/**
	 * 设置  techUserName
	 *@param: techUserName  技术负责人
	 */
	public void setTechUserName(String techUserName){
		this.techUserName = techUserName;
	}
	/**
	 * 获取  buildUnitUserId
	 *@return: String  建设单位负责人
	 */
	public String getBuildUnitUserId(){
		return this.buildUnitUserId;
	}

	/**
	 * 设置  buildUnitUserId
	 *@param: buildUnitUserId  建设单位负责人
	 */
	public void setBuildUnitUserId(String buildUnitUserId){
		this.buildUnitUserId = buildUnitUserId;
	}
	/**
	 * 获取  buildUnitUserName
	 *@return: String  建设单位负责人
	 */
	public String getBuildUnitUserName(){
		return this.buildUnitUserName;
	}

	/**
	 * 设置  buildUnitUserName
	 *@param: buildUnitUserName  建设单位负责人
	 */
	public void setBuildUnitUserName(String buildUnitUserName){
		this.buildUnitUserName = buildUnitUserName;
	}
	/**
	 * 获取  designUnitUserId
	 *@return: String  设计单位负责人
	 */
	public String getDesignUnitUserId(){
		return this.designUnitUserId;
	}

	/**
	 * 设置  designUnitUserId
	 *@param: designUnitUserId  设计单位负责人
	 */
	public void setDesignUnitUserId(String designUnitUserId){
		this.designUnitUserId = designUnitUserId;
	}
	/**
	 * 获取  designUnitUserName
	 *@return: String  设计单位负责人
	 */
	public String getDesignUnitUserName(){
		return this.designUnitUserName;
	}

	/**
	 * 设置  designUnitUserName
	 *@param: designUnitUserName  设计单位负责人
	 */
	public void setDesignUnitUserName(String designUnitUserName){
		this.designUnitUserName = designUnitUserName;
	}
	/**
	 * 获取  entrustUnitUserId
	 *@return: String  委托单位负责人
	 */
	public String getEntrustUnitUserId(){
		return this.entrustUnitUserId;
	}

	/**
	 * 设置  entrustUnitUserId
	 *@param: entrustUnitUserId  委托单位负责人
	 */
	public void setEntrustUnitUserId(String entrustUnitUserId){
		this.entrustUnitUserId = entrustUnitUserId;
	}
	/**
	 * 获取  entrustUnitUserName
	 *@return: String  委托单位负责人
	 */
	public String getEntrustUnitUserName(){
		return this.entrustUnitUserName;
	}

	/**
	 * 设置  entrustUnitUserName
	 *@param: entrustUnitUserName  委托单位负责人
	 */
	public void setEntrustUnitUserName(String entrustUnitUserName){
		this.entrustUnitUserName = entrustUnitUserName;
	}
	/**
	 * 获取  supervisingUnitUserId
	 *@return: String  监理单位负责人
	 */
	public String getSupervisingUnitUserId(){
		return this.supervisingUnitUserId;
	}

	/**
	 * 设置  supervisingUnitUserId
	 *@param: supervisingUnitUserId  监理单位负责人
	 */
	public void setSupervisingUnitUserId(String supervisingUnitUserId){
		this.supervisingUnitUserId = supervisingUnitUserId;
	}
	/**
	 * 获取  supervisingUnitUserName
	 *@return: String  监理单位负责人
	 */
	public String getSupervisingUnitUserName(){
		return this.supervisingUnitUserName;
	}

	/**
	 * 设置  supervisingUnitUserName
	 *@param: supervisingUnitUserName  监理单位负责人
	 */
	public void setSupervisingUnitUserName(String supervisingUnitUserName){
		this.supervisingUnitUserName = supervisingUnitUserName;
	}
	/**
	 * 获取  mainUnitUserId
	 *@return: String  总包单位负 责人  -
	 */
	public String getMainUnitUserId(){
		return this.mainUnitUserId;
	}

	/**
	 * 设置  mainUnitUserId
	 *@param: mainUnitUserId  总包单位负 责人  -
	 */
	public void setMainUnitUserId(String mainUnitUserId){
		this.mainUnitUserId = mainUnitUserId;
	}
	/**
	 * 获取  mainUnitUserName
	 *@return: String  总包单位负 责人  -
	 */
	public String getMainUnitUserName(){
		return this.mainUnitUserName;
	}

	/**
	 * 设置  mainUnitUserName
	 *@param: mainUnitUserName  总包单位负 责人  -
	 */
	public void setMainUnitUserName(String mainUnitUserName){
		this.mainUnitUserName = mainUnitUserName;
	}
	/**
	 * 获取  implementUnitUserId
	 *@return: String  支护施工单位负责人
	 */
	public String getImplementUnitUserId(){
		return this.implementUnitUserId;
	}

	/**
	 * 设置  implementUnitUserId
	 *@param: implementUnitUserId  支护施工单位负责人
	 */
	public void setImplementUnitUserId(String implementUnitUserId){
		this.implementUnitUserId = implementUnitUserId;
	}
	/**
	 * 获取  implementUnitUserName
	 *@return: String  支护施工单位负责人
	 */
	public String getImplementUnitUserName(){
		return this.implementUnitUserName;
	}

	/**
	 * 设置  implementUnitUserName
	 *@param: implementUnitUserName  支护施工单位负责人
	 */
	public void setImplementUnitUserName(String implementUnitUserName){
		this.implementUnitUserName = implementUnitUserName;
	}
	/**
	 * 获取  buildUnit
	 *@return: String  建设单位
	 */
	public String getBuildUnit(){
		return this.buildUnit;
	}

	/**
	 * 设置  buildUnit
	 *@param: buildUnit  建设单位
	 */
	public void setBuildUnit(String buildUnit){
		this.buildUnit = buildUnit;
	}
	/**
	 * 获取  designUnit
	 *@return: String  设计单位
	 */
	public String getDesignUnit(){
		return this.designUnit;
	}

	/**
	 * 设置  designUnit
	 *@param: designUnit  设计单位
	 */
	public void setDesignUnit(String designUnit){
		this.designUnit = designUnit;
	}
	/**
	 * 获取  entrustUnit
	 *@return: String  委托单位
	 */
	public String getEntrustUnit(){
		return this.entrustUnit;
	}

	/**
	 * 设置  entrustUnit
	 *@param: entrustUnit  委托单位
	 */
	public void setEntrustUnit(String entrustUnit){
		this.entrustUnit = entrustUnit;
	}
	/**
	 * 获取  supervisingUnit
	 *@return: String  监理单位
	 */
	public String getSupervisingUnit(){
		return this.supervisingUnit;
	}

	/**
	 * 设置  supervisingUnit
	 *@param: supervisingUnit  监理单位
	 */
	public void setSupervisingUnit(String supervisingUnit){
		this.supervisingUnit = supervisingUnit;
	}
	/**
	 * 获取  mainUnit
	 *@return: String  总包单位
	 */
	public String getMainUnit(){
		return this.mainUnit;
	}

	/**
	 * 设置  mainUnit
	 *@param: mainUnit  总包单位
	 */
	public void setMainUnit(String mainUnit){
		this.mainUnit = mainUnit;
	}
	/**
	 * 获取  implementUnit
	 *@return: String  支护施工单位
	 */
	public String getImplementUnit(){
		return this.implementUnit;
	}

	/**
	 * 设置  implementUnit
	 *@param: implementUnit  支护施工单位
	 */
	public void setImplementUnit(String implementUnit){
		this.implementUnit = implementUnit;
	}
	/**
	 * 获取  entrustAddr
	 *@return: String  委托单位地址
	 */
	public String getEntrustAddr(){
		return this.entrustAddr;
	}

	/**
	 * 设置  entrustAddr
	 *@param: entrustAddr  委托单位地址
	 */
	public void setEntrustAddr(String entrustAddr){
		this.entrustAddr = entrustAddr;
	}
	/**
	 * 获取  pointProtect
	 *@return: String  测点保护措施
	 */
	public String getPointProtect(){
		return this.pointProtect;
	}

	/**
	 * 设置  pointProtect
	 *@param: pointProtect  测点保护措施
	 */
	public void setPointProtect(String pointProtect){
		this.pointProtect = pointProtect;
	}
	/**
	 * 获取  alarmIndex
	 *@return: String  报警指标
	 */
	public String getAlarmIndex(){
		return this.alarmIndex;
	}

	/**
	 * 设置  alarmIndex
	 *@param: alarmIndex  报警指标
	 */
	public void setAlarmIndex(String alarmIndex){
		this.alarmIndex = alarmIndex;
	}
	/**
	 * 获取  monitorId
	 *@return: String  组织ID
	 */
	public String getMonitorId(){
		return this.monitorId;
	}

	/**
	 * 设置  monitorId
	 *@param: monitorId  组织ID
	 */
	public void setMonitorId(String monitorId){
		this.monitorId = monitorId;
	}
	/**
	 * 获取  monitorName
	 *@return: String  组织名称
	 */
	public String getMonitorName(){
		return this.monitorName;
	}

	/**
	 * 设置  monitorName
	 *@param: monitorName  组织名称
	 */
	public void setMonitorName(String monitorName){
		this.monitorName = monitorName;
	}
	/**
	 * 获取  warnPointCount
	 *@return: Integer  预警测点数统计
	 */
	public Integer getWarnPointCount(){
		return this.warnPointCount;
	}

	/**
	 * 设置  warnPointCount
	 *@param: warnPointCount  预警测点数统计
	 */
	public void setWarnPointCount(Integer warnPointCount){
		this.warnPointCount = warnPointCount;
	}
	/**
	 * 获取  alarmPointCount
	 *@return: Integer  告警测点数统计
	 */
	public Integer getAlarmPointCount(){
		return this.alarmPointCount;
	}

	/**
	 * 设置  alarmPointCount
	 *@param: alarmPointCount  告警测点数统计
	 */
	public void setAlarmPointCount(Integer alarmPointCount){
		this.alarmPointCount = alarmPointCount;
	}
	/**
	 * 获取  controlPointCount
	 *@return: Integer  超控测点数统计
	 */
	public Integer getControlPointCount(){
		return this.controlPointCount;
	}

	/**
	 * 设置  controlPointCount
	 *@param: controlPointCount  超控测点数统计
	 */
	public void setControlPointCount(Integer controlPointCount){
		this.controlPointCount = controlPointCount;
	}
	/**
	 * 获取  implementLatestLog
	 *@return: String  施工最新进度
	 */
	public String getImplementLatestLog(){
		return this.implementLatestLog;
	}

	/**
	 * 设置  implementLatestLog
	 *@param: implementLatestLog  施工最新进度
	 */
	public void setImplementLatestLog(String implementLatestLog){
		this.implementLatestLog = implementLatestLog;
	}
	/**
	 * 获取  province
	 *@return: String  省
	 */
	public String getProvince(){
		return this.province;
	}

	/**
	 * 设置  province
	 *@param: province  省
	 */
	public void setProvince(String province){
		this.province = province;
	}
	/**
	 * 获取  city
	 *@return: String  市
	 */
	public String getCity(){
		return this.city;
	}

	/**
	 * 设置  city
	 *@param: city  市
	 */
	public void setCity(String city){
		this.city = city;
	}
	/**
	 * 获取  district
	 *@return: String  区
	 */
	public String getDistrict(){
		return this.district;
	}

	/**
	 * 设置  district
	 *@param: district  区
	 */
	public void setDistrict(String district){
		this.district = district;
	}
	
}