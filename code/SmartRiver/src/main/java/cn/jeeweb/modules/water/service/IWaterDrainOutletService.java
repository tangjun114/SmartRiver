package cn.jeeweb.modules.water.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.water.entity.WaterDrainOutlet;

/**   
 * @Title: 排污口管理
 * @Description: 排污口管理
 * @author liyonglei
 * @date 2018-11-14 19:33:01
 * @version V1.0   
 *
 */
public interface IWaterDrainOutletService extends ICommonService<WaterDrainOutlet> {

}

