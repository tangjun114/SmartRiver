package cn.jeeweb.modules.water.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.water.mapper.WaterEquipmentMapper;
import cn.jeeweb.modules.water.entity.WaterEquipment;
import cn.jeeweb.modules.water.service.IWaterEquipmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 监测站设备
 * @Description: 监测站设备
 * @author liyonglei
 * @date 2018-11-13 21:09:06
 * @version V1.0   
 *
 */
@Transactional
@Service("waterEquipmentService")
public class WaterEquipmentServiceImpl  extends CommonServiceImpl<WaterEquipmentMapper,WaterEquipment> implements  IWaterEquipmentService {

}
