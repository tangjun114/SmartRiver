package cn.jeeweb.modules.webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.web.controller.FFTableController;

import cn.jeeweb.modules.monitor.entity.McMonitorDevice;

@Controller
@RequestMapping("${api.url.prefix}/monitordevice")
public class MonitorDeviceApi extends FFTableController<McMonitorDevice>
{
	

}
