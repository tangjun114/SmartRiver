package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorSensor;
 
/**   
 * @Title: 传感器信息数据库控制层接口
 * @Description: 传感器信息数据库控制层接口
 * @author shawloong
 * @date 2017-12-07 11:17:08
 * @version V1.0   
 *
 */
public interface McMonitorSensorMapper extends BaseMapper<McMonitorSensor> {
    
}