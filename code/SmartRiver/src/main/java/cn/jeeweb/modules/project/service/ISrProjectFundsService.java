package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectFunds;

/**   
 * @Title: 工程经费
 * @Description: 工程经费
 * @author wsh
 * @date 2018-12-10 20:42:10
 * @version V1.0   
 *
 */
public interface ISrProjectFundsService extends ICommonService<SrProjectFunds> {

}

