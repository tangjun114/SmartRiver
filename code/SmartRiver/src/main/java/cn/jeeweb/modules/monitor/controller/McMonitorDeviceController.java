package cn.jeeweb.modules.monitor.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McMonitorDevice;
import cn.jeeweb.modules.supervise.controller.McProjectBaseController;

/**   
 * @Title: 设备信息
 * @Description: 设备信息
 * @author shawloong
 * @date 2017-10-14 00:12:17
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/monitor/mcmonitordevice")
@RequiresPathPermission("monitor:mcmonitordevice")
public class McMonitorDeviceController extends McOrgBaseController<McMonitorDevice, String> {
	
	@Override
	public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
	
       String deviceTypeName = request.getParameter("typeName");
       if(!StringUtils.isEmpty(deviceTypeName)){
    	   model.addAttribute("typeName", deviceTypeName);
		}
       super.preList(model, request, response);
       
	}
	
	
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McMonitorDevice> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
		String  deviceTypeName = request.getParameter("typeName");
		if(!StringUtils.isEmpty(deviceTypeName)){
			entityWrapper.eq("typeName", deviceTypeName);
		}
		super.preAjaxList(queryable, entityWrapper, request, response);
	}
}
