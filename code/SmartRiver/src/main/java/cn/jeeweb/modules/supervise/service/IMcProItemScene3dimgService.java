package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemScene3dimg;

/**   
 * @Title: 项目监控-项目三维图
 * @Description: 项目监控-项目三维图
 * @author shawloong
 * @date 2017-11-02 21:39:34
 * @version V1.0   
 *
 */
public interface IMcProItemScene3dimgService extends ICommonService<McProItemScene3dimg> {

}

