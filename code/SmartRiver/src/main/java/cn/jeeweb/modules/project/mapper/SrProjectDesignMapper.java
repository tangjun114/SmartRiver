package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectDesign;
 
/**   
 * @Title: 项目设计方案数据库控制层接口
 * @Description: 项目设计方案数据库控制层接口
 * @author wsh
 * @date 2018-12-10 18:23:54
 * @version V1.0   
 *
 */
public interface SrProjectDesignMapper extends BaseMapper<SrProjectDesign> {
    
}