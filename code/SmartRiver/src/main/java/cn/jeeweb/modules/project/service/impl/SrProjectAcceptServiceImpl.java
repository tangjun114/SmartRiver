package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectAcceptMapper;
import cn.jeeweb.modules.project.entity.SrProjectAccept;
import cn.jeeweb.modules.project.service.ISrProjectAcceptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目验收
 * @Description: 项目验收
 * @author wsh
 * @date 2019-03-31 21:56:15
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectAcceptService")
public class SrProjectAcceptServiceImpl  extends CommonServiceImpl<SrProjectAcceptMapper,SrProjectAccept> implements  ISrProjectAcceptService {

}
