package cn.jeeweb.modules.webservice;

import java.util.Date;
import java.util.List;

import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;

public class UploadDataModel {

	private String monitorItemId;
	private Date uploadTime;
	private int count;
	private String pointCode;
	private List<McProItemRealTimeMonData> data;
 	
	public String getPointCode() {
		return pointCode;
	}
	public void setPointCode(String pointCode) {
		this.pointCode = pointCode;
	}
	public String getMonitorItemId() {
		return monitorItemId;
	}
	public void setMonitorItemId(String monitorItemId) {
		this.monitorItemId = monitorItemId;
	}
 
	public Date getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}
	public List<McProItemRealTimeMonData> getData() {
		return data;
	}
	public void setData(List<McProItemRealTimeMonData> data) {
		this.data = data;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
}
