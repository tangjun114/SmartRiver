package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectBudget;

/**   
 * @Title: 项目预算
 * @Description: 项目预算
 * @author jerry
 * @date 2018-11-13 21:29:59
 * @version V1.0   
 *
 */
public interface ISrProjectBudgetService extends ICommonService<SrProjectBudget> {

}

