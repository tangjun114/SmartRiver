package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodSaveHaven;
 
/**   
 * @Title: 避难中心数据库控制层接口
 * @Description: 避难中心数据库控制层接口
 * @author liyonglei
 * @date 2018-11-09 11:19:20
 * @version V1.0   
 *
 */
public interface FloodSaveHavenMapper extends BaseMapper<FloodSaveHaven> {
    
}