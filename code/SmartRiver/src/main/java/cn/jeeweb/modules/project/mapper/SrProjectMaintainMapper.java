package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectMaintain;
 
/**   
 * @Title: 项目维护数据库控制层接口
 * @Description: 项目维护数据库控制层接口
 * @author wsh
 * @date 2018-12-25 20:43:42
 * @version V1.0   
 *
 */
public interface SrProjectMaintainMapper extends BaseMapper<SrProjectMaintain> {
    
}