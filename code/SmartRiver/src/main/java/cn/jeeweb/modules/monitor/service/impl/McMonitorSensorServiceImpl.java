package cn.jeeweb.modules.monitor.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.monitor.mapper.McMonitorSensorMapper;
import cn.jeeweb.modules.constant.SensorStatusConst;
import cn.jeeweb.modules.monitor.entity.McMonitorSensor;
import cn.jeeweb.modules.monitor.service.IMcMonitorSensorService;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import com.ff.common.util.validate.ValidatorUtil;

import org.apache.commons.lang3.StringEscapeUtils;

/**   
 * @Title: 传感器信息
 * @Description: 传感器信息
 * @author shawloong
 * @date 2017-10-13 00:07:04
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorSensorService")
public class McMonitorSensorServiceImpl  extends CommonServiceImpl<McMonitorSensorMapper,McMonitorSensor> implements  IMcMonitorSensorService {
	
	@Override
	public boolean insert(McMonitorSensor mcMonitorSensor) {
		// 保存主表
		mcMonitorSensor.setStatus(SensorStatusConst.NO_USE);
		super.insert(mcMonitorSensor);
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(McMonitorSensor mcMonitorSensor) {
		try {
			// 更新主表
			super.insertOrUpdate(mcMonitorSensor);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}

	@Override
	public void setPointInfo(McProItemMeasuringPoint point) 
	{


	 
 		List<String> ids = new ArrayList<String>();
		if(!ValidatorUtil.isEmpty(point.getSensorId1()))
		{
			ids.add(point.getSensorId1());
		}
		if(!ValidatorUtil.isEmpty(point.getSensorId2()))
		{
			ids.add(point.getSensorId2());
		}
		if(!ValidatorUtil.isEmpty(point.getSensorId3()))
		{
			ids.add(point.getSensorId3());
		}
		if(!ValidatorUtil.isEmpty(point.getSensorId4()))
		{
			ids.add(point.getSensorId4());
		}
		
		
		clearPointInfo(point);	
		
		for(String id : ids)
		{
			McMonitorSensor sensor = this.selectById(id);
			if(null != sensor)
			{
				sensor.setProjectId(point.getProjectId());
				sensor.setProjectName(point.getProjectName());
				sensor.setMonitorItemTypeCode(point.getMonitorItemTypeCode());
				sensor.setMonitorItemTypeName(point.getMonitorItemTypeName());
				sensor.setMonitorItemId(point.getMonitorItemId());
				sensor.setMonitorItemName(point.getMonitorItemName());
				sensor.setMeasurePointId(point.getId());
				sensor.setMeasurePointCode(point.getMeasuringPointCode());
				sensor.setStatus(SensorStatusConst.USED);
				this.updateAllColumnById(sensor);
			}
		}
	}
	public void clearPointInfo(McProItemMeasuringPoint point) 
	{
		if(!ValidatorUtil.isEmpty(point.getId()))
		{	
			Queryable qy = QueryRequest.newQueryable();
			qy.addCondition("measurePointId", point.getId());
			
			List<McMonitorSensor> sensorList = this.listWithNoPage(qy);
			for(McMonitorSensor sensor : sensorList)
			{
				//sensor.setProjectId(null);
				//sensor.setProjectName(null);
				//sensor.setMonitorItemTypeCode(null);
				//sensor.setMonitorItemTypeName(null);
				sensor.setMonitorItemId(null);
				sensor.setMonitorItemName(null);
				sensor.setMeasurePointId(null);
				sensor.setMeasurePointCode(null);
				sensor.setStatus(SensorStatusConst.NO_USE);
				this.updateAllColumnById(sensor);
			}
			
		}
	}
	
	
}
