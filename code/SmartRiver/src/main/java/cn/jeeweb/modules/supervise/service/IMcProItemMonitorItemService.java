package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;

/**   
 * @Title: 项目监控-测点设置-监测项
 * @Description: 项目监控-测点设置-监测项
 * @author shawloong
 * @date 2017-11-05 19:41:39
 * @version V1.0   
 *
 */
public interface IMcProItemMonitorItemService extends ICommonService<McProItemMonitorItem> {
	public void refresh(String id);
}

