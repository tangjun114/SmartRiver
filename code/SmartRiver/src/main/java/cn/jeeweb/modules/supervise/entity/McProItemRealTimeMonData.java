package cn.jeeweb.modules.supervise.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.DataEntity;
import cn.jeeweb.modules.constant.DataResultConst;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 项目监控-监测情况-实时监测数据
 * @Description: 项目监控-监测情况-实时监测数据
 * @author shawloong
 * @date 2017-11-23 00:13:45
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_real_time_mon_data")
@SuppressWarnings("serial")
public class McProItemRealTimeMonData extends DataEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**监测项Id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**特征值*/
    @TableField(value = "charac_desc")
	private String characDesc;
    /**点号*/
    @TableField(value = "measuring_point_code")
	private String measuringPointCode;
    /**点id*/
    @TableField(value = "measuring_point_id")
	private String measuringPointId;
    /**特征值(键)*/
    @TableField(value = "charac_key")
	private String characKey;
    /**数值*/
    @TableField(value = "now_value")
	private Double nowValue = 0.0;
    /**状态*/
    @TableField(value = "status")
	private String status;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**上次结果*/
    @TableField(value = "last_value")
	private Double lastValue = 0.0;
    /**差值*/
    @TableField(value = "minus_value")
	private Double minusValue = 0.0;
    /**累计值*/
    @TableField(value = "sum_value")
	private Double sumValue = 0.0;
    /**变化率*/
    @TableField(value = "change_rate")
	private Double changeRate = 0.0;
    /**预警值*/
    @TableField(value = "warn_value")
	private Double warnValue = 0.0;
    /**控制值*/
    @TableField(value = "control_value")
	private Double controlValue = 0.0;
    /**测量次数*/
    @TableField(value = "test_count")
	private Integer testCount;
    /**场次*/
    @TableField(value = "project_count")
	private Integer projectCount;
    /**上传时间*/
    @TableField(value = "upload_time")
	private Date uploadTime;
    /**上传类型*/
    @TableField(value = "upload_type")
	private String uploadType;
    /**报警值*/
    @TableField(value = "alarm_value")
	private Double alarmValue;
    /**采集时间*/
    @TableField(value = "collect_time")
	private Date collectTime;
    /**有效性状态*/
    @TableField(value = "data_valid")
	private String dataValid = "未处理";
    /**监测分区ID*/
    @TableField(value = "subarea_id")
	private String subareaId;
    /**监测分区*/
    @TableField(value = "subarea_name")
	private String subareaName;
    /**是否是最新的场次*/
    @TableField(value = "new_project_count")
	private String newProjectCount;
    /**检测结果*/
    @TableField(value = "measuring_result")
	private String measuringResult;
    /**x值*/
    @TableField(value = "xvalue")
	private Double xvalue;
    /**y值*/
    @TableField(value = "yvalue")
	private Double yvalue;
    /**监测项类型编码*/
    @TableField(value = "monitor_item_type_code")
	private String monitorItemTypeCode;
    /**监测项类型名称*/
    @TableField(value = "monitor_item_type_name")
	private String monitorItemTypeName;
    /**是否是有效的结果*/
    @TableField(value = "data_result")
	private Integer dataResult = DataResultConst.no_use;
    /**是否已经测试*/
    @TableField(value = "test")
	private String test;
    /**当前数据显示的标志*/
    @TableField(value = "flag")
	private String flag;
    /**数值最大深度*/
    @TableField(value = "now_value_depth")
	private String nowValueDepth;
    /**差值最大深度*/
    @TableField(value = "minus_value_depth")
	private String minusValueDepth;
    /**累计值最大深度*/
    @TableField(value = "sum_value_depth")
	private String sumValueDepth;
    /**变化率最大深度*/
    @TableField(value = "change_rate_depth")
	private String changeRateDepth;
    /**存储深度对应的字段*/
    @TableField(value = "depth_ext")
	private String depthExt;
    /**初始累计至*/
    @TableField(value = "initial_cumulative_value")
	private Double initialCumulativeValue = 0.0;
    /**初始值*/
    @TableField(value = "init_value")
	private Double initValue = 0.0;
    /**速率报警值*/
    @TableField(value = "rate_alarm_value")
	private Double RateAlarmValue = 0.0;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项Id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项Id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  characDesc
	 *@return: String  特征值
	 */
	public String getCharacDesc(){
		return this.characDesc;
	}

	/**
	 * 设置  characDesc
	 *@param: characDesc  特征值
	 */
	public void setCharacDesc(String characDesc){
		this.characDesc = characDesc;
	}
	/**
	 * 获取  measuringPointCode
	 *@return: String  点号
	 */
	public String getMeasuringPointCode(){
		return this.measuringPointCode;
	}

	/**
	 * 设置  measuringPointCode
	 *@param: measuringPointCode  点号
	 */
	public void setMeasuringPointCode(String measuringPointCode){
		this.measuringPointCode = measuringPointCode;
	}
	/**
	 * 获取  measuringPointId
	 *@return: String  点id
	 */
	public String getMeasuringPointId(){
		return this.measuringPointId;
	}

	/**
	 * 设置  measuringPointId
	 *@param: measuringPointId  点id
	 */
	public void setMeasuringPointId(String measuringPointId){
		this.measuringPointId = measuringPointId;
	}
	/**
	 * 获取  characKey
	 *@return: String  特征值(键)
	 */
	public String getCharacKey(){
		return this.characKey;
	}

	/**
	 * 设置  characKey
	 *@param: characKey  特征值(键)
	 */
	public void setCharacKey(String characKey){
		this.characKey = characKey;
	}
	/**
	 * 获取  nowValue
	 *@return: Double  数值
	 */
	public Double getNowValue(){
		return this.nowValue;
	}

	/**
	 * 设置  nowValue
	 *@param: nowValue  数值
	 */
	public void setNowValue(Double nowValue){
		this.nowValue = nowValue;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  lastValue
	 *@return: Double  上次结果
	 */
	public Double getLastValue(){
		return this.lastValue;
	}

	/**
	 * 设置  lastValue
	 *@param: lastValue  上次结果
	 */
	public void setLastValue(Double lastValue){
		this.lastValue = lastValue;
	}
	/**
	 * 获取  minusValue
	 *@return: Double  差值
	 */
	public Double getMinusValue(){
		return this.minusValue;
	}

	/**
	 * 设置  minusValue
	 *@param: minusValue  差值
	 */
	public void setMinusValue(Double minusValue){
		this.minusValue = minusValue;
	}
	/**
	 * 获取  sumValue
	 *@return: Double  累计值
	 */
	public Double getSumValue(){
		return this.sumValue;
	}

	/**
	 * 设置  sumValue
	 *@param: sumValue  累计值
	 */
	public void setSumValue(Double sumValue){
		this.sumValue = sumValue;
	}
	/**
	 * 获取  changeRate
	 *@return: Double  变化率
	 */
	public Double getChangeRate(){
		return this.changeRate;
	}

	/**
	 * 设置  changeRate
	 *@param: changeRate  变化率
	 */
	public void setChangeRate(Double changeRate){
		this.changeRate = changeRate;
	}
	/**
	 * 获取  warnValue
	 *@return: Double  预警值
	 */
	public Double getWarnValue(){
		return this.warnValue;
	}

	/**
	 * 设置  warnValue
	 *@param: warnValue  预警值
	 */
	public void setWarnValue(Double warnValue){
		this.warnValue = warnValue;
	}
	/**
	 * 获取  controlValue
	 *@return: Double  控制值
	 */
	public Double getControlValue(){
		return this.controlValue;
	}

	/**
	 * 设置  controlValue
	 *@param: controlValue  控制值
	 */
	public void setControlValue(Double controlValue){
		this.controlValue = controlValue;
	}
	/**
	 * 获取  testCount
	 *@return: Integer  测量次数
	 */
	public Integer getTestCount(){
		return this.testCount;
	}

	/**
	 * 设置  testCount
	 *@param: testCount  测量次数
	 */
	public void setTestCount(Integer testCount){
		this.testCount = testCount;
	}
	/**
	 * 获取  projectCount
	 *@return: Integer  场次
	 */
	public Integer getProjectCount(){
		return this.projectCount;
	}

	/**
	 * 设置  projectCount
	 *@param: projectCount  场次
	 */
	public void setProjectCount(Integer projectCount){
		this.projectCount = projectCount;
	}
	/**
	 * 获取  uploadTime
	 *@return: Date  上传时间
	 */
	public Date getUploadTime(){
		return this.uploadTime;
	}

	/**
	 * 设置  uploadTime
	 *@param: uploadTime  上传时间
	 */
	public void setUploadTime(Date uploadTime){
		this.uploadTime = uploadTime;
	}
	/**
	 * 获取  uploadType
	 *@return: String  上传类型
	 */
	public String getUploadType(){
		return this.uploadType;
	}

	/**
	 * 设置  uploadType
	 *@param: uploadType  上传类型
	 */
	public void setUploadType(String uploadType){
		this.uploadType = uploadType;
	}
	/**
	 * 获取  alarmValue
	 *@return: Double  报警值
	 */
	public Double getAlarmValue(){
		return this.alarmValue;
	}

	/**
	 * 设置  alarmValue
	 *@param: alarmValue  报警值
	 */
	public void setAlarmValue(Double alarmValue){
		this.alarmValue = alarmValue;
	}
	/**
	 * 获取  collectTime
	 *@return: Date  采集时间
	 */
	public Date getCollectTime(){
		return this.collectTime;
	}

	/**
	 * 设置  collectTime
	 *@param: collectTime  采集时间
	 */
	public void setCollectTime(Date collectTime){
		this.collectTime = collectTime;
	}
	/**
	 * 获取  dataValid
	 *@return: String  有效性状态
	 */
	public String getDataValid(){
		return this.dataValid;
	}

	/**
	 * 设置  dataValid
	 *@param: dataValid  有效性状态
	 */
	public void setDataValid(String dataValid){
		this.dataValid = dataValid;
	}
	/**
	 * 获取  subareaId
	 *@return: String  监测分区ID
	 */
	public String getSubareaId(){
		return this.subareaId;
	}

	/**
	 * 设置  subareaId
	 *@param: subareaId  监测分区ID
	 */
	public void setSubareaId(String subareaId){
		this.subareaId = subareaId;
	}
	/**
	 * 获取  subareaName
	 *@return: String  监测分区
	 */
	public String getSubareaName(){
		return this.subareaName;
	}

	/**
	 * 设置  subareaName
	 *@param: subareaName  监测分区
	 */
	public void setSubareaName(String subareaName){
		this.subareaName = subareaName;
	}
	/**
	 * 获取  newProjectCount
	 *@return: String  是否是最新的场次
	 */
	public String getNewProjectCount(){
		return this.newProjectCount;
	}

	/**
	 * 设置  newProjectCount
	 *@param: newProjectCount  是否是最新的场次
	 */
	public void setNewProjectCount(String newProjectCount){
		this.newProjectCount = newProjectCount;
	}
	/**
	 * 获取  measuringResult
	 *@return: String  检测结果
	 */
	public String getMeasuringResult(){
		return this.measuringResult;
	}

	/**
	 * 设置  measuringResult
	 *@param: measuringResult  检测结果
	 */
	public void setMeasuringResult(String measuringResult){
		this.measuringResult = measuringResult;
	}
	/**
	 * 获取  xvalue
	 *@return: Double  x值
	 */
	public Double getXvalue(){
		return this.xvalue;
	}

	/**
	 * 设置  xvalue
	 *@param: xvalue  x值
	 */
	public void setXvalue(Double xvalue){
		this.xvalue = xvalue;
	}
	/**
	 * 获取  yvalue
	 *@return: Double  y值
	 */
	public Double getYvalue(){
		return this.yvalue;
	}

	/**
	 * 设置  yvalue
	 *@param: yvalue  y值
	 */
	public void setYvalue(Double yvalue){
		this.yvalue = yvalue;
	}
	/**
	 * 获取  monitorItemTypeCode
	 *@return: String  监测项类型编码
	 */
	public String getMonitorItemTypeCode(){
		return this.monitorItemTypeCode;
	}

	/**
	 * 设置  monitorItemTypeCode
	 *@param: monitorItemTypeCode  监测项类型编码
	 */
	public void setMonitorItemTypeCode(String monitorItemTypeCode){
		this.monitorItemTypeCode = monitorItemTypeCode;
	}
	/**
	 * 获取  monitorItemTypeName
	 *@return: String  监测项类型名称
	 */
	public String getMonitorItemTypeName(){
		return this.monitorItemTypeName;
	}

	/**
	 * 设置  monitorItemTypeName
	 *@param: monitorItemTypeName  监测项类型名称
	 */
	public void setMonitorItemTypeName(String monitorItemTypeName){
		this.monitorItemTypeName = monitorItemTypeName;
	}
	/**
	 * 获取  dataResult
	 *@return: Integer  是否是有效的结果
	 */
	public Integer getDataResult(){
		return this.dataResult;
	}

	/**
	 * 设置  dataResult
	 *@param: dataResult  是否是有效的结果
	 */
	public void setDataResult(Integer dataResult){
		this.dataResult = dataResult;
	}
	/**
	 * 获取  test
	 *@return: String  是否已经测试
	 */
	public String getTest(){
		return this.test;
	}

	/**
	 * 设置  test
	 *@param: test  是否已经测试
	 */
	public void setTest(String test){
		this.test = test;
	}
	/**
	 * 获取  flag
	 *@return: String  当前数据显示的标志
	 */
	public String getFlag(){
		return this.flag;
	}

	/**
	 * 设置  flag
	 *@param: flag  当前数据显示的标志
	 */
	public void setFlag(String flag){
		this.flag = flag;
	}
	/**
	 * 获取  nowValueDepth
	 *@return: String  数值最大深度
	 */
	public String getNowValueDepth(){
		return this.nowValueDepth;
	}

	/**
	 * 设置  nowValueDepth
	 *@param: nowValueDepth  数值最大深度
	 */
	public void setNowValueDepth(String nowValueDepth){
		this.nowValueDepth = nowValueDepth;
	}
	/**
	 * 获取  minusValueDepth
	 *@return: String  差值最大深度
	 */
	public String getMinusValueDepth(){
		return this.minusValueDepth;
	}

	/**
	 * 设置  minusValueDepth
	 *@param: minusValueDepth  差值最大深度
	 */
	public void setMinusValueDepth(String minusValueDepth){
		this.minusValueDepth = minusValueDepth;
	}
	/**
	 * 获取  sumValueDepth
	 *@return: String  累计值最大深度
	 */
	public String getSumValueDepth(){
		return this.sumValueDepth;
	}

	/**
	 * 设置  sumValueDepth
	 *@param: sumValueDepth  累计值最大深度
	 */
	public void setSumValueDepth(String sumValueDepth){
		this.sumValueDepth = sumValueDepth;
	}
	/**
	 * 获取  changeRateDepth
	 *@return: String  变化率最大深度
	 */
	public String getChangeRateDepth(){
		return this.changeRateDepth;
	}

	/**
	 * 设置  changeRateDepth
	 *@param: changeRateDepth  变化率最大深度
	 */
	public void setChangeRateDepth(String changeRateDepth){
		this.changeRateDepth = changeRateDepth;
	}
	/**
	 * 获取  depthExt
	 *@return: String  存储深度对应的字段
	 */
	public String getDepthExt(){
		return this.depthExt;
	}

	/**
	 * 设置  depthExt
	 *@param: depthExt  存储深度对应的字段
	 */
	public void setDepthExt(String depthExt){
		this.depthExt = depthExt;
	}
	/**
	 * 获取  initialCumulativeValue
	 *@return: Double  初始累计至
	 */
	public Double getInitialCumulativeValue(){
		return this.initialCumulativeValue;
	}

	/**
	 * 设置  initialCumulativeValue
	 *@param: initialCumulativeValue  初始累计至
	 */
	public void setInitialCumulativeValue(Double initialCumulativeValue){
		this.initialCumulativeValue = initialCumulativeValue;
	}
	/**
	 * 获取  initValue
	 *@return: Double  初始值
	 */
	public Double getInitValue(){
		return this.initValue;
	}

	/**
	 * 设置  initValue
	 *@param: initValue  初始值
	 */
	public void setInitValue(Double initValue){
		this.initValue = initValue;
	}
	/**
	 * 获取  RateAlarmValue
	 *@return: Double  速率报警值
	 */
	public Double getRateAlarmValue(){
		return this.RateAlarmValue;
	}

	/**
	 * 设置  RateAlarmValue
	 *@param: RateAlarmValue  速率报警值
	 */
	public void setRateAlarmValue(Double RateAlarmValue){
		this.RateAlarmValue = RateAlarmValue;
	}
	
}
