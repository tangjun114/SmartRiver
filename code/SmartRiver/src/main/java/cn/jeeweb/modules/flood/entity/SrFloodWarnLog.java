package cn.jeeweb.modules.flood.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import java.util.Date;

/**   
 * @Title: 报警记录
 * @Description: 报警记录
 * @author wsh
 * @date 2019-03-17 14:20:32
 * @version V1.0   
 *
 */
@TableName("sr_flood_warn_log")
@SuppressWarnings("serial")
public class SrFloodWarnLog extends AbstractEntity<String> {

    /**id*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**site_id*/
    @TableField(value = "site_id")
	private String siteId;
    /**site_name*/
    @TableField(value = "site_name")
	private String siteName;
    /**site_type*/
    @TableField(value = "site_type")
	private Integer siteType;
    /**warn_info*/
    @TableField(value = "warn_info")
	private String warnInfo;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**site_kind*/
    @TableField(value = "site_kind")
	private Integer siteKind;
	
	/**
	 * 获取  id
	 *@return: String  id
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  siteId
	 *@return: String  site_id
	 */
	public String getSiteId(){
		return this.siteId;
	}

	/**
	 * 设置  siteId
	 *@param: siteId  site_id
	 */
	public void setSiteId(String siteId){
		this.siteId = siteId;
	}
	/**
	 * 获取  siteName
	 *@return: String  site_name
	 */
	public String getSiteName(){
		return this.siteName;
	}

	/**
	 * 设置  siteName
	 *@param: siteName  site_name
	 */
	public void setSiteName(String siteName){
		this.siteName = siteName;
	}
	/**
	 * 获取  siteType
	 *@return: Integer  site_type
	 */
	public Integer getSiteType(){
		return this.siteType;
	}

	/**
	 * 设置  siteType
	 *@param: siteType  site_type
	 */
	public void setSiteType(Integer siteType){
		this.siteType = siteType;
	}
	/**
	 * 获取  warnInfo
	 *@return: String  warn_info
	 */
	public String getWarnInfo(){
		return this.warnInfo;
	}

	/**
	 * 设置  warnInfo
	 *@param: warnInfo  warn_info
	 */
	public void setWarnInfo(String warnInfo){
		this.warnInfo = warnInfo;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  siteKind
	 *@return: Integer  site_kind
	 */
	public Integer getSiteKind(){
		return this.siteKind;
	}

	/**
	 * 设置  siteKind
	 *@param: siteKind  site_kind
	 */
	public void setSiteKind(Integer siteKind){
		this.siteKind = siteKind;
	}
	
}
