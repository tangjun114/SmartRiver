package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectInfo;
 
/**   
 * @Title: 项目信息数据库控制层接口
 * @Description: 项目信息数据库控制层接口
 * @author jerry
 * @date 2018-11-13 21:30:12
 * @version V1.0   
 *
 */
public interface SrProjectInfoMapper extends BaseMapper<SrProjectInfo> {
    
}