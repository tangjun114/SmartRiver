package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodAssetsStockRecord;

/**   
 * @Title: 库存统计查询
 * @Description: 库存统计查询
 * @author liyonglei
 * @date 2018-11-23 15:27:46
 * @version V1.0   
 *
 */
public interface IFloodAssetsStockRecordService extends ICommonService<FloodAssetsStockRecord> {

}

