package cn.jeeweb.modules.flood.controller;


import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.flood.entity.FloodRainfull;
import cn.jeeweb.modules.flood.service.IFloodRainfullService;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.task.task.RainfullTask;
import com.ff.bi.model.RptDimDataJson;
import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wsh
 * @version V1.0
 * @Title: 雨量监测
 * @Description: 雨量监测
 * @date 2018-11-27 18:15:03
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/floodrainfull")
@RequiresPathPermission("flood:floodrainfull")
public class FloodRainfullController extends BaseCRUDController<FloodRainfull, String> {
    @Autowired
    IFloodRainfullService floodRainfullService;

    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        String type = request.getParameter("type");
        String siteId = request.getParameter("siteId");
        model.addAttribute("recordType", type);
        model.addAttribute("siteId", siteId);
    }

    @Override
    public String list(Model model, HttpServletRequest request, HttpServletResponse response) {
        return super.list(model, request, response);
    }

    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<FloodRainfull> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
        String recordType = request.getParameter("recordType");
        String siteId = request.getParameter("siteId");
        queryable.addCondition("recordType", recordType);
        if (StringUtils.isNotEmpty(siteId)) {
            queryable.addCondition("siteId", siteId);
        }
        queryable.addOrder(Sort.Direction.DESC,"recordTime");
        super.preAjaxList(queryable, entityWrapper, request, response);
    }

    @RequestMapping(value = "/content1")
    public String showIndex1(Model model, HttpServletRequest request, HttpServletResponse response) {
        this.setAllAttribute(model, request, response);
        return display("content1");
    }

    @RequestMapping("/chart")
    public String sta(Model model, HttpServletRequest request, HttpServletResponse response) {
        return display("chart");
    }

    @RequestMapping("/chart/rainfull")
    public String rainfull(Model model, HttpServletRequest request, HttpServletResponse response) {
        return display("rainfull");
    }

    @RequestMapping("/chart/waterlevel")
    public String waterlevel(Model model, HttpServletRequest request, HttpServletResponse response) {
        return display("waterlevel");
    }

    @RequestMapping("/chart/data")
    @ResponseBody
    public BaseRspJson<RptDimDataJson> rainfullData(@RequestBody BaseReqJson reqJson) {
        BaseRspJson<RptDimDataJson> rsp = new BaseRspJson<>();
        RptDimDataJson data = new RptDimDataJson();

        String type = this.getObj(reqJson, String.class);
        if ("rainfull".equals(type)) {
            data = getRainfullData(reqJson);
        } else if ("waterlevel".equals(type)) {
            data = getWaterLevelData(reqJson);
        }
        rsp.setObj(data);
        return rsp;
    }

    private RptDimDataJson getRainfullData(BaseReqJson request) {
        RptDimDataJson obj = new RptDimDataJson();
        List<FloodRainfull> dataList = null;
        List<?> x_data = null;
        List<?> y_data = null;
        String siteId = "";
        String startTime = "";
        String endTime = "";
        List<Condition.Filter> filters = request.getFilter();
        for (Condition.Filter filter : filters) {
            if ("siteId".equals(filter.getProperty())) {
                siteId = (String) filter.getValue();
            }
            if ("startTime".equals(filter.getProperty())) {
                startTime = (String) filter.getValue();
            }
            if ("endTime".equals(filter.getProperty())) {
                endTime = (String) filter.getValue();
            }
        }

        EntityWrapper<FloodRainfull> entityWrapper1 = new EntityWrapper<FloodRainfull>();
        entityWrapper1.eq("site_id", siteId);
        entityWrapper1.gt("record_time", startTime);
        entityWrapper1.lt("record_time", endTime);
        entityWrapper1.orderBy("record_time", true);
        entityWrapper1.eq("record_type", 0);
        dataList = floodRainfullService.selectList(entityWrapper1);
        x_data = DataFilterUtil.getFieldValueFromList(dataList, "recordTime");
        y_data = DataFilterUtil.getFieldValueFromList(dataList, "rainfull");

        obj.setX_data(x_data);
        obj.setY_data(y_data);


        return obj;

    }


    private RptDimDataJson getWaterLevelData(BaseReqJson request) {
        RptDimDataJson obj = new RptDimDataJson();
        List<FloodRainfull> dataList = null;
        List<?> x_data = null;
        List<?> y_data = null;
        String siteId = "";
        String startTime = "";
        String endTime = "";
        List<Condition.Filter> filters = request.getFilter();
        for (Condition.Filter filter : filters) {
            if ("startTime".equals(filter.getProperty())) {
                startTime = (String) filter.getValue();
            }
            if ("siteId".equals(filter.getProperty())) {
                siteId = (String) filter.getValue();
            }
            if ("endTime".equals(filter.getProperty())) {
                endTime = (String) filter.getValue();
            }
        }

        EntityWrapper<FloodRainfull> entityWrapper1 = new EntityWrapper<FloodRainfull>();
        entityWrapper1.eq("site_id", siteId);
        entityWrapper1.gt("record_time", startTime);
        entityWrapper1.lt("record_time", endTime);
        entityWrapper1.orderBy("record_time", true);
        entityWrapper1.eq("record_type", 1);
        dataList = floodRainfullService.selectList(entityWrapper1);
        x_data = DataFilterUtil.getFieldValueFromList(dataList, "recordTime");
        y_data = DataFilterUtil.getFieldValueFromList(dataList, "waterLevel");

        obj.setX_data(x_data);
        obj.setY_data(y_data);


        return obj;

    }

}
