package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McMonitorRealdata;

/**   
 * @Title: 监测原始记录信息
 * @Description: 监测原始记录信息
 * @author Aether
 * @date 2017-11-29 23:54:11
 * @version V1.0   
 *
 */
public interface IMcMonitorRealdataService extends ICommonService<McMonitorRealdata> {

}

