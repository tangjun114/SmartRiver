package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemAlarmModifyLog;
 
/**   
 * @Title: 报警修改日志数据库控制层接口
 * @Description: 报警修改日志数据库控制层接口
 * @author Aether
 * @date 2018-06-16 20:17:34
 * @version V1.0   
 *
 */
public interface McProItemAlarmModifyLogMapper extends BaseMapper<McProItemAlarmModifyLog> {
    
}