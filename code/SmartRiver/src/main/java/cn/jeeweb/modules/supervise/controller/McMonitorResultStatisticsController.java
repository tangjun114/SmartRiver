package cn.jeeweb.modules.supervise.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McMonitorResultStatistics;

/**   
 * @Title: 监测结果统计
 * @Description: 监测结果统计
 * @author aether
 * @date 2018-01-24 21:32:05
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcmonitorresultstatistics")
@RequiresPathPermission("supervise:mcmonitorresultstatistics")
public class McMonitorResultStatisticsController extends McProjectBaseController<McMonitorResultStatistics, String> {

}
