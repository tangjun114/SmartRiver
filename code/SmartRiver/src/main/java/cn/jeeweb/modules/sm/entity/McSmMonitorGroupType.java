package cn.jeeweb.modules.sm.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 监测组类别
 * @Description: 监测组类别
 * @author jerry
 * @date 2018-04-03 08:10:49
 * @version V1.0   
 *
 */
@TableName("mc_sm_monitor_group_type")
@SuppressWarnings("serial")
public class McSmMonitorGroupType extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**监测组类别*/
    @TableField(value = "group_type_name")
	private String groupTypeName;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**编码*/
    @TableField(value = "code")
	private String code;
    /**单位*/
    @TableField(value = "unit")
	private String unit;
    /**最大值名称*/
    @TableField(value = "max_name")
	private String maxName;
    /**最小值名称*/
    @TableField(value = "min_name")
	private String minName;
    /**监测项备注*/
    @TableField(value = "item_remark")
	private String itemRemark;
    /**当前值与累计值的单位换算关系*/
    @TableField(value = "value_ratio")
	private String valueRatio;
    /**观测方法*/
    @TableField(value = "observation_method")
	private String observationMethod;
    /**默认观测方法*/
    @TableField(value = "default_method")
	private String defaultMethod;
    /**排序值*/
    @TableField(value = "sort_id")
	private Integer sortId;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  groupTypeName
	 *@return: String  监测组类别
	 */
	public String getGroupTypeName(){
		return this.groupTypeName;
	}

	/**
	 * 设置  groupTypeName
	 *@param: groupTypeName  监测组类别
	 */
	public void setGroupTypeName(String groupTypeName){
		this.groupTypeName = groupTypeName;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  code
	 *@return: String  编码
	 */
	public String getCode(){
		return this.code;
	}

	/**
	 * 设置  code
	 *@param: code  编码
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 * 获取  unit
	 *@return: String  单位
	 */
	public String getUnit(){
		return this.unit;
	}

	/**
	 * 设置  unit
	 *@param: unit  单位
	 */
	public void setUnit(String unit){
		this.unit = unit;
	}
	/**
	 * 获取  maxName
	 *@return: String  最大值名称
	 */
	public String getMaxName(){
		return this.maxName;
	}

	/**
	 * 设置  maxName
	 *@param: maxName  最大值名称
	 */
	public void setMaxName(String maxName){
		this.maxName = maxName;
	}
	/**
	 * 获取  minName
	 *@return: String  最小值名称
	 */
	public String getMinName(){
		return this.minName;
	}

	/**
	 * 设置  minName
	 *@param: minName  最小值名称
	 */
	public void setMinName(String minName){
		this.minName = minName;
	}
	/**
	 * 获取  itemRemark
	 *@return: String  监测项备注
	 */
	public String getItemRemark(){
		return this.itemRemark;
	}

	/**
	 * 设置  itemRemark
	 *@param: itemRemark  监测项备注
	 */
	public void setItemRemark(String itemRemark){
		this.itemRemark = itemRemark;
	}
	/**
	 * 获取  valueRatio
	 *@return: String  当前值与累计值的单位换算关系
	 */
	public String getValueRatio(){
		return this.valueRatio;
	}

	/**
	 * 设置  valueRatio
	 *@param: valueRatio  当前值与累计值的单位换算关系
	 */
	public void setValueRatio(String valueRatio){
		this.valueRatio = valueRatio;
	}
	/**
	 * 获取  observationMethod
	 *@return: String  观测方法
	 */
	public String getObservationMethod(){
		return this.observationMethod;
	}

	/**
	 * 设置  observationMethod
	 *@param: observationMethod  观测方法
	 */
	public void setObservationMethod(String observationMethod){
		this.observationMethod = observationMethod;
	}
	/**
	 * 获取  defaultMethod
	 *@return: String  默认观测方法
	 */
	public String getDefaultMethod(){
		return this.defaultMethod;
	}

	/**
	 * 设置  defaultMethod
	 *@param: defaultMethod  默认观测方法
	 */
	public void setDefaultMethod(String defaultMethod){
		this.defaultMethod = defaultMethod;
	}
	/**
	 * 获取  sortId
	 *@return: Integer  排序值
	 */
	public Integer getSortId(){
		return this.sortId;
	}

	/**
	 * 设置  sortId
	 *@param: sortId  排序值
	 */
	public void setSortId(Integer sortId){
		this.sortId = sortId;
	}
	
}
