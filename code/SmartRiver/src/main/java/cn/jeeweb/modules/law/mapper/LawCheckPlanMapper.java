package cn.jeeweb.modules.law.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.law.entity.LawCheckPlan;
 
/**   
 * @Title: 巡检计划数据库控制层接口
 * @Description: 巡检计划数据库控制层接口
 * @author liyonglei
 * @date 2018-11-13 20:27:22
 * @version V1.0   
 *
 */
public interface LawCheckPlanMapper extends BaseMapper<LawCheckPlan> {
    
}