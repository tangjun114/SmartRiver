package cn.jeeweb.modules.sm.controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.ff.common.util.validate.ValidatorUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.common.controller.BaseTreeController;
import cn.jeeweb.core.common.entity.tree.TreeSortUtil;
import cn.jeeweb.core.model.PageJson;
import cn.jeeweb.core.query.data.PropertyPreFilterable;
import cn.jeeweb.core.query.data.QueryPropertyPreFilter;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.ObjectUtils;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.sm.entity.McSmParamConfig;
import cn.jeeweb.modules.sm.service.IMcSmParamConfigService;

/**   
 * @Title: 系统管理-参数设置
 * @Description: 系统管理-参数设置
 * @author shawloong
 * @date 2017-10-11 23:30:03
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/sm/mcsmparamconfig")
@RequiresPathPermission("sm:mcsmparamconfig")
public class McSmParamConfigController extends BaseTreeController<McSmParamConfig, String> {
	@Autowired
	private IMcSmParamConfigService mcSmParamConfigService;
	/**
	 * 根据页码和每页记录数，以及查询条件动态加载数据
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "treeData")
	@Override
	public void treeData(Queryable queryable,
			@RequestParam(value = "nodeid", required = false, defaultValue = "") String nodeid,
			@RequestParam(value = "async", required = false, defaultValue = "false") boolean async,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		EntityWrapper<McSmParamConfig> entityWrapper = new EntityWrapper<McSmParamConfig>(entityClass);
		entityWrapper.setTableAlias("t.");
		entityWrapper.orderBy("index", true);
		List<McSmParamConfig> treeNodeList = null;
		if (!async) { // 非异步 查自己和子子孙孙
			if (!ObjectUtils.isNullOrEmpty(nodeid)) {
				// 判断的应该是多个OR条件
				entityWrapper.like("parentIds", nodeid) ;
			} 
			treeNodeList = mcSmParamConfigService.selectTreeList(queryable, entityWrapper);
			TreeSortUtil.create().sort(treeNodeList).async(treeNodeList);
		} else { // 异步模式只查自己
			// queryable.addCondition("parentId", nodeid);
			if (ObjectUtils.isNullOrEmpty(nodeid)) {
				// 判断的应该是多个OR条件
				entityWrapper.isNull("parentId");
			} else {
				entityWrapper.eq("parentId", nodeid);
			}
			treeNodeList = mcSmParamConfigService.selectTreeList(queryable, entityWrapper);
			TreeSortUtil.create().sync(treeNodeList);
		}
		PropertyPreFilterable propertyPreFilterable = new QueryPropertyPreFilter();
		propertyPreFilterable.addQueryProperty("id", "name", "expanded", "hasChildren", "leaf", "loaded", "level",
				"parentId","fieldMapper","paraValue");
		SerializeFilter filter = propertyPreFilterable.constructFilter(entityClass);
		PageJson<McSmParamConfig> pagejson = new PageJson<McSmParamConfig>(treeNodeList);
		String content = JSON.toJSONString(pagejson, filter);
		StringUtils.printJson(response, content);
	}
	
	
	@RequestMapping("/alltype")
	@ResponseBody
	public BaseRspJson<List<McSmParamConfig>> getAllType(@RequestBody BaseReqJson request)
	{
		BaseRspJson<List<McSmParamConfig>> rsp = new BaseRspJson<List<McSmParamConfig>>();
		Queryable queryable = QueryRequest.newQueryable();
		queryable.addCondition("parentId", "8feb09f7fc45497ebbed7efdf527f2aa");
		List<McSmParamConfig> obj = this.commonService.listWithNoPage(queryable);
		rsp.setObj(obj);
  		return rsp;
	}
	
	@RequestMapping("/sensorbytype")
	@ResponseBody
	public BaseRspJson<List<McSmParamConfig>> sensorByType(@RequestBody BaseReqJson request)
	{
		BaseRspJson<List<McSmParamConfig>> rsp = new BaseRspJson<List<McSmParamConfig>>();
		Queryable queryable = this.GetFilterCondition(request);
		if(null == queryable.getCondition())
		{
			Queryable qy = QueryRequest.newQueryable();
			qy.addCondition("parentId","ac5dfaa19dcc4ff7bad51182db712043");
 			List<McSmParamConfig> obj = this.commonService.listWithNoPage(qy);
			rsp.setObj(obj);
		}
		else
		{
			McSmParamConfig typeConfig = this.commonService.get(queryable);
			if(null != typeConfig)
	 		{
				queryable =  QueryRequest.newQueryable();
				queryable.addCondition("parentId", typeConfig.getId());
				queryable.addCondition("name", "传感器类型");
				McSmParamConfig config = this.commonService.get(queryable);
				if(null != config)
				{
					Queryable qy = QueryRequest.newQueryable();
					qy.addCondition("parentId", config.getId());
		 			List<McSmParamConfig> obj = this.commonService.listWithNoPage(qy);
					rsp.setObj(obj);
				}
			}
		}
 

		
  		return rsp;
	}
	@RequestMapping("/devicebytype")
	@ResponseBody
	public BaseRspJson<List<McSmParamConfig>> deviceByType(@RequestBody BaseReqJson request)
	{
		BaseRspJson<List<McSmParamConfig>> rsp = new BaseRspJson<List<McSmParamConfig>>();
		Queryable queryable = this.GetFilterCondition(request);
		if(null == queryable.getCondition())
		{
 			queryable.addCondition("parentId", "6836d173d77847f797eadf3d1ae33a3b");
			List<McSmParamConfig> obj = this.commonService.listWithNoPage(queryable);
			rsp.setObj(obj);
		}
		else
		{
			McSmParamConfig typeConfig = this.commonService.get(queryable);
			if(null != typeConfig)
	 		{
				queryable =  QueryRequest.newQueryable();
				queryable.addCondition("parentId", typeConfig.getId());
				queryable.addCondition("name", "设备类型");
				McSmParamConfig config = this.commonService.get(queryable);
				if(null != config)
				{
					Queryable qy = QueryRequest.newQueryable();
					qy.addCondition("parentId", config.getId());
		 			List<McSmParamConfig> obj = this.commonService.listWithNoPage(qy);
					rsp.setObj(obj);
				}
			}
		}
  		return rsp;
	}
	
	@RequestMapping("/devicebymodel")
	@ResponseBody
	public BaseRspJson<List<McSmParamConfig>> deviceByModel(@RequestBody BaseReqJson request)
	{
		BaseRspJson<List<McSmParamConfig>> rsp = new BaseRspJson<List<McSmParamConfig>>();
		Queryable queryable = this.GetFilterCondition(request);
		
		queryable.addCondition("parentId", "6836d173d77847f797eadf3d1ae33a3b");
 
			McSmParamConfig typeConfig = this.commonService.get(queryable);
			if(null != typeConfig)
	 		{
				queryable =  QueryRequest.newQueryable();
				queryable.addCondition("parentId", typeConfig.getId());
	 			List<McSmParamConfig> obj = this.commonService.listWithNoPage(queryable);
	 			rsp.setObj(obj);
			}
	 
  		return rsp;
	}
	@RequestMapping("/sensorbymodel")
	@ResponseBody
	public BaseRspJson<List<McSmParamConfig>> sensorByModel(@RequestBody BaseReqJson request)
	{
		BaseRspJson<List<McSmParamConfig>> rsp = new BaseRspJson<List<McSmParamConfig>>();
		Queryable queryable = this.GetFilterCondition(request);
		
		queryable.addCondition("parentId", "ac5dfaa19dcc4ff7bad51182db712043");
 
			McSmParamConfig typeConfig = this.commonService.get(queryable);
			if(null != typeConfig)
	 		{
				queryable =  QueryRequest.newQueryable();
				queryable.addCondition("parentId", typeConfig.getId());
	 			List<McSmParamConfig> obj = this.commonService.listWithNoPage(queryable);
	 			rsp.setObj(obj);
			}
	 
  		return rsp;
	}
}
