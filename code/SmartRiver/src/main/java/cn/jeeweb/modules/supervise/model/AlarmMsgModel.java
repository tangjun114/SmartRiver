package cn.jeeweb.modules.supervise.model;

public class AlarmMsgModel 
{
	private String monitorUnit;
	private String projectName;
	private String monitorItemName;
 	private String time;
	private String alarmMsg;
 
	private String website;
	private String projectMember;
	public String getMonitorUnit() {
		return monitorUnit;
	}
	public void setMonitorUnit(String monitorUnit) {
		this.monitorUnit = monitorUnit;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getMonitorItemName() {
		return monitorItemName;
	}
	public void setMonitorItemName(String monitorItemName) {
		this.monitorItemName = monitorItemName;
	}
 
	public String getTime() {
		return time;
	}
	public String getAlarmMsg() {
		return alarmMsg;
	}
	public void setAlarmMsg(String alarmMsg) {
		this.alarmMsg = alarmMsg;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getProjectMember() {
		return projectMember;
	}
	public void setProjectMember(String projectMember) {
		this.projectMember = projectMember;
	}
	public void setTime(String time) {
		this.time = time;
	}
	 
	

}
