package cn.jeeweb.modules.repo.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.repo.mapper.McRepoStandardMapper;
import cn.jeeweb.modules.repo.entity.McRepoStandard;
import cn.jeeweb.modules.repo.service.IMcRepoStandardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;

/**   
 * @Title: 规范资料
 * @Description: 规范资料
 * @author shawloong
 * @date 2017-10-04 15:16:45
 * @version V1.0   
 *
 */
@Transactional
@Service("mcRepoStandardService")
public class McRepoStandardServiceImpl  extends CommonServiceImpl<McRepoStandardMapper,McRepoStandard> implements  IMcRepoStandardService {
	
	@Override
	public boolean insert(McRepoStandard mcRepoStandard) {
		// 保存主表
		super.insert(mcRepoStandard);
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(McRepoStandard mcRepoStandard) {
		try {
			// 更新主表
			super.insertOrUpdate(mcRepoStandard);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}
	
	
	
}
