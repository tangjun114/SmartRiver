package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProDeviceAbnormalMapper;
import cn.jeeweb.modules.supervise.entity.McProDeviceAbnormal;
import cn.jeeweb.modules.supervise.service.IMcProDeviceAbnormalService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 设备异常状况
 * @Description: 设备异常状况
 * @author Aether
 * @date 2018-04-19 21:46:24
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProDeviceAbnormalService")
public class McProDeviceAbnormalServiceImpl  extends CommonServiceImpl<McProDeviceAbnormalMapper,McProDeviceAbnormal> implements  IMcProDeviceAbnormalService {

}
