package cn.jeeweb.modules.flood.controller;


import cn.jeeweb.core.model.PageJson;
import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.data.PropertyPreFilterable;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.data.Sort;
import cn.jeeweb.core.query.utils.QueryableConvertUtils;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.ReflectionUtils;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.flood.entity.FloodDestoryReport;
import cn.jeeweb.modules.flood.entity.FloodRainfull;
import cn.jeeweb.modules.flood.service.IFloodDestoryReportService;
import cn.jeeweb.modules.sys.entity.Dict;
import cn.jeeweb.modules.sys.utils.DictUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.ff.bi.model.RptDimDataJson;
import com.ff.common.util.format.DateUtil;
import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.util.meta.ReflectionUtil;
import com.ff.common.util.validate.ValidatorUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;
import net.sf.ehcache.search.aggregator.Sum;
import org.apache.http.util.EntityUtils;
import org.jfree.data.time.Year;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.FloodDestory;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author wsh
 * @version V1.0
 * @Title: 水毁信息表
 * @Description: 水毁信息表
 * @date 2018-11-28 20:21:15
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/flooddestory")
@RequiresPathPermission("flood:flooddestory")
public class FloodDestoryController extends BaseCRUDController<FloodDestory, String> {
    @Autowired
    IFloodDestoryReportService floodDestoryReportService;

    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        model.addAttribute("id", id);
    }


    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<FloodDestory> entityWrapper, HttpServletRequest request, HttpServletResponse response) {

        super.preAjaxList(queryable, entityWrapper, request, response);
        String id = request.getParameter("id");
        if (StringUtils.isNotEmpty(id)) {
            queryable.addCondition("id", id);
        }
    }

    @Override
    public BaseRspJson<List<FloodDestory>> LoadAll(@RequestBody BaseReqJson request) {
        BaseRspJson<List<FloodDestory>> rsp = new BaseRspJson<List<FloodDestory>>();
        Queryable queryable = this.GetFilterCondition(request);
        List<FloodDestory> obj = this.commonService.listWithNoPage(queryable);
        List<Dict> list = DictUtils.getDictList("flood_destory_degree");
        Map<String, Dict> map = DataFilterUtil.buildMap("value", list);
        for (FloodDestory temp : obj) {
            Dict tempDict = map.get(temp.getGrade());
            if (null != tempDict) {
                temp.setGrade(tempDict.getLabel());
            }
        }
        rsp.setObj(obj);
        return rsp;
    }



    @Override
    public void afterSave(FloodDestory entity, HttpServletRequest request, HttpServletResponse response) {
        super.afterSave(entity, request, response);
        FloodDestoryReport report = new FloodDestoryReport();
        BeanUtils.copyProperties(entity, report);
        report.setDay(DateUtil.getDayOfTime(entity.getRecordTime()));
        report.setMonth(DateUtil.getMonthOfTime(entity.getRecordTime()));
        report.setYear(DateUtil.getYearOfTime(entity.getRecordTime()));
        floodDestoryReportService.insert(report);
    }


}
