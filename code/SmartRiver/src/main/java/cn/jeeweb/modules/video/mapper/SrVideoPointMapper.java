package cn.jeeweb.modules.video.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.video.entity.SrVideoPoint;
 
/**   
 * @Title: 视频站点管理数据库控制层接口
 * @Description: 视频站点管理数据库控制层接口
 * @author wsh
 * @date 2019-04-01 14:30:31
 * @version V1.0   
 *
 */
public interface SrVideoPointMapper extends BaseMapper<SrVideoPoint> {
    
}