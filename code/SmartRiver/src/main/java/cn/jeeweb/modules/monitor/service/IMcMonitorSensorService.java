package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McMonitorSensor;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;

/**   
 * @Title: 传感器信息
 * @Description: 传感器信息
 * @author shawloong
 * @date 2017-10-13 00:07:04
 * @version V1.0   
 *
 */
public interface IMcMonitorSensorService extends ICommonService<McMonitorSensor> {

	public void setPointInfo(McProItemMeasuringPoint point);
	public void clearPointInfo(McProItemMeasuringPoint point);
}

