package cn.jeeweb.modules.supervise.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;
import cn.jeeweb.modules.supervise.mapper.McProItemMonitorItemMapper;
import cn.jeeweb.modules.supervise.service.IMcProItemMeasuringPointService;
import cn.jeeweb.modules.supervise.service.IMcProItemMonitorItemService;

/**   
 * @Title: 项目监控-测点设置-监测项
 * @Description: 项目监控-测点设置-监测项
 * @author shawloong
 * @date 2017-11-05 19:41:39
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemMonitorItemService")
public class McProItemMonitorItemServiceImpl  extends CommonServiceImpl<McProItemMonitorItemMapper,McProItemMonitorItem> implements  IMcProItemMonitorItemService {

	@Autowired IMcProItemMeasuringPointService pointService;
	public void refresh(String id)
	{
		
		EntityWrapper<McProItemMeasuringPoint> wrapper = new EntityWrapper<McProItemMeasuringPoint>(McProItemMeasuringPoint.class);
		wrapper.eq("monitor_item_id", id);
		int count = pointService.selectCount(wrapper );
		McProItemMonitorItem entity = this.selectById(id);
		entity.setPointCount(count);
		this.updateAllColumnById(entity);
		
	}
	@Override
	public boolean deleteById(Serializable id) {
		// TODO Auto-generated method stub
		EntityWrapper<McProItemMeasuringPoint> wrapper = new EntityWrapper<McProItemMeasuringPoint>(McProItemMeasuringPoint.class);
		wrapper.eq("monitor_item_id", id);
		List<McProItemMeasuringPoint> dataList = pointService.selectList(wrapper);
		for(McProItemMeasuringPoint point: dataList)
		{
			pointService.deleteById(point.getId());
		}
		return super.deleteById(id);
	}
	@Override
	public boolean deleteBatchIds(List<? extends Serializable> idList) {
		// TODO Auto-generated method stub
		
		EntityWrapper<McProItemMeasuringPoint> wrapper = new EntityWrapper<McProItemMeasuringPoint>(McProItemMeasuringPoint.class);
		wrapper.in("monitor_item_id", idList);
		List<McProItemMeasuringPoint> dataList = pointService.selectList(wrapper);
		for(McProItemMeasuringPoint point: dataList)
		{
			pointService.deleteById(point.getId());
		}
		
		return super.deleteBatchIds(idList);
	}
	
}
