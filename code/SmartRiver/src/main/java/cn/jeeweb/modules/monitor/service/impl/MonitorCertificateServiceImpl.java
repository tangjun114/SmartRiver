package cn.jeeweb.modules.monitor.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.monitor.mapper.MonitorCertificateMapper;
import cn.jeeweb.modules.monitor.entity.MonitorCertificate;
import cn.jeeweb.modules.monitor.service.IMonitorCertificateService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 机构证书管理
 * @Description: 机构证书管理
 * @author shawloong
 * @date 2017-10-05 21:31:07
 * @version V1.0   
 *
 */
@Transactional
@Service("monitorCertificateService")
public class MonitorCertificateServiceImpl  extends CommonServiceImpl<MonitorCertificateMapper,MonitorCertificate> implements  IMonitorCertificateService {

}
