package cn.jeeweb.modules.supervise.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.util.validate.ValidatorUtil;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.supervise.entity.McProItemMessage;
import cn.jeeweb.modules.supervise.entity.McProReport;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 报警短信
 * @Description: 报警短信
 * @author jrrey
 * @date 2018-05-30 16:34:13
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemmessage")
@RequiresPathPermission("supervise:mcproitemmessage")
public class McProItemMessageController extends McProjectBaseController<McProItemMessage, String> {

	@Autowired
	IMcProjectService iMcProjectService;

	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProItemMessage> entityWrapper, HttpServletRequest request,
			HttpServletResponse response) {
		String  projectId = request.getParameter("projectId");
		String  isMy = request.getParameter("isMy");

		// TODO Auto-generated method stub
		if(!ValidatorUtil.isEmpty(isMy))
		{
			entityWrapper.like("user_id", UserUtils.getUser().getId());
		}
		else
		{	
			if(StringUtils.isEmpty(projectId)){
				User user = UserUtils.getUser();
				entityWrapper.in("projectId", iMcProjectService.getProjectIdListByUser(user));
	  			
			}
		}
		super.preAjaxList(queryable, entityWrapper, request, response);
 	}
}
