package cn.jeeweb.modules.repo.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.repo.entity.McRepoMaterial;

/**   
 * @Title: 资料下载
 * @Description: 资料下载
 * @author shawloong
 * @date 2017-10-04 13:26:47
 * @version V1.0   
 *
 */
public interface IMcRepoMaterialService extends ICommonService<McRepoMaterial> {

}

