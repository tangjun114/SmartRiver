package cn.jeeweb.modules.monitor.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.monitor.mapper.McMonitorCertificateMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorCertificate;
import cn.jeeweb.modules.monitor.service.IMcMonitorCertificateService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 机构证书管理
 * @Description: 机构证书管理
 * @author shawloong
 * @date 2017-10-05 22:28:45
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorCertificateService")
public class McMonitorCertificateServiceImpl  extends CommonServiceImpl<McMonitorCertificateMapper,McMonitorCertificate> implements  IMcMonitorCertificateService {

}
