package cn.jeeweb.modules.sys.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Condition.Filter;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.sys.entity.Dict;
import cn.jeeweb.modules.sys.entity.DictGroup;
import cn.jeeweb.modules.sys.service.IDictGroupService;
import cn.jeeweb.modules.sys.utils.DictUtils;

@Controller
@RequestMapping("${admin.url.prefix}/sys/dict")
@RequiresPathPermission("sys:user")
public class DictController extends BaseCRUDController<Dict, String> {
	@Autowired
	private IDictGroupService dictGroupService;

	@Override
	public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
		String gid = request.getParameter("gid");
		DictGroup group = dictGroupService.selectById(gid);
		model.addAttribute("group", group);
	}

	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<Dict> entityWrapper, HttpServletRequest request,
			HttpServletResponse response) {
		String gid = request.getParameter("gid");
		queryable.addCondition("gid", gid);
	}

	@Override
	public void preEdit(Dict entity, Model model, HttpServletRequest request, HttpServletResponse response) {
		String gid = request.getParameter("gid");
		model.addAttribute("gid", gid);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/loadbygroup")
	@ResponseBody
	public BaseRspJson<List<Dict>> loadbygroup(@RequestBody BaseReqJson req,HttpServletRequest request)
	{
		BaseRspJson<List<Dict>> rsp = new BaseRspJson<List<Dict>>();
		
		//Queryable queryable = this.GetFilterCondition(req);
		//String group = request.getParameter("group");
		String group ="";
		List<Filter> filterList = req.getFilter();
		for(Filter f : filterList){
			if("group".equals(f.getProperty())){
				group = (String) f.getValue();
				break;
			}
		}
			
		List<Dict> obj = DictUtils.getDictList(group);
		rsp.setObj(obj);
  		return rsp;
	}
}
