package cn.jeeweb.modules.project.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectQualityIndicator;

/**   
 * @Title: 项目质量指标
 * @Description: 项目质量指标
 * @author jerry
 * @date 2018-11-14 17:57:30
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectqualityindicator")
@RequiresPathPermission("project:srprojectqualityindicator")
public class SrProjectQualityIndicatorController extends BaseCRUDController<SrProjectQualityIndicator, String> {

}
