package cn.jeeweb.modules.supervise.service;

import java.util.List;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 项目管理
 * @Description: 项目管理
 * @author shawloong
 * @date 2017-11-21 16:31:35
 * @version V1.0   
 *
 */
public interface IMcProjectService extends ICommonService<McProject> {

	public Condition getProjectContiondByUser(User user);
	public List<?> getProjectIdListByUser(User user);
	
	/**
	 * 更新预警、告警、超控测点数
	 * @param projectId
	 */
	public void updateCount(String projectId, String alarmTypeId);
}

