package cn.jeeweb.modules.supervise.service;

import org.apache.poi.ss.usermodel.Workbook;

import cn.jeeweb.modules.supervise.entity.McProReport;

public interface IExcelReportService
{
	public void createExcel();
	public String getBasePath();
	public String getTemplate();
	public String getOutFile();
	public void setData(McProReport report);
	public void createIndex(Workbook workBook);
	public void createTitlePage(Workbook workBook);
	public void createCatalog(Workbook workBook);
	public void createMain(Workbook workBook);
	public void createContent(Workbook workBook);
}
