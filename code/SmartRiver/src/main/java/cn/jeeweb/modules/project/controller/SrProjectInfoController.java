package cn.jeeweb.modules.project.controller;


import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.project.entity.SrProjectInfo;
import cn.jeeweb.modules.project.entity.SrProjectInfoChangeLog;
import cn.jeeweb.modules.project.service.ISrProjectInfoChangeLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;

/**
 * @author jerry
 * @version V1.0
 * @Title: 项目信息
 * @Description: 项目信息
 * @date 2018-11-13 21:30:12
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectinfo")
@RequiresPathPermission("project:srprojectinfo")
public class SrProjectInfoController extends BaseCRUDController<SrProjectInfo, String> {

    @Autowired
    ISrProjectInfoChangeLogService srProjectInfoChangeLogService;


    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        super.preList(model, request, response);
        String status = request.getParameter("status");
        if (StringUtils.isNotEmpty(status)) {
            model.addAttribute("status", status);
        }
    }


    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<SrProjectInfo> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
        super.preAjaxList(queryable, entityWrapper, request, response);
        String status = request.getParameter("status");
        if (StringUtils.isNotEmpty(status)) {
            queryable.addCondition("status", status);
        }
        super.preAjaxList(queryable, entityWrapper, request, response);
    }

    @Override
    public void afterSave(SrProjectInfo entity, HttpServletRequest request, HttpServletResponse response) {
        super.afterSave(entity, request, response);
        SrProjectInfoChangeLog log = new SrProjectInfoChangeLog();

        org.springframework.beans.BeanUtils.copyProperties(entity, log);
        srProjectInfoChangeLogService.insert(log);
    }

}
