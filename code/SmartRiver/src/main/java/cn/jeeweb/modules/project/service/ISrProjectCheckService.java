package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectCheck;

/**   
 * @Title: 项目检查
 * @Description: 项目检查
 * @author wsh
 * @date 2019-03-31 16:40:32
 * @version V1.0   
 *
 */
public interface ISrProjectCheckService extends ICommonService<SrProjectCheck> {

}

