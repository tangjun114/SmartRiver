package cn.jeeweb.modules.water.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.water.entity.WaterDetectStation;

/**   
 * @Title: 监测站管理
 * @Description: 监测站管理
 * @author liyonglei
 * @date 2018-11-28 19:41:24
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/water/waterdetectstation")
@RequiresPathPermission("water:waterdetectstation")
public class WaterDetectStationController extends BaseCRUDController<WaterDetectStation, String> {

}
