package cn.jeeweb.modules.monitor.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.util.meta.ReflectionUtil;
import com.ff.common.util.validate.ValidatorUtil;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McMonitorSensor;
import cn.jeeweb.modules.supervise.controller.McProjectBaseController;
import cn.jeeweb.modules.supervise.entity.McProject;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 传感器信息
 * @Description: 传感器信息
 * @author shawloong
 * @date 2017-12-07 11:05:43
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/monitor/mcmonitorsensor")
@RequiresPathPermission("monitor:mcmonitorsensor")
public class McMonitorSensorController extends McOrgBaseController<McMonitorSensor, String> {
	
	@Override
	public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
	
       String sensorTypeName = request.getParameter("sensorTypeName");
       if(!StringUtils.isEmpty(sensorTypeName)){
    	   model.addAttribute("sensorTypeName", sensorTypeName);
		}
       super.preList(model, request, response);
       
	}
	
	
	@Autowired
	IMcProjectService projectService;
	
	@Override
	public void preAjaxList(Queryable queryable,EntityWrapper<McMonitorSensor> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
		String  projectId = request.getParameter("projectId");
		if(!StringUtils.isEmpty(projectId)){
			entityWrapper.eq("projectId", projectId);
 			 
		}
		else
		{
			User user = UserUtils.getUser();
			 
			String val = (String) queryable.getValue("monitorId");
			if(ValidatorUtil.isEmpty(val))
			{
				entityWrapper.eq("monitorId", user.getOrganizationIds());
	 		}
		}
		
		
		String  sensorTypeName = request.getParameter("sensorTypeName");
		if(!StringUtils.isEmpty(sensorTypeName)){
			entityWrapper.eq("sensorTypeName", sensorTypeName);
		}
		
 
	}
	
	@Override
	public void preSave(McMonitorSensor entity, HttpServletRequest request, HttpServletResponse response) {
		String projectId = request.getParameter("projectId");
		
		McProject project = projectService.selectById(projectId);
		if (null != project) {
			ReflectionUtil.setObjectField(entity, "projectId", project.getId());
			ReflectionUtil.setObjectField(entity, "projectName", project.getName());

		}
		super.preSave(entity, request, response);
	}
 
 
}
