package cn.jeeweb.modules.supervise.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.DataEntity;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 项目监控-实施日志
 * @Description: 项目监控-实施日志
 * @author shawloong
 * @date 2017-11-15 00:48:21
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_implement_log")
@SuppressWarnings("serial")
public class McProItemImplementLog extends DataEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
 
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**日志类型*/
    @TableField(value = "implement_log_type")
	private String implementLogType;
    /**可见类型*/
    @TableField(value = "visible_type")
	private String visibleType;
    /**附件*/
    @TableField(value = "file_path")
	private String filePath;
    /**附件名称*/
    @TableField(value = "file_name")
	private String fileName;
    /**图片*/
    @TableField(value = "img_path")
	private String imgPath;
    /**图片名称*/
    @TableField(value = "img_name")
	private String imgName;
    /**内容*/
    @TableField(value = "content")
	private String content;
    /**工况编号*/
    @TableField(value = "serial")
	private String serial;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  implementLogType
	 *@return: String  日志类型
	 */
	public String getImplementLogType(){
		return this.implementLogType;
	}

	/**
	 * 设置  implementLogType
	 *@param: implementLogType  日志类型
	 */
	public void setImplementLogType(String implementLogType){
		this.implementLogType = implementLogType;
	}
	/**
	 * 获取  visibleType
	 *@return: String  可见类型
	 */
	public String getVisibleType(){
		return this.visibleType;
	}

	/**
	 * 设置  visibleType
	 *@param: visibleType  可见类型
	 */
	public void setVisibleType(String visibleType){
		this.visibleType = visibleType;
	}
	/**
	 * 获取  filePath
	 *@return: String  附件
	 */
	public String getFilePath(){
		return this.filePath;
	}

	/**
	 * 设置  filePath
	 *@param: filePath  附件
	 */
	public void setFilePath(String filePath){
		this.filePath = filePath;
	}
	/**
	 * 获取  fileName
	 *@return: String  附件名称
	 */
	public String getFileName(){
		return this.fileName;
	}

	/**
	 * 设置  fileName
	 *@param: fileName  附件名称
	 */
	public void setFileName(String fileName){
		this.fileName = fileName;
	}
	/**
	 * 获取  imgPath
	 *@return: String  图片
	 */
	public String getImgPath(){
		return this.imgPath;
	}

	/**
	 * 设置  imgPath
	 *@param: imgPath  图片
	 */
	public void setImgPath(String imgPath){
		this.imgPath = imgPath;
	}
	/**
	 * 获取  imgName
	 *@return: String  图片名称
	 */
	public String getImgName(){
		return this.imgName;
	}

	/**
	 * 设置  imgName
	 *@param: imgName  图片名称
	 */
	public void setImgName(String imgName){
		this.imgName = imgName;
	}
	/**
	 * 获取  content
	 *@return: String  内容
	 */
	public String getContent(){
		return this.content;
	}

	/**
	 * 设置  content
	 *@param: content  内容
	 */
	public void setContent(String content){
		this.content = content;
	}
	/**
	 * 获取  serial
	 *@return: String  工况编号
	 */
	public String getSerial(){
		return this.serial;
	}

	/**
	 * 设置  serial
	 *@param: serial  工况编号
	 */
	public void setSerial(String serial){
		this.serial = serial;
	}
	
}
