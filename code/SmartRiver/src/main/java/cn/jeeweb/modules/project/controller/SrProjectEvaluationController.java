package cn.jeeweb.modules.project.controller;


import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.project.entity.SrProjectAccept;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectEvaluation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wsh
 * @version V1.0
 * @Title: 项目评比
 * @Description: 项目评比
 * @date 2019-03-31 22:54:37
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectevaluation")
@RequiresPathPermission("project:srprojectevaluation")
public class SrProjectEvaluationController extends BaseCRUDController<SrProjectEvaluation, String> {
    @Override
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {
        super.preList(model, request, response);
        String proId = request.getParameter("proId");
        if (StringUtils.isNotEmpty(proId)) {
            model.addAttribute("proId", proId);
        }
    }


    @Override
    public void preAjaxList(Queryable queryable, EntityWrapper<SrProjectEvaluation> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
        super.preAjaxList(queryable, entityWrapper, request, response);
        String proId = request.getParameter("proId");
        if (StringUtils.isNotEmpty(proId)) {
            queryable.addCondition("proId", proId);
        }
        entityWrapper.orderBy("create_date", false);
    }
}
