package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemMeasuringHoleMapper;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringHole;
import cn.jeeweb.modules.supervise.service.IMcProItemMeasuringHoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目监控-测点设置-监测项-测孔
 * @Description: 项目监控-测点设置-监测项-测孔
 * @author shawloong
 * @date 2017-11-15 23:22:45
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemMeasuringHoleService")
public class McProItemMeasuringHoleServiceImpl  extends CommonServiceImpl<McProItemMeasuringHoleMapper,McProItemMeasuringHole> implements  IMcProItemMeasuringHoleService {

}
