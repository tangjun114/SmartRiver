package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McMonitorCollection;

/**   
 * @Title: 通信设备
 * @Description: 通信设备
 * @author shawloong
 * @date 2018-01-28 21:44:38
 * @version V1.0   
 *
 */
public interface IMcMonitorCollectionService extends ICommonService<McMonitorCollection> {

}

