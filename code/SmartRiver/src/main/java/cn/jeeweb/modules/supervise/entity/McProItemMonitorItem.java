package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 项目监控-测点设置-监测项
 * @Description: 项目监控-测点设置-监测项
 * @author shawloong
 * @date 2017-12-21 17:15:18
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_monitor_item")
@SuppressWarnings("serial")
public class McProItemMonitorItem extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**监测项名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**监测分区ID*/
    @TableField(value = "subarea_id")
	private String subareaId;
    /**监测分区*/
    @TableField(value = "subarea_name")
	private String subareaName;
    /**是否为自动化*/
    @TableField(value = "is_auto")
	private String isAuto;
    /**监测组类别Id*/
    @TableField(value = "group_type_id")
	private String groupTypeId;
    /**监测组类别*/
    @TableField(value = "group_type_name")
	private String groupTypeName;
    /**依据规范Id*/
    @TableField(value = "standard_id")
	private String standardId;
    /**依据规范*/
    @TableField(value = "standard_name")
	private String standardName;
    /**观测方法Id*/
    @TableField(value = "observation_id")
	private String observationId;
    /**观测方法名称*/
    @TableField(value = "observation_name")
	private String observationName;
    /**监测工作位置*/
    @TableField(value = "working_position")
	private String workingPosition;
    /**累计值报警值统计*/
    @TableField(value = "total_alarm_value")
	private String totalAlarmValue;
    /**变化速率报警值统计*/
    @TableField(value = "rate_change_alarm_value")
	private String rateChangeAlarmValue;
    /**累计值控制值统计*/
    @TableField(value = "total_control_value")
	private String totalControlValue;
    /**变化速率控制值统计*/
    @TableField(value = "rate_contro_value")
	private String rateChangeControlValue;
    /**黄色预警值*/
    @TableField(value = "yellow_warning_value")
	private String yellowWarningValue;
    /**设计数量*/
    @TableField(value = "design_quantity")
	private String designQuantity;
    /**监测项类型编码*/
    @TableField(value = "group_type_code")
	private String groupTypeCode;
    /**监测仪器类型Id*/
    @TableField(value = "device_type_id")
	private String deviceTypeId;
    /**监测仪器类型名称*/
    @TableField(value = "device_type_name")
	private String deviceTypeName;
    /**监测仪器型号Id*/
    @TableField(value = "device_model_id")
	private String deviceModelId;
    /**监测仪器型号名称*/
    @TableField(value = "device_model_name")
	private String deviceModelName;
    /**监测次数*/
    @TableField(value = "monitor_count")
	private String monitorCount;
    /**自动化采集参数*/
    @TableField(value = "auto_para")
	private String autoPara;
    /**启动时间（时+分）*/
    @TableField(value = "auto_start_time")
	private String autoStartTime;
    /**启动开关（开启/关闭）*/
    @TableField(value = "auto_switch")
	private String autoSwitch;
    /**是否发生异常短信*/
    @TableField(value = "auto_send_message")
	private String autoSendMessage;
    
    
    /**测点的数量*/
    @TableField(value = "point_count")
	private Integer pointCount = 0;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  subareaId
	 *@return: String  监测分区ID
	 */
	public String getSubareaId(){
		return this.subareaId;
	}

	/**
	 * 设置  subareaId
	 *@param: subareaId  监测分区ID
	 */
	public void setSubareaId(String subareaId){
		this.subareaId = subareaId;
	}
	/**
	 * 获取  subareaName
	 *@return: String  监测分区
	 */
	public String getSubareaName(){
		return this.subareaName;
	}

	/**
	 * 设置  subareaName
	 *@param: subareaName  监测分区
	 */
	public void setSubareaName(String subareaName){
		this.subareaName = subareaName;
	}
	/**
	 * 获取  isAuto
	 *@return: String  是否为自动化
	 */
	public String getIsAuto(){
		return this.isAuto;
	}

	/**
	 * 设置  isAuto
	 *@param: isAuto  是否为自动化
	 */
	public void setIsAuto(String isAuto){
		this.isAuto = isAuto;
	}
	/**
	 * 获取  groupTypeId
	 *@return: String  监测组类别Id
	 */
	public String getGroupTypeId(){
		return this.groupTypeId;
	}

	/**
	 * 设置  groupTypeId
	 *@param: groupTypeId  监测组类别Id
	 */
	public void setGroupTypeId(String groupTypeId){
		this.groupTypeId = groupTypeId;
	}
	/**
	 * 获取  groupTypeName
	 *@return: String  监测组类别
	 */
	public String getGroupTypeName(){
		return this.groupTypeName;
	}

	/**
	 * 设置  groupTypeName
	 *@param: groupTypeName  监测组类别
	 */
	public void setGroupTypeName(String groupTypeName){
		this.groupTypeName = groupTypeName;
	}
	/**
	 * 获取  standardId
	 *@return: String  依据规范Id
	 */
	public String getStandardId(){
		return this.standardId;
	}

	/**
	 * 设置  standardId
	 *@param: standardId  依据规范Id
	 */
	public void setStandardId(String standardId){
		this.standardId = standardId;
	}
	/**
	 * 获取  standardName
	 *@return: String  依据规范
	 */
	public String getStandardName(){
		return this.standardName;
	}

	/**
	 * 设置  standardName
	 *@param: standardName  依据规范
	 */
	public void setStandardName(String standardName){
		this.standardName = standardName;
	}
	/**
	 * 获取  observationId
	 *@return: String  观测方法Id
	 */
	public String getObservationId(){
		return this.observationId;
	}

	/**
	 * 设置  observationId
	 *@param: observationId  观测方法Id
	 */
	public void setObservationId(String observationId){
		this.observationId = observationId;
	}
	/**
	 * 获取  observationName
	 *@return: String  观测方法名称
	 */
	public String getObservationName(){
		return this.observationName;
	}

	/**
	 * 设置  observationName
	 *@param: observationName  观测方法名称
	 */
	public void setObservationName(String observationName){
		this.observationName = observationName;
	}
	/**
	 * 获取  workingPosition
	 *@return: String  监测工作位置
	 */
	public String getWorkingPosition(){
		return this.workingPosition;
	}

	/**
	 * 设置  workingPosition
	 *@param: workingPosition  监测工作位置
	 */
	public void setWorkingPosition(String workingPosition){
		this.workingPosition = workingPosition;
	}
	/**
	 * 获取  totalAlarmValue
	 *@return: String  累计值报警值统计
	 */
	public String getTotalAlarmValue(){
		return this.totalAlarmValue;
	}

	/**
	 * 设置  totalAlarmValue
	 *@param: totalAlarmValue  累计值报警值统计
	 */
	public void setTotalAlarmValue(String totalAlarmValue){
		this.totalAlarmValue = totalAlarmValue;
	}
	/**
	 * 获取  rateChangeAlarmValue
	 *@return: String  变化速率报警值统计
	 */
	public String getRateChangeAlarmValue(){
		return this.rateChangeAlarmValue;
	}

	/**
	 * 设置  rateChangeAlarmValue
	 *@param: rateChangeAlarmValue  变化速率报警值统计
	 */
	public void setRateChangeAlarmValue(String rateChangeAlarmValue){
		this.rateChangeAlarmValue = rateChangeAlarmValue;
	}
	/**
	 * 获取  totalControlValue
	 *@return: String  累计值控制值统计
	 */
	public String getTotalControlValue(){
		return this.totalControlValue;
	}

	/**
	 * 设置  totalControlValue
	 *@param: totalControlValue  累计值控制值统计
	 */
	public void setTotalControlValue(String totalControlValue){
		this.totalControlValue = totalControlValue;
	}
	/**
	 * 获取  rateChangeControlValue
	 *@return: String  变化速率控制值统计
	 */
	public String getRateChangeControlValue(){
		return this.rateChangeControlValue;
	}

	/**
	 * 设置  rateChangeControlValue
	 *@param: rateChangeControlValue  变化速率控制值统计
	 */
	public void setRateChangeControlValue(String rateChangeControlValue){
		this.rateChangeControlValue = rateChangeControlValue;
	}
	/**
	 * 获取  yellowWarningValue
	 *@return: String  黄色预警值
	 */
	public String getYellowWarningValue(){
		return this.yellowWarningValue;
	}

	/**
	 * 设置  yellowWarningValue
	 *@param: yellowWarningValue  黄色预警值
	 */
	public void setYellowWarningValue(String yellowWarningValue){
		this.yellowWarningValue = yellowWarningValue;
	}
	/**
	 * 获取  designQuantity
	 *@return: String  设计数量
	 */
	public String getDesignQuantity(){
		return this.designQuantity;
	}

	/**
	 * 设置  designQuantity
	 *@param: designQuantity  设计数量
	 */
	public void setDesignQuantity(String designQuantity){
		this.designQuantity = designQuantity;
	}
	/**
	 * 获取  groupTypeCode
	 *@return: String  监测项类型编码
	 */
	public String getGroupTypeCode(){
		return this.groupTypeCode;
	}

	/**
	 * 设置  groupTypeCode
	 *@param: groupTypeCode  监测项类型编码
	 */
	public void setGroupTypeCode(String groupTypeCode){
		this.groupTypeCode = groupTypeCode;
	}
	/**
	 * 获取  deviceTypeId
	 *@return: String  监测仪器类型Id
	 */
	public String getDeviceTypeId(){
		return this.deviceTypeId;
	}

	/**
	 * 设置  deviceTypeId
	 *@param: deviceTypeId  监测仪器类型Id
	 */
	public void setDeviceTypeId(String deviceTypeId){
		this.deviceTypeId = deviceTypeId;
	}
	/**
	 * 获取  deviceTypeName
	 *@return: String  监测仪器类型名称
	 */
	public String getDeviceTypeName(){
		return this.deviceTypeName;
	}

	/**
	 * 设置  deviceTypeName
	 *@param: deviceTypeName  监测仪器类型名称
	 */
	public void setDeviceTypeName(String deviceTypeName){
		this.deviceTypeName = deviceTypeName;
	}
	/**
	 * 获取  deviceModelId
	 *@return: String  监测仪器型号Id
	 */
	public String getDeviceModelId(){
		return this.deviceModelId;
	}

	/**
	 * 设置  deviceModelId
	 *@param: deviceModelId  监测仪器型号Id
	 */
	public void setDeviceModelId(String deviceModelId){
		this.deviceModelId = deviceModelId;
	}
	/**
	 * 获取  deviceModelName
	 *@return: String  监测仪器型号名称
	 */
	public String getDeviceModelName(){
		return this.deviceModelName;
	}

	/**
	 * 设置  deviceModelName
	 *@param: deviceModelName  监测仪器型号名称
	 */
	public void setDeviceModelName(String deviceModelName){
		this.deviceModelName = deviceModelName;
	}
	/**
	 * 获取  monitorCount
	 *@return: String  监测次数
	 */
	public String getMonitorCount(){
		return this.monitorCount;
	}

	/**
	 * 设置  monitorCount
	 *@param: monitorCount  监测次数
	 */
	public void setMonitorCount(String monitorCount){
		this.monitorCount = monitorCount;
	}
	/**
	 * 获取  autoPara
	 *@return: String  自动化采集参数
	 */
	public String getAutoPara(){
		return this.autoPara;
	}

	/**
	 * 设置  autoPara
	 *@param: autoPara  自动化采集参数
	 */
	public void setAutoPara(String autoPara){
		this.autoPara = autoPara;
	}
	/**
	 * 获取  autoStartTime
	 *@return: String  启动时间（时+分）
	 */
	public String getAutoStartTime(){
		return this.autoStartTime;
	}

	/**
	 * 设置  autoStartTime
	 *@param: autoStartTime  启动时间（时+分）
	 */
	public void setAutoStartTime(String autoStartTime){
		this.autoStartTime = autoStartTime;
	}
	/**
	 * 获取  autoSwitch
	 *@return: String  启动开关（开启/关闭）
	 */
	public String getAutoSwitch(){
		return this.autoSwitch;
	}

	/**
	 * 设置  autoSwitch
	 *@param: autoSwitch  启动开关（开启/关闭）
	 */
	public void setAutoSwitch(String autoSwitch){
		this.autoSwitch = autoSwitch;
	}
	/**
	 * 获取  autoSendMessage
	 *@return: String  是否发生异常短信
	 */
	public String getAutoSendMessage(){
		return this.autoSendMessage;
	}

	/**
	 * 设置  autoSendMessage
	 *@param: autoSendMessage  是否发生异常短信
	 */
	public void setAutoSendMessage(String autoSendMessage){
		this.autoSendMessage = autoSendMessage;
	}

	public Integer getPointCount() {
		return pointCount;
	}

	public void setPointCount(Integer pointCount) {
		this.pointCount = pointCount;
	}
	
	
	
}
