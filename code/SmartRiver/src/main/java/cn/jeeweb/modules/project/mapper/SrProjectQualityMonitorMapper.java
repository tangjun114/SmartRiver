package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectQualityMonitor;
 
/**   
 * @Title: 工程质量检测数据库控制层接口
 * @Description: 工程质量检测数据库控制层接口
 * @author wsh
 * @date 2018-12-13 19:29:51
 * @version V1.0   
 *
 */
public interface SrProjectQualityMonitorMapper extends BaseMapper<SrProjectQualityMonitor> {
    
}