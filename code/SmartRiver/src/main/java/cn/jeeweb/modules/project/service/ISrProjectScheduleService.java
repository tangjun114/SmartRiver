package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectSchedule;

/**   
 * @Title: 项目进度
 * @Description: 项目进度
 * @author wsh
 * @date 2018-12-10 20:41:29
 * @version V1.0   
 *
 */
public interface ISrProjectScheduleService extends ICommonService<SrProjectSchedule> {

}

