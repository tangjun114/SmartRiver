package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorOtherDevice;
 
/**   
 * @Title: 其他设备数据库控制层接口
 * @Description: 其他设备数据库控制层接口
 * @author jerry
 * @date 2018-01-26 15:29:54
 * @version V1.0   
 *
 */
public interface McMonitorOtherDeviceMapper extends BaseMapper<McMonitorOtherDevice> {
    
}