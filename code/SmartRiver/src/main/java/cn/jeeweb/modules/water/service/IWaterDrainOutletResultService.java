package cn.jeeweb.modules.water.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.water.entity.WaterDrainOutletResult;

/**   
 * @Title: 排污口水质监控
 * @Description: 排污口水质监控
 * @author liyonglei
 * @date 2018-11-14 19:41:31
 * @version V1.0   
 *
 */
public interface IWaterDrainOutletResultService extends ICommonService<WaterDrainOutletResult> {

}

