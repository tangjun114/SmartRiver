package cn.jeeweb.modules.law.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.law.entity.LawCheckPlan;

/**   
 * @Title: 巡检计划
 * @Description: 巡检计划
 * @author liyonglei
 * @date 2018-11-13 20:27:22
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/law/lawcheckplan")
@RequiresPathPermission("law:lawcheckplan")
public class LawCheckPlanController extends BaseCRUDController<LawCheckPlan, String> {

}
