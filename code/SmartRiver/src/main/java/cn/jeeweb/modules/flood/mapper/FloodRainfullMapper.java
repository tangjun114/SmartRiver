package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodRainfull;
 
/**   
 * @Title: 雨量监测数据库控制层接口
 * @Description: 雨量监测数据库控制层接口
 * @author wsh
 * @date 2018-11-27 18:15:03
 * @version V1.0   
 *
 */
public interface FloodRainfullMapper extends BaseMapper<FloodRainfull> {
    
}