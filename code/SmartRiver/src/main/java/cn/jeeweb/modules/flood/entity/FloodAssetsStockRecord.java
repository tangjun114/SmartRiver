package cn.jeeweb.modules.flood.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;

import java.math.BigDecimal;
import java.util.Date;

/**   
 * @Title: 库存统计查询
 * @Description: 库存统计查询
 * @author liyonglei
 * @date 2018-11-23 15:27:46
 * @version V1.0   
 *
 */
@TableName("sr_flood_assets_stock_record")
@SuppressWarnings("serial")
public class FloodAssetsStockRecord extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by")
	private String createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by")
	private String updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**资产代码*/
    @TableField(value = "assets_code")
	private String assetsCode;
    /**资产名称*/
    @TableField(value = "assets_name")
	private String assetsName;
    /**资产类别代码*/
    @TableField(value = "assets_type_code")
	private String assetsTypeCode;
    /**资产类别名称*/
    @TableField(value = "assets_type_name")
	private String assetsTypeName;
    /**库存变化数量*/
    @TableField(value = "change_qty")
	private BigDecimal changeQty;
    /**库存变化类型名称*/
    @TableField(value = "change_type")
	private String changeType;
    /**单位*/
    @TableField(value = "unit")
	private String unit;
    /**库存点名称*/
    @TableField(value = "stock_location")
	private String stockLocation;
    /**状态*/
    @TableField(value = "status")
	private String status;
    /**申请人ID*/
    @TableField(value = "apply_user_id")
	private String applyUserId;
    /**申请人用户名*/
    @TableField(value = "apply_user_name")
	private String applyUserName;
    /**申请人真实姓名*/
    @TableField(value = "apply_user_real_name")
	private String applyUserRealName;
    /**审核人ID*/
    @TableField(value = "check_user_id")
	private String checkUserId;
    /**审核人用户名*/
    @TableField(value = "check_user_name")
	private String checkUserName;
    /**审核人真实姓名*/
    @TableField(value = "check_user_real_name")
	private String checkUserRealName;
    /**审核时间*/
    @TableField(value = "check_time")
	private Date checkTime;
    /**操作组编号*/
    @TableField(value = "group_id")
	private String groupId;

	@TableField(exist = false)
	private BigDecimal StockQty;

	@TableField(exist = false)
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getStockQty() {
		return StockQty;
	}

	public void setStockQty(BigDecimal stockQty) {
		StockQty = stockQty;
	}

	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: String  创建者
	 */
	public String getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(String createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: String  更新者
	 */
	public String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  assetsCode
	 *@return: String  资产代码
	 */
	public String getAssetsCode(){
		return this.assetsCode;
	}

	/**
	 * 设置  assetsCode
	 *@param: assetsCode  资产代码
	 */
	public void setAssetsCode(String assetsCode){
		this.assetsCode = assetsCode;
	}
	/**
	 * 获取  assetsName
	 *@return: String  资产名称
	 */
	public String getAssetsName(){
		return this.assetsName;
	}

	/**
	 * 设置  assetsName
	 *@param: assetsName  资产名称
	 */
	public void setAssetsName(String assetsName){
		this.assetsName = assetsName;
	}
	/**
	 * 获取  assetsTypeCode
	 *@return: String  资产类别代码
	 */
	public String getAssetsTypeCode(){
		return this.assetsTypeCode;
	}

	/**
	 * 设置  assetsTypeCode
	 *@param: assetsTypeCode  资产类别代码
	 */
	public void setAssetsTypeCode(String assetsTypeCode){
		this.assetsTypeCode = assetsTypeCode;
	}
	/**
	 * 获取  assetsTypeName
	 *@return: String  资产类别名称
	 */
	public String getAssetsTypeName(){
		return this.assetsTypeName;
	}

	/**
	 * 设置  assetsTypeName
	 *@param: assetsTypeName  资产类别名称
	 */
	public void setAssetsTypeName(String assetsTypeName){
		this.assetsTypeName = assetsTypeName;
	}
	/**
	 * 获取  changeQty
	 *@return: Double  库存变化数量
	 */
	public BigDecimal getChangeQty(){
		return this.changeQty;
	}

	/**
	 * 设置  changeQty
	 *@param: changeQty  库存变化数量
	 */
	public void setChangeQty(BigDecimal changeQty){
		this.changeQty = changeQty;
	}
	/**
	 * 获取  changeType
	 *@return: String  库存变化类型名称
	 */
	public String getChangeType(){
		return this.changeType;
	}

	/**
	 * 设置  changeType
	 *@param: changeType  库存变化类型名称
	 */
	public void setChangeType(String changeType){
		this.changeType = changeType;
	}
	/**
	 * 获取  unit
	 *@return: String  单位
	 */
	public String getUnit(){
		return this.unit;
	}

	/**
	 * 设置  unit
	 *@param: unit  单位
	 */
	public void setUnit(String unit){
		this.unit = unit;
	}
	/**
	 * 获取  stockLocation
	 *@return: String  库存点名称
	 */
	public String getStockLocation(){
		return this.stockLocation;
	}

	/**
	 * 设置  stockLocation
	 *@param: stockLocation  库存点名称
	 */
	public void setStockLocation(String stockLocation){
		this.stockLocation = stockLocation;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  applyUserId
	 *@return: String  申请人ID
	 */
	public String getApplyUserId(){
		return this.applyUserId;
	}

	/**
	 * 设置  applyUserId
	 *@param: applyUserId  申请人ID
	 */
	public void setApplyUserId(String applyUserId){
		this.applyUserId = applyUserId;
	}
	/**
	 * 获取  applyUserName
	 *@return: String  申请人用户名
	 */
	public String getApplyUserName(){
		return this.applyUserName;
	}

	/**
	 * 设置  applyUserName
	 *@param: applyUserName  申请人用户名
	 */
	public void setApplyUserName(String applyUserName){
		this.applyUserName = applyUserName;
	}
	/**
	 * 获取  applyUserRealName
	 *@return: String  申请人真实姓名
	 */
	public String getApplyUserRealName(){
		return this.applyUserRealName;
	}

	/**
	 * 设置  applyUserRealName
	 *@param: applyUserRealName  申请人真实姓名
	 */
	public void setApplyUserRealName(String applyUserRealName){
		this.applyUserRealName = applyUserRealName;
	}
	/**
	 * 获取  checkUserId
	 *@return: String  审核人ID
	 */
	public String getCheckUserId(){
		return this.checkUserId;
	}

	/**
	 * 设置  checkUserId
	 *@param: checkUserId  审核人ID
	 */
	public void setCheckUserId(String checkUserId){
		this.checkUserId = checkUserId;
	}
	/**
	 * 获取  checkUserName
	 *@return: String  审核人用户名
	 */
	public String getCheckUserName(){
		return this.checkUserName;
	}

	/**
	 * 设置  checkUserName
	 *@param: checkUserName  审核人用户名
	 */
	public void setCheckUserName(String checkUserName){
		this.checkUserName = checkUserName;
	}
	/**
	 * 获取  checkUserRealName
	 *@return: String  审核人真实姓名
	 */
	public String getCheckUserRealName(){
		return this.checkUserRealName;
	}

	/**
	 * 设置  checkUserRealName
	 *@param: checkUserRealName  审核人真实姓名
	 */
	public void setCheckUserRealName(String checkUserRealName){
		this.checkUserRealName = checkUserRealName;
	}
	/**
	 * 获取  checkTime
	 *@return: Date  审核时间
	 */
	public Date getCheckTime(){
		return this.checkTime;
	}

	/**
	 * 设置  checkTime
	 *@param: checkTime  审核时间
	 */
	public void setCheckTime(Date checkTime){
		this.checkTime = checkTime;
	}
	/**
	 * 获取  groupId
	 *@return: String  操作组编号
	 */
	public String getGroupId(){
		return this.groupId;
	}

	/**
	 * 设置  groupId
	 *@param: groupId  操作组编号
	 */
	public void setGroupId(String groupId){
		this.groupId = groupId;
	}
	
}
