package cn.jeeweb.modules.sm.mapper;

import cn.jeeweb.core.common.mapper.BaseTreeMapper;
import cn.jeeweb.modules.sm.entity.McSmParamConfig;
 
/**   
 * @Title: 系统管理-参数设置数据库控制层接口
 * @Description: 系统管理-参数设置数据库控制层接口
 * @author shawloong
 * @date 2017-10-11 23:30:03
 * @version V1.0   
 *
 */
public interface McSmParamConfigMapper extends BaseTreeMapper<McSmParamConfig> {
    
}