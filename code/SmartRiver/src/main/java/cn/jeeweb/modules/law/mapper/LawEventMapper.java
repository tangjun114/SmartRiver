package cn.jeeweb.modules.law.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.law.entity.LawEvent;
 
/**   
 * @Title: 违法事件数据库控制层接口
 * @Description: 违法事件数据库控制层接口
 * @author liyonglei
 * @date 2018-11-13 08:40:53
 * @version V1.0   
 *
 */
public interface LawEventMapper extends BaseMapper<LawEvent> {
    
}