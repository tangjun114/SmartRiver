package cn.jeeweb.modules.repo.controller;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.monitor.controller.McOrgBaseController;
import cn.jeeweb.modules.repo.entity.McRepoReference;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 参考文献
 * @Description: 参考文献
 * @author shawloong
 * @date 2017-10-04 14:38:30
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/repo/mcreporeference")
@RequiresPathPermission("repo:mcreporeference")
public class McRepoReferenceController extends McOrgBaseController<McRepoReference, String> {
	
	@Override
	public void preEdit(McRepoReference mcRepoReference, Model model, HttpServletRequest request, HttpServletResponse response) {
	}
	
	/**
	 * 保存数据之前
	 * 
	 * @param entity
	 * @param request
	 * @param response
	 */
	@Override
	public void preSave(McRepoReference entity, HttpServletRequest request, HttpServletResponse response) {
		if(null==entity.getCreateBy()){
			entity.setCreateBy(UserUtils.getUser());
		}
		
		if (null==entity.getCreateByName()){
			entity.setCreateByName(UserUtils.getUser().getUsername());
		}
		if(null==entity.getCreateDate()){
			entity.setCreateDate(new Date());
		}
		super.preSave(entity, request, response);
	}
}
