package cn.jeeweb.modules.flood.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;

import java.util.Date;

/**
 * @author wsh
 * @version V1.0
 * @Title: 站点警报阈值
 * @Description: 站点警报阈值
 * @date 2018-12-02 23:09:31
 */
@TableName("sr_flood_site_warn")
@SuppressWarnings("serial")
public class FloodSiteWarn extends AbstractEntity<String> {

    /**
     * id
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    /**
     * parentId
     */
    @TableField(value = "parent_id")
    private String parentId;
    /**
     * threshold
     */
    @TableField(value = "threshold")
    private Integer threshold;
    /**
     * name
     */
    @TableField(value = "name")
    private String name;
    /**
     * 创建者
     */
    @TableField(value = "create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField(value = "update_date")
    private Date updateDate;
    /**
     * 删除标记（0：正常；1：删除）
     */
    @TableField(value = "del_flag")
    private String delFlag;
    /**
     * 备注信息
     */
    @TableField(value = "remarks")
    private String remarks;

    /**
     * 获取  id
     *
     * @return: String  id
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置  id
     *
     * @param: id  id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取  parentid
     *
     * @return: String  parentId
     */
    public String getParentId() {
        return this.parentId;
    }

    /**
     * 设置  parentid
     *
     * @param: parentid  parentId
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取  name
     *
     * @return: String  name
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置  name
     *
     * @param: name  name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取  createBy
     *
     * @return: String  创建者
     */
    public String getCreateBy() {
        return this.createBy;
    }

    /**
     * 设置  createBy
     *
     * @param: createBy  创建者
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取  createDate
     *
     * @return: Date  创建时间
     */
    public Date getCreateDate() {
        return this.createDate;
    }

    /**
     * 设置  createDate
     *
     * @param: createDate  创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取  updateBy
     *
     * @return: String  更新者
     */
    public String getUpdateBy() {
        return this.updateBy;
    }

    /**
     * 设置  updateBy
     *
     * @param: updateBy  更新者
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取  updateDate
     *
     * @return: Date  更新时间
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 设置  updateDate
     *
     * @param: updateDate  更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取  delFlag
     *
     * @return: String  删除标记（0：正常；1：删除）
     */
    public String getDelFlag() {
        return this.delFlag;
    }

    /**
     * 设置  delFlag
     *
     * @param: delFlag  删除标记（0：正常；1：删除）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 获取  remarks
     *
     * @return: String  备注信息
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置  remarks
     *
     * @param: remarks  备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }
}
