package cn.jeeweb.modules.monitor.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolLog;
 
/**   
 * @Title: 巡检日志数据库控制层接口
 * @Description: 巡检日志数据库控制层接口
 * @author shawloong
 * @date 2017-10-15 23:43:36
 * @version V1.0   
 *
 */
public interface McMonitorPatrolLogMapper extends BaseMapper<McMonitorPatrolLog> {
    
}