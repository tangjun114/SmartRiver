package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.SrFloodWarnLog;

/**   
 * @Title: 报警记录
 * @Description: 报警记录
 * @author wsh
 * @date 2019-03-17 14:20:32
 * @version V1.0   
 *
 */
public interface ISrFloodWarnLogService extends ICommonService<SrFloodWarnLog> {

}

