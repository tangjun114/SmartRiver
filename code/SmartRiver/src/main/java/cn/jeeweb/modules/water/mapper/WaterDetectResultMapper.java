package cn.jeeweb.modules.water.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.water.entity.WaterDetectResult;
 
/**   
 * @Title: 检测数据数据库控制层接口
 * @Description: 检测数据数据库控制层接口
 * @author shawloong
 * @date 2018-11-13 21:18:03
 * @version V1.0   
 *
 */
public interface WaterDetectResultMapper extends BaseMapper<WaterDetectResult> {
    
}