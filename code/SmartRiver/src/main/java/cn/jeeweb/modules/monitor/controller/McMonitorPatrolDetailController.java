package cn.jeeweb.modules.monitor.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolDetail;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolLog;
import cn.jeeweb.modules.monitor.service.IMcMonitorPatrolLogService;
import cn.jeeweb.modules.supervise.controller.McProjectBaseController;
import cn.jeeweb.modules.sys.entity.Dict;
import cn.jeeweb.modules.sys.utils.DictUtils;

/**   
 * @Title: 巡检详情
 * @Description: 巡检详情
 * @author Aether
 * @date 2018-02-03 11:28:27
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/monitor/mcmonitorpatroldetail")
@RequiresPathPermission("monitor:mcmonitorpatroldetail")
public class McMonitorPatrolDetailController extends McProjectBaseController<McMonitorPatrolDetail, String> {

	@Autowired
	IMcMonitorPatrolLogService patrolService;
	@Override
	public void preAjaxList(Queryable queryable,EntityWrapper<McMonitorPatrolDetail> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
		String  patrolId = request.getParameter("patrolId");
		//if(!StringUtils.isEmpty(patrolId))
		{
			entityWrapper.eq("patrolId", patrolId);
			//entityWrapper.orderBy("createDate", false);
		}
	}
	
	
	@Override
	public void preSave(McMonitorPatrolDetail entity, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		//super.preSave(entity, request, response);
		//String  patrolId = request.getParameter("patrolId");
		String  patrolId = entity.getPatrolId();
		entity.setPatrolId(patrolId);
		McMonitorPatrolLog log = patrolService.selectById(patrolId);
		entity.setPatrolId(patrolId);
		entity.setPatrolName(log.getName());
		
	}


	@RequestMapping("/loadSituation")
	@ResponseBody
	public BaseRspJson<List<Dict>> LoadSituation(@RequestBody BaseReqJson request)
	{
		BaseRspJson<List<Dict>> rsp = new BaseRspJson<List<Dict>>();
		//Queryable queryable = this.GetFilterCondition(request);
		List<Dict> obj = DictUtils.getDictList("patrol_situation_type");
		rsp.setObj(obj);
  		return rsp;
	}
	
}
