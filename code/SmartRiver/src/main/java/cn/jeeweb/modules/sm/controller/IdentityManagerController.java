package cn.jeeweb.modules.sm.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.sm.entity.IdentityManager;

/**   
 * @Title: 身份管理
 * @Description: 身份管理
 * @author shawloong
 * @date 2017-09-29 00:44:22
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/sm/identitymanager")
@RequiresPathPermission("sm:identitymanager")
public class IdentityManagerController extends BaseCRUDController<IdentityManager, String> {
	
	@Override
	public void preEdit(IdentityManager identityManager, Model model, HttpServletRequest request, HttpServletResponse response) {
	}
}
