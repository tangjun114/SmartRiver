package cn.jeeweb.modules.project.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.project.entity.SrProjectQualityMonitor;

/**   
 * @Title: 工程质量检测
 * @Description: 工程质量检测
 * @author wsh
 * @date 2018-12-13 19:29:51
 * @version V1.0   
 *
 */
public interface ISrProjectQualityMonitorService extends ICommonService<SrProjectQualityMonitor> {

}

