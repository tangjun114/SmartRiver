package cn.jeeweb.modules.supervise.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import cn.jeeweb.modules.supervise.entity.McProReport;

@Component
public class ReportTask {

	@Async
	public void run(McProReport report)
	{
		IExcelReportService excelReportService = ExcelReportFactory.getServiceByType();
		excelReportService.setData(report);
		excelReportService.createExcel();
	}
}
