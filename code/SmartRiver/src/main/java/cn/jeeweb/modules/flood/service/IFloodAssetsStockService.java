package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodAssetsStock;

/**   
 * @Title: 库存管理
 * @Description: 库存管理
 * @author liyonglei
 * @date 2018-11-09 16:59:23
 * @version V1.0   
 *
 */
public interface IFloodAssetsStockService extends ICommonService<FloodAssetsStock> {

}

