package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemSceneImgMapper;
import cn.jeeweb.modules.supervise.entity.McProItemSceneImg;
import cn.jeeweb.modules.supervise.service.IMcProItemSceneImgService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 现场照片
 * @Description: 现场照片
 * @author shawloong
 * @date 2017-11-02 21:37:47
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemSceneImgService")
public class McProItemSceneImgServiceImpl  extends CommonServiceImpl<McProItemSceneImgMapper,McProItemSceneImg> implements  IMcProItemSceneImgService {

}
