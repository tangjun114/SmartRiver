package cn.jeeweb.modules.monitor.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 巡检详情
 * @Description: 巡检详情
 * @author Aether
 * @date 2018-02-06 23:12:25
 * @version V1.0   
 *
 */
@TableName("mc_monitor_patrol_detail")
@SuppressWarnings("serial")
public class McMonitorPatrolDetail extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**情况类型id*/
    @TableField(value = "situation_type_id")
	private String situationTypeId;
    /**情况类型名称*/
    @TableField(value = "situation_type_name")
	private String situationTypeName;
    /**巡检项目名称*/
    @TableField(value = "detail_name")
	private String detailName;
    /**异常说明*/
    @TableField(value = "abnormal_desc")
	private String abnormalDesc;
    /**有无异常*/
    @TableField(value = "abnormal_result")
	private String abnormalResult;
    /**异常图片*/
    @TableField(value = "abnormal_imgs")
	private String abnormalImgs;
    /**项目编号*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**巡检id*/
    @TableField(value = "patrol_id")
	private String patrolId;
    /**巡检名称*/
    @TableField(value = "patrol_name")
	private String patrolName;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  situationTypeId
	 *@return: String  情况类型id
	 */
	public String getSituationTypeId(){
		return this.situationTypeId;
	}

	/**
	 * 设置  situationTypeId
	 *@param: situationTypeId  情况类型id
	 */
	public void setSituationTypeId(String situationTypeId){
		this.situationTypeId = situationTypeId;
	}
	/**
	 * 获取  situationTypeName
	 *@return: String  情况类型名称
	 */
	public String getSituationTypeName(){
		return this.situationTypeName;
	}

	/**
	 * 设置  situationTypeName
	 *@param: situationTypeName  情况类型名称
	 */
	public void setSituationTypeName(String situationTypeName){
		this.situationTypeName = situationTypeName;
	}
	/**
	 * 获取  detailName
	 *@return: String  巡检项目名称
	 */
	public String getDetailName(){
		return this.detailName;
	}

	/**
	 * 设置  detailName
	 *@param: detailName  巡检项目名称
	 */
	public void setDetailName(String detailName){
		this.detailName = detailName;
	}
	/**
	 * 获取  abnormalDesc
	 *@return: String  异常说明
	 */
	public String getAbnormalDesc(){
		return this.abnormalDesc;
	}

	/**
	 * 设置  abnormalDesc
	 *@param: abnormalDesc  异常说明
	 */
	public void setAbnormalDesc(String abnormalDesc){
		this.abnormalDesc = abnormalDesc;
	}
	/**
	 * 获取  abnormalResult
	 *@return: String  有无异常
	 */
	public String getAbnormalResult(){
		return this.abnormalResult;
	}

	/**
	 * 设置  abnormalResult
	 *@param: abnormalResult  有无异常
	 */
	public void setAbnormalResult(String abnormalResult){
		this.abnormalResult = abnormalResult;
	}
	/**
	 * 获取  abnormalImgs
	 *@return: String  异常图片
	 */
	public String getAbnormalImgs(){
		return this.abnormalImgs;
	}

	/**
	 * 设置  abnormalImgs
	 *@param: abnormalImgs  异常图片
	 */
	public void setAbnormalImgs(String abnormalImgs){
		this.abnormalImgs = abnormalImgs;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目编号
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目编号
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  patrolId
	 *@return: String  巡检id
	 */
	public String getPatrolId(){
		return this.patrolId;
	}

	/**
	 * 设置  patrolId
	 *@param: patrolId  巡检id
	 */
	public void setPatrolId(String patrolId){
		this.patrolId = patrolId;
	}
	/**
	 * 获取  patrolName
	 *@return: String  巡检名称
	 */
	public String getPatrolName(){
		return this.patrolName;
	}

	/**
	 * 设置  patrolName
	 *@param: patrolName  巡检名称
	 */
	public void setPatrolName(String patrolName){
		this.patrolName = patrolName;
	}
	
}
