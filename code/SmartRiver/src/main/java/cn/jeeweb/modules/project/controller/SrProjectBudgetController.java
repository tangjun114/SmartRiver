package cn.jeeweb.modules.project.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectBudget;

/**   
 * @Title: 项目预算
 * @Description: 项目预算
 * @author jerry
 * @date 2018-11-13 21:29:59
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectbudget")
@RequiresPathPermission("project:srprojectbudget")
public class SrProjectBudgetController extends BaseCRUDController<SrProjectBudget, String> {

}
