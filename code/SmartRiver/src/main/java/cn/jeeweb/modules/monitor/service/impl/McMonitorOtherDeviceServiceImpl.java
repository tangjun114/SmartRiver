package cn.jeeweb.modules.monitor.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.monitor.mapper.McMonitorOtherDeviceMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorOtherDevice;
import cn.jeeweb.modules.monitor.service.IMcMonitorOtherDeviceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 其他设备
 * @Description: 其他设备
 * @author jerry
 * @date 2018-01-26 15:29:54
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorOtherDeviceService")
public class McMonitorOtherDeviceServiceImpl  extends CommonServiceImpl<McMonitorOtherDeviceMapper,McMonitorOtherDevice> implements  IMcMonitorOtherDeviceService {

}
