package cn.jeeweb.modules.sm.mapper;

import cn.jeeweb.core.common.mapper.BaseTreeMapper;
import cn.jeeweb.modules.sm.entity.McSmProMenu;
 
/**   
 * @Title: 项目管理-监测菜单数据库控制层接口
 * @Description: 项目管理-监测菜单数据库控制层接口
 * @author shawloong
 * @date 2017-10-30 00:26:12
 * @version V1.0   
 *
 */
public interface McSmProMenuMapper extends BaseTreeMapper<McSmProMenu> {
    
}