package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 项目监控-监测情况-实时监测数据统计
 * @Description: 项目监控-监测情况-实时监测数据统计
 * @author shawloong
 * @date 2018-03-06 20:19:22
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_real_time_total_data")
@SuppressWarnings("serial")
public class McProItemRealTimeTotalData extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**监测项Id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**监测项类型编码*/
    @TableField(value = "monitor_item_type_code")
	private String monitorItemTypeCode;
    /**监测项类型名称*/
    @TableField(value = "monitor_item_type_name")
	private String monitorItemTypeName;
    /**测量次数*/
    @TableField(value = "count")
	private Integer count;
    /**状态*/
    @TableField(value = "status")
	private String status;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**日*/
    @TableField(value = "day")
	private String day;
    /**月*/
    @TableField(value = "month")
	private String month;
    /**年*/
    @TableField(value = "year")
	private String year;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项Id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项Id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  count
	 *@return: Integer  测量次数
	 */
	public Integer getCount(){
		return this.count;
	}

	/**
	 * 设置  count
	 *@param: count  测量次数
	 */
	public void setCount(Integer count){
		this.count = count;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  day
	 *@return: String  日
	 */
	public String getDay(){
		return this.day;
	}

	/**
	 * 设置  day
	 *@param: day  日
	 */
	public void setDay(String day){
		this.day = day;
	}
	/**
	 * 获取  month
	 *@return: String  月
	 */
	public String getMonth(){
		return this.month;
	}

	/**
	 * 设置  month
	 *@param: month  月
	 */
	public void setMonth(String month){
		this.month = month;
	}
	/**
	 * 获取  year
	 *@return: String  年
	 */
	public String getYear(){
		return this.year;
	}

	/**
	 * 设置  year
	 *@param: year  年
	 */
	public void setYear(String year){
		this.year = year;
	}

	public String getMonitorItemTypeCode() {
		return monitorItemTypeCode;
	}

	public void setMonitorItemTypeCode(String monitorItemTypeCode) {
		this.monitorItemTypeCode = monitorItemTypeCode;
	}

	public String getMonitorItemTypeName() {
		return monitorItemTypeName;
	}

	public void setMonitorItemTypeName(String monitorItemTypeName) {
		this.monitorItemTypeName = monitorItemTypeName;
	}
	
}
