package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectScheduleDetailMapper;
import cn.jeeweb.modules.project.entity.SrProjectScheduleDetail;
import cn.jeeweb.modules.project.service.ISrProjectScheduleDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 进度拆分管理
 * @Description: 进度拆分管理
 * @author wsh
 * @date 2018-12-26 23:43:05
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectScheduleDetailService")
public class SrProjectScheduleDetailServiceImpl  extends CommonServiceImpl<SrProjectScheduleDetailMapper,SrProjectScheduleDetail> implements  ISrProjectScheduleDetailService {

}
