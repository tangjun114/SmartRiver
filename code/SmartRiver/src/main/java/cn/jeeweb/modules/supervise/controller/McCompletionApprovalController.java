package cn.jeeweb.modules.supervise.controller;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McCompletionApproval;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 完工审批表
 * @Description: 完工审批表
 * @author shawloong
 * @date 2017-10-07 16:06:19
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mccompletionapproval")
@RequiresPathPermission("supervise:mccompletionapproval")
public class McCompletionApprovalController extends BaseCRUDController<McCompletionApproval, String> {
	/**
	 * 保存数据之前
	 * 
	 * @param entity
	 * @param request
	 * @param response
	 */
	@Override
	public void preSave(McCompletionApproval data, HttpServletRequest request, HttpServletResponse response) {
		if(null==data.getCreateBy()){
			data.setCreateBy(UserUtils.getUser());
		}
		if(null==data.getCreateByName()){
			data.setCreateByName(UserUtils.getUser().getRealname());
		}
		if(null==data.getCreateDate()){
			data.setCreateDate(new Date());
		}
	
	}
}
