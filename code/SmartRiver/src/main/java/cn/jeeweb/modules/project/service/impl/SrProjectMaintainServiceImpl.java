package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectMaintainMapper;
import cn.jeeweb.modules.project.entity.SrProjectMaintain;
import cn.jeeweb.modules.project.service.ISrProjectMaintainService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目维护
 * @Description: 项目维护
 * @author wsh
 * @date 2018-12-25 20:43:42
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectMaintainService")
public class SrProjectMaintainServiceImpl  extends CommonServiceImpl<SrProjectMaintainMapper,SrProjectMaintain> implements  ISrProjectMaintainService {

}
