package cn.jeeweb.modules.supervise.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProDeviceAbnormal;
import cn.jeeweb.modules.supervise.entity.McProItemAlarmModifyLog;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 报警修改日志
 * @Description: 报警修改日志
 * @author Aether
 * @date 2018-06-16 20:17:34
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemalarmmodifylog")
@RequiresPathPermission("supervise:mcproitemalarmmodifylog")
public class McProItemAlarmModifyLogController extends McProjectBaseController<McProItemAlarmModifyLog, String> {

	@Autowired
	private IMcProjectService iMcProjectService;
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProItemAlarmModifyLog> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
 		entityWrapper.in("projectId",  iMcProjectService.getProjectIdListByUser(UserUtils.getUser()));
 		super.preAjaxList(queryable, entityWrapper, request, response);
	}
}
