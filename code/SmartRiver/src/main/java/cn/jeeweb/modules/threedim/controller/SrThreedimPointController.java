package cn.jeeweb.modules.threedim.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.threedim.entity.SrThreedimPoint;

/**   
 * @Title: 实时影像地点
 * @Description: 实时影像地点
 * @author jerry
 * @date 2018-11-27 18:29:20
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/threedim/srthreedimpoint")
@RequiresPathPermission("threedim:srthreedimpoint")
public class SrThreedimPointController extends BaseCRUDController<SrThreedimPoint, String> {

}
