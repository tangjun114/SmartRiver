package cn.jeeweb.modules.flood.controller;


import cn.jeeweb.core.model.AjaxJson;
import cn.jeeweb.core.utils.ObjectUtils;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.constant.FloodStockChangeTypeEnum;
import cn.jeeweb.modules.constant.FloodStockRecordStatusEnum;
import cn.jeeweb.modules.flood.entity.FloodAssetsStockRecord;
import cn.jeeweb.modules.flood.service.IFloodAssetsStockRecordService;
import cn.jeeweb.modules.flood.service.IFloodAssetsStockService;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.utils.UserUtils;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.FloodAssetsStock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.jar.JarEntry;

/**
 * @author liyonglei
 * @version V1.0
 * @Title: 库存管理
 * @Description: 库存管理
 * @date 2018-11-09 16:59:23
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/floodassetsstock")
@RequiresPathPermission("flood:floodassetsstock")
public class FloodAssetsStockController extends BaseCRUDController<FloodAssetsStock, String> {

    @Autowired
    private IFloodAssetsStockRecordService assetsStockRecordService;

    @Override
    public AjaxJson create(Model model, FloodAssetsStock entity, BindingResult result, HttpServletRequest request, HttpServletResponse response) {
        AjaxJson ajaxJson = new AjaxJson();
        ajaxJson.success("保存成功");
        if (hasError(entity, result)) {
            // 错误提示
            String errorMsg = errorMsg(result);
            if (!StringUtils.isEmpty(errorMsg)) {
                ajaxJson.fail(errorMsg);
            } else {
                ajaxJson.fail("保存失败");
            }
            return ajaxJson;
        }
        try {
            preSave(entity, request, response);
            User user = UserUtils.getUser();
            FloodAssetsStockRecord record = new FloodAssetsStockRecord();
            record.setAssetsCode(entity.getAssetsCode());
            record.setAssetsName(entity.getAssetsName());
            record.setChangeQty(entity.getStockQty());
            assetsStockRecordService.insert(record);
            afterSave(entity, request, response);
        } catch (Exception e) {
            ajaxJson.fail("保存失败!<br />原因:" + e.getMessage());
        }
        return ajaxJson;
    }

    @Override
    public AjaxJson update(Model model, FloodAssetsStock entity, BindingResult result, HttpServletRequest request, HttpServletResponse response) {
        log.info("Update Controller>>>>>>>> entity is :" + JSON.toJSON(entity));
        AjaxJson ajaxJson = new AjaxJson();
        ajaxJson.success("保存成功");
        if (hasError(entity, result)) {
            // 错误提示
            String errorMsg = errorMsg(result);
            if (!StringUtils.isEmpty(errorMsg)) {
                ajaxJson.fail(errorMsg);
            } else {
                ajaxJson.fail("保存失败");
            }
            return ajaxJson;
        }
        try {
            preSave(entity, request, response);
            FloodAssetsStockRecord record = new FloodAssetsStockRecord();
            record.setId(entity.getId());
            record.setType(entity.getType());
            record.setStockLocation(entity.getStockLocation());
            record.setStockQty(entity.getStockQty());
            log.info("record is :" + JSON.toJSON(record));
            assetsStockRecordService.insertOrUpdate(record);
            afterSave(entity, request, response);
        } catch (Exception e) {
            ajaxJson.fail("保存失败!<br />原因:" + e.getMessage());
        }
        return ajaxJson;
    }
}
