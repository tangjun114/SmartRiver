package cn.jeeweb.modules.repo.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.repo.entity.McRepoStandard;
 
/**   
 * @Title: 规范资料数据库控制层接口
 * @Description: 规范资料数据库控制层接口
 * @author shawloong
 * @date 2017-10-04 15:16:45
 * @version V1.0   
 *
 */
public interface McRepoStandardMapper extends BaseMapper<McRepoStandard> {
    
}