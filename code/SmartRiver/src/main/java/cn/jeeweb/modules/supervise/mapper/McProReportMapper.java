package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProReport;
 
/**   
 * @Title: 报告列表数据库控制层接口
 * @Description: 报告列表数据库控制层接口
 * @author Aether
 * @date 2018-03-10 22:42:24
 * @version V1.0   
 *
 */
public interface McProReportMapper extends BaseMapper<McProReport> {
    
}