package cn.jeeweb.modules.supervise.controller;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.entity.McProItemAlarm;
import cn.jeeweb.modules.monitor.service.IMcProItemAlarmService;
import cn.jeeweb.modules.supervise.entity.McProDeviceAbnormal;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 设备异常状况
 * @Description: 设备异常状况
 * @author Aether
 * @date 2018-04-19 21:46:24
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcprodeviceabnormal")
@RequiresPathPermission("supervise:mcprodeviceabnormal")
public class McProDeviceAbnormalController extends McProjectBaseController<McProDeviceAbnormal, String> {

	private static final String HANDLED = "handled";
	@Override
	public void preSave(McProDeviceAbnormal entity, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preSave(entity, request, response);
		User user = UserUtils.getUser();
		entity.setHandlerId(user.getId());
		entity.setHandlerName(user.getRealname());
		entity.setHandleTime(new Date());
		entity.setStatus(HANDLED);
	}
	
	@Autowired
	private IMcProjectService iMcProjectService;
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProDeviceAbnormal> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
 		entityWrapper.in("projectId",  iMcProjectService.getProjectIdListByUser(UserUtils.getUser()));
 		super.preAjaxList(queryable, entityWrapper, request, response);
	}
	
}
