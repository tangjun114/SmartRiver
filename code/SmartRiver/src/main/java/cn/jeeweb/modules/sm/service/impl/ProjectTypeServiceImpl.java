package cn.jeeweb.modules.sm.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.sm.mapper.ProjectTypeMapper;
import cn.jeeweb.modules.sm.entity.ProjectType;
import cn.jeeweb.modules.sm.service.IProjectTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目类型
 * @Description: 项目类型
 * @author shawloong
 * @date 2017-09-28 00:43:37
 * @version V1.0   
 *
 */
@Transactional
@Service("projectTypeService")
public class ProjectTypeServiceImpl  extends CommonServiceImpl<ProjectTypeMapper,ProjectType> implements  IProjectTypeService {

}
