package cn.jeeweb.modules.water.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.water.entity.WaterDrainOutletReport;

/**   
 * @Title: 排污口检测报表
 * @Description: 排污口检测报表
 * @author wsh
 * @date 2019-03-19 19:09:33
 * @version V1.0   
 *
 */
public interface IWaterDrainOutletReportService extends ICommonService<WaterDrainOutletReport> {

}

