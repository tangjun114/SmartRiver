package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemDevice;
 
/**   
 * @Title: 项目与设备管理表数据库控制层接口
 * @Description: 项目与设备管理表数据库控制层接口
 * @author jerry
 * @date 2018-01-29 17:01:28
 * @version V1.0   
 *
 */
public interface McProItemDeviceMapper extends BaseMapper<McProItemDevice> {
    
}