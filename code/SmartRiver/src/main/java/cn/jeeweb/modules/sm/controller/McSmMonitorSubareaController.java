package cn.jeeweb.modules.sm.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.sm.entity.McSmMonitorSubarea;

/**   
 * @Title: 系统管理-监测分区
 * @Description: 系统管理-监测分区
 * @author shawloong
 * @date 2017-11-05 18:34:12
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/sm/mcsmmonitorsubarea")
@RequiresPathPermission("sm:mcsmmonitorsubarea")
public class McSmMonitorSubareaController extends BaseCRUDController<McSmMonitorSubarea, String> {

	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McSmMonitorSubarea> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
		String  projectId = request.getParameter("projectId");
		if(!StringUtils.isEmpty(projectId)){
			entityWrapper.eq("project_id", projectId);
		}
	}

	@Override
	public void preSave(McSmMonitorSubarea entity, HttpServletRequest request, HttpServletResponse response) {
		String projectId = request.getParameter("projectId");
		if (!StringUtils.isEmpty(projectId)) {
			entity.setProjectId(projectId);
		}
	}

	@Override
	public void preEdit(McSmMonitorSubarea entity, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String projectId = request.getParameter("projectId");
		if (!StringUtils.isEmpty(projectId)) {
			entity.setProjectId(projectId);
		}
	}
	
	

}
