package cn.jeeweb.modules.water.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.water.entity.WaterDrainOutletResult;

/**   
 * @Title: 排污口水质监控
 * @Description: 排污口水质监控
 * @author liyonglei
 * @date 2018-11-14 19:41:31
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/water/waterdrainoutletresult")
@RequiresPathPermission("water:waterdrainoutletresult")
public class WaterDrainOutletResultController extends BaseCRUDController<WaterDrainOutletResult, String> {

}
