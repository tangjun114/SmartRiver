package cn.jeeweb.modules.law.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.law.entity.LawCheckResult;

/**   
 * @Title: 巡查结果
 * @Description: 巡查结果
 * @author liyonglei
 * @date 2018-11-13 20:40:25
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/law/lawcheckresult")
@RequiresPathPermission("law:lawcheckresult")
public class LawCheckResultController extends BaseCRUDController<LawCheckResult, String> {

}
