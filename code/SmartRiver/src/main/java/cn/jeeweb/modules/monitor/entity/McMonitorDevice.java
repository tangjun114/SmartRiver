package cn.jeeweb.modules.monitor.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;

/**   
 * @Title: 设备信息
 * @Description: 设备信息
 * @author shawloong
 * @date 2017-12-28 15:53:12
 * @version V1.0   
 *
 */
@TableName("mc_monitor_device")
@SuppressWarnings("serial")
public class McMonitorDevice extends AbstractEntity<String> {

    /**id*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**监测机构ID*/
    @TableField(value = "monitor_id")
	private String monitorId;
    /**监测机构*/
    @TableField(value = "monitor_name")
	private String monitorName;
    /**设备类型值*/
    @TableField(value = "type")
	private String type;
    /**设备类型*/
    @TableField(value = "type_name")
	private String typeName;
    /**是否自动化*/
    @TableField(value = "is_auto")
	private String isAuto;
    /**设备型号值*/
    @TableField(value = "unit_type")
	private String unitType;
    /**设备型号*/
    @TableField(value = "unit_type_name")
	private String unitTypeName;
    /**设备编号*/
    @TableField(value = "code")
	private String code;
    /**设备名修正*/
    @TableField(value = "name_correction")
	private String nameCorrection;
    /**规格修正*/
    @TableField(value = "spec_revision")
	private String specRevision;
    /**检定/校准日期*/
    @TableField(value = "correcting_date")
	private String correctingDate;
    /**检定/校准到期日期*/
    @TableField(value = "correcting_end_date")
	private String correctingEndDate;
    /**使用状态*/
    @TableField(value = "status")
	private String status;
    /**设备负责人(1)ID*/
    @TableField(value = "head1_id")
	private String head1Id;
    /**设备负责人(1)*/
    @TableField(value = "head1_name")
	private String head1Name;
    /**设备负责人(2)ID*/
    @TableField(value = "head2_id")
	private String head2Id;
    /**设备负责人(2)*/
    @TableField(value = "head2_name")
	private String head2Name;
    /**注意事项*/
    @TableField(value = "matters")
	private String matters;
    /**连接状态*/
    @TableField(value = "linK_status")
	private String linkStatus;
    /**是否过期*/
    @TableField(value = "is_overdue")
	private String isOverdue;
    /**生产厂家*/
    @TableField(value = "manufacturer")
	private String manufacturer;
    /** 精度*/
    @TableField(value = "accuracy")
	private String accuracy;
    /**准确度等级/不确定度*/
    @TableField(value = "accuracy_level")
	private String accuracyLevel;
    /**测量范围*/
    @TableField(value = "measuring_range")
	private String measuringRange;
    /**检定/校准机构*/
    @TableField(value = "calibration_institution")
	private String calibrationInstitution;
    /**图片*/
    @TableField(value = "img")
	private String img;
    /**项目Id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
	
	/**
	 * 获取  id
	 *@return: String  id
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  monitorId
	 *@return: String  监测机构ID
	 */
	public String getMonitorId(){
		return this.monitorId;
	}

	/**
	 * 设置  monitorId
	 *@param: monitorId  监测机构ID
	 */
	public void setMonitorId(String monitorId){
		this.monitorId = monitorId;
	}
	/**
	 * 获取  monitorName
	 *@return: String  监测机构
	 */
	public String getMonitorName(){
		return this.monitorName;
	}

	/**
	 * 设置  monitorName
	 *@param: monitorName  监测机构
	 */
	public void setMonitorName(String monitorName){
		this.monitorName = monitorName;
	}
	/**
	 * 获取  type
	 *@return: String  设备类型值
	 */
	public String getType(){
		return this.type;
	}

	/**
	 * 设置  type
	 *@param: type  设备类型值
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 * 获取  typeName
	 *@return: String  设备类型
	 */
	public String getTypeName(){
		return this.typeName;
	}

	/**
	 * 设置  typeName
	 *@param: typeName  设备类型
	 */
	public void setTypeName(String typeName){
		this.typeName = typeName;
	}
	/**
	 * 获取  isAuto
	 *@return: String  是否自动化
	 */
	public String getIsAuto(){
		return this.isAuto;
	}

	/**
	 * 设置  isAuto
	 *@param: isAuto  是否自动化
	 */
	public void setIsAuto(String isAuto){
		this.isAuto = isAuto;
	}
	/**
	 * 获取  unitType
	 *@return: String  设备型号值
	 */
	public String getUnitType(){
		return this.unitType;
	}

	/**
	 * 设置  unitType
	 *@param: unitType  设备型号值
	 */
	public void setUnitType(String unitType){
		this.unitType = unitType;
	}
	/**
	 * 获取  unitTypeName
	 *@return: String  设备型号
	 */
	public String getUnitTypeName(){
		return this.unitTypeName;
	}

	/**
	 * 设置  unitTypeName
	 *@param: unitTypeName  设备型号
	 */
	public void setUnitTypeName(String unitTypeName){
		this.unitTypeName = unitTypeName;
	}
	/**
	 * 获取  code
	 *@return: String  设备编号
	 */
	public String getCode(){
		return this.code;
	}

	/**
	 * 设置  code
	 *@param: code  设备编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 * 获取  nameCorrection
	 *@return: String  设备名修正
	 */
	public String getNameCorrection(){
		return this.nameCorrection;
	}

	/**
	 * 设置  nameCorrection
	 *@param: nameCorrection  设备名修正
	 */
	public void setNameCorrection(String nameCorrection){
		this.nameCorrection = nameCorrection;
	}
	/**
	 * 获取  specRevision
	 *@return: String  规格修正
	 */
	public String getSpecRevision(){
		return this.specRevision;
	}

	/**
	 * 设置  specRevision
	 *@param: specRevision  规格修正
	 */
	public void setSpecRevision(String specRevision){
		this.specRevision = specRevision;
	}
	/**
	 * 获取  correctingDate
	 *@return: String  检定/校准日期
	 */
	public String getCorrectingDate(){
		return this.correctingDate;
	}

	/**
	 * 设置  correctingDate
	 *@param: correctingDate  检定/校准日期
	 */
	public void setCorrectingDate(String correctingDate){
		this.correctingDate = correctingDate;
	}
	/**
	 * 获取  correctingEndDate
	 *@return: String  检定/校准到期日期
	 */
	public String getCorrectingEndDate(){
		return this.correctingEndDate;
	}

	/**
	 * 设置  correctingEndDate
	 *@param: correctingEndDate  检定/校准到期日期
	 */
	public void setCorrectingEndDate(String correctingEndDate){
		this.correctingEndDate = correctingEndDate;
	}
	/**
	 * 获取  status
	 *@return: String  使用状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  使用状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  head1Id
	 *@return: String  设备负责人(1)ID
	 */
	public String getHead1Id(){
		return this.head1Id;
	}

	/**
	 * 设置  head1Id
	 *@param: head1Id  设备负责人(1)ID
	 */
	public void setHead1Id(String head1Id){
		this.head1Id = head1Id;
	}
	/**
	 * 获取  head1Name
	 *@return: String  设备负责人(1)
	 */
	public String getHead1Name(){
		return this.head1Name;
	}

	/**
	 * 设置  head1Name
	 *@param: head1Name  设备负责人(1)
	 */
	public void setHead1Name(String head1Name){
		this.head1Name = head1Name;
	}
	/**
	 * 获取  head2Id
	 *@return: String  设备负责人(2)ID
	 */
	public String getHead2Id(){
		return this.head2Id;
	}

	/**
	 * 设置  head2Id
	 *@param: head2Id  设备负责人(2)ID
	 */
	public void setHead2Id(String head2Id){
		this.head2Id = head2Id;
	}
	/**
	 * 获取  head2Name
	 *@return: String  设备负责人(2)
	 */
	public String getHead2Name(){
		return this.head2Name;
	}

	/**
	 * 设置  head2Name
	 *@param: head2Name  设备负责人(2)
	 */
	public void setHead2Name(String head2Name){
		this.head2Name = head2Name;
	}
	/**
	 * 获取  matters
	 *@return: String  注意事项
	 */
	public String getMatters(){
		return this.matters;
	}

	/**
	 * 设置  matters
	 *@param: matters  注意事项
	 */
	public void setMatters(String matters){
		this.matters = matters;
	}
	/**
	 * 获取  linkStatus
	 *@return: String  连接状态
	 */
	public String getLinkStatus(){
		return this.linkStatus;
	}

	/**
	 * 设置  linkStatus
	 *@param: linkStatus  连接状态
	 */
	public void setLinkStatus(String linkStatus){
		this.linkStatus = linkStatus;
	}
	/**
	 * 获取  isOverdue
	 *@return: String  是否过期
	 */
	public String getIsOverdue(){
		return this.isOverdue;
	}

	/**
	 * 设置  isOverdue
	 *@param: isOverdue  是否过期
	 */
	public void setIsOverdue(String isOverdue){
		this.isOverdue = isOverdue;
	}
	/**
	 * 获取  manufacturer
	 *@return: String  生产厂家
	 */
	public String getManufacturer(){
		return this.manufacturer;
	}

	/**
	 * 设置  manufacturer
	 *@param: manufacturer  生产厂家
	 */
	public void setManufacturer(String manufacturer){
		this.manufacturer = manufacturer;
	}
	/**
	 * 获取  accuracy
	 *@return: String   精度
	 */
	public String getAccuracy(){
		return this.accuracy;
	}

	/**
	 * 设置  accuracy
	 *@param: accuracy   精度
	 */
	public void setAccuracy(String accuracy){
		this.accuracy = accuracy;
	}
	/**
	 * 获取  accuracyLevel
	 *@return: String  准确度等级/不确定度
	 */
	public String getAccuracyLevel(){
		return this.accuracyLevel;
	}

	/**
	 * 设置  accuracyLevel
	 *@param: accuracyLevel  准确度等级/不确定度
	 */
	public void setAccuracyLevel(String accuracyLevel){
		this.accuracyLevel = accuracyLevel;
	}
	/**
	 * 获取  measuringRange
	 *@return: String  测量范围
	 */
	public String getMeasuringRange(){
		return this.measuringRange;
	}

	/**
	 * 设置  measuringRange
	 *@param: measuringRange  测量范围
	 */
	public void setMeasuringRange(String measuringRange){
		this.measuringRange = measuringRange;
	}
	/**
	 * 获取  calibrationInstitution
	 *@return: String  检定/校准机构
	 */
	public String getCalibrationInstitution(){
		return this.calibrationInstitution;
	}

	/**
	 * 设置  calibrationInstitution
	 *@param: calibrationInstitution  检定/校准机构
	 */
	public void setCalibrationInstitution(String calibrationInstitution){
		this.calibrationInstitution = calibrationInstitution;
	}
	/**
	 * 获取  img
	 *@return: String  图片
	 */
	public String getImg(){
		return this.img;
	}

	/**
	 * 设置  img
	 *@param: img  图片
	 */
	public void setImg(String img){
		this.img = img;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目Id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目Id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	
}