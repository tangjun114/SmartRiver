package cn.jeeweb.modules.supervise.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ff.common.util.log.FFLogFactory;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.entity.McProReport;
import cn.jeeweb.modules.supervise.mapper.McProReportMapper;
import cn.jeeweb.modules.supervise.service.ExcelReportFactory;
import cn.jeeweb.modules.supervise.service.IExcelReportService;
import cn.jeeweb.modules.supervise.service.IMcProReportService;
import cn.jeeweb.modules.supervise.service.ReportTask;

/**   
 * @Title: 报告列表
 * @Description: 报告列表
 * @author Aether
 * @date 2018-03-10 22:42:24
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProReportService")
@EnableAsync
public class McProReportServiceImpl  extends CommonServiceImpl<McProReportMapper,McProReport> implements  IMcProReportService {

	protected Logger log = FFLogFactory.getLog(McProReportServiceImpl.class);
	
	
	@Autowired
	ReportTask reportTask;
	@Override
	public boolean insert(McProReport entity) {
		// TODO Auto-generated method stub
		
		entity.setRemarks("报告正在生成中");
		
		boolean result = super.insert(entity);
  		  
		reportTask.run(entity);
		return result;
	}


	
}
