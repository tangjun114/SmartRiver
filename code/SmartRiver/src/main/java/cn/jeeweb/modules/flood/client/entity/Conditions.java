package cn.jeeweb.modules.flood.client.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @Author:Wish
 * @Date: Created in 2018/12/14 11:27
 **/
public class Conditions implements Serializable {
    private static final long serialVersionUID = 1L;
    private Criterias criterias;
    private List<String> orders;


    public Criterias getCriterias() {
        return criterias;
    }

    public void setCriterias(Criterias criterias) {
        this.criterias = criterias;
    }

    public List<String> getOrders() {
        return orders;
    }

    public void setOrders(List<String> orders) {
        this.orders = orders;
    }
}
