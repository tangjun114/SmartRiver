package cn.jeeweb.modules.project.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.project.entity.SrProjectFunds;

/**   
 * @Title: 工程经费
 * @Description: 工程经费
 * @author wsh
 * @date 2018-12-10 20:42:10
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/project/srprojectfunds")
@RequiresPathPermission("project:srprojectfunds")
public class SrProjectFundsController extends BaseCRUDController<SrProjectFunds, String> {

}
