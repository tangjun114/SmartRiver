package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemInspection;
 
/**   
 * @Title: 抽查记录数据库控制层接口
 * @Description: 抽查记录数据库控制层接口
 * @author jerry
 * @date 2018-06-04 10:36:48
 * @version V1.0   
 *
 */
public interface McProItemInspectionMapper extends BaseMapper<McProItemInspection> {
    
}