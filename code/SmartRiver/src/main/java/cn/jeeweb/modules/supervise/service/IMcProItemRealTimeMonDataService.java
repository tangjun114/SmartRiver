package cn.jeeweb.modules.supervise.service;

import java.util.List;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;
import cn.jeeweb.modules.supervise.model.SumDataModel;
import cn.jeeweb.modules.webservice.UploadDataModel;

/**   
 * @Title: 项目监控-监测情况-实时监测数据
 * @Description: 项目监控-监测情况-实时监测数据
 * @author shawloong
 * @date 2017-11-21 16:32:11
 * @version V1.0   
 *
 */
public interface IMcProItemRealTimeMonDataService extends ICommonService<McProItemRealTimeMonData> {

	public void create(UploadDataModel upload);
	
	public void valid(UploadDataModel upload);
	public List<SumDataModel> getSumData(String projectId,String monitorItemId,String projectCount );
 }

