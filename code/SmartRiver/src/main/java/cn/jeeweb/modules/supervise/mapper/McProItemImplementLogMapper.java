package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemImplementLog;
 
/**   
 * @Title: 项目监控-实施日志数据库控制层接口
 * @Description: 项目监控-实施日志数据库控制层接口
 * @author shawloong
 * @date 2017-11-15 00:48:21
 * @version V1.0   
 *
 */
public interface McProItemImplementLogMapper extends BaseMapper<McProItemImplementLog> {
    
}