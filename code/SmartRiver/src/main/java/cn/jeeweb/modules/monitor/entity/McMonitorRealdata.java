package cn.jeeweb.modules.monitor.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.AbstractEntity;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 监测原始记录信息
 * @Description: 监测原始记录信息
 * @author Aether
 * @date 2017-11-30 23:04:14
 * @version V1.0   
 *
 */
@TableName("mc_monitor_realdata")
@SuppressWarnings("serial")
public class McMonitorRealdata extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**监测项id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**监测项名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**监测项类别id*/
    @TableField(value = "monitor_type_id")
	private String monitorTypeId;
    /**监测项类别名称*/
    @TableField(value = "monitor_type_name")
	private String monitorTypeName;
    /**采集时间*/
    @TableField(value = "collect_time")
	private Date collectTime;
    /**上传时间*/
    @TableField(value = "upload_time")
	private Date uploadTime;
    /**上传人*/
    @TableField(value = "upload_person")
	private String uploadPerson;
    /**监测次数*/
    @TableField(value = "monitor_count")
	private Integer monitorCount;
    /**测点数量*/
    @TableField(value = "mp_count")
	private Integer mpCount;
    /**检测结果*/
    @TableField(value = "measure_result")
	private String measureResult;
    /**文件名*/
    @TableField(value = "file_name")
	private String fileName;
    /**文件路径*/
    @TableField(value = "file_path")
	private String filePath;
    /**状态*/
    @TableField(value = "status")
	private Integer status;
    /**是否有效*/
    @TableField(value = "is_valid")
	private Integer isValid;
    /**分区id*/
    @TableField(value = "subarea_id")
	private String subareaId;
    /**分区名称*/
    @TableField(value = "subarea_name")
	private String subareaName;
    /**测点id*/
    @TableField(value = "mp_id")
	private String mpId;
    //申请理由， 非持久
    @TableField(exist=false)
    private String reason;
    //申请理由， 非持久
    @TableField(exist=false)    
    private String applyFile;
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  monitorTypeId
	 *@return: String  监测项类别id
	 */
	public String getMonitorTypeId(){
		return this.monitorTypeId;
	}

	/**
	 * 设置  monitorTypeId
	 *@param: monitorTypeId  监测项类别id
	 */
	public void setMonitorTypeId(String monitorTypeId){
		this.monitorTypeId = monitorTypeId;
	}
	/**
	 * 获取  monitorTypeName
	 *@return: String  监测项类别名称
	 */
	public String getMonitorTypeName(){
		return this.monitorTypeName;
	}

	/**
	 * 设置  monitorTypeName
	 *@param: monitorTypeName  监测项类别名称
	 */
	public void setMonitorTypeName(String monitorTypeName){
		this.monitorTypeName = monitorTypeName;
	}
	/**
	 * 获取  collectTime
	 *@return: Date  采集时间
	 */
	public Date getCollectTime(){
		return this.collectTime;
	}

	/**
	 * 设置  collectTime
	 *@param: collectTime  采集时间
	 */
	public void setCollectTime(Date collectTime){
		this.collectTime = collectTime;
	}
	/**
	 * 获取  uploadTime
	 *@return: Date  上传时间
	 */
	public Date getUploadTime(){
		return this.uploadTime;
	}

	/**
	 * 设置  uploadTime
	 *@param: uploadTime  上传时间
	 */
	public void setUploadTime(Date uploadTime){
		this.uploadTime = uploadTime;
	}
	/**
	 * 获取  uploadPerson
	 *@return: String  上传人
	 */
	public String getUploadPerson(){
		return this.uploadPerson;
	}

	/**
	 * 设置  uploadPerson
	 *@param: uploadPerson  上传人
	 */
	public void setUploadPerson(String uploadPerson){
		this.uploadPerson = uploadPerson;
	}
	/**
	 * 获取  monitorCount
	 *@return: Integer  监测次数
	 */
	public Integer getMonitorCount(){
		return this.monitorCount;
	}

	/**
	 * 设置  monitorCount
	 *@param: monitorCount  监测次数
	 */
	public void setMonitorCount(Integer monitorCount){
		this.monitorCount = monitorCount;
	}
	/**
	 * 获取  mpCount
	 *@return: Integer  测点数量
	 */
	public Integer getMpCount(){
		return this.mpCount;
	}

	/**
	 * 设置  mpCount
	 *@param: mpCount  测点数量
	 */
	public void setMpCount(Integer mpCount){
		this.mpCount = mpCount;
	}
	/**
	 * 获取  measureResult
	 *@return: String  检测结果
	 */
	public String getMeasureResult(){
		return this.measureResult;
	}

	/**
	 * 设置  measureResult
	 *@param: measureResult  检测结果
	 */
	public void setMeasureResult(String measureResult){
		this.measureResult = measureResult;
	}
	/**
	 * 获取  fileName
	 *@return: String  文件名
	 */
	public String getFileName(){
		return this.fileName;
	}

	/**
	 * 设置  fileName
	 *@param: fileName  文件名
	 */
	public void setFileName(String fileName){
		this.fileName = fileName;
	}
	/**
	 * 获取  filePath
	 *@return: String  文件路径
	 */
	public String getFilePath(){
		return this.filePath;
	}

	/**
	 * 设置  filePath
	 *@param: filePath  文件路径
	 */
	public void setFilePath(String filePath){
		this.filePath = filePath;
	}
	/**
	 * 获取  status
	 *@return: Integer  状态
	 */
	public Integer getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(Integer status){
		this.status = status;
	}
	/**
	 * 获取  isValid
	 *@return: Integer  是否有效
	 */
	public Integer getIsValid(){
		return this.isValid;
	}

	/**
	 * 设置  isValid
	 *@param: isValid  是否有效
	 */
	public void setIsValid(Integer isValid){
		this.isValid = isValid;
	}
	/**
	 * 获取  subareaId
	 *@return: String  分区id
	 */
	public String getSubareaId(){
		return this.subareaId;
	}

	/**
	 * 设置  subareaId
	 *@param: subareaId  分区id
	 */
	public void setSubareaId(String subareaId){
		this.subareaId = subareaId;
	}
	/**
	 * 获取  subareaName
	 *@return: String  分区名称
	 */
	public String getSubareaName(){
		return this.subareaName;
	}

	/**
	 * 设置  subareaName
	 *@param: subareaName  分区名称
	 */
	public void setSubareaName(String subareaName){
		this.subareaName = subareaName;
	}
	/**
	 * 获取  mpId
	 *@return: String  测点id
	 */
	public String getMpId(){
		return this.mpId;
	}

	/**
	 * 设置  mpId
	 *@param: mpId  测点id
	 */
	public void setMpId(String mpId){
		this.mpId = mpId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getApplyFile() {
		return applyFile;
	}

	public void setApplyFile(String applyFile) {
		this.applyFile = applyFile;
	}
	
	
	
}
