package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemOtherDevice;

/**   
 * @Title: 项目中的其他设备
 * @Description: 项目中的其他设备
 * @author jerry
 * @date 2018-01-28 18:52:59
 * @version V1.0   
 *
 */
public interface IMcProItemOtherDeviceService extends ICommonService<McProItemOtherDevice> {

}

