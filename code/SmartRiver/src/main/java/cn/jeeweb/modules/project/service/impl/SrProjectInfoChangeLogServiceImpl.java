package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectInfoChangeLogMapper;
import cn.jeeweb.modules.project.entity.SrProjectInfoChangeLog;
import cn.jeeweb.modules.project.service.ISrProjectInfoChangeLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 工程信息变动调整记录
 * @Description: 工程信息变动调整记录
 * @author wsh
 * @date 2019-03-31 14:34:53
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectInfoChangeLogService")
public class SrProjectInfoChangeLogServiceImpl  extends CommonServiceImpl<SrProjectInfoChangeLogMapper,SrProjectInfoChangeLog> implements  ISrProjectInfoChangeLogService {

}
