package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProject;
 
/**   
 * @Title: 项目管理数据库控制层接口
 * @Description: 项目管理数据库控制层接口
 * @author shawloong
 * @date 2017-11-21 16:31:35
 * @version V1.0   
 *
 */
public interface McProjectMapper extends BaseMapper<McProject> {
    
}