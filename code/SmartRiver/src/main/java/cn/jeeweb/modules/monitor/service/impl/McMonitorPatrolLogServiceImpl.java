package cn.jeeweb.modules.monitor.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ff.common.util.validate.ValidatorUtil;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolDetail;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolLog;
import cn.jeeweb.modules.monitor.mapper.McMonitorPatrolLogMapper;
import cn.jeeweb.modules.monitor.service.IMcMonitorPatrolDetailService;
import cn.jeeweb.modules.monitor.service.IMcMonitorPatrolLogService;

/**   
 * @Title: 巡检日志
 * @Description: 巡检日志
 * @author shawloong
 * @date 2017-10-15 23:43:36
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorPatrolLogService")
public class McMonitorPatrolLogServiceImpl  extends CommonServiceImpl<McMonitorPatrolLogMapper,McMonitorPatrolLog> implements  IMcMonitorPatrolLogService {

	@Autowired
	IMcMonitorPatrolDetailService iMcMonitorPatrolDetailService;
	@Override
	public boolean insert(McMonitorPatrolLog entity) {
		// TODO Auto-generated method stub
		boolean result = super.insert(entity);
		
		if(!ValidatorUtil.isEmpty(entity.getDetailList()))
		{
			for(McMonitorPatrolDetail e :entity.getDetailList())
			{
				e.setPatrolId(entity.getId());
				e.setPatrolName(entity.getName());
				 
			}
			iMcMonitorPatrolDetailService.insertBatch(entity.getDetailList());
 		}
		
		
		return result;
	}

	
}
