package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.flood.mapper.FloodDestoryReportMapper;
import cn.jeeweb.modules.flood.entity.FloodDestoryReport;
import cn.jeeweb.modules.flood.service.IFloodDestoryReportService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 水毁报表
 * @Description: 水毁报表
 * @author wsh
 * @date 2018-11-27 18:19:56
 * @version V1.0   
 *
 */
@Transactional
@Service("floodDestoryReportService")
public class FloodDestoryReportServiceImpl  extends CommonServiceImpl<FloodDestoryReportMapper,FloodDestoryReport> implements  IFloodDestoryReportService {

}
