package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;

/**   
 * @Title: 项目监控-测点设置-监测项-测点
 * @Description: 项目监控-测点设置-监测项-测点
 * @author shawloong
 * @date 2017-11-21 16:31:17
 * @version V1.0   
 *
 */
public interface IMcProItemMeasuringPointService extends ICommonService<McProItemMeasuringPoint> {

}

