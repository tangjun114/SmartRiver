package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemExchangeMapper;
import cn.jeeweb.modules.supervise.entity.McProItemExchange;
import cn.jeeweb.modules.supervise.service.IMcProItemExchangeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目沟通交流
 * @Description: 项目沟通交流
 * @author Aether
 * @date 2018-04-28 13:26:54
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemExchangeService")
public class McProItemExchangeServiceImpl  extends CommonServiceImpl<McProItemExchangeMapper,McProItemExchange> implements  IMcProItemExchangeService {

}
