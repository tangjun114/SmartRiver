package cn.jeeweb.modules.monitor.entity;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.AbstractEntity;

/**   
 * @Title: 巡检日志
 * @Description: 巡检日志
 * @author shawloong
 * @date 2018-01-10 22:49:58
 * @version V1.0   
 *
 */
@TableName("mc_monitor_patrol_log")
@SuppressWarnings("serial")
public class McMonitorPatrolLog extends AbstractEntity<String> {

    /**ID*/
    @TableId(value = "id", type = IdType.UUID)
	private String id = "";
    /**名称*/
    @TableField(value = "name")
	private String name;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**巡检日期*/
    @TableField(value = "patrol_date")
	private Date patrolDate;
    /**巡检人id*/
    @TableField(value = "rummager_id")
	private String rummagerId;
    /**巡检人*/
    @TableField(value = "rummager_name")
	private String rummagerName;
    /**地址*/
    @TableField(value = "address")
	private String address;
    /**经度*/
    @TableField(value = "latitude")
	private String latitude;
    /**纬度*/
    @TableField(value = "longitude")
	private String longitude;
    /**状态*/
    @TableField(value = "status")
	private String status;
    /**说明*/
    @TableField(value = "desc")
	private String desc;
    /**图片*/
    @TableField(value = "imgs")
	private String imgs;
    /**总体情况*/
    @TableField(value = "overall_situation")
	private String overallSituation;
    /**结论*/
    @TableField(value = "summary")
	private String summary;
    @TableField(exist = false)
    private String imgIds;
    
    @TableField(exist = false)
    private List<McMonitorPatrolDetail>  detailList;
    
	/**
	 * @return the imgIds
	 */
	public String getImgIds() {
		return imgIds;
	}

	/**
	 * @param imgIds the imgIds to set
	 */
	public void setImgIds(String imgIds) {
		this.imgIds = imgIds;
	}

	/**
	 * 获取  id
	 *@return: String  ID
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  ID
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  name
	 *@return: String  名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  patrolDate
	 *@return: Date  巡检日期
	 */
	public Date getPatrolDate(){
		return this.patrolDate;
	}

	/**
	 * 设置  patrolDate
	 *@param: patrolDate  巡检日期
	 */
	public void setPatrolDate(Date patrolDate){
		this.patrolDate = patrolDate;
	}
	/**
	 * 获取  rummagerId
	 *@return: String  巡检人id
	 */
	public String getRummagerId(){
		return this.rummagerId;
	}

	/**
	 * 设置  rummagerId
	 *@param: rummagerId  巡检人id
	 */
	public void setRummagerId(String rummagerId){
		this.rummagerId = rummagerId;
	}
	/**
	 * 获取  rummagerName
	 *@return: String  巡检人
	 */
	public String getRummagerName(){
		return this.rummagerName;
	}

	/**
	 * 设置  rummagerName
	 *@param: rummagerName  巡检人
	 */
	public void setRummagerName(String rummagerName){
		this.rummagerName = rummagerName;
	}
	/**
	 * 获取  address
	 *@return: String  地址
	 */
	public String getAddress(){
		return this.address;
	}

	/**
	 * 设置  address
	 *@param: address  地址
	 */
	public void setAddress(String address){
		this.address = address;
	}
	/**
	 * 获取  latitude
	 *@return: String  经度
	 */
	public String getLatitude(){
		return this.latitude;
	}

	/**
	 * 设置  latitude
	 *@param: latitude  经度
	 */
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
	/**
	 * 获取  longitude
	 *@return: String  纬度
	 */
	public String getLongitude(){
		return this.longitude;
	}

	/**
	 * 设置  longitude
	 *@param: longitude  纬度
	 */
	public void setLongitude(String longitude){
		this.longitude = longitude;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  desc
	 *@return: String  说明
	 */
	public String getDesc(){
		return this.desc;
	}

	/**
	 * 设置  desc
	 *@param: desc  说明
	 */
	public void setDesc(String desc){
		this.desc = desc;
	}
	/**
	 * 获取  imgs
	 *@return: String  图片
	 */
	public String getImgs(){
		return this.imgs;
	}

	/**
	 * 设置  imgs
	 *@param: imgs  图片
	 */
	public void setImgs(String imgs){
		this.imgs = imgs;
	}
	/**
	 * 获取  overallSituation
	 *@return: String  总体情况
	 */
	public String getOverallSituation(){
		return this.overallSituation;
	}

	/**
	 * 设置  overallSituation
	 *@param: overallSituation  总体情况
	 */
	public void setOverallSituation(String overallSituation){
		this.overallSituation = overallSituation;
	}
	/**
	 * 获取  summary
	 *@return: String  结论
	 */
	public String getSummary(){
		return this.summary;
	}

	/**
	 * 设置  summary
	 *@param: summary  结论
	 */
	public void setSummary(String summary){
		this.summary = summary;
	}

	public List<McMonitorPatrolDetail> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<McMonitorPatrolDetail> detailList) {
		this.detailList = detailList;
	}
	
}
