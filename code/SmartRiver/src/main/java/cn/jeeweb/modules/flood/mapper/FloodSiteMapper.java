package cn.jeeweb.modules.flood.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.flood.entity.FloodSite;
 
/**   
 * @Title: 监控站点数据库控制层接口
 * @Description: 监控站点数据库控制层接口
 * @author wsh
 * @date 2018-11-27 18:04:01
 * @version V1.0   
 *
 */
public interface FloodSiteMapper extends BaseMapper<FloodSite> {
    
}