package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 项目中的通信设备
 * @Description: 项目中的通信设备
 * @author shawloong
 * @date 2018-03-20 11:42:24
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_collection")
@SuppressWarnings("serial")
public class McProItemCollection extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**采集仪编号*/
    @TableField(value = "code")
	private String code;
    /**使用状态*/
    @TableField(value = "status")
	private String status;
    /**生产厂家*/
    @TableField(value = "manufacturer")
	private String manufacturer;
    /**项目Id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**电话号码*/
    @TableField(value = "phone")
	private String phone;
    /**名称*/
    @TableField(value = "name")
	private String name;
    /**类型*/
    @TableField(value = "type")
	private String type;
    /**类型名称*/
    @TableField(value = "type_name")
	private String typeName;
    /**型号*/
    @TableField(value = "unit_type")
	private String unitType;
    /**型号名称*/
    @TableField(value = "unit_type_name")
	private String unitTypeName;
    /**监测机构ID*/
    @TableField(value = "monitor_id")
	private String monitorId;
    /**监测机构*/
    @TableField(value = "monitor_name")
	private String monitorName;
    /**通信仪器机构*/
    @TableField(value = "collection_id")
	private String collectionId;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  code
	 *@return: String  采集仪编号
	 */
	public String getCode(){
		return this.code;
	}

	/**
	 * 设置  code
	 *@param: code  采集仪编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 * 获取  status
	 *@return: String  使用状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  使用状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  manufacturer
	 *@return: String  生产厂家
	 */
	public String getManufacturer(){
		return this.manufacturer;
	}

	/**
	 * 设置  manufacturer
	 *@param: manufacturer  生产厂家
	 */
	public void setManufacturer(String manufacturer){
		this.manufacturer = manufacturer;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目Id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目Id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  phone
	 *@return: String  电话号码
	 */
	public String getPhone(){
		return this.phone;
	}

	/**
	 * 设置  phone
	 *@param: phone  电话号码
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	/**
	 * 获取  name
	 *@return: String  名称
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 设置  name
	 *@param: name  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * 获取  type
	 *@return: String  类型
	 */
	public String getType(){
		return this.type;
	}

	/**
	 * 设置  type
	 *@param: type  类型
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 * 获取  typeName
	 *@return: String  类型名称
	 */
	public String getTypeName(){
		return this.typeName;
	}

	/**
	 * 设置  typeName
	 *@param: typeName  类型名称
	 */
	public void setTypeName(String typeName){
		this.typeName = typeName;
	}
	/**
	 * 获取  unitType
	 *@return: String  型号
	 */
	public String getUnitType(){
		return this.unitType;
	}

	/**
	 * 设置  unitType
	 *@param: unitType  型号
	 */
	public void setUnitType(String unitType){
		this.unitType = unitType;
	}
	/**
	 * 获取  unitTypeName
	 *@return: String  型号名称
	 */
	public String getUnitTypeName(){
		return this.unitTypeName;
	}

	/**
	 * 设置  unitTypeName
	 *@param: unitTypeName  型号名称
	 */
	public void setUnitTypeName(String unitTypeName){
		this.unitTypeName = unitTypeName;
	}
	/**
	 * 获取  monitorId
	 *@return: String  监测机构ID
	 */
	public String getMonitorId(){
		return this.monitorId;
	}

	/**
	 * 设置  monitorId
	 *@param: monitorId  监测机构ID
	 */
	public void setMonitorId(String monitorId){
		this.monitorId = monitorId;
	}
	/**
	 * 获取  monitorName
	 *@return: String  监测机构
	 */
	public String getMonitorName(){
		return this.monitorName;
	}

	/**
	 * 设置  monitorName
	 *@param: monitorName  监测机构
	 */
	public void setMonitorName(String monitorName){
		this.monitorName = monitorName;
	}
	/**
	 * 获取  collectionId
	 *@return: String  通信仪器机构
	 */
	public String getCollectionId(){
		return this.collectionId;
	}

	/**
	 * 设置  collectionId
	 *@param: collectionId  通信仪器机构
	 */
	public void setCollectionId(String collectionId){
		this.collectionId = collectionId;
	}
	
}
