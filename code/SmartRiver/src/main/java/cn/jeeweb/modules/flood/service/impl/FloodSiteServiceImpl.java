package cn.jeeweb.modules.flood.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.flood.mapper.FloodSiteMapper;
import cn.jeeweb.modules.flood.entity.FloodSite;
import cn.jeeweb.modules.flood.service.IFloodSiteService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 监控站点
 * @Description: 监控站点
 * @author wsh
 * @date 2018-11-27 18:04:01
 * @version V1.0   
 *
 */
@Transactional
@Service("floodSiteService")
public class FloodSiteServiceImpl  extends CommonServiceImpl<FloodSiteMapper,FloodSite> implements  IFloodSiteService {

}
