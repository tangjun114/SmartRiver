package cn.jeeweb.modules.sm.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.sm.mapper.McSmMonitorGroupTypeMapper;
import cn.jeeweb.modules.sm.entity.McSmMonitorGroupType;
import cn.jeeweb.modules.sm.service.IMcSmMonitorGroupTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 监测组类别
 * @Description: 监测组类别
 * @author jerry
 * @date 2018-03-14 15:54:31
 * @version V1.0   
 *
 */
@Transactional
@Service("mcSmMonitorGroupTypeService")
public class McSmMonitorGroupTypeServiceImpl  extends CommonServiceImpl<McSmMonitorGroupTypeMapper,McSmMonitorGroupType> implements  IMcSmMonitorGroupTypeService {

}
