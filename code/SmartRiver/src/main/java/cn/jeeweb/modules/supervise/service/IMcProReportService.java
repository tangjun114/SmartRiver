package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProReport;

/**   
 * @Title: 报告列表
 * @Description: 报告列表
 * @author Aether
 * @date 2018-03-10 22:42:24
 * @version V1.0   
 *
 */
public interface IMcProReportService extends ICommonService<McProReport> {

}

