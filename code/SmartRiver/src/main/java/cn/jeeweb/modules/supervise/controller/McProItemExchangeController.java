package cn.jeeweb.modules.supervise.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProItemExchange;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 项目沟通交流
 * @Description: 项目沟通交流
 * @author Aether
 * @date 2018-04-28 13:26:54
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemexchange")
@RequiresPathPermission("supervise:mcproitemexchange")
public class McProItemExchangeController extends McProjectBaseController<McProItemExchange, String> {

	
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProItemExchange> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preAjaxList(queryable, entityWrapper, request, response);
		entityWrapper.eq("create_by", UserUtils.getUser().getId());
		entityWrapper.or();
		//一个用户 可以在不同项目中 不同身份吗
		entityWrapper.eq("visible_type", UserUtils.getUser().getSubType());

	}

}
