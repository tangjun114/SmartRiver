package cn.jeeweb.modules.monitor.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.monitor.mapper.McMonitorDeviceMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorDevice;
import cn.jeeweb.modules.monitor.service.IMcMonitorDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;

/**   
 * @Title: 设备信息
 * @Description: 设备信息
 * @author shawloong
 * @date 2017-10-14 00:12:17
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorDeviceService")
public class McMonitorDeviceServiceImpl  extends CommonServiceImpl<McMonitorDeviceMapper,McMonitorDevice> implements  IMcMonitorDeviceService {
	
	@Override
	public boolean insert(McMonitorDevice mcMonitorDevice) {
		// 保存主表
		super.insert(mcMonitorDevice);
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(McMonitorDevice mcMonitorDevice) {
		try {
			// 更新主表
			super.insertOrUpdate(mcMonitorDevice);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}
	
	
	
}
