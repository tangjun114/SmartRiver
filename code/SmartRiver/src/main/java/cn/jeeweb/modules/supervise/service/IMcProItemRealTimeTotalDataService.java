package cn.jeeweb.modules.supervise.service;

import java.util.List;
import java.util.Map;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeTotalData;

/**   
 * @Title: 项目监控-监测情况-实时监测数据统计
 * @Description: 项目监控-监测情况-实时监测数据统计
 * @author shawloong
 * @date 2018-03-06 20:19:22
 * @version V1.0   
 *
 */
public interface IMcProItemRealTimeTotalDataService extends ICommonService<McProItemRealTimeTotalData> {

	List<Map<String,Object>> getSumData(Queryable wrapper);
}

