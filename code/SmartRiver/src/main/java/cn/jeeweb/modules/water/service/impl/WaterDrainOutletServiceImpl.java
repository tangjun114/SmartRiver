package cn.jeeweb.modules.water.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.water.mapper.WaterDrainOutletMapper;
import cn.jeeweb.modules.water.entity.WaterDrainOutlet;
import cn.jeeweb.modules.water.service.IWaterDrainOutletService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 排污口管理
 * @Description: 排污口管理
 * @author liyonglei
 * @date 2018-11-14 19:33:01
 * @version V1.0   
 *
 */
@Transactional
@Service("waterDrainOutletService")
public class WaterDrainOutletServiceImpl  extends CommonServiceImpl<WaterDrainOutletMapper,WaterDrainOutlet> implements  IWaterDrainOutletService {

}
