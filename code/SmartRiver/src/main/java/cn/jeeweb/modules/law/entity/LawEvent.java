package cn.jeeweb.modules.law.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 违法事件
 * @Description: 违法事件
 * @author liyonglei
 * @date 2018-11-13 08:40:53
 * @version V1.0   
 *
 */
@TableName("sr_law_event")
@SuppressWarnings("serial")
public class LawEvent extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**事件名称*/
    @TableField(value = "event_name")
	private String eventName;
    /**事件地址*/
    @TableField(value = "event_addr")
	private String eventAddr;
    /**发生时间*/
    @TableField(value = "occur_time")
	private Date occurTime;
    /**事件等级*/
    @TableField(value = "event_level")
	private String eventLevel;
    /**上报人*/
    @TableField(value = "report_user")
	private String reportUser;
    /**图片*/
    @TableField(value = "event_img")
	private String eventImg;
    /**视频*/
    @TableField(value = "event_video")
	private String eventVideo;
    /**所属科室*/
    @TableField(value = "own_department")
	private String ownDepartment;
    /**所属所*/
    @TableField(value = "own_station")
	private String ownStation;
    /**type*/
    @TableField(value = "type")
	private String type;
    /**状态*/
    @TableField(value = "status")
	private String status;
    /**责任人*/
    @TableField(value = "responsible")
	private String responsible;
    /**事件描述*/
    @TableField(value = "desc")
	private String desc;
    /**处理措施*/
    @TableField(value = "treatment")
	private String treatment;
    /**违法人员*/
    @TableField(value = "offender")
	private String offender;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  eventName
	 *@return: String  事件名称
	 */
	public String getEventName(){
		return this.eventName;
	}

	/**
	 * 设置  eventName
	 *@param: eventName  事件名称
	 */
	public void setEventName(String eventName){
		this.eventName = eventName;
	}
	/**
	 * 获取  eventAddr
	 *@return: String  事件地址
	 */
	public String getEventAddr(){
		return this.eventAddr;
	}

	/**
	 * 设置  eventAddr
	 *@param: eventAddr  事件地址
	 */
	public void setEventAddr(String eventAddr){
		this.eventAddr = eventAddr;
	}
	/**
	 * 获取  occurTime
	 *@return: Date  发生时间
	 */
	public Date getOccurTime(){
		return this.occurTime;
	}

	/**
	 * 设置  occurTime
	 *@param: occurTime  发生时间
	 */
	public void setOccurTime(Date occurTime){
		this.occurTime = occurTime;
	}
	/**
	 * 获取  eventLevel
	 *@return: String  事件等级
	 */
	public String getEventLevel(){
		return this.eventLevel;
	}

	/**
	 * 设置  eventLevel
	 *@param: eventLevel  事件等级
	 */
	public void setEventLevel(String eventLevel){
		this.eventLevel = eventLevel;
	}
	/**
	 * 获取  reportUser
	 *@return: String  上报人
	 */
	public String getReportUser(){
		return this.reportUser;
	}

	/**
	 * 设置  reportUser
	 *@param: reportUser  上报人
	 */
	public void setReportUser(String reportUser){
		this.reportUser = reportUser;
	}
	/**
	 * 获取  eventImg
	 *@return: String  图片
	 */
	public String getEventImg(){
		return this.eventImg;
	}

	/**
	 * 设置  eventImg
	 *@param: eventImg  图片
	 */
	public void setEventImg(String eventImg){
		this.eventImg = eventImg;
	}
	/**
	 * 获取  eventVideo
	 *@return: String  视频
	 */
	public String getEventVideo(){
		return this.eventVideo;
	}

	/**
	 * 设置  eventVideo
	 *@param: eventVideo  视频
	 */
	public void setEventVideo(String eventVideo){
		this.eventVideo = eventVideo;
	}
	/**
	 * 获取  ownDepartment
	 *@return: String  所属科室
	 */
	public String getOwnDepartment(){
		return this.ownDepartment;
	}

	/**
	 * 设置  ownDepartment
	 *@param: ownDepartment  所属科室
	 */
	public void setOwnDepartment(String ownDepartment){
		this.ownDepartment = ownDepartment;
	}
	/**
	 * 获取  ownStation
	 *@return: String  所属所
	 */
	public String getOwnStation(){
		return this.ownStation;
	}

	/**
	 * 设置  ownStation
	 *@param: ownStation  所属所
	 */
	public void setOwnStation(String ownStation){
		this.ownStation = ownStation;
	}
	/**
	 * 获取  type
	 *@return: String  type
	 */
	public String getType(){
		return this.type;
	}

	/**
	 * 设置  type
	 *@param: type  type
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  responsible
	 *@return: String  责任人
	 */
	public String getResponsible(){
		return this.responsible;
	}

	/**
	 * 设置  responsible
	 *@param: responsible  责任人
	 */
	public void setResponsible(String responsible){
		this.responsible = responsible;
	}
	/**
	 * 获取  desc
	 *@return: String  事件描述
	 */
	public String getDesc(){
		return this.desc;
	}

	/**
	 * 设置  desc
	 *@param: desc  事件描述
	 */
	public void setDesc(String desc){
		this.desc = desc;
	}
	/**
	 * 获取  treatment
	 *@return: String  处理措施
	 */
	public String getTreatment(){
		return this.treatment;
	}

	/**
	 * 设置  treatment
	 *@param: treatment  处理措施
	 */
	public void setTreatment(String treatment){
		this.treatment = treatment;
	}
	/**
	 * 获取  
wéifǎ rényuán
4/5000
offender
	 *@return: String  违法人员
	 */
	public String getOffender(){
		return this.offender;
	}

	/**
	 * 设置  
wéifǎ rényuán
4/5000
offender
	 *@param: 
wéifǎ rényuán
4/5000
offender  违法人员
	 */
	public void setOffender(String offender){
		this.offender =offender;
	}
	
}
