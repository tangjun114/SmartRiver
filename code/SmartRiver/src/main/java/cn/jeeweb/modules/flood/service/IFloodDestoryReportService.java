package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodDestoryReport;

/**   
 * @Title: 水毁报表
 * @Description: 水毁报表
 * @author wsh
 * @date 2018-11-27 18:19:56
 * @version V1.0   
 *
 */
public interface IFloodDestoryReportService extends ICommonService<FloodDestoryReport> {

}

