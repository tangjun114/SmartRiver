package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemStandardMapper;
import cn.jeeweb.modules.supervise.entity.McProItemStandard;
import cn.jeeweb.modules.supervise.service.IMcProItemStandardService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目规范
 * @Description: 项目规范
 * @author jerry
 * @date 2017-12-25 15:25:05
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemStandardService")
public class McProItemStandardServiceImpl  extends CommonServiceImpl<McProItemStandardMapper,McProItemStandard> implements  IMcProItemStandardService {

}
