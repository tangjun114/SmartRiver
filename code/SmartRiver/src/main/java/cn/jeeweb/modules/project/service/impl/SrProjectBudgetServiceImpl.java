package cn.jeeweb.modules.project.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.project.mapper.SrProjectBudgetMapper;
import cn.jeeweb.modules.project.entity.SrProjectBudget;
import cn.jeeweb.modules.project.service.ISrProjectBudgetService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目预算
 * @Description: 项目预算
 * @author jerry
 * @date 2018-11-13 21:29:59
 * @version V1.0   
 *
 */
@Transactional
@Service("srProjectBudgetService")
public class SrProjectBudgetServiceImpl  extends CommonServiceImpl<SrProjectBudgetMapper,SrProjectBudget> implements  ISrProjectBudgetService {

}
