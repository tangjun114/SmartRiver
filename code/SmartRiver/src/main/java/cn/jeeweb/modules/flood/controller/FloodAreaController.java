package cn.jeeweb.modules.flood.controller;


import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.FloodArea;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wsh
 * @version V1.0
 * @Title: 水清监测区域
 * @Description: 水情监测区域
 * @date 2018-11-30 16:11:01
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/floodarea")
@RequiresPathPermission("flood:floodarea")
public class FloodAreaController extends BaseCRUDController<FloodArea, String> {
}
