package cn.jeeweb.modules.supervise.controller;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.parse.QueryToWrapper;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeTotalData;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeMonDataService;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeTotalDataService;

/**   
 * @Title: 项目监控-监测情况-实时监测数据统计
 * @Description: 项目监控-监测情况-实时监测数据统计
 * @author shawloong
 * @date 2018-03-06 20:19:22
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemrealtimetotaldata")
@RequiresPathPermission("supervise:mcproitemrealtimetotaldata")
public class McProItemRealTimeTotalDataController extends BaseCRUDController<McProItemRealTimeTotalData, String> {

	@Autowired
	private IMcProItemRealTimeTotalDataService iMcProItemRealTimeTotalDataService;
	
 


	@RequestMapping("/sum")
	@ResponseBody
	public BaseRspJson<List<Map<String, Object>>> sum(@RequestBody BaseReqJson request) {
		// TODO Auto-generated method stub
		
 		BaseRspJson<List<Map<String, Object>>> rsp = new BaseRspJson<List<Map<String, Object>>>();
		
 		Queryable qy = this.GetFilterCondition(request);
 		 
 
		List<Map<String, Object>> obj = iMcProItemRealTimeTotalDataService.getSumData(qy);
 		rsp.setObj(obj);
  		return rsp;
 	}
	
	
 
}
