package cn.jeeweb.modules.supervise.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import cn.jeeweb.core.common.entity.AbstractEntity;
import cn.jeeweb.modules.supervise.model.report.GDSimpleForm;
import cn.jeeweb.modules.sys.entity.User;

/**   
 * @Title: 报告列表
 * @Description: 报告列表
 * @author Aether
 * @date 2018-03-24 21:51:36
 * @version V1.0   
 *
 */
@TableName("mc_pro_report")
@SuppressWarnings("serial")
public class McProReport extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**报告名称*/
    @TableField(value = "report_name")
	private String reportName;
    /**报告类型*/
    @TableField(value = "report_type")
	private String reportType;
    /**报告地址*/
    @TableField(value = "report_path")
	private String reportPath;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目编号*/
    @TableField(value = "project_code")
	private String projectCode;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**附加信息*/
    @TableField(value = "report_extend")
	private String reportExtend;
    /**校核人*/
    @TableField(value = "checker")
	private String checker;
    /**校核时间*/
    @TableField(value = "check_time")
	private Date checkTime;
    /**审核人*/
    @TableField(value = "approver")
	private String approver;
    /**审核时间*/
    @TableField(value = "approve_time")
	private Date approveTime;
    /**报告状态*/
    @TableField(value = "status")
	private String status;
    /**报告生成人*/
    @TableField(value = "create_by_name")
	private String createByName;
	
	@TableField(exist=false)
    private GDSimpleForm extendForm;
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  reportName
	 *@return: String  报告名称
	 */
	public String getReportName(){
		return this.reportName;
	}

	/**
	 * 设置  reportName
	 *@param: reportName  报告名称
	 */
	public void setReportName(String reportName){
		this.reportName = reportName;
	}
	/**
	 * 获取  reportType
	 *@return: String  报告类型
	 */
	public String getReportType(){
		return this.reportType;
	}

	/**
	 * 设置  reportType
	 *@param: reportType  报告类型
	 */
	public void setReportType(String reportType){
		this.reportType = reportType;
	}
	/**
	 * 获取  reportPath
	 *@return: String  报告地址
	 */
	public String getReportPath(){
		return this.reportPath;
	}

	/**
	 * 设置  reportPath
	 *@param: reportPath  报告地址
	 */
	public void setReportPath(String reportPath){
		this.reportPath = reportPath;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectCode
	 *@return: String  项目编号
	 */
	public String getProjectCode(){
		return this.projectCode;
	}

	/**
	 * 设置  projectCode
	 *@param: projectCode  项目编号
	 */
	public void setProjectCode(String projectCode){
		this.projectCode = projectCode;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  reportExtend
	 *@return: String  附加信息
	 */
	public String getReportExtend(){
		return this.reportExtend;
	}

	/**
	 * 设置  reportExtend
	 *@param: reportExtend  附加信息
	 */
	public void setReportExtend(String reportExtend){
		this.reportExtend = reportExtend;
	}
	/**
	 * 获取  checker
	 *@return: String  校核人
	 */
	public String getChecker(){
		return this.checker;
	}

	/**
	 * 设置  checker
	 *@param: checker  校核人
	 */
	public void setChecker(String checker){
		this.checker = checker;
	}
	/**
	 * 获取  checkTime
	 *@return: Date  校核时间
	 */
	public Date getCheckTime(){
		return this.checkTime;
	}

	/**
	 * 设置  checkTime
	 *@param: checkTime  校核时间
	 */
	public void setCheckTime(Date checkTime){
		this.checkTime = checkTime;
	}
	/**
	 * 获取  approver
	 *@return: String  审核人
	 */
	public String getApprover(){
		return this.approver;
	}

	/**
	 * 设置  approver
	 *@param: approver  审核人
	 */
	public void setApprover(String approver){
		this.approver = approver;
	}
	/**
	 * 获取  approveTime
	 *@return: Date  审核时间
	 */
	public Date getApproveTime(){
		return this.approveTime;
	}

	/**
	 * 设置  approveTime
	 *@param: approveTime  审核时间
	 */
	public void setApproveTime(Date approveTime){
		this.approveTime = approveTime;
	}
	/**
	 * 获取  status
	 *@return: String  报告状态
	 */
	public String getStatus(){
		if (this.status =="" ||this.status ==null){
			return "to_check";
		}
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  报告状态
	 */
	public void setStatus(String status){
		this.status = status;
	}

	public GDSimpleForm getExtendForm() {
		return extendForm;
	}

	public void setExtendForm(GDSimpleForm extendForm) {
		this.extendForm = extendForm;
	}

	/**
	 * 获取  createByName
	 *@return: String  报告生成人
	 */
	public String getCreateByName(){
		return this.createByName;
	}

	/**
	 * 设置  createByName
	 *@param: createByName  报告生成人
	 */
	public void setCreateByName(String createByName){
		this.createByName = createByName;
	}
	
	
	
}
