package cn.jeeweb.modules.flood.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;

import java.util.Date;

/**
 * @author wsh
 * @version V1.0
 * @Title: 水毁报表
 * @Description: 水毁报表
 * @date 2018-11-27 18:19:56
 */
@TableName("sr_flood_destory_report")
@SuppressWarnings("serial")
public class FloodDestoryReport extends AbstractEntity<String> {

    /**
     * 字段主键
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;
    /**
     * 创建者
     */
    @TableField(value = "create_by", el = "createBy.id")
    private User createBy;
    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField(value = "update_by", el = "updateBy.id")
    private User updateBy;
    /**
     * 更新时间
     */
    @TableField(value = "update_date")
    private Date updateDate;
    /**
     * 删除标记（0：正常；1：删除）
     */
    @TableField(value = "del_flag")
    private String delFlag;
    /**
     * 备注信息
     */
    @TableField(value = "remarks")
    private String remarks;
    /**
     * 区域id
     */
    @TableField(value = "area_id")
    private String areaId;
    /**
     * 区域name
     */
    @TableField(value = "area_name")
    private String areaName;
    /**
     * 经济损失
     */
    @TableField(value = "econ_loss")
    private Double econLoss;
    /**
     * 年
     */
    @TableField(value = "year")
    private Integer year;
    /**
     * 月
     */
    @TableField(value = "month")
    private Integer month;
    /**
     * 天
     */
    @TableField(value = "day")
    private Integer day;


    @TableField(value = "record_time")
    private Date recordTime;


    /**
     * 获取  id
     *
     * @return: String  字段主键
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置  id
     *
     * @param: id  字段主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取  createBy
     *
     * @return: User  创建者
     */
    public User getCreateBy() {
        return this.createBy;
    }

    /**
     * 设置  createBy
     *
     * @param: createBy  创建者
     */
    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取  createDate
     *
     * @return: Date  创建时间
     */
    public Date getCreateDate() {
        return this.createDate;
    }

    /**
     * 设置  createDate
     *
     * @param: createDate  创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取  updateBy
     *
     * @return: User  更新者
     */
    public User getUpdateBy() {
        return this.updateBy;
    }

    /**
     * 设置  updateBy
     *
     * @param: updateBy  更新者
     */
    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取  updateDate
     *
     * @return: Date  更新时间
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * 设置  updateDate
     *
     * @param: updateDate  更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取  delFlag
     *
     * @return: String  删除标记（0：正常；1：删除）
     */
    public String getDelFlag() {
        return this.delFlag;
    }

    /**
     * 设置  delFlag
     *
     * @param: delFlag  删除标记（0：正常；1：删除）
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 获取  remarks
     *
     * @return: String  备注信息
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置  remarks
     *
     * @param: remarks  备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取  areaId
     *
     * @return: String  区域id
     */
    public String getAreaId() {
        return this.areaId;
    }

    /**
     * 设置  areaId
     *
     * @param: areaId  区域id
     */
    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    /**
     * 获取  areaName
     *
     * @return: String  区域name
     */
    public String getAreaName() {
        return this.areaName;
    }

    /**
     * 设置  areaName
     *
     * @param: areaName  区域name
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }


    /**
     * 获取  year
     *
     * @return: Integer  年
     */
    public Integer getYear() {
        return this.year;
    }

    /**
     * 设置  year
     *
     * @param: year  年
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /**
     * 获取  month
     *
     * @return: Integer  月
     */
    public Integer getMonth() {
        return this.month;
    }

    /**
     * 设置  month
     *
     * @param: month  月
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /**
     * 获取  day
     *
     * @return: Integer  天
     */
    public Integer getDay() {
        return this.day;
    }

    /**
     * 设置  day
     *
     * @param: day  天
     */
    public void setDay(Integer day) {
        this.day = day;
    }

    public Double getEconLoss() {
        return econLoss;
    }

    public void setEconLoss(Double econLoss) {
        this.econLoss = econLoss;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }
}
