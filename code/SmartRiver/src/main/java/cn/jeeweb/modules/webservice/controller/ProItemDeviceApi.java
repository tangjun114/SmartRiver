package cn.jeeweb.modules.webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.web.controller.FFTableController;

import cn.jeeweb.modules.monitor.entity.McMonitorDevice;
import cn.jeeweb.modules.supervise.entity.McProItemDevice;

@Controller
@RequestMapping("${api.url.prefix}/proitemdevice")
public class ProItemDeviceApi extends FFTableController<McProItemDevice>
{
	

}
