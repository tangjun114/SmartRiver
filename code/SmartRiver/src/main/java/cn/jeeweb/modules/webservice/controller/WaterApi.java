package cn.jeeweb.modules.webservice.controller;

import cn.jeeweb.modules.water.entity.SrWaterQualityMonitor;
import cn.jeeweb.modules.water.service.ISrWaterQualityMonitorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.common.util.log.FFLogFactory;
import com.ff.common.web.controller.FFBaseController;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.modules.sys.entity.User;

@Controller
@RequestMapping("${api.url.prefix}/water")
public class WaterApi extends FFBaseController {
    protected Logger log = FFLogFactory.getLog(WaterApi.class);

    @Autowired
    ISrWaterQualityMonitorService srWaterQualityMonitorService;

    @RequestMapping("/create")
    @ResponseBody
    public BaseRspJson<SrWaterQualityMonitor> create(@RequestBody SrWaterQualityMonitor obj) {
        BaseRspJson<SrWaterQualityMonitor> rsp = new BaseRspJson<SrWaterQualityMonitor>();

        //SrWaterQualityMonitor monitor = getObj(request, SrWaterQualityMonitor.class);

        srWaterQualityMonitorService.insert(obj);
        rsp.setObj(obj);
        return rsp;
    }

}
