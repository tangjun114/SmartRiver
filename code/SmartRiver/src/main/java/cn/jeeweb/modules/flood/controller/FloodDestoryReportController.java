package cn.jeeweb.modules.flood.controller;


import cn.jeeweb.core.query.data.Condition;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.flood.service.IFloodDestoryReportService;
import com.ff.bi.model.RptDimDataJson;
import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.flood.entity.FloodDestoryReport;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wsh
 * @version V1.0
 * @Title: 水毁报表
 * @Description: 水毁报表
 * @date 2018-11-27 18:19:56
 */
@Controller
@RequestMapping("${admin.url.prefix}/flood/flooddestoryreport")
@RequiresPathPermission("flood:flooddestoryreport")
public class FloodDestoryReportController extends BaseCRUDController<FloodDestoryReport, String> {
    @Autowired
    IFloodDestoryReportService floodDestoryReportService;

    @RequestMapping("/report")
    public String sta(Model model, HttpServletRequest request, HttpServletResponse response) {
        return display("report");
    }

    @RequestMapping("/chart/data")
    @ResponseBody
    public BaseRspJson<List<RptDimDataJson>> rainfullData(@RequestBody BaseReqJson reqJson) {
        BaseRspJson<List<RptDimDataJson>> rsp = new BaseRspJson<>();
        List<RptDimDataJson> dataList = new ArrayList<>();
        dataList.add(getChartData(reqJson));
        dataList.add(getAreaChartData(reqJson));
        rsp.setObj(dataList);
        return rsp;
    }

    private RptDimDataJson getChartData(BaseReqJson request) {
        RptDimDataJson obj = new RptDimDataJson();
        List<FloodDestoryReport> dataList = null;
        List<?> x_data = null;
        List<?> y_data = null;
        String areaId = "";
        String level = "";
        String startTime = "";
        String endTime = "";
        List<Condition.Filter> filters = request.getFilter();
        for (Condition.Filter filter : filters) {
            if ("areaId".equals(filter.getProperty())) {
                areaId = (String) filter.getValue();
            }
            if ("level".equals(filter.getProperty())) {
                level = (String) filter.getValue();
            }
            if ("startTime".equals(filter.getProperty())) {
                startTime = (String) filter.getValue();
            }
            if ("endTime".equals(filter.getProperty())) {
                endTime = (String) filter.getValue();
            }
        }

        EntityWrapper<FloodDestoryReport> entityWrapper1 = new EntityWrapper<FloodDestoryReport>();
        String groupBy = "DATE_FORMAT(record_time,'%Y-%m-%d')";
        if (level.equals("year")) {
            groupBy = "DATE_FORMAT(record_time,'%Y')";
        } else if (level.equals("month")) {
            groupBy = "DATE_FORMAT(record_time,'%Y-%m')";
        }
        entityWrapper1.setSqlSelect("sum(econ_loss) as econLoss,area_id as areaId,area_name as areaName," + groupBy + " as id");
        entityWrapper1.eq("area_id", areaId);
        entityWrapper1.gt("record_time", startTime);
        entityWrapper1.lt("record_time", endTime);

        entityWrapper1.groupBy(groupBy);
        entityWrapper1.orderBy(level, true);
        dataList = floodDestoryReportService.selectList(entityWrapper1);
        x_data = DataFilterUtil.getFieldValueFromList(dataList, "id");
        y_data = DataFilterUtil.getFieldValueFromList(dataList, "econLoss");

        obj.setX_data(x_data);
        obj.setY_data(y_data);


        return obj;

    }

    private RptDimDataJson getAreaChartData(BaseReqJson request) {
        RptDimDataJson obj = new RptDimDataJson();
        List<FloodDestoryReport> dataList = null;
        List<?> x_data = null;
        List<?> y_data = null;
        String areaId = "";
        String level = "";
        String startTime = "";
        String endTime = "";
        List<Condition.Filter> filters = request.getFilter();
        for (Condition.Filter filter : filters) {
            if ("startTime".equals(filter.getProperty())) {
                startTime = (String) filter.getValue();
            }
            if ("endTime".equals(filter.getProperty())) {
                endTime = (String) filter.getValue();
            }
        }

        EntityWrapper<FloodDestoryReport> entityWrapper1 = new EntityWrapper<FloodDestoryReport>();
        String groupBy = "DATE_FORMAT(record_time,'%Y-%m-%d')";
        if (level.equals("year")) {
            groupBy = "DATE_FORMAT(record_time,'%Y')";
        } else if (level.equals("month")) {
            groupBy = "DATE_FORMAT(record_time,'%Y-%m')";
        }
        entityWrapper1.setSqlSelect("sum(econ_loss) as econLoss,area_id as areaId,area_name as areaName");
        entityWrapper1.gt("record_time", startTime);
        entityWrapper1.lt("record_time", endTime);

        entityWrapper1.groupBy("area_id");
        dataList = floodDestoryReportService.selectList(entityWrapper1);
        x_data = DataFilterUtil.getFieldValueFromList(dataList, "areaName");
        y_data = DataFilterUtil.getFieldValueFromList(dataList, "econLoss");

        obj.setX_data(x_data);
        obj.setY_data(y_data);
        obj.setY_name("各区域经济损失占比:");


        return obj;

    }
}
