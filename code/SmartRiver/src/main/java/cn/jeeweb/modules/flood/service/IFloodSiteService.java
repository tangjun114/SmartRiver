package cn.jeeweb.modules.flood.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.flood.entity.FloodSite;

/**   
 * @Title: 监控站点
 * @Description: 监控站点
 * @author wsh
 * @date 2018-11-27 18:04:01
 * @version V1.0   
 *
 */
public interface IFloodSiteService extends ICommonService<FloodSite> {

}

