package cn.jeeweb.modules.repo.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.repo.entity.McRepoReference;
 
/**   
 * @Title: 参考文献数据库控制层接口
 * @Description: 参考文献数据库控制层接口
 * @author shawloong
 * @date 2017-10-04 14:38:30
 * @version V1.0   
 *
 */
public interface McRepoReferenceMapper extends BaseMapper<McRepoReference> {
    
}