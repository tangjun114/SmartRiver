package cn.jeeweb.modules.monitor.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolLog;

/**   
 * @Title: 巡检日志
 * @Description: 巡检日志
 * @author shawloong
 * @date 2017-10-15 23:43:36
 * @version V1.0   
 *
 */
public interface IMcMonitorPatrolLogService extends ICommonService<McMonitorPatrolLog> {

}

