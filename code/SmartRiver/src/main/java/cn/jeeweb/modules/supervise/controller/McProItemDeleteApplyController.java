package cn.jeeweb.modules.supervise.controller;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.model.AjaxJson;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.FileUtil;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.monitor.service.IMcMonitorRealdataService;
import cn.jeeweb.modules.supervise.entity.McProItemAlarmModifyLog;
import cn.jeeweb.modules.supervise.entity.McProItemDeleteApply;
import cn.jeeweb.modules.supervise.service.IMcProItemDeleteApplyService;
import cn.jeeweb.modules.supervise.service.IMcProjectService;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**   
 * @Title: 原始文件删除申请
 * @Description: 原始文件删除申请
 * @author Aether
 * @date 2018-06-05 08:41:22
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemdeleteapply")
@RequiresPathPermission("supervise:mcproitemdeleteapply")
public class McProItemDeleteApplyController extends McProjectBaseController<McProItemDeleteApply, String> {

	@Autowired 
	IMcMonitorRealdataService realdataService;
	
	@Autowired 
	IMcProItemDeleteApplyService deleteApplyService;
	
	@Override
	public void preSave(McProItemDeleteApply entity, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.preSave(entity, request, response);
		
	}

	@Override
	public AjaxJson doSave(McProItemDeleteApply entity, HttpServletRequest request, HttpServletResponse response,
			BindingResult result) {
//		McProItemDeleteApply data = get(entity.getId());
//			
//		if("approved".equals(data.getStatus())) {
//			AjaxJson ajaxjson = new AjaxJson();
//			ajaxjson.fail("申请已审批");
//			return ajaxjson;
//		}
//		if (!StringUtils.isEmpty(data.getFilePath())) {
//			String basePath = ServletUtils.getRequest().getServletContext().getRealPath("/");
//			String filePath = data.getFilePath();
//			FileUtil.delFile(basePath + filePath);
//		}
//		entity.setHandleTime(new Date());
//		entity.setHandleUserId(UserUtils.getUser().getId());
//		entity.setHandleUserName(UserUtils.getUser().getRealname());
//		entity.setStatus("approved");
		return super.doSave(entity, request, response, result);

		
	}

	@Autowired
	private IMcProjectService iMcProjectService;
	@Override
	public void preAjaxList(Queryable queryable, EntityWrapper<McProItemDeleteApply> entityWrapper,
			HttpServletRequest request, HttpServletResponse response) {
 		entityWrapper.in("projectId",  iMcProjectService.getProjectIdListByUser(UserUtils.getUser()));
 		super.preAjaxList(queryable, entityWrapper, request, response);
	}
	@Override
	public void afterSave(McProItemDeleteApply entity, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		super.afterSave(entity, request, response);
		realdataService.deleteById(entity.getDataId());
	}
	
	
	@RequestMapping(value = "/approve", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson approveApply(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String gid = request.getParameter("gid");
		McProItemDeleteApply data = get(gid);
		AjaxJson ajaxjson = new AjaxJson();
		ajaxjson.success("审批成功");
		if("approved".equals(data.getStatus())) {		
			ajaxjson.fail("申请已审批");
			return ajaxjson;
		}
		if (!StringUtils.isEmpty(data.getFilePath())) {
			String basePath = ServletUtils.getRequest().getServletContext().getRealPath("/");
			String filePath = data.getFilePath();
			FileUtil.delFile(basePath + filePath);
		}
		data.setHandleTime(new Date());
		data.setHandleUserId(UserUtils.getUser().getId());
		data.setHandleUserName(UserUtils.getUser().getRealname());
		data.setStatus("approved");
		
		realdataService.deleteById(data.getDataId());
		
		deleteApplyService.updateById(data);
				
		return ajaxjson;
	}
	
	

}
