package cn.jeeweb.modules.supervise.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.supervise.mapper.McProItemCollectionMapper;
import cn.jeeweb.modules.supervise.entity.McProItemCollection;
import cn.jeeweb.modules.supervise.service.IMcProItemCollectionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 项目中的通信设备
 * @Description: 项目中的通信设备
 * @author shawloong
 * @date 2018-03-20 11:42:24
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemCollectionService")
public class McProItemCollectionServiceImpl  extends CommonServiceImpl<McProItemCollectionMapper,McProItemCollection> implements  IMcProItemCollectionService {

}
