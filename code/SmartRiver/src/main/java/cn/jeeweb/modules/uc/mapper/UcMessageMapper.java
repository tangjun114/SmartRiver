package cn.jeeweb.modules.uc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.uc.entity.UcMessage;
 
/**   
 * @Title: 个人中心消息数据库控制层接口
 * @Description: 个人中心消息数据库控制层接口
 * @author shawloong
 * @date 2017-09-30 00:04:45
 * @version V1.0   
 *
 */
public interface UcMessageMapper extends BaseMapper<UcMessage> {
    
}