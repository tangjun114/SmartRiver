package cn.jeeweb.modules.supervise.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;

import cn.jeeweb.modules.supervise.entity.McProItemRealTimeTotalData;
 
/**   
 * @Title: 项目监控-监测情况-实时监测数据统计数据库控制层接口
 * @Description: 项目监控-监测情况-实时监测数据统计数据库控制层接口
 * @author shawloong
 * @date 2018-03-06 20:19:22
 * @version V1.0   
 *
 */
public interface McProItemRealTimeTotalDataMapper extends BaseMapper<McProItemRealTimeTotalData> {
 
}