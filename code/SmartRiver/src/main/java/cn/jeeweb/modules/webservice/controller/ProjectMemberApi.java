package cn.jeeweb.modules.webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ff.common.web.controller.FFTableController;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.modules.supervise.entity.McProItemMember;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;

@Controller
@RequestMapping("${api.url.prefix}/projectmember")
public class ProjectMemberApi extends FFTableController<McProItemMember>
{

	@Override
	public BaseRspJson<McProItemMember> Create(BaseReqJson<McProItemMember> request) {
		// TODO Auto-generated method stub
		return super.Create(request);
	}
 
	
}
