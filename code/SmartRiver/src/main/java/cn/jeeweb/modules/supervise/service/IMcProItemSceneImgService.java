package cn.jeeweb.modules.supervise.service;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.modules.supervise.entity.McProItemSceneImg;

/**   
 * @Title: 现场照片
 * @Description: 现场照片
 * @author shawloong
 * @date 2017-11-02 21:37:47
 * @version V1.0   
 *
 */
public interface IMcProItemSceneImgService extends ICommonService<McProItemSceneImg> {

}

