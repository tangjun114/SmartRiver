package cn.jeeweb.modules.law.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.law.mapper.LawCheckResultMapper;
import cn.jeeweb.modules.law.entity.LawCheckResult;
import cn.jeeweb.modules.law.service.ILawCheckResultService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 巡查结果
 * @Description: 巡查结果
 * @author liyonglei
 * @date 2018-11-13 20:40:25
 * @version V1.0   
 *
 */
@Transactional
@Service("lawCheckResultService")
public class LawCheckResultServiceImpl  extends CommonServiceImpl<LawCheckResultMapper,LawCheckResult> implements  ILawCheckResultService {

}
