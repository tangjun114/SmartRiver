package cn.jeeweb.modules.monitor.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.monitor.mapper.McMonitorPatrolDetailMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorPatrolDetail;
import cn.jeeweb.modules.monitor.service.IMcMonitorPatrolDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 巡检详情
 * @Description: 巡检详情
 * @author Aether
 * @date 2018-02-03 11:28:27
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorPatrolDetailService")
public class McMonitorPatrolDetailServiceImpl  extends CommonServiceImpl<McMonitorPatrolDetailMapper,McMonitorPatrolDetail> implements  IMcMonitorPatrolDetailService {

}
