package cn.jeeweb.modules.monitor.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.monitor.mapper.McMonitorCollectionMapper;
import cn.jeeweb.modules.monitor.entity.McMonitorCollection;
import cn.jeeweb.modules.monitor.service.IMcMonitorCollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.jeeweb.core.utils.ServletUtils;
import cn.jeeweb.core.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;

/**   
 * @Title: 通信设备
 * @Description: 通信设备
 * @author shawloong
 * @date 2018-01-28 21:44:38
 * @version V1.0   
 *
 */
@Transactional
@Service("mcMonitorCollectionService")
public class McMonitorCollectionServiceImpl  extends CommonServiceImpl<McMonitorCollectionMapper,McMonitorCollection> implements  IMcMonitorCollectionService {
	
	@Override
	public boolean insert(McMonitorCollection mcMonitorCollection) {
		// 保存主表
		super.insert(mcMonitorCollection);
		return true;
	}
	
	@Override
	public boolean insertOrUpdate(McMonitorCollection mcMonitorCollection) {
		try {
			// 更新主表
			super.insertOrUpdate(mcMonitorCollection);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}
	
	
	
}
