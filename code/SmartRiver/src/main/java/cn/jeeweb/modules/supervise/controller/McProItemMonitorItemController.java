package cn.jeeweb.modules.supervise.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.common.util.format.JsonConvert;

import cn.jeeweb.cache.MonitorTypeParaCache;
import cn.jeeweb.core.model.AjaxJson;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.monitor.entity.McMonitorRealdata;
import cn.jeeweb.modules.monitor.entity.McProItemAlarm;
import cn.jeeweb.modules.monitor.service.IMcMonitorRealdataService;
import cn.jeeweb.modules.monitor.service.IMcProItemAlarmService;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;
import cn.jeeweb.modules.supervise.service.IMcProItemMeasuringPointService;
import cn.jeeweb.modules.supervise.service.IMcProItemMonitorItemService;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeMonDataService;

/**   
 * @Title: 项目监控-测点设置-监测项
 * @Description: 项目监控-测点设置-监测项
 * @author shawloong
 * @date 2017-11-05 19:41:39
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemmonitoritem")
@RequiresPathPermission("supervise:mcproitemmonitoritem")
public class McProItemMonitorItemController extends McProjectBaseController<McProItemMonitorItem, String> {
 
//	@Autowired
//	private MonitorTypeParaCache cache;
	
	@Autowired
	private IMcProItemMonitorItemService monitorItemService;
	
	@Autowired
	private IMcMonitorRealdataService realDataService;
	
	@Autowired
	private IMcProItemRealTimeMonDataService monDataService;
	
	
	@Autowired
	private IMcProItemMeasuringPointService pointService;
	
	@Autowired
	private IMcProItemAlarmService alarmService;
	
//	@Autowired
//	private IMcProItemMeasuringPointService mpService;
	
	
	@Override
	public void preSave(McProItemMonitorItem entity, HttpServletRequest request, HttpServletResponse response) {
 
 		super.preSave(entity, request, response);
	}
	
	@Autowired
	private MonitorTypeParaCache monitorTypeParaCache;
	@RequestMapping(value = "/clear", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson clearData(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String errorMsg = "";
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.success("操作成功");
		String id = request.getParameter("gid");
		boolean result = true;
		try {
			McProItemMonitorItem entity = get(id);
			//清除monitor_item下的monitor_count,清除监测次数
			entity.setMonitorCount("");
			
			result = monitorItemService.updateById(entity);
		} catch (Exception e2) {
			errorMsg += "清除监测次数;";
			e2.printStackTrace();
		}

		//清除监测项对应的原始数据文件列表；		
		
		try {
			EntityWrapper<McProItemMeasuringPoint> wrapper = new EntityWrapper<McProItemMeasuringPoint>();
			wrapper.eq("monitor_item_id", id);
			List<McProItemMeasuringPoint> pointList = pointService.selectList(wrapper);
			for(McProItemMeasuringPoint point : pointList)
			{
				point.setInitialCumulativeValue(0.0);
 				point.setInitValue(0.0);
				point.setXvalue(0.0);
				point.setYvalue(0.0);
				if(monitorTypeParaCache.isShift(point.getMonitorItemTypeCode()))
				{
					List<McProItemMeasuringPoint> initDeptList = JsonConvert.jsonToObject(point.getDepthExt(), List.class,McProItemMeasuringPoint.class);
					for(McProItemMeasuringPoint e :initDeptList)
					{
						e.setInitialCumulativeValue(0.0);
						e.setXvalue(0.0);
						e.setInitValue(0.0);
						e.setYvalue(0.0);
 					}
					point.setDepthExt(JsonConvert.ObjectToJson(initDeptList));
				}
			}
			pointService.updateBatchById(pointList);
			
		} catch (Exception e1) {
			errorMsg += "清楚X Y 值;";
			e1.printStackTrace();
		}
		//清除监测项对应的原始数据文件列表；		
		try {
			EntityWrapper<McMonitorRealdata> entityWrapper1 = new EntityWrapper<McMonitorRealdata>();
			entityWrapper1.eq("monitor_item_id", id);
			result = realDataService.delete(entityWrapper1);
		} catch (Exception e1) {
			errorMsg += "清除原始数据文件;";
			e1.printStackTrace();
		}

		//清除mon_data对应监测项所有数据；
		try {
			EntityWrapper<McProItemRealTimeMonData> entityWrapper2 = new EntityWrapper<McProItemRealTimeMonData>();
			entityWrapper2.eq("monitor_item_id", id);
			result = monDataService.delete(entityWrapper2);
		} catch (Exception e) {
			errorMsg += "清除实时监控数据;";
			e.printStackTrace();
		}

		
		//清除该监测项对应的报警列表
		try {
			EntityWrapper<McProItemAlarm> entityWrapper3 = new EntityWrapper<McProItemAlarm>();
			entityWrapper3.eq("monitor_item_id", id);
			result = alarmService.delete(entityWrapper3);
		} catch (Exception e) {
			errorMsg += "清除报警数据;";
			e.printStackTrace();
		}
		//恢复相关数值至初始值：测点累计变化值，初始累计值
			
		if (errorMsg != null && !"".equals(errorMsg)) {
			ajaxJson.fail(errorMsg+"操作失败");
		}

		return ajaxJson;
	}
}
