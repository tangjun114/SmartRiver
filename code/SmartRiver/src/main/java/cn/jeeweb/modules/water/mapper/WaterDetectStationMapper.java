package cn.jeeweb.modules.water.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.water.entity.WaterDetectStation;
 
/**   
 * @Title: 监测站管理数据库控制层接口
 * @Description: 监测站管理数据库控制层接口
 * @author liyonglei
 * @date 2018-11-28 19:41:24
 * @version V1.0   
 *
 */
public interface WaterDetectStationMapper extends BaseMapper<WaterDetectStation> {
    
}