package cn.jeeweb.modules.supervise.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.ff.common.util.format.DateUtil;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.QueryUtil;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.utils.QueryableConvertUtils;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.modules.supervise.entity.McProItemMonitorItem;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;
import cn.jeeweb.modules.supervise.entity.McProItemRealTimeTotalData;
import cn.jeeweb.modules.supervise.mapper.McProItemRealTimeTotalDataMapper;
import cn.jeeweb.modules.supervise.service.IMcProItemMonitorItemService;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeMonDataService;
import cn.jeeweb.modules.supervise.service.IMcProItemRealTimeTotalDataService;

/**   
 * @Title: 项目监控-监测情况-实时监测数据统计
 * @Description: 项目监控-监测情况-实时监测数据统计
 * @author shawloong
 * @date 2018-03-06 20:19:22
 * @version V1.0   
 *
 */
@Transactional
@Service("mcProItemRealTimeTotalDataService")
public class McProItemRealTimeTotalDataServiceImpl  extends CommonServiceImpl<McProItemRealTimeTotalDataMapper,McProItemRealTimeTotalData> implements  IMcProItemRealTimeTotalDataService {

	public final Logger log = Logger.getLogger(this.getClass());
	@Autowired
	IMcProItemRealTimeMonDataService iMcProItemRealTimeMonDataService;
	
	@Autowired
	IMcProItemMonitorItemService iMcProItemMonitorItemService;
	@Autowired 
	McProItemRealTimeTotalDataMapper mcProItemRealTimeTotalDataMapper;
	public void run()
	{
		Date today = DateUtil.getCurrentDate();
		Date yesterday = DateUtil.dayCalculate(today, -1);
		String todayStr = DateUtil.DateToString(today, "yyy-MM-dd");
		String yesStr = DateUtil.DateToString(yesterday, "yyy-MM-dd");
		
		
		EntityWrapper<McProItemRealTimeMonData> entityWrapper = new EntityWrapper<McProItemRealTimeMonData>(McProItemRealTimeMonData.class);
		entityWrapper.lt("collect_time", todayStr);
		entityWrapper.ge("collect_time", yesStr);

		entityWrapper.setSqlSelect("select count(id) as project_count");
		entityWrapper.groupBy("monitor_item_id");
		List<McProItemRealTimeMonData> dataList = iMcProItemRealTimeMonDataService.selectList(entityWrapper );
		
		
		List<McProItemRealTimeTotalData> objList = new ArrayList<McProItemRealTimeTotalData>();
		for(McProItemRealTimeMonData e :dataList)
		{
			McProItemRealTimeTotalData data = new McProItemRealTimeTotalData();
			data.setCount(e.getProjectCount());
			data.setMonitorItemId(e.getMonitorItemId());
			data.setMonitorItemName(e.getMonitorItemName());
			data.setProjectId(e.getProjectId());
			data.setProjectName(e.getProjectName());
			data.setDay(yesStr);
			objList.add(data);
		}
		this.insertBatch(objList);
	}
	@Override
	public List<Map<String,Object>> getSumData(Queryable qy) {

 		//entityWrapper.lt("collect_time", todayStr);
		//entityWrapper.ge("collect_time", yesStr);
		Wrapper<McProItemMonitorItem> itemWraper = new EntityWrapper<McProItemMonitorItem>();
		
		itemWraper.eq("project_id", qy.getValue("projectId"));
		//itemWraper.eq("project_id", qy.getValue("projectId"));

		List<McProItemMonitorItem> itemList = iMcProItemMonitorItemService.selectList(itemWraper);

		//wrapper.setSqlSelect("distinct monitor_item_name as monitorItemName ");
		//wrapper.groupBy("monitor_item_id");
		//List<McProItemRealTimeTotalData> itemList = this.selectList(wrapper);
		String sql ="ifnull(day,\'汇总\') AS 日期,";
		for(McProItemMonitorItem e : itemList)
		{
			sql += "SUM(IF(monitor_item_name='"+e.getMonitorItemName()+"',count,0)) AS '"+ e.getMonitorItemName() + "', ";
		}
		sql += " SUM(count) AS 汇总";
		
		
 		
		Wrapper<McProItemRealTimeTotalData> wrapper = QueryUtil.getWrapper(qy, McProItemRealTimeTotalData.class);
  
		wrapper.setSqlSelect(sql);
		wrapper.groupBy("day WITH ROLLUP");
//		
    	List<Map<String,Object>> data = this.selectMaps(wrapper);

		return data ;
	}
	
	
}
