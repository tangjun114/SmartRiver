package cn.jeeweb.modules.supervise.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.supervise.entity.McProItemOtherDevice;
 
/**   
 * @Title: 项目中的其他设备数据库控制层接口
 * @Description: 项目中的其他设备数据库控制层接口
 * @author jerry
 * @date 2018-01-28 18:52:59
 * @version V1.0   
 *
 */
public interface McProItemOtherDeviceMapper extends BaseMapper<McProItemOtherDevice> {
    
}