package cn.jeeweb.modules.law.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.law.entity.LawRoutePlan;

/**   
 * @Title: 路线规划
 * @Description: 路线规划
 * @author 李永雷
 * @date 2018-11-13 09:08:03
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/law/lawrouteplan")
@RequiresPathPermission("law:lawrouteplan")
public class LawRoutePlanController extends BaseCRUDController<LawRoutePlan, String> {

}
