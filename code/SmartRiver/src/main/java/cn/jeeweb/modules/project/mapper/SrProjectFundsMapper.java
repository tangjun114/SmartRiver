package cn.jeeweb.modules.project.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import cn.jeeweb.modules.project.entity.SrProjectFunds;
 
/**   
 * @Title: 工程经费数据库控制层接口
 * @Description: 工程经费数据库控制层接口
 * @author wsh
 * @date 2018-12-10 20:42:10
 * @version V1.0   
 *
 */
public interface SrProjectFundsMapper extends BaseMapper<SrProjectFunds> {
    
}