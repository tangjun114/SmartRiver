package cn.jeeweb.modules.supervise.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringHole;
import cn.jeeweb.modules.supervise.entity.McProItemMeasuringPoint;

/**   
 * @Title: 项目监控-测点设置-监测项-测孔
 * @Description: 项目监控-测点设置-监测项-测孔
 * @author shawloong
 * @date 2017-11-05 23:21:42
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/supervise/mcproitemmeasuringhole")
@RequiresPathPermission("supervise:mcproitemmeasuringhole")
public class McProItemMeasuringHoleController extends McProjectBaseController<McProItemMeasuringHole, String> {
	
 
 
	@Override
	public String display(String suffixName) {
		if("edit".endsWith(suffixName)){
			suffixName = "edit_self";
		}
		if (!suffixName.startsWith("/")) {
			
			suffixName = "/" + suffixName;
		}
		return getViewPrefix().toLowerCase() + suffixName;
	}
}
