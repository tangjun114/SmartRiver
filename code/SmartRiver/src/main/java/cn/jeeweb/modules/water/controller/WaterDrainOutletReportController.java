package cn.jeeweb.modules.water.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.jeeweb.core.common.controller.BaseCRUDController;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresPathPermission;
import cn.jeeweb.modules.water.entity.WaterDrainOutletReport;

/**   
 * @Title: 排污口检测报表
 * @Description: 排污口检测报表
 * @author wsh
 * @date 2019-03-19 19:09:33
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("${admin.url.prefix}/water/waterdrainoutletreport")
@RequiresPathPermission("water:waterdrainoutletreport")
public class WaterDrainOutletReportController extends BaseCRUDController<WaterDrainOutletReport, String> {

}
