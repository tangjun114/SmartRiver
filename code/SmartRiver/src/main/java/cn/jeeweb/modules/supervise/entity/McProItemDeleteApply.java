package cn.jeeweb.modules.supervise.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 原始文件删除申请
 * @Description: 原始文件删除申请
 * @author Aether
 * @date 2018-06-08 23:18:00
 * @version V1.0   
 *
 */
@TableName("mc_pro_item_delete_apply")
@SuppressWarnings("serial")
public class McProItemDeleteApply extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**创建时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**数据id*/
    @TableField(value = "data_id")
	private String dataId;
    /**申请人*/
    @TableField(value = "apply_user_id")
	private String applyUserId;
    /**申请人名称*/
    @TableField(value = "apply_user_name")
	private String applyUserName;
    /**状态*/
    @TableField(value = "status")
	private String status;
    /**原因*/
    @TableField(value = "reason")
	private String reason;
    /**审批人*/
    @TableField(value = "handle_user_id")
	private String handleUserId;
    /**审批人名称*/
    @TableField(value = "handle_user_name")
	private String handleUserName;
    /**审批时间*/
    @TableField(value = "handle_time")
	private Date handleTime;
    /**项目id*/
    @TableField(value = "project_id")
	private String projectId;
    /**项目名称*/
    @TableField(value = "project_name")
	private String projectName;
    /**监测项id*/
    @TableField(value = "monitor_item_id")
	private String monitorItemId;
    /**监测项名称*/
    @TableField(value = "monitor_item_name")
	private String monitorItemName;
    /**测点*/
    @TableField(value = "point")
	private String point;
    /**测点数量*/
    @TableField(value = "point_num")
	private Integer pointNum;
    /**监测数量*/
    @TableField(value = "monitor_count")
	private Integer monitorCount;
    /**申请文件*/
    @TableField(value = "apply_file")
	private String applyFile;
    /**组织id*/
    @TableField(value = "monitor_id")
	private String monitorId;
    /**组织名称*/
    @TableField(value = "monitor_name")
	private String monitorName;
    /**检测结果*/
    @TableField(value = "measure_result")
	private String measureResult;
    /**文件名称*/
    @TableField(value = "file_name")
	private String fileName;
    /**文件路径*/
    @TableField(value = "file_path")
	private String filePath;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  createDate
	 *@return: Date  创建时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  dataId
	 *@return: String  数据id
	 */
	public String getDataId(){
		return this.dataId;
	}

	/**
	 * 设置  dataId
	 *@param: dataId  数据id
	 */
	public void setDataId(String dataId){
		this.dataId = dataId;
	}
	/**
	 * 获取  applyUserId
	 *@return: String  申请人
	 */
	public String getApplyUserId(){
		return this.applyUserId;
	}

	/**
	 * 设置  applyUserId
	 *@param: applyUserId  申请人
	 */
	public void setApplyUserId(String applyUserId){
		this.applyUserId = applyUserId;
	}
	/**
	 * 获取  applyUserName
	 *@return: String  申请人名称
	 */
	public String getApplyUserName(){
		return this.applyUserName;
	}

	/**
	 * 设置  applyUserName
	 *@param: applyUserName  申请人名称
	 */
	public void setApplyUserName(String applyUserName){
		this.applyUserName = applyUserName;
	}
	/**
	 * 获取  status
	 *@return: String  状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  reason
	 *@return: String  原因
	 */
	public String getReason(){
		return this.reason;
	}

	/**
	 * 设置  reason
	 *@param: reason  原因
	 */
	public void setReason(String reason){
		this.reason = reason;
	}
	/**
	 * 获取  handleUserId
	 *@return: String  审批人
	 */
	public String getHandleUserId(){
		return this.handleUserId;
	}

	/**
	 * 设置  handleUserId
	 *@param: handleUserId  审批人
	 */
	public void setHandleUserId(String handleUserId){
		this.handleUserId = handleUserId;
	}
	/**
	 * 获取  handleUserName
	 *@return: String  审批人名称
	 */
	public String getHandleUserName(){
		return this.handleUserName;
	}

	/**
	 * 设置  handleUserName
	 *@param: handleUserName  审批人名称
	 */
	public void setHandleUserName(String handleUserName){
		this.handleUserName = handleUserName;
	}
	/**
	 * 获取  handleTime
	 *@return: Date  审批时间
	 */
	public Date getHandleTime(){
		return this.handleTime;
	}

	/**
	 * 设置  handleTime
	 *@param: handleTime  审批时间
	 */
	public void setHandleTime(Date handleTime){
		this.handleTime = handleTime;
	}
	/**
	 * 获取  projectId
	 *@return: String  项目id
	 */
	public String getProjectId(){
		return this.projectId;
	}

	/**
	 * 设置  projectId
	 *@param: projectId  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 * 获取  projectName
	 *@return: String  项目名称
	 */
	public String getProjectName(){
		return this.projectName;
	}

	/**
	 * 设置  projectName
	 *@param: projectName  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 * 获取  monitorItemId
	 *@return: String  监测项id
	 */
	public String getMonitorItemId(){
		return this.monitorItemId;
	}

	/**
	 * 设置  monitorItemId
	 *@param: monitorItemId  监测项id
	 */
	public void setMonitorItemId(String monitorItemId){
		this.monitorItemId = monitorItemId;
	}
	/**
	 * 获取  monitorItemName
	 *@return: String  监测项名称
	 */
	public String getMonitorItemName(){
		return this.monitorItemName;
	}

	/**
	 * 设置  monitorItemName
	 *@param: monitorItemName  监测项名称
	 */
	public void setMonitorItemName(String monitorItemName){
		this.monitorItemName = monitorItemName;
	}
	/**
	 * 获取  point
	 *@return: String  测点
	 */
	public String getPoint(){
		return this.point;
	}

	/**
	 * 设置  point
	 *@param: point  测点
	 */
	public void setPoint(String point){
		this.point = point;
	}
	/**
	 * 获取  pointNum
	 *@return: Integer  测点数量
	 */
	public Integer getPointNum(){
		return this.pointNum;
	}

	/**
	 * 设置  pointNum
	 *@param: pointNum  测点数量
	 */
	public void setPointNum(Integer pointNum){
		this.pointNum = pointNum;
	}
	/**
	 * 获取  monitorCount
	 *@return: Integer  监测数量
	 */
	public Integer getMonitorCount(){
		return this.monitorCount;
	}

	/**
	 * 设置  monitorCount
	 *@param: monitorCount  监测数量
	 */
	public void setMonitorCount(Integer monitorCount){
		this.monitorCount = monitorCount;
	}
	/**
	 * 获取  applyFile
	 *@return: String  申请文件
	 */
	public String getApplyFile(){
		return this.applyFile;
	}

	/**
	 * 设置  applyFile
	 *@param: applyFile  申请文件
	 */
	public void setApplyFile(String applyFile){
		this.applyFile = applyFile;
	}
	/**
	 * 获取  monitorId
	 *@return: String  组织id
	 */
	public String getMonitorId(){
		return this.monitorId;
	}

	/**
	 * 设置  monitorId
	 *@param: monitorId  组织id
	 */
	public void setMonitorId(String monitorId){
		this.monitorId = monitorId;
	}
	/**
	 * 获取  monitorName
	 *@return: String  组织名称
	 */
	public String getMonitorName(){
		return this.monitorName;
	}

	/**
	 * 设置  monitorName
	 *@param: monitorName  组织名称
	 */
	public void setMonitorName(String monitorName){
		this.monitorName = monitorName;
	}
	/**
	 * 获取  measureResult
	 *@return: String  检测结果
	 */
	public String getMeasureResult(){
		return this.measureResult;
	}

	/**
	 * 设置  measureResult
	 *@param: measureResult  检测结果
	 */
	public void setMeasureResult(String measureResult){
		this.measureResult = measureResult;
	}
	/**
	 * 获取  fileName
	 *@return: String  文件名称
	 */
	public String getFileName(){
		return this.fileName;
	}

	/**
	 * 设置  fileName
	 *@param: fileName  文件名称
	 */
	public void setFileName(String fileName){
		this.fileName = fileName;
	}
	/**
	 * 获取  filePath
	 *@return: String  文件路径
	 */
	public String getFilePath(){
		return this.filePath;
	}

	/**
	 * 设置  filePath
	 *@param: filePath  文件路径
	 */
	public void setFilePath(String filePath){
		this.filePath = filePath;
	}
	
}
