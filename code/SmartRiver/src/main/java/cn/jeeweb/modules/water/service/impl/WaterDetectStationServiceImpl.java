package cn.jeeweb.modules.water.service.impl;

import cn.jeeweb.core.common.service.impl.CommonServiceImpl;
import cn.jeeweb.modules.water.mapper.WaterDetectStationMapper;
import cn.jeeweb.modules.water.entity.WaterDetectStation;
import cn.jeeweb.modules.water.service.IWaterDetectStationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @Title: 监测站管理
 * @Description: 监测站管理
 * @author liyonglei
 * @date 2018-11-28 19:41:24
 * @version V1.0   
 *
 */
@Transactional
@Service("waterDetectStationService")
public class WaterDetectStationServiceImpl  extends CommonServiceImpl<WaterDetectStationMapper,WaterDetectStation> implements  IWaterDetectStationService {

}
