package cn.jeeweb.modules.uc.entity;

import cn.jeeweb.core.common.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import cn.jeeweb.modules.sys.entity.User;
import java.util.Date;

/**   
 * @Title: 个人中心消息
 * @Description: 个人中心消息
 * @author shawloong
 * @date 2017-09-30 00:04:45
 * @version V1.0   
 *
 */
@TableName("uc_message")
@SuppressWarnings("serial")
public class UcMessage extends AbstractEntity<String> {

    /**字段主键*/
    @TableId(value = "id", type = IdType.UUID)
	private String id;
    /**创建者*/
    @TableField(value = "create_by",el="createBy.id")
	private User createBy;
    /**更新者*/
    @TableField(value = "update_by",el="updateBy.id")
	private User updateBy;
    /**更新时间*/
    @TableField(value = "update_date")
	private Date updateDate;
    /**删除标记（0：正常；1：删除）*/
    @TableField(value = "del_flag")
	private String delFlag;
    /**备注信息*/
    @TableField(value = "remarks")
	private String remarks;
    /**信息*/
    @TableField(value = "message")
	private String message;
    /**消息状态*/
    @TableField(value = "status")
	private String status;
    /**时间*/
    @TableField(value = "create_date")
	private Date createDate;
    /**消息状态*/
    @TableField(value = "messageType")
	private String messageType;
    /**收信人(持有人)*/
    @TableField(value = "recipients",el="recipients.id")
	private User recipients;
	
	/**
	 * 获取  id
	 *@return: String  字段主键
	 */
	public String getId(){
		return this.id;
	}

	/**
	 * 设置  id
	 *@param: id  字段主键
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 获取  createBy
	 *@return: User  创建者
	 */
	public User getCreateBy(){
		return this.createBy;
	}

	/**
	 * 设置  createBy
	 *@param: createBy  创建者
	 */
	public void setCreateBy(User createBy){
		this.createBy = createBy;
	}
	/**
	 * 获取  updateBy
	 *@return: User  更新者
	 */
	public User getUpdateBy(){
		return this.updateBy;
	}

	/**
	 * 设置  updateBy
	 *@param: updateBy  更新者
	 */
	public void setUpdateBy(User updateBy){
		this.updateBy = updateBy;
	}
	/**
	 * 获取  updateDate
	 *@return: Date  更新时间
	 */
	public Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 * 设置  updateDate
	 *@param: updateDate  更新时间
	 */
	public void setUpdateDate(Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 * 获取  delFlag
	 *@return: String  删除标记（0：正常；1：删除）
	 */
	public String getDelFlag(){
		return this.delFlag;
	}

	/**
	 * 设置  delFlag
	 *@param: delFlag  删除标记（0：正常；1：删除）
	 */
	public void setDelFlag(String delFlag){
		this.delFlag = delFlag;
	}
	/**
	 * 获取  remarks
	 *@return: String  备注信息
	 */
	public String getRemarks(){
		return this.remarks;
	}

	/**
	 * 设置  remarks
	 *@param: remarks  备注信息
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	/**
	 * 获取  message
	 *@return: String  信息
	 */
	public String getMessage(){
		return this.message;
	}

	/**
	 * 设置  message
	 *@param: message  信息
	 */
	public void setMessage(String message){
		this.message = message;
	}
	/**
	 * 获取  status
	 *@return: String  消息状态
	 */
	public String getStatus(){
		return this.status;
	}

	/**
	 * 设置  status
	 *@param: status  消息状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 * 获取  createDate
	 *@return: Date  时间
	 */
	public Date getCreateDate(){
		return this.createDate;
	}

	/**
	 * 设置  createDate
	 *@param: createDate  时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 * 获取  messageType
	 *@return: String  消息状态
	 */
	public String getMessageType(){
		return this.messageType;
	}

	/**
	 * 设置  messageType
	 *@param: messageType  消息状态
	 */
	public void setMessageType(String messageType){
		this.messageType = messageType;
	}
	/**
	 * 获取  recipients
	 *@return: User  收信人(持有人)
	 */
	public User getRecipients(){
		return this.recipients;
	}

	/**
	 * 设置  recipients
	 *@param: recipients  收信人(持有人)
	 */
	public void setRecipients(User recipients){
		this.recipients = recipients;
	}
	
}
