package cn.jeeweb.core.utils.sms.sender;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.ff.common.util.format.JsonConvert;

import cn.jeeweb.core.utils.PropertiesUtil;
import cn.jeeweb.core.utils.sms.data.SmsResult;
import cn.jeeweb.core.utils.sms.data.SmsTemplate;
import cn.jeeweb.core.utils.sms.sender.cloopen.CCPRestSDK;

public class AlidayuSmsSender extends SmsSender {

    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
    static final String accessKeyId = "LTAIZTdtxYAewzGm";
    static final String accessKeySecret = "OWxEoEDE2kyVP13HfLMzflkSdv29Dz";
    
    static final String sign = "广东云监";
    
	protected Logger logger = LoggerFactory.getLogger(getClass());
 
	public AlidayuSmsSender() {
		try {
			init();
		} catch (Exception e) {

		}
	}

	@Override
	public void init() {
	}

	@Override
	public SmsResult send(String phone, SmsTemplate smsTemplate, String... datas) {
		HashMap<String, Object> result = null;
		
		String templateId = smsTemplate.getTemplateId();
		 
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        SmsResult requestResult = new SmsResult();
        requestResult.setSuccess(true);
        try
        {
        	
        
        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
    
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(sign);
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(templateId);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        //request.setTemplateParam("{\"name\":\"Tom\", \"code\":\"123\"}");
        
        if(null != datas && datas.length>0)
        {
        	request.setTemplateParam(datas[0]);
        }
        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");

        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId("yourOutId");

        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
        
        logger.info("the message is "+JsonConvert.ObjectToJson(sendSmsResponse));
        requestResult.setCode(sendSmsResponse.getCode());
        requestResult.setMsg(sendSmsResponse.getMessage());
        requestResult.setSmsid(sendSmsResponse.getRequestId());
        if(sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK"))
        {
        	 requestResult.setSuccess(true);
         }
        else
        {
        	requestResult.setSuccess(false);
        }
        }
        catch(Exception e)
        {
        	logger.error("send message error",e);
        	requestResult.setSuccess(false);
        	requestResult.setMsg(e.getMessage());
        }
		return requestResult;
	}

	private SmsResult mapToResult(Map<String, Object> result) {
		SmsResult requestResult = new SmsResult();
		requestResult.setSuccess(Boolean.FALSE);
		requestResult.setSenderName(name());
		if (result != null) {
			if ("000000".equals(result.get("statusCode"))) {
				requestResult.setCode(result.get("statusCode") + "");
				requestResult.setMsg("发送成功");
				// 正常返回输出data包体信息（map）
				HashMap<String, Object> data = (HashMap<String, Object>) result.get("data");
				requestResult.setSmsid(data.get("smsMessageSid") + "");
				requestResult.setSuccess(Boolean.TRUE);
			} else {
				requestResult.setSuccess(Boolean.FALSE);
				requestResult.setCode(result.get("statusCode") + "");
				requestResult.setMsg(result.get("statusMsg") + "");
			}
		}
		return requestResult;
	}

	@Override
	protected String name() {
		return "Alidayu";
	}

	public static void main(String[] args) {
		// 70585
		SmsTemplate smsTemplate = SmsTemplate.newTemplateById("70585");
		CCPSmsSender ccpSmsSender = new CCPSmsSender();
		ccpSmsSender.send("15085980308,18166727295", smsTemplate, "2345");
	}
}
