package cn.jeeweb.core.common.controller;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import cn.jeeweb.core.query.data.Sort;
import com.ff.common.service.model.FFExcelMeta;
import com.ff.common.util.file.ExcelUtil;
import com.ff.common.util.file.FileExportUtil;
import org.quartz.SimpleTrigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.ff.common.util.validate.ValidatorUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.core.common.data.DuplicateValid;
import cn.jeeweb.core.common.entity.AbstractEntity;
import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.core.model.AjaxJson;
import cn.jeeweb.core.model.PageJson;
import cn.jeeweb.core.model.ValidJson;
import cn.jeeweb.core.query.annotation.PageableDefaults;
import cn.jeeweb.core.query.data.PropertyPreFilterable;
import cn.jeeweb.core.query.data.QueryUtil;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.core.query.utils.QueryableConvertUtils;
import cn.jeeweb.core.query.wrapper.EntityWrapper;
import cn.jeeweb.core.security.shiro.authz.annotation.RequiresMethodPermissions;
import cn.jeeweb.core.utils.JeewebPropertiesUtil;
import cn.jeeweb.core.utils.ObjectUtils;
import cn.jeeweb.core.utils.StringUtils;
import cn.jeeweb.modules.sys.utils.UserUtils;

public abstract class BaseCRUDController<Entity extends AbstractEntity<ID>, ID extends Serializable>
        extends BaseBeanController<Entity> {

    protected ICommonService<Entity> commonService;

    /**
     * 设置基础service
     *
     * @param
     */
    @Autowired
    public void setCommonService(ICommonService<Entity> commonService) {
        this.commonService = commonService;
    }


    public ICommonService<Entity> getCommonService() {
        return commonService;
    }


    public Entity get(ID id) {
        if (!ObjectUtils.isNullOrEmpty(id)) {
            return commonService.selectById(id);
        } else {
            return newModel();
        }
    }

    /**
     * list 运行之前
     *
     * @param model
     * @param request
     * @param response
     */
    public void preList(Model model, HttpServletRequest request, HttpServletResponse response) {

        this.setAllAttribute(model, request, response);
    }

    protected void setAllAttribute(Model model, HttpServletRequest request, HttpServletResponse response) {
        Enumeration<String> paras = request.getParameterNames();
        if (null != paras) {
            for (Enumeration<String> e = paras; e.hasMoreElements(); ) {

                String thisName = e.nextElement().toString();

                String thisValue = request.getParameter(thisName);
                if (!StringUtils.isEmpty(thisName)) {
                    model.addAttribute(thisName, thisValue);
                }
            }
        }
        model.addAttribute("user", UserUtils.getUser());
    }

    @RequestMapping(value = "/content")
    public String showIndex(Model model, HttpServletRequest request, HttpServletResponse response) {
        this.setAllAttribute(model, request, response);
        return display("content");
    }

    @RequestMapping("/loadall")
    @ResponseBody
    public BaseRspJson<List<Entity>> LoadAll(@RequestBody BaseReqJson request) {
        BaseRspJson<List<Entity>> rsp = new BaseRspJson<List<Entity>>();
        Queryable queryable = this.GetFilterCondition(request);
        List<Entity> obj = this.commonService.listWithNoPage(queryable);
        rsp.setObj(obj);
        return rsp;
    }

    public EntityWrapper<Entity> getWrapper(BaseReqJson request) {
        Queryable queryable = this.GetFilterCondition(request);


        return QueryUtil.getWrapper(queryable, this.entityClass);
    }


    @RequestMapping("/get")
    @ResponseBody
    public BaseRspJson<Entity> get(@RequestBody BaseReqJson<String> request) {
        BaseRspJson<Entity> rsp = new BaseRspJson<Entity>();

        String obj = this.getObj(request, String.class);
        Entity e = this.commonService.selectById(obj);
        rsp.setObj(e);
        return rsp;
    }

    @RequiresMethodPermissions("list")
    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model, HttpServletRequest request, HttpServletResponse response) {
        preList(model, request, response);
        String page = request.getParameter("page");
        if (!ValidatorUtil.isEmpty(page)) {
            return display(page);
        }
        return display("list");
    }


    @RequestMapping(value = "/select")
    public String select(Model model, HttpServletRequest request, HttpServletResponse response) {
        preList(model, request, response);
        return display("select");
    }

    /**
     * 在异步获取数据之前
     *
     * @param
     * @param request
     * @param response
     */
    public void preAjaxList(Queryable queryable, EntityWrapper<Entity> entityWrapper, HttpServletRequest request, HttpServletResponse response) {
        String sort = request.getParameter("sort");
        String order = request.getParameter("order");
        if (StringUtils.isNotEmpty(sort)) {
            String[] sorts = sort.split(",");
            for (int i = 1; i < sorts.length; i++) {
                String temp = sorts[i];
                if (i != sorts.length - 1) {
                    String[] sortChild = temp.split(" ");
                    if (sortChild[1].equals("asc")) {
                        queryable.addOrder(Sort.Direction.ASC, sortChild[0]);
                    } else {
                        queryable.addOrder(Sort.Direction.DESC, sortChild[1]);
                    }
                } else {
                    if (order.equals("asc")) {
                        queryable.addOrder(Sort.Direction.ASC, temp);
                    } else {
                        queryable.addOrder(Sort.Direction.DESC, temp);
                    }
                }
            }
        }
    }

    /**
     * 根据页码和每页记录数，以及查询条件动态加载数据
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "ajaxList", method = {RequestMethod.GET, RequestMethod.POST})
    @PageableDefaults(sort = "id=desc")
    private void ajaxList(Queryable queryable, PropertyPreFilterable propertyPreFilterable, HttpServletRequest request,
                          HttpServletResponse response) throws IOException {
        EntityWrapper<Entity> entityWrapper = new EntityWrapper<Entity>(entityClass);
        preAjaxList(queryable, entityWrapper, request, response);
        propertyPreFilterable.addQueryProperty("id");
        // 预处理
        QueryableConvertUtils.convertQueryValueToEntityValue(queryable, entityClass);
        SerializeFilter filter = propertyPreFilterable.constructFilter(entityClass);
        PageJson<Entity> pagejson = new PageJson<Entity>(commonService.list(queryable, entityWrapper));
        String content = JSON.toJSONString(pagejson, filter);
        StringUtils.printJson(response, content);
    }

    public String showView(Entity entity, Model model, HttpServletRequest request, HttpServletResponse response) {
        return "";
    }

    /**
     * @param model
     * @param id
     * @param request
     * @param response
     * @return
     * @title: _view
     * @description: 查看
     * @return: String
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String _view(Model model, @PathVariable("id") ID id, HttpServletRequest request,
                        HttpServletResponse response) {
        Entity entity = get(id);
        showUpdate(entity, model, request, response);
        model.addAttribute("data", entity);
        String showView = showView(newModel(), model, request, response);
        if (!StringUtils.isEmpty(showView)) {
            return display(showView);
        }
        return display("edit");
    }

    public void preEdit(Entity entity, Model model, HttpServletRequest request, HttpServletResponse response) {
        this.setAllAttribute(model, request, response);
    }

    public void preCopy(Entity entity, Model model, HttpServletRequest request, HttpServletResponse response) {
        this.setAllAttribute(model, request, response);
    }

    public String showCreate(Entity entity, Model model, HttpServletRequest request, HttpServletResponse response) {
        return "";
    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String _showCreate(Model model, HttpServletRequest request, HttpServletResponse response) {
        preEdit(newModel(), model, request, response);
        String creteaView = showCreate(newModel(), model, request, response);
        if (!model.containsAttribute("data")) {
            Entity data = newModel();
            model.addAttribute("data", data);
        }
        if (!StringUtils.isEmpty(creteaView)) {
            return creteaView;
        }
        return display("edit");
    }

    public void preDetail(Entity entity, Model model, HttpServletRequest request, HttpServletResponse response) {
        this.setAllAttribute(model, request, response);
    }

    @RequestMapping(value = "detail")
    public String detail(Model model, HttpServletRequest request, HttpServletResponse response) {
        ID id = (ID) request.getParameter("id");
        Entity entity = get(id);

        preDetail(entity, model, request, response);
        model.addAttribute("data", entity);

        return display("detail");
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson create(Model model, @Valid @ModelAttribute("data") Entity entity, BindingResult result,
                           HttpServletRequest request, HttpServletResponse response) {
        return doSave(entity, request, response, result);
    }

    public String showUpdate(Entity entity, Model model, HttpServletRequest request, HttpServletResponse response) {
        return "";
    }

    @RequestMapping(value = "{id}/update", method = RequestMethod.GET)
    public String _showUpdate(@PathVariable("id") ID id, Model model, HttpServletRequest request,
                              HttpServletResponse response) {
        Entity entity = get(id);
        preEdit(entity, model, request, response);
        model.addAttribute("data", entity);
        String updateView = showUpdate(newModel(), model, request, response);
        if (!StringUtils.isEmpty(updateView)) {
            return updateView;
        }
        return display("edit");
    }

    @RequestMapping(value = "{id}/copy", method = RequestMethod.GET)
    public String _showCopy(@PathVariable("id") ID id, Model model, HttpServletRequest request,
                            HttpServletResponse response) {
        Entity entity = get(id);
        entity.setId(null);
        preCopy(entity, model, request, response);
        model.addAttribute("data", entity);
        String updateView = showUpdate(newModel(), model, request, response);
        if (!StringUtils.isEmpty(updateView)) {
            return updateView;
        }
        return display("edit");
    }

    @RequestMapping(value = "{id}/copy", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson copy(Model model, @Valid @ModelAttribute("data") Entity entity, BindingResult result,
                         HttpServletRequest request, HttpServletResponse response) {
        return doSave(entity, request, response, result);
    }

    @RequestMapping(value = "{id}/update", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson update(Model model, @Valid @ModelAttribute("data") Entity entity, BindingResult result,
                           HttpServletRequest request, HttpServletResponse response) {
        return doSave(entity, request, response, result);
    }

    /**
     * 保存数据之前
     *
     * @param entity
     * @param request
     * @param response
     */
    public void preSave(Entity entity, HttpServletRequest request, HttpServletResponse response) {
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson doSave(Entity entity, HttpServletRequest request, HttpServletResponse response,
                           BindingResult result) {
        AjaxJson ajaxJson = new AjaxJson();
        ajaxJson.success("保存成功");
        if (hasError(entity, result)) {
            // 错误提示
            String errorMsg = errorMsg(result);
            if (!StringUtils.isEmpty(errorMsg)) {
                ajaxJson.fail(errorMsg);
            } else {
                ajaxJson.fail("保存失败");
            }
            return ajaxJson;
        }
        try {
            preSave(entity, request, response);
            if (ObjectUtils.isNullOrEmpty(entity.getId())) {
                commonService.insert(entity);
            } else {
                commonService.insertOrUpdate(entity);
            }
            afterSave(entity, request, response);
        } catch (Exception e) {
            e.printStackTrace();
            ajaxJson.fail("保存失败!<br />原因:" + e.getMessage());
        }
        return ajaxJson;
    }

    /**
     * 保存数据之后
     *
     * @param entity
     * @param request
     * @param response
     */
    public void afterSave(Entity entity, HttpServletRequest request, HttpServletResponse response) {
    }

    @RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson delete(@PathVariable("id") ID id) {
        AjaxJson ajaxJson = new AjaxJson();
        ajaxJson.success("删除成功");
        if (JeewebPropertiesUtil.getProperties().getBoolean("demoMode")) {
            ajaxJson.fail("演示模式，不允许删除！");
            return ajaxJson;
        }
        try {
            commonService.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
            ajaxJson.fail("删除失败");
        }
        return ajaxJson;
    }

    @RequestMapping(value = "batch/delete", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public AjaxJson batchDelete(@RequestParam(value = "ids", required = false) ID[] ids) {
        AjaxJson ajaxJson = new AjaxJson();
        ajaxJson.success("删除成功");
        if (JeewebPropertiesUtil.getProperties().getBoolean("demoMode")) {
            ajaxJson.fail("演示模式，不允许删除！");
            return ajaxJson;
        }
        try {
            List<ID> idList = java.util.Arrays.asList(ids);
            commonService.deleteBatchIds(idList);
        } catch (Exception e) {
            e.printStackTrace();
            ajaxJson.fail("删除失败");
        }
        return ajaxJson;
    }

    @RequestMapping(value = "validate", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ValidJson validate(DuplicateValid duplicateValid, HttpServletRequest request) {
        ValidJson validJson = new ValidJson();
        Boolean valid = Boolean.FALSE;
        try {
            EntityWrapper<Entity> entityWrapper = new EntityWrapper<Entity>(entityClass);
            valid = commonService.doValid(duplicateValid, entityWrapper);
            if (valid) {
                validJson.setStatus("y");
                validJson.setInfo("验证通过!");
            } else {
                validJson.setStatus("n");
                if (!StringUtils.isEmpty(duplicateValid.getErrorMsg())) {
                    validJson.setInfo(duplicateValid.getErrorMsg());
                } else {
                    validJson.setInfo("当前信息重复!");
                }
            }
        } catch (Exception e) {
            validJson.setStatus("n");
            validJson.setInfo("验证异常，请检查字段是否正确!");
        }
        return validJson;
    }

    @RequestMapping("/export")

    public void export(Queryable queryable, PropertyPreFilterable propertyPreFilterable, HttpServletRequest request,
                       HttpServletResponse response) {
        EntityWrapper<Entity> entityWrapper = new EntityWrapper<Entity>(entityClass);
        preAjaxList(queryable, entityWrapper, request, response);
        propertyPreFilterable.addQueryProperty("id");
        // 预处理
        QueryableConvertUtils.convertQueryValueToEntityValue(queryable, entityClass);
        PageJson<Entity> pagejson = new PageJson<Entity>(commonService.list(queryable, entityWrapper));
        File excel = ExcelUtil.export(entityClass, pagejson.getResults());
        FileExportUtil.fileExport(response, excel);
    }

    /**
     * 获取request
     *
     * @return
     */
    protected HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 获取页面传递的某一个参数值
     */
    public String getString(String key) {
        return getRequest().getParameter(key);
    }

}
