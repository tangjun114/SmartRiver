package cn.jeeweb.core.utils.sms.sender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yundashi.sms.SmsMultiSender;
import com.yundashi.sms.SmsMultiSenderResult;

import cn.jeeweb.core.utils.sms.data.SmsResult;
import cn.jeeweb.core.utils.sms.data.SmsTemplate;

public class BailingSmsSender extends SmsSender {

    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
    static final long appid = 140009369325L;
    static final String appkey = "0122444fb56b5ee41ca1ca96ac1eaa1f";
    
    static final String sign = "国勘云";
    
	protected Logger logger = LoggerFactory.getLogger(getClass());
 
	public BailingSmsSender() {
		try {
			init();
		} catch (Exception e) {

		}
	}

	@Override
	public void init() {
	}

	@Override
	public SmsResult send(String phone, SmsTemplate smsTemplate, String... datas) {
		SmsResult requestResult = new SmsResult();
		
		try
		{
		SmsMultiSender multiSender = new SmsMultiSender(appid, appkey);
        SmsMultiSenderResult multiSenderResult;
        
        List<String> phoneNumbers = new ArrayList<String>();
        String[] phones = phone.split(",");
        phoneNumbers = Arrays.asList(phones);
        String msg = datas[0];
      //  multiSenderResult = multiSender.send(1, "86", phoneNumbers, msg, "", "");
        
        multiSenderResult =  multiSender.sendWithParam("86", phoneNumbers, Integer.valueOf(smsTemplate.getTemplateId()), Arrays.asList(datas), sign, "", "");
        
        if(null== multiSenderResult || multiSenderResult.result != 0)
        {
        	 requestResult.setSuccess(false);
        	 requestResult.setMsg(multiSenderResult.errMsg);
         }
        else
        {
        	requestResult.setSuccess(true);
        }
        }
        catch(Exception e)
        {
        	logger.error("send message error",e);
        	requestResult.setSuccess(false);
        	requestResult.setMsg(e.getMessage());
        }
		return requestResult;
	}

	private SmsResult mapToResult(Map<String, Object> result) {
		SmsResult requestResult = new SmsResult();
		requestResult.setSuccess(Boolean.FALSE);
		requestResult.setSenderName(name());
		if (result != null) {
			if ("000000".equals(result.get("statusCode"))) {
				requestResult.setCode(result.get("statusCode") + "");
				requestResult.setMsg("发送成功");
				// 正常返回输出data包体信息（map）
				HashMap<String, Object> data = (HashMap<String, Object>) result.get("data");
				requestResult.setSmsid(data.get("smsMessageSid") + "");
				requestResult.setSuccess(Boolean.TRUE);
			} else {
				requestResult.setSuccess(Boolean.FALSE);
				requestResult.setCode(result.get("statusCode") + "");
				requestResult.setMsg(result.get("statusMsg") + "");
			}
		}
		return requestResult;
	}

	@Override
	protected String name() {
		return "Bailing";
	}

	public static void main(String[] args) {
		// 70585
		SmsTemplate smsTemplate = SmsTemplate.newTemplateById("70585");
		CCPSmsSender ccpSmsSender = new CCPSmsSender();
		ccpSmsSender.send("15085980308,18166727295", smsTemplate, "2345");
	}
}
