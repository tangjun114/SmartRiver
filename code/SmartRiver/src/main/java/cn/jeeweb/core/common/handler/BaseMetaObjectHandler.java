package cn.jeeweb.core.common.handler;

import java.util.Date;

import org.apache.ibatis.reflection.MetaObject;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;

import cn.jeeweb.core.common.constant.DataBaseConstant;
import cn.jeeweb.core.common.constant.DataBaseFieldConstant;
import cn.jeeweb.modules.sys.entity.User;
import cn.jeeweb.modules.sys.utils.UserUtils;

/**
 * 
 * All rights Reserved, Designed By www.jeeweb.cn
 * 
 * @title: MyMetaObjectHandler.java
 * @package cn.jeeweb.core.common.handler
 * @description: 填充创建时间，创建人，更新时间，更新人
 * @author: 王存见
 * @date: 2017年7月20日 上午10:55:42
 * @version V1.0
 * @copyright: 2017 www.jeeweb.cn Inc. All rights reserved.
 *
 */
public class BaseMetaObjectHandler extends MetaObjectHandler {

	/**
	 * 新增
	 */
	public void insertFill(MetaObject metaObject) {
		// 创建用户
		Object createBy = getFieldValByName(DataBaseFieldConstant.CREATE_BY, metaObject);
		if (createBy == null) {
			
			if(User.class == getFieldTypeByName(DataBaseFieldConstant.CREATE_BY,metaObject))
			{
				setFieldValByName(DataBaseFieldConstant.CREATE_BY, UserUtils.getUser(), metaObject);
			}
 		}

		// 创建时间
		Object createDate = getFieldValByName(DataBaseFieldConstant.CREATE_DATE, metaObject);
		if (createDate == null) {
			
			setFieldValByName(DataBaseFieldConstant.CREATE_DATE, new Date(), metaObject);
		}

		// 删除标记
		Object delFlag = getFieldValByName(DataBaseFieldConstant.DEL_FLAG, metaObject);
		if (delFlag == null) {
			setFieldValByName(DataBaseFieldConstant.DEL_FLAG, DataBaseConstant.DEL_FLAG_NORMAL, metaObject);
		}
	}

	@Override
	public void updateFill(MetaObject metaObject) {
		// 更新用户
		Object updateBy = getFieldValByName(DataBaseFieldConstant.UPDATE_BY, metaObject);
		if (updateBy == null) {
			Class clazz = User.class;
			Class fieldClazz = getFieldTypeByName(DataBaseFieldConstant.UPDATE_BY,metaObject);
			if(clazz.equals(fieldClazz))
			{
				setFieldValByName(DataBaseFieldConstant.UPDATE_BY, UserUtils.getUser(), metaObject);
			}
		}

		// 更新用户
		Object updateDate = getFieldValByName(DataBaseFieldConstant.UPDATE_DATE, metaObject);
		if (updateDate == null) {
			setFieldValByName(DataBaseFieldConstant.UPDATE_DATE, new Date(), metaObject);
		}
	}
    public Class getFieldTypeByName(String fieldName, MetaObject metaObject) {
    	Class clazz = null;
        if (metaObject.hasGetter(fieldName)) {
        	clazz = metaObject.getGetterType(fieldName);
        } else if (metaObject.hasGetter(META_OBJ_PREFIX + "." + fieldName)) {
        	clazz = metaObject.getGetterType(META_OBJ_PREFIX + "." + fieldName);
        }
        return clazz;
    }
}