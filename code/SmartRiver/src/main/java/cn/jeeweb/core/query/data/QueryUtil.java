package cn.jeeweb.core.query.data;

import com.ff.common.web.json.BaseReqJson;

import cn.jeeweb.core.query.parse.QueryToWrapper;
import cn.jeeweb.core.query.wrapper.EntityWrapper;

public class QueryUtil {

	public static <T> EntityWrapper<T> getWrapper(Queryable qy,Class<T> clazz)
	{
 		
		QueryToWrapper<T> queryToWrapper = new QueryToWrapper<T>();

		EntityWrapper<T> wrapper = new EntityWrapper<T>(clazz);
		queryToWrapper.parseCondition(wrapper , qy);
		// 排序问题
		queryToWrapper.parseSort(wrapper, qy);
		
		return wrapper;
	}
	
}
