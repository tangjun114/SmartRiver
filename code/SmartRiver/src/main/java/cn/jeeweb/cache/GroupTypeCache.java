package cn.jeeweb.cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ff.common.cache.FFAbstractCache;

import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.modules.sm.entity.McSmParamConfig;
import cn.jeeweb.modules.sm.service.IMcSmParamConfigService;

@Service
public class GroupTypeCache extends FFAbstractCache<String, McSmParamConfig>
{

	@Autowired
	private IMcSmParamConfigService mcSmParamConfigService;
	@Override
	public void load() {
		Queryable queryable = QueryRequest.newQueryable();
		queryable.addCondition("parentId", "8feb09f7fc45497ebbed7efdf527f2aa");
		List<McSmParamConfig> objList = this.mcSmParamConfigService.listWithNoPage(queryable);
		this.add("id",objList);
	}

}
