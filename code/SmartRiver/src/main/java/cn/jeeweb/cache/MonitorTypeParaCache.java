package cn.jeeweb.cache;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ff.common.cache.FFAbstractCache;
import com.ff.common.util.validate.ValidatorUtil;

import cn.jeeweb.core.query.data.QueryRequest;
import cn.jeeweb.core.query.data.Queryable;
import cn.jeeweb.modules.sm.entity.McSmMonitorGroupType;
import cn.jeeweb.modules.sm.service.IMcSmMonitorGroupTypeService;
@Service
public class MonitorTypeParaCache extends FFAbstractCache<String, McSmMonitorGroupType>
{

	protected Logger log = Logger.getLogger(getClass());
	@Autowired
	private IMcSmMonitorGroupTypeService mcSmMonitorGroupTypeService;
	@Override
	public void load() {
		Queryable queryable = QueryRequest.newQueryable();
 		List<McSmMonitorGroupType> objList = this.mcSmMonitorGroupTypeService.listWithNoPage(queryable);
		this.add("code",objList);
	}

	public String addUnit(String name,String code)
	{
		 
		name = name + "(" + get(code).getUnit() + ")";
		return name;
	}
	
	public String getMaxNameUnit(String typeCode)
	{
		return addUnit(this.get(typeCode).getMaxName(), typeCode);
	}
	

 
	@Override
	public McSmMonitorGroupType get(String key) {
		// TODO Auto-generated method stub
		McSmMonitorGroupType type = super.get(key);
		if(null == type)
		{
			type = new McSmMonitorGroupType();
		}
		return type ;
	}

	public String getMinNameUnit(String typeCode)
	{
		return addUnit(this.get(typeCode).getMinName(), typeCode);
	}
	
	public String getMaxRateUnit(String typeCode)
	{
		return "最大变化率("+get(typeCode).getUnit() + "/d)";
	}
	
	public Double getRatio(String typeCode)
	{
		Double ratio = 1.0;
		McSmMonitorGroupType type = get(typeCode);
		if(null != type)
		{
			if(!ValidatorUtil.isEmpty(type.getValueRatio()))
			{
				try
				{
					ratio = Double.valueOf(type.getValueRatio());
 				}
				catch(Exception e)
				{
					log.error("",e);
				}
			}
		}
		return ratio;
	}
	public boolean isShift(String code)
	{
		if("T003".equals(code)){
			return true;
		}
		return false;
	}
}