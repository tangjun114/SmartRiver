/**
 * @Description 
 * @author tangjun
 * @date 2016年8月10日
 * 
 */
package com.ff.bi.service.calc;

import com.ff.bi.model.RptCalc;
import com.ff.bi.model.RptDim;
import com.ff.bi.model.RptIndicator;

/**
 * @Description 
 * @author tangjun
 * @date 2016年8月10日
 */
public interface RptCalcService
{
 	
	public Object calc(Object nowValue, Object compareValue,RptCalc calc, RptIndicator indicator);
	
	public Object getDimCmpValue(Object nowValue,RptDim dim,RptCalc calc);

}
