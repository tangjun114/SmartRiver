/**
 * @Description 
 * @author tangjun
 * @date 2016年8月9日
 * 
 */
package com.ff.bi.service.axis;

import java.util.List;

import com.ff.bi.model.RptDim;
import com.ff.common.dao.model.FFCondition;
import com.ff.common.service.BaseService;

/**
 * @Description 
 * @author tangjun
 * @date 2016年8月9日
 */
public interface RptAxisDataSource
{
	public List<?> getXAxisData(RptDim dim,List<FFCondition> conditionList,BaseService service);
 
	public String getDimMapField(RptDim dim);

}
