/**
 * @Description 
 * @author tangjun
 * @date 2016年8月9日
 * 
 */
package com.ff.bi.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.ff.bi.constant.RptCalcEnum;

/**
 * @Description 
 * @author tangjun
 * @date 2016年8月9日
 */

@Retention(RetentionPolicy.RUNTIME) 
public @interface RptCalcAnno
{
	public RptCalcEnum calc();
	public String formula() default "";
 
}
