/**
 * @Description 
 * @author tangjun
 * @date 2016年8月10日
 * 
 */
package com.ff.bi.cache;


import org.springframework.stereotype.Service;

import com.ff.bi.model.RptIndicator;
import com.ff.common.cache.FFAbstractCache;

/**
 * @Description 
 * @author tangjun
 * @date 2016年8月10日
 */
@Service
public class RptIndicatorCache extends FFAbstractCache<String,RptIndicator>
{

	/* (non-Javadoc)
	 * @see com.fuego.common.cache.AbstractCache#load()
	 */
	@Override
	public void load()
	{
 
 	}


}
