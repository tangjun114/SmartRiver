package com.ff.common.util.format;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.ff.common.util.log.FFLogFactory;
import com.ff.common.util.validate.ValidatorUtil;


public class DateUtil {
    private static final Logger log = FFLogFactory.getLog(DateUtil.class);

    public static final String yyyyMMddHHmmss = "yyyyMMddHHmmss";
    public static final String defaultFormat = "yyyy-MM-dd HH:mm:ss";

    public static long getDateTime(Date date) {
        if (null != date) {
            return date.getTime();
        }
        return 0;
    }

    public static String DateToString(Date date) {
        return DateToString(date, defaultFormat);
    }

    public static String DateToString(Date date, String format) {
        String str = null;
        //DateFormat d = DateFormat.getDateInstance(DateFormat.MEDIUM);
        SimpleDateFormat d = new SimpleDateFormat(format);

        if (null == date) {
            return "";
        }
        try {
            str = d.format(date);
        } catch (Exception e) {
            str = "";
            log.warn("the date formart is wrong." + date);
        }
        return str;

    }

    public static Date stringToDate(String date) {
        return stringToDate(date, defaultFormat);
    }

    public static Date stringToDate(String date, String format) {
        Date da;
        SimpleDateFormat sformat = new SimpleDateFormat(format);
        if (ValidatorUtil.isEmpty(date)) {
            return null;
        }
        try {
            da = sformat.parse(date);

        } catch (Exception e) {
            int year = 0;
            int month = 0;
            int day = 1;
            int hour = 0;
            int min = 0;
            int sec = 0;
            int millsec = 0;
            int num = 0;
            String temp = "";

            for (int i = 0; i < date.length(); i++) {
                char a = date.charAt(i);
                if (ValidatorUtil.isInt(a + "")) {
                    num++;
                    temp += a;
                    if (4 == num) {
                        year = Integer.valueOf(temp);
                        temp = "";
                    } else if (6 == num) {
                        month = Integer.valueOf(temp);
                        //month--;
                        temp = "";
                    } else if (8 == num) {
                        day = Integer.valueOf(temp);
                        temp = "";
                    } else if (10 == num) {
                        hour = Integer.valueOf(temp);
                        temp = "";
                    } else if (12 == num) {
                        min = Integer.valueOf(temp);
                        temp = "";
                    } else if (14 == num) {
                        sec = Integer.valueOf(temp);
                        temp = "";
                    } else if (17 == num) {
                        millsec = Integer.valueOf(temp);
                        temp = "";
                    }

                }

            }
            Calendar cal = Calendar.getInstance();
            month--;
            cal.set(year, month, day, hour, min, sec);
            cal.setTimeInMillis(cal.getTimeInMillis() + millsec);
            //log.error("Err: Date Str is:" + date);
            //throw new RuntimeException(e);
            da = cal.getTime();

        }
        return da;

    }


    /**
     * get the first date of current month
     *
     * @return
     */
    public static String getCurMonthFirstDate() {
        Calendar cal_1 = Calendar.getInstance();// 获取当前日期
        cal_1.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
        Date date = cal_1.getTime();
        return DateToString(date);
    }

    /**
     * get the last date of current month
     *
     * @return
     */
    public static String getCurMonthLastDate() {
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date date = ca.getTime();

        return DateToString(date);
    }

    public static Date getCurrentDate() {

        return new Date(System.currentTimeMillis());
    }

    public static Timestamp getCurrentDateTime() {

        return new Timestamp(System.currentTimeMillis());
    }

    public static String getCurrentDateTimeStr() {
        Date time = new Timestamp(System.currentTimeMillis());

        return time.toString();
    }

    public static int countDayNum(Date startDate, Date endDate) {
        if (null == startDate || null == endDate) {
            return 0;
        }
        long milSec = startDate.getTime() - endDate.getTime();
        int dayNum = (int) (milSec / 1000 / 3600 / 24);
        return dayNum;

    }

    public static Date dayCalculate(Date date, int dateNum) {
        Calendar convertDate = Calendar.getInstance();
        convertDate.setTime(date);
        convertDate.add(Calendar.DATE, dateNum);
        Date result = convertDate.getTime();
        return result;

    }

    public static Date minCalculate(Date date, int minNum) {
        Calendar convertDate = Calendar.getInstance();
        convertDate.setTime(date);
        convertDate.add(Calendar.MINUTE, minNum);
        Date result = convertDate.getTime();
        return result;

    }

    public final static char[] upper = "零一二三四五六七八九十".toCharArray();

    /**
     * 根据小写数字格式的日期转换成大写格式的日期
     *
     * @param date
     * @return
     */
    public static String getUpperDate(Date d) {
        //支持yyyy-MM-dd、yyyy/MM/dd、yyyyMMdd等格式

        String date = DateToString(d, "yyyy-MM-dd");
        if (date == null) return null;
        //非数字的都去掉
        date = date.replaceAll("\\D", "");
        if (date.length() != 8) return null;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 4; i++) {//年
            sb.append(upper[Integer.parseInt(date.substring(i, i + 1))]);
        }
        sb.append("年");//拼接年
        int month = Integer.parseInt(date.substring(4, 6));
        if (month <= 10) {
            sb.append(upper[month]);
        } else {
            sb.append("十").append(upper[month % 10]);
        }
        sb.append("月");//拼接月

        int day = Integer.parseInt(date.substring(6));
        if (day <= 10) {
            sb.append(upper[day]);
        } else if (day < 20) {
            sb.append("十").append(upper[day % 10]);
        } else {
            sb.append(upper[day / 10]).append("十");
            int tmp = day % 10;
            if (tmp != 0) sb.append(upper[tmp]);
        }
        sb.append("日");//拼接日
        return sb.toString();
    }

    public static int getDayOfTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static int getMonthOfTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }


    public static int getYearOfTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

}
