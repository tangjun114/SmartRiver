/**
 * @Description 
 * @author tangjun
 * @date 2016年8月4日
 * 
 */
package com.ff.common.web.json;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.ff.common.constant.FFErrorCode;

/**
 * @Description 
 * @author tangjun
 * @date 2016年8月4日
 */
public class BaseRspJson<E> implements Serializable
{
	private int errorCode = FFErrorCode.SUCCESS;
	private String message;
	private PageJson page;
	private E obj;
	private Map<String,Object> map = new HashMap<String, Object>();
	/**
	 * @return the errorCode
	 */
	public int getErrorCode()
	{
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}
	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}
	/**
	 * @return the obj
	 */
	public E getObj()
	{
		return obj;
	}
	/**
	 * @param obj the obj to set
	 */
	public void setObj(E obj)
	{
		this.obj = obj;
	}
	public Map<String, Object> getMap()
	{
		return map;
	}
	public void setMap(Map<String, Object> map)
	{
		this.map = map;
	}
	public PageJson getPage() {
		return page;
	}
	public void setPage(PageJson page) {
		this.page = page;
	}
 
	
	
}
