/**
 * @Description 
 * @author tangjun
 * @date 2016年8月8日
 * 
 */
package com.ff.common.web.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.common.util.log.FFLogFactory;
import com.ff.common.util.meta.ReflectionUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;
import com.ff.common.web.json.PageDataJson;
import com.ff.common.web.json.PageJson;

import cn.jeeweb.core.common.service.ICommonService;
import cn.jeeweb.core.query.data.Page;
import cn.jeeweb.core.query.data.Queryable;

/**
 * @Description 
 * @author tangjun
 * @date 2016年8月8日
 */
public class FFTableController<E> extends FFBaseController  
{
	private Logger log = FFLogFactory.getLog(this.getClass());
	//@Autowired
	//private BaseService<E> baseService;
	
	protected ICommonService<E> baseService;
	
	public FFTableController()
	{

		
	}
 
	@Autowired
	public void setCommonService(ICommonService<E> commonService) {
		this.baseService = commonService;
	}
	
	public Class GetTableClass()
	{
		// TODO Auto-generated method stub
		return  ReflectionUtil.getSuperClassGenricType(getClass());
	}
	/**
	 * @return the service
	 */
	public ICommonService<E> getService()
	{
		return baseService;
	}
 
 
	public BaseRspJson<List<E>> Load(@RequestBody BaseReqJson<?> request)
	{
	    BaseRspJson<List<E>> rsp = new BaseRspJson<List<E>>();
 		//List<FFCondition> conditionList = this.GetFilterCondition();
		Queryable queryable = this.GetFilterCondition(request);
		Page<E> obj = getService().list(queryable);
		PageDataJson<E> pageData = getPageData(obj);
		rsp.setPage(pageData.getPage());
		rsp.setObj(pageData.getDataList());
 		return rsp;
 	}

	private PageDataJson<E> getPageData(Page<E> obj) {
		PageDataJson<E> pageData = new PageDataJson<E>();
		PageJson page = new PageJson();
		page.setCurrentPage(obj.getNumber());
		page.setPageSize(obj.getSize());
		page.setTotal(obj.getTotalElements());
		pageData.setPage(page);
		pageData.setDataList(obj.getContent());
		return pageData;
	}
	
	@RequestMapping("/loadall")
	@ResponseBody
	public BaseRspJson<List<E>> LoadAll(@RequestBody BaseReqJson request)
	{
		BaseRspJson<List<E>> rsp = new BaseRspJson<List<E>>();
		Queryable queryable = this.GetFilterCondition(request);
		List<E> obj = getService().listWithNoPage(queryable);
		rsp.setObj(obj);
  		return rsp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public BaseRspJson<E> get(@RequestBody BaseReqJson<String> request)
	{
		BaseRspJson<E> rsp = new BaseRspJson<E>();
 
  		String obj = this.getObj(request,String.class);
		E e = getService().selectById(obj);
		rsp.setObj(e);
   		return rsp;
	}
	
	@RequestMapping("/create")
	@ResponseBody
	public BaseRspJson<E> Create(@RequestBody BaseReqJson<E> request)
	{
		BaseRspJson<E> rsp = new BaseRspJson<E>();
 
  		E obj = this.getObj(request,this.GetTableClass());
		getService().insert(obj);
		rsp.setObj(obj);
 
  		return rsp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public BaseRspJson<E> update(@RequestBody BaseReqJson<E> request)
	{
		BaseRspJson<E> rsp = new BaseRspJson<E>();
 
  		E obj = this.getObj(request,this.GetTableClass());
		getService().updateById(obj);
		rsp.setObj(obj);
 
  		return rsp;
	}

 

 
}
