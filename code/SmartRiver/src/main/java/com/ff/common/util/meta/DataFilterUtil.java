package com.ff.common.util.meta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataFilterUtil
{
	
	public static <E,T> Map<E,T> buildMap(String fieldName,List<T> data)
	{
		Map<E,T> map = new HashMap<E,T>();
		
		if(null != data)
		{
			for(T e :data)
			{
				E key = (E)ReflectionUtil.getValueByFieldName(e, fieldName);
				map.put(key, e);
			}
		}
		
		return map;
	}
	
	public static <E,T> Map<E,List<T>> buildMapList(String fieldName,List<T> data)
	{
		Map<E,List<T>> map = new HashMap<E,List<T>>();
		
		if(null != data)
		{
			for(T e :data)
			{
				E key = (E)ReflectionUtil.getValueByFieldName(e, fieldName);
				
				List<T> list = map.get(key);
				if(null == list)
				{
					list = new ArrayList<T>();
				}
				list.add(e);
				map.put(key, list);
			}
		}
		
		return map;
	}
	public static List<?>  getFieldValueFromList(List<?> data,String fieldName)
	{
		List temp = new ArrayList<>();
		if(null == data)
		{
			return temp;
		}
		for(Object e : data)
		{
			Object val = ReflectionUtil.getValueByFieldName(e, fieldName);
			temp.add(val);
		}
		return temp;
	}
	
	public static  <T> T getObjFromList(List<T> data,String fieldName,String fieldValue)
	{
 		if(null == data)
		{
			return null;
		}
		for(T e : data)
		{
			Object val = ReflectionUtil.getValueByFieldName(e, fieldName);
			if(null != fieldValue && fieldValue.equals(val))
			{
				return e;
			}
		}
		return null;
	}
	
	public static  <T> T getAbsMaxMinObjFromList(List<T> data,String fieldName,boolean isMax)
	{
		if(null == data)
		{
			return null;
		}
 		T max = data.get(0);
		for(T e : data)
		{
			Object val = ReflectionUtil.getValueByFieldName(e, fieldName);
			Object valMax = ReflectionUtil.getValueByFieldName(max, fieldName);
			
			if(null != val && null != valMax)
			{
				if(isMax)
				{
					if(Math.abs(Double.valueOf(val.toString())) > Math.abs(Double.valueOf(valMax.toString())))
					{
						max = e;
					}
	 			}
				else
				{
					if(Math.abs(Double.valueOf(val.toString())) < Math.abs(Double.valueOf(valMax.toString())))
					{
						max = e;
					}
				}
 			}
			
		}
		return max;
	}
	
	public static  <T> T getMaxMinObjFromList(List<T> data,String fieldName,boolean isMax)
	{
 		if(null == data)
		{
			return null;
		}
 		T max = data.get(0);
		for(T e : data)
		{
			Object val = ReflectionUtil.getValueByFieldName(e, fieldName);
			Object valMax = ReflectionUtil.getValueByFieldName(max, fieldName);
			
			if(null != val && null != valMax)
			{
				if(isMax)
				{
					if(Double.valueOf(val.toString()) > Double.valueOf(valMax.toString()))
					{
						max = e;
					}
	 			}
				else
				{
					if(Double.valueOf(val.toString()) < Double.valueOf(valMax.toString()))
					{
						max = e;
					}
				}
 			}
			
		}
		return max;
	}
	
 
	
	public static  <T> List<T> getObjListFromList(List<T> data,String fieldName,String fieldValue)
	{
 		if(null == data)
		{
			return null;
		}
 		List<T> temp = new ArrayList<T>();
		for(T e : data)
		{
			Object val = ReflectionUtil.getValueByFieldName(e, fieldName);
			if(null != fieldValue && fieldValue.equals(val))
			{
				temp.add(e);
			}
		}
		return temp;
	}
}
