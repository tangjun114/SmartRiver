package com.ff.common.web.json;

import java.io.Serializable;

public class PageJson implements Serializable
{
	private int pageSize = 20;
	private int currentPage = 1;
	private long total;
	private long startIndex;
	public int getPageSize()
	{
		return pageSize;
	}
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}
	public int getCurrentPage()
	{
		return currentPage;
	}
	public void setCurrentPage(int currentPage)
	{
		this.currentPage = currentPage;
	}
	public long getTotal()
	{
		return total;
	}
	public void setTotal(long total)
	{
		this.total = total;
	}
	public long getStartIndex() 
	{
		this.startIndex = (this.currentPage-1)*this.pageSize;
		return startIndex;
	}
	public void setStartIndex(long startIndex)
	{
		this.startIndex = startIndex;
	}
	
	
	
}
