package com.ff.common.api;

import java.util.List;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.ff.common.dao.model.FFCondition;
import com.ff.common.service.BaseService;
import com.ff.common.util.meta.ReflectionUtil;
import com.ff.common.web.json.BaseRspJson;
import com.ff.common.web.json.PageDataJson;
import com.ff.common.web.json.PageJson;

 
public abstract class BaseApiServiceImpl<E> implements BaseApiService,InitializingBean
{

	@Autowired
	protected BaseService<E> baseService;
	@Override
	public BaseRspJson<?> getAll(List<FFCondition> conditionList, int currentPage, int pageSize)
	{
		BaseRspJson rsp = new BaseRspJson();
		
		PageJson page = new PageJson();
		page.setCurrentPage(currentPage);
		page.setPageSize(pageSize);
		PageDataJson<E> data = baseService.getAll(conditionList, page );
		rsp.setObj(data);
		// TODO Auto-generated method stub
		return rsp;
	}
	public void afterPropertiesSet() throws Exception
	{
		if(null != baseService)
		{
			baseService.setEnitiyClass(this.GetTableClass());	
		}		
	}
	private  Class GetTableClass()
	{
		// TODO Auto-generated method stub
		return  ReflectionUtil.getSuperClassGenricType(getClass());
	}
}
