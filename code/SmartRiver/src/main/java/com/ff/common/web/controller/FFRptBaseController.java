/**
 * @Description 
 * @author tangjun
 * @date 2016年8月10日
 * 
 */
package com.ff.common.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ff.bi.model.RptDataRspJson;
import com.ff.bi.model.RptDimDataJson;
import com.ff.bi.service.JeeRptBaseService;
import com.ff.common.dao.model.FFCondition;
import com.ff.common.util.meta.DataFilterUtil;
import com.ff.common.web.json.BaseReqJson;
import com.ff.common.web.json.BaseRspJson;

import cn.jeeweb.modules.supervise.entity.McProItemRealTimeMonData;

/**
 * @Description 
 * @author tangjun
 * @date 2016年8月10日
 */
public class FFRptBaseController<E> extends FFTableController<E>
{
 

	@RequestMapping("/load")
	@ResponseBody
	@Override
	public BaseRspJson Load(@RequestBody BaseReqJson<?> request) {
		// TODO Auto-generated method stub
		BaseRspJson<RptDimDataJson> rsp = new BaseRspJson<RptDimDataJson>();
		List<E>  dataList = this.baseService.listWithNoPage(this.GetFilterCondition(request));
		
		RptDimDataJson obj = this.getObj(request, RptDimDataJson.class);
		
		List<?> x_data =  DataFilterUtil.getFieldValueFromList(dataList, obj.getDim());
		List<?> y_data =  DataFilterUtil.getFieldValueFromList(dataList, obj.getIndicator());

		obj.setX_data(x_data);
		obj.setY_data(y_data);
	 
		rsp.setObj(obj);
		
		
		return rsp;
	}
	
 

}
