/**   
 * @Title: MispHttpClient.java 
 * @Package cn.fuego.misp.service.http 
 * @Description: TODO
 * @author Tang Jun   
 * @date 2015-3-27 下午6:46:07 
 * @version V1.0   
 */
package com.ff.common.service.http;

import java.net.URLDecoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.ff.common.util.log.FFLogFactory;

/**
 * @ClassName: MispHttpClient
 * @Description: TODO
 * @author Tang Jun
 * @date 2015-3-27 下午6:46:07
 * 
 */
public class FFHttpClient
{
	private static final Logger log = FFLogFactory.getLog(FFHttpClient.class);
	private static final String CODE_WITH_UTF_8 = "utf-8";
	public static String httpPost(String url, String req)
	{
		// post请求返回结果
		
		String rsp = null;

		
		if(url.contains("https://"))
		{
			try
			{
				rsp = FFHttpsClient.doPost(url, req);
			}
			catch (Exception e) {
				log.error("post failed ",e);
			}
 			
		}
		else
		{
			try
			{
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost method = new HttpPost(url);
				if (null != req)
				{
					// 解决中文乱码问题
					StringEntity entity = new StringEntity(req, CODE_WITH_UTF_8);
					entity.setContentEncoding(CODE_WITH_UTF_8);
					// entity.setContentType("application/json");
					method.setEntity(entity);
				}
				HttpResponse result = httpClient.execute(method);
				url = URLDecoder.decode(url, CODE_WITH_UTF_8);
				/** 请求发送成功，并得到响应 **/
				if (result.getStatusLine().getStatusCode() == 200)
				{
 
						rsp = EntityUtils.toString(result.getEntity(),CODE_WITH_UTF_8);
						
 
				}
			} catch (Exception e)
			{
				log.error("post请求提交失败:" + url, e);
			}
		}
		
		log.info("the rsp is "+rsp);

		return rsp;
	}
	
	
	
}
