package com.ff.common.util.chart;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.charts.AxisPosition;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.TextAnchor;

import com.ff.common.util.log.FFLogFactory;

public class ChartUtil {
	
	private static Logger log = FFLogFactory.getLog(ChartUtil.class);
	
	
 
	
	public static void createChart(String path,String name,XYDataset dataSet) {

 
		try {

		    StandardChartTheme standardChartTheme = new StandardChartTheme("CN");
	        standardChartTheme.setExtraLargeFont(new Font("宋书", Font.PLAIN, 15));
	        standardChartTheme.setRegularFont(new Font("宋书", Font.PLAIN, 15));
	        standardChartTheme.setLargeFont(new Font("宋书", Font.PLAIN, 15));
	        ChartFactory.setChartTheme(standardChartTheme);
 	        
			JFreeChart chart = ChartFactory.createTimeSeriesChart(null, // 报表题目，字符串类型
 			"时间", // 横轴
 			name, // 纵轴
 			dataSet, // 获得数据集
 
 			true, // 显示图例
 			false, // 不用生成工具
 			false // 不用生成URL地址
 			);
 
 			XYPlot plot = chart.getXYPlot();

			// 图像属性部分
 			plot.setBackgroundPaint(Color.white);
 			plot.setDomainGridlinesVisible(true); // 设置背景网格线是否可见
 			plot.setDomainGridlinePaint(Color.BLACK); // 设置背景网格线颜色
 			plot.setRangeGridlinePaint(Color.GRAY);
 			plot.setNoDataMessage("没有数据");// 没有数据时显示的文字说明。
 			// 数据轴属性部分
 			
			NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
 			rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
 			rangeAxis.setAutoRangeIncludesZero(true); // 自动生成
 			 
			rangeAxis.setLabelPaint(Color.black);
 			rangeAxis.setUpperMargin(0.20);
 			rangeAxis.setLabelAngle(0);
 			rangeAxis.setAutoRange(false);

  			XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
 			renderer.setBaseItemLabelsVisible(true);
 			renderer.setSeriesPaint(0, Color.black); // 设置折线的颜色
  			renderer.setBaseItemLabelsVisible(false);
 			renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
 			ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_LEFT));
 
 			renderer.setBaseItemLabelGenerator(new StandardXYItemLabelGenerator());;
 
  			DateAxis axis = (DateAxis) plot.getDomainAxis(); 
  			axis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));
  			axis.setAutoTickUnitSelection(true);
 

			renderer.setBaseItemLabelFont(new Font("Dialog", 1, 14)); // 设置提示折点数据形状

			plot.setRenderer(renderer);
 
 
			File fos_jpg = new File(path);
 
			ChartUtilities.saveChartAsJPEG(fos_jpg, chart, // 统计图表对象
 			600, // 宽
 			300 // 高
 			);

		} catch (IOException e) {

			e.printStackTrace();
			log.error("create image failed",e);

		}

	}
	
	public static void createYChart(String path,String xName,String yName,XYDataset dataSet) {

		 
		try {

		    StandardChartTheme standardChartTheme = new StandardChartTheme("CN");
	        standardChartTheme.setExtraLargeFont(new Font("宋书", Font.PLAIN, 12));
	        standardChartTheme.setRegularFont(new Font("宋书", Font.PLAIN, 12));
	        standardChartTheme.setLargeFont(new Font("宋书", Font.PLAIN, 12));
	        ChartFactory.setChartTheme(standardChartTheme);
 	        
			JFreeChart chart = ChartFactory.createXYLineChart(null, // 报表题目，字符串类型
			xName, // 横轴
			yName, // 纵轴
 			dataSet, // 获得数据集
 			PlotOrientation.HORIZONTAL,
 			true, // 显示图例
 			false, // 不用生成工具
 			false // 不用生成URL地址
 			);
 
 			XYPlot plot = chart.getXYPlot();

 			
 			plot.getDomainAxis().setInverted(true);//设置X轴倒序 
 			plot.getRangeAxis().setInverted(true);
 		 
 	 
 			plot.setDomainAxisLocation(AxisLocation.TOP_OR_RIGHT);
 			plot.setRangeAxisLocation(AxisLocation.TOP_OR_RIGHT);
			// 图像属性部分
 			plot.setBackgroundPaint(Color.white);
 			plot.setDomainGridlinesVisible(true); // 设置背景网格线是否可见
 			plot.setDomainGridlinePaint(Color.BLACK); // 设置背景网格线颜色
 			plot.setRangeGridlinePaint(Color.GRAY);
 			plot.setNoDataMessage("没有数据");// 没有数据时显示的文字说明。
 			
 			
 			// 数据轴属性部分
 			
			NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
 			rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
 			rangeAxis.setAutoRangeIncludesZero(true); // 自动生成
 			 
			rangeAxis.setLabelPaint(Color.black);
 			rangeAxis.setUpperMargin(0.20);
 			rangeAxis.setLabelAngle(0);
 			rangeAxis.setAutoRange(false);

  			XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
 			renderer.setBaseItemLabelsVisible(true);
 			renderer.setSeriesPaint(0, Color.black); // 设置折线的颜色
  			renderer.setBaseItemLabelsVisible(false);
 			renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
 			ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_LEFT));
 
 			renderer.setBaseItemLabelGenerator(new StandardXYItemLabelGenerator());;
  

			renderer.setBaseItemLabelFont(new Font("Dialog", 1, 14)); // 设置提示折点数据形状

			plot.setRenderer(renderer);
 
 
			File fos_jpg = new File(path);
 
			ChartUtilities.saveChartAsJPEG(fos_jpg, chart, // 统计图表对象
 			186, // 宽
 			796 // 高
 			);

		} catch (IOException e) {

			e.printStackTrace();
			log.error("create image failed",e);

		}

	}
}
