package com.ff.common.service;

import com.ff.common.constant.FFErrorCode;

public class FFException extends RuntimeException 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private int errorCode = FFErrorCode.FAIL;

	public FFException()
	{
		super();
	}
	
	public FFException(int errorCode)
	{
		super(String.valueOf(errorCode));

		this.errorCode = errorCode;
	}
	
	public FFException(int errorCode,Throwable cause)
	{
 
		this.errorCode = errorCode;
	}

	public FFException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		// super(message, cause, enableSuppression, writableStackTrace);
	}

	public FFException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public FFException(String message)
	{
		super(message);
	}

	public FFException(Throwable cause)
	{
		super(cause);
	}

	
	public int getErrorCode()
	{
		return errorCode;
	}
	
 
	@Override
	public String toString()
	{
		String message = getLocalizedMessage();
		return (message != null) ? (message) : "";

	}
}
