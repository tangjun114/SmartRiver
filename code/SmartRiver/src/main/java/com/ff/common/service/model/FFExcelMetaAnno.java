/**
 * @Description
 * @author tangjun
 * @date 2016年8月17日
 */
package com.ff.common.service.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @Description
 * @author tangjun
 * @date 2016年8月17日
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface FFExcelMetaAnno {
    public String name();

}
