package com.ff.common.api;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.ff.common.dao.model.FFCondition;
import com.ff.common.web.json.BaseRspJson;
import com.ff.common.web.json.PageDataJson;

@Service()
@Scope("prototype")
public interface BaseApiService
{
	public BaseRspJson<?> getAll(List<FFCondition> conditionList,int currentPage,int pageSize);
}
