package com.ff.common.service;

import java.util.List;

import com.ff.common.dao.model.FFCondition;
import com.ff.common.web.json.FFSysUser;
import com.ff.common.web.json.PageDataJson;
import com.ff.common.web.json.PageJson;

public interface IBaseService<E>
{
	public PageDataJson<E> load(FFSysUser user,List<FFCondition> conditionList,PageJson page);
	
	public List<E> load(FFSysUser user,List<FFCondition> conditionList);
 	public E create(FFSysUser user,E obj);
	public void update(FFSysUser user,E obj);
 	public E show(FFSysUser user,String id);
	public void delete(FFSysUser user,String id);
	public void delete(FFSysUser user,List<String> idList);


}
